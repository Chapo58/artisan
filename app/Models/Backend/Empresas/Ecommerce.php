<?php

namespace App\Models\Backend\Empresas;

use App\Models\Backend\Permisos\Permiso;
use App\Models\Backend\Planes\Plan;
use App\Models\Frontend\Configuraciones\CondicionIva;
use App\Models\Frontend\Configuraciones\Moneda;
use App\Models\Frontend\Personas\Domicilio;
use App\Models\Frontend\Ventas\ListaPrecio;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ecommerce extends Model{

    use SoftDeletes;

    protected $table = 'ecommerce';

    protected $primaryKey = 'id';

    protected $fillable = [
        'empresa_id',
        'nombre',
        'logo_url',
        'telefono',
        'telefono2',
        'telefono3',
        'email',
        'email2',
        'domicilio_id',
        'facebook',
        'twitter',
        'instagram',
        'google_plus',
        'linkedin',
        'skype',
        'habilitado',
        'estilo',
        'color',
        'descripcion',
        'politica_privacidad',
        'terminos_condiciones',
        'precio_final',
        'mostrar_precios',
        'compras_sin_stock',
        'articulos_sin_stock',
        'moneda_id',
        'lista_precio_id',
        'tawkto_habilitado',
        'tawkto_codigo'
    ];

    protected $dates = ['deleted_at'];

    public function empresa(){
        return $this->belongsTo(Empresa::class);
    }

    public function domicilio(){
        return $this->belongsTo(Domicilio::class);
    }

    public function logo(){
            if($this->logo_url){
                return url(substr($this->logo_url, 1));
            } else {
                return url('images/frontend/logos/logo_mini.png');
            }
    }

    public function moneda(){
        return $this->belongsTo(Moneda::class);
    }

    public function listaPrecio(){
        return $this->belongsTo(ListaPrecio::class, 'lista_precio_id');
    }

    public function __toString(){
        return (string) $this->nombre;
    }
}
