<?php

namespace App\Models\Backend\Empresas;

use App\Models\Access\User\User;
use App\Models\Frontend\Configuraciones\CondicionIva;
use App\Models\Frontend\Personas\Domicilio;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Distribuidor extends Model{

    use SoftDeletes;

    protected $table = 'distribuidores';

    protected $primaryKey = 'id';

    protected $fillable = ['razon_social', 'cuit', 'email', 'web', 'telefono', 'condicion_iva_id', 'domicilio_id', 'usuario_id','logo','favicon'];

    protected $dates = ['deleted_at'];

    public function condicionIva(){
        return $this->belongsTo(CondicionIva::class, 'condicion_iva_id');
    }

    public function usuario(){
        return $this->belongsTo(User::class, 'usuario_id');
    }

    public function domicilio(){
        return $this->belongsTo(Domicilio::class);
    }

    public function empresas(){
        return $this->hasMany(Empresa::class);
    }

    public static function getDistrivuidoresArray(){
        $distribuidores = ['' => 'Seleccione un distribuidor...'] + Distribuidor::pluck('razon_social', 'id')->all();
        return $distribuidores;
    }

    public function __toString(){
        return (string) $this->razon_social;
    }
}
