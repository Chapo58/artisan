<?php

namespace App\Models\Backend\Empresas;

use App\Models\Backend\Permisos\Permiso;
use App\Models\Backend\Planes\Plan;
use App\Models\Frontend\Configuraciones\CondicionIva;
use App\Models\Frontend\Personas\Domicilio;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Empresa extends Model{

    use SoftDeletes;

    protected $table = 'empresas';

    protected $primaryKey = 'id';

    protected $fillable = [
        'razon_social',
        'nombre',
        'imagen_url',
        'cuit',
        'email',
        'web',
        'telefono',
        'ingresos_brutos',
        'inicio_actividad',
        'condicion_iva_id',
        'domicilio_id',
        'distribuidor_id',
        'fecha_vencimiento',
        'plan_id'
    ];

    protected $dates = ['deleted_at', 'inicio_actividad', 'fecha_vencimiento'];

    public function condicionIva(){
        return $this->belongsTo(CondicionIva::class, 'condicion_iva_id');
    }

    public function distribuidor(){
        return $this->belongsTo(Distribuidor::class, 'distribuidor_id');
    }

    public function domicilio(){
        return $this->belongsTo(Domicilio::class);
    }

    public function plan(){
        return $this->belongsTo(Plan::class);
    }

    public function sucursales(){
        return $this->hasMany(Sucursal::class);
    }

    public function ecommerce(){
        return $this->hasOne(Ecommerce::class);
    }

    public static function listarEmpresasPorDistribuidor($distribuidor, $perPage = 25, $keyword = null){
        $query = Empresa::whereNull('deleted_at');
        if(!$distribuidor->usuario->hasRole('Administrador')){
            $query->where('distribuidor_id', $distribuidor->id);
        }
        $query->where(function ($q) use ($keyword) {
            $q->where('razon_social', 'LIKE', "%$keyword%")
                ->orWhere('nombre', 'LIKE', "%$keyword%")
                ->orWhere('cuit', 'LIKE', "%$keyword%")
                ->orWhere('email', 'LIKE', "%$keyword%")
                ->orWhere('web', 'LIKE', "%$keyword%")
                ->orWhere('telefono', 'LIKE', "%$keyword%")
                ->orWhere('ingresos_brutos', 'LIKE', "%$keyword%");
        });
        return $query->paginate($perPage);
    }

    public function logo($pdf = null){
        if($pdf){
            if($this->imagen_url){
                return substr($this->imagen_url, 1);
            } else if(isset($this->distribuidor) && $this->distribuidor->logo_url){
                return substr($this->distribuidor->logo_url, 1);
            } else {
                return 'images/frontend/logos/logo_mini.png';
            }
        } else {
            if($this->imagen_url){
                return url(substr($this->imagen_url, 1));
            } else if(isset($this->distribuidor) && $this->distribuidor->logo_url){
                return url(substr($this->distribuidor->logo_url, 1));
            } else {
                return url('images/frontend/logos/logo_mini.png');
            }
        }
    }

    public function permisos(){
        return $this->belongsToMany(Permiso::class);
    }

    public function __toString(){
        return (string) $this->razon_social;
    }
}
