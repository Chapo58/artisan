<?php

namespace App\Models\Backend\Empresas;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sucursal extends Model
{

    use SoftDeletes;

    protected $table = 'sucursales';

    protected $primaryKey = 'id';

    protected $fillable = ['empresa_id', 'domicilio_id', 'nombre', 'email', 'telefono'];

    protected $dates = ['deleted_at'];

    public function empresa(){
        return $this->belongsTo(Empresa::class);
    }

    public static function getSucursalesPorEmpresa($empresa){
        $sucursales = Sucursal::where('empresa_id', $empresa->id)
            ->whereNull('deleted_at')
            ->get();
        return $sucursales;
    }

    public static function getSucursalesPorEmpresaArray($empresa){
        $sucursales = self::getSucursalesPorEmpresa($empresa)->pluck('nombre', 'id')->all();
        return $sucursales;
    }

    public function __toString(){
        return (string) $this->nombre;
    }
}
