<?php

namespace App\Models\Backend\Permisos;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Modulo extends Model{

    use SoftDeletes;

    protected $table = 'modulos';

    protected $primaryKey = 'id';

    protected $fillable = ['nombre', 'titulo'];

    protected $dates = ['deleted_at'];

    public static function getModulos(){
        $modulos = Modulo::whereNull('deleted_at')
            ->get();
        return $modulos;
    }

    public function permisos(){
        return $this->hasMany(Permiso::class);
    }

    public function __toString(){
        return (string) $this->titulo;
    }
}
