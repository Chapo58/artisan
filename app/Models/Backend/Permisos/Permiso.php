<?php

namespace App\Models\Backend\Permisos;

use App\Models\Backend\Empresas\Empresa;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Permiso extends Model{

    use SoftDeletes;

    protected $table = 'permisos';

    protected $primaryKey = 'id';

    protected $fillable = ['nombre', 'titulo'];

    protected $dates = ['deleted_at'];

    public static function getPermisos(){
        $permisos = Permiso::whereNull('deleted_at')
            ->get();
        return $permisos;
    }

    public function modulo(){
        return $this->belongsTo(Modulo::class);
    }

    public function empresas(){
        return $this->belongsToMany(Empresa::class);
    }

    public function __toString(){
        return (string) $this->titulo;
    }
}
