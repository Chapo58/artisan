<?php

namespace App\Models\Backend\Configuraciones;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tawkto extends Model{

    use SoftDeletes;

    protected $table = 'tawkto';

    protected $primaryKey = 'id';

    protected $fillable = ['distribuidor_id', 'codigo', 'habilitado'];

    protected $dates = ['deleted_at'];

    public function __toString(){
        return (string) $this->codigo;
    }
}
