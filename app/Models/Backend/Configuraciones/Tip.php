<?php

namespace App\Models\Backend\Configuraciones;

use App\Models\Access\User\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tip extends Model{

    use SoftDeletes;

    protected $table = 'tips';

    protected $primaryKey = 'id';

    protected $fillable = ['texto', 'ruta'];

    protected $dates = ['deleted_at'];

    public function users(){
        return $this->belongsToMany(User::class)
            ->withPivot('entendido');
    }

    public function __toString(){
        return (string) $this->texto;
    }
}
