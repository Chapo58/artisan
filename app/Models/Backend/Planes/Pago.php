<?php

namespace App\Models\Backend\Planes;

use App\Models\Access\User\User;
use App\Models\Backend\Empresas\Distribuidor;
use App\Models\Backend\Empresas\Empresa;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pago extends Model{

    use SoftDeletes;

    protected $table = 'pagos';

    protected $primaryKey = 'id';

    protected $fillable = ['usuario_id','empresa_id','distribuidor_id', 'forma_pago_id', 'plan_id', 'periodo','importe','nota','observaciones','estado'];

    protected $dates = ['deleted_at'];

    public function usuario(){
        return $this->belongsTo(User::class, 'usuario_id');
    }

    public function empresa(){
        return $this->belongsTo(Empresa::class);
    }

    public function distribuidor(){
        return $this->belongsTo(Distribuidor::class, 'distribuidor_id');
    }

    public function formaPago(){
        return $this->belongsTo(FormaPago::class);
    }

    public function plan(){
        return $this->belongsTo(Plan::class);
    }

    public function __toString(){
        return (string) $this->usuario->empresa;
    }
}
