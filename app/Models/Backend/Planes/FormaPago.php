<?php

namespace App\Models\Backend\Planes;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FormaPago extends Model{

    use SoftDeletes;

    protected $table = 'formas_pagos';

    protected $primaryKey = 'id';

    protected $fillable = ['tipo_forma_pago', 'distribuidor_id', 'empresa_id', 'instrucciones', 'habilitado'];

    protected $dates = ['deleted_at'];

    public static function getFormasPagoPorDistribuidor($distribuidor){
        if($distribuidor){
            $formasPago = FormaPago::where('distribuidor_id', $distribuidor->id)
                ->where('habilitado',1)
                ->get();
        }
        if(!isset($formasPago) || !$formasPago->first()){ // Si el distribuidor todavia no cargo ningun metodo de pago se usan los nuestros
            $formasPago = FormaPago::where('distribuidor_id', 1)
                ->where('habilitado',1)
                ->get();
        }
        return $formasPago;
    }

    public static function getFormasPagoPorDistribuidorArray($distribuidor){
        $formasPago = self::getFormasPagoPorDistribuidor($distribuidor)->pluck('tipo_forma_pago', 'id')->all();
        return $formasPago;
    }

    public static function getFormasPagoPorEmpresa($empresa){
        $formasPago = FormaPago::where('empresa_id', $empresa->id)
            ->where('habilitado',1)
            ->get();
        return $formasPago;
    }

    public static function getFormasPagoPorEmpresaArray($empresa){
        $formasPago = self::getFormasPagoPorEmpresa($empresa)->pluck('tipo_forma_pago', 'id')->all();
        return $formasPago;
    }

    public function __toString(){
        return (string) $this->tipo_forma_pago;
    }
}
