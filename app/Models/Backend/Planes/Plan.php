<?php

namespace App\Models\Backend\Planes;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Plan extends Model{

    use SoftDeletes;

    protected $table = 'planes';

    protected $primaryKey = 'id';

    protected $fillable = ['nombre', 'cantidad_usuarios', 'cantidad_puntos_venta', 'cantidad_sucursales', 'chat', 'precio','link_mercadopago_mensual','link_mercadopago_anual'];

    protected $dates = ['deleted_at'];

    public function __toString(){
        return (string) $this->nombre;
    }
}
