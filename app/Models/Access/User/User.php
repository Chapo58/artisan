<?php

namespace App\Models\Access\User;

use App\Models\Backend\Configuraciones\Tip;
use App\Models\Backend\Empresas\Distribuidor;
use App\Models\Backend\Empresas\Empresa;
use App\Models\Backend\Empresas\Sucursal;
use App\Models\Backend\Permisos\Modulo;
use App\Models\Backend\Permisos\Permiso;
use App\Models\Frontend\Configuraciones\PuntoDeVenta;
use App\Models\Frontend\Personas\Cliente;
use App\Models\Frontend\Personas\Empleado;
use Illuminate\Notifications\Notifiable;
use App\Models\Access\User\Traits\UserAccess;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Access\User\Traits\Scope\UserScope;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\Access\User\Traits\UserSendPasswordReset;
use App\Models\Access\User\Traits\Attribute\UserAttribute;
use App\Models\Access\User\Traits\Relationship\UserRelationship;

/**
 * Class User.
 */
class User extends Authenticatable
{
    use UserScope,
        UserAccess,
        Notifiable,
        SoftDeletes,
        UserAttribute,
        UserRelationship,
        UserSendPasswordReset;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'status',
        'confirmation_code',
        'confirmed',
        'empresa_id',
        'empleado_id',
        'sucursal_id',
        'panel_color',
        'panel_orientacion',
        'avatar',
        'role_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->table = config('access.users_table');
    }

    public static function getUsuariosPorEmpresa($empresa,$vendedor = null){
        $query = User::select('users.id','empleados.nombre');
        $query->where('users.empresa_id', $empresa->id);
        $query->join('empleados','empleados.id','users.empleado_id');
        if($vendedor){
            $query->join('role_user','role_user.user_id','users.id');
            $query->where('role_user.role_id',5);
        }
        $query->whereNull('users.deleted_at');
        $usuarios = $query->get();

        return $usuarios;
    }

    public static function getUsuariosPorEmpresaArray($empresa,$vendedor = null){
        $usuarios = User::getUsuariosPorEmpresa($empresa,$vendedor)->sortBy('nombre')->pluck('nombre', 'id')->all();
        return ['' => 'Seleccione un Usuario...'] + $usuarios;
    }

    public function controladoraFiscal(){
        $puntoDeVenta = PuntoDeVenta::where('usuario_id_fiscal',$this->id)->first();
        return $puntoDeVenta;
    }

    public function distribuidor(){
        $distribuidor = Distribuidor::where('usuario_id',$this->id)->first();
        return $distribuidor;
    }

    public function permiso($nombre,$modulo = false){
        $empresa = $this->empresa;
        if(!$modulo){ // Si no esta preguntando por un modulo
            $permiso = Permiso::where('nombre',$nombre)->first();
            if($permiso){
                if($empresa->permisos->contains($permiso->id)){
                    return true;
                }
            }
        } else {
            $modulo = Modulo::where('nombre',$nombre)->first();
            if($modulo){
                if($empresa->permisos->where('modulo_id',$modulo->id)->first()){
                    return true;
                }
            }
        }
        return false;
    }

    public function tips($url){
        $tip = Tip::where('ruta','=',$url)->first();
        if($tip){
            if($tip->users()->where('user_id', $this->id)->where('entendido','=',0)->exists()){
                $return = '
                <div class="alert alert-info alert-rounded row">
                    <div class="col-md-9">
                        <h3 class="text-info"><i class="fa fa-exclamation-circle"></i> Tips</h3>'
                        .$tip->texto.'
                    </div>
                    <div class="col-md-3">
                        <button type="button" class="btn btn-rounded btn-info pull-right m-t-20" onclick="marcarTipLeido();" data-dismiss="alert" aria-label="Cerrar">
                            Entendido
                        </button>
                    </div>
                </div>';
                return $return;
            } else {
                return '';
            }
        } else {
            return '';
        }

    }

    public function __toString(){
        return $this->name;
    }
}
