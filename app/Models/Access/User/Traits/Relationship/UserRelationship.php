<?php

namespace App\Models\Access\User\Traits\Relationship;

use App\Models\Access\User\SocialLogin;
use App\Models\Backend\Configuraciones\Tip;
use App\Models\Backend\Empresas\Empresa;
use App\Models\Backend\Empresas\Sucursal;
use App\Models\Backend\Permisos\Permiso;
use App\Models\Frontend\Personas\Cliente;
use App\Models\Frontend\Personas\Empleado;

/**
 * Class UserRelationship.
 */
trait UserRelationship
{
    /**
     * Many-to-Many relations with Role.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany(config('access.role'), config('access.role_user_table'), 'user_id', 'role_id');
    }

    /**
     * @return mixed
     */
    public function providers()
    {
        return $this->hasMany(SocialLogin::class);
    }

    public function empresa(){
        return $this->belongsTo(Empresa::class);
    }

    public function sucursal(){
        return $this->belongsTo(Sucursal::class);
    }

    public function empleado(){
        return $this->belongsTo(Empleado::class, 'empleado_id');
    }

    public function cliente(){
        return $this->hasOne(Cliente::class);
    }

    public function permisos(){
        return $this->belongsToMany(Permiso::class);
    }

    public function tips(){
        return $this->belongsToMany(Tip::class)
            ->withPivot('entendido');
    }

}
