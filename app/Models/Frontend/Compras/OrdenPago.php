<?php

namespace App\Models\Frontend\Compras;

use App\Models\Backend\Empresas\Empresa;
use App\Models\Backend\Empresas\Sucursal;
use App\Models\Frontend\Configuraciones\Moneda;
use App\Models\Frontend\Personas\Proveedor;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class OrdenPago extends Model{

    use SoftDeletes;

    protected $table = 'ordenes_pagos';

    protected $primaryKey = 'id';

    protected $fillable = ['cotizacion', 'numero', 'importe', 'total_pago', 'estado', 'descripcion', 'empresa_id', 'sucursal_id', 'proveedor_id', 'moneda_id'];

    protected $dates = ['deleted_at', 'fecha'];

    public function empresa(){
        return $this->belongsTo(Empresa::class);
    }

    public function sucursal(){
        return $this->belongsTo(Sucursal::class);
    }

    public function proveedor(){
        return $this->belongsTo(Proveedor::class);
    }

    public function moneda(){
        return $this->belongsTo(Moneda::class);
    }

    public function detalles(){
        return $this->hasMany(DetalleOrden::class);
    }

    public function detallesPagos(){
        return $this->hasMany(DetallePagoOrden::class);
    }

    public static function getOrdenesPorEmpresa($empresa){
        $ordenes = OrdenPago::where('empresa_id', $empresa->id)
            ->whereNull('deleted_at')
            ->get();
        return $ordenes;
    }

    public static function getOrdenesPorSucursal($sucursal){
        $ordenes = OrdenPago::where('empresa_id', $sucursal->empresa->id)
            ->where('sucursal_id', $sucursal->id)
            ->whereNull('deleted_at')
            ->get();
        return $ordenes;
    }

    public static function getOrdenesPorEmpresaArray($empresa){
        $ordenes = self::getOrdenesPorEmpresa($empresa)->pluck('numero', 'id')->all();
        return ['' => 'Seleccione una orden...'] + $ordenes;
    }

    public static function getOrdenesPorSucursalArray($sucursal){
        $ordenes = self::getOrdenesPorSucursal($sucursal)->pluck('numero', 'id')->all();
        return ['' => 'Seleccione una orden...'] + $ordenes;
    }

    public static function listarOrdenesPorEmpresaSucursal($empresa, $sucursal = null){
        $query = OrdenPago::select('ordenes_pagos.id','ordenes_pagos.numero','ordenes_pagos.fecha','proveedores.razon_social','ordenes_pagos.importe', 'monedas.signo');
        $query->where('ordenes_pagos.empresa_id', $empresa->id);
        $query->join('proveedores', 'proveedores.id', '=', 'ordenes_pagos.proveedor_id');
        $query->join('monedas', 'monedas.id', 'ordenes_pagos.moneda_id');
        $query->whereNull('ordenes_pagos.deleted_at');
        if (Auth::user()->hasRole('Usuario Vendedor')) {
            $query->where('ordenes_pagos.sucursal_id', $sucursal->id);
        }
        $query->orderBy('ordenes_pagos.id','DESC');
        return $query->get();
    }

    public function __toString(){
        return (string) $this->numero.' '.$this->proveedor;
    }
}
