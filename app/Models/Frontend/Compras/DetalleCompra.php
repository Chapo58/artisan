<?php

namespace App\Models\Frontend\Compras;

use App\Models\Frontend\Articulos\Articulo;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DetalleCompra extends Model{

    use SoftDeletes;

    protected $table = 'detalles_compras';

    protected $primaryKey = 'id';

    protected $fillable = ['compra_id', 'articulo_id', 'precio_neto', 'precio_iva', 'porcentaje_iva', 'cantidad', 'descripcion', 'estado'];

    protected $dates = ['deleted_at'];

    public function compra(){
        return $this->belongsTo(Compra::class);
    }

    public function articulo(){
        return $this->belongsTo(Articulo::class);
    }

    public function __toString(){
        return $this->precio. " - ". $this->articulo->nombre;
    }
}
