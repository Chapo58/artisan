<?php

namespace App\Models\Frontend\Compras;

use App\Models\Frontend\Articulos\Articulo;
use App\Models\Frontend\Compras\Compra;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DetallePagoCompra extends Model
{

    use SoftDeletes;

    protected $table = 'detalles_pago_compras';

    protected $primaryKey = 'id';

    protected $fillable = ['compra_id', 'entidad_id', 'forma_pago', 'monto', 'interes', 'descripcion', 'estado'];

    protected $dates = ['deleted_at', 'fecha'];

    public function compra(){
        return $this->belongsTo(Compra::class);
    }

    public function __toString(){
        return $this->descripcion;
    }
}
