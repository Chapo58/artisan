<?php

namespace App\Models\Frontend\Compras;

use App\Models\Backend\Empresas\Empresa;
use App\Models\Backend\Empresas\Sucursal;
use App\Models\Frontend\Personas\Proveedor;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use DB;

class OrdenCompra extends Model{

    use SoftDeletes;

    protected $table = 'ordenes_compras';

    protected $primaryKey = 'id';

    protected $fillable = ['empresa_id', 'sucursal_id', 'proveedor_id', 'fecha', 'numero', 'descripcion', 'estado'];

    protected $dates = ['fecha','deleted_at'];

    public function proveedor(){
        return $this->belongsTo(Proveedor::class);
    }

    public function detalles(){
        return $this->hasMany(DetalleOrdenCompra::class);
    }

    public function empresa(){
        return $this->belongsTo(Empresa::class);
    }

    public function sucursal(){
        return $this->belongsTo(Sucursal::class);
    }

    public static function getOrdenesComprasPorEmpresa($empresa){
        $ordenesCompras = OrdenCompra::where('empresa_id', $empresa->id)->get();
        return $ordenesCompras;
    }

    public static function getOrdenesComprasPorEmpresaArray($empresa){
        $ordenesCompras = self::getOrdenesComprasPorEmpresa($empresa)->pluck('numero', 'id')->all();
        return ['' => 'Seleccione una orden de compra...'] + $ordenesCompras;
    }

    public static function listarOrdenesComprasPorEmpresaSucursal($empresa, $sucursal = null){
        $query = OrdenCompra::select('ordenes_compras.id', 'ordenes_compras.fecha', 'ordenes_compras.numero', 'proveedores.razon_social');
        $query->join('proveedores', 'proveedores.id', '=', 'ordenes_compras.proveedor_id');
        $query->where('ordenes_compras.empresa_id', $empresa->id);
        $query->whereNull('ordenes_compras.deleted_at');
        $query->orderBy('ordenes_compras.fecha','DESC');
        if (Auth::user()->hasRole('Usuario Vendedor')) {
            $query->where('ordenes_compras.sucursal_id', $sucursal->id);
        }
        return $query->get();
    }

    public function __toString(){
        return 'Orden de Compra N° '.$this->numero;
    }
}
