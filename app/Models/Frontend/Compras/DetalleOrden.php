<?php

namespace App\Models\Frontend\Compras;

use App\Models\Frontend\Ventas\CuentaCorriente;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DetalleOrden extends Model{

    use SoftDeletes;

    protected $table = 'detalles_ordenes';

    protected $primaryKey = 'id';

    protected $fillable = ['descripcion', 'monto', 'orden_pago_id', 'cuenta_corriente_id', 'nota_debito_id'];

    protected $dates = ['deleted_at'];

    public function ordenDePago(){
        return $this->belongsTo(OrdenPago::class, 'orden_pago_id');
    }

    public function cuentaCorriente(){
        return $this->belongsTo(CuentaCorriente::class, 'cuenta_corriente_id');
    }

    public function notaCredito(){
//        return $this->belongsTo(CuentaCorriente::class);
    }

    public function __toString(){
        return (string) $this->id;
    }
}
