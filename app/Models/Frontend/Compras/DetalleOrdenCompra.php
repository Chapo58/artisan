<?php

namespace App\Models\Frontend\Compras;

use App\Models\Frontend\Articulos\Articulo;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DetalleOrdenCompra extends Model{

    use SoftDeletes;

    protected $table = 'detalles_ordenes_compras';

    protected $primaryKey = 'id';

    protected $fillable = ['orden_compra_id', 'articulo_id', 'articulo_nombre', 'cantidad', 'estado'];

    protected $dates = ['deleted_at'];

    public function ordenCompra(){
        return $this->belongsTo(OrdenCompra::class);
    }

    public function articulo(){
        return $this->belongsTo(Articulo::class);
    }

    public function __toString(){
        if($this->articulo){
            return '('.$this->articulo->codigo.') '.$this->articulo->nombre;
        } else {
            return $this->articulo_nombre;
        }
    }
}
