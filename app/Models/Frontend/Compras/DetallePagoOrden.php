<?php

namespace App\Models\Frontend\Compras;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DetallePagoOrden extends Model{

    use SoftDeletes;

    protected $table = 'detalles_pago_ordenes';

    protected $primaryKey = 'id';

    protected $fillable = ['orden_pago_id', 'entidad_id', 'forma_cobro', 'monto', 'interes', 'descripcion', 'estado'];

    protected $dates = ['deleted_at'];

    public function ordenDePago(){
        return $this->belongsTo(OrdenPago::class, 'orden_pago_id');
    }

    public function __toString(){
        return (string) $this->id;
    }
}
