<?php

namespace App\Models\Frontend\Compras;

use App\Models\Backend\Empresas\Empresa;
use App\Models\Backend\Empresas\Sucursal;
use App\Models\Frontend\Configuraciones\Estado;
use App\Models\Frontend\Configuraciones\Moneda;
use App\Models\Frontend\Configuraciones\PorcentajeIva;
use App\Models\Frontend\Configuraciones\PuntoDeVenta;
use App\Models\Frontend\Personas\Proveedor;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use DB;

class Compra extends Model{

    use SoftDeletes;

    protected $table = 'compras';

    protected $primaryKey = 'id';

    protected $fillable = ['empresa_id', 'sucursal_id', 'proveedor_id', 'tipo_comprobante_id', 'moneda_id', 'punto_venta', 'cotizacion', 'numero', 'importe_neto', 'importe_iva', 'importe_descuento', 'porcentaje_descuento', 'importe_impuesto_interno', 'retencion', 'concepto_no_gravado', 'total', 'descripcion', 'estado', 'total_pago', 'cae'];

    protected $dates = ['fecha','deleted_at', 'fecha_vencimiento'];

    public function proveedor(){
        return $this->belongsTo(Proveedor::class);
    }

    public function moneda(){
        return $this->belongsTo(Moneda::class);
    }

    public function puntoVenta(){
        return $this->belongsTo(PuntoDeVenta::class);
    }

    public function detalles(){
        return $this->hasMany(DetalleCompra::class);
    }

    public function detallesPagos(){
        return $this->hasMany(DetallePagoCompra::class);
    }

    public function empresa(){
        return $this->belongsTo(Empresa::class);
    }

    public function sucursal(){
        return $this->belongsTo(Sucursal::class);
    }

    public static function getComprasPorEmpresa($empresa){
        $compras = Compra::where('empresa_id', $empresa->id)->get();
        return $compras;
    }

    public static function getComprasPorEmpresaArray($empresa){
        $compras = Compra::select(DB::raw('CONCAT(punto_venta, "-" ,numero) AS nombre'), 'compras.*')
            ->where('empresa_id', $empresa->id)
            ->where('estado', Estado::ACTIVO)//Ver de hacer una busqueda por estado, por ahora son todas activas
            ->pluck('nombre', 'id')
            ->all();
        return ['' => 'Seleccione una compra...'] + $compras;
    }

    public static function listarComprasPorEmpresaSucursal($empresa, $sucursal = null){
        $query = Compra::join('proveedores', 'proveedores.id', '=', 'compras.proveedor_id');
        $query->join('monedas', 'monedas.id', '=', 'compras.moneda_id');
        $query->select('compras.id', 'compras.fecha', DB::raw("CONCAT(compras.punto_venta,'-',compras.numero) AS numero"), 'compras.tipo_comprobante_id', 'compras.total_pago', 'proveedores.razon_social', 'monedas.signo');
        $query->where('compras.empresa_id', $empresa->id);
        $query->whereNull('compras.deleted_at');
        $query->orderBy('compras.fecha','DESC');
        if (Auth::user()->hasRole('Usuario Vendedor')) {
            $query->where('compras.sucursal_id', $sucursal->id);
        }
        return $query->get();
    }

    public static function obtenerPorcentajesPeriodo($periodo){
        $empresa = Auth::user()->empresa;
        $year = substr($periodo, -4);
        $month = substr($periodo, 0, 2);

        $porcentajes = DetalleCompra::select('detalles_compras.porcentaje_iva')
            ->join('compras','compras.id','=','detalles_compras.compra_id')
            ->where('compras.empresa_id', $empresa->id)
            ->where('compras.punto_venta','<>', '9999')
            ->whereYear('compras.fecha', '=', $year)
            ->whereMonth('compras.fecha', '=', $month)
            ->groupBy('porcentaje_iva')
            ->get();

        return $porcentajes;
    }

    public function __toString(){
        return $this->punto_venta.'-'.$this->numero.' '.$this->proveedor->razon_social;
    }
}
