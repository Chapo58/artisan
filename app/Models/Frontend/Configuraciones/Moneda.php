<?php

namespace App\Models\Frontend\Configuraciones;

use App\Models\Backend\Empresas\Empresa;
use App\Models\Frontend\Fondos\CuentaBancaria;
use App\Models\Frontend\Fondos\MovimientoBancario;
use App\Models\Frontend\Produccion\Rollo;
use App\Models\Frontend\Caja\Caja;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Moneda extends Model{

    use SoftDeletes;

    protected $table = 'monedas';

    protected $primaryKey = 'id';

    protected $fillable = ['empresa_id', 'numero', 'nombre', 'signo', 'cotizacion'];

    protected $dates = ['deleted_at'];

    public function cuentasBancarias(){
      return $this->hasMany(CuentaBancaria::class);
    }

    public function movimientoBancario(){
      return $this->hasMany(MovimientoBancario::class);
    }

    public function cajas(){
      return $this->hasMany(Caja::class);
    }

    public function empresa(){
        return $this->belongsTo(Empresa::class);
    }

    public function rollos(){
        return $this->hasMany(Rollo::class);
    }

    public static function getMonedasPorEmpresa($empresa){
        $monedas = Moneda::where('empresa_id', $empresa->id)->get();
        return $monedas;
    }

    public static function getMonedasPorEmpresaArray($empresa){
        $monedas = self::getMonedasPorEmpresa($empresa)->pluck('nombre', 'id')->all();
        return ['' => 'Seleccione una moneda...'] + $monedas;
    }

    public static function getCotizacionDolar(){
        $data_in = "http://ws.geeklab.com.ar/dolar/get-dolar-json.php";
        $data_json = @file_get_contents($data_in);
        $libre = 0;
        $blue = 0;
        if(strlen($data_json)>0) {
            $data_out = json_decode($data_json,true);
            if(is_array($data_out)) {
                $libre = $data_out['libre'];
                $blue = $data_out['blue'];
            }
        }
        return $libre;
    }

    public function __toString(){
        return (string) $this->nombre;
    }
}
