<?php

namespace App\Models\Frontend\Configuraciones;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Historial extends Model
{

    use SoftDeletes;

    protected $table = 'historiales';

    protected $primaryKey = 'id';

    protected $fillable = ['user_id', 'entidad_id', 'empresa_id', 'tipo_historial', 'texto', 'url'];

    protected $dates = ['deleted_at'];

    public function __toString(){
        return (string) $this->id;
    }
}
