<?php

namespace App\Models\Frontend\Configuraciones;

use function GuzzleHttp\Psr7\str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EntidadRelacionadaLink extends Model{

    use SoftDeletes;

    protected $table = 'entidades_relacionadas_link';

    protected $primaryKey = 'id';

    protected $fillable = ['entidad_id', 'entidad_clase', 'url', 'presentacion'];

    protected $dates = ['deleted_at'];

    public function getLink(){
        return (string) $this->url . '/' . $this->entidad_id;
    }

    static public function crearNueva($objeto, $request, $url = null, $presentacion = null){
        $nuevoEntidadRelacionada = New EntidadRelacionadaLink();
        $nuevoEntidadRelacionada->entidad_id = $objeto->id;
        $nuevoEntidadRelacionada->entidad_clase = class_basename($objeto);
        if($url == null){
            $nuevoEntidadRelacionada->url = $request->getRequestUri();
        }else{
            $nuevoEntidadRelacionada->url = $url;
        }
        if($presentacion == null){
            $nuevoEntidadRelacionada->presentacion = $objeto->__toString();
        }else{
            $nuevoEntidadRelacionada->presentacion = $presentacion;
        }
        $nuevoEntidadRelacionada->save();

        return $nuevoEntidadRelacionada;
    }

    static public function buscar($objeto){
        $entidadRelacionada = EntidadRelacionadaLink::where('entidad_clase','=', class_basename($objeto))
            ->where('entidad_id','=',$objeto->id)
            ->whereNull('deleted_at')
            ->first();
        return $entidadRelacionada;
    }

    public function __toString(){
        return (string) $this->presentacion;
    }
}
