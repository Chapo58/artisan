<?php

namespace App\Models\Frontend\Configuraciones;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Provincia extends Model
{

    use SoftDeletes;

    protected $table = 'provincias';

    protected $primaryKey = 'id';

    protected $fillable = ['nombre', 'pais_id'];

    protected $dates = ['deleted_at'];

    public function pais(){
        return $this->belongsTo(Pais::class);
    }

    public function localidades(){
        return $this->hasMany(Localidad::class);
    }

    public function __toString(){
        return $this->nombre;
    }
}
