<?php

namespace App\Models\Frontend\Configuraciones;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TipoPuntoVenta extends Model{

    use SoftDeletes;

    protected $table = 'tipos_punto_venta';

    protected $primaryKey = 'id';

    protected $fillable = ['nombre', 'empresa_id'];

    protected $dates = ['deleted_at'];

    public function __toString(){
        return (string) $this->nombre;
    }
}
