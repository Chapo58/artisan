<?php

namespace App\Models\Frontend\Configuraciones;

use App\Models\Backend\Empresas\Sucursal;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Opcion extends Model{

    use SoftDeletes;

    protected $table = 'opciones';

    protected $primaryKey = 'id';

    protected $fillable = ['empresa_id', 'sucursal_id', 'punto_venta_id', 'presentacion', 'nombre', 'valor'];

    protected $dates = ['deleted_at'];

    public function sucursal(){
        return $this->belongsTo(Sucursal::class);
    }

    public function puntoVenta(){
        return $this->belongsTo(PuntoDeVenta::class, 'punto_venta_id');
    }

    public static function getValorOpcion($nombre, $empresa, $puntoDeVenta = null){
        $query = Opcion::where('empresa_id', $empresa->id);
        $query->where('nombre','like',$nombre);
        if($puntoDeVenta){
            $query->where('punto_venta_id', $puntoDeVenta);
        }
        $objetoOpcion = $query->first();

        if(isset($objetoOpcion)){
            $valor = $objetoOpcion->valor;
        }else{
            $valor = null;
        }
        return $valor;
    }

    public static function setValorOpcion($nombre, $valor, $empresa){
        $objetoOpcion = Opcion::where('empresa_id', $empresa->id)
            ->where('nombre','like',$nombre)->first();
        $objetoOpcion->valor = $valor;
        return $objetoOpcion->update();
    }

    public function __toString(){
        return (string) $this->presentacion;
    }
}
