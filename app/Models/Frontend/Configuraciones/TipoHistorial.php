<?php

namespace App\Models\Frontend\Configuraciones;

use Illuminate\Database\Eloquent\Model as Model;

class TipoHistorial extends Model{

    const CREADO       = 0;
    const ACTUALIZADO  = 1;
    const BORRADO      = 2;

    public static $estados = [
        self::CREADO        => 'Creado',
        self::ACTUALIZADO   => 'Actualizado',
        self::BORRADO       => 'Borrado',
    ];

    public function esCreado(){
        return $this->estados == self::CREADO;
    }

    public function esActualizado(){
        return $this->estados == self::ACTUALIZADO;
    }

    public function esBorrado(){
        return $this->estados == self::BORRADO;
    }

}
