<?php

namespace App\Models\Frontend\Configuraciones;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ActividadAfip extends Model{

    use SoftDeletes;

    protected $table = 'actividades_afip';

    protected $primaryKey = 'id';

    protected $fillable = ['empresa_id', 'sucursal_id', 'codigo', 'descripcion'];

    protected $dates = ['deleted_at'];

    public static function getActividadesAfipPorEmpresa($empresa){
        $actividadesAfip = ActividadAfip::where('empresa_id', $empresa->id)->get();
        return $actividadesAfip;
    }

    public static function getActividadesAfipPorEmpresaArray($empresa){
        $actividadesAfip = self::getActividadesAfipPorEmpresa($empresa)->pluck('codigo', 'id')->all();
        return ['' => 'Seleccione una actividad...'] + $actividadesAfip;
    }

    public function __toString(){
        return (string) $this->codigo . " " . $this->descripcion;
    }
}
