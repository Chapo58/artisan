<?php

namespace App\Models\Frontend\Configuraciones;

use App\Models\Backend\Empresas\Empresa;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Frontend\Fondos\Banco;

class TarjetaPropia extends Model{

    use SoftDeletes;

    protected $table = 'tarjetas_propias';

    protected $primaryKey = 'id';

    protected $fillable = ['nombre', 'titular', 'fecha_vencimiento', 'numero', 'limite', 'banco_id', 'tipo','sucursal_id','empresa_id'];

    protected $dates = ['deleted_at','fecha_vencimiento'];

    public function banco(){
      return $this->belongsTo(Banco::class);
    }

    public function empresa(){
        return $this->belongsTo(Empresa::class);
    }

    public static function getTarjetasPorEmpresa($empresa){
        $tarjetasPropias = TarjetaPropia::where('empresa_id', $empresa->id)
            ->whereNull('deleted_at')
            ->get();
        return $tarjetasPropias;
    }

    public static function getTarjetasPorEmpresaArray($empresa){
        $bancos = self::getTarjetasPorEmpresa($empresa)->pluck('nombre', 'id')->all();
        return ['' => 'Seleccione una tarjeta...'] + $bancos;
    }

    public static function listarTarjetasPorEmpresa($empresa){
        $query = TarjetaPropia::where('empresa_id', $empresa->id);
        $query->whereNull('deleted_at');
        return $query->get();
    }

    public function __toString(){
        return (string) $this->banco->nombre.' '.$this->nombre;
    }
}
