<?php

namespace App\Models\Frontend\Configuraciones;

use App\Models\Frontend\Personas\Proveedor;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CondicionIva extends Model{
    const MONOTRIBUTISTA                = 1;
    const RESPONSABLE_INSCRIPTO         = 2;
    const EXENTO                        = 4;
    const CONSUMIDOR_FINAL              = 5;

    use SoftDeletes;

    protected $table = 'condiciones_iva';

    protected $primaryKey = 'id';

    protected $fillable = ['nombre', 'alicuota'];

    protected $dates = ['deleted_at'];

    public static function condicionesEmpresas(){
        $condiciones = CondicionIva::where('nombre','<>','Consumidor Final')->get();
        return $condiciones;
    }

    public static function condicionesEmpresasArray(){
        $condiciones = self::condicionesEmpresas()->pluck('nombre', 'id')->all();
        return ['' => 'Seleccione una opción...'] + $condiciones;
    }

    public static function getCondicionesIva(){
        $condiciones = CondicionIva::get();
        return $condiciones;
    }

    public static function getCondicionesIvaArray(){
        $condiciones = self::getCondicionesIva()->pluck('nombre', 'id')->all();
        return ['' => 'Seleccione una opción...'] + $condiciones;
    }

    public function __toString(){
        return $this->nombre;
    }
}
