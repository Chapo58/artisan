<?php

namespace App\Models\Frontend\Configuraciones;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Unidad extends Model{

    use SoftDeletes;

    protected $table = 'unidades';

    protected $primaryKey = 'id';

    protected $fillable = ['nombre', 'presentacion'];

    protected $dates = ['deleted_at'];

    public function __toString(){
        return $this->nombre;
    }
}
