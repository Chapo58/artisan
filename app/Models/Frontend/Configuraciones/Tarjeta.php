<?php

namespace App\Models\Frontend\Configuraciones;

use App\Models\Backend\Empresas\Empresa;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Frontend\Ventas\CuponTarjeta;

class Tarjeta extends Model{

    use SoftDeletes;

    protected $table = 'tarjetas';

    protected $primaryKey = 'id';

    protected $fillable = ['numero', 'nombre', 'comercio','empresa_id'];

    protected $dates = ['deleted_at'];

    public function empresa(){
        return $this->belongsTo(Empresa::class);
    }

    public function planesTarjeta(){
      return $this->hasMany(PlanTarjeta::class);
    }

    public function cuponesTarjetas(){
      return $this->hasMany(CuponTarjeta::class);
    }

    public static function getTarjetasPorEmpresa($empresa){
        $tarjetas = Tarjeta::where('empresa_id', $empresa->id)
            ->whereNull('deleted_at')
            ->get();
        return $tarjetas;
    }

    public static function getTarjetasPorEmpresaArray($empresa){
        $tarjetas = Tarjeta::getTarjetasPorEmpresa($empresa)->sortBy('nombre')->pluck('nombre', 'id')->all();
        return ['' => 'Seleccione una tarjeta...'] + $tarjetas;
    }

    public function __toString(){
        return (string) $this->nombre;
    }
}
