<?php

namespace App\Models\Frontend\Configuraciones;

use Illuminate\Database\Eloquent\Model as Model;

class Estado extends Model{

    const BORRADO                                       = 0;
    const ACTIVO                                        = 1;
    const FINALIZADO                                    = 2;
    const EN_PROCESO                                    = 3;
    const DESACTIVADO                                   = 4;
    const PENDIENTE                                     = 5;
    const COBRADO                                       = 6;
    const DEPOSITADO                                    = 7;
    const ACREDITADO                                    = 8;
    const PLANTILLA                                     = 9;
    const FACTURA_AUTOMATICA_PENDIENTE                  = 10;
    const FACTURA_AUTOMATICA_PENDIENTE_FALTANTE_STOCK   = 11;
    const GUARDADO                                      = 12;

    public static $getArray = [
        self::BORRADO                       => 'Borrado',
        self::ACTIVO                        => 'Activa',
        self::FINALIZADO                    => 'Finalizado',
        self::EN_PROCESO                    => 'En proceso',
        self::DESACTIVADO                   => 'Desactivado',
        self::PENDIENTE                     => 'Pendiente',
        self::COBRADO                       => 'Cobrado',
        self::DEPOSITADO                    => 'Depositado',
        self::ACREDITADO                    => 'Acreditado',
        self::PLANTILLA                     => 'Plantilla',
        self::FACTURA_AUTOMATICA_PENDIENTE  => 'Factura Automática Pendiente',
        self::FACTURA_AUTOMATICA_PENDIENTE_FALTANTE_STOCK  => 'Factura Automática Pendiente Faltante Stock',
        self::GUARDADO                     => 'Guardado',
    ];

    public function esBorrado(){
        return $this->estados == self::BORRADO;
    }

    public function esActivo(){
        return $this->estados == self::ACTIVO;
    }

    public function esFinalizado(){
        return $this->estados == self::FINALIZADO;
    }

    public function esEnProceso(){
        return $this->estados == self::EN_PROCESO;
    }

    public function esDesactivado(){
        return $this->estados == self::DESACTIVADO;
    }

}
