<?php

namespace App\Models\Frontend\Configuraciones;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Localidad extends Model
{

    use SoftDeletes;

    protected $table = 'localidades';

    protected $primaryKey = 'id';

    protected $fillable = ['nombre', 'provincia_id'];

    protected $dates = ['deleted_at'];

    public function provincia(){
        return $this->belongsTo(Provincia::class);
    }

    public function __toString(){
        return (string) $this->nombre;
    }
}
