<?php

namespace App\Models\Frontend\Configuraciones;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pais extends Model
{

    use SoftDeletes;

    protected $table = 'paises';

    protected $primaryKey = 'id';

    protected $fillable = ['nombre'];

    protected $dates = ['deleted_at'];

    public function provincias(){
        return $this->hasMany(Provincia::class);
    }

    public function __toString(){
        return (string) $this->nombre;
    }
}
