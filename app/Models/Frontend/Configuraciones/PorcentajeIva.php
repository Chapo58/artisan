<?php

namespace App\Models\Frontend\Configuraciones;

use Illuminate\Database\Eloquent\Model as Model;

class PorcentajeIva extends Model{

    const CERO          = 0;
    const DIEZCOMACINCO = 1;
    const VEINTIUNO     = 2;
    const DOSCOMACINCO  = 3;
    const CINCO         = 4;
    const VEINTISIETE   = 5;

    public static $valores = [
        self::CERO          => 0,
        self::DIEZCOMACINCO => 10.5,
        self::VEINTIUNO     => 21,
        self::DOSCOMACINCO  => 2.5,
        self::CINCO         => 5,
        self::VEINTISIETE   => 27,
    ];

    public static $constantes = [
        self::CERO          => 0,
        self::DIEZCOMACINCO => 1,
        self::VEINTIUNO     => 2,
        self::DOSCOMACINCO  => 3,
        self::CINCO         => 4,
        self::VEINTISIETE   => 5,
    ];

    public function esCero(){
        return $this->estados == self::CERO;
    }

    public function esDiezComaCinco(){
        return $this->estados == self::DIEZCOMACINCO;
    }

    public function esVeintiuno(){
        return $this->estados == self::VEINTIUNO;
    }

    public function esDosComaCinco(){
        return $this->estados == self::DOSCOMACINCO;
    }

    public function esCinco(){
        return $this->estados == self::CINCO;
    }

    public function esVeintiSiete(){
        return $this->estados == self::VEINTISIETE;
    }

    public static $arrayCombo = [
        ''                  => 'Seleccione una opción...',
        self::CERO          => 0,
        self::DIEZCOMACINCO => 10.5,
        self::VEINTIUNO     => 21,
        self::DOSCOMACINCO  => 2.5,
        self::CINCO         => 5,
        self::VEINTISIETE   => 27,
    ];
}
