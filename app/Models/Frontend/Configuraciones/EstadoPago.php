<?php

namespace App\Models\Frontend\Configuraciones;

use Illuminate\Database\Eloquent\Model as Model;

class EstadoPago extends Model{

    const PENDIENTE   = 0;
    const ACEPTADO   = 1;
    const RECHAZADO   = 2;

    public static $getArray = [
        self::PENDIENTE   => 'Pendiente',
        self::ACEPTADO   => 'Aceptado',
        self::RECHAZADO   => 'Rechazado',
    ];

}
