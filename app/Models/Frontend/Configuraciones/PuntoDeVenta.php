<?php

namespace App\Models\Frontend\Configuraciones;

use App\Models\Access\User\User;
use App\Models\Backend\Empresas\Empresa;
use App\Models\Backend\Empresas\Sucursal;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class PuntoDeVenta extends Model{

    use SoftDeletes;

    protected $table = 'puntos_de_ventas';

    protected $primaryKey = 'id';

    protected $fillable = ['numero', 'tipo_punto_id', 'sucursal_id', 'empresa_id', 'host_fiscal', 'puerto_fiscal', 'modelo_impresora_fiscal', 'baudios_fiscal', 'usuario_id_fiscal', 'anchura_papel_comandera', 'cert_electronica', 'key_electronica'];

    protected $dates = ['deleted_at'];

    public function tipo(){
        return $this->belongsTo(TipoPuntoVenta::class, 'tipo_punto_id');
    }

    public function usuario(){
        return $this->belongsTo(User::class, 'usuario_id_fiscal');
    }

    public function sucursal(){
        return $this->belongsTo(Sucursal::class);
    }

    public function empresa(){
        return $this->belongsTo(Empresa::class);
    }

    public static function getPuntosVentaPorEmpresaSucursalArray($empresa, $sucursal = null){
        $query = PuntoDeVenta::select(DB::raw('CONCAT("Nº ", puntos_de_ventas.numero, " - Tipo " , tipos_punto_venta.nombre, " - Sucursal ", sucursales.nombre) AS punto_venta'), 'puntos_de_ventas.*')
            ->join('tipos_punto_venta', 'tipos_punto_venta.id', '=', 'puntos_de_ventas.tipo_punto_id')
            ->join('sucursales', 'sucursales.id', '=', 'puntos_de_ventas.sucursal_id')
            ->where('puntos_de_ventas.empresa_id', $empresa->id)
            ->whereNull('puntos_de_ventas.deleted_at');
            if(isset($sucursal)){
                $query->where(function($q) use ($sucursal) {
                    $q->where('puntos_de_ventas.sucursal_id', $sucursal->id);
                });
            }
        return ['' => 'Seleccione un punto de venta...'] + $query->pluck('punto_venta', 'puntos_de_ventas.id')->all();
    }

    public static function listarPuntosDeVentaPorEmpresaSucursal($empresa, $sucursal = null){
        $query = PuntoDeVenta::where('empresa_id', $empresa->id);
        $query->whereNull('deleted_at');
        if(isset($sucursal)){
            $query->where(function($q) use ($sucursal) {
                $q->where('sucursal_id', $sucursal->id);
            });
        }
        return $query->get();
    }

    public static function getPuntosDeVentaXPorSucursal($sucursal){
        $query = PuntoDeVenta::select('puntos_de_ventas.*');
        $query->leftJoin('tipos_punto_venta', 'tipos_punto_venta.id', '=', 'puntos_de_ventas.tipo_punto_id');
        $query->where('puntos_de_ventas.sucursal_id', $sucursal->id);
        $query->where(function($q) {
            $q->where('tipos_punto_venta.nombre', 'Comandera');
            $q->orWhere('tipos_punto_venta.nombre', 'Impresora Manual');
        });
        return $query->get();
    }

    public static function getPuntosDeVentaAPorSucursal($sucursal){
        $query = PuntoDeVenta::select('puntos_de_ventas.*');
        $query->leftJoin('tipos_punto_venta', 'tipos_punto_venta.id', '=', 'puntos_de_ventas.tipo_punto_id');
        $query->where('puntos_de_ventas.sucursal_id', $sucursal->id);
        $query->where(function($q) {
            $q->where('tipos_punto_venta.nombre', 'Impresora Fiscal');
            $q->orWhere('tipos_punto_venta.nombre', 'Factura Electrónica');
        });
        return $query->get();
    }

    public function esBlanco(){
        if($this->tipo == 'Impresora Fiscal' || $this->tipo == 'Factura Electrónica'){
            return true;
        } else {
            return false;
        }
    }

    public function __toString(){
        return (string) $this->numero;
    }
}
