<?php

namespace App\Models\Frontend\Configuraciones;

use Illuminate\Database\Eloquent\Model as Model;

class TipoTarjeta extends Model{

    const CREDITO       = 0;
    const DEBITO        = 1;


    public static $tipos = [
        self::CREDITO       => 'Credito',
        self::DEBITO        => 'Debito',
    ];

}
