<?php

namespace App\Models\Frontend\Configuraciones;

use Illuminate\Database\Eloquent\Model as Model;

class TipoControladoraFiscal extends Model{

    const Hasar715       = 0;
    const Hasar715v2       = 2;
    const Hasar615       = 3;
    const Hasar320       = 4;
    const HasarPR4       = 5;
    const HasarPR5F       = 6;
    const Hasar950       = 7;
    const Hasar951       = 8;
    const Hasar441       = 9;
    const Hasar321       = 10;
    const Hasar322       = 11;
    const Hasar322v2       = 12;
    const Hasar330       = 13;
    const Hasar1120       = 14;
    const HasarPL8F       = 15;
    const HasarPL8Fv2       = 16;
    const HasarPL23       = 17;
    const EpsonTM300AF       = 18;
    const EpsonTMU220AF       = 19;
    const EpsonTM2000       = 20;
    const EpsonTM2000AFPlus       = 21;
    const EpsonLX300       = 22;
    const HasarPT1000F       = 23;
    const EpsonTMT900FA       = 24;

    public static $arrayCombo = [
        ''                  => 'Seleccionar Modelo...',
        self::Hasar715     => 'Hasar715',
        self::Hasar715v2     => 'Hasar715v2',
        self::Hasar615     => 'Hasar615',
        self::Hasar320     => 'Hasar320',
        self::HasarPR4     => 'HasarPR4',
        self::HasarPR5F     => 'HasarPR5F',
        self::Hasar950     => 'Hasar950',
        self::Hasar951     => 'Hasar951',
        self::Hasar441     => 'Hasar441',
        self::Hasar321     => 'Hasar321',
        self::Hasar322     => 'Hasar322',
        self::Hasar322v2     => 'Hasar322v2',
        self::Hasar330     => 'Hasar330',
        self::Hasar1120     => 'Hasar1120',
        self::HasarPL8F     => 'HasarPL8F',
        self::HasarPL8Fv2     => 'HasarPL8Fv2',
        self::HasarPL23     => 'HasarPL23',
        self::EpsonTM300AF     => 'EpsonTM300AF',
        self::EpsonTMU220AF     => 'EpsonTMU220AF',
        self::EpsonTM2000     => 'EpsonTM2000',
        self::EpsonTM2000AFPlus     => 'EpsonTM2000AFPlus',
        self::EpsonLX300     => 'EpsonLX300',
        self::HasarPT1000F     => 'HasarPT1000F',
        self::EpsonTMT900FA     => 'EpsonTMT900FA',
    ];

}
