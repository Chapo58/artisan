<?php

namespace App\Models\Frontend\Configuraciones;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Frontend\Ventas\CuponTarjeta;

class PlanTarjeta extends Model{

    use SoftDeletes;

    protected $table = 'plan_tarjetas';

    protected $primaryKey = 'id';

    protected $fillable = ['nombre', 'tarjeta_id', 'interes', 'cuotas','dias_acreditacion'];

    protected $dates = ['deleted_at'];

    public function tarjeta(){
        return $this->belongsTo(Tarjeta::class);
    }

    public function cuponesTarjetas(){
        return $this->hasMany(CuponTarjeta::class);
    }

    public static function getPlanesPorEmpresa($empresa){
        $planesTarjeta = PlanTarjeta::select('plan_tarjetas.*')
            ->join('tarjetas', 'tarjetas.id', '=', 'plan_tarjetas.tarjeta_id')
            ->where('tarjetas.empresa_id', $empresa->id)
            ->get();
        return $planesTarjeta;
    }

    public static function getPlanesPorEmpresaArray($empresa){
        $planesTarjeta = PlanTarjeta::select('plan_tarjetas.*')
            ->join('tarjetas', 'tarjetas.id', '=', 'plan_tarjetas.tarjeta_id')
            ->where('tarjetas.empresa_id', $empresa->id)
            ->pluck('nombre', 'id')
            ->all();
        return ['' => 'Seleccione una opción...'] + $planesTarjeta;
    }

    public static function listarPlanesTarjetasPorEmpresa($empresa){
        $query = PlanTarjeta::select('plan_tarjetas.id','plan_tarjetas.nombre','plan_tarjetas.interes','plan_tarjetas.cuotas','tarjetas.nombre as nombretarjeta')
        ->join('tarjetas', 'tarjetas.id', '=', 'plan_tarjetas.tarjeta_id')
        ->where('tarjetas.empresa_id', '=', $empresa->id)
        ->whereNull('tarjetas.deleted_at');
        return $query->get();
    }

    public function __toString(){
        return (string) $this->tarjeta . " - " . $this->nombre;
    }
}
