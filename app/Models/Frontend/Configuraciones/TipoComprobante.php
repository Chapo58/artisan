<?php

namespace App\Models\Frontend\Configuraciones;

use Illuminate\Database\Eloquent\Model as Model;

class TipoComprobante extends Model{

    const FACTURA_A       = 1;
    const FACTURA_B       = 6;
    const FACTURA_C       = 11;
    const FACTURA_M       = 51;
    const TIQUE_FACTURA_A = 81;
    const TIQUE_FACTURA_B = 82;
    const TIQUE_FACTURA_C = 111;
    const TIQUE_FACTURA_M = 118;
    const NOTA_CREDITO_A  = 3;
    const NOTA_CREDITO_B  = 8;
    const NOTA_CREDITO_C  = 13;
    const NOTA_CREDITO_M  = 53;
    const TIQUE_NOTA_CREDITO_A = 112;
    const TIQUE_NOTA_CREDITO_B = 113;
    const TIQUE_NOTA_CREDITO_C = 114;
    const TIQUE_NOTA_CREDITO_M = 119;
    const NOTA_DEBITO_A  = 2;
    const NOTA_DEBITO_B  = 7;
    const NOTA_DEBITO_C  = 12;
    const NOTA_DEBITO_M  = 52;
    const TIQUE_NOTA_DEBITO_A = 115;
    const TIQUE_NOTA_DEBITO_B = 116;
    const TIQUE_NOTA_DEBITO_C = 117;
    const TIQUE_NOTA_DEBITO_M = 120;
    const PRESUPUESTO_A       = 16;
    const PRESUPUESTO_B       = 17;
    const PRESUPUESTO_C       = 18;
    const RECIBO_A       = 4;
    const RECIBO_B       = 9;
    const RECIBO_C       = 15;

    public static $comprobantes = [
        self::FACTURA_A     => 'Factura A',
        self::FACTURA_B     => 'Factura B',
        self::FACTURA_C     => 'Factura C',
        self::TIQUE_FACTURA_A     => 'Tique Factura A',
        self::TIQUE_FACTURA_B     => 'Tique Factura B',
        self::TIQUE_FACTURA_C     => 'Tique Factura C',
        self::NOTA_CREDITO_A    => 'Nota de Credito A',
        self::NOTA_CREDITO_B    => 'Nota de Credito B',
        self::NOTA_CREDITO_C    => 'Nota de Credito C',
        self::TIQUE_NOTA_CREDITO_A    => 'Tique Nota de Credito A',
        self::TIQUE_NOTA_CREDITO_B    => 'Tique Nota de Credito B',
        self::TIQUE_NOTA_CREDITO_C    => 'Tique Nota de Credito C',
        self::NOTA_DEBITO_A => 'Nota de Debito A',
        self::NOTA_DEBITO_B => 'Nota de Debito B',
        self::NOTA_DEBITO_C => 'Nota de Debito C',
        self::TIQUE_NOTA_DEBITO_A    => 'Tique Nota de Debito A',
        self::TIQUE_NOTA_DEBITO_B    => 'Tique Nota de Debito B',
        self::TIQUE_NOTA_DEBITO_C    => 'Tique Nota de Debito C',
        self::PRESUPUESTO_A     => 'Presupuesto A',
        self::PRESUPUESTO_B     => 'Presupuesto B',
        self::PRESUPUESTO_C     => 'Presupuesto C',
        self::RECIBO_A     => 'Recibo',
        self::RECIBO_B     => 'Recibo',
        self::RECIBO_C     => 'Recibo',
    ];

    public static $arrayComboFacturas = [
        ''                  => 'Tipo comprobante...',
        self::FACTURA_A     => 'Factura A',
        self::FACTURA_B     => 'Factura B',
        self::FACTURA_C     => 'Factura C',
        self::TIQUE_FACTURA_A     => 'Tique Factura A',
        self::TIQUE_FACTURA_B     => 'Tique Factura B',
        self::TIQUE_FACTURA_C     => 'Tique Factura C',
    ];

    public static $arrayComboNotasCredito = [
    ''                          => 'Tipo comprobante...',
        self::NOTA_CREDITO_A    => 'Nota de Credito A',
        self::NOTA_CREDITO_B    => 'Nota de Credito B',
        self::NOTA_CREDITO_C    => 'Nota de Credito C',
        self::TIQUE_NOTA_CREDITO_A    => 'Tique Nota de Credito A',
        self::TIQUE_NOTA_CREDITO_B    => 'Tique Nota de Credito B',
        self::TIQUE_NOTA_CREDITO_C    => 'Tique Nota de Credito C',
    ];

    public static $arrayComboNotasDebito = [
    ''                      => 'Tipo comprobante...',
        self::NOTA_DEBITO_A => 'Nota de Debito A',
        self::NOTA_DEBITO_B => 'Nota de Debito B',
        self::NOTA_DEBITO_C => 'Nota de Debito C',
        self::TIQUE_NOTA_DEBITO_A    => 'Tique Nota de Debito A',
        self::TIQUE_NOTA_DEBITO_B    => 'Tique Nota de Debito B',
        self::TIQUE_NOTA_DEBITO_C    => 'Tique Nota de Debito C',
    ];

    public static $arrayComboPresupuestos = [
        ''                      => 'Tipo comprobante...',
        self::PRESUPUESTO_A     => 'Presupuesto A',
        self::PRESUPUESTO_B     => 'Presupuesto B',
        self::PRESUPUESTO_C     => 'Presupuesto C',
    ];

    public static $arrayComboCompras = [
        ''                      => 'Tipo comprobante...',
        self::FACTURA_A     => 'Factura A',
        self::FACTURA_B     => 'Factura B',
        self::FACTURA_C     => 'Factura C',
        self::TIQUE_FACTURA_A     => 'Tique Factura A',
        self::TIQUE_FACTURA_B     => 'Tique Factura B',
        self::TIQUE_FACTURA_C     => 'Tique Factura C',
        self::NOTA_CREDITO_A    => 'Nota de Credito A',
        self::NOTA_CREDITO_B    => 'Nota de Credito B',
        self::NOTA_CREDITO_C    => 'Nota de Credito C',
        self::TIQUE_NOTA_CREDITO_A    => 'Tique Nota de Credito A',
        self::TIQUE_NOTA_CREDITO_B    => 'Tique Nota de Credito B',
        self::TIQUE_NOTA_CREDITO_C    => 'Tique Nota de Credito C',
    ];

    public static function getArrayComprobantes($empresa,$tipo){
        if($tipo == 'Factura'){
            if($empresa->condicion_iva_id == CondicionIva::RESPONSABLE_INSCRIPTO){
                return [
                    self::FACTURA_A     => 'Factura A',
                    self::FACTURA_B     => 'Factura B',
                    self::TIQUE_FACTURA_A     => 'Tique Factura A',
                    self::TIQUE_FACTURA_B     => 'Tique Factura B'
                ];
            } else {
                return [
                    self::FACTURA_C     => 'Factura C',
                    self::TIQUE_FACTURA_C     => 'Tique Factura C'
                ];
            }
        } else if($tipo == 'NotaCredito'){
            if($empresa->condicion_iva_id == CondicionIva::RESPONSABLE_INSCRIPTO) {
                return [
                    self::NOTA_CREDITO_A    => 'Nota de Credito A',
                    self::NOTA_CREDITO_B    => 'Nota de Credito B',
                    self::TIQUE_NOTA_CREDITO_A    => 'Tique Nota de Credito A',
                    self::TIQUE_NOTA_CREDITO_B    => 'Tique Nota de Credito B'
                ];
            } else {
                return [
                    self::NOTA_CREDITO_C    => 'Nota de Credito C',
                    self::TIQUE_NOTA_CREDITO_C    => 'Tique Nota de Credito C'
                ];
            }
        } else {
            if($empresa->condicion_iva_id == CondicionIva::RESPONSABLE_INSCRIPTO) {
                return [
                    self::NOTA_DEBITO_A => 'Nota de Debito A',
                    self::NOTA_DEBITO_B => 'Nota de Debito B',
                    self::TIQUE_NOTA_DEBITO_A    => 'Tique Nota de Debito A',
                    self::TIQUE_NOTA_DEBITO_B    => 'Tique Nota de Debito B'
                ];
            } else {
                return [
                    self::NOTA_DEBITO_C => 'Nota de Debito C',
                    self::TIQUE_NOTA_DEBITO_C    => 'Tique Nota de Debito C'
                ];
            }
        }
    }

}
