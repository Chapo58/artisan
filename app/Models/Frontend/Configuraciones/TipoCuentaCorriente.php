<?php

namespace App\Models\Frontend\Configuraciones;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Frontend\Ventas\CuentaCorriente;

class TipoCuentaCorriente extends Model{

    use SoftDeletes;

    protected $table = 'tipos_cuenta_corriente';

    protected $primaryKey = 'id';

    protected $fillable = ['nombre', 'interes', 'cuotas', 'dias', 'empresa_id'];

    protected $dates = ['deleted_at'];

    public function cuentaCorriente(){
      return $this->hasMany(CuentaCorriente::class);
    }

    public static function getTipos($empresa){
        $tipos = TipoCuentaCorriente::where('empresa_id', $empresa->id)
            ->whereNull('deleted_at')
            ->get();
        return $tipos;
    }

    public static function getTiposArray($empresa){
        $tipos = TipoCuentaCorriente::getTipos($empresa)->sortBy('nombre')->pluck('nombre', 'id')->all();
        return ['' => 'Seleccione un tipo...'] + $tipos;
    }

    public function __toString(){
        return (string) $this->nombre;
    }
}
