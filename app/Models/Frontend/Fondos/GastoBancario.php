<?php

namespace App\Models\Frontend\Fondos;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GastoBancario extends Model{

    use SoftDeletes;

    protected $table = 'gastos_bancarios';

    protected $primaryKey = 'id';

    protected $fillable = ['nombre', 'porcentaje', 'importe', 'empresa_id'];

    protected $dates = ['deleted_at'];



    public function __toString()
    {
        return (string) $this->nombre;
    }
}
