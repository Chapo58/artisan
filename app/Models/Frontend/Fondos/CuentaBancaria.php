<?php

namespace App\Models\Frontend\Fondos;

use App\Models\Frontend\Configuraciones\Moneda;
use App\Models\Frontend\Ventas\CuponTarjeta;
use App\Models\Frontend\Caja\Caja;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use Illuminate\Support\Facades\Auth;

class CuentaBancaria extends Model{

    use SoftDeletes;

    protected $table = 'cuentas_bancarias';

    protected $primaryKey = 'id';

    protected $fillable = ['banco_id', 'numero', 'tipo', 'moneda_id', 'cbu', 'cuit','saldo'];

    protected $dates = ['deleted_at'];

    public function banco(){
        return $this->belongsTo(Banco::class);
    }

    public function moneda(){
        return $this->belongsTo(Moneda::class);
    }

    public function chequeras(){
        return $this->hasMany(Chequera::class);
    }

    public function chequesPropios(){
        return $this->hasMany(ChequePropio::class);
    }

    public function cajas(){
        return $this->hasMany(Caja::class);
    }

    public function cuponesTarjetas(){
        return $this->hasMany(CuponTarjeta::class);
    }

    public static function getCuentasBancariasPorEmpresa($empresa){
        $cuentasBancarias = CuentaBancaria::select('cuentas_bancarias.*')
            ->join('bancos', 'bancos.id', '=', 'cuentas_bancarias.banco_id')
            ->where('bancos.empresa_id', $empresa->id)
            ->get();
      return $cuentasBancarias;
    }

    public static function getCuentasBancariasPorEmpresaArray($empresa){
        $cuentasBancarias = CuentaBancaria::select(DB::raw('CONCAT("Nº ", cuentas_bancarias.numero, " - Banco " , bancos.nombre) AS nombre_cuenta'), 'cuentas_bancarias.id')
            ->join('bancos', 'bancos.id', '=', 'cuentas_bancarias.banco_id')
            ->where('bancos.empresa_id', $empresa->id)
            ->pluck('nombre_cuenta', 'cuentas_bancarias.id')
            ->all();
        return ['' => 'Seleccione una cuenta...'] + $cuentasBancarias;
    }

    public function movimientosBancarios(){
        return $this->hasMany(MovimientoBancario::class);
    }

    public function __toString(){
        return (string) $this->banco . " - " . $this->numero;
    }
}
