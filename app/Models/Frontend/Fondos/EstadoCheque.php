<?php

namespace App\Models\Frontend\Fondos;

use Illuminate\Database\Eloquent\Model as Model;

class EstadoCheque extends Model{

    const PENDIENTE     = 0;
    const COBRADO       = 1;
    const DEPOSITADO    = 2;
    const ENTREGADO     = 3;

    public static $getArray = [
        self::PENDIENTE     => 'Pendiene',
        self::COBRADO       => 'Cobrado',
        self::DEPOSITADO    => 'Depositado',
        self::ENTREGADO     => 'Entregado',
    ];

    public function esPendiente(){
        return $this->estados == self::PENDIENTE;
    }

    public function esCobrado(){
        return $this->estados == self::COBRADO;
    }

    public function esDepositado(){
        return $this->estados == self::DEPOSITADO;
    }

    public function esEntregado(){
        return $this->estados == self::ENTREGADO;
    }

}
