<?php

namespace App\Models\Frontend\Fondos;

use App\Models\Backend\Empresas\Empresa;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Frontend\Caja\Caja;
use App\Models\Frontend\Caja\MovimientoCaja;
use Illuminate\Support\Facades\DB;

class Cheque extends Model{

    use SoftDeletes;

    protected $table = 'cheques';

    protected $primaryKey = 'id';

    protected $fillable = ['empresa_id', 'caja_id', 'banco_id', 'importe', 'numero', 'firmante', 'sucursal', 'estado'];

    protected $dates = ['deleted_at','emision','vencimiento','fecha_final'];

    public function empresa(){
        return $this->belongsTo(Empresa::class);
    }

    public function caja(){
        return $this->belongsTo(Caja::class);
    }

    public function banco(){
        return $this->belongsTo(Banco::class);
    }

    public function movimientos(){
        return $this->belongsToMany(MovimientoCaja::class);
    }

    public static function getChequesPorEmpresa($empresa, $estado = null){
        $query = Cheque::where('empresa_id', $empresa->id)
            ->whereNull('deleted_at')
            ->whereNull('deleted_at');
        if(isset($estado)){
            $query->where('estado', "=", $estado);
        }
        return $query->get();
    }

    public static function getChequesPorEmpresaArray($empresa, $estadoCheque = 0){
        $cheques = Cheque::select(DB::raw('CONCAT("Nº ",cheques.numero, " - Banco " , bancos.nombre, " - $", cheques.importe) AS cheque'), 'cheques.id')
            ->join('bancos', 'bancos.id', '=', 'cheques.banco_id')
            ->where('cheques.empresa_id', $empresa->id)
            ->where('cheques.estado', $estadoCheque)
            ->whereNull('cheques.deleted_at')
            ->pluck('cheque', 'cheques.id')
            ->all();
        return ['' => 'Seleccione un cheque...'] + $cheques;
    }

    public function __toString(){
        return (string) 'N°: '.$this->numero;
    }
}
