<?php

namespace App\Models\Frontend\Fondos;

use App\Models\Backend\Empresas\Empresa;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class ChequePropio extends Model{

    use SoftDeletes;

    protected $table = 'cheques_propios';

    protected $primaryKey = 'id';

    protected $fillable = ['importe', 'numero', 'chequera_id', 'cuenta_bancaria_id', 'empresa_id', 'estado', 'vencimiento', 'emision'];

    protected $dates = ['deleted_at', 'vencimiento', 'emision'];

    public function empresa(){
        return $this->belongsTo(Empresa::class);
    }

    public function cuentaBancaria(){
      return $this->belongsTo(CuentaBancaria::class);
    }

    public function chequera(){
        return $this->belongsTo(Chequera::class);
    }

    public static function listarChequesPorEmpresa($empresa){
        $query = ChequePropio::select('cheques_propios.id','cheques_propios.numero','cheques_propios.importe','cheques_propios.emision','cheques_propios.vencimiento','cheques_propios.estado',DB::raw("CONCAT(bancos.nombre,' - ',cuentas_bancarias.numero) AS cuentaBancaria"));
        $query->leftJoin('cuentas_bancarias','cuentas_bancarias.id','=', 'cheques_propios.cuenta_bancaria_id');
        $query->leftJoin('bancos','bancos.id','=', 'cuentas_bancarias.banco_id');
        $query->where('cheques_propios.empresa_id', $empresa->id);
        $query->whereNull('cheques_propios.deleted_at');
        $query->orderBy('cheques_propios.vencimiento','DESC');
        return $query->get();
    }

    public function __toString(){
        return (string) 'N°: '.$this->numero;
    }
}
