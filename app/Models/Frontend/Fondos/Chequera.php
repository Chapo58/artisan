<?php

namespace App\Models\Frontend\Fondos;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Chequera extends Model
{

    use SoftDeletes;

    protected $table = 'chequeras';

    protected $primaryKey = 'id';

    protected $fillable = ['cuenta_bancaria_id', 'desde', 'hasta'];

    protected $dates = ['deleted_at'];

    public function cuentaBancaria(){
      return $this->belongsTo(CuentaBancaria::class);
    }

    public static function getChequerasPorEmpresa($empresa){
        $chequeras = Chequera::select('chequeras.*')
            ->join('cuentas_bancarias', 'cuentas_bancarias.id', '=', 'chequeras.cuenta_bancaria_id')
            ->join('bancos', 'bancos.id', '=', 'cuentas_bancarias.banco_id')
            ->where('bancos.empresa_id', $empresa->id)
            ->whereNull('chequeras.deleted_at')
            ->get();
        return $chequeras;
    }

    public static function getChequerasPorEmpresaArray($empresa){
        $chequeras = Chequera::select(DB::raw('CONCAT("(",chequeras.desde,",",chequeras.hasta,") Nº ", cuentas_bancarias.numero, " - Banco " , bancos.nombre) AS nombre_chequera'), 'chequeras.id')
            ->join('cuentas_bancarias', 'cuentas_bancarias.id', '=', 'chequeras.cuenta_bancaria_id')
            ->join('bancos', 'bancos.id', '=', 'cuentas_bancarias.banco_id')
            ->where('bancos.empresa_id', $empresa->id)
            ->whereNull('chequeras.deleted_at')
            ->pluck('nombre_chequera', 'chequeras.id')
            ->all();
        return ['' => 'Seleccione una chequera...'] + $chequeras;
    }

    public function __toString(){
        return (string) $this->cuentaBancaria;
    }

}
