<?php

namespace App\Models\Frontend\Fondos;

use App\Models\Backend\Empresas\Empresa;
use App\Models\Frontend\Configuraciones\Moneda;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MovimientoBancario extends Model
{

    use SoftDeletes;

    protected $table = 'movimientos_bancarios';

    protected $primaryKey = 'id';

    protected $fillable = ['importe', 'fecha', 'numero', 'cuenta_bancaria_origen_id', 'cuenta_bancaria_destino_id', 'entidad_id', 'empresa_id', 'entidad_clase', 'url', 'moneda_id', 'tipo', 'comentario'];

    protected $dates = ['deleted_at','fecha'];

    public function cuentaBancariaOrigen(){
      return $this->belongsTo(CuentaBancaria::class, 'cuenta_bancaria_origen_id');
    }

    public function cuentaBancariaDestino(){
        return $this->belongsTo(CuentaBancaria::class, 'cuenta_bancaria_destino_id');
    }

    public function moneda(){
      return $this->belongsTo(Moneda::class);
    }

    public function empresa(){
        return $this->belongsTo(Empresa::class);
    }

    public static function getMovimientosBancariosPorEmpresa($empresa){
        $movimientosBancarios = MovimientoBancario::where('empresa_id', $empresa->id)
            ->whereNull('deleted_at')
            ->get();
        return $movimientosBancarios;
    }

    public static function getMovimientosBancariosPorEmpresaArray($empresa){
        $movimientosBancarios = self::getMovimientosBancariosPorEmpresa($empresa)->pluck('numero', 'id')->all();
        return ['' => 'Seleccione un movimiento...'] + $movimientosBancarios;
    }

    public static function listarMovimientosBancariosPorEmpresa($empresa){
        $query = MovimientoBancario::where('empresa_id', $empresa->id);
        $query->whereNull('deleted_at');
        $query->orderBy('id','DESC');
        return $query->get();
    }

    public function __toString(){
        return (string) $this->cuentaBancariaDestino;
    }
}
