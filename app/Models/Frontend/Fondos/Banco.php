<?php

namespace App\Models\Frontend\Fondos;

use App\Models\Backend\Empresas\Empresa;
use App\Models\Frontend\Compras\CarteraCheque;
use App\Models\Frontend\Configuraciones\TarjetaPropia;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Banco extends Model
{

    use SoftDeletes;

    protected $table = 'bancos';

    protected $primaryKey = 'id';

    protected $fillable = ['nombre', 'contacto', 'telefono', 'empresa_id'];

    protected $dates = ['deleted_at'];

    public function cuentasBancarias(){
      return $this->hasMany(CuentaBancaria::class);
    }

    public function cheques(){
      return $this->hasMany(Cheque::class);
    }

    public function tarjetasPropias(){
      return $this->hasMany(TarjetaPropia::class);
    }

    public function empresa(){
        return $this->belongsTo(Empresa::class);
    }

    public static function getBancosPorEmpresa($empresa){
        $bancos = Banco::where('empresa_id', $empresa->id)
            ->whereNull('deleted_at')
            ->get();
        return $bancos;
    }

    public static function getBancosPorEmpresaArray($empresa){
        $bancos = self::getBancosPorEmpresa($empresa)->pluck('nombre', 'id')->all();
        return $bancos;
    }

    public function __toString(){
        return (string) $this->nombre;
    }
}
