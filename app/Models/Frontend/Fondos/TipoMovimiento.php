<?php

namespace App\Models\Frontend\Fondos;

use Illuminate\Database\Eloquent\Model as Model;

class TipoMovimiento extends Model{

    const DEPOSITO      = 0;
    const EXTRACCION    = 1;
    const TRANSFERENCIA = 2;

    public static $tipos = [
        self::DEPOSITO      => 'Deposito',
        self::EXTRACCION    => 'Extraccion',
        self::TRANSFERENCIA => 'Transferencia',
    ];

}
