<?php

namespace App\Models\Frontend\Pedidos;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transporte extends Model
{

    use SoftDeletes;

    protected $table = 'transportes';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    protected $fillable = ['codigo_interno', 'nombre', 'direccion', 'codigo_postal', 'telefono', 'email', 'observaciones', 'empresa_id'];

    protected $dates = ['deleted_at'];

    

    public function __toString()
    {
        return (string) $this->nombre;
    }
}
