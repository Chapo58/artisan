<?php

namespace App\Models\Frontend\Ventas;

use App\Models\Frontend\Configuraciones\Estado;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Jenssegers\Date\Date;

class DetalleCobroVenta extends Model{

    use SoftDeletes;

    protected $table = 'detalles_cobro_ventas';

    protected $primaryKey = 'id';

    protected $fillable = ['venta_id', 'entidad_id', 'forma_cobro' , 'monto', 'interes', 'descripcion', 'estado'];

    protected $dates = ['deleted_at', 'fecha'];

    public function venta(){
        return $this->belongsTo(Venta::class);
    }

    public static function nuevo($venta, $monto, $descripcion, $formaDePago, $entidad_id = null){
        $nuevoCobro = new DetalleCobroVenta();
        $nuevoCobro->fecha = new date();
        $nuevoCobro->venta_id = $venta->id;
        $nuevoCobro->monto = $monto;
        $nuevoCobro->estado = Estado::ACTIVO;
        $nuevoCobro->descripcion = $descripcion;
        $nuevoCobro->forma_cobro = $formaDePago;
        $nuevoCobro->entidad_id = $entidad_id;
        $nuevoCobro->save();
    }

    public static function actualizar($detalle, $monto, $entidad_id = null){
        $detalle->fecha = new date();
        $detalle->monto = $monto;
        $detalle->entidad_id = $entidad_id;
        $detalle->update();
    }

    public function __toString(){
        return $this->descripcion;
    }
}
