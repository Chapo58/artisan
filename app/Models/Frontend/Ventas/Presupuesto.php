<?php

namespace App\Models\Frontend\Ventas;

use App\Models\Backend\Empresas\Empresa;
use App\Models\Backend\Empresas\Sucursal;
use App\Models\Frontend\Configuraciones\Moneda;
use App\Models\Frontend\Personas\Cliente;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Presupuesto extends Model{

    use SoftDeletes;

    protected $table = 'presupuestos';

    protected $primaryKey = 'id';

    protected $fillable = ['empresa_id', 'sucursal_id', 'cliente_id', 'moneda_id', 'lista_precios_id', 'tipo_comprobante_id', 'cotizacion', 'numero', 'importe_neto', 'importe_iva', 'importe_descuento', 'porcentaje_descuento', 'importe_impuesto_interno', 'total', 'estado', 'descripcion'];

    protected $dates = ['fecha', 'fecha_vencimiento', 'deleted_at'];

    public function empresa(){
        return $this->belongsTo(Empresa::class);
    }

    public function sucursal(){
        return $this->belongsTo(Sucursal::class);
    }

    public function cliente(){
        return $this->belongsTo(Cliente::class);
    }

    public function moneda(){
        return $this->belongsTo(Moneda::class);
    }

    public function detalles(){
        return $this->hasMany(DetallePresupuesto::class);
    }

    public function listaPrecios(){
        return $this->belongsTo(ListaPrecio::class, 'lista_precios_id');
    }

    public static function listarPresupuestosPorEmpresaSucursal($empresa, $sucursal = null){
        $query = Presupuesto::where('presupuestos.empresa_id', $empresa->id);
        $query->join('clientes', 'clientes.id', '=', 'presupuestos.cliente_id');
        $query->join('monedas', 'monedas.id', '=', 'presupuestos.moneda_id');
        $query->join('lista_precios', 'lista_precios.id', '=', 'presupuestos.lista_precios_id');
        $query->select('presupuestos.id', 'presupuestos.fecha', 'presupuestos.numero', 'presupuestos.tipo_comprobante_id','presupuestos.total', 'clientes.razon_social', 'monedas.signo', 'lista_precios.nombre AS lista');
        $query->whereNull('presupuestos.deleted_at');
        $query->orderBy('presupuestos.fecha','DESC');
        if (Auth::user()->hasRole('Usuario Vendedor')) {
            $query->where('presupuestos.sucursal_id', $sucursal->id);
        }
        return $query->get();
    }

    public function __toString(){
        return (string) $this->numero .' - '. $this->cliente;
    }
}
