<?php

namespace App\Models\Frontend\Ventas;

use App\Models\Frontend\Articulos\Articulo;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DetalleVencimientoFacturaAutomatica extends Model{

    protected $table = 'detalles_vencimientos_fa';

    protected $primaryKey = 'id';

    protected $fillable = ['factura_automatica_id', 'desde', 'hasta', 'porcentaje_recargo'];

    public function facturaAutomatica(){
        return $this->belongsTo(FacturaVentaAutomatica::class, 'factura_automatica_id');
    }

    public function montoRecargo(){
        return round($this->facturaAutomatica->venta->total * $this->porcentaje_recargo / 100, 2);
    }

    public function __toString(){
        return $this->porcentaje_recargo;
    }
}
