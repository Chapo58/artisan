<?php

namespace App\Models\Frontend\Ventas;

use Illuminate\Database\Eloquent\Model as Model;

class EstadoFacturacion extends Model{

    const FACTURADO = 1;
    const ERROR     = 2;
    const ANULADO   = 3;
    const PENDIENTE = 4;

    public static $getArray = [
        self::PENDIENTE => 'Pendiene',
        self::FACTURADO => 'Facturado',
        self::ERROR     => 'Error',
        self::ANULADO   => 'Anulado',
    ];

}
