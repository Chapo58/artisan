<?php

namespace App\Models\Frontend\Ventas;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ListaPrecio extends Model
{

    use SoftDeletes;

    protected $table = 'lista_precios';

    protected $primaryKey = 'id';

    protected $fillable = ['empresa_id', 'nombre', 'descripcion', 'estado', 'porcentaje'];

    protected $dates = ['deleted_at'];

    public function precios(){
        return $this->hasMany(Precio::class);
    }

    public static function getListasPorEmpresa($empresa){
        $listas = ListaPrecio::where('empresa_id', $empresa->id)->get();
        return $listas;
    }

    public static function getListasPorEmpresaArray($empresa){
        $listas = self::getListasPorEmpresa($empresa)->pluck('nombre', 'id')->all();
        return $listas;
    }

    public function __toString(){
        return $this->nombre;
    }

}
