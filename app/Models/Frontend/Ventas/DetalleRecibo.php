<?php

namespace App\Models\Frontend\Ventas;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DetalleRecibo extends Model{

    use SoftDeletes;

    protected $table = 'detalles_recibos';

    protected $primaryKey = 'id';

    protected $fillable = ['descripcion', 'monto', 'recibo_id', 'cuenta_corriente_id', 'nota_debito_id'];

    protected $dates = ['deleted_at'];

    public function recibo(){
        return $this->belongsTo(Recibo::class);
    }

    public function cuentaCorriente(){
        return $this->belongsTo(CuentaCorriente::class, 'cuenta_corriente_id');
    }

    public function notaDebito(){
//        return $this->belongsTo(CuentaCorriente::class);
    }

    public function __toString(){
        return (string) $this->id;
    }
}
