<?php

namespace App\Models\Frontend\Ventas;

use App\Models\Backend\Empresas\Empresa;
use App\Models\Backend\Empresas\Sucursal;
use App\Models\Frontend\Configuraciones\Moneda;
use App\Models\Frontend\Configuraciones\PorcentajeIva;
use App\Models\Frontend\Configuraciones\PuntoDeVenta;
use App\Models\Frontend\Personas\Cliente;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use DB;

class NotaCredito extends Model{

    use SoftDeletes;

    protected $table = 'notas_credito';

    protected $primaryKey = 'id';

    protected $fillable = ['numero', 'importe_neto', 'importe_iva', 'importe_descuento', 'porcentaje_descuento', 'importe_impuesto_interno', 'total',
        'total_pago', 'cotizacion', 'estado', 'descripcion', 'empresa_id', 'cliente_id', 'moneda_id',
        'punto_venta_id', 'tipo_comprobante_id', 'lista_precios_id', 'sucursal_id', 'venta_id',
        'concepto_nota_credito', 'cae'];

    protected $dates = ['deleted_at', 'fecha', 'fecha_vencimiento'];

    public function empresa(){
        return $this->belongsTo(Empresa::class);
    }

    public function sucursal(){
        return $this->belongsTo(Sucursal::class);
    }

    public function venta(){
        return $this->belongsTo(Venta::class);
    }

    public function cliente(){
        return $this->belongsTo(Cliente::class);
    }

    public function moneda(){
        return $this->belongsTo(Moneda::class);
    }

    public function detalles(){
        return $this->hasMany(DetalleNotaCredito::class);
    }

    public function detallesPagos(){
        return $this->hasMany(DetallePagoNotaCredito::class);
    }

    public function puntoVenta(){
        return $this->belongsTo(PuntoDeVenta::class);
    }

    public function listaPrecios(){
        return $this->belongsTo(ListaPrecio::class, 'lista_precios_id');
    }

    public static function listarNotasDeCreditoPorEmpresaSucursal($empresa, $sucursal = null){
        $query = NotaCredito::join('clientes', 'clientes.id', '=', 'notas_credito.cliente_id');
        $query->join('puntos_de_ventas', 'puntos_de_ventas.id', '=', 'notas_credito.punto_venta_id');
        $query->join('monedas', 'monedas.id', '=', 'notas_credito.moneda_id');
        $query->join('lista_precios', 'lista_precios.id', '=', 'notas_credito.lista_precios_id');
        $query->select('notas_credito.id','notas_credito.fecha',DB::raw("CONCAT(puntos_de_ventas.numero,'-',notas_credito.numero) AS numero"), 'clientes.razon_social', 'notas_credito.tipo_comprobante_id', 'notas_credito.estado_facturacion', 'notas_credito.total_pago', 'puntos_de_ventas.tipo_punto_id', 'monedas.signo', 'lista_precios.nombre as lista');
        $query->where('notas_credito.empresa_id', $empresa->id);
        $query->whereNull('notas_credito.deleted_at');
        $query->orderBy('notas_credito.fecha', 'DESC');
        if (Auth::user()->hasRole('Usuario Vendedor')) {
            $query->where('notas_credito.sucursal_id', $sucursal->id);
        }
        return $notasCredito = $query->get();
    }

    public static function obtenerNotasCreditoPeriodo($periodo,$puntosDeVenta){
        $empresa = Auth::user()->empresa;
        $year = substr($periodo, -4);
        $month = substr($periodo, 0, 2);

        $query = NotaCredito::where('empresa_id', $empresa->id);
        $query->whereYear('fecha', '=', $year);
        $query->whereMonth('fecha', '=', $month);
        $query->where('estado_facturacion','<>', EstadoFacturacion::ERROR);
        $query->where('estado_facturacion','<>', EstadoFacturacion::PENDIENTE);
        $query->where('punto_venta_id', $puntosDeVenta->get(0)->id);
        $query->whereNull('deleted_at');
        $query->where(function($q) use ($puntosDeVenta) {
            foreach ($puntosDeVenta as $key => $p){
                if($key == 0){
                    $q->Where('punto_venta_id', $p->id);
                } else {
                    $q->orWhere('punto_venta_id', $p->id);
                }
            }
        });
        $query->whereNull('deleted_at');
        $query->orderBy('fecha', 'asc');
        $notasCredito = $query->get();

        return $notasCredito;
    }

    public function interesesPago(){
        $interes = 0;
        $detallesPagos = DetallePagoNotaCredito::where('nota_credito_id',$this->id)->get();
        foreach($detallesPagos as $pago){
            $interes += $pago->interes;
        }
        return $interes;
    }

    public function __toString(){
        return $this->puntoVenta.'-'.$this->numero;
    }
}