<?php

namespace App\Models\Frontend\Ventas;

use App\Models\Frontend\Articulos\Articulo;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Precio extends Model
{

    use SoftDeletes;

    protected $table = 'precios';

    protected $primaryKey = 'id';

    protected $fillable = ['precio', 'numero', 'articulo_id', 'lista_precio_id'];

    protected $dates = ['deleted_at', 'fecha_hasta'];

    public function articulo(){
        return $this->belongsTo(Articulo::class);
    }

    public function listaPrecio(){
        return $this->belongsTo(ListaPrecio::class, 'lista_precio_id');
    }

    public function __toString(){
        return $this->articulo->nombre. ' ($ '. $this->precio.')';
    }

    public static function listarPreciosPorEmpresaSucursalListaEnStock($empresa, $sucursal = null, $lista_precios_id, $enStock = true, $perPage = 25, $keyword = null){
        $query = Precio::join('articulos', 'articulos.id', '=', 'precios.articulo_id')
            ->join('lista_precios', 'lista_precios.id', '=', 'precios.lista_precio_id')
            ->leftjoin('monedas', 'monedas.id', '=', 'articulos.moneda_id')
            ->leftjoin('marcas', 'marcas.id', '=', 'articulos.marca_id')
            ->leftjoin('stocks', 'stocks.articulo_id', '=', 'articulos.id')
            ->select('precios.*', 'articulos.*', 'stocks.id as stock', 'stocks.cantidad as existencia', 'stocks.deleted_at as stock_borrado', 'monedas.cotizacion', 'lista_precios.porcentaje as porcentaje_lista', 'marcas.nombre as marca')
            ->where('articulos.empresa_id', $empresa->id)
            ->where('precios.lista_precio_id', '=', $lista_precios_id)
            ->whereNull('precios.deleted_at')
            ->whereNull('articulos.deleted_at');
        if($enStock == true){
            $query->where(function($q) use ($sucursal){
                $q->orWhereNull('stocks.id')
                    ->orWhereNotNull('stocks.deleted_at')
                    ->orWhere('articulos.lleva_stock','=',0)
                    ->orWhere(function($r) use ($sucursal){
                        $r->where('stocks.cantidad', '>', 0)
                          ->where('stocks.sucursal_id', $sucursal->id);
                });
            });
        }
        if(isset($keyword)){
            $query->where(function($q) use ($keyword) {
                $q->orWhere(DB::raw('CONCAT_WS(" ", articulos.nombre, marcas.nombre, articulos.modelo)'), 'LIKE', "%$keyword%");
                $q->orWhere(DB::raw('CONCAT_WS(" ", articulos.nombre, marcas.nombre, articulos.modelo_secundario)'), 'LIKE', "%$keyword%");
                $q->orWhere(DB::raw('CONCAT_WS(" ", articulos.nombre, articulos.modelo, articulos.modelo_secundario)'), 'LIKE', "%$keyword%");
                $q->orWhere(DB::raw('CONCAT_WS(" ", articulos.nombre, articulos.modelo_secundario, articulos.modelo)'), 'LIKE', "%$keyword%");
                $q->orWhere(DB::raw('CONCAT_WS(" ", marcas.nombre, articulos.nombre, articulos.modelo)'), 'LIKE', "%$keyword%");
                $q->orWhere(DB::raw('CONCAT_WS(" ", marcas.nombre, articulos.modelo, articulos.modelo_secundario)'), 'LIKE', "%$keyword%");
                $q->orWhere(DB::raw('CONCAT_WS(" ", marcas.nombre, articulos.modelo_secundario, articulos.modelo)'), 'LIKE', "%$keyword%");
                $q->orWhere(DB::raw('CONCAT_WS(" ", articulos.codigo, articulos.modelo, articulos.modelo_secundario)'), 'LIKE', "%$keyword%");
                $q->orWhere(DB::raw('CONCAT_WS(" ", articulos.codigo, articulos.modelo_secundario, articulos.modelo)'), 'LIKE', "%$keyword%");
                if(strlen($keyword) >= 4){
                    $q->orWhere('articulos.codigo_barra', 'LIKE', "%$keyword%");
                }
            });
        }
        return $query->paginate($perPage);
    }

    public static function buscarPreciosPorArticulo($empresa, $articulo){
        $query = Precio::join('articulos', 'articulos.id', '=', 'precios.articulo_id')
            ->select('precios.*')
            ->where('articulos.empresa_id', $empresa->id)
            ->where('articulos.id', '=', $articulo->id)
            ->whereNull('precios.deleted_at')
            ->whereNull('articulos.deleted_at');
        return $query->get();
    }
}
