<?php

namespace App\Models\Frontend\Ventas;

use App\Models\Frontend\Articulos\Articulo;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DetalleNotaCredito extends Model{

    use SoftDeletes;

    protected $table = 'detalles_notas_credito';

    protected $primaryKey = 'id';

    protected $fillable = ['nota_credito_id', 'articulo_id', 'precio_neto', 'precio_iva', 'porcentaje_iva', 'importe_impuesto_interno', 'cantidad',
        'descripcion', 'estado'];

    protected $dates = ['deleted_at'];

    public function notaCredito(){
        return $this->belongsTo(NotaCredito::class, 'nota_credito_id');
    }

    public function articulo(){
        return $this->belongsTo(Articulo::class);
    }

    public function __toString(){
        return $this->precio. " - ". $this->articulo->nombre;
    }
}
