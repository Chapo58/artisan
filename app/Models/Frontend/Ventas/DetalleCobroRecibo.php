<?php

namespace App\Models\Frontend\Ventas;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DetalleCobroRecibo extends Model{

    use SoftDeletes;

    protected $table = 'detalles_cobro_recibos';

    protected $primaryKey = 'id';

    protected $fillable = ['recibo_id', 'entidad_id', 'forma_cobro', 'monto', 'interes', 'descripcion', 'estado'];

    protected $dates = ['deleted_at'];

    public function recibo(){
        return $this->belongsTo(Recibo::class);
    }

    public function __toString(){
        return (string) $this->id;
    }
}
