<?php

namespace App\Models\Frontend\Ventas;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DetalleCobroNotaDebito extends Model{

    use SoftDeletes;

    protected $table = 'detalles_cobros_notas_debito';

    protected $primaryKey = 'id';

    protected $fillable = ['nota_debito_id', 'entidad_id', 'forma_cobro' , 'monto', 'interes', 'descripcion', 'estado'];

    protected $dates = ['deleted_at', 'fecha'];

    public function notaDebito(){
        return $this->belongsTo(NotaDebito::class);
    }

    public function __toString(){
        return $this->descripcion;
    }
}
