<?php

namespace App\Models\Frontend\Ventas;

use App\Http\Controllers\Frontend\Caja\MovimientoCajaController;
use App\Models\Backend\Empresas\Empresa;
use App\Models\Backend\Empresas\Sucursal;
use App\Models\Frontend\Caja\ConceptoDeCaja;
use App\Models\Frontend\Caja\FormaDePago;
use App\Models\Frontend\Configuraciones\Estado;
use App\Models\Frontend\Configuraciones\Opcion;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class FacturaVentaAutomatica extends Model{

    use SoftDeletes;

    protected $table = 'facturas_ventas_automaticas';

    protected $primaryKey = 'id';

    protected $fillable = ['empresa_id', 'venta_id', 'fecha_inicio', 'fecha_fin', 'dia_facturacion', 'estado'];

    protected $dates = ['deleted_at', 'fecha_inicio', 'fecha_fin'];

    public function empresa(){
        return $this->belongsTo(Empresa::class);
    }

    public function venta(){
        return $this->belongsTo(Venta::class);
    }

    public function intervalos(){
        return $this->hasMany(DetalleVencimientoFacturaAutomatica::class, 'factura_automatica_id');
    }

    public function ventasGeneradas(){
        return $this->hasMany(Venta::class, 'factura_automatica_id');
    }

    public function totalIntervalo($intervalo_id){
        if($intervalo_id == 0){
            return $this->venta->total;
        }else{
            $total = $this->venta->total;
            foreach ($this->intervalos->sortBy('desde') as $intervalo){
                $total = $total + $total*$intervalo->porcentaje_recargo/100;
                if($intervalo->id == $intervalo_id){
                    break;
                }
            }
            return round($total, 2);
        }
    }

    public static function getFacturasAutomaticas($empresa){
        if(isset($empresa)){
            $objetos = FacturaVentaAutomatica::where('empresa_id', $empresa->id)->get();
        }else{
            $objetos = FacturaVentaAutomatica::get();
        }

        return $objetos;
    }

    public static function listarFacturasAutomaticas($empresa, $perPage = 25, $keyword = null){
        $query = Venta::join('clientes', 'clientes.id', '=', 'ventas.cliente_id');
        $query->join('puntos_de_ventas', 'puntos_de_ventas.id', '=', 'ventas.punto_venta_id');
        $query->join('monedas', 'monedas.id', '=', 'ventas.moneda_id');
        $query->join('lista_precios', 'lista_precios.id', '=', 'ventas.lista_precios_id');
        $query->select('ventas.*', 'clientes.razon_social', 'puntos_de_ventas.numero as punto_venta', 'monedas.signo', 'lista_precios.nombre as lista');
        $query->where('ventas.empresa_id', $empresa->id);
        $query->whereNull('ventas.deleted_at');
        $query->where('ventas.estado','<>', Estado::PLANTILLA);
        $query->where('ventas.estado','<>', Estado::FACTURA_AUTOMATICA);
        $query->orderBy('ventas.fecha','DESC');
        if (Auth::user()->hasRole('Usuario Vendedor')) {
            $query->where('ventas.sucursal_id', $sucursal->id);
        }
        $query->where(function ($q) use ($keyword) {
            $q->where('ventas.numero', 'LIKE', "%$keyword%")
                ->orWhere('puntos_de_ventas.numero', 'LIKE', "%$keyword%")
                ->orWhere('clientes.razon_social', 'LIKE', "%$keyword%");
        });
        return $query->paginate($perPage);
    }

    public static function generarFacturaSegunTemplate($facturaTemplate){
        $faltanteStock = false;

        $empresa = $facturaTemplate->empresa;
        $sucursal = Sucursal::findOrFail($facturaTemplate->venta->sucursal_id);

        $fecha_hoy = Carbon::now();

        $nombresClaves = [
            0 => 'numeracion_factura_a',
            1 => 'numeracion_factura_b',
            2 => 'numeracion_factura_c'
        ];

        $nuevaFacturaAutomatica = New Venta();
        $nuevaFacturaAutomatica->numero = $facturaTemplate->venta->numero;
        if($facturaTemplate->venta->puntoVenta->tipo->id == 1){//si es Fiscal busco la numeración del punto de venta
            $opcion = Opcion::where('empresa_id', $empresa->id)
                ->where('nombre', 'LIKE', $nombresClaves[$facturaTemplate->venta->tipo_comprobante_id])
                ->where('punto_venta_id', $facturaTemplate->venta->puntoVenta->id)
                ->first();
            $numero_factura = $opcion->valor;
            $opcion->valor = str_pad($opcion->valor + 1, 8, '0', STR_PAD_LEFT);
            $opcion->save();
        }else{//si es Manual busco la numeración genérica
            $opcion = Opcion::where('empresa_id', $empresa->id)
                ->where('nombre', 'LIKE', $nombresClaves[$facturaTemplate->venta->tipo_comprobante_id])
                ->first();
            $numero_factura = $opcion->valor;
            $opcion->valor = str_pad($opcion->valor + 1, 8, '0', STR_PAD_LEFT);
            $opcion->save();
        }
        $nuevaFacturaAutomatica->numero = $numero_factura;

        $nuevaFacturaAutomatica->fecha = Carbon::create($fecha_hoy->year, $fecha_hoy->month, $facturaTemplate->dia_facturacion, 8, 0, 0);
        $nuevaFacturaAutomatica->empresa_id = $facturaTemplate->venta->empresa_id;
        $nuevaFacturaAutomatica->sucursal_id = $facturaTemplate->venta->sucursal_id;
        $nuevaFacturaAutomatica->cliente_id = $facturaTemplate->venta->cliente_id;
        $nuevaFacturaAutomatica->moneda_id = $facturaTemplate->venta->moneda_id;
        $nuevaFacturaAutomatica->cotizacion = $facturaTemplate->venta->cotizacion;
        $nuevaFacturaAutomatica->tipo_comprobante_id = $facturaTemplate->venta->tipo_comprobante_id;
        $nuevaFacturaAutomatica->punto_venta_id = $facturaTemplate->venta->punto_venta_id;
        $nuevaFacturaAutomatica->lista_precios_id = $facturaTemplate->venta->lista_precios_id;
        $nuevaFacturaAutomatica->importe_neto = $facturaTemplate->venta->importe_neto;
        $nuevaFacturaAutomatica->importe_iva = $facturaTemplate->venta->importe_iva;
        $nuevaFacturaAutomatica->impuesto = $facturaTemplate->venta->impuesto;
        $nuevaFacturaAutomatica->importe_impuesto_interno = $facturaTemplate->venta->importe_impuesto_interno;
        $nuevaFacturaAutomatica->total = $facturaTemplate->venta->total;
        $nuevaFacturaAutomatica->total_cobro = $facturaTemplate->venta->total_cobro;
        $nuevaFacturaAutomatica->descripcion = $facturaTemplate->venta->descripcion;
        $nuevaFacturaAutomatica->estado = Estado::FACTURA_AUTOMATICA_PENDIENTE;
        $nuevaFacturaAutomatica->estado_facturacion = EstadoFacturacion::PENDIENTE;
        $nuevaFacturaAutomatica->factura_automatica_id = $facturaTemplate->id;
        $nuevaFacturaAutomatica->save();

        foreach ($facturaTemplate->venta->detalles as $detalle){
            $nuevoDetalle = New DetalleVenta();
            $nuevoDetalle->articulo_id = $detalle->articulo_id;
            $nuevoDetalle->cantidad = $detalle->cantidad;
            $nuevoDetalle->precio = $detalle->precio;
            $nuevoDetalle->porcentaje_iva = $detalle->porcentaje_iva;
            $nuevoDetalle->impuesto_interno = $detalle->impuesto_interno;
            $nuevoDetalle->estado = Estado::ACTIVO;
            $nuevoDetalle->venta_id = $nuevaFacturaAutomatica->id;
            $nuevoDetalle->save();

            $stockArticulo = $nuevoDetalle->articulo->stockPorSucursal($sucursal);
            if(isset($stockArticulo)){
                if($stockArticulo->cantidad >= $nuevoDetalle->cantidad){
                    $stockArticulo->cantidad -= $nuevoDetalle->cantidad;
                    $stockArticulo->save();
                }else{
                    $faltanteStock = true;
                }
            }
        }

        if($faltanteStock == true){
            $nuevaFacturaAutomatica->estado = Estado::FACTURA_AUTOMATICA_PENDIENTE_FALTANTE_STOCK;
            $nuevaFacturaAutomatica->update();
        }

        DetalleCobroVenta::nuevo($nuevaFacturaAutomatica, $nuevaFacturaAutomatica->total, 'A Cuenta Corriente', FormaDePago::CUENTA_CORRIENTE);

        CuentaCorriente::nuevaDeuda($nuevaFacturaAutomatica, '/ventas/venta', $nuevaFacturaAutomatica->total);

        return $nuevaFacturaAutomatica;
    }

    public static function actualizarFacturaVencimientos($facturaTemplate, $factura){
        $fecha_hoy = Carbon::now();

        $actualizada = false;

        if($factura->fecha < $fecha_hoy){
            $diasRetraso = $factura->fecha->diffInDays($fecha_hoy);
            foreach ($facturaTemplate->intervalos->sortBy('desde') as $intervalo){
                if($diasRetraso > $intervalo->desde && $diasRetraso < $intervalo->hasta){
                    if($factura->impuesto < $facturaTemplate->totalIntervalo($intervalo->id) - $factura->total){
                        $factura->impuesto = $facturaTemplate->totalIntervalo($intervalo->id) - $factura->total;
                        $montoAnterior = $factura->total;
                        $factura->total = $factura->total + $factura->impuesto;
                        $factura->total_cobro = $factura->total;
                        $factura->update();

                        $actualizada = true;

                        DetalleCobroVenta::actualizar($factura->detallesCobros->first(), $factura->total);

                        CuentaCorriente::actualizarDeuda($factura, $factura->total, $montoAnterior);
                    }
                }
            }
        }

        return $actualizada;
    }

    public static function facturaMensualGenerada($facturaTemplate){
        $fecha_hoy = Carbon::now();

        $facturaGenerada = false;

        if(isset($facturaTemplate->ventasGeneradas)){
            foreach ($facturaTemplate->ventasGeneradas as $factura){
                if($factura->fecha->year == $fecha_hoy->year && $factura->fecha->month == $fecha_hoy->month){
                    $facturaGenerada = true;
                }
            }
        }

        return $facturaGenerada;
    }

    public static function esFacturaAutomatica($entidad_id, $url){
        if($url == '/ventas/venta'){
            $venta = Venta::findOrFail($entidad_id);
            if($venta->estado == Estado::FACTURA_AUTOMATICA_PENDIENTE){
                return true;
            }else{
                return false;
            }
        }
        return false;
    }

    public static function cancelar($entidad_id){
        $venta = Venta::findOrFail($entidad_id);

        $venta->estado = Estado::ACTIVO;
        $venta->update();

        $controllerMovimientoCaja = new MovimientoCajaController();
        $controllerMovimientoCaja->nuevaEntrada($venta, 'ventas/venta', FormaDePago::CUENTA_CORRIENTE, ConceptoDeCaja::VENTA, $venta->total);
    }

    public function __toString(){
        return $this->venta->cliente.' (desde '.$this->fecha_inicio->format('d/m/Y').', hasta '.$this->fecha_fin->format('d/m/Y').')';
    }
}
