<?php

namespace App\Models\Frontend\Ventas;

use Illuminate\Database\Eloquent\Model as Model;

class ConceptoNotaCredito extends Model{

    const DEVOLUCION    = 0;
    const DESCUENTO     = 1;
    const ERROR         = 2;
    const ANULACION     = 3;

    public static $getArray = [
        ''                  => 'Concepto...',
        self::DEVOLUCION    => 'Devolución',
        self::DESCUENTO     => 'Descuento',
        self::ERROR         => 'Error',
        self::ANULACION     => 'Anulación'
    ];

}
