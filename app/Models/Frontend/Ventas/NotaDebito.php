<?php

namespace App\Models\Frontend\Ventas;

use App\Models\Backend\Empresas\Empresa;
use App\Models\Backend\Empresas\Sucursal;
use App\Models\Frontend\Configuraciones\Moneda;
use App\Models\Frontend\Configuraciones\PuntoDeVenta;
use App\Models\Frontend\Personas\Cliente;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use DB;

class NotaDebito extends Model{

    use SoftDeletes;

    protected $table = 'notas_debito';

    protected $primaryKey = 'id';

    protected $fillable = ['numero', 'importe_neto', 'importe_iva', 'importe_descuento', 'porcentaje_descuento', 'importe_impuesto_interno', 'total',
        'total_cobro', 'cotizacion', 'estado', 'descripcion', 'empresa_id', 'cliente_id', 'moneda_id',
        'punto_venta_id', 'tipo_comprobante_id', 'lista_precios_id', 'sucursal_id', 'venta_id', 'cae'];

    protected $dates = ['deleted_at', 'fecha', 'fecha_vencimiento'];

    public function empresa(){
        return $this->belongsTo(Empresa::class);
    }

    public function sucursal(){
        return $this->belongsTo(Sucursal::class);
    }

    public function venta(){
        return $this->belongsTo(Venta::class);
    }

    public function cliente(){
        return $this->belongsTo(Cliente::class);
    }

    public function moneda(){
        return $this->belongsTo(Moneda::class);
    }

    public function detalles(){
        return $this->hasMany(DetalleNotaDebito::class);
    }

    public function detallesCobros(){
        return $this->hasMany(DetalleCobroNotaDebito::class);
    }

    public function puntoVenta(){
        return $this->belongsTo(PuntoDeVenta::class);
    }

    public function listaPrecios(){
        return $this->belongsTo(ListaPrecio::class, 'lista_precios_id');
    }

    public static function listarNotasDeDebitoPorEmpresaSucursal($empresa, $sucursal = null){
        $query = NotaDebito::join('clientes', 'clientes.id', '=', 'notas_debito.cliente_id');
        $query->join('puntos_de_ventas', 'puntos_de_ventas.id', '=', 'notas_debito.punto_venta_id');
        $query->join('monedas', 'monedas.id', '=', 'notas_debito.moneda_id');
        $query->join('lista_precios', 'lista_precios.id', '=', 'notas_debito.lista_precios_id');
        $query->select('notas_debito.id','notas_debito.fecha',DB::raw("CONCAT(puntos_de_ventas.numero,'-',notas_debito.numero) AS numero"), 'clientes.razon_social', 'notas_debito.tipo_comprobante_id', 'notas_debito.estado_facturacion', 'notas_debito.total_cobro', 'puntos_de_ventas.tipo_punto_id', 'monedas.signo', 'lista_precios.nombre as lista');
        $query->where('notas_debito.empresa_id', $empresa->id);
        $query->whereNull('notas_debito.deleted_at');
        $query->orderBy('notas_debito.fecha', 'DESC');
        if (Auth::user()->hasRole('Usuario Vendedor')) {
            $query->where('notas_debito.sucursal_id', $sucursal->id);
        }
        return $notasDebito = $query->get();
    }

    public function interesesCobro(){
        $interes = 0;
        $detallesCobro = DetalleCobroNotaDebito::where('nota_debito_id',$this->id)->get();
        foreach($detallesCobro as $cobro){
            $interes += $cobro->interes;
        }
        return $interes;
    }

    public function __toString(){
        return $this->puntoVenta.'-'.$this->numero;
    }
}
