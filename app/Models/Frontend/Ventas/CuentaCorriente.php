<?php

namespace App\Models\Frontend\Ventas;

use App\Models\Backend\Empresas\Empresa;
use App\Models\Frontend\Personas\Proveedor;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Frontend\Personas\Cliente;
use App\Models\Frontend\Configuraciones\TipoCuentaCorriente;
use DB;

class CuentaCorriente extends Model{

    use SoftDeletes;

    protected $table = 'cuentas_corrientes';

    protected $primaryKey = 'id';

    protected $fillable = ['cliente_id', 'proveedor_id', 'entidad_id', 'url', 'detalle', 'tipo_comprobante', 'tipo_cuenta_corriente_id', 'debe', 'haber', 'empresa_id', 'estado'];

    protected $dates = ['deleted_at','proximo_vencimiento'];

    public function empresa(){
        return $this->belongsTo(Empresa::class);
    }

    public function cliente(){
      return $this->belongsTo(Cliente::class);
    }

    public function proveedor(){
        return $this->belongsTo(Proveedor::class);
    }

    public function tipoCuentaCorriente(){
      return $this->belongsTo(TipoCuentaCorriente::class);
    }

    public static function listarCuentasCorrientesPorClienteEmpresa($cliente, $empresa){
        $query = CuentaCorriente::select('cuentas_corrientes.*', 'clientes.razon_social')
            ->leftjoin('clientes', 'clientes.id', '=', 'cuentas_corrientes.cliente_id')
            ->where('cuentas_corrientes.empresa_id', $empresa->id)
            ->where('cuentas_corrientes.cliente_id', $cliente->id)
            ->whereNull('cuentas_corrientes.deleted_at');
        return $query->get();
    }

    public static function listarCuentasCorrientesPorProveedorEmpresa($proveedor, $empresa){
        $query = CuentaCorriente::select('cuentas_corrientes.*', 'proveedores.razon_social')
            ->leftjoin('proveedores', 'proveedores.id', '=', 'cuentas_corrientes.proveedor_id')
            ->where('cuentas_corrientes.empresa_id', $empresa->id)
            ->where('cuentas_corrientes.proveedor_id', $proveedor->id)
            ->whereNull('cuentas_corrientes.deleted_at');
        return $query->get();
    }

    public static function listarCuentasCorrientesPorEmpresa($empresa){
        $query = CuentaCorriente::select('cuentas_corrientes.id', 'cuentas_corrientes.entidad_id', 'cuentas_corrientes.url', 'cuentas_corrientes.detalle', 'cuentas_corrientes.tipo_comprobante', 'cuentas_corrientes.debe', 'cuentas_corrientes.haber', 'cuentas_corrientes.proximo_vencimiento', 'clientes.razon_social as cliente', 'proveedores.razon_social as proveedor','tipos_cuenta_corriente.nombre as tipoCuentaCorriente')
            ->leftjoin('clientes', 'clientes.id', '=', 'cuentas_corrientes.cliente_id')
            ->leftjoin('proveedores', 'proveedores.id', '=', 'cuentas_corrientes.proveedor_id')
            ->leftjoin('tipos_cuenta_corriente', 'tipos_cuenta_corriente.id', '=', 'cuentas_corrientes.tipo_cuenta_corriente_id')
            ->where('cuentas_corrientes.empresa_id', $empresa->id)
            ->whereNull('cuentas_corrientes.deleted_at');
        return $query->get();
    }

    public static function getCuentasCorrientesParaRecibo($cliente){
        $cuentasCorrientes = CuentaCorriente::select('cuentas_corrientes.created_at as fecha',
                DB::raw('CONCAT("Cuenta Corriente") AS descripcion'),
                'cuentas_corrientes.id',
                'cuentas_corrientes.detalle as detalle',
                DB::raw('(cuentas_corrientes.debe-cuentas_corrientes.haber) AS monto'),
                DB::raw('"cuentas_corrientes" as tipo'))
            ->leftjoin('clientes', 'clientes.id', '=', 'cuentas_corrientes.cliente_id')
            ->where('cuentas_corrientes.empresa_id', $cliente->empresa->id)
            ->where('cuentas_corrientes.cliente_id', $cliente->id)
            ->where('cuentas_corrientes.estado', EstadoCuentaCorriente::PENDIENTE)
            ->whereNull('cuentas_corrientes.deleted_at')
            ->get();
        return $cuentasCorrientes;
    }

    public static function getCuentasCorrientesParaOrdenDePago($cliente){
        $cuentasCorrientes = CuentaCorriente::select('cuentas_corrientes.created_at as fecha',
            DB::raw('CONCAT("Cuenta Corriente") AS descripcion'),
            'cuentas_corrientes.id',
            'cuentas_corrientes.detalle as detalle',
            DB::raw('(cuentas_corrientes.haber-cuentas_corrientes.debe) AS monto'),
            DB::raw('"cuentas_corrientes" as tipo'))
            ->leftjoin('proveedores', 'proveedores.id', '=', 'cuentas_corrientes.proveedor_id')
            ->where('cuentas_corrientes.empresa_id', $cliente->empresa->id)
            ->where('cuentas_corrientes.proveedor_id', $cliente->id)
            ->where('cuentas_corrientes.estado', EstadoCuentaCorriente::PENDIENTE)
            ->whereNull('cuentas_corrientes.deleted_at')
            ->get();
        return $cuentasCorrientes;
    }

    //TODO ver la pos de usarlo para compras futuras
    public static function nuevaDeuda($venta, $url, $monto, $tipo_cuenta_corriente = null){
        $nuevaCuantCorriente = new CuentaCorriente();
        $nuevaCuantCorriente->entidad_id = $venta->id;
        $nuevaCuantCorriente->detalle = $venta->__toString();
        $nuevaCuantCorriente->empresa_id = $venta->empresa_id;
        $nuevaCuantCorriente->cliente_id = $venta->cliente_id;
        $nuevaCuantCorriente->tipo_comprobante = $venta->tipo_comprobante_id;
        $nuevaCuantCorriente->url = $url;
        $nuevaCuantCorriente->debe = $monto;
        $nuevaCuantCorriente->haber = 0;
        $nuevaCuantCorriente->estado = EstadoCuentaCorriente::PENDIENTE;
        $nuevaCuantCorriente->save();

        Cliente::where('id', $venta->cliente_id)->increment('saldo_actual', $monto);
    }

    public static function actualizarDeuda($venta, $monto, $montoAnterior){
        $cuentaCorriente = CuentaCorriente::where('entidad_id', $venta->id)
            ->where('url', '/ventas/venta')->first();
        $cuentaCorriente->debe = $monto;
        $cuentaCorriente->update();

        Cliente::where('id', $venta->cliente_id)->increment('saldo_actual', $monto - $montoAnterior);
    }

    public function __toString(){
        return (string) $this->detalle;
    }
}
