<?php

namespace App\Models\Frontend\Ventas;

use App\Models\Access\User\User;
use App\Models\Backend\Empresas\Empresa;
use App\Models\Backend\Empresas\Sucursal;
use App\Models\Frontend\Configuraciones\Estado;
use App\Models\Frontend\Configuraciones\Moneda;
use App\Models\Frontend\Configuraciones\PorcentajeIva;
use App\Models\Frontend\Configuraciones\PuntoDeVenta;
use App\Models\Frontend\Configuraciones\TipoComprobante;
use App\Models\Frontend\Personas\Cliente;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use DB;

class Venta extends Model{

    use SoftDeletes;

    protected $table = 'ventas';

    protected $primaryKey = 'id';

    protected $fillable = [
        'empresa_id',
        'sucursal_id',
        'usuario_id',
        'cliente_id',
        'moneda_id',
        'cotizacion',
        'numero',
        'importe_neto',
        'importe_iva',
        'importe_descuento',
        'porcentaje_descuento',
        'importe_impuesto_interno',
        'total',
        'total_cobro',
        'descripcion',
        'estado',
        'estado_facturacion ',
        'tipo_comprobante_id',
        'punto_venta_id',
        'lista_precios_id',
        'factura_automatica_id',
        'cae',
        'fecha'
    ];

    protected $dates = ['fecha', 'deleted_at', 'fecha_vencimiento'];

    public function empresa(){
        return $this->belongsTo(Empresa::class);
    }

    public function sucursal(){
        return $this->belongsTo(Sucursal::class);
    }

    public function usuario(){
        return $this->belongsTo(User::class, 'usuario_id');
    }

    public function cliente(){
        return $this->belongsTo(Cliente::class);
    }

    public function moneda(){
        return $this->belongsTo(Moneda::class);
    }

    public function facturaAutomatica(){
        return $this->belongsTo(FacturaVentaAutomatica::class, 'factura_automatica_id');
    }

    public function detalles(){
        return $this->hasMany(DetalleVenta::class);
    }

    public function detallesCobros(){
        return $this->hasMany(DetalleCobroVenta::class);
    }

    public function puntoVenta(){
        return $this->belongsTo(PuntoDeVenta::class);
    }

    public function listaPrecios(){
        return $this->belongsTo(ListaPrecio::class, 'lista_precios_id');
    }

    public function tipoComprobante(){
        if(isset($this->tipo_comprobante_id)){
            return TipoComprobante::$comprobantes[$this->tipo_comprobante_id];
        }else{
            return '';
        }
    }

    public static function listarVentasPorEmpresaSucursal($empresa, $sucursal = null, $perPage = null, $keyword = null){
        $query = Venta::leftJoin('clientes', 'clientes.id', '=', 'ventas.cliente_id');
        $query->leftJoin('puntos_de_ventas', 'puntos_de_ventas.id', '=', 'ventas.punto_venta_id');
        $query->leftJoin('monedas', 'monedas.id', '=', 'ventas.moneda_id');
        $query->leftJoin('lista_precios', 'lista_precios.id', '=', 'ventas.lista_precios_id');
        $query->leftJoin('users', 'users.id', '=', 'ventas.usuario_id');
        $query->select('ventas.id', 'ventas.fecha', DB::raw("CONCAT(puntos_de_ventas.numero,'-',ventas.numero) AS numero"), 'ventas.tipo_comprobante_id', 'ventas.estado_facturacion', 'ventas.estado', 'ventas.total', 'puntos_de_ventas.tipo_punto_id', 'clientes.razon_social', 'monedas.signo', 'lista_precios.nombre AS lista','users.name AS usuario');
        $query->where('ventas.empresa_id', $empresa->id);
        $query->whereNull('ventas.deleted_at');
        $query->where('ventas.estado','<>', Estado::PLANTILLA);
        $query->where('ventas.estado','<>', Estado::FACTURA_AUTOMATICA_PENDIENTE);
        $query->orderBy('ventas.fecha','DESC');
        if (Auth::user()->hasRole('Usuario Vendedor')) {
            $query->where('ventas.sucursal_id', $sucursal->id);
        }
        $query->where(function ($q) use ($keyword) {
            $q->where('ventas.numero', 'LIKE', "%$keyword%")
                ->orWhere('puntos_de_ventas.numero', 'LIKE', "%$keyword%")
                ->orWhere('clientes.razon_social', 'LIKE', "%$keyword%");
        });
        if($perPage){
            return $query->paginate($perPage);
        } else {
            return $query->get();
        }
    }

    public static function listarVentasAutomaticasPorEmpresaSucursal($empresa, $sucursal = null){
        $query = Venta::join('clientes', 'clientes.id', '=', 'ventas.cliente_id');
        $query->join('puntos_de_ventas', 'puntos_de_ventas.id', '=', 'ventas.punto_venta_id');
        $query->join('monedas', 'monedas.id', '=', 'ventas.moneda_id');
        $query->join('lista_precios', 'lista_precios.id', '=', 'ventas.lista_precios_id');
        $query->select('ventas.id', 'ventas.fecha', DB::raw("CONCAT(puntos_de_ventas.numero,'-',ventas.numero) AS numero"), 'ventas.tipo_comprobante_id', 'ventas.estado_facturacion', 'ventas.total_cobro', 'puntos_de_ventas.tipo_punto_id', 'clientes.razon_social', 'monedas.signo', 'lista_precios.nombre AS lista');
        $query->where('ventas.empresa_id', $empresa->id);
        $query->whereNull('ventas.deleted_at');
        $query->whereNotNull('ventas.factura_automatica_id');
        $query->where('ventas.estado','<>', Estado::PLANTILLA);
        $query->orderBy('ventas.fecha','DESC');
        if (Auth::user()->hasRole('Usuario Vendedor')) {
            $query->where('ventas.sucursal_id', $sucursal->id);
        }
        return $query->get();
    }

    public static function obtenerVentasPeriodo($periodo,$puntosDeVenta){
        $empresa = Auth::user()->empresa;
        $year = substr($periodo, -4);
        $month = substr($periodo, 0, 2);

        $query = Venta::where('empresa_id', $empresa->id);
        $query->whereYear('fecha', '=', $year);
        $query->whereMonth('fecha', '=', $month);
        $query->where('estado_facturacion','<>', EstadoFacturacion::ERROR);
        $query->where('estado_facturacion','<>', EstadoFacturacion::PENDIENTE);
        $query->where('punto_venta_id', $puntosDeVenta->get(0)->id);
        $query->whereNull('deleted_at');
        $query->where(function($q) use ($puntosDeVenta) {
            foreach ($puntosDeVenta as $key => $p){
                if($key == 0){
                    $q->Where('punto_venta_id', $p->id);
                } else {
                    $q->orWhere('punto_venta_id', $p->id);
                }
            }
        });
        $query->whereNull('deleted_at');
        $query->orderBy('fecha', 'asc');
        $ventas = $query->get();

        return $ventas;
    }

    public static function obtenerPorcentajesPeriodo($periodo){
        $empresa = Auth::user()->empresa;
        $year = substr($periodo, -4);
        $month = substr($periodo, 0, 2);

        $porcentajes = DetalleVenta::select('articulos.porcentaje_iva')
            ->join('ventas','ventas.id','=','detalles_ventas.venta_id')
            ->join('articulos','articulos.id','=','detalles_ventas.articulo_id')
            ->where('ventas.empresa_id', $empresa->id)
            ->where('ventas.estado_facturacion','<>', EstadoFacturacion::ERROR)
            ->whereYear('ventas.fecha', '=', $year)
            ->whereMonth('ventas.fecha', '=', $month)
            ->groupBy('porcentaje_iva')
            ->get();

        return $porcentajes;
    }

    public function interesesCobro(){
        $interes = 0;
        $detallesCobro = DetalleCobroVenta::where('venta_id',$this->id)->get();
        foreach($detallesCobro as $cobro){
            $interes += $cobro->interes;
        }
        return $interes;
    }


    public function __toString(){
        return $this->puntoVenta.'-'.$this->numero;
    }
}
