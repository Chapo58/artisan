<?php

namespace App\Models\Frontend\Ventas;

use App\Models\Frontend\Configuraciones\EntidadRelacionadaLink;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Frontend\Fondos\CuentaBancaria;
use App\Models\Frontend\Configuraciones\Tarjeta;
use App\Models\Frontend\Configuraciones\PlanTarjeta;

class CuponTarjeta extends Model
{

    use SoftDeletes;

    protected $table = 'cupon_tarjetas';

    protected $primaryKey = 'id';

    protected $fillable = ['fecha', 'fecha_acreditacion' , 'tarjeta_id', 'plan_tarjeta_id', 'entidad_relacionada_link_id', 'cuenta_bancaria_id', 'lote', 'cupon', 'titular', 'importe', 'autorizacion', 'estado'];

    protected $dates = ['deleted_at', 'fecha', 'fecha_acreditacion'];

    public function tarjeta(){
      return $this->belongsTo(Tarjeta::class);
    }

    public function planTarjeta(){
      return $this->belongsTo(PlanTarjeta::class);
    }

    public function cuentaBancaria(){
        return $this->belongsTo(CuentaBancaria::class);
    }

    public function entidadRelacionada(){
        return $this->belongsTo(EntidadRelacionadaLink::class, 'entidad_relacionada_link_id');
    }

    public function __toString(){
        return (string) $this->lote." / ".$this->cupon;
    }
}
