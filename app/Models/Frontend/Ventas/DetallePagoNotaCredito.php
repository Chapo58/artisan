<?php

namespace App\Models\Frontend\Ventas;

use App\Models\Frontend\Articulos\Articulo;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DetallePagoNotaCredito extends Model{

    use SoftDeletes;

    protected $table = 'detalles_pago_notas_credito';

    protected $primaryKey = 'id';

    protected $fillable = ['nota_credito_id', 'entidad_id', 'forma_pago' , 'monto', 'interes', 'descripcion', 'estado'];

    protected $dates = ['deleted_at', 'fecha'];

    public function notaCredito(){
        return $this->belongsTo(NotaCredito::class);
    }

    public function __toString(){
        return $this->descripcion;
    }
}
