<?php

namespace App\Models\Frontend\Ventas;

use App\Models\Backend\Empresas\Empresa;
use App\Models\Backend\Empresas\Sucursal;
use App\Models\Frontend\Configuraciones\Moneda;
use App\Models\Frontend\Personas\Cliente;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Recibo extends Model{

    use SoftDeletes;

    protected $table = 'recibos';

    protected $primaryKey = 'id';

    protected $fillable = ['cotizacion', 'numero', 'importe', 'total_cobro', 'estado', 'descripcion', 'empresa_id', 'sucursal_id', 'cliente_id', 'moneda_id'];

    protected $dates = ['deleted_at', 'fecha'];

    public function empresa(){
        return $this->belongsTo(Empresa::class);
    }

    public function sucursal(){
        return $this->belongsTo(Sucursal::class);
    }

    public function cliente(){
        return $this->belongsTo(Cliente::class);
    }

    public function moneda(){
        return $this->belongsTo(Moneda::class);
    }

    public function detalles(){
        return $this->hasMany(DetalleRecibo::class);
    }

    public function detallesCobros(){
        return $this->hasMany(DetalleCobroRecibo::class);
    }

    public static function getRecibosPorEmpresa($empresa){
        $recibos = Recibo::where('empresa_id', $empresa->id)
            ->whereNull('deleted_at')
            ->get();
        return $recibos;
    }

    public static function getRecibosPorSucursal($sucursal){
        $recibos = Recibo::where('empresa_id', $sucursal->empresa->id)
            ->where('sucursal_id', $sucursal->id)
            ->whereNull('deleted_at')
            ->get();
        return $recibos;
    }

    public static function getRecibosPorEmpresaArray($empresa){
        $recibos = self::getRecibosPorEmpresa($empresa)->pluck('numero', 'id')->all();
        return ['' => 'Seleccione un recibo...'] + $recibos;
    }

    public static function getRecibosPorSucursalArray($sucursal){
        $recibos = self::getRecibosPorSucursal($sucursal)->pluck('numero', 'id')->all();
        return ['' => 'Seleccione un recibo...'] + $recibos;
    }

    public static function listarRecibosPorEmpresaSucursal($empresa, $sucursal = null){
        $query = Recibo::select('recibos.id','recibos.numero','recibos.fecha','clientes.razon_social','recibos.importe', 'monedas.signo');
        $query->where('recibos.empresa_id', $empresa->id);
        $query->join('clientes', 'clientes.id', 'recibos.cliente_id');
        $query->join('monedas', 'monedas.id', 'recibos.moneda_id');
        $query->whereNull('recibos.deleted_at');
        $query->orderBy('recibos.fecha','DESC');
        if (Auth::user()->hasRole('Usuario Vendedor')) {
            $query->where('recibos.sucursal_id', $sucursal->id);
        }
        return $query->get();
    }

    public function __toString(){
        return (string) $this->numero;
    }
}
