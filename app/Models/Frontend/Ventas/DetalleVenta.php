<?php

namespace App\Models\Frontend\Ventas;

use App\Models\Frontend\Articulos\Articulo;
use App\Models\Frontend\Configuraciones\PorcentajeIva;
use App\Models\Frontend\Configuraciones\TipoComprobante;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DetalleVenta extends Model{

    use SoftDeletes;

    protected $table = 'detalles_ventas';

    protected $primaryKey = 'id';

    protected $fillable = ['venta_id', 'articulo_id', 'precio_neto', 'precio_iva', 'porcentaje_iva', 'importe_impuesto_interno', 'cantidad',
        'descripcion', 'estado'];

    protected $dates = ['deleted_at'];

    public function venta(){
        return $this->belongsTo(Venta::class);
    }

    public function articulo(){
        return $this->belongsTo(Articulo::class);
    }

    public function porcentajeIva(){
        if(isset($this->porcentaje_iva)){
            return PorcentajeIva::$valores[$this->porcentaje_iva];
        }else{
            return '';
        }
    }

    public function precioUnitario(){
        if($this->venta->tipo_comprobante_id == TipoComprobante::FACTURA_A || $this->venta->tipo_comprobante_id == TipoComprobante::TIQUE_FACTURA_A){
            $precio = $this->precio_neto;
        } else {
            $precio = $this->precio_neto * ((PorcentajeIva::$valores[$this->porcentaje_iva]/100) + 1) + $this->importe_impuesto_interno;
        }

        return $precio;
    }

    public function __toString(){
        return $this->precio. " - ". $this->articulo->nombre;
    }
}
