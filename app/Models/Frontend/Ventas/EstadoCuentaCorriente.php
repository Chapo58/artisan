<?php

namespace App\Models\Frontend\Ventas;

use Illuminate\Database\Eloquent\Model as Model;

class EstadoCuentaCorriente extends Model{

    const PENDIENTE  = 0;
    const FINALIZADA = 1;

    public static $getArray = [
        self::PENDIENTE     => 'Pendiene',
        self::FINALIZADA    => 'Cobrado',
    ];

    public function esPendiente(){
        return $this->estados == self::PENDIENTE;
    }

    public function esFinalizada(){
        return $this->estados == self::FINALIZADA;
    }

}
