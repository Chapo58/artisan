<?php

namespace App\Models\Frontend\Ventas;

use App\Models\Frontend\Articulos\Articulo;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DetalleNotaDebito extends Model{

    use SoftDeletes;

    protected $table = 'detalles_notas_debito';

    protected $primaryKey = 'id';

    protected $fillable = ['nota_debito_id', 'articulo_id', 'precio_neto', 'precio_iva', 'porcentaje_iva', 'importe_impuesto_interno', 'cantidad',
        'descripcion', 'estado'];

    protected $dates = ['deleted_at'];

    public function notaDebito(){
        return $this->belongsTo(NotaDebito::class);
    }

    public function articulo(){
        return $this->belongsTo(Articulo::class);
    }

    public function __toString(){
        return $this->precio. " - ". $this->articulo->nombre;
    }
}
