<?php

namespace App\Models\Frontend\Articulos;

use App\Models\Backend\Empresas\Empresa;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Rubro extends Model{

    use SoftDeletes;

    protected $table = 'rubros';

    protected $primaryKey = 'id';

    protected $fillable = ['empresa_id', 'nombre', 'descripcion', 'imagen_url'];

    protected $dates = ['deleted_at'];

    public function subrubros(){
        return $this->hasMany(Subrubro::class);
    }

    public function articulos(){
        return $this->hasMany(Articulo::class);
    }

    public function empresa(){
        return $this->belongsTo(Empresa::class);
    }

    public static function getRubrosPorEmpresa($empresa){
        $rubros = Rubro::where('empresa_id', $empresa->id)->get();
        return $rubros;
    }

    public static function getRubrosPorEmpresaArray($empresa){
        $rubros = self::getRubrosPorEmpresa($empresa)->sortBy('nombre')->pluck('nombre', 'id')->all();
        return ['' => 'Seleccione un rubro...'] + $rubros;
    }

    public function __toString(){
        return $this->nombre;
    }
}
