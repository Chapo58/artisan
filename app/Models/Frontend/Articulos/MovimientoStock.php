<?php

namespace App\Models\Frontend\Articulos;

use App\Models\Access\User\User;
use App\Models\Backend\Empresas\Sucursal;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MovimientoStock extends Model{
    use SoftDeletes;

    protected $table = 'movimientos_stock';

    protected $primaryKey = 'id';

    protected $fillable = ['stock_id', 'tipo_movimiento_stock_id', 'cantidad', 'descripcion', 'sucursal_origen_id', 'sucursal_destino_id', 'estado','usuario_id'];

    protected $dates = ['created_at','updated_at','deleted_at'];

    public function tipoMovimiento(){
        return $this->belongsTo(TipoMovimientoStock::class, 'tipo_movimiento_stock_id');
    }

    public function stock(){
        return $this->belongsTo(Stock::class, 'stock_id');
    }

    public function sucursalOrigen(){
        return $this->belongsTo(Sucursal::class, 'sucursal_origen_id');
    }

    public function sucursalDestino(){
        return $this->belongsTo(Sucursal::class, 'sucursal_destino_id');
    }

    public function usuario(){
        return $this->belongsTo(User::class, 'usuario_id');
    }
}
