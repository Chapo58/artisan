<?php

namespace App\Models\Frontend\Articulos;

use Illuminate\Database\Eloquent\Model as Model;

class EstadoMovimientoStock extends Model{

    const PENDIENTE       = 1;
    const APROBADO        = 2;
    const RECHAZADO    = 3;

    public static $getArray = [
        self::PENDIENTE       => 'Pendiente',
        self::APROBADO        => 'Aprobado',
        self::RECHAZADO    => 'Rechazado',
    ];

}
