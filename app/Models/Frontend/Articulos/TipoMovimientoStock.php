<?php

namespace App\Models\Frontend\Articulos;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TipoMovimientoStock extends Model{

    use SoftDeletes;

    protected $table = 'tipos_movimientos_stock';

    protected $primaryKey = 'id';

    protected $fillable = ['nombre', 'descripcion'];

    protected $dates = ['deleted_at'];

    public function __toString(){
        return $this->nombre;
    }
}
