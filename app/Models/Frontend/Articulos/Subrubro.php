<?php

namespace App\Models\Frontend\Articulos;

use App\Models\Backend\Empresas\Empresa;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subrubro extends Model{

    use SoftDeletes;

    protected $table = 'subrubros';

    protected $primaryKey = 'id';

    protected $fillable = ['empresa_id', 'nombre', 'descripcion', 'rubro_id'];

    protected $dates = ['deleted_at'];

    public function rubro(){
		return $this->belongsTo(Rubro::class);
	}

    public function articulos(){
        return $this->hasMany(Articulo::class);
    }

    public function empresa(){
        return $this->belongsTo(Empresa::class);
    }

    public static function getSubrubrosPorEmpresa($empresa){
        $subrubros = Subrubro::where('empresa_id', $empresa->id)->get();
        return $subrubros;
    }

    public static function getSubrubrosPorEmpresaArray($empresa){
        $subrubros = self::getSubrubrosPorEmpresa($empresa)->sortBy('nombre')->pluck('nombre', 'id')->all();
        return ['' => 'Seleccione un subrubro...'] + $subrubros;
    }

    public function __toString(){
        return $this->nombre;
    }
}
