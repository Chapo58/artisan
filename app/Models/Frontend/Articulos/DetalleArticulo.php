<?php

namespace App\Models\Frontend\Articulos;

use App\Models\Frontend\Configuraciones\Moneda;
use App\Models\Frontend\Personas\Proveedor;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DetalleArticulo extends Model{

    use SoftDeletes;

    protected $table = 'detalle_articulos';
    
    protected $primaryKey = 'id';

    protected $fillable = ['costo', 'cantidad', 'proveedor_id', 'articulo_id', 'moneda_id', 'importe_iva'];

    protected $dates = ['deleted_at'];

    public function proveedor(){
        return $this->belongsTo(Proveedor::class);
    }

    public function articulo(){
        return $this->belongsTo(Articulo::class);
    }

    public function moneda(){
        return $this->belongsTo(Moneda::class);
    }
}
