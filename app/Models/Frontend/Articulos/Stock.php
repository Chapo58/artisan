<?php

namespace App\Models\Frontend\Articulos;

use App\Models\Backend\Empresas\Sucursal;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Stock extends Model{

    use SoftDeletes;

    protected $table = 'stocks';

    protected $primaryKey = 'id';

    protected $fillable = ['articulo_id', 'cantidad', 'maximo', 'minimo', 'alarma', 'notificacion', 'sucursal_id'];

    protected $dates = ['deleted_at'];

    public function articulo(){
        return $this->belongsTo(Articulo::class);
    }

    public function movimientosStock(){
        return $this->hasMany(MovimientoStock::class);
    }

    public static function alertas($empresa, $sucursal = null){
        $query = Stock::join('articulos', 'articulos.id', '=', 'stocks.articulo_id');
        if(isset($empresa)){
            $query->where('articulos.empresa_id', $empresa->id);
        } else {
            $query->where('articulos.empresa_id', 1);
        }
        $query->where('stocks.alarma', true);
        $query->where('stocks.maximo', '<>', 0);
        $query->whereRaw('stocks.minimo >= stocks.cantidad');
        $query->whereNull('articulos.deleted_at');
        $query->whereNull('stocks.deleted_at');
        if(isset($sucursal)){
            $query->where(function ($q) use ($sucursal) {
                $q->where('stocks.sucursal_id', $sucursal->id);
            });
        }
        return $query->get();
    }

    public static function cantidadAlertas($empresa, $sucursal = null){
        return self::alertas($empresa, $sucursal)->count();
    }

    public function sucursal(){
        return $this->belongsTo(Sucursal::class);
    }

    public function __toString(){
        return $this->articulo;
    }
}
