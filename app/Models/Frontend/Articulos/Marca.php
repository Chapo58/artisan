<?php

namespace App\Models\Frontend\Articulos;

use App\Models\Backend\Empresas\Empresa;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Marca extends Model{

    use SoftDeletes;

    protected $table = 'marcas';

    protected $primaryKey = 'id';

    protected $fillable = ['empresa_id', 'nombre', 'descripcion', 'imagen_url'];

    protected $dates = ['deleted_at'];

    public function articulos(){
		return $this->hasMany(Articulo::class);
	}

    public function empresa(){
        return $this->belongsTo(Empresa::class);
    }

    public static function getMarcasPorEmpresa($empresa){
        $marcas = Marca::where('empresa_id', $empresa->id)->get();
        return $marcas;
    }

    public static function getMarcasPorEmpresaArray($empresa){
        $marcas = self::getMarcasPorEmpresa($empresa)->sortBy('nombre')->pluck('nombre', 'id')->all();
        return ['' => 'Seleccione una marca...'] + $marcas;
    }

    public function __toString(){
        return $this->nombre;
    }
	
}
