<?php

namespace App\Models\Frontend\Articulos;

use App\Models\Backend\Empresas\Empresa;
use App\Models\Frontend\Configuraciones\ActividadAfip;
use App\Models\Frontend\Configuraciones\Moneda;
use App\Models\Frontend\Configuraciones\Opcion;
use App\Models\Frontend\Configuraciones\PorcentajeIva;
use App\Models\Frontend\Configuraciones\Unidad;
use App\Models\Frontend\Ventas\Precio;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class Articulo extends Model{
    use SoftDeletes;

    protected $table = 'articulos';

    protected $primaryKey = 'id';

    protected $fillable = [
        'rubro_id',
        'subrubro_id',
        'marca_id',
        'unidad_id',
        'empresa_id',
        'actividad_afip_id',
        'moneda_id',
        'codigo',
        'codigo_barra',
        'utilidad',
        'nombre',
        'descripcion',
        'modelo',
        'modelo_secundario',
        'porcentaje_iva',
        'cantidad',
        'imagen_url',
        'ultimo_costo',
        'impuesto',
        'grupo_articulos_id',
        'lleva_stock',
        'destacado',
        ];

    protected $dates = ['deleted_at'];

    public function rubro(){
        return $this->belongsTo(Rubro::class);
    }

    public function subrubro(){
        return $this->belongsTo(Subrubro::class);
    }

    public function marca(){
        return $this->belongsTo(Marca::class);
    }

    public function moneda(){
        return $this->belongsTo(Moneda::class);
    }

    public function detalleArticulos(){
        return $this->hasMany(DetalleArticulo::class);
    }

    public function stocks(){
        return $this->hasMany(Stock::class);
    }

    public function unidad(){
        return $this->belongsTo(Unidad::class);
    }

    public function actividadAfip(){
        return $this->belongsTo(ActividadAfip::class, 'actividad_afip_id');
    }

    public function porcentajeIva(){
        if(isset($this->porcentaje_iva)){
            return PorcentajeIva::$valores[$this->porcentaje_iva];
        }else{
            return '';
        }
    }

    public function porcentajeRemarcado(){
        if(isset($this->ultimo_costo) && $this->ultimo_costo != 0){
            return number_format(((($this->ultimo_costo/(1-($this->utilidad/100)))*100)/$this->ultimo_costo)-100, 2);
        }else{
            return 0;
        }
    }

    public function precios(){
        return $this->hasMany(Precio::class);
    }

    public function empresa(){
        return $this->belongsTo(Empresa::class);
    }

    public function grupo(){
        return $this->belongsTo(GrupoArticulos::class, 'grupo_articulos_id');
    }

    public function descontarStock($cantidad, $sucursal){
        if($this->lleva_stock){
            $stock = Stock::where('articulo_id', $this->id)
                ->where('sucursal_id', $sucursal->id)
                ->whereNull('deleted_at')
                ->first();
            if(isset($stock)){
                $stock->cantidad-= $cantidad;
                $stock->save();
                return true;
            }else{
                return false;
            }
        }
    }

    public function sumarStock($cantidad, $sucursal){
        if($this->lleva_stock) {
            $stock = Stock::where('articulo_id', $this->id)
                ->where('sucursal_id', $sucursal->id)
                ->whereNull('deleted_at')
                ->first();
            if (isset($stock)) {
                $stock->cantidad += $cantidad;
                $stock->save();
                return true;
            } else {
                return false;
            }
        }
    }

    public static function listarArticulos($empresa, $perPage = 25, $keyword = null, $conGrupo = true){
        $query = Articulo::join('rubros', 'rubros.id', '=', 'articulos.rubro_id')
            ->join('subrubros', 'subrubros.id', '=', 'articulos.subrubro_id')
            ->leftjoin('marcas', 'marcas.id', '=', 'articulos.marca_id')
            ->select('articulos.*')
            ->where('articulos.empresa_id', $empresa->id)
            ->whereNull('articulos.deleted_at');
        if($conGrupo == false){
            $query->whereNull('articulos.grupo_articulos_id');
        }
        if(isset($keyword)){
            $query->where(function($q) use ($keyword) {
                $q->orWhere('rubros.nombre', 'LIKE', "%$keyword%");
                $q->orWhere('subrubros.nombre', 'LIKE', "%$keyword%");
                $q->orWhere(DB::raw('CONCAT_WS(" ", articulos.nombre, marcas.nombre, articulos.modelo)'), 'LIKE', "%$keyword%");
                $q->orWhere(DB::raw('CONCAT_WS(" ", articulos.nombre, marcas.nombre, articulos.modelo_secundario)'), 'LIKE', "%$keyword%");
                $q->orWhere(DB::raw('CONCAT_WS(" ", articulos.nombre, articulos.modelo, articulos.modelo_secundario)'), 'LIKE', "%$keyword%");
                $q->orWhere(DB::raw('CONCAT_WS(" ", articulos.nombre, articulos.modelo_secundario, articulos.modelo)'), 'LIKE', "%$keyword%");
                $q->orWhere(DB::raw('CONCAT_WS(" ", marcas.nombre, articulos.nombre, articulos.modelo)'), 'LIKE', "%$keyword%");
                $q->orWhere(DB::raw('CONCAT_WS(" ", marcas.nombre, articulos.modelo, articulos.modelo_secundario)'), 'LIKE', "%$keyword%");
                $q->orWhere(DB::raw('CONCAT_WS(" ", marcas.nombre, articulos.modelo_secundario, articulos.modelo)'), 'LIKE', "%$keyword%");
                $q->orWhere(DB::raw('CONCAT_WS(" ", articulos.codigo, articulos.modelo, articulos.modelo_secundario)'), 'LIKE', "%$keyword%");
                $q->orWhere(DB::raw('CONCAT_WS(" ", articulos.codigo, articulos.modelo_secundario, articulos.modelo)'), 'LIKE', "%$keyword%");
                if(strlen($keyword) >= 4){
                    $q->orWhere('articulos.codigo_barra', 'LIKE', "%$keyword%");
                }
            });
        }
        return $query->paginate($perPage);
    }

    public static function listarArticulosConStock($empresa, $sucursal_id, $perPage = 25, $keyword = null){
        $query = Articulo::join('rubros', 'rubros.id', '=', 'articulos.rubro_id')
            ->join('subrubros', 'subrubros.id', '=', 'articulos.subrubro_id')
            ->leftjoin('marcas', 'marcas.id', '=', 'articulos.marca_id')
            ->leftjoin('stocks', 'stocks.articulo_id', '=', 'articulos.id')
            ->select('articulos.*', 'stocks.cantidad as existencia')
            ->where('articulos.empresa_id', $empresa->id)
            ->whereNull('articulos.deleted_at')
            ->where(function($q) use ($sucursal_id){
                $q->orWhereNull('stocks.id')
                    ->orWhereNotNull('stocks.deleted_at')
                    ->orWhere(function($r) use ($sucursal_id){
                        $r->where('stocks.cantidad', '>', 0)
                            ->where('stocks.sucursal_id', $sucursal_id);
                    });
            });
        if(isset($keyword)){
            $query->where(function($q) use ($keyword) {
                $q->where('articulos.nombre', 'LIKE', "%$keyword%");
                $q->orWhere('articulos.codigo', 'LIKE', "%$keyword%");
                $q->orWhere('articulos.modelo', 'LIKE', "%$keyword%");
                $q->orWhere('articulos.modelo_secundario', 'LIKE', "%$keyword%");
                $q->orWhere('rubros.nombre', 'LIKE', "%$keyword%");
                $q->orWhere('subrubros.nombre', 'LIKE', "%$keyword%");
                $q->orWhere('marcas.nombre', 'LIKE', "%$keyword%");
                $q->orWhere(DB::raw('CONCAT(articulos.nombre, " ", marcas.nombre)'), 'LIKE', "%$keyword%");
                $q->orWhere(DB::raw('CONCAT(marcas.nombre, " ", articulos.nombre)'), 'LIKE', "%$keyword%");
                $q->orWhere(DB::raw('CONCAT(articulos.nombre, " ", articulos.modelo)'), 'LIKE', "%$keyword%");
                $q->orWhere(DB::raw('CONCAT(articulos.modelo, " ", articulos.nombre)'), 'LIKE', "%$keyword%");
                $q->orWhere(DB::raw('CONCAT(articulos.codigo, " ", articulos.nombre)'), 'LIKE', "%$keyword%");
                if(strlen($keyword) >= 4){
                    $q->orWhere('articulos.codigo_barra', 'LIKE', "%$keyword%");
                }
            });
        }
        return $query->paginate($perPage);
    }

    public function stockPorSucursal($sucursal){
        $stock = Stock::where('articulo_id', $this->id)
            ->where('sucursal_id', $sucursal->id)
            ->whereNull('deleted_at')
            ->first();
        return $stock;
    }

    public function getStock($sucursal = null){
        if($this->lleva_stock){
            if($sucursal){
                $stock = Stock::where('articulo_id', $this->id)
                    ->where('sucursal_id', $sucursal->id)
                    ->whereNull('deleted_at')
                    ->first();
            } else {
                $stock = Stock::where('articulo_id',$this->id)->whereNull('deleted_at')->first();
            }
            if(!$stock){
                return '0';
            }
            return $stock->cantidad;
        } else {
            return '';
        }
    }

    public function getPrecio($articulo = null, $precioFinal = true, $lista = null){
        if(!$articulo) $articulo = $this;
        if($lista){
            $query = Precio::where('lista_precio_id',$lista->id);
        } else {
            $listaPorDefecto = Opcion::where('empresa_id', $articulo->empresa->id)
                ->where('nombre', 'lista_precios_defecto_id')
                ->first();
            $query = Precio::where('lista_precio_id',$listaPorDefecto->valor);
        }
        $query->where('articulo_id',$articulo->id);
        $precio = $query->first();
        if(!$precio) return 0;

        if($precioFinal){
            $porcentajes_iva = PorcentajeIva::$valores;
            $iva = $precio->precio * $porcentajes_iva[$articulo->porcentaje_iva] / 100;
            $precio  = $precio->precio + $iva;
        } else {
            $precio = $precio->precio;
        }
        return number_format($precio,2,'.','');
    }

    public function getPrecioPorArticuloId($articulo_id){
        $articulo = Articulo::find($articulo_id);
        $listaPorDefecto = Opcion::where('empresa_id', $articulo->empresa->id)
            ->where('nombre', 'lista_precios_defecto_id')
            ->first();
        $precio = Precio::where('lista_precio_id',$listaPorDefecto->valor)->where('articulo_id',$articulo_id)->first();
        $porcentajes_iva = PorcentajeIva::$valores;
        $iva = $precio->precio * $porcentajes_iva[$articulo->porcentaje_iva] / 100;

        if(!$precio) return 0;

        $precio = $precio->precio + $iva;
        return number_format($precio,2);
    }

    public function esNuevo(){
        if($this->created_at >= Carbon::now()->subDays(30)){
            return true;
        } else {
            return false;
        }
    }

    public function getImagenUrlAttribute(){
        if(!$this->attributes['imagen_url']){
            return '/articulos/no-image.jpg';
        } else {
            if (file_exists( public_path() . $this->attributes['imagen_url'])) { // Reviso que la imagen exista
                return $this->attributes['imagen_url'];
            } else {
                return '/articulos/no-image.jpg';
            }

        }
    }

    public function __toString(){
        return $this->nombre;
    }

    public static function boot(){
        parent::boot();

        self::creating(function($model){ // Por si quieren crear un domicilio sin paisarticulo sin rubro o subrubro
            if(!$model->rubro_id){
                $rubroOtro = Rubro::where('empresa_id', '=', $model->empresa_id)
                    ->where('nombre', '=', 'Otro')->first();
                $model->rubro_id = $rubroOtro->id;
            }

            if(!$model->subrubro_id){
                $subrubroOtro = Subrubro::where('empresa_id', '=', $model->empresa_id)
                    ->where('nombre', '=', 'Otro')->first();
                $model->subrubro_id = $subrubroOtro->id;
            }
        });
    }

}
