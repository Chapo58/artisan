<?php

namespace App\Models\Frontend\Articulos;

use App\Models\Backend\Empresas\Empresa;
use App\Models\Frontend\Configuraciones\ActividadAfip;
use App\Models\Frontend\Configuraciones\Moneda;
use App\Models\Frontend\Configuraciones\PorcentajeIva;
use App\Models\Frontend\Configuraciones\Unidad;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GrupoArticulos extends Model{
    use SoftDeletes;

    protected $table = 'grupos_articulos';

    protected $primaryKey = 'id';

    protected $fillable = [
        'nombre_grupo',
        'empresa_id',
        'rubro_id',
        'subrubro_id',
        'marca_id',
        'unidad_id',
        'actividad_afip_id',
        'moneda_id',
        'codigo',
        'codigo_barra',
        'nombre',
        'porcentaje_iva',
        'cantidad',
        'imagen_url',
        'ultimo_costo',
        'impuesto',
        'utilidad',
    ];

    protected $dates = ['deleted_at'];

    public function empresa(){
        return $this->belongsTo(Empresa::class);
    }

    public function rubro(){
        return $this->belongsTo(Rubro::class);
    }

    public function subrubro(){
        return $this->belongsTo(Subrubro::class);
    }

    public function marca(){
        return $this->belongsTo(Marca::class);
    }

    public function unidad(){
        return $this->belongsTo(Unidad::class);
    }

    public function actividadAfip(){
        return $this->belongsTo(ActividadAfip::class, 'actividad_afip_id');
    }

    public function moneda(){
        return $this->belongsTo(Moneda::class);
    }

    public function porcentajeIva(){
        if(isset($this->porcentaje_iva)){
            return PorcentajeIva::$valores[$this->porcentaje_iva];
        }else{
            return '';
        }
    }

    public function porcentajeRemarcado(){
        if(isset($this->ultimo_costo) && $this->ultimo_costo != 0){
            return number_format(((($this->ultimo_costo/(1-($this->utilidad/100)))*100)/$this->ultimo_costo)-100, 2);
        }else{
            return 0;
        }
    }

    public function articulos(){
        return $this->hasMany(Articulo::class);
    }

    public static function listarGruposPorEmpresa($empresa){
        $query = GrupoArticulos::select('grupos_articulos.id','grupos_articulos.nombre_grupo','rubros.nombre as rubro','subrubros.nombre as subrubro','marcas.nombre as marca');
        $query->leftjoin('rubros', 'rubros.id', '=', 'grupos_articulos.rubro_id');
        $query->leftjoin('subrubros', 'subrubros.id', '=', 'grupos_articulos.subrubro_id');
        $query->leftjoin('marcas', 'marcas.id', '=', 'grupos_articulos.marca_id');
        $query->where('grupos_articulos.empresa_id', $empresa->id);
        $query->whereNull('grupos_articulos.deleted_at');
        $query->orderBy('grupos_articulos.nombre_grupo','ASC');
        return $query->get();
    }

    public function __toString(){
        return $this->nombre;
    }
}
