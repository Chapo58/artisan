<?php

namespace App\Models\Frontend\Ecommerce;

use App\Models\Frontend\Articulos\Articulo;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DetalleOrdenVenta extends Model{

    use SoftDeletes;

    protected $table = 'detalles_ordenes_ventas';

    protected $primaryKey = 'id';

    protected $fillable = ['orden_venta_id', 'articulo_id', 'precio_neto', 'precio_iva', 'porcentaje_iva', 'importe_impuesto_interno', 'cantidad', 'estado'];

    protected $dates = ['deleted_at'];

    public function ordenVenta(){
        return $this->belongsTo(OrdenVenta::class);
    }

    public function articulo(){
        return $this->belongsTo(Articulo::class);
    }

    public function __toString(){
        return $this->precio. " - ". $this->articulo->nombre;
    }
}
