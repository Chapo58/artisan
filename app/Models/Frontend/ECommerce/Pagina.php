<?php

namespace App\Models\Frontend\ECommerce;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pagina extends Model{

    use SoftDeletes;

    protected $table = 'paginas';

    protected $primaryKey = 'id';

    protected $fillable = ['empresa_id', 'titulo', 'subtitulo', 'imagen_url', 'contenido'];

    protected $dates = ['deleted_at'];

    public function __toString(){
        return (string) $this->titulo;
    }
}
