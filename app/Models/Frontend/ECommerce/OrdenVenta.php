<?php

namespace App\Models\Frontend\ECommerce;

use App\Models\Backend\Empresas\Empresa;
use App\Models\Backend\Planes\FormaPago;
use App\Models\Frontend\Configuraciones\Moneda;
use App\Models\Frontend\Personas\Cliente;
use App\Models\Frontend\Ventas\ListaPrecio;
use App\Models\Frontend\Ventas\Venta;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\HtmlString;

class OrdenVenta extends Model{

    use SoftDeletes;

    protected $table = 'ordenes_ventas';

    protected $primaryKey = 'id';

    protected $fillable = [
        'empresa_id',
        'cliente_id',
        'forma_pago_id',
        'venta_id',
        'moneda_id',
        'cotizacion',
        'lista_precio_id',
        'importe_neto',
        'importe_iva',
        'importe_descuento',
        'porcentaje_descuento',
        'importe_impuesto_interno',
        'total',
        'numero',
        'mensaje',
        'estado'
    ];

    protected $dates = ['deleted_at'];

    public function empresa(){
        return $this->belongsTo(Empresa::class);
    }

    public function cliente(){
        return $this->belongsTo(Cliente::class);
    }

    public function formaPago(){
        return $this->hasMany(FormaPago::class);
    }

    public function venta(){
        return $this->belongsTo(Venta::class);
    }

    public function moneda(){
        return $this->belongsTo(Moneda::class);
    }

    public function listaPrecio(){
        return $this->belongsTo(ListaPrecio::class, 'lista_precio_id');
    }

    public function detalles(){
        return $this->hasMany(DetalleOrdenVenta::class);
    }

    public static function listarOrdenesVentasPorEmpresa($empresa){
        $query = OrdenVenta::where('ordenes_ventas.empresa_id', $empresa->id);
        $query->join('clientes', 'clientes.id', '=', 'ordenes_ventas.cliente_id');
        $query->join('monedas', 'monedas.id', '=', 'ordenes_ventas.moneda_id');
        $query->join('lista_precios', 'lista_precios.id', '=', 'ordenes_ventas.lista_precio_id');
        $query->select('ordenes_ventas.id', 'ordenes_ventas.numero', 'ordenes_ventas.created_at', 'ordenes_ventas.estado', 'ordenes_ventas.total', 'clientes.razon_social', 'monedas.signo', 'lista_precios.nombre AS lista');
        $query->whereNull('ordenes_ventas.deleted_at');
        $query->orderBy('ordenes_ventas.created_at','DESC');
        return $query->get();
    }

    public function __toString(){
        return (string) $this->numero .' - '. $this->cliente;
    }
}
