<?php

namespace App\Models\Frontend\ECommerce;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Caracteristica extends Model{

    use SoftDeletes;

    protected $table = 'caracteristicas';

    protected $primaryKey = 'id';

    protected $fillable = ['empresa_id', 'titulo', 'texto', 'imagen_url'];

    protected $dates = ['deleted_at'];

    public function __toString(){
        return (string) $this->titulo;
    }
}
