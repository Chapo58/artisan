<?php

namespace App\Models\Frontend\ECommerce;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Suscripcion extends Model{

    use SoftDeletes;

    protected $table = 'suscripciones';

    protected $primaryKey = 'id';

    protected $fillable = ['email', 'empresa_id'];

    protected $dates = ['deleted_at'];

    public function __toString(){
        return (string) $this->email;
    }
}
