<?php

namespace App\Models\Frontend\ECommerce;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Slider extends Model{

    use SoftDeletes;

    protected $table = 'sliders';

    protected $primaryKey = 'id';

    protected $fillable = ['empresa_id', 'titulo', 'texto', 'color', 'imagen_url', 'link', 'orden'];

    protected $dates = ['deleted_at'];

    public function __toString(){
        return (string) $this->titulo;
    }
}
