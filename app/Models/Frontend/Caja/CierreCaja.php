<?php

namespace App\Models\Frontend\Caja;

use App\Models\Access\User\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class CierreCaja extends Model{

    use SoftDeletes;

    protected $table = 'cierre_cajas';

    protected $primaryKey = 'id';

    protected $fillable = ['caja_id', 'usuario_apertura_id', 'usuario_cierre_id', 'cuenta_bancaria_id','saldo_apertura','saldo_cierre'];

    protected $dates = ['deleted_at', 'fecha_apertura', 'fecha_cierre'];

    public function caja(){
      return $this->belongsTo(Caja::class);
    }

    public static function getUltimoCierreCajaPorEmpleado($empleado){
        $cierreCaja = CierreCaja::where('caja_id', $empleado->caja_id)
            ->orderBy('fecha_apertura', 'desc')
            ->whereNull('deleted_at')
            ->first();
        return $cierreCaja;
    }

    public static function crearNuevoCierre($caja, $saldo){
        $usuario = Auth::user();

        $nuevoCierreCaja = new CierreCaja();
        $nuevoCierreCaja->caja_id = $caja->id;
        $nuevoCierreCaja->fecha_apertura = Carbon::now();
        $nuevoCierreCaja->usuario_apertura_id = $usuario->id;
        $nuevoCierreCaja->saldo_apertura = $saldo;
        $nuevoCierreCaja->save();

        return $nuevoCierreCaja;
    }

    public static function cerrarNuevoCierre($caja, $saldo){
        $usuario = Auth::user();

        $cierreCaja = CierreCaja::where('caja_id',$caja->id)
            ->whereNull('fecha_cierre')
            ->whereNull('deleted_at')
            ->orderBy('fecha_apertura', 'desc')
            ->first();

        $cierreCaja->fecha_cierre = Carbon::now();
        $cierreCaja->saldo_cierre = $saldo;
        $cierreCaja->usuario_cierre_id = $usuario->id;
        $cierreCaja->save();

        return $cierreCaja;
    }

    public static function getCierreCajaPorMovimiento($movimientoCaja){
        if(!isset($movimientoCaja->created_At)){ // Si me pasan una coleccion busco solamente el primer elemento
            $movimientoCaja = $movimientoCaja->first();
        }
        if(!$movimientoCaja){ // Si no hay movimientos
            return true;
        } else {
            $cierreCaja = CierreCaja::where('caja_id', $movimientoCaja->caja->id)
                ->where('fecha_apertura', '=<' , $movimientoCaja->created_At)
                ->where('fecha_cierre', '=>' , $movimientoCaja->created_At)
                ->first();
            return $cierreCaja;
        }
    }

    public function usuarioApertura(){
        return $this->belongsTo(User::class, 'usuario_apertura_id');
    }

    public function usuarioCierre(){
        return $this->belongsTo(User::class, 'usuario_cierre_id');
    }

    public function __toString(){
        return (string) $this->caja;
    }
}
