<?php

namespace App\Models\Frontend\Caja;

use App\Models\Backend\Empresas\Empresa;
use App\Models\Frontend\Configuraciones\Moneda;
use App\Models\Frontend\Fondos\Cheque;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class MovimientoCaja extends Model{

    use SoftDeletes;

    protected $table = 'movimiento_cajas';

    protected $primaryKey = 'id';

    protected $fillable = ['empresa_id', 'usuario_id', 'caja_id', 'moneda_id', 'concepto_personalizado_id', 'tipo', 'forma_de_pago', 'entrada', 'salida', 'entidad_id', 'entidad_clase', 'url', 'concepto_de_caja', 'detalle', 'comentario'];

    protected $dates = ['deleted_at', 'created_at'];

    public function empresa(){
        return $this->belongsTo(Empresa::class);
    }

    public function conceptoPersonalizado(){
        return $this->belongsTo(ConceptoPersonalizado::class, 'concepto_personalizado_id');
    }

    public function caja(){
        return $this->belongsTo(Caja::class);
    }

    public function moneda(){
        return $this->belongsTo(Moneda::class);
    }

    public function cheques(){
        return $this->belongsToMany(Cheque::class);
    }

    public static function listarMovimientosPorCierreDeCaja($cierreCaja){
        if($cierreCaja->caja){
            $caja = $cierreCaja->caja;
        } else {
            $caja = Caja::where('empresa_id',Auth::user()->empresa->id)->first();
        }
        $query = MovimientoCaja::where('caja_id', $caja->id);
        $query->whereNull('deleted_at');
        $query->where('created_at', ">=", $cierreCaja->fecha_apertura);
        if (isset($cierreCaja->fecha_cierre)) {
            $query->where('created_at', "<=", $cierreCaja->fecha_cierre);
        }
        $query->orderBy('created_at', 'desc');
        return $query->get();
    }

    public function __toString(){
        return (string) $this->created_at->format('H:i:s').' '.$this->detalle;
    }
}
