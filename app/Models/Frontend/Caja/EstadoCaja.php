<?php

namespace App\Models\Frontend\Caja;

use Illuminate\Database\Eloquent\Model as Model;

class EstadoCaja extends Model{

    const BORRADA   = 0;
    const ABIERTA   = 1;
    const CERRADA   = 2;

    public static $getArray = [
        self::BORRADA   => 'Borrada',
        self::ABIERTA   => 'Abierta',
        self::CERRADA   => 'Cerrada',
    ];

}
