<?php

namespace App\Models\Frontend\Caja;

use Illuminate\Database\Eloquent\Model as Model;

class TipoMovimientoCaja extends Model{

    const INGRESO       = 3;
    const EGRESO        = 4;
    const TRANSFERENCIA        = 5;

    public static $tipos = [
        self::INGRESO       => 'Ingreso',
        self::EGRESO        => 'Egreso',
        self::EGRESO        => 'Transferencia',
    ];

}
