<?php

namespace App\Models\Frontend\Caja;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ConceptoPersonalizado extends Model{

    use SoftDeletes;

    protected $table = 'concepto_cajas';

    protected $primaryKey = 'id';

    protected $fillable = ['cuenta_id', 'nombre', 'empresa_id'];

    protected $dates = ['deleted_at'];

    public function movimientosCaja(){
      return $this->hasMany(MovimientoCaja::class);
    }

    public static function getConceptosPersonalizadosPorEmpresa($empresa){
        $conceptos = ConceptoPersonalizado::where('empresa_id', $empresa->id)
            ->whereNull('deleted_at')
            ->get();
        return $conceptos;
    }

    public static function getConceptosPersonalizadosPorEmpresaArray($empresa){
        $conceptos = self::getConceptosPersonalizadosPorEmpresa($empresa)->sortBy('nombre')->pluck('nombre', 'id')->all();
        return $conceptos;
    }

    public function __toString(){
        return (string) $this->nombre;
    }
}
