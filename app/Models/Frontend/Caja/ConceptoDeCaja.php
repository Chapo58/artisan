<?php

namespace App\Models\Frontend\Caja;

use Illuminate\Database\Eloquent\Model as Model;

class ConceptoDeCaja extends Model{

    const COBRO_CHEQUE          = 4;
    const DEPOSITO_CHEQUE       = 5;
    const ACREDITACION_CUPON    = 6;
    const MOVIMIENTO_BANCARIO   = 7;
    const COMPRA                = 8;
    const VENTA                 = 9;
    const INGRESOS_VARIOS       = 10;
    const EGRESOS_VARIOS        = 11;
    const SALDO_INICIAL         = 12;
    const AJUSTE_SALDO          = 13;
    const COBRO_RECIBO          = 14;
    const PAGO_ORDEN            = 15;
    const NOTA_CREDITO          = 16;
    const NOTA_DEBITO           = 17;
    const TRANSFERENCIA_SALDO   = 18;

    public static $formasDePago = [
        self::COBRO_CHEQUE          => 'Cobro de Cheque',
        self::DEPOSITO_CHEQUE       => 'Deposito de Cheque',
        self::ACREDITACION_CUPON    => 'Acreditacion de Cupon',
        self::MOVIMIENTO_BANCARIO   => 'Movimiento Bancario',
        self::COMPRA                => 'Compra',
        self::VENTA                 => 'Venta',
        self::INGRESOS_VARIOS       => 'Ingresos Varios',
        self::EGRESOS_VARIOS        => 'Egresos Varios',
        self::SALDO_INICIAL         => 'Saldo Inicial',
        self::AJUSTE_SALDO          => 'Ajuste Saldo',
        self::COBRO_RECIBO          => 'Cobro Recibo',
        self::PAGO_ORDEN            => 'Pago Orden de Pago',
        self::NOTA_CREDITO          => 'Nota de Crédito',
        self::NOTA_DEBITO           => 'Nota de Débito',
        self::TRANSFERENCIA_SALDO   => 'Transferencia de Saldo',
    ];

}
