<?php

namespace App\Models\Frontend\Caja;

use Illuminate\Database\Eloquent\Model as Model;

class FormaDePago extends Model{

    const EFECTIVO          = 0;
    const CHEQUE            = 1;
    const CUENTA_BANCARIA   = 2;
    const TARJETA           = 3;
    const CUENTA_CORRIENTE  = 4;
    const CHEQUE_PROPIO     = 5;
    const MERCADO_PAGO     = 6;

    public static $getArray = [
        self::EFECTIVO          => 'Efectivo',
        self::CHEQUE            => 'Cheque',
        self::CUENTA_BANCARIA   => 'Cuenta Bancaria',
        self::TARJETA           => 'Tarjeta',
        self::CUENTA_CORRIENTE  => 'Cuenta Corriente',
        self::CHEQUE_PROPIO     => 'Cheque propio',
        self::MERCADO_PAGO     => 'Mercado Pago',
    ];

    public static $getArrayVenta = [
        self::EFECTIVO          => 'Efectivo',
        self::CHEQUE            => 'Cheque',
        self::CUENTA_BANCARIA   => 'Cuenta Bancaria',
        self::TARJETA           => 'Tarjeta',
        self::CUENTA_CORRIENTE  => 'Cuenta Corriente',
        self::MERCADO_PAGO  => 'Mercado Pago',
    ];

    public static $getArrayRecibo = [
        self::EFECTIVO          => 'Efectivo',
        self::CHEQUE            => 'Cheque',
        self::CUENTA_BANCARIA   => 'Cuenta Bancaria',
        self::TARJETA           => 'Tarjeta',
        self::MERCADO_PAGO  => 'Mercado Pago',
    ];

    public static $getArrayOrdenPago = [
        self::EFECTIVO          => 'Efectivo',
        self::CHEQUE            => 'Cheque',
        self::CUENTA_BANCARIA   => 'Cuenta Bancaria',
        self::TARJETA           => 'Tarjeta',
        self::CHEQUE_PROPIO     => 'Cheque Propio',
        self::MERCADO_PAGO  => 'Mercado Pago',
    ];

}
