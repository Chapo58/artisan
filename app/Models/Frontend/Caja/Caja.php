<?php

namespace App\Models\Frontend\Caja;

use App\Models\Backend\Empresas\Sucursal;
use App\Models\Frontend\Configuraciones\Moneda;
use App\Models\Frontend\Fondos\Cheque;
use App\Models\Frontend\Fondos\CuentaBancaria;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Caja extends Model
{

    use SoftDeletes;

    protected $table = 'cajas';

    protected $primaryKey = 'id';

    protected $fillable = ['numero', 'nombre', 'moneda_id', 'sucursal_id', 'cuenta_bancaria_id', 'estado'];

    protected $dates = ['deleted_at'];

    public function moneda(){
      return $this->belongsTo(Moneda::class);
    }

    public function sucursal(){
        return $this->belongsTo(Sucursal::class);
    }

    public function movimientoCaja(){
      return $this->hasMany(MovimientoCaja::class);
    }

    public function cierresCajas(){
      return $this->hasMany(CierreCaja::class);
    }

    public function cuentaBancaria(){
      return $this->belongsTo(CuentaBancaria::class);
    }

    public static function getCajasPorEmpresa($empresa){
        $cajas = Caja::where('empresa_id', $empresa->id)
            ->whereNull('deleted_at')
            ->get();
        return $cajas;
    }

    public static function getCajasPorEmpresaArray($empresa){
        $cajas = self::getCajasPorEmpresa($empresa)->pluck('nombre', 'id')->all();
        return $cajas;
    }

    public function __toString(){
        return (string) $this->numero ." - ". $this->nombre;
    }
}
