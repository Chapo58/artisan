<?php

namespace App\Models\Frontend\Produccion;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TipoTela extends Model{

    use SoftDeletes;

    protected $table = 'tipos_telas';

    protected $primaryKey = 'id';

    protected $fillable = ['nombre', 'empresa_id'];

    protected $dates = ['deleted_at'];

    public function rollos(){
      return $this->hasMany(Rollo::class);
    }

    public static function getTiposPorEmpresa($empresa){
        $tiposTela = TipoTela::where('empresa_id', $empresa->id)->get();
        return $tiposTela;
    }

    public static function getTiposPorEmpresaArray($empresa){
        $tiposTela = self::getTiposPorEmpresa($empresa)->pluck('nombre', 'id')->all();
        return ['' => 'Seleccione un tipo...'] + $tiposTela;
    }

    public function __toString(){
        return (string) $this->nombre;
    }
}
