<?php

namespace App\Models\Frontend\Produccion;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Color extends Model{

    use SoftDeletes;

    protected $table = 'colores';

    protected $primaryKey = 'id';

    protected $fillable = ['nombre', 'empresa_id'];

    protected $dates = ['deleted_at'];

    public function rollos(){
      return $this->hasMany(Rollo::class);
    }

    public static function getColoresPorEmpresa($empresa){
        $colores = Color::where('empresa_id', $empresa->id)->get();
        return $colores;
    }

    public static function getColoresPorEmpresaArray($empresa){
        $colores = self::getColoresPorEmpresa($empresa)->pluck('nombre', 'id')->all();
        return ['' => 'Seleccione un color...'] + $colores;
    }

    public function __toString(){
        return (string) $this->nombre;
    }
}
