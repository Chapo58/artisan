<?php

namespace App\Models\Frontend\Produccion;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DetallePlanillaCorte extends Model{

    use SoftDeletes;

    protected $table = 'detalles_planillas_cortes';

    protected $primaryKey = 'id';

    protected $fillable = ['planillas_corte_id', 'rollo_id', 'color', 'tela', 'peso', 'tiradas', 'restos', 'prendas'];

    protected $dates = ['deleted_at'];

    public function planillaCorte(){
        return $this->belongsTo(DetallePlanillaCorte::class, 'planillas_corte_id');
    }

    public function rollo(){
        return $this->belongsTo(Rollo::class);
    }

    public function __toString(){
        return (string) $this->id;
    }
}