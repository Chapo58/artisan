<?php

namespace App\Models\Frontend\Produccion;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TipoCorte extends Model{

    use SoftDeletes;

    protected $table = 'tipo_cortes';

    protected $primaryKey = 'id';

    protected $fillable = ['nombre', 'empresa_id'];

    protected $dates = ['deleted_at'];

    public function tiposProductos(){
        return $this->hasMany(TipoProducto::class);
    }

    public static function getTiposCortesPorEmpresa($empresa){
        $tiposCortes = TipoCorte::where('empresa_id', $empresa->id)->get();
        return $tiposCortes;
    }

    public static function getTiposCortesPorEmpresaArray($empresa){
        $tiposCortes = self::getTiposCortesPorEmpresa($empresa)->pluck('nombre', 'id')->all();
        return ['' => 'Seleccione un tipo de corte...'] + $tiposCortes;
    }

    public function __toString(){
        return (string) $this->nombre;
    }
}
