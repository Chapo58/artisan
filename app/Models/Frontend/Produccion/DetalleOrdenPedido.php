<?php

namespace App\Models\Frontend\Produccion;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DetalleOrdenPedido extends Model{

    use SoftDeletes;

    protected $table = 'detalles_orden_pedidos_lqh';

    protected $primaryKey = 'id';

    protected $fillable = ['orden_pedido_id', 'tipo_producto_id', 'detalle'];

    protected $dates = ['deleted_at'];

    public function ordenPedido(){
        return $this->belongsTo(OrdenPedido::class);
    }

    public function __toString(){
        return (string) $this->ordenPedido;
    }
    
}
