<?php

namespace App\Models\Frontend\Produccion;

use App\Models\Backend\Empresas\Empresa;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PlanillaCorte extends Model{

    use SoftDeletes;

    protected $table = 'planillas_corte';

    protected $primaryKey = 'id';

    protected $fillable = ['numero', 'empresa', 'tipo_corte',
        'talle0', 'talle2', 'talle4', 'talle6', 'talle8', 'talle10', 'talle12',
        'talle14', 'talle16', 'talleS', 'talleM', 'talleL', 'talleXL', 'talleXXL', 'talleXXXL', 'empresa_id'];

    protected $dates = ['deleted_at', 'fecha'];

    public function empresaSistema(){
        return $this->belongsTo(Empresa::class, 'empresa_id');
    }

    public function detalles(){
        return $this->hasMany(DetallePlanillaCorte::class, 'planillas_corte_id');
    }

    public static function listarPlanillasPorEmpresa($empresa, $perPage = 25, $keyword = null){
        $query = PlanillaCorte::where('empresa_id', $empresa->id);
        $query->whereNull('deleted_at');
        if(isset($keyword)){
            $query->where(function($q) use ($keyword) {
                $q->orWhere('numero', 'LIKE', "%$keyword%");
                $q->orWhere('empresa', 'LIKE', "%$keyword%");
                $q->orWhere('tipo_corte', 'LIKE', "%$keyword%");
            });
        }
        return $query->paginate($perPage);
    }

    public function __toString(){
        return (string) 'N° ' . $this->numero.' | '.$this->empresa.' - '. $this->tipo_corte;
    }
}
