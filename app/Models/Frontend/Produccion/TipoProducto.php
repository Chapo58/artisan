<?php

namespace App\Models\Frontend\Produccion;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TipoProducto extends Model{

    use SoftDeletes;

    protected $table = 'tipo_productos';

    protected $primaryKey = 'id';

    protected $fillable = ['nombre', 'tipo_corte_id', 'empresa_id'];

    protected $dates = ['deleted_at'];

    public function tipoCorte(){
        return $this->belongsTo(TipoCorte::class);
    }

    public function __toString(){
        return (string) $this->nombre;
    }
}
