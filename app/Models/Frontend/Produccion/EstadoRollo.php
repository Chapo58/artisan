<?php

namespace App\Models\Frontend\Produccion;

use Illuminate\Database\Eloquent\Model as Model;

class EstadoRollo extends Model{

    const DISPONIBLE = 0;
    const RESTO      = 1;
    const FINALIZADO = 2;

    public static $getArray = [
        self::DISPONIBLE    => 'Disponible',
        self::RESTO         => 'Resto',
        self::FINALIZADO    => 'Finalizado',
    ];

    public function esDisponible(){
        return $this->estados == self::DISPONIBLE;
    }

    public function esResto(){
        return $this->estados == self::RESTO;
    }

    public function esFinalizado(){
        return $this->estados == self::FINALIZADO;
    }

}
