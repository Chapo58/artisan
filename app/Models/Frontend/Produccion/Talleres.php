<?php

namespace App\Models\Frontend\Produccion;

use App\Models\Backend\Empresas\Empresa;
use App\Models\Frontend\Configuraciones\CondicionIva;
use App\Models\Frontend\Personas\Domicilio;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Talleres extends Model{

    use SoftDeletes;

    protected $table = 'talleres';

    protected $primaryKey = 'id';

    protected $fillable = ['condicion_iva_id', 'domicilio_id', 'razon_social', 'cuit', 'email', 'telefono', 'empresa_id'];

    protected $dates = ['deleted_at'];

    public function empresa(){
        return $this->belongsTo(Empresa::class);
    }

    public function condicionIva(){
        return $this->belongsTo(CondicionIva::class, 'condicion_iva_id');
    }

    public function domicilio(){
        return $this->belongsTo(Domicilio::class);
    }

    public static function getTalleresPorEmpresa($empresa){
        $tiposCortes = Talleres::where('empresa_id', $empresa->id)->get();
        return $tiposCortes;
    }

    public static function getTalleresPorEmpresaArray($empresa){
        $talleres = self::getTalleresPorEmpresa($empresa)->pluck('razon_social', 'id')->all();
        return ['' => 'Seleccione un taller...'] + $talleres;
    }

    public function __toString(){
        return (string) $this->razon_social;
    }
}
