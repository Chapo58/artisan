<?php

namespace App\Models\Frontend\Produccion;

use App\Models\Backend\Empresas\Empresa;
use App\Models\Frontend\Configuraciones\Moneda;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Rollo extends Model{

    use SoftDeletes;

    protected $table = 'rollos';

    protected $primaryKey = 'id';

    protected $fillable = ['tipo_tela_id', 'color_id', 'partida', 'peso', 'tintoreria', 'codigo', 'costo', 'estado', 'moneda_id', 'empresa_id', 'porcentaje_iva'];

    protected $dates = ['deleted_at'];

    public function tipoTela(){
        return $this->belongsTo(TipoTela::class);
    }

    public function color(){
        return $this->belongsTo(Color::class);
    }

    public function moneda(){
        return $this->belongsTo(Moneda::class);
    }

    public function empresa(){
        return $this->belongsTo(Empresa::class);
    }

    public function __toString(){
        return (string) '(' . $this->codigo . ') ' . $this->tipoTela.' color '.$this->color;
    }
}
