<?php

namespace App\Models\Frontend\Produccion;

use App\Models\Frontend\Personas\Cliente;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrdenPedido extends Model{

    use SoftDeletes;

    protected $table = 'orden_pedidos_lqh';

    protected $primaryKey = 'id';

    protected $fillable = ['cliente_id', 'numero_opm', 'fecha_pedido', 'fecha_entrega', 'imagen1', 'imagen2', 'imagen3', 'tipo_corte_id', 'tipo_tela_id', 'color_id', 'cantidad_estampas', 'diseño', 'observaciones', 'talle12', 'talle14', 'talle16', 'talleS', 'talleM', 'talleL', 'talleXL', 'talleXXL', 'talleXXXL', 'total_unidades', 'empresa_id'];

    protected $dates = ['deleted_at', 'fecha_pedido', 'fecha_entrega'];

    public function detalles(){
        return $this->hasMany(DetalleOrdenPedido::class);
    }

    public function tipoTela(){
        return $this->belongsTo(TipoTela::class);
    }

    public function tipoCorte(){
        return $this->belongsTo(TipoCorte::class);
    }

    public function color(){
        return $this->belongsTo(Color::class);
    }

    public function cliente(){
        return $this->belongsTo(Cliente::class);
    }

    public function __toString(){
        return (string) $this->numero_opm;
    }
    
}
