<?php

namespace App\Models\Frontend\Calendario;

use Illuminate\Database\Eloquent\Model;
use App\Models\Backend\Empresas\Empresa;
use Illuminate\Database\Eloquent\SoftDeletes;

class Event extends Model
{

  use SoftDeletes;

  protected $table = 'events';

  protected $primaryKey = 'id';

  protected $fillable = [
      'color','empresa_id','sucursal_id','empleado_id','titulo','fecha_inicio','fecha_fin','hora_inicio','hora_fin','estado','tipo_evento_id'
  ];

    protected $dates = ['created_at','updated_at','deleted_at','fecha_inicio','fecha_fin'];

    public function empresa(){
        return $this->belongsTo(Empresa::class);
    }

    public function __toString(){
        return (string) $this->titulo;
    }

}
