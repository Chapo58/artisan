<?php

namespace App\Models\Frontend\Personas;

use App\Models\Frontend\Caja\Caja;
use App\Models\Frontend\Configuraciones\PuntoDeVenta;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Empleado extends Model{

    use SoftDeletes;

    protected $table = 'empleados';

    protected $primaryKey = 'id';

    protected $fillable = ['empresa_id', 'sucursal_id', 'domicilio_id','caja_id', 'punto_venta_id', 'nombre', 'cuil','telefono'];

    protected $dates = ['deleted_at'];

    public function domicilio(){
        return $this->belongsTo(Domicilio::class);
    }

    public function caja(){
        return $this->belongsTo(Caja::class);
    }

    public function puntoVenta(){
        return $this->belongsTo(PuntoDeVenta::class);
    }

    public function __toString(){
        return (string) $this->nombre;
    }
}
