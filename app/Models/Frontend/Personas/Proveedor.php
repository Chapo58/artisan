<?php

namespace App\Models\Frontend\Personas;

use App\Models\Backend\Empresas\Empresa;
use App\Models\Frontend\Configuraciones\CondicionIva;
use App\Models\Frontend\Ventas\CuentaCorriente;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Proveedor extends Model
{

    use SoftDeletes;

    protected $table = 'proveedores';

    protected $primaryKey = 'id';

    protected $fillable = ['razon_social', 'cuit', 'email', 'contacto', 'telefono', 'empresa_id', 'condicion_iva_id', 'domicilio_id'];

    protected $dates = ['deleted_at'];

    public function empresa(){
        return $this->belongsTo(Empresa::class);
    }

    public function condicionIva(){
        return $this->belongsTo(CondicionIva::class, 'condicion_iva_id');
    }

    public function domicilio(){
        return $this->belongsTo(Domicilio::class);
    }

    public function cuentasCorrientes(){
        return $this->hasMany(CuentaCorriente::class);
    }

    public function __toString(){
        return $this->razon_social;
    }

}
