<?php

namespace App\Models\Frontend\Personas;

use App\Models\Access\User\User;
use App\Models\Backend\Empresas\Empresa;
use App\Models\Frontend\Configuraciones\CondicionIva;
use App\Models\Frontend\Ventas\ListaPrecio;
use App\Models\Frontend\Ventas\CuentaCorriente;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cliente extends Model{

    use SoftDeletes;

    protected $table = 'clientes';

    protected $primaryKey = 'id';

    protected $fillable = [
        'empresa_id',
        'razon_social',
        'dni',
        'condicion_iva_id',
        'lista_precios_id',
        'domicilio_id',
        'email',
        'contacto',
        'telefono',
        'saldo_maximo',
        'saldo_actual',
        'user_id'
    ];

    protected $dates = ['deleted_at'];

    public function empresa(){
        return $this->belongsTo(Empresa::class);
    }

    public function condicionIva(){
        return $this->belongsTo(CondicionIva::class, 'condicion_iva_id');
    }

    public function listaPrecio(){
        return $this->belongsTo(ListaPrecio::class, 'lista_precios_id');
    }

    public function domicilio(){
        return $this->belongsTo(Domicilio::class);
    }

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }

    public function cuentasCorrientes(){
        return $this->hasMany(CuentaCorriente::class);
    }

    public function __toString(){
        return (string) $this->razon_social;
    }
}
