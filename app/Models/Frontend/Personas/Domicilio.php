<?php

namespace App\Models\Frontend\Personas;

use App\Models\Frontend\Configuraciones\Localidad;
use App\Models\Frontend\Configuraciones\Pais;
use App\Models\Frontend\Configuraciones\Provincia;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Domicilio extends Model
{

    use SoftDeletes;

    protected $table = 'domicilios';

    protected $primaryKey = 'id';

    protected $fillable = ['direccion', 'codigo_postal', 'pais_id', 'provincia_id', 'localidad'];

    protected $dates = ['deleted_at'];

    public function __toString(){
        return $this->direccion .' - '. $this->localidad .' - '. $this->provincia .' - '. $this->pais;
    }

    public function pais(){
        return $this->belongsTo(Pais::class);
    }

    public function provincia(){
        return $this->belongsTo(Provincia::class);
    }

    public static function boot(){
        parent::boot();

        self::creating(function($model){ // Por si quieren crear un domicilio sin pais, le seteo Argentina por defecto
            if(!$model->pais_id){
                $model->pais_id = 1;
            }

            if(!$model->direccion){
                $model->direccion = 'Direccion';
            }
        });

    }
}
