<?php

namespace App\Http\Composers;

use App\Models\Backend\Configuraciones\Tawkto;
use Illuminate\View\View;

/**
 * Class GlobalComposer.
 */
class GlobalComposer
{
    /**
     * Bind data to the view.
     *
     * @param View $view
     *
     * @return void
     */
    public function compose(View $view){
        if(access()->user()){
            if(access()->user()->empresa){
                $tawkto = Tawkto::where('distribuidor_id',access()->user()->empresa->distribuidor_id)->first();
                if(!$tawkto){
                    $tawkto = Tawkto::where('distribuidor_id',1)->first();
                }
            }
            if(isset($tawkto)){
                $data = [
                    'user'  => access()->user(),
                    'tawkto'   => $tawkto
                ];
            } else {
                $data = [
                    'user'  => access()->user()
                ];
            }
            $view->with($data);
        }
    }
}
