<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Frontend\Configuraciones\CondicionIva;
use App\Models\Frontend\ECommerce\Suscripcion;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Session;


class LandingController extends Controller{

    public function index(){
        if(Auth::user()){
            $rol = Auth::user()->roles->first()->name;
            if($rol == 'Administrador' || $rol == 'Distribuidor'){
                return redirect('admin/dashboard');
            } else if($rol == 'Cliente') {
                return redirect('ecommerce/dashboard');
            } else {
                return redirect('dashboard');
            }
        } else {
            $condiciones_iva = CondicionIva::condicionesEmpresas();
            return view('landing.index', compact('condiciones_iva'));
        }
    }

    public function registro(){
        $condiciones_iva = CondicionIva::condicionesEmpresas();
        return view('landing.registro', compact('condiciones_iva'));
    }

    public function contacto(){
        return view('landing.contacto');
    }

    public function servicios(){
        return view('landing.servicios');
    }

    public function ecommerce(){
        return view('landing.ecommerce');
    }

    public function app(){
        return view('landing.app');
    }

    public function privacidad(){
        return view('landing.privacidad');
    }

    public function terminos(){
        return view('landing.terminos');
    }

    public function suscribirse(Request $request){
        $suscripcion = New Suscripcion();
        $suscripcion->empresa_id = 1;
        $suscripcion->email = $request->email;
        $suscripcion->save();
        return redirect('/');
    }

    public function enviarMail(Request $request){

        Mail::send('mails.contacto', ['request' => $request,], function ($mensaje) use ($request) {
            $mensaje->from('contacto@artisan.com.ar', 'CONTACTO ARTISAN');
            $mensaje->to('soporte@artisan.com.ar', 'Soporte Artisan');
            $mensaje->subject('Contacto de Artisan');
        });
        if (count(Mail::failures()) > 0) {
            Session::flash('flash_message', 'El mail no pudo ser enviado, por favor utilice el chat para contactarnos.');
            return redirect('landing\contacto');
        }


        Session::flash('flash_message', 'Hemos recibido tu mensaje, nos contactaremos a la brevedad.');
        return redirect('landing\contacto');

    }

}
