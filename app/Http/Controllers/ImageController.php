<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Image;

class ImageController extends Controller{



    public function resizeImage(){
        return view('resizeImage');
    }

    public function resizeImagePost(Request $request){//DEMO
        $this->validate($request, [
            'title' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $image = $request->file('image');
        $input['imagename'] = time().'.'.$image->getClientOriginalExtension();

        $destinationPath = public_path('/thumbnail');
        $img = Image::make($image->getRealPath());
        $img->resize(250, 250, function ($constraint) {
            $constraint->aspectRatio();
        })->save($destinationPath.'/'.$input['imagename']);

        $destinationPath = public_path('/images');
        $image->move($destinationPath, $input['imagename']);

        $this->postImage->add($input);

        return back()
            ->with('success','Imagen Actualizada Correctamente')
            ->with('imageName',$input['imagename']);
    }

    public function guardarImagen(Request $request,$url,$ancho,$alto){
        $image = $request->file('image');

        if(isset($image)){
            $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path($url);

            $img = Image::make($image->getRealPath());
            $img->resize($ancho, $alto, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath.'/'.$input['imagename']);

            return $url.$input['imagename'];
        }

        return false;
    }
//    public function uploadImages(Request $request,$url)
//    {
//        $image = $request->file('image');
//        if(isset($image)){
//            $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
//            $destinationPath = public_path($url);
//
//            $img = Image::make($image->getRealPath());
//            $img->save($destinationPath.'/'.$input['imagename']);
//
//            dd($request);
//            return response()->json($img);
//        }
//        return false;
//
//    }


    public function guardarImagenesDistribuidor(Request $request){
        $logo = $request->file('logo');
        $favicon = $request->file('favicon');

        $imagenes['logo'] = false;
        $imagenes['favicon'] = false;

        if(isset($logo)){
            $input['imagename'] = time().'.'.$logo->getClientOriginalExtension();
            $destinationPath = public_path('uploads/distribuidores');

            $img = Image::make($logo->getRealPath());
            $img->resize(250, 250, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath.'/'.$input['imagename']);

            $imagenes['logo'] = '/uploads/distribuidores/'.$input['imagename'];
        }

        if(isset($favicon)){
            $input['imagename'] = time().'.'.$favicon->getClientOriginalExtension();
            $destinationPath = public_path('distribuidores');

            $img = Image::make($favicon->getRealPath());
            $img->resize(250, 250, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath.'/'.$input['imagename']);

            $imagenes['favicon'] = '/uploads//distribuidores/'.$input['imagename'];
        }

        return $imagenes;
    }

}
