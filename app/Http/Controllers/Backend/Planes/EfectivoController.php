<?php

namespace App\Http\Controllers\Backend\Planes;

use App\Models\Backend\Planes\FormaPago;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;
use DB;

class EfectivoController extends Controller{

    public function index(){
        $efectivo = FormaPago::where('distribuidor_id',Auth::user()->distribuidor()->id)->where('tipo_forma_pago','Efectivo')->first();
        return view('backend.planes.efectivo.index', compact('efectivo'));
    }

    public function guardar(Request $request){
        $this->validate($request, [
            'instrucciones' => 'required'
		]);

        $requestData = $request->all();

        if(!isset($requestData['habilitado'])){
            $requestData['habilitado'] = 0;
        }

        if($request->id){
            $formaPago = FormaPago::findOrFail($request->id);
            $formaPago->update($requestData);
        } else {
            $nuevaFormaPago = new FormaPago($requestData);
            $nuevaFormaPago->distribuidor_id = Auth::user()->distribuidor()->id;
            $nuevaFormaPago->tipo_forma_pago = 'Efectivo';
            $nuevaFormaPago->save();
        }

        Session::flash('flash_message', 'Forma de pago guardada!');
        return redirect('admin/planes/efectivo');
    }
}
