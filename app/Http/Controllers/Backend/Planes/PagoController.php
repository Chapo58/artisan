<?php

namespace App\Http\Controllers\Backend\Planes;

use App\Models\Backend\Empresas\Empresa;
use App\Models\Frontend\Configuraciones\EstadoPago;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Models\Backend\Planes\Pago;
use Illuminate\Http\Request;
use Session;
use DB;

class PagoController extends Controller{

    public function index(Request $request){
        $keyword = $request->get('search');
        $perPage = 25;
        $usuario = Auth::user();
        $estadosPagos = EstadoPago::$getArray;

        if (!empty($keyword)) {
            $pagos = Pago::where('periodo', 'LIKE', "%$keyword%")
                ->where('distribuidor_id', $usuario->distribuidor()->id)
                ->paginate($perPage);
        } else {
            if ($usuario->hasRole('Administrador')) {
                $pagos = Pago::paginate($perPage);
            } else {
                $pagos = Pago::where('distribuidor_id', $usuario->distribuidor()->id)
                    ->paginate($perPage);
            }
        }

        return view('backend.planes.pago.index', compact('pagos','estadosPagos'));
    }

    public function create(){
        if (Auth::user()->hasRole('Administrador')) {
            return view('backend.planes.pago.create');
        }
    }

    public function store(Request $request){
        $this->validate($request, [
			'plan_id' => 'required'
		]);

        $requestData = $request->all();
        Plan::create($requestData);
        Session::flash('flash_message', trans('labels.frontend.pago').trans('alerts.frontend.created').'!');

        return redirect('admin/planes/pago');
    }

    public function show($id){
        $pago = Pago::findOrFail($id);
        $estadosPagos = EstadoPago::$getArray;
        $usuario = Auth::user();

        if(isset($pago->distribuidor) && $pago->distribuidor->id == $usuario->distribuidor()->id || $usuario->hasRole('Administrador')){
            return view('backend.planes.pago.show', compact('pago','estadosPagos'));
        } else {
            return redirect('admin/planes/pago');
        }

    }

    public function edit($id){
        $pago = Pago::findOrFail($id);
        if (Auth::user()->hasRole('Administrador')) {
            return view('backend.planes.pago.edit', compact('pago'));
        }
    }

    public function update($id, Request $request){
        $this->validate($request, [
			'estado' => 'required'
		]);

        $requestData = $request->all();
        $pago = Pago::findOrFail($id);

        $empresa = Empresa::findOrFail($pago->empresa->id);
        if($pago->estado == EstadoPago::ACEPTADO && $request->estado != EstadoPago::ACEPTADO){
            // Se cancela el pago y se cambia la fecha de vencimiento de la empresa hacia atras
            if($pago->periodo == 'Mensual'){
                $empresa->fecha_vencimiento = $empresa->fecha_vencimiento->modify('-1 months');
            } else {
                $empresa->fecha_vencimiento = $empresa->fecha_vencimiento->modify('-1 year');
            }
        } else if($pago->estado != EstadoPago::ACEPTADO && $request->estado == EstadoPago::ACEPTADO){
            // Si el pago es aceptado y antes no lo era
            if($pago->periodo == 'Mensual'){
                $empresa->fecha_vencimiento = $empresa->fecha_vencimiento->addMonths(1);
            } else {
                $empresa->fecha_vencimiento = $empresa->fecha_vencimiento->addYears(1);
            }

            if(!isset($empresa->plan) || $empresa->plan != $pago->plan){ // Si la empresa todavia no tiene un plan o tiene otro distinto, se le asigna este
                $empresa->plan_id = $pago->plan->id;
            }
        }
        $empresa->save();

        $pago->update($requestData);

        Session::flash('flash_message', trans('labels.frontend.pago').trans('alerts.frontend.updated').'!');

        return redirect('admin/planes/pago');
    }

    public function destroy($id){
        if (Auth::user()->hasRole('Administrador')) {
            $pago = Pago::findOrFail($id);
            $empresa = Empresa::findOrFail($pago->empresa->id);
            if($pago->estado == EstadoPago::ACEPTADO){
                // Se cancela el pago y se cambia la fecha de vencimiento de la empresa hacia atras
                if($pago->periodo == 'Mensual'){
                    $empresa->fecha_vencimiento = $empresa->fecha_vencimiento->modify('-1 months');
                } else {
                    $empresa->fecha_vencimiento = $empresa->fecha_vencimiento->modify('-1 year');
                }
                $empresa->save();
            }

            Pago::destroy($id);

            Session::flash('flash_message',  'Plan Eliminado!');

            return redirect('admin/planes/pago');
        }
    }

}
