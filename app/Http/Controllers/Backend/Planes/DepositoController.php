<?php

namespace App\Http\Controllers\Backend\Planes;

use App\Models\Backend\Planes\FormaPago;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;
use DB;

class DepositoController extends Controller{

    public function index(){
        $deposito = FormaPago::where('distribuidor_id',Auth::user()->distribuidor()->id)->where('tipo_forma_pago','Deposito')->first();
        return view('backend.planes.deposito.index', compact('deposito'));
    }

    public function guardar(Request $request){
        $this->validate($request, [
            'instrucciones' => 'required'
		]);

        $requestData = $request->all();

        if(!isset($requestData['habilitado'])){
            $requestData['habilitado'] = 0;
        }

        if($request->id){
            $formaPago = FormaPago::findOrFail($request->id);
            $formaPago->update($requestData);
        } else {
            $nuevaFormaPago = new FormaPago($requestData);
            $nuevaFormaPago->distribuidor_id = Auth::user()->distribuidor()->id;
            $nuevaFormaPago->tipo_forma_pago = 'Deposito';
            $nuevaFormaPago->save();
        }

        Session::flash('flash_message', 'Forma de pago guardada!');
        return redirect('admin/planes/deposito');
    }
}
