<?php

namespace App\Http\Controllers\Backend\Planes;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Models\Backend\Planes\Plan;
use Illuminate\Http\Request;
use Session;
use DB;

class PlanController extends Controller{

    public function index(Request $request){
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $planes = Plan::where('nombre', 'LIKE', "%$keyword%")
                ->paginate($perPage);
        } else {
            $planes = Plan::paginate($perPage);
        }

        return view('backend.planes.plan.index', compact('planes'));
    }

    public function create(){
        if (Auth::user()->hasRole('Administrador')) {
            return view('backend.planes.plan.create');
        } else {
            return redirect('admin/empresas/empresa');
        }
    }

    public function store(Request $request){
        $this->validate($request, [
			'nombre' => 'required'
		]);

        $requestData = $request->all();
        Plan::create($requestData);
        Session::flash('flash_message', trans('labels.frontend.plan').trans('alerts.frontend.created').'!');

        return redirect('admin/planes/plan');
    }

    public function show($id){
        $plan = Plan::findOrFail($id);

        return view('backend.planes.plan.show', compact('plan'));
    }

    public function edit($id){
        $plan = Plan::findOrFail($id);
        if (Auth::user()->hasRole('Administrador')) {
            return view('backend.planes.plan.edit', compact('plan'));
        } else {
            return redirect('admin/empresas/empresa');
        }
    }

    public function update($id, Request $request){
        $this->validate($request, [
			'nombre' => 'required'
		]);

        $requestData = $request->all();

        $plan = Plan::findOrFail($id);
        $plan->update($requestData);

        Session::flash('flash_message', trans('labels.frontend.plan').trans('alerts.frontend.updated').'!');

        return redirect('admin/planes/plan');
    }

    public function destroy($id){
        if (Auth::user()->hasRole('Administrador')) {
            Plan::destroy($id);

            Session::flash('flash_message',  'Plan Eliminado!');

            return redirect('admin/planes/plan');
        }
    }

}
