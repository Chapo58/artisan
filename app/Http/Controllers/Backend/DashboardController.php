<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Backend\Empresas\Empresa;
use App\Models\Backend\Planes\Pago;
use App\Models\Frontend\Configuraciones\EstadoPago;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use DB;

class DashboardController extends Controller{

    public function index(){
        $usuario = Auth::user();

        if (Auth::user()->hasRole('Administrador')) {
            $empresasActivas = Empresa::where('fecha_vencimiento','>=',Carbon::now())->count();
            $empresasVencidas = Empresa::where('fecha_vencimiento','<',Carbon::now())->count();
            $pagosPendientes = Pago::where('estado',EstadoPago::PENDIENTE)->count();
            $ingresoMensual = Pago::where('estado',EstadoPago::ACEPTADO)->sum('importe');
        } else {
            $empresasActivas = Empresa::where('distribuidor_id',$usuario->id)->where('fecha_vencimiento','>=',Carbon::now())->count();
            $empresasVencidas = Empresa::where('distribuidor_id',$usuario->id)->where('fecha_vencimiento','<',Carbon::now())->count();
            $pagosPendientes = Pago::where('distribuidor_id',$usuario->id)->where('estado',EstadoPago::PENDIENTE)->count();
            $ingresoMensual = Pago::where('distribuidor_id',$usuario->id)->where('estado',EstadoPago::ACEPTADO)->sum('importe');
        }


        return view('backend.dashboard', compact('empresasActivas','empresasVencidas','pagosPendientes','ingresoMensual'));
    }
}
