<?php

namespace App\Http\Controllers\Backend\Empresas;

use App\Http\Controllers\Controller;
use App\Models\Backend\Empresas\Empresa;
use App\Models\Backend\Permisos\Modulo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;

class PermisoEmpresaController extends Controller{

    public function index($id_empresa){
        $empresa = Empresa::findOrFail($id_empresa);
        $modulos = Modulo::getModulos();

        return view('backend.empresas.empresa.permisos', compact('empresa','modulos'));
    }

    public function guardar(Request $request){
        $requestData = $request->all();
        $empresa = Empresa::findOrFail($requestData['empresa_id']);

        if(isset($requestData['permisos'])) {
            $empresa->permisos()->detach();
            $permisos = $requestData['permisos'];
            foreach ($permisos as $permiso) {
                $empresa->permisos()->attach($permiso);
            }
        }

        Session::flash('flash_message', 'Permisos de la empresa actualizados!');

        return redirect('admin/empresas/empresa');
    }

}
