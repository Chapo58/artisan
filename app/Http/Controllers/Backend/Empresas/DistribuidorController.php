<?php

namespace App\Http\Controllers\Backend\Empresas;

use App\Http\Controllers\Frontend\Configuraciones\UsuarioController;
use App\Http\Controllers\ImageController;
use App\Models\Frontend\Configuraciones\CondicionIva;
use App\Models\Frontend\Configuraciones\Estado;
use App\Models\Frontend\Configuraciones\Localidad;
use App\Models\Frontend\Configuraciones\Provincia;
use App\Models\Frontend\Personas\Domicilio;
use App\Http\Controllers\Controller;
use App\Models\Backend\Empresas\Distribuidor;
use App\Repositories\Backend\Access\Role\RoleRepository;
use App\Repositories\Backend\Access\User\UserRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Session;
use DB;

class DistribuidorController extends Controller{

    protected $users;
    protected $roles;

    public function __construct(UserRepository $users, RoleRepository $roles){
        $this->users = $users;
        $this->roles = $roles;
    }

    public function index(Request $request){
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $distribuidores = Distribuidor::where('razon_social', 'LIKE', "%$keyword%")
                ->paginate($perPage);
        } else {
            $distribuidores = Distribuidor::paginate($perPage);
        }

        if (Auth::user()->hasRole('Administrador')) {
            return view('backend.empresas.distribuidor.index', compact('distribuidores'));
        } else {
            return $this->edit(Auth::user()->distribuidor()->id);
        }

    }

    public function create(){
        $condiciones_iva = ['' => 'Seleccione una opción...'] + CondicionIva::pluck('nombre', 'id')->all();
        $provincias = ['' => 'Seleccione una opción...'] + Provincia::orderBy('nombre')->pluck('nombre', 'id')->all();

        if (Auth::user()->hasRole('Administrador')) {
            return view('backend.empresas.distribuidor.create', compact('condiciones_iva','provincias'));
        }
    }

    public function store(Request $request){
        $this->validate($request, [
			'razon_social' => 'required',
			'condicion_iva_id' => 'required'
		]);

        DB::transaction(function() use ($request) {
            $requestData = $request->all();

            $domicilio = new Domicilio($requestData['domicilio']);
            $domicilio->save();

            $distribuidor = new Distribuidor($requestData);
            $distribuidor->domicilio_id = $domicilio->id;

            $controllerImagen = new ImageController();
            $imagenes_url = $controllerImagen->guardarImagenesDistribuidor($request);
            if($imagenes_url['logo'] != false){
                $distribuidor->logo_url = $imagenes_url['logo'];
            }
            if($imagenes_url['favicon'] != false){
                $distribuidor->favicon_url = $imagenes_url['favicon'];
            }

            $assignees_roles[2] = '2';
            $parametrosDatos = [
                'name' => $request->razon_social,
                'email' => $request->admin_email,
                'password' => $request->admin_password,
                'password_confirmation' => $request->admin_password,
                'status' => Estado::ACTIVO,
                'confirmed' => TRUE
            ];
            $parametrosRoles = [
                'assignees_roles' => $assignees_roles
            ];
            $nuevoUsuario = $this->users->create(['data' => $parametrosDatos, 'roles' => $parametrosRoles]);
            $distribuidor->usuario_id = $nuevoUsuario->id;

            $distribuidor->save();

            Session::flash('flash_message', trans('labels.frontend.distribuidor').trans('alerts.frontend.created').'!');
        });
        return redirect('admin/empresas/distribuidor');
    }

    public function show($id){
        $distribuidor = Distribuidor::findOrFail($id);

        if (Auth::user()->hasRole('Administrador')) {
            return view('backend.empresas.distribuidor.show', compact('distribuidor'));
        } else {
            return redirect('admin/empresas/empresa');
        }
    }

    public function edit($id){
        $distribuidor = Distribuidor::findOrFail($id);
        $condiciones_iva = ['' => 'Seleccione una opción...'] + CondicionIva::pluck('nombre', 'id')->all();
        $provincias = ['' => 'Seleccione una opción...'] + Provincia::orderBy('nombre')->pluck('nombre', 'id')->all();
        $localidades = ['' => 'Seleccione una opción...'] + Localidad::orderBy('nombre')->pluck('nombre', 'id')->all();
        if (Auth::user()->hasRole('Administrador')) {
            return view('backend.empresas.distribuidor.edit', compact('distribuidor','condiciones_iva','provincias','localidades'));
        } else {
            if(Auth::user()->distribuidor()->id == $id){
                return view('backend.empresas.distribuidor.edit', compact('distribuidor','condiciones_iva','provincias','localidades'));
            }
        }
    }

    public function update($id, Request $request){
        $this->validate($request, [
            'razon_social' => 'required',
            'condicion_iva_id' => 'required'
		]);

        DB::transaction(function() use ($id, $request) {
            $requestData = $request->all();

            $distribuidor = Distribuidor::findOrFail($id);
            $distribuidor->razon_social = $requestData['razon_social'];
            $distribuidor->cuit = $requestData['cuit'];
            $distribuidor->email = $requestData['email'];
            $distribuidor->telefono = $requestData['telefono'];
            $distribuidor->web = $requestData['web'];
            $distribuidor->condicion_iva_id = $requestData['condicion_iva_id'];

            if(!isset($distribuidor->domicilio)){
                $domicilio = new Domicilio($requestData['domicilio']);
                $domicilio->save();
                $distribuidor->domicilio_id = $domicilio->id;
            }else{
                $distribuidor->domicilio->update($requestData['domicilio']);
            }

            $controllerImagen = new ImageController();
            $imagenes_url = $controllerImagen->guardarImagenesDistribuidor($request);
            if($imagenes_url['logo'] != false){
                $distribuidor->logo_url = $imagenes_url['logo'];
            }
            if($imagenes_url['favicon'] != false){
                $distribuidor->favicon_url = $imagenes_url['favicon'];
            }

            $distribuidor->update();

            Session::flash('flash_message', trans('labels.frontend.distribuidor').trans('alerts.frontend.updated').'!');
        });
        return redirect('admin/empresas/distribuidor');
    }

    public function destroy($id){
        if (Auth::user()->hasRole('Administrador')) {
            Distribuidor::destroy($id);

            Session::flash('flash_message',  'Distribuidor Eliminado!');

            return redirect('admin/empresas/distribuidor');
        }
    }

}
