<?php

namespace App\Http\Controllers\Backend\Empresas;

use App\Http\Controllers\Frontend\Fondos\ChequeController;
use App\Http\Controllers\ImageController;
use App\Models\Backend\Empresas\Distribuidor;
use App\Models\Backend\Permisos\Permiso;
use App\Models\Backend\Planes\FormaPago;
use App\Models\Backend\Planes\Plan;
use App\Models\Frontend\Caja\ConceptoDeCaja;
use App\Models\Frontend\Caja\ConceptoPersonalizado;
use App\Models\Frontend\Caja\MovimientoCaja;
use App\Models\Frontend\Calendario\Event;
use App\Models\Frontend\Compras\Compra;
use App\Models\Frontend\Compras\OrdenPago;
use App\Models\Frontend\Configuraciones\ActividadAfip;
use App\Models\Frontend\Configuraciones\Historial;
use App\Models\Frontend\Configuraciones\Pais;
use App\Models\Frontend\Configuraciones\Provincia;
use App\Models\Frontend\Configuraciones\Tarjeta;
use App\Models\Frontend\Configuraciones\TarjetaPropia;
use App\Models\Frontend\Configuraciones\TipoComprobante;
use App\Models\Frontend\Fondos\Cheque;
use App\Models\Frontend\Fondos\ChequePropio;
use App\Models\Frontend\Fondos\Chequera;
use App\Models\Frontend\Fondos\CuentaBancaria;
use App\Models\Frontend\Fondos\EstadoCheque;
use App\Models\Frontend\Fondos\GastoBancario;
use App\Models\Frontend\Fondos\MovimientoBancario;
use App\Models\Frontend\Ventas\DetalleVenta;
use App\Models\Frontend\Ventas\NotaCredito;
use App\Models\Frontend\Ventas\NotaDebito;
use App\Models\Frontend\Ventas\Presupuesto;
use App\Models\Frontend\Ventas\Recibo;
use App\Models\Frontend\Ventas\Venta;
use Carbon\Carbon;
use DateTime;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Frontend\Configuraciones\UsuarioController;
use App\Models\Access\User\User;
use App\Models\Backend\Empresas\Empresa;
use App\Models\Backend\Empresas\Sucursal;
use App\Models\Frontend\Articulos\Articulo;
use App\Models\Frontend\Articulos\DetalleArticulo;
use App\Models\Frontend\Articulos\Marca;
use App\Models\Frontend\Articulos\Rubro;
use App\Models\Frontend\Articulos\Subrubro;
use App\Models\Frontend\Caja\Caja;
use App\Models\Frontend\Caja\EstadoCaja;
use App\Models\Frontend\Configuraciones\CondicionIva;
use App\Models\Frontend\Configuraciones\Estado;
use App\Models\Frontend\Configuraciones\Moneda;
use App\Models\Frontend\Configuraciones\Opcion;
use App\Models\Frontend\Configuraciones\PuntoDeVenta;
use App\Models\Frontend\Fondos\Banco;
use App\Models\Frontend\Personas\Cliente;
use App\Models\Frontend\Personas\Domicilio;
use App\Models\Frontend\Personas\Empleado;
use App\Models\Frontend\Personas\Proveedor;
use App\Models\Frontend\Ventas\ListaPrecio;
use App\Models\Frontend\Ventas\Precio;
use App\Repositories\Backend\Access\Role\RoleRepository;
use App\Repositories\Backend\Access\User\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Session;
use DB;

class EmpresaController extends Controller{

    public function index(Request $request){
        $keyword = $request->get('search');
        $perPage = 25;

        $distribuidor = Auth::user()->distribuidor();

        $empresas = Empresa::listarEmpresasPorDistribuidor($distribuidor, $perPage, $keyword);

        return view('backend.empresas.empresa.index', compact('empresas'));
    }

    public function create(){
        $condiciones_iva = ['' => 'Seleccione una opción...'] + CondicionIva::pluck('nombre', 'id')->all();
        $paises = ['' => 'Seleccione una opción...'] + Pais::pluck('nombre', 'id')->all();
        $provincias = ['' => 'Seleccione una opción...'] + Provincia::orderBy('nombre')->pluck('nombre', 'id')->all();
        $planes = ['' => 'Seleccione una opción...'] + Plan::orderBy('precio','ASC')->pluck('nombre', 'id')->all();
        $distribuidores = Distribuidor::getDistrivuidoresArray();

        return view('backend.empresas.empresa.create', compact('condiciones_iva', 'paises', 'provincias','planes','distribuidores'));
    }

    public function store(Request $request){
        $this->validate($request, [
			'razon_social' => 'required',
			'condicion_iva_id' => 'required'
		]);

        DB::transaction(function() use ($request) {
            $requestData = $request->all();

            $domicilio = new Domicilio($requestData['domicilio']);
            $domicilio->save();

            $empresa = new Empresa($requestData);
            if(!isset($requestData['distribuidor_id'])){
                $empresa->distribuidor_id = Auth::user()->distribuidor()->id;
            }
            $empresa->domicilio_id = $domicilio->id;
            $empresa->fecha_vencimiento = Carbon::now()->addMonths(1);
            if(isset($requestData['inicio_actividad'])){
                $date = DateTime::createFromFormat('d/m/Y', $requestData['inicio_actividad']);
                $empresa->inicio_actividad = $date->format('Y-m-d H:i:s');
            }

            $controllerImagen = new ImageController();
            $imagen_url = $controllerImagen->guardarImagen($request,'/uploads/empresas/',480,200);
            if($imagen_url != false){
                $empresa->imagen_url = $imagen_url;
            }
            $empresa->save();

            //  Creo todos los registros por defecto de la empresa
            $monedaLocal = EmpresaController::seedingNuevasEmpresas($empresa);

            $cuenta = 0;
            if(isset($requestData['detalles'])) {
                $detalles = $requestData['detalles'];
                foreach ($detalles as $detalle) {
                    if((int)$detalle['estado'] == Estado::ACTIVO && !empty($detalle['nombre'])){
                        $cuenta++;

                        $sucursal = new Sucursal($detalle);
                        $sucursal->empresa_id = $empresa->id;
                        $sucursal->email = $empresa->email;
                        $sucursal->telefono = $empresa->telefono;
                        $sucursal->save();

                        $caja = new Caja();
                        $caja->numero = $cuenta;
                        $caja->nombre = 'Caja Principal';
                        $caja->moneda_id = $monedaLocal->id;
                        $caja->empresa_id = $empresa->id;
                        $caja->sucursal_id = $sucursal->id;
                        $caja->estado = EstadoCaja::CERRADA;
                        $caja->save();

                        $puntoVenta = new PuntoDeVenta();
                        $puntoVenta->sucursal_id = $sucursal->id;
                        $puntoVenta->empresa_id = $empresa->id;
                        $puntoVenta->tipo_punto_id = 2;
                        $puntoVenta->numero = str_pad($cuenta, 4, '0', STR_PAD_LEFT);
                        $puntoVenta->save();

                        $puntoVenta = new PuntoDeVenta();
                        $puntoVenta->sucursal_id = $sucursal->id;
                        $puntoVenta->empresa_id = $empresa->id;
                        $puntoVenta->tipo_punto_id = 2;
                        $puntoVenta->numero = '9999';
                        $puntoVenta->save();
                    }
                }
            }

            if($cuenta == 0){
                $sucursal = new Sucursal();
                $sucursal->nombre = 'Casa Central';
                $sucursal->empresa_id = $empresa->id;
                $sucursal->email = $empresa->email;
                $sucursal->telefono = $empresa->telefono;
                $sucursal->save();

                $caja = new Caja();
                $caja->numero = $cuenta;
                $caja->nombre = 'Caja Principal';
                $caja->moneda_id = $monedaLocal->id;
                $caja->empresa_id = $empresa->id;
                $caja->sucursal_id = $sucursal->id;
                $caja->estado = EstadoCaja::CERRADA;
                $caja->save();

                $puntoVenta = new PuntoDeVenta();
                $puntoVenta->sucursal_id = $sucursal->id;
                $puntoVenta->empresa_id = $empresa->id;
                $puntoVenta->tipo_punto_id = 2;
                $puntoVenta->numero = str_pad($cuenta, 4, '0', STR_PAD_LEFT);
                $puntoVenta->save();

                $puntoVenta = new PuntoDeVenta();
                $puntoVenta->sucursal_id = $sucursal->id;
                $puntoVenta->empresa_id = $empresa->id;
                $puntoVenta->tipo_punto_id = 2;
                $puntoVenta->numero = '9999';
                $puntoVenta->save();
            }

            $banco = Banco::where('empresa_id',$empresa->id)->first();
            $cheque = new Cheque();
            $cheque->banco_id = $banco->id;
            $cheque->importe = 1000;
            $cheque->numero = '01234567';
            $cheque->firmante = 'Nombre Apellido';
            $cheque->vencimiento = Carbon::now()->addWeek();
            $cheque->estado = EstadoCheque::COBRADO;
            $cheque->caja_id = $caja->id;
            $cheque->empresa_id = $empresa->id;
            $cheque->save();

            $cliente = Cliente::where('empresa_id',$empresa->id)->first();
            $listaPrecio = ListaPrecio::where('empresa_id',$empresa->id)->where('nombre','Minorista')->first();
            $venta = new Venta();
            $venta->empresa_id = $empresa->id;
            $venta->cliente_id = $cliente->id;
            $venta->moneda_id = $monedaLocal->id;
            $venta->cotizacion = 1;
            $venta->fecha = Carbon::now();
            $venta->numero = '00000001';
            $venta->importe_neto = 157.3;
            $venta->importe_iva = 33.03;
            $venta->total = 190.33;
            $venta->estado = Estado::ACTIVO;
            $venta->punto_venta_id = $puntoVenta->id;
            $venta->tipo_comprobante_id = TipoComprobante::FACTURA_B;
            $venta->lista_precios_id = $listaPrecio->id;
            $venta->sucursal_id = $sucursal->id;
            $venta->total_cobro = 190.33;
            $venta->save();

            $articulo = Articulo::where('empresa_id',$empresa->id)->first();
            $detalleVenta = New DetalleVenta();
            $detalleVenta->venta_id = $venta->id;
            $detalleVenta->articulo_id = $articulo->id;
            $detalleVenta->precio_neto = 190.33;
            $detalleVenta->precio_iva = 0;
            $detalleVenta->cantidad = 2;
            $detalleVenta->estado = Estado::ACTIVO;
            $detalleVenta->save();

            $empleado = new Empleado();
            $empleado->empresa_id = $empresa->id;
            $empleado->sucursal_id = $sucursal->id;
            $empleado->caja_id = $caja->id;
            $empleado->punto_venta_id = $puntoVenta->id;
            $empleado->nombre = 'Usuario Admin';
            $empleado->cuil = '22-22222222-2';
            $empleado->save();

            $roles = new RoleRepository();
            $users = new UserRepository($roles);
            $usuarioController = new UsuarioController($users, $roles);
            $usuarioController->nuevoUsuario('Usuario Admin', $requestData['admin_email'], $requestData['admin_password'], $requestData['admin_password'], $empresa, $sucursal, $empleado, 'Usuario Admin');

            Session::flash('flash_message', trans('labels.frontend.empresa').trans('alerts.frontend.created').'!');
        });

        Mail::send('mails.welcome', ['request' => $request,], function ($mensaje) use ($request) {
            $mensaje->from('noreply@artisan.com.ar', '¡Bienvenido a Artisan!');
            $mensaje->to($request->admin_email, $request->razon_social);
            $mensaje->subject('¡Bienvenido a Artisan!');
        });

        return redirect('admin/empresas/empresa');
    }

    public function show($id){
        $empresa = Empresa::findOrFail($id);

        if ($empresa->distribuidor && $empresa->distribuidor->id == Auth::user()->distribuidor()->id || Auth::user()->hasRole('Administrador')) {
            $usuarios = User::where('users.empresa_id', '=', $empresa->id)
                ->join('role_user', 'role_user.user_id', '=', 'users.id')
                ->join('roles', 'roles.id', '=', 'role_user.role_id')
                ->select('users.*', 'roles.name as rol_string')
                ->get();

            return view('backend.empresas.empresa.show', compact('empresa', 'usuarios'));
        }
        return redirect('admin/empresas/empresa');
    }

    public function edit($id){
        $empresa = Empresa::findOrFail($id);

        if ($empresa->distribuidor && $empresa->distribuidor->id == Auth::user()->distribuidor()->id || Auth::user()->hasRole('Administrador')) {
            $condiciones_iva = ['' => 'Seleccione una opción...'] + CondicionIva::pluck('nombre', 'id')->all();
            $paises = ['' => 'Seleccione una opción...'] + Pais::pluck('nombre', 'id')->all();
            $provincias = ['' => 'Seleccione una opción...'] + Provincia::orderBy('nombre')->pluck('nombre', 'id')->all();
            $planes = ['' => 'Seleccione una opción...'] + Plan::orderBy('precio','ASC')->pluck('nombre', 'id')->all();
            $distribuidores = Distribuidor::getDistrivuidoresArray();

            return view('backend.empresas.empresa.edit', compact('empresa', 'condiciones_iva', 'paises', 'provincias','planes','distribuidores'));
        }
        return redirect('admin/empresas/empresa');
    }

    public function update($id, Request $request){
        $this->validate($request, [
			'razon_social' => 'required'
		]);

        DB::transaction(function() use ($id, $request) {
            $requestData = $request->all();

            $empresa = Empresa::findOrFail($id);
            $empresa->razon_social = $requestData['razon_social'];
            $empresa->nombre = $requestData['nombre'];
            $empresa->cuit = $requestData['cuit'];
            $empresa->email = $requestData['email'];
            $empresa->telefono = $requestData['telefono'];
            $empresa->web = $requestData['web'];
            $empresa->ingresos_brutos = $requestData['ingresos_brutos'];
            $empresa->condicion_iva_id = $requestData['condicion_iva_id'];
            $empresa->plan_id = $requestData['plan_id'];
            if(isset($requestData['inicio_actividad'])){
                $date = DateTime::createFromFormat('d/m/Y', $requestData['inicio_actividad']);
                $empresa->inicio_actividad = $date->format('Y-m-d H:i:s');
            }
            if(isset($requestData['fecha_vencimiento'])){
                $empresa->fecha_vencimiento = $requestData['fecha_vencimiento'];
            }
            if($requestData['distribuidor_id']){
                $empresa->distribuidor_id = $requestData['distribuidor_id'];
            }

            if(!isset($empresa->domicilio)){
                $domicilio = new Domicilio($requestData['domicilio']);
                $domicilio->save();
                $empresa->domicilio_id = $domicilio->id;
            }else{
                $empresa->domicilio->update($requestData['domicilio']);
            }

            $controllerImagen = new ImageController();
            $imagen_url = $controllerImagen->guardarImagen($request,'/uploads/empresas/',480,200);
            if($imagen_url != false){
                $empresa->imagen_url = $imagen_url;
            }

            $empresa->update();

            $cuenta = 0;
            if(isset($requestData['detalles'])) {
                $detalles = $requestData['detalles'];
                foreach ($detalles as $detalle) {
                    if((int)$detalle['estado'] == Estado::ACTIVO){
                        $cuenta++;

                        $sucursal = new Sucursal($detalle);
                        $sucursal->empresa_id = $empresa->id;
                        $sucursal->save();

                        $monedaLocal = Moneda::where('empresa_id', '=', $empresa->id)
                            ->whereNull('deleted_at')
                            ->where('cotizacion', '=', 1)
                            ->first();

                        $caja = new Caja();
                        $caja->numero = $cuenta;
                        $caja->nombre = 'Caja Principal';
                        $caja->moneda_id = $monedaLocal->id;
                        $caja->empresa_id = $empresa->id;
                        $caja->sucursal_id = $sucursal->id;
                        $caja->estado = EstadoCaja::CERRADA;
                        $caja->save();

                        $puntoVenta = new PuntoDeVenta();
                        $puntoVenta->sucursal_id = $sucursal->id;
                        $puntoVenta->empresa_id = $empresa->id;
                        $puntoVenta->tipo_punto_id = 2;
                        $puntoVenta->numero = str_pad($cuenta, 4, '0', STR_PAD_LEFT);
                        $puntoVenta->save();

                        $puntoVenta = new PuntoDeVenta();
                        $puntoVenta->sucursal_id = $sucursal->id;
                        $puntoVenta->empresa_id = $empresa->id;
                        $puntoVenta->tipo_punto_id = 2;
                        $puntoVenta->numero = '9999';
                        $puntoVenta->save();
                    }
                }
            }

            Session::flash('flash_message', trans('labels.frontend.empresa').trans('alerts.frontend.updated').'!');
        });

        return redirect('admin/empresas/empresa');
    }

    public function destroy($id){
        DB::transaction(function() use ($id) {
            $empresa = Empresa::findOrFail($id);
            if(isset($empresa->domicilio)){
                $empresa->domicilio->delete();
            }
            $empresa->delete();

            $opciones = Opcion::where('empresa_id', $empresa->id)->wherenull('deleted_at')->get();
            foreach ($opciones as $opcion){
                $opcion->delete();
            }

            $bancos = Banco::getBancosPorEmpresa($empresa);
            foreach ($bancos as $banco){
                $banco->delete();
            }

            $monedas = Moneda::getMonedasPorEmpresa($empresa);
            foreach ($monedas as $moneda){
                $moneda->delete();
            }

            $proveedores = Proveedor::where('empresa_id', $empresa->id)->wherenull('deleted_at')->get();
            foreach ($proveedores as $proveedor){
                $proveedor->delete();
                if(isset($proveedor->domicilio)){
                    $proveedor->domicilio->delete();
                }
                if(isset($proveedor->cuentasCorrientes)){
                    foreach ($proveedor->cuentasCorrientes as $cuentas) {
                        $cuentas->delete();
                    }
                }
            }

            $clientes = Cliente::where('empresa_id', $empresa->id)->wherenull('deleted_at')->get();
            foreach ($clientes as $cliente){
                $cliente->delete();
                if(isset($cliente->domicilio)){
                    $cliente->domicilio->delete();
                }
                if(isset($cliente->cuentasCorrientes)){
                    foreach ($cliente->cuentasCorrientes as $cuentas) {
                        $cuentas->delete();
                    }
                }
            }

            $articulos = Articulo::where('empresa_id', $empresa->id)->wherenull('deleted_at')->get();
            foreach ($articulos as $articulo){
                $articulo->delete();
                if(isset($articulo->detalleArticulos)){
                    foreach ($articulo->detalleArticulos as $detalleArticulo) {
                        $detalleArticulo->delete();
                    }
                }
                if(isset($articulo->stocks)){
                    foreach ($articulo->stocks as $stock) {
                        $stock->delete();
                        if(isset($stock->movimientosStock)){
                            foreach ($stock->movimientosStock as $movimiento) {
                                $movimiento->delete();
                            }
                        }
                    }
                }
            }

            $rubros = Rubro::getRubrosPorEmpresa($empresa);
            foreach ($rubros as $rubro){
                $rubro->delete();
            }

            $subrubros = Subrubro::getSubrubrosPorEmpresa($empresa);
            foreach ($subrubros as $subrubro){
                $subrubro->delete();
            }

            $marcas = Marca::getMarcasPorEmpresa($empresa);
            foreach ($marcas as $marca){
                $marca->delete();
            }

            $listasPrecios = ListaPrecio::getListasPorEmpresa($empresa);
            foreach ($listasPrecios as $lista){
                $lista->delete();
                if(isset($lista->precios)){
                    foreach ($lista->precios as $precio) {
                        $precio->delete();
                    }
                }
            }

            $empleados = Empleado::where('empresa_id', $empresa->id)->wherenull('deleted_at')->get();
            foreach ($empleados as $empleado){
                $empleado->delete();
                if(isset($empleado->domicilio)){
                    $empleado->domicilio->delete();
                }
            }

            $usuarios = User::where('empresa_id', $empresa->id)->wherenull('deleted_at')->get();
            foreach ($usuarios as $usuario){
                $usuario->email = 'Emp:'.$empresa->id." Email ".$usuario->email;
                $usuario->update();
                $usuario->delete();
            }

            $sucursales = Sucursal::getSucursalesPorEmpresa($empresa);
            foreach ($sucursales as $sucursal){
                $sucursal->delete();
            }

            $cajas = Caja::getCajasPorEmpresa($empresa);
            foreach ($cajas as $caja){
                $caja->delete();
                if(isset($caja->movimientoCaja)){
                    foreach ($caja->movimientoCaja as $movimiento) {
                        $movimiento->delete();
                    }
                }
                if(isset($caja->cierresCajas)){
                    foreach ($caja->cierresCajas as $cierre) {
                        $cierre->delete();
                    }
                }
            }

            $puntosVentas = PuntoDeVenta::where('empresa_id', $empresa->id)->wherenull('deleted_at')->get();
            foreach ($puntosVentas as $puntoVenta){
                $puntoVenta->delete();
            }

            $actividadesAfip = ActividadAfip::where('empresa_id', $empresa->id)->wherenull('deleted_at')->get();
            foreach ($actividadesAfip as $actividad){
                $actividad->delete();
            }

            $chequeras = Chequera::getChequerasPorEmpresa($empresa);
            foreach ($chequeras as $chequera){
                $chequera->delete();
            }

            $cheques = Cheque::getChequesPorEmpresa($empresa);
            foreach ($cheques as $cheque){
                $cheque->delete();
            }

            $chequesPropios = ChequePropio::where('empresa_id', $empresa->id)->wherenull('deleted_at')->get();
            foreach ($chequesPropios as $chequePropio){
                $chequePropio->delete();
            }

            $conceptosPersonalizados = ConceptoPersonalizado::getConceptosPersonalizadosPorEmpresa($empresa);
            foreach ($conceptosPersonalizados as $conceptoPersonalizado){
                $conceptoPersonalizado->delete();
            }

            $cuentasBancarias = CuentaBancaria::getCuentasBancariasPorEmpresa($empresa);
            foreach ($cuentasBancarias as $cuenta){
                $cuenta->delete();
            }

            $tarjetas = Tarjeta::getTarjetasPorEmpresa($empresa);
            foreach ($tarjetas as $tarjeta){
                $tarjeta->delete();
                if(isset($tarjeta->cuponesTarjetas)){
                    foreach ($tarjeta->cuponesTarjetas as $cupones) {
                        $cupones->delete();
                    }
                }
            }

            $tarjetasPropias = TarjetaPropia::getTarjetasPorEmpresa($empresa);
            foreach ($tarjetasPropias as $tarjetaPropia){
                $tarjetaPropia->delete();
            }

            $eventos = Event::where('empresa_id', $empresa->id)->wherenull('deleted_at')->get();
            foreach ($eventos as $evento){
                $evento->delete();
            }

            $gastosBancarios = GastoBancario::where('empresa_id', $empresa->id)->wherenull('deleted_at')->get();
            foreach ($gastosBancarios as $gasto){
                $gasto->delete();
            }

            $historiales = Historial::where('empresa_id', $empresa->id)->wherenull('deleted_at')->get();
            foreach ($historiales as $historial){
                $historial->delete();
            }

            $movimientosBancarios = MovimientoBancario::getMovimientosBancariosPorEmpresa($empresa);
            foreach ($movimientosBancarios as $movimientoBancario){
                $movimientoBancario->delete();
            }

            $movimientosCajas = MovimientoCaja::where('empresa_id', $empresa->id)->wherenull('deleted_at')->get();
            foreach ($movimientosCajas as $movimientoCaja){
                $movimientoCaja->delete();
            }

            $recibos = Recibo::getRecibosPorEmpresa($empresa);
            foreach ($recibos as $recibo){
                $recibo->delete();
                if(isset($recibo->detalles)){
                    foreach ($recibo->detalles as $detalle) {
                        $detalle->delete();
                    }
                }
                if(isset($recibo->detallesCobros)){
                    foreach ($recibo->detallesCobros as $detalleCobro) {
                        $detalleCobro->delete();
                    }
                }
            }

            $ordenesDePagos = OrdenPago::getOrdenesPorEmpresa($empresa);
            foreach ($ordenesDePagos as $ordenDePago){
                $ordenDePago->delete();
                if(isset($ordenDePago->detalles)){
                    foreach ($ordenDePago->detalles as $detalle) {
                        $detalle->delete();
                    }
                }
                if(isset($recibo->detallesPagos)){
                    foreach ($recibo->detallesPagos as $detallePago) {
                        $detallePago->delete();
                    }
                }
            }

            $presupuestos = Presupuesto::where('empresa_id', $empresa->id)->wherenull('deleted_at')->get();
            foreach ($presupuestos as $presupuesto){
                $presupuesto->delete();
                if(isset($presupuesto->detalles)){
                    foreach ($presupuesto->detalles as $detalle) {
                        $detalle->delete();
                    }
                }
            }

            $notasDebitos = NotaDebito::where('empresa_id', $empresa->id)->wherenull('deleted_at')->get();
            foreach ($notasDebitos as $notaDebito){
                $notaDebito->delete();
                if(isset($notaDebito->detalles)){
                    foreach ($ordenDePago->detalles as $detalle) {
                        $detalle->delete();
                    }
                }
                if(isset($notaDebito->detallesCobros)){
                    foreach ($notaDebito->detallesCobros as $detalleCobro) {
                        $detalleCobro->delete();
                    }
                }
            }

            $notasCreditos = NotaCredito::where('empresa_id', $empresa->id)->wherenull('deleted_at')->get();
            foreach ($notasCreditos as $notaCredito){
                $notaCredito->delete();
                if(isset($notaCredito->detalles)){
                    foreach ($notaCredito->detalles as $detalle) {
                        $detalle->delete();
                    }
                }
                if(isset($notaCredito->detallesPagos)){
                    foreach ($notaCredito->detallesPagos as $detallePago) {
                        $detallePago->delete();
                    }
                }
            }

            $compras = Compra::getComprasPorEmpresa($empresa);
            foreach ($compras as $compra){
                $compra->delete();
                if(isset($compra->detalles)){
                    foreach ($compra->detalles as $detalle) {
                        $detalle->delete();
                    }
                }
                if(isset($compra->detallesPagos)){
                    foreach ($compra->detallesPagos as $detallePago) {
                        $detallePago->delete();
                    }
                }
            }

            $ventas = Venta::where('empresa_id', $empresa->id)->wherenull('deleted_at')->get();
            foreach ($ventas as $venta){
                $venta->delete();
                if(isset($venta->detalles)){
                    foreach ($venta->detalles as $detalle) {
                        $detalle->delete();
                    }
                }
                if(isset($venta->detallesCobros)){
                    foreach ($venta->detallesCobros as $detalleCobro) {
                        $detalleCobro->delete();
                    }
                }
            }

            Session::flash('flash_message',  trans('labels.frontend.empresa').trans('alerts.frontend.deleted').'!');
        });

        return redirect('admin/empresas/empresa');
    }

    public function actualizarEmpresa($id){
        DB::transaction(function() use ($id) {
            $empresa = Empresa::findOrFail($id);

            $contadorActualizaciones = 0;

            //Comprobar Opciones

            if($empresa->condicionIva->id == CondicionIva::RESPONSABLE_INSCRIPTO){
                $opcionGuardada = Opcion::where('empresa_id', $empresa->id)->where('nombre', 'numeracion_factura_a')->first();
                if (!isset($opcionGuardada)) {
                    $opcion = new Opcion();
                    $opcion->empresa_id = $empresa->id;
                    $opcion->nombre = 'numeracion_factura_a';
                    $opcion->presentacion = 'Numeración de Factura A';
                    $opcion->valor = '00000001';
                    $opcion->save();
                    $opcion = null;
                    $contadorActualizaciones++;
                }
                $opcionGuardada = null;

                $opcionGuardada = Opcion::where('empresa_id', $empresa->id)->where('nombre', 'numeracion_factura_b')->first();
                if (!isset($opcionGuardada)) {
                    $opcion = new Opcion();
                    $opcion->empresa_id = $empresa->id;
                    $opcion->nombre = 'numeracion_factura_b';
                    $opcion->presentacion = 'Numeración de Factura B';
                    $opcion->valor = '00000001';
                    $opcion->save();
                    $opcion = null;
                    $contadorActualizaciones++;
                }
                $opcionGuardada = null;

                $opcionGuardada = Opcion::where('empresa_id', $empresa->id)->where('nombre', 'numeracion_nc_a')->first();
                if (!isset($opcionGuardada)) {
                    $opcion = new Opcion();
                    $opcion->empresa_id = $empresa->id;
                    $opcion->nombre = 'numeracion_nc_a';
                    $opcion->presentacion = 'Numeración de Nota de Crédito A';
                    $opcion->valor = '00000001';
                    $opcion->save();
                    $opcion = null;
                    $contadorActualizaciones++;
                }
                $opcionGuardada = null;

                $opcionGuardada = Opcion::where('empresa_id', $empresa->id)->where('nombre', 'numeracion_nc_b')->first();
                if (!isset($opcionGuardada)) {
                    $opcion = new Opcion();
                    $opcion->empresa_id = $empresa->id;
                    $opcion->nombre = 'numeracion_nc_b';
                    $opcion->presentacion = 'Numeración de Nota de Crédito B';
                    $opcion->valor = '00000001';
                    $opcion->save();
                    $opcion = null;
                    $contadorActualizaciones++;
                }
                $opcionGuardada = null;

                $opcionGuardada = Opcion::where('empresa_id', $empresa->id)->where('nombre', 'numeracion_nd_a')->first();
                if (!isset($opcionGuardada)) {
                    $opcion = new Opcion();
                    $opcion->empresa_id = $empresa->id;
                    $opcion->nombre = 'numeracion_nc_a';
                    $opcion->presentacion = 'Numeración de Nota de Débito A';
                    $opcion->valor = '00000001';
                    $opcion->save();
                    $opcion = null;
                    $contadorActualizaciones++;
                }
                $opcionGuardada = null;

                $opcionGuardada = Opcion::where('empresa_id', $empresa->id)->where('nombre', 'numeracion_nd_b')->first();
                if (!isset($opcionGuardada)) {
                    $opcion = new Opcion();
                    $opcion->empresa_id = $empresa->id;
                    $opcion->nombre = 'numeracion_nd_b';
                    $opcion->presentacion = 'Numeración de Nota de Débito B';
                    $opcion->valor = '00000001';
                    $opcion->save();
                    $opcion = null;
                    $contadorActualizaciones++;
                }
                $opcionGuardada = null;

            } else {
                $opcionGuardada = Opcion::where('empresa_id', $empresa->id)->where('nombre', 'numeracion_factura_c')->first();
                if (!isset($opcionGuardada)) {
                    $opcion = new Opcion();
                    $opcion->empresa_id = $empresa->id;
                    $opcion->nombre = 'numeracion_factura_c';
                    $opcion->presentacion = 'Numeración de Factura C';
                    $opcion->valor = '00000001';
                    $opcion->save();
                    $opcion = null;
                    $contadorActualizaciones++;
                }
                $opcionGuardada = null;

                $opcionGuardada = Opcion::where('empresa_id', $empresa->id)->where('nombre', 'numeracion_nc_c')->first();
                if (!isset($opcionGuardada)) {
                    $opcion = new Opcion();
                    $opcion->empresa_id = $empresa->id;
                    $opcion->nombre = 'numeracion_nc_c';
                    $opcion->presentacion = 'Numeración de Nota de Crédito C';
                    $opcion->valor = '00000001';
                    $opcion->save();
                    $opcion = null;
                    $contadorActualizaciones++;
                }
                $opcionGuardada = null;

                $opcionGuardada = Opcion::where('empresa_id', $empresa->id)->where('nombre', 'numeracion_nd_c')->first();
                if (!isset($opcionGuardada)) {
                    $opcion = new Opcion();
                    $opcion->empresa_id = $empresa->id;
                    $opcion->nombre = 'numeracion_nd_c';
                    $opcion->presentacion = 'Numeración de Nota de Débito C';
                    $opcion->valor = '00000001';
                    $opcion->save();
                    $opcion = null;
                    $contadorActualizaciones++;
                }
                $opcionGuardada = null;

            }

            $opcionGuardada = Opcion::where('empresa_id', $empresa->id)->where('nombre', 'numeracion_recibo')->first();
            if (!isset($opcionGuardada)) {
                $opcion = new Opcion();
                $opcion->empresa_id = $empresa->id;
                $opcion->nombre = 'numeracion_recibo';
                $opcion->presentacion = 'Numeración de Recibo';
                $opcion->valor = '00000001';
                $opcion->save();
                $opcion = null;
                $contadorActualizaciones++;
            }
            $opcionGuardada = null;

            $opcionGuardada = Opcion::where('empresa_id', $empresa->id)->where('nombre', 'numeracion_orden_pago')->first();
            if (!isset($opcionGuardada)) {
                $opcion = new Opcion();
                $opcion->empresa_id = $empresa->id;
                $opcion->nombre = 'numeracion_orden_pago';
                $opcion->presentacion = 'Numeración de Orden de Pago';
                $opcion->valor = '00000001';
                $opcion->save();
                $opcion = null;
                $contadorActualizaciones++;
            }
            $opcionGuardada = null;

            $opcionGuardada = Opcion::where('empresa_id', $empresa->id)->where('nombre', 'numeracion_presupuesto')->first();
            if (!isset($opcionGuardada)) {
                $opcion = new Opcion();
                $opcion->empresa_id = $empresa->id;
                $opcion->nombre = 'numeracion_presupuesto';
                $opcion->presentacion = 'Numeración Presupuesto';
                $opcion->valor = '00000001';
                $opcion->save();
                $opcion = null;
                $contadorActualizaciones++;
            }
            $opcionGuardada = null;

            $opcionGuardada = Opcion::where('empresa_id', $empresa->id)->where('nombre', 'numeracion_orden_compra')->first();
            if (!isset($opcionGuardada)) {
                $opcion = new Opcion();
                $opcion->empresa_id = $empresa->id;
                $opcion->nombre = 'numeracion_orden_compra';
                $opcion->presentacion = 'Numeración de Orden de Compra';
                $opcion->valor = '00000001';
                $opcion->save();
                $opcion = null;
                $contadorActualizaciones++;
            }
            $opcionGuardada = null;

            $opcionGuardada = Opcion::where('empresa_id', $empresa->id)->where('nombre', 'valor_redondeo_ventas')->first();
            if (!isset($opcionGuardada)) {
                $opcion = new Opcion();
                $opcion->empresa_id = $empresa->id;
                $opcion->nombre = 'valor_redondeo_ventas';
                $opcion->presentacion = 'Valor de Redondeo de Ventas';
                $opcion->valor = '0';
                $opcion->save();
                $opcion = null;
                $contadorActualizaciones++;
            }
            $opcionGuardada = null;

            //Proveedor y Cliente
            $proveedorGuardado = Proveedor::where('empresa_id', $empresa->id)->where('razon_social', 'Otro')->first();
            if (!isset($proveedorGuardado)) {
                $proveedor = new Proveedor();
                $proveedor->empresa_id = $empresa->id;
                $proveedor->razon_social = 'Otro';
                $proveedor->cuit = '27-00000000-6';
                $proveedor->condicion_iva_id = 1;//Monotributista
                $proveedor->save();
                $contadorActualizaciones++;
            }

            $clienteGuaradado = Cliente::where('empresa_id', $empresa->id)->where('razon_social', 'Consumidor Final')->first();
            if (!isset($clienteGuaradado)) {
                $cliente = new Cliente();
                $cliente->empresa_id = $empresa->id;
                $cliente->razon_social = 'Consumidor Final';
                $cliente->condicion_iva_id = 5;//CF
                $cliente->dni = '27000000006';
                $cliente->save();
                $contadorActualizaciones++;
            }

            //Rubro, Subrubro y Marca
            $rubro = Rubro::where('empresa_id', $empresa->id)->where('nombre', 'Otro')->first();
            if (!isset($rubro)) {
                $rubro = new Rubro();
                $rubro->empresa_id = $empresa->id;
                $rubro->nombre = 'Otro';
                $rubro->save();
                $contadorActualizaciones++;
            }

            $subrubroGuardado = Subrubro::where('empresa_id', $empresa->id)->where('nombre', 'Otro')->first();
            if (!isset($subrubroGuardado)) {
                $subrubro = new Subrubro();
                $subrubro->empresa_id = $empresa->id;
                $subrubro->rubro_id = $rubro->id;
                $subrubro->nombre = 'Otro';
                $subrubro->save();
                $contadorActualizaciones++;
            }


            $marca = Marca::where('empresa_id', $empresa->id)->where('nombre', 'Otra')->first();
            if (!isset($marca)) {
                $marca = new Marca();
                $marca->empresa_id = $empresa->id;
                $marca->nombre = 'Otra';
                $marca->save();
                $contadorActualizaciones++;
            }


            //ListasPrecios
            $listaMinorista = ListaPrecio::where('empresa_id', $empresa->id)->where('nombre', 'Minorista')->first();
            if (!isset($listaMinorista)) {
                $listaMinorista = new ListaPrecio();
                $listaMinorista->empresa_id = $empresa->id;
                $listaMinorista->nombre = 'Minorista';
                $listaMinorista->estado = Estado::ACTIVO;
                $listaMinorista->porcentaje = 60;
                $listaMinorista->save();
                $contadorActualizaciones++;
            }

            $listaMayorista = ListaPrecio::where('empresa_id', $empresa->id)->where('nombre', 'Mayorista')->first();
            if (!isset($listaMayorista)) {
                $listaMayorista = new ListaPrecio();
                $listaMayorista->empresa_id = $empresa->id;
                $listaMayorista->nombre = 'Mayorista';
                $listaMayorista->estado = Estado::ACTIVO;
                $listaMayorista->porcentaje = 30;
                $listaMayorista->save();
                $contadorActualizaciones++;
            }

            // Lista de Precios por defecto
            $opcionGuardada = Opcion::where('empresa_id', $empresa->id)->where('nombre', 'lista_precios_defecto_id')->first();
            if (!isset($opcionGuardada)) {
                $opcion = new Opcion();
                $opcion->empresa_id = $empresa->id;
                $opcion->nombre = 'lista_precios_defecto_id';
                $opcion->presentacion = 'Lista de Precios por Defecto';
                $opcion->valor = $listaMinorista->id;
                $opcion->save();
                $opcion = null;
                $contadorActualizaciones++;
            }
            $opcionGuardada = null;

            //PuntosDeVenta 9999 x Sucursal
            $sucursales = $empresa->sucursales;
            foreach ($sucursales as $sucursal){
                $puntoVenta = PuntoDeVenta::where('sucursal_id',$sucursal->id)->whereNull('deleted_at')->where('numero', '9999')->first();
                if(!isset($puntoVenta)){
                    $puntoVenta = new PuntoDeVenta();
                    $puntoVenta->sucursal_id = $sucursal->id;
                    $puntoVenta->empresa_id = $empresa->id;
                    $puntoVenta->tipo_punto_id = 2;
                    $puntoVenta->numero = '9999';
                    $puntoVenta->save();
                    $contadorActualizaciones++;
                }
            }

            // Monedas
            $pesosArgentinos = Moneda::where('empresa_id', $empresa->id)->where('nombre', 'Pesos Argentinos')->first();
            if (!isset($pesosArgentinos)) {
                $monedaLocal = new Moneda();
                $monedaLocal->empresa_id = $empresa->id;
                $monedaLocal->numero = 1;
                $monedaLocal->nombre = 'Pesos Argentinos';
                $monedaLocal->signo = 'ARS';
                $monedaLocal->cotizacion = 1;
                $monedaLocal->save();
                $contadorActualizaciones++;
            }

            $dolares = Moneda::where('empresa_id', $empresa->id)->where('nombre', 'Dolares')->first();
            if (!isset($dolares)) {
                $monedaUSD = new Moneda();
                $monedaUSD->empresa_id = $empresa->id;
                $monedaUSD->numero = 2;
                $monedaUSD->nombre = 'Dolares';
                $monedaUSD->signo = 'USD';
                $monedaUSD->cotizacion = 23;
                $monedaUSD->save();
                $contadorActualizaciones++;
            }


            if($contadorActualizaciones == 0){
                Session::flash('flash_message',  '¡La cuenta de la Empresa '.$empresa->razon_social.' está Actualizada!');
            }else{
                Session::flash('flash_message',  '¡Se hicieron '.$contadorActualizaciones.' actualizaciones en la cuenta de la Empresa '.$empresa->razon_social.'!');
            }
        });

        return redirect('admin/empresas/empresa');
    }

    public static function creacionAutomatica(Request $request){

            $empresa = new Empresa();
            $empresa->razon_social = ucfirst($request->razon_social);
            $empresa->condicion_iva_id = $request->condicion_iva_id;
            $empresa->email = $request->email;
            $empresa->plan_id = 1;
            $empresa->save();

            $sucursal = new Sucursal();
            $sucursal->nombre = 'Casa Central';
            $sucursal->empresa_id = $empresa->id;
            $sucursal->email = $empresa->email;
            $sucursal->telefono = $empresa->telefono;
            $sucursal->save();

            $monedaLocal = EmpresaController::seedingNuevasEmpresas($empresa);

            $caja = new Caja();
            $caja->numero = 1;
            $caja->nombre = 'Caja Principal';
            $caja->moneda_id = $monedaLocal->id;
            $caja->empresa_id = $empresa->id;
            $caja->sucursal_id = $sucursal->id;
            $caja->estado = EstadoCaja::CERRADA;
            $caja->save();

            $banco = Banco::where('empresa_id',$empresa->id)->first();
            $cheque = new Cheque();
            $cheque->banco_id = $banco->id;
            $cheque->importe = 1000;
            $cheque->numero = '01234567';
            $cheque->firmante = 'Nombre Apellido';
            $cheque->vencimiento = Carbon::now()->addWeek();
            $cheque->estado = EstadoCheque::COBRADO;
            $cheque->caja_id = $caja->id;
            $cheque->empresa_id = $empresa->id;
            $cheque->save();

            $puntoVenta = new PuntoDeVenta();
            $puntoVenta->sucursal_id = $sucursal->id;
            $puntoVenta->empresa_id = $empresa->id;
            $puntoVenta->tipo_punto_id = 2;
            $puntoVenta->numero = '9999';
            $puntoVenta->save();

            $cliente = Cliente::where('empresa_id',$empresa->id)->first();
            $listaPrecio = ListaPrecio::where('empresa_id',$empresa->id)->where('nombre','Minorista')->first();
            $venta = new Venta();
            $venta->empresa_id = $empresa->id;
            $venta->cliente_id = $cliente->id;
            $venta->moneda_id = $monedaLocal->id;
            $venta->cotizacion = 1;
            $venta->fecha = Carbon::now();
            $venta->numero = '00000001';
            $venta->importe_neto = 157.3;
            $venta->importe_iva = 33.03;
            $venta->total = 190.33;
            $venta->estado = Estado::ACTIVO;
            $venta->punto_venta_id = $puntoVenta->id;
            $venta->tipo_comprobante_id = TipoComprobante::FACTURA_B;
            $venta->lista_precios_id = $listaPrecio->id;
            $venta->sucursal_id = $sucursal->id;
            $venta->total_cobro = 190.33;
            $venta->save();

            $articulo = Articulo::where('empresa_id',$empresa->id)->first();
            $detalleVenta = New DetalleVenta();
            $detalleVenta->venta_id = $venta->id;
            $detalleVenta->articulo_id = $articulo->id;
            $detalleVenta->precio_neto = 190.33;
            $detalleVenta->precio_iva = 0;
            $detalleVenta->cantidad = 2;
            $detalleVenta->estado = Estado::ACTIVO;
            $detalleVenta->save();

            $empleado = new Empleado();
            $empleado->empresa_id = $empresa->id;
            $empleado->sucursal_id = $sucursal->id;
            $empleado->caja_id = $caja->id;
            $empleado->punto_venta_id = $puntoVenta->id;
            $empleado->nombre = 'Usuario Admin';
            $empleado->cuil = '22-22222222-2';
            $empleado->save();

            $roles = new RoleRepository();
            $users = new UserRepository($roles);
            $usuarioController = new UsuarioController($users, $roles);
            $usuarioController->nuevoUsuario('Usuario Admin', $request->email, $request->password, $request->password, $empresa, $sucursal, $empleado, 'Usuario Admin');

            Mail::send('mails.welcome', ['request' => $request,], function ($mensaje) use ($request) {
                $mensaje->from('noreply@artisan.com.ar', '¡Bienvenido a Artisan!');
                $mensaje->to($request->email, $request->razon_social);
                $mensaje->subject('¡Bienvenido a Artisan!');
            });

            Session::flash('flash_message', trans('labels.frontend.empresa').trans('alerts.frontend.created').'!');

    }

    private static function seedingNuevasEmpresas($empresa){

        $return = DB::transaction(function() use ($empresa) {

            if($empresa->condicion_iva_id == 2){ // Es Responsable Inscripto
                $opcion01 = new Opcion();
                $opcion01->empresa_id = $empresa->id;
                $opcion01->nombre = 'numeracion_factura_a';
                $opcion01->presentacion  = 'Numeración de Factura A';
                $opcion01->valor = '00000001';
                $opcion01->save();

                $opcion02 = new Opcion();
                $opcion02->empresa_id = $empresa->id;
                $opcion02->nombre = 'numeracion_factura_b';
                $opcion02->presentacion  = 'Numeración de Factura B';
                $opcion02->valor = '00000001';
                $opcion02->save();

                $opcion06 = new Opcion();
                $opcion06->empresa_id = $empresa->id;
                $opcion06->nombre = 'numeracion_nc_a';
                $opcion06->presentacion  = 'Numeración de Nota de Crédito A';
                $opcion06->valor = '00000001';
                $opcion06->save();

                $opcion07 = new Opcion();
                $opcion07->empresa_id = $empresa->id;
                $opcion07->nombre = 'numeracion_nc_b';
                $opcion07->presentacion  = 'Numeración de Nota de Crédito B';
                $opcion07->valor = '00000001';
                $opcion07->save();

                $opcion09 = new Opcion();
                $opcion09->empresa_id = $empresa->id;
                $opcion09->nombre = 'numeracion_nd_a';
                $opcion09->presentacion  = 'Numeración de Nota de Débito A';
                $opcion09->valor = '00000001';
                $opcion09->save();

                $opcion10 = new Opcion();
                $opcion10->empresa_id = $empresa->id;
                $opcion10->nombre = 'numeracion_nd_b';
                $opcion10->presentacion  = 'Numeración de Nota de Débito B';
                $opcion10->valor = '00000001';
                $opcion10->save();
            } else {
                $opcion03 = new Opcion();
                $opcion03->empresa_id = $empresa->id;
                $opcion03->nombre = 'numeracion_factura_c';
                $opcion03->presentacion  = 'Numeración de Factura C';
                $opcion03->valor = '00000001';
                $opcion03->save();

                $opcion08 = new Opcion();
                $opcion08->empresa_id = $empresa->id;
                $opcion08->nombre = 'numeracion_nc_c';
                $opcion08->presentacion  = 'Numeración de Nota de Crédito C';
                $opcion08->valor = '00000001';
                $opcion08->save();

                $opcion11 = new Opcion();
                $opcion11->empresa_id = $empresa->id;
                $opcion11->nombre = 'numeracion_nd_c';
                $opcion11->presentacion  = 'Numeración de Nota de Débito C';
                $opcion11->valor = '00000001';
                $opcion11->save();
            }

            $opcion04 = new Opcion();
            $opcion04->empresa_id = $empresa->id;
            $opcion04->nombre = 'numeracion_recibo';
            $opcion04->presentacion  = 'Numeración de Recibo';
            $opcion04->valor = '00000001';
            $opcion04->save();

            $opcion05 = new Opcion();
            $opcion05->empresa_id = $empresa->id;
            $opcion05->nombre = 'numeracion_orden_pago';
            $opcion05->presentacion  = 'Numeración de Orden de Pago';
            $opcion05->valor = '00000001';
            $opcion05->save();

            $opcion12 = new Opcion();
            $opcion12->empresa_id = $empresa->id;
            $opcion12->nombre = 'numeracion_presupuesto';
            $opcion12->presentacion  = 'Numeración Presupuesto';
            $opcion12->valor = '00000001';
            $opcion12->save();

            $opcion14 = new Opcion();
            $opcion14->empresa_id = $empresa->id;
            $opcion14->nombre = 'numeracion_orden_compra';
            $opcion14->presentacion  = 'Numeración de Orden de Compra';
            $opcion14->valor = '00000001';
            $opcion14->save();

            $opcion15 = new Opcion();
            $opcion15->empresa_id = $empresa->id;
            $opcion15->nombre = 'valor_redondeo_ventas';
            $opcion15->presentacion  = 'Valor de Redondeo de Ventas';
            $opcion15->valor = '0';
            $opcion15->save();

            // Bancos
            $banco1 = new Banco();
            $banco1->nombre = 'Macro';
            $banco1->empresa_id = $empresa->id;
            $banco1->save();

            $banco2 = new Banco();
            $banco2->nombre = 'Galicia';
            $banco2->empresa_id = $empresa->id;
            $banco2->save();

            $banco3 = new Banco();
            $banco3->nombre = 'La Nación';
            $banco3->empresa_id = $empresa->id;
            $banco3->save();

            $banco4 = new Banco();
            $banco4->nombre = 'BBVA Banco Francés';
            $banco4->empresa_id = $empresa->id;
            $banco4->save();

            $banco5 = new Banco();
            $banco5->nombre = 'Supervielle';
            $banco5->empresa_id = $empresa->id;
            $banco5->save();

            $banco6 = new Banco();
            $banco6->nombre = 'Patagonia';
            $banco6->empresa_id = $empresa->id;
            $banco6->save();

            $banco7 = new Banco();
            $banco7->nombre = 'Hipotecario';
            $banco7->empresa_id = $empresa->id;
            $banco7->save();

            $banco8 = new Banco();
            $banco8->nombre = 'Santander Rio';
            $banco8->empresa_id = $empresa->id;
            $banco8->save();

            $banco9 = new Banco();
            $banco9->nombre = 'Credicoop';
            $banco9->empresa_id = $empresa->id;
            $banco9->save();

            $banco10 = new Banco();
            $banco10->nombre = 'Santa Fe';
            $banco10->empresa_id = $empresa->id;
            $banco10->save();

            $banco11 = new Banco();
            $banco11->nombre = 'Bancor';
            $banco11->empresa_id = $empresa->id;
            $banco11->save();

            $monedaLocal = new Moneda();
            $monedaLocal->empresa_id = $empresa->id;
            $monedaLocal->numero = 1;
            $monedaLocal->nombre = 'Pesos Argentinos';
            $monedaLocal->signo = 'ARS';
            $monedaLocal->cotizacion = 1;
            $monedaLocal->save();

            $monedaUSD = new Moneda();
            $monedaUSD->empresa_id = $empresa->id;
            $monedaUSD->numero = 2;
            $monedaUSD->nombre = 'Dolares';
            $monedaUSD->signo = 'USD';
            $monedaUSD->cotizacion = 23;
            $monedaUSD->save();

            $domicilioPrueba = new Domicilio();
            $domicilioPrueba->direccion = '25 de Mayo 123';
            $domicilioPrueba->pais_id = '1';
            $domicilioPrueba->provincia_id = '1';
            $domicilioPrueba->localidad = 'Localidad';
            $domicilioPrueba->save();

            $proveedor = new Proveedor();
            $proveedor->empresa_id = $empresa->id;
            $proveedor->razon_social = 'Otro';
            $proveedor->cuit = '27-00000000-6';
            $proveedor->condicion_iva_id = 1;//Monotributista
            $proveedor->save();

            $rubro = new Rubro();
            $rubro->empresa_id = $empresa->id;
            $rubro->nombre = 'Otro';
            $rubro->save();

            $subrubro = new Subrubro();
            $subrubro->empresa_id = $empresa->id;
            $subrubro->rubro_id = $rubro->id;
            $subrubro->nombre = 'Otro';
            $subrubro->save();

            $marca = new Marca();
            $marca->empresa_id = $empresa->id;
            $marca->nombre = 'Otra';
            $marca->save();

            $articulo = new Articulo();
            $articulo->empresa_id = $empresa->id;
            $articulo->rubro_id = $rubro->id;
            $articulo->subrubro_id = $subrubro->id;
            $articulo->marca_id = $marca->id;
            $articulo->unidad_id = 2;//Unidad
            $articulo->moneda_id = $monedaLocal->id;
            $articulo->nombre = 'Articulo Ejemplo';
            $articulo->modelo = 'v2';
            $articulo->codigo = '1';
            $articulo->ultimo_costo = 100;
            $articulo->porcentaje_iva = 2;//21%
            $articulo->cantidad = 1;
            $articulo->save();

            $detalleArticulo = new DetalleArticulo();
            $detalleArticulo->articulo_id = $articulo->id;
            $detalleArticulo->proveedor_id = $proveedor->id;
            $detalleArticulo->moneda_id = $monedaLocal->id;
            $detalleArticulo->costo = $articulo->ultimo_costo;
            $detalleArticulo->importe_iva = 21;
            $detalleArticulo->cantidad = 1;
            $detalleArticulo->save();

            $listaMinorista = new ListaPrecio();
            $listaMinorista->empresa_id = $empresa->id;
            $listaMinorista->nombre = 'Minorista';
            $listaMinorista->estado = Estado::ACTIVO;
            $listaMinorista->porcentaje = 60;
            $listaMinorista->save();

            $precio01 = new Precio();
            $precio01->articulo_id = $articulo->id;
            $precio01->lista_precio_id = $listaMinorista->id;
            $precio01->precio = 157.3;
            $precio01->save();

            $listaMayorista = new ListaPrecio();
            $listaMayorista->empresa_id = $empresa->id;
            $listaMayorista->nombre = 'Mayorista';
            $listaMayorista->estado = Estado::ACTIVO;
            $listaMayorista->porcentaje = 30;
            $listaMayorista->save();

            $precio02 = new Precio();
            $precio02->articulo_id = $articulo->id;
            $precio02->lista_precio_id = $listaMayorista->id;
            $precio02->precio = 157.3;
            $precio02->save();

            $opcion13 = new Opcion();
            $opcion13->empresa_id = $empresa->id;
            $opcion13->nombre = 'lista_precios_defecto_id';
            $opcion13->presentacion = 'Lista de Precios por Defecto';
            $opcion13->valor = $listaMinorista->id;
            $opcion13->save();

            $cliente = new Cliente();
            $cliente->empresa_id = $empresa->id;
            $cliente->razon_social = 'Consumidor Final';
            $cliente->condicion_iva_id = 5;//CF
            $cliente->dni = '27000000006';
            $cliente->domicilio_id = $domicilioPrueba->id;
            $cliente->lista_precios_id = $listaMinorista->id;
            $cliente->save();

            $concepto1 = new ConceptoPersonalizado();
            $concepto1->empresa_id = $empresa->id;
            $concepto1->nombre = 'Ingresos Varios';
            $concepto1->save();

            $concepto2 = new ConceptoPersonalizado();
            $concepto2->empresa_id = $empresa->id;
            $concepto2->nombre = 'Egresos Varios';
            $concepto2->save();

            $evento1 = new Event();
            $evento1->empresa_id = $empresa->id;
            $evento1->titulo = 'Reunion Ejemplo';
            $evento1->fecha_inicio = Carbon::now()->addDay();
            $evento1->hora_inicio = '13:00:00';
            $evento1->color = '#f51155';
            $evento1->save();

            $evento2 = new Event();
            $evento2->empresa_id = $empresa->id;
            $evento2->titulo = 'Evento Ejemplo';
            $evento2->fecha_inicio = Carbon::now()->addDays(2);
            $evento2->hora_inicio = '15:30:00';
            $evento2->color = '#00CC0E';
            $evento2->save();

            $permisos = Permiso::where('nombre','<>','facturacion-automatica')
                        ->where('nombre','<>','orden-compra')
                        ->where('nombre','<>','grupos')
                        ->where('nombre','<>','gastos-bancarios')
                        ->where('nombre','<>','chequeras')
                        ->where('nombre','<>','vendedores')
                        ->where('nombre','<>','informe-actividades')
                        ->where('nombre','<>','actividades-fiscales')
                        ->where('nombre','<>','vendedores')
                        ->get();

            foreach ($permisos as $permiso) {
                $empresa->permisos()->attach($permiso);
            }

            $nuevaFormaPago = New FormaPago();
            $nuevaFormaPago->tipo_forma_pago = 'Efectivo';
            $nuevaFormaPago->empresa_id = $empresa->id;
            $nuevaFormaPago->save();

            $nuevaFormaPago = New FormaPago();
            $nuevaFormaPago->tipo_forma_pago = 'Deposito';
            $nuevaFormaPago->empresa_id = $empresa->id;
            $nuevaFormaPago->save();

            return $monedaLocal;
        });

        return $return;

    }

}
