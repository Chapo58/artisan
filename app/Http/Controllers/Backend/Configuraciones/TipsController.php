<?php

namespace App\Http\Controllers\Backend\Configuraciones;

use App\Models\Access\User\User;
use App\Models\Backend\Configuraciones\Tip;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;
use DB;

class TipsController extends Controller{

    public function index(Request $request){
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $tips = Tip::where('texto', 'LIKE', "%$keyword%")
                ->where('ruta', 'LIKE', "%$keyword%")
                ->paginate($perPage);
        } else {
            $tips = Tip::paginate($perPage);
        }

        return view('backend.configuraciones.tips.index', compact('tips'));
    }

    public function create(){
        if (Auth::user()->hasRole('Administrador')) {
            return view('backend.configuraciones.tips.create');
        } else {
            return redirect('admin/empresas/empresa');
        }
    }

    public function store(Request $request){
        $this->validate($request, [
            'texto' => 'required',
            'ruta' => 'required|unique:tips,ruta,NULL,id,deleted_at,NULL'
        ]);

        $requestData = $request->all();
        $nuevoTip = Tip::create($requestData);

        $usuarios = User::whereNotNull('empleado_id')->whereNull('deleted_at')->get();
        foreach($usuarios as $user){
            $nuevoTip->users()->attach($user->id);
        }

        Session::flash('flash_message', 'Tip creado correctamente!');
        return redirect('admin/configuraciones/tips');
    }

    public function show($id){
        $tip = Tip::findOrFail($id);

        return view('backend.configuraciones.tips.show', compact('tip'));
    }

    public function edit($id){
        $tip = Tip::findOrFail($id);
        if (Auth::user()->hasRole('Administrador')) {
            return view('backend.configuraciones.tips.edit', compact('tip'));
        } else {
            return redirect('admin/empresas/empresa');
        }
    }

    public function update($id, Request $request){
        $this->validate($request, [
            'texto' => 'required',
            'ruta' => 'required'
        ]);

        $requestData = $request->all();

        $tip = Tip::findOrFail($id);
        $tip->update($requestData);

        $tip->users()->detach();
        $usuarios = User::whereNotNull('empleado_id')->whereNull('deleted_at')->get();
        foreach($usuarios as $user){
            $tip->users()->attach($user->id);
        }

        Session::flash('flash_message', 'Tip actualizado correctamente!');

        return redirect('admin/configuraciones/tips');
    }

    public function destroy($id){
        if (Auth::user()->hasRole('Administrador')) {
            $tip = Tip::findOrFail($id);
            $tip->users()->detach();

            Tip::destroy($id);

            Session::flash('flash_message',  'Tip Eliminado!');

            return redirect('admin/configuraciones/tips');
        }
    }

}
