<?php

namespace App\Http\Controllers\Backend\Configuraciones;

use App\Models\Backend\Configuraciones\Tawkto;
use App\Models\Backend\Empresas\Empresa;
use App\Models\Backend\Permisos\Permiso;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;
use DB;

class ActualizarController extends Controller{

    public function index(){
        return view('backend.configuraciones.actualizar.index');
    }

    public function actualizar(){
        $empresas = Empresa::whereNull('deleted_at')->get();
        $permisos = Permiso::get();

        foreach($empresas as $empresa){
            $empresa->permisos()->detach();
            foreach($permisos as $permiso){
                $empresa->permisos()->attach($permiso->id);
            }
        }

        Session::flash('flash_message', 'El sistema ha sido actualizado!');
        return redirect('admin/configuraciones/actualizar');
    }
}
