<?php

namespace App\Http\Controllers\Backend\Configuraciones;

use App\Models\Backend\Configuraciones\Tawkto;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;
use DB;

class TawktoController extends Controller{

    public function index(){
        $tawk = Tawkto::where('distribuidor_id',Auth::user()->distribuidor()->id)->first();
        return view('backend.configuraciones.tawkto.index', compact('tawk'));
    }

    public function guardar(Request $request){
        $this->validate($request, [
            'codigo' => 'required'
		]);

        $requestData = $request->all();

        if(!isset($requestData['habilitado'])){
            $requestData['habilitado'] = 0;
        }

        if($request->id){
            $tawk = Tawkto::findOrFail($request->id);
            $tawk->update($requestData);
        } else {
            $nuevoChat = new Tawkto($requestData);
            $nuevoChat->distribuidor_id = Auth::user()->distribuidor()->id;
            $nuevoChat->save();
        }

        Session::flash('flash_message', 'Codigo del Widget Guardado!');
        return redirect('admin/configuraciones/tawkto');
    }
}
