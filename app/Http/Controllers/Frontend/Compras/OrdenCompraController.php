<?php

namespace App\Http\Controllers\Frontend\Compras;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Frontend\Configuraciones\HistorialController;
use App\Models\Frontend\Compras\Compra;
use App\Models\Frontend\Compras\DetalleCompra;
use App\Models\Frontend\Compras\DetalleOrdenCompra;
use App\Models\Frontend\Compras\OrdenCompra;
use App\Models\Frontend\Configuraciones\Estado;
use App\Models\Frontend\Configuraciones\Opcion;
use Yajra\Datatables\Facades\Datatables;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Session;

class OrdenCompraController extends Controller{

    public function index(){
        return view('frontend.compras.orden-compra.index');
    }

    public function datos(){
        $empresa = Auth::user()->empresa;
        $sucursal = Auth::user()->sucursal;

        $ordenesCompras = OrdenCompra::listarOrdenesComprasPorEmpresaSucursal($empresa, $sucursal);

        return Datatables::of($ordenesCompras)
            ->addColumn('action', function ($ordenCompra) {
                return '
                <a href="'.url('/compras/orden-compra/' . $ordenCompra->id).'" class="mytooltip"><i class="fa fa-eye text-success m-r-10"></i>
                    <span class="tooltip-content3">Ver Orden de Compra</span>
                </a>
                <a href="'.url('/compras/orden-compra/' . $ordenCompra->id . '/edit').'" class="mytooltip">
                    <i class="fa fa-pencil text-info m-r-10"></i>
                    <span class="tooltip-content3">Modificar Orden de Compra</span>
                </a>
                <a href="'.url('/compras/orden-compra/generar-compra/' . $ordenCompra->id).'" class="mytooltip">
                    <i class="fa fa-copy text-warning m-r-10"></i>
                    <span class="tooltip-content3">Generar Compra</span>
                </a>
                <a class="delete mytooltip pointer" data-id="'.$ordenCompra->id.'">
                    <i class="fa fa-close text-danger"></i>
                    <span class="tooltip-content3">Eliminar Orden de Compra</span>
                </a>
                ';
            })
            ->setRowId('id')
            ->removeColumn('id')
            ->editColumn('fecha', function ($ordenCompra) {
                return $ordenCompra->fecha->format('d/m/Y');
            })
            ->editColumn('numero', function ($ordenCompra) {
                return 'Orden Compra N° '.$ordenCompra->numero;
            })
            ->make(true);
    }

    public function create(){
        $empresa = Auth::user()->empresa;
        $numero = Opcion::getValorOpcion('numeracion_orden_compra', $empresa);

        return view('frontend.compras.orden-compra.create', compact('numero'));
    }

    public function store(Request $request){
        $this->validate($request, [
			'proveedor_id' => 'required',
			'fecha' => 'required',
			'numero' => 'required'
		]);

        DB::transaction(function() use ($request) {
            $requestData = $request->all();
            $empresa = Auth::user()->empresa;
            $sucursal = Auth::user()->sucursal;

            $date = DateTime::createFromFormat('d/m/Y', $requestData['fecha']);
            $requestData['fecha'] = $date->format('Y-m-d H:i:s');
            $ordenCompra = New OrdenCompra($requestData);
            $ordenCompra->estado = Estado::ACTIVO;
            $ordenCompra->empresa_id = $empresa->id;
            $ordenCompra->sucursal_id = Auth::user()->sucursal_id;
            $ordenCompra->save();

            $ultimo_codigo = str_pad((int)$ordenCompra->numero + 1, 8, '0', STR_PAD_LEFT);
            Opcion::setValorOpcion('numeracion_orden_compra', $ultimo_codigo, $empresa);

            if(isset($requestData['detalles'])) {
                $detalles = $requestData['detalles'];
                foreach ($detalles as $detalle) {
                    if((int)$detalle['estado'] == Estado::ACTIVO && isset($detalle['cantidad'])){
                        $nuevoDetalle = New DetalleOrdenCompra($detalle);
                        $nuevoDetalle->orden_compra_id = $ordenCompra->id;
                        if(!isset($detalle['articulo_id'])){
                            $nuevoDetalle->articulo_nombre = $detalle['articulo'];
                        }
                        $nuevoDetalle->save();
                    }
                }
            }

            $controllerHistorial = new HistorialController();
            $controllerHistorial->nuevoHistorialCreacion($ordenCompra, $request);
        });

        return redirect('compras/orden-compra')->withFlashSuccess('Orden de Compra creada correctamente.');
    }

    public function show($id){
        $ordenCompra = OrdenCompra::findOrFail($id);
        $empresa = Auth::user()->empresa;

        if ($ordenCompra->empresa_id == Auth::user()->empresa_id) {
            return view('frontend.compras.orden-compra.show', compact('ordenCompra','empresa'));
        }
        return redirect('compras/orden-compra');
    }

    public function edit($id){
        $ordenCompra = OrdenCompra::findOrFail($id);
        $empresa = Auth::user()->empresa;
        $editar = TRUE;

        if ($ordenCompra->empresa == $empresa) {
            return view('frontend.compras.orden-compra.edit', compact('ordenCompra', 'empresa', 'editar'));
        }

        return redirect('compras/orden-compra');
    }

    public function update($id, Request $request){
        $this->validate($request, [
            'proveedor_id' => 'required',
            'fecha' => 'required',
            'numero' => 'required'
        ]);

        $ordenCompra = OrdenCompra::findOrFail($id);

        if ($ordenCompra->empresa_id != Auth::user()->empresa_id) {
            return redirect('compras/orden-compra');
        }else{
            if (Auth::user()->hasRole('Usuario Vendedor')) {
                if (!$ordenCompra->sucursal_id == Auth::user()->sucursal_id) {
                    return redirect('compras/orden-compra');
                }
            }
        }

        DB::transaction(function() use ($ordenCompra, $request) {
            $requestData = $request->all();
            $empresa = Auth::user()->empresa;
            $sucursal = Auth::user()->sucursal;

            $date = DateTime::createFromFormat('d/m/Y', $requestData['fecha']);
            $requestData['fecha'] = $date->format('Y-m-d H:i:s');
            $ordenCompra->descripcion = $requestData['descripcion'];
            $ordenCompra->update();

            if(isset($requestData['detalles'])){
                $detalles = $requestData['detalles'];
                foreach ($detalles as $detalle) {
                    if((int)$detalle['id'] > 0){//Editar cantidad de detalle o borrarlo
                        $detalleGuardado = $ordenCompra->detalles->find((int)$detalle['id']);
                        if((int)$detalle['estado'] == Estado::ACTIVO && isset($detalle['cantidad'])){
                            $detalleGuardado->cantidad = $detalle['cantidad'];
                            $detalleGuardado->update();
                        }else{
                            $detalleGuardado->delete();
                        }
                    }else{
                        if((int)$detalle['estado'] == Estado::ACTIVO && isset($detalle['cantidad'])){
                            $nuevoDetalle = New DetalleOrdenCompra($detalle);
                            $nuevoDetalle->orden_compra_id = $ordenCompra->id;
                            if(!isset($detalle['articulo_id'])){
                                $nuevoDetalle->articulo_nombre = $detalle['articulo'];
                            }
                            $nuevoDetalle->save();
                        }
                    }
                }
            }

            $controllerHistorial = new HistorialController();
            $controllerHistorial->nuevoHistorialActualizacion($ordenCompra, $request);
        });

        return redirect('compras/orden-compra')->withFlashSuccess('Orden de Compra modificada correctamente.');
    }

    public function destroy($id){
        $ordenCompra = OrdenCompra::findOrFail($id);
        $empresa = Auth::user()->empresa;

        if ($ordenCompra->empresa != $empresa) {
            return redirect('compras/orden-compra');
        }else{
            if (Auth::user()->hasRole('Usuario Vendedor')) {
                if (!$ordenCompra->sucursal_id == Auth::user()->sucursal_id) {
                    return redirect('compras/orden-compra');
                }
            }
        }

        DB::transaction(function() use ($ordenCompra) {
            if(!$ordenCompra->detalles->isEmpty()){
                foreach($ordenCompra->detalles as $detalle){
                    $detalle->delete();
                }
            }

            $ordenCompra->delete();

            $controllerHistorial = new HistorialController();
            $controllerHistorial->nuevoHistorialEliminacion($ordenCompra);
        });

        return redirect('compras/orden-compra')->withFlashSuccess('Orden de Compra eliminada correctamente.');
    }

    public function generarCompra($id){
        $empresa = Auth::user()->empresa;
        $ordenCompra = OrdenCompra::findOrFail($id);

        if ($ordenCompra->empresa == $empresa) {
            $nuevaCompra = new Compra();
            $nuevaCompra->proveedor = $ordenCompra->proveedor;
            $nuevaCompra->proveedor_id = $ordenCompra->proveedor_id;
            $nuevaCompra->fecha = null;

            $detallesCompra = null;
            $aux = 1;
            foreach ($ordenCompra->detalles as $detalle){
                $nuevoDetalle = new DetalleCompra();
                $nuevoDetalle->id = $aux*-1;
                if($detalle->articulo){
                    $nuevoDetalle->articulo = $detalle->articulo;
                    $nuevoDetalle->articulo_id = $detalle->articulo_id;
                } else {
                    $nuevoDetalle->articulo = $detalle->articulo_nombre;
                }
                $nuevoDetalle->cantidad = $detalle->cantidad;
                $nuevoDetalle->estado = $detalle->estado;
                $detallesCompra[$aux*-1] = $nuevoDetalle;
                $aux++;
            }
            $nuevaCompra->detalles = $detallesCompra;

            $compraController = new CompraController();

            return $compraController->create($nuevaCompra);
        }
    }

}
