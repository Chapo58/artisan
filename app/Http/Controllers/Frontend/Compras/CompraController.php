<?php

namespace App\Http\Controllers\Frontend\Compras;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Frontend\Caja\MovimientoCajaController;
use App\Http\Controllers\Frontend\Calendario\EventsController;
use App\Http\Controllers\Frontend\Configuraciones\HistorialController;
use App\Http\Controllers\Frontend\Ventas\CuentaCorrienteController;
use App\Models\Frontend\Articulos\Articulo;
use App\Models\Frontend\Articulos\DetalleArticulo;
use App\Models\Frontend\Articulos\Rubro;
use App\Models\Frontend\Articulos\Stock;
use App\Models\Frontend\Articulos\Subrubro;
use App\Models\Frontend\Caja\CierreCaja;
use App\Models\Frontend\Caja\ConceptoDeCaja;
use App\Models\Frontend\Caja\EstadoCaja;
use App\Models\Frontend\Caja\FormaDePago;
use App\Models\Frontend\Caja\MovimientoCaja;
use App\Models\Frontend\Compras\Compra;
use App\Models\Frontend\Compras\DetalleCompra;
use App\Models\Frontend\Compras\DetallePagoCompra;
use App\Models\Frontend\Configuraciones\Estado;
use App\Models\Frontend\Configuraciones\Moneda;
use App\Models\Frontend\Configuraciones\PorcentajeIva;
use App\Models\Frontend\Configuraciones\TarjetaPropia;
use App\Models\Frontend\Configuraciones\TipoComprobante;
use App\Models\Frontend\Fondos\Cheque;
use App\Models\Frontend\Fondos\ChequePropio;
use App\Models\Frontend\Fondos\Chequera;
use App\Models\Frontend\Fondos\CuentaBancaria;
use App\Models\Frontend\Fondos\EstadoCheque;
use App\Models\Frontend\Fondos\MovimientoBancario;
use App\Models\Frontend\Fondos\TipoMovimiento;
use App\Models\Frontend\Ventas\ListaPrecio;
use App\Models\Frontend\Ventas\Precio;
use Yajra\Datatables\Facades\Datatables;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Session;

class CompraController extends Controller{

    public function index(){
        $estadoCaja = EstadoCaja::CERRADA;
        if(isset(Auth::user()->empleado->caja)){
            if(Auth::user()->empleado->caja->estado == EstadoCaja::ABIERTA) {
                $estadoCaja = EstadoCaja::ABIERTA;
            }
        }

        return view('frontend.compras.compra.index', compact('estadoCaja'));
    }

    public function datos(){
        $empresa = Auth::user()->empresa;
        $sucursal = Auth::user()->sucursal;

        $compras = Compra::listarComprasPorEmpresaSucursal($empresa, $sucursal);

        $tipos_comprobantes = TipoComprobante::$comprobantes;

        return Datatables::of($compras)
            ->addColumn('action', function ($compra) {
                return '
                <a href="'.url('/compras/compra/' . $compra->id).'" class="mytooltip"><i class="fa fa-eye text-success m-r-10"></i>
                    <span class="tooltip-content3">Ver Comprobante</span>
                </a>
                <a class="delete mytooltip pointer" data-id="'.$compra->id.'">
                    <i class="fa fa-close text-danger"></i>
                    <span class="tooltip-content3">Eliminar Comprobante</span>
                </a>
                ';
            })
            ->setRowId('id')
            ->removeColumn('id')
            ->removeColumn('tipo_comprobante_id')
            ->removeColumn('signo')
            ->editColumn('fecha', function ($compra) {
                return $compra->fecha->format('d/m/Y');
            })
            ->editColumn('total_pago', function ($compra) {
                return $compra->signo.' '.$compra->total_pago;
            })
            ->editColumn('numero', function ($compra) use ($tipos_comprobantes) {
                return $tipos_comprobantes[$compra->tipo_comprobante_id].' '.$compra->numero;
            })
            ->make(true);
    }

    public function create($compra = null){
        if(isset(Auth::user()->empleado->caja)){
            if(Auth::user()->empleado->caja->estado != EstadoCaja::ABIERTA) {
                return redirect('compras/compra');
            }
        }

        $empresa = Auth::user()->empresa;
        $tipos_comprobantes = TipoComprobante::$arrayComboCompras;
        $monedas = Moneda::getMonedasPorEmpresaArray($empresa);
        $monedaPorDefecto = Moneda::where('empresa_id', $empresa->id)->whereNull('deleted_at')->where('cotizacion','=', 1)->first();

        $porcentajes_iva = PorcentajeIva::$arrayCombo;
        $formas_pagos = FormaDePago::$getArray;

        $tarjetas = TarjetaPropia::getTarjetasPorEmpresaArray($empresa);
        $cheques_terceros = Cheque::getChequesPorEmpresaArray($empresa, EstadoCheque::PENDIENTE);
        $cuentas_bancarias = CuentaBancaria::getCuentasBancariasPorEmpresaArray($empresa);
        $chequeras = Chequera::getChequerasPorEmpresaArray($empresa);

        return view('frontend.compras.compra.create', compact('compra','tipos_comprobantes', 'monedas', 'porcentajes_iva', 'formas_pagos', 'cuentas_bancarias', 'tarjetas', 'cheques_terceros', 'chequeras', 'monedaPorDefecto'));
    }

    public function store(Request $request){
        $this->validate($request, [
			'proveedor_id' => 'required',
			'moneda_id' => 'required',
			'fecha' => 'required',
			'numero' => 'required',
            'tipo_comprobante_id' => 'required',
			'punto_venta' => 'required',
			'total' => 'required'
		]);

        DB::transaction(function() use ($request) {
            $requestData = $request->all();
            $empresa = Auth::user()->empresa;
            $sucursal = Auth::user()->sucursal;

            $compra = New Compra($requestData);
            $date = DateTime::createFromFormat('d/m/Y', $requestData['fecha']);
            $compra->fecha = $date->format('Y-m-d H:i:s');
            $compra->estado = Estado::ACTIVO;
            $compra->empresa_id = $empresa->id;
            $compra->sucursal_id = Auth::user()->sucursal_id;
            if($compra->importe_neto == null)
                $compra->importe_neto = 0;
            if($compra->importe_iva == null)
                $compra->importe_iva = 0;
            $compra->save();

            $porcentajes_iva = PorcentajeIva::$valores;
            if(isset($requestData['detalles'])) {
                $detalles = $requestData['detalles'];
                foreach ($detalles as $detalle) {
                    if((int)$detalle['estado'] == Estado::ACTIVO && isset($detalle['cantidad'])){
                        if(!isset($detalle['articulo_id'])){
                            $nuevoArticulo = $this->crearNuevoArticulo($detalle, $compra->moneda);
                            $detalle['articulo_id'] = $nuevoArticulo->id;
                        }else{
                            $articulo = Articulo::findOrFail((int)$detalle['articulo_id']);
                            if(isset($requestData['actualizar_precios'])){
                                $this->actualizarPreciosArticulo($articulo, $detalle['precio_neto'], $compra->moneda);
                            }
                            if(isset($requestData['actualizar_stocks'])) {
                                if ($compra->tipo_comprobante_id > 2 && $compra->tipo_comprobante_id < 6) {//Es NC
                                    $articulo->descontarStock((int)$detalle['cantidad'],$sucursal);
                                } else {
                                    $articulo->sumarStock((int)$detalle['cantidad'],$sucursal);
                                }
                            }
                        }

                        $this->crearNuevoDetalleArticulo($detalle, $requestData['proveedor_id'], $requestData['moneda_id']);

                        $nuevoDetalle = New DetalleCompra($detalle);
                        $nuevoDetalle->compra_id = $compra->id;
                        $nuevoDetalle->precio_iva = $detalle['precio_neto'] * ($porcentajes_iva[$detalle['porcentaje_iva']] / 100);
                        $nuevoDetalle->save();
                    }
                }
            }

            if(isset($requestData['detalles_pago'])) {
                $detalles_pago = $requestData['detalles_pago'];
                $controllerMovimientoCaja = new MovimientoCajaController();
                foreach ($detalles_pago as $pago) {
                    if ((int)$pago['estado'] == Estado::ACTIVO) {
                        $nuevoPago = New DetallePagoCompra($pago);
                        $nuevoPago->fecha = $date;
                        $nuevoPago->compra_id = $compra->id;
                        $nuevoPago->estado = Estado::ACTIVO;

                        if($compra->tipo_comprobante_id > 2 && $compra->tipo_comprobante_id < 6){//Es NC
                            $controllerMovimientoCaja->nuevaEntrada($compra, 'compras/compra', (int)$pago['forma_pago'], ConceptoDeCaja::NOTA_CREDITO, (float)$pago['monto']);
                        }else{
                            $controllerMovimientoCaja->nuevaSalida($compra, 'compras/compra', (int)$pago['forma_pago'], ConceptoDeCaja::COMPRA, (float)$pago['monto']);
                        }

                        if((int)$pago['forma_pago'] == FormaDePago::EFECTIVO) {
                            $nuevoPago->descripcion = 'Pago en Efectivo';
                            $nuevoPago->forma_pago = FormaDePago::EFECTIVO;
                        }else if((int)$pago['forma_pago'] == FormaDePago::MERCADO_PAGO) {
                            $nuevoPago->descripcion = 'Pago en Mercado Pago';
                            $nuevoPago->forma_pago = FormaDePago::MERCADO_PAGO;
                        }else if((int)$pago['forma_pago'] == FormaDePago::CHEQUE) {//Cheque de terceros
                            $cheque = Cheque::findOrFail((int)$pago['cheque_id']);

                            $cheque->estado = EstadoCheque::ENTREGADO;
                            $cheque->save();

                            $nuevoPago->entidad_id = $cheque->id;
                            $nuevoPago->descripcion = 'Pago con Cheque';
                            $nuevoPago->forma_pago = FormaDePago::CHEQUE;
                        }else if((int)$pago['forma_pago'] == FormaDePago::CUENTA_BANCARIA) {
                            $nuevoMovimientoBancario = new MovimientoBancario($pago);
                            $nuevoMovimientoBancario->importe = (float)$pago['monto'];
                            $nuevoMovimientoBancario->cuenta_bancaria_origen_id = $pago['cuenta_bancaria_id'];
                            $nuevoMovimientoBancario->moneda_id = $compra->moneda_id;
                            $nuevoMovimientoBancario->tipo = TipoMovimiento::TRANSFERENCIA;
                            $nuevoMovimientoBancario->fecha = $date;
                            $nuevoMovimientoBancario->empresa_id = $empresa->id;
                            $nuevoMovimientoBancario->entidad_id = $compra->id;
                            $nuevoMovimientoBancario->entidad_clase = class_basename($compra);
                            $nuevoMovimientoBancario->url = 'compras/compra';
                            $nuevoMovimientoBancario->save();

                            $nuevoPago->entidad_id = $nuevoMovimientoBancario->id;
                            $nuevoPago->descripcion = 'Pago con Tranferencia Bancaria';
                            $nuevoPago->forma_pago = FormaDePago::CUENTA_BANCARIA;
                        }else if((int)$pago['forma_pago'] == FormaDePago::TARJETA) {
                            $nuevoPago->descripcion = 'Pago con Tarjeta';
                            $nuevoPago->forma_pago = FormaDePago::TARJETA;
                        }else if((int)$pago['forma_pago'] == FormaDePago::CUENTA_CORRIENTE) {
                            $controllerCuentaCorriente = new CuentaCorrienteController();
                            if($compra->tipo_comprobante_id > 2 && $compra->tipo_comprobante_id < 6) {//Es NC
                                $controllerCuentaCorriente->debe($compra, $request, null, $pago['monto']);
                            }else{
                                $controllerCuentaCorriente->haber($compra, $request, null, $pago['monto']);
                            }


                            $nuevoPago->descripcion = 'Pago en Cuenta Corriente';
                            $nuevoPago->forma_pago = FormaDePago::CUENTA_CORRIENTE;
                        }else if((int)$pago['forma_pago'] == FormaDePago::CHEQUE_PROPIO) {
                            $chequePropio = new ChequePropio($pago);
                            if(isset($pago['emision'])) {
                                $date = DateTime::createFromFormat('d/m/Y', $pago['emision']);
                                $chequePropio->emision = $date->format('Y-m-d H:i:s');
                            }
                            if(isset($pago['vencimiento'])){
                                $date = DateTime::createFromFormat('d/m/Y', $pago['vencimiento']);
                                $chequePropio->vencimiento = $date->format('Y-m-d H:i:s');

                                $evento = New EventsController();
                                $evento->crearEvento($chequePropio->vencimiento,'Vencimiento de Cheque Propio Nº '.$pago['numero']);
                            }
                            $chequePropio->estado = EstadoCheque::ENTREGADO;
                            $chequePropio->cuenta_bancaria_id = $chequePropio->chequera->cuenta_bancaria_id;
                            $chequePropio->empresa_id = $empresa->id;
                            $chequePropio->save();

                            $nuevoPago->descripcion = 'Pago con Cheque Propio Nº '.$chequePropio->numero;
                            $nuevoPago->forma_pago = FormaDePago::CHEQUE_PROPIO;
                        }
                        $nuevoPago->save();
                    }
                }
            }

            $controllerHistorial = new HistorialController();
            $controllerHistorial->nuevoHistorialCreacion($compra, $request);
        });

        return redirect('compras/compra')->withFlashSuccess('Compra creada correctamente.');
    }

    public function show($id){
        $compra = Compra::findOrFail($id);

        if ($compra->empresa_id == Auth::user()->empresa_id) {
            $tipos_comprobantes = TipoComprobante::$comprobantes;
            $porcentajes_iva = PorcentajeIva::$arrayCombo;
            return view('frontend.compras.compra.show', compact('compra', 'tipos_comprobantes', 'porcentajes_iva'));
        }
        return redirect('compras/compra');
    }

    public function edit($id){
        $compra = Compra::findOrFail($id);
        $empresa = Auth::user()->empresa;
        $editar = TRUE;

        if ($compra->empresa == $empresa) {
            $tipos_comprobantes = TipoComprobante::$arrayComboFacturas;
            $monedas = Moneda::getMonedasPorEmpresaArray($empresa);

            $porcentajes_iva = PorcentajeIva::$arrayCombo;
            $formas_pagos = [0 => 'Efectivo', 1 => 'Cheques de Terceros', 2 => 'Cuenta Bancaria', 3 => 'Tarjeta', 5 => 'Cheque Propio'];

            $tarjetas = TarjetaPropia::getTarjetasPorEmpresaArray($empresa);
            $cheques_terceros = Cheque::getChequesPorEmpresaArray($empresa, EstadoCheque::PENDIENTE);
            $cuentas_bancarias =  CuentaBancaria::getCuentasBancariasPorEmpresaArray($empresa);
            $chequeras = Chequera::getChequerasPorEmpresaArray($empresa);

            return view('frontend.compras.compra.edit', compact('compra', 'tipos_comprobantes', 'monedas', 'porcentajes_iva', 'formas_pagos', 'tarjetas', 'cuentas_bancarias', 'cheques_terceros', 'chequeras', 'editar'));
        }
        return redirect('compras/compra');
    }

    public function update($id, Request $request){
        $this->validate($request, [
            'moneda_id' => 'required',
            'punto_venta' => 'required',
            'total' => 'required'
        ]);

        $compra = Compra::findOrFail($id);

        if (!$compra->empresa_id == Auth::user()->empresa_id) {
            return redirect('ventas/venta');
        }else{
            if (Auth::user()->hasRole('Usuario Vendedor')) {
                if (!$compra->sucursal_id == Auth::user()->sucursal_id) {
                    return redirect('ventas/venta');
                }
            }
        }

        DB::transaction(function() use ($compra, $request) {
            $requestData = $request->all();
            $empresa = Auth::user()->empresa;
            $sucursal = Auth::user()->sucursal;

            $date = date("Y-m-d H:i:s");
            $compra->punto_venta = $requestData['punto_venta'];
//            $compra->moneda_id = $requestData['moneda_id'];
//            $compra->cotizacion = $requestData['cotizacion'];
            if($requestData['importe_neto'] != null)
                $compra->importe_neto = $requestData['importe_neto'];
            if($requestData['importe_iva'] != null)
                $compra->importe_iva = $requestData['importe_iva'];
            $compra->impuesto = $requestData['impuesto'];
            $compra->total = $requestData['total'];
            $compra->descripcion = $requestData['descripcion'];
            $compra->update();

            $porcentajes_iva = PorcentajeIva::$valores;
            if(isset($requestData['detalles'])){
                $detalles = $requestData['detalles'];
                foreach ($detalles as $detalle) {
                    if((int)$detalle['id'] > 0){//Editar cantidad de detalle o borrarlo
                        if((int)$detalle['estado'] == Estado::ACTIVO && isset($detalle['cantidad']) && isset($detalle['articulo_id'])){
                            $detalleGuardado = $compra->detalles->find((int)$detalle['id']);
                            $stockArticulo = $detalleGuardado->articulo->stockPorSucursal($sucursal);

                            $diferencia = $detalle['cantidad'] - $detalleGuardado->cantidad;
                            if($diferencia > 0){//Agregar difencia a stock
                                $stockArticulo->cantidad += $diferencia;
                                $stockArticulo->update();
                            }elseif($diferencia < 0){//Restar difencia a stock
                                $stockArticulo->cantidad += $diferencia;
                                $stockArticulo->update();
                            }
                            $detalleGuardado->cantidad = $detalle['cantidad'];

                            if(isset($requestData['actualizar_precios'])){
                                $this->actualizarPreciosArticulo($detalleGuardado->articulo, $detalle['precio_neto'], $compra->moneda);
                            }

                            $detalleGuardado->update();
                        }else{
                            $detalleGuardado = $compra->detalles->find((int)$detalle['id']);

                            $stockArticulo = $detalleGuardado->articulo->stockPorSucursal($sucursal);
                            $stockArticulo->cantidad -= $detalle['cantidad'];
                            $stockArticulo->update();

                            $detalleGuardado->delete();
                        }
                    }else{
                        if((int)$detalle['estado'] == Estado::ACTIVO && isset($detalle['cantidad'])){
                            if(!isset($detalle['articulo_id'])){
                                $nuevoArticulo = $this->crearNuevoArticulo($detalle, $compra->moneda);
                                $detalle['articulo_id'] = $nuevoArticulo->id;
                            }else{
                                $articulo = Articulo::findOrFail((int)$detalle['articulo_id']);
                                $stockArticulo = $articulo->stockPorSucursal($sucursal);
                                if(isset($stockArticulo)){
                                    $stockArticulo->cantidad += (int)$detalle['cantidad'];
                                    $stockArticulo->update();
                                }
                            }

                            $this->crearNuevoDetalleArticulo($detalle, $requestData['proveedor_id'], $requestData['moneda_id']);

                            $nuevoDetalle = New DetalleCompra($detalle);
                            $nuevoDetalle->compra_id = $compra->id;
                            $nuevoDetalle->precio_iva = $detalle['precio_neto'] * ($porcentajes_iva[$detalle['porcentaje_iva']] / 100);
                            $nuevoDetalle->save();
                        }
                    }
                }
            }

            if(isset($requestData['detalles_pago'])) {
                $detalles_pago = $requestData['detalles_pago'];
                $controllerMovimientoCaja = new MovimientoCajaController();
                foreach ($detalles_pago as $pago) {
                    if((int)$pago['id'] > 0){//Borrado de detalles
                        if((int)$pago['estado'] == Estado::BORRADO){
                            DetallePagoCompra::destroy((int)$pago['id']);
                        }
                    }else{//Guardar nuevos detalles
                        if((int)$pago['estado'] == Estado::ACTIVO) {
                            $nuevoPago = New DetallePagoCompra($pago);
                            $nuevoPago->fecha = $date;
                            $nuevoPago->compra_id = $compra->id;
                            $nuevoPago->estado = Estado::ACTIVO;

                            $controllerMovimientoCaja->nuevaSalida($compra, 'compras/compra', (int)$pago['forma_pago'], ConceptoDeCaja::COMPRA, (float)$pago['monto']);
                            if((int)$pago['forma_pago'] == FormaDePago::EFECTIVO) {
                                $nuevoPago->descripcion = 'Pago en Efectivo';
                                $nuevoPago->forma_pago = FormaDePago::EFECTIVO;
                            }else if((int)$pago['forma_pago'] == FormaDePago::MERCADO_PAGO) {
                                $nuevoPago->descripcion = 'Pago en Mercado Pago';
                                $nuevoPago->forma_pago = FormaDePago::MERCADO_PAGO;
                            }else if((int)$pago['forma_pago'] == FormaDePago::CHEQUE) {
                                $cheque = Cheque::findOrFail((int)$pago['cheque_id']);

                                $cheque->estado = EstadoCheque::ENTREGADO;
                                $cheque->save();

                                $nuevoPago->entidad_id = $cheque->id;
                                $nuevoPago->descripcion = 'Pago con Cheque';
                                $nuevoPago->forma_pago = FormaDePago::CHEQUE;
                            }else if((int)$pago['forma_pago'] == FormaDePago::CUENTA_BANCARIA) {
                                $nuevoMovimientoBancario = new MovimientoBancario($pago);
                                $nuevoMovimientoBancario->importe = (float)$pago['monto'];
                                $nuevoMovimientoBancario->cuenta_bancaria_origen_id = $pago['cuenta_bancaria_id'];
                                $nuevoMovimientoBancario->moneda_id = $compra->moneda_id;
                                $nuevoMovimientoBancario->tipo = TipoMovimiento::TRANSFERENCIA;
                                $nuevoMovimientoBancario->fecha = $date;
                                $nuevoMovimientoBancario->empresa = $empresa->id;
                                $nuevoMovimientoBancario->entidad_id = $compra->id;
                                $nuevoMovimientoBancario->entidad_clase = class_basename($compra);
                                $nuevoMovimientoBancario->url = 'compras/compra';
                                $nuevoMovimientoBancario->save();

                                $nuevoPago->entidad_id = $nuevoMovimientoBancario->id;
                                $nuevoPago->descripcion = 'Pago con Tranferencia Bancaria';
                                $nuevoPago->forma_pago = FormaDePago::CUENTA_BANCARIA;
                            }else if((int)$pago['forma_pago'] == FormaDePago::TARJETA) {
                                $nuevoPago->descripcion = 'Pago con Tarjeta';
                                $nuevoPago->forma_pago = FormaDePago::TARJETA;
                            }else if((int)$pago['forma_pago'] == FormaDePago::CUENTA_CORRIENTE) {
                                $controllerCuentaCorriente = new CuentaCorrienteController();
                                $controllerCuentaCorriente->debe($compra, $request, null, $pago['monto']);

                                $nuevoPago->descripcion = 'Pago en Cuenta Corriente';
                                $nuevoPago->forma_pago = FormaDePago::CUENTA_CORRIENTE;
                            }else if((int)$pago['forma_pago'] == FormaDePago::CHEQUE_PROPIO) {
                                $chequePropio = new ChequePropio($pago);
                                if(isset($pago['emision'])) {
                                    $date = DateTime::createFromFormat('d/m/Y', $pago['emision']);
                                    $chequePropio->emision = $date->format('Y-m-d H:i:s');
                                }
                                if(isset($pago['vencimiento'])){
                                    $date = DateTime::createFromFormat('d/m/Y', $pago['vencimiento']);
                                    $chequePropio->vencimiento = $date->format('Y-m-d H:i:s');
                                }
                                $chequePropio->estado = EstadoCheque::ENTREGADO;
                                $chequePropio->cuenta_bancaria_id = $chequePropio->chequera->cuenta_bancaria_id;
                                $chequePropio->empresa_id = $empresa->id;
                                $chequePropio->save();

                                $nuevoPago->descripcion = 'Pago con Cheque Propio Nº '.$chequePropio->numero;
                                $nuevoPago->forma_pago = FormaDePago::CHEQUE_PROPIO;
                            }
                            $nuevoPago->save();
                        }
                    }
                }
            }

            $controllerHistorial = new HistorialController();
            $controllerHistorial->nuevoHistorialActualizacion($compra, $request);
        });

        return redirect('compras/compra')->withFlashSuccess('Compra modificada correctamente.');
    }

    public function destroy($id){
        $compra = Compra::findOrFail($id);
        $empresa = Auth::user()->empresa;

        if ($compra->empresa != $empresa) {
            return redirect('compras/compra');
        }else{
            if (Auth::user()->hasRole('Usuario Vendedor')) {
                if (!$compra->sucursal_id == Auth::user()->sucursal_id) {
                    return redirect('compras/compra');
                }
            }
        }

        DB::transaction(function() use ($compra) {
            $movimientosCaja = MovimientoCaja::where('entidad_clase', 'Compra')->where('entidad_id',$compra->id);
            $cierreCaja = CierreCaja::getCierreCajaPorMovimiento($movimientosCaja);
            if(!$cierreCaja){ // Si los movimientos no pertenecen a una caja ya cerrada los elimino
                $movimientosCaja->delete();
            }

            $sucursal = $compra->sucursal;
            if(!$compra->detalles->isEmpty() && $sucursal){
                foreach($compra->detalles as $detalle){
                    if(isset($detalle->articulo)){
                        $detalle->articulo->descontarStock($detalle->cantidad,$sucursal);
                    }
                    $detalle->delete();
                }
            }

            $compra->delete();

            $controllerHistorial = new HistorialController();
            $controllerHistorial->nuevoHistorialEliminacion($compra);
        });

        return redirect('compras/compra')->withFlashSuccess('Compra eliminada correctamente.');
    }

    private function crearNuevoDetalleArticulo($detalle, $proveedor_id, $moneda_id){
        $porcentajes_iva = PorcentajeIva::$arrayCombo;

        $detalleArticulo = new DetalleArticulo();
        $detalleArticulo->costo = (float)$detalle['precio_neto'];
        $detalleArticulo->cantidad = 1;
        $detalleArticulo->proveedor_id = $proveedor_id;
        $detalleArticulo->moneda_id = $moneda_id;
        $detalleArticulo->articulo_id = $detalle['articulo_id'];
        $detalleArticulo->importe_iva = $detalleArticulo->costo * $porcentajes_iva[(int)$detalle['porcentaje_iva']]/100;
        $detalleArticulo->save();
    }

    private function crearNuevoArticulo($detalle, $moneda){
        $empresa = Auth::user()->empresa;

        $nuevoArticulo = New Articulo();
        $nuevoArticulo->nombre = $detalle['articulo'];
        $nuevoArticulo->porcentaje_iva = (int)$detalle['porcentaje_iva'];
        $nuevoArticulo->ultimo_costo = (float)$detalle['precio_neto'];
        $nuevoArticulo->unidad_id = 2;//Unidad: Unidad
        $nuevoArticulo->cantidad = 1;
        $rubroOtro = Rubro::where('empresa_id', '=', $empresa->id)
            ->where('nombre', '=', 'Otro')->first();
        $nuevoArticulo->rubro_id = $rubroOtro->id;
        $subrubroOtro = Subrubro::where('empresa_id', '=', $empresa->id)
            ->where('nombre', '=', 'Otro')->first();
        $nuevoArticulo->subrubro_id = $subrubroOtro->id;
        $nuevoArticulo->empresa_id = $empresa->id;
        $nuevoArticulo->moneda_id = $moneda->id;
        $codigoArticulo = Articulo::where('empresa_id',$empresa->id)->max('codigo') + 1;
        $nuevoArticulo->codigo = $codigoArticulo;
        $nuevoArticulo->save();

        $stock = New Stock();
        $stock->cantidad = $detalle['cantidad'];
        $stock->maximo = 100;
        $stock->minimo = 20;
        $stock->alarma = true;
        $stock->articulo_id = $nuevoArticulo->id;
        $stock->sucursal_id = Auth::user()->sucursal_id;
        $stock->save();

        $listasPrecios = ListaPrecio::where('empresa_id',$empresa->id)->get();
        foreach($listasPrecios as $lista){
            $precioNuevo = new Precio();
            $precioNuevo->precio = ($nuevoArticulo->ultimo_costo * $lista->porcentaje)/100 + $nuevoArticulo->ultimo_costo;
            $precioNuevo->articulo_id = $nuevoArticulo->id;
            $precioNuevo->lista_precio_id = $lista->id;
            $precioNuevo->save();
        }

        return $nuevoArticulo;
    }

    private function actualizarPreciosArticulo ($articulo, $costo, $monedaCompra){
        $costoAnterior = $articulo->ultimo_costo;
        if($articulo->moneda == $monedaCompra) {
            $articulo->ultimo_costo = $costo;
        }else{
            if($articulo->moneda->cotizacion == 1){
                $articulo->ultimo_costo = $costo*$monedaCompra->cotizacion;
            }else{
                $articulo->ultimo_costo = $costo/$articulo->moneda->cotizacion;
            }
        }
        $articulo->update();

        $precios = Precio::where('articulo_id', '=', $articulo->id)
            ->whereNull('deleted_at')
            ->whereNull('fecha_hasta')
            ->get();
        foreach ($precios as $precio){
            $porcentaje = (($precio->precio - $costoAnterior)/$costoAnterior)*100; // Calculo el procentaje de ganancia que tenia en esa lista anteriormente
            $precio->precio = ($articulo->ultimo_costo * $porcentaje)/100 + $articulo->ultimo_costo;
            $precio->save();
        }
    }

    public function obtenerDatos(Request $request){
        $compra_id = $request->input('compra_id');
        $empresa = Auth::user()->empresa;

        $compra = Compra::where('empresa_id', $empresa->id)
            ->where('id', '=', $compra_id)
            ->first();
        if($compra == null){
            return response()->json('No existe.');
        }

        $resultado = $compra->punto_venta . '-' . $compra->numero . ' ' . $compra->proveedor;

        return response()->json($resultado);
    }

}
