<?php

namespace App\Http\Controllers\Frontend\Compras;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Frontend\Caja\MovimientoCajaController;
use App\Http\Controllers\Frontend\Configuraciones\HistorialController;
use App\Http\Controllers\Frontend\Ventas\CuentaCorrienteController;
use App\Models\Frontend\Caja\ConceptoDeCaja;
use App\Models\Frontend\Caja\EstadoCaja;
use App\Models\Frontend\Caja\FormaDePago;
use App\Models\Frontend\Compras\DetalleOrden;
use App\Models\Frontend\Compras\DetallePagoOrden;
use App\Models\Frontend\Compras\OrdenPago;
use App\Models\Frontend\Configuraciones\Estado;
use App\Models\Frontend\Configuraciones\Moneda;
use App\Models\Frontend\Configuraciones\Opcion;
use App\Models\Frontend\Configuraciones\TarjetaPropia;
use App\Models\Frontend\Fondos\Banco;
use App\Models\Frontend\Fondos\Cheque;
use App\Models\Frontend\Fondos\ChequePropio;
use App\Models\Frontend\Fondos\Chequera;
use App\Models\Frontend\Fondos\CuentaBancaria;
use App\Models\Frontend\Fondos\EstadoCheque;
use App\Models\Frontend\Fondos\MovimientoBancario;
use App\Models\Frontend\Fondos\TipoMovimiento;
use App\Models\Frontend\Personas\Proveedor;
use App\Models\Frontend\Ventas\CuentaCorriente;
use App\Models\Frontend\Ventas\EstadoCuentaCorriente;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\Datatables\Facades\Datatables;
use DateTime;
use Session;
use DB;

class OrdenPagoController extends Controller{

    public function index(){

        $estadoCaja = EstadoCaja::CERRADA;
        if(isset(Auth::user()->empleado->caja)){
            if(Auth::user()->empleado->caja->estado == EstadoCaja::ABIERTA) {
                $estadoCaja = EstadoCaja::ABIERTA;
            }
        }

        return view('frontend.compras.orden-pago.index', compact('estadoCaja'));
    }

    public function datos(){
        $empresa = Auth::user()->empresa;
        $sucursal = Auth::user()->sucursal;

        $ordenes = OrdenPago::listarOrdenesPorEmpresaSucursal($empresa, $sucursal);

        return Datatables::of($ordenes)
            ->addColumn('action', function ($orden) {
                return '
                <a href="'.url('/compras/orden-pago/' . $orden->id).'" class="mytooltip"><i class="fa fa-eye text-success m-r-10"></i>
                    <span class="tooltip-content3">Ver Orden de Pago</span>
                </a>
                <a class="delete mytooltip pointer" data-id="'.$orden->id.'">
                    <i class="fa fa-close text-danger"></i>
                    <span class="tooltip-content3">Eliminar Orden de Pago</span>
                </a>
                ';
            })
            ->setRowId('id')
            ->removeColumn('id')
            ->removeColumn('signo')
            ->editColumn('fecha', function ($orden) {
                return $orden->fecha->format('d/m/Y');
            })
            ->editColumn('importe', function ($orden) {
                return $orden->signo.' '.$orden->importe;
            })
            ->editColumn('numero', function ($orden) {
                return 'Orden Pago N° '.$orden->numero;
            })
            ->make(true);
    }

    public function create(){
        return redirect('compras/orden-pago');
    }

    public function store(Request $request){
        $this->validate($request, [
            'fecha' => 'required'
        ]);

        DB::transaction(function() use ($request) {
            $requestData = $request->all();


            $empresa = Auth::user()->empresa;
            $sucursal = Auth::user()->sucursal;

            $orden = new OrdenPago($requestData);
            $date = DateTime::createFromFormat('d/m/Y', $requestData['fecha']);
            $orden->fecha = $date->format('Y-m-d H:i:s');
            $orden->importe = $requestData['total'];
            $orden->empresa_id = $empresa->id;
            $orden->estado = Estado::ACTIVO;
            if(Auth::user()->hasRole('Usuario Vendedor')){
                $orden->sucursal_id = $sucursal->id;
            }
            $orden->save();

            $ultimo_codigo = str_pad((int)$orden->numero + 1, 8, '0', STR_PAD_LEFT);
            Opcion::setValorOpcion('numeracion_orden_pago', $ultimo_codigo, $empresa);

            $moneda = $orden->moneda;
            $moneda->cotizacion = $requestData['cotizacion'];
            $moneda->update();

            $auxiliarPago = (float)$requestData['total'];

            if(isset($requestData['detalles'])) {
                $detalles = $requestData['detalles'];
                foreach ($detalles as $detalle) {
                    if ((int)$detalle['estado'] == Estado::ACTIVO && $auxiliarPago > 0) {
                        $nuevoDetalle = New DetalleOrden($detalle);
                        $nuevoDetalle->orden_pago_id = $orden->id;
                        if ($detalle['tipo'] == 'cuentas_corrientes') {
                            $nuevoDetalle->cuenta_corriente_id = $detalle['id'];
                            $nuevoDetalle->descripcion = "Pago de Cuenta Corriente.";
                        } elseif ($detalle['tipo'] == 'notas_debitos') {
                            //$nuevoDetalle->nota_debito_id = $detalle['id'];
                            //$nuevoDetalle->descripcion = "Cobro de Noda de Débito.";
                        }

                        $cuentaCorriente = $nuevoDetalle->cuentaCorriente;
                        $saldo = (float)$cuentaCorriente->haber - (float)$cuentaCorriente->debe;
                        if ($auxiliarPago >= $saldo) {
                            $nuevoDetalle->monto = $saldo;
                            $cuentaCorriente->debe = $cuentaCorriente->haber;
                            $cuentaCorriente->estado = EstadoCuentaCorriente::FINALIZADA;
                            $auxiliarPago -= $saldo;
                        } else {
                            $nuevoDetalle->monto = $auxiliarPago;
                            $cuentaCorriente->debe += $auxiliarPago;
                            $auxiliarPago = 0;
                        }
                        $nuevoDetalle->save();
                        $cuentaCorriente->update();
                    }
                }
            }

            if($auxiliarPago > 0){
                $controllerCuentaCorriente = new CuentaCorrienteController();
                $cuentaCorriente = $controllerCuentaCorriente->debe($orden, $request, null, $auxiliarPago);


                $nuevoDetalle = New DetalleOrden();
                $nuevoDetalle->orden_pago_id = $orden->id;
                $nuevoDetalle->cuenta_corriente_id = $cuentaCorriente->id;
                $nuevoDetalle->descripcion = "Pago de Cuenta Corriente";
                $nuevoDetalle->monto = $auxiliarPago;
                $nuevoDetalle->save();
            }

            if(isset($requestData['detalles_pago'])) {
                $detallesPago = $requestData['detalles_pago'];
                $controllerMovimientoCaja = new MovimientoCajaController();
                foreach ($detallesPago as $pago) {
                    if ((int)$pago['estado'] == Estado::ACTIVO) {
                        $nuevoPago = New DetallePagoOrden($pago);
                        $nuevoPago->orden_pago_id = $orden->id;
                        $nuevoPago->estado = Estado::ACTIVO;

                        //TODO ver q hacer con el interes, cambiar este método, no ayuda...
                        $controllerMovimientoCaja->nuevaSalida($orden, 'compras/orden-pago', (int)$pago['forma_pago'], ConceptoDeCaja::PAGO_ORDEN, (float)$pago['monto']);
                        if((int)$pago['forma_pago'] == FormaDePago::EFECTIVO) {
                            $nuevoPago->descripcion = 'Pago en Efectivo';
                            $nuevoPago->forma_pago = FormaDePago::EFECTIVO;
                        }else if((int)$pago['forma_pago'] == FormaDePago::MERCADO_PAGO) {
                            $nuevoPago->descripcion = 'Pago en Mercado Pago';
                            $nuevoPago->forma_pago = FormaDePago::MERCADO_PAGO;
                        }else if((int)$pago['forma_pago'] == FormaDePago::CHEQUE) {
                            $cheque = Cheque::findOrFail((int)$pago['cheque_id']);

                            $cheque->estado = EstadoCheque::ENTREGADO;
                            $cheque->save();

                            $nuevoPago->entidad_id = $cheque->id;
                            $nuevoPago->descripcion = 'Pago con Cheque';
                            $nuevoPago->forma_pago = FormaDePago::CHEQUE;
                        }else if((int)$pago['forma_pago'] == FormaDePago::CUENTA_BANCARIA) {
                            $nuevoMovimientoBancario = new MovimientoBancario($pago);
                            $nuevoMovimientoBancario->importe = (float)$pago['monto'];
                            $nuevoMovimientoBancario->cuenta_bancaria_origen_id = $pago['cuenta_bancaria_id'];
                            $nuevoMovimientoBancario->moneda_id = $orden->moneda_id;
                            $nuevoMovimientoBancario->tipo = TipoMovimiento::TRANSFERENCIA;
                            $nuevoMovimientoBancario->fecha = $date;
                            $nuevoMovimientoBancario->empresa_id = $empresa->id;
                            $nuevoMovimientoBancario->entidad_id = $orden->id;
                            $nuevoMovimientoBancario->entidad_clase = class_basename($orden);
                            $nuevoMovimientoBancario->url = 'compras/orden-pago';
                            $nuevoMovimientoBancario->save();

                            $nuevoPago->entidad_id = $nuevoMovimientoBancario->id;
                            $nuevoPago->descripcion = 'Pago con Tranferencia Bancaria';
                            $nuevoPago->forma_pago = FormaDePago::CUENTA_BANCARIA;
                        }else if((int)$pago['forma_pago'] == FormaDePago::TARJETA) {
                            $nuevoPago->descripcion = 'Pago con Tarjeta';
                            $nuevoPago->forma_pago = FormaDePago::TARJETA;
                        }else if((int)$pago['forma_pago'] == FormaDePago::CHEQUE_PROPIO) {
                            $chequePropio = new ChequePropio($pago);
                            if(isset($pago['emision'])) {
                                $date = DateTime::createFromFormat('d/m/Y', $pago['emision']);
                                $chequePropio->emision = $date->format('Y-m-d H:i:s');
                            }
                            if(isset($pago['vencimiento'])){
                                $date = DateTime::createFromFormat('d/m/Y', $pago['vencimiento']);
                                $chequePropio->vencimiento = $date->format('Y-m-d H:i:s');
                            }
                            $chequePropio->estado = EstadoCheque::ENTREGADO;
                            $chequePropio->cuenta_bancaria_id = $chequePropio->chequera->cuenta_bancaria_id;
                            $chequePropio->empresa_id = $empresa->id;
                            $chequePropio->save();

                            $nuevoPago->descripcion = 'Pago con Cheque Propio Nº '.$chequePropio->numero;
                            $nuevoPago->forma_pago = FormaDePago::CHEQUE_PROPIO;
                        }
                        $nuevoPago->save();
                    }
                }
            }
            $controllerHistorial = new HistorialController();
            $controllerHistorial->nuevoHistorialCreacion($orden, $request);
        });
        return redirect('compras/orden-pago')->withFlashSuccess('Orden de Pago creado correctamente.');
    }

    public function show($id){
        $empresa = Auth::user()->empresa;

        $orden = OrdenPago::findOrFail($id);

        if ($orden->empresa == $empresa) {
            return view('frontend.compras.orden-pago.show', compact('orden'));
        }
        return redirect('compras/orden-pago');
    }

    public function edit($id){
        return redirect('compras/orden-pago');
    }

    public function update($id, Request $request){
        $this->validate($request, [
            'fecha' => 'required'
        ]);

        $requestData = $request->all();
        
        $orden = OrdenPago::findOrFail($id);
        $orden->update($requestData);

        Session::flash('flash_message', trans('labels.frontend.ordenpago').trans('alerts.frontend.updated').'!');

        return redirect('compras/orden-pago');
    }

    public function destroy($id){
        $empresa = Auth::user()->empresa;

        $orden = OrdenPago::findOrFail($id);

        if($orden->empresa == $empresa){
            $orden->delete();
            Session::flash('flash_success', 'Orden de Pago eliminado correctamente.');
        }
        return redirect('compras/orden-pago');
    }

    public function crearOrdenPagoProveedor($proveedor_id){
        if(isset(Auth::user()->empleado->caja)){
            if(Auth::user()->empleado->caja->estado != EstadoCaja::ABIERTA) {
                return redirect('compras/orden-pago');
            }
        }

        $empresa = Auth::user()->empresa;

        $proveedor = Proveedor::findOrFail($proveedor_id);

        if ($proveedor->empresa == $empresa) {
            $fecha_hoy = date('d/m/Y');
            $monedas = Moneda::getMonedasPorEmpresaArray($empresa);
            $monedaPorDefecto = Moneda::where('empresa_id', $empresa->id)->whereNull('deleted_at')->where('cotizacion','=', 1)->first();
            $numero_recibo = Opcion::getValorOpcion('numeracion_orden_pago', $empresa);
            $detalles = CuentaCorriente::getCuentasCorrientesParaOrdenDePago($proveedor);

            $formas_pagos = FormaDePago::$getArrayOrdenPago;
            $bancos = Banco::getBancosPorEmpresaArray($empresa);
            $tarjetas = TarjetaPropia::getTarjetasPorEmpresaArray($empresa);
            $cheques_terceros = Cheque::getChequesPorEmpresaArray($empresa, EstadoCheque::PENDIENTE);
            $cuentas_bancarias = CuentaBancaria::getCuentasBancariasPorEmpresaArray($empresa);
            $chequeras = Chequera::getChequerasPorEmpresaArray($empresa);

            //TODO combinar de alguna forma con las NC en la misma coleccion

            return view('frontend.compras.orden-pago.create', compact('proveedor', 'fecha_hoy', 'monedas', 'numero_recibo', 'detalles', 'formas_pagos', 'bancos', 'tarjetas', 'cheques_terceros', 'cuentas_bancarias', 'chequeras', 'monedaPorDefecto'));
        }

        return redirect('compras/orden-pago');
    }
}
