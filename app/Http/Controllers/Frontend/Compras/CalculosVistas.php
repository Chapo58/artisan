<?php

namespace App\Http\Controllers\Frontend\Compras;

use App\Http\Controllers\Controller;
use App\Models\Frontend\Configuraciones\Estado;
use App\Models\Frontend\Configuraciones\PorcentajeIva;
use App\Models\Frontend\Configuraciones\TipoComprobante;
use Illuminate\Http\Request;
use DB;
use Session;

class CalculosVistas extends Controller{

    protected $detalles;
    protected $importeDescuento;
    protected $tipoComprobante;
    protected $impuestosInternos;
    protected $retenciones;
    protected $impuestosNoGrabados;

    public function __construct(Request $request){
        $this->detalles = $request->detalles;
        $this->importeDescuento = $request->importe_descuento;
        $this->tipoComprobante = $request->tipo_comprobante_id;
        $this->impuestosInternos = $request->importe_impuesto_interno;
        $this->retenciones = $request->retencion;
        $this->impuestosNoGrabados = $request->concepto_no_gravado;
    }

    public function calcularTotales(Request $request){
        $calculos = New CalculosVistas($request);
        $resultados['netos'] = $calculos->calcularNetos();
        $resultados['ivas'] = $calculos->calcularIvas();
        $resultados['bruto'] = round($resultados['netos']['total'] + $resultados['ivas']['total'],2);
        $resultados['total'] = $calculos->calcularTotal();

        return response()->json($resultados);
    }

    public function calcularNetos(){
        $constantes_iva = PorcentajeIva::$constantes;

        $neto['total'] = 0;
        foreach($constantes_iva as $porcentajeIva){
            $neto[$porcentajeIva] = 0;
        }

        foreach ($this->detalles as $detalle) {
            if ((int)$detalle['estado'] == Estado::ACTIVO && isset($detalle['cantidad'])) {
                $neto[$detalle['porcentaje_iva']] += $detalle['precio_neto'] * $detalle['cantidad'];
                $neto['total'] += $detalle['precio_neto'] * $detalle['cantidad'];
            }
        }
        $neto['total'] = round($neto['total'],2);

        return $neto;
    }

    public function calcularIvas(){
        $constantes_iva = PorcentajeIva::$constantes;
        $porcentajes_iva = PorcentajeIva::$valores;

        $iva['total'] = 0;
        foreach($constantes_iva as $porcentajeIva){
            $iva[$porcentajeIva] = 0;
        }

        foreach ($this->detalles as $detalle) {
            if ((int)$detalle['estado'] == Estado::ACTIVO && isset($detalle['cantidad'])) {
                $importeNeto = $detalle['precio_neto'] * $detalle['cantidad'];
                $iva[$detalle['porcentaje_iva']] += $importeNeto * $porcentajes_iva[$detalle['porcentaje_iva']] / 100;
                $iva['total'] += round($importeNeto * $porcentajes_iva[$detalle['porcentaje_iva']] / 100,2);
            }
        }

        $iva['total'] = round($iva['total'],2);

        return $iva;
    }

    public function calcularTotal($interesesCobro = false){
        $porcentajes_iva = PorcentajeIva::$valores;
        $constantes_iva = PorcentajeIva::$constantes;
        $netos = $this->calcularNetos();
        $ivas = $this->calcularIvas();

        $porcentajeDescuento = (($this->importeDescuento * -1) / $netos['total']) * 100;
        $totalNeto = 0;
        $totalIva = 0;

        foreach($constantes_iva as $porcentajeIva) {
            $importeDescuento = ($netos[$porcentajeIva] * $porcentajeDescuento) / 100;
            $calcularNeto = $netos[$porcentajeIva] - $importeDescuento;
            $totalNeto += $calcularNeto;
            if ($this->tipoComprobante == TipoComprobante::FACTURA_A) {
                $calcularIva = $calcularNeto * $porcentajes_iva[$porcentajeIva] / 100;
                $totalIva += $calcularIva;
            } else {
                $totalIva = $ivas['total'];
            }
        }
        $total = $totalNeto + $totalIva + $this->impuestosInternos + $this->retenciones + $this->impuestosNoGrabados;
        return round($total,2);
    }

}
