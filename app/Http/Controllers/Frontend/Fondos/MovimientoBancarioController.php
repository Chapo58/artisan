<?php

namespace App\Http\Controllers\Frontend\Fondos;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Frontend\Caja\MovimientoCajaController;
use App\Models\Frontend\Fondos\MovimientoBancario;
use App\Models\Frontend\Fondos\CuentaBancaria;
use App\Models\Frontend\Caja\FormaDePago;
use App\Models\Frontend\Caja\ConceptoDeCaja;
use App\Models\Frontend\Configuraciones\Moneda;
use App\Models\Frontend\Fondos\TipoMovimiento;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;
use Carbon\Carbon;
use DB;

class MovimientoBancarioController extends Controller{

    public function index(){
        $empresa = Auth::user()->empresa;
        $movimientosBancarios = MovimientoBancario::listarMovimientosBancariosPorEmpresa($empresa);
        $tipos = [0 => 'Deposito', 1 => 'Extraccion', 2 => 'Transferencia'];

        return view('frontend.fondos.movimiento-bancario.index', compact('movimientosBancarios','tipos'));
    }

    public function create($tipoMovimiento){
        $empresa = Auth::user()->empresa;

        $cuentaBancaria = CuentaBancaria::getCuentasBancariasPorEmpresaArray($empresa);
        $monedas = Moneda::getMonedasPorEmpresaArray($empresa);

        $fecha = Carbon::now();

        return view('frontend.fondos.movimiento-bancario.create', compact('cuentaBancaria','monedas', 'fecha', 'tipoMovimiento'));
    }

    public function store(Request $request){
        $this->validate($request, [
          'importe' => 'required|numeric|min:1',
          'fecha' => 'required|date',
          'cuenta_bancaria_destino_id' => 'required|exists:cuentas_bancarias,id',
          'moneda_id' => 'required|exists:monedas,id'
        ]);
        $empresa = Auth::user()->empresa;

        $requestData = $request->all();
        $requestData['empresa_id'] = $empresa->id;
        $nuevoMovimiento = MovimientoBancario::create($requestData);

        $cuentaBancariaController = new CuentaBancariaController();
        $movimientoCaja = new MovimientoCajaController();

        if($request['tipo'] == TipoMovimiento::DEPOSITO){
            $cuentaBancariaController->deposito($request['cuenta_bancaria_destino_id'],$request['importe']);

            // Movimiento de entrada en cuenta bancaria
            $movimientoCaja->nuevaEntrada($nuevoMovimiento,'/fondos/movimiento-bancario',FormaDePago::CUENTA_BANCARIA,ConceptoDeCaja::INGRESOS_VARIOS);

            if($request['generarMovimiento']){
              // Movimiento de salida del efectivo
              $movimientoCaja->nuevaSalida($nuevoMovimiento,'/fondos/movimiento-bancario',FormaDePago::EFECTIVO,ConceptoDeCaja::EGRESOS_VARIOS);
            }
        } elseif($request['tipo'] == TipoMovimiento::EXTRACCION) {
            $cuentaBancariaController->extraccion($request['cuenta_bancaria_destino_id'],$request['importe']);
            // Movimiento de salida en cuenta bancaria
            $movimientoCaja->nuevaSalida($nuevoMovimiento,'/fondos/movimiento-bancario',FormaDePago::CUENTA_BANCARIA,ConceptoDeCaja::EGRESOS_VARIOS);

            if($request['generarMovimiento']){
              // Movimiento de entrada del efectivo
              $movimientoCaja->nuevaEntrada($nuevoMovimiento,'/fondos/movimiento-bancario',FormaDePago::EFECTIVO,ConceptoDeCaja::INGRESOS_VARIOS);
            }
        } else {
            $cuentaBancariaController->transferencia($request['cuenta_bancaria_origen_id'],$request['cuenta_bancaria_destino_id'],$request['importe']);

            // Movimiento de entrada en cuenta bancaria
            $movimientoCaja->nuevaEntrada($nuevoMovimiento,'/fondos/movimiento-bancario',FormaDePago::CUENTA_BANCARIA,ConceptoDeCaja::INGRESOS_VARIOS);
        }

        return redirect('fondos/movimiento-bancario')->withFlashSuccess('Movimiento Bancario creado correctamente.');
    }

    public function show($id){
        $movimientobancario = MovimientoBancario::findOrFail($id);
        $tipos = [0 => 'Deposito', 1 => 'Extraccion', 2 => 'Transferencia'];

        if($movimientobancario->banco->empresa_id == Auth::user()->empresa_id){
            return view('frontend.fondos.movimiento-bancario.show', compact('movimientobancario','tipos'));
        }
    }

    public function edit($id){
        $empresa = Auth::user()->empresa;

        $movimientoBancario = MovimientoBancario::findOrFail($id);

        $cuentaBancaria = CuentaBancaria::getCuentasBancariasPorEmpresaArray($empresa);
        $monedas = Moneda::getMonedasPorEmpresaArray($empresa);
        if($movimientoBancario->banco->empresa_id == Auth::user()->empresa_id){
            return view('frontend.fondos.movimiento-bancario.edit', compact('movimientoBancario','cuentaBancaria', 'monedas'));
        }

    }

    public function update($id, Request $request){
        $this->validate($request, [
    			'importe' => 'required|numeric|min:1',
    			'fecha' => 'required|date',
    			'cuenta_bancaria_destino_id' => 'required|exists:cuentas_bancarias,id',
    			'moneda_id' => 'required|exists:monedas,id'
    		]);
        $requestData = $request->all();

        $movimientobancario = MovimientoBancario::findOrFail($id);

        if($movimientobancario->banco->empresa_id == Auth::user()->empresa_id){
            $movimientobancario->update($requestData);

            $cuentaBancariaController = new CuentaBancariaController;

            if($request['tipo'] == TipoMovimiento::DEPOSITO){
                $cuentaBancariaController->deposito($request['cuenta_bancaria_destino_id'],$request['importe']);
            } elseif($request['tipo'] == TipoMovimiento::EXTRACCION) {
                $cuentaBancariaController->extraccion($request['cuenta_bancaria_destino_id'],$request['importe']);
            } else {
                $cuentaBancariaController->transferencia($request['cuenta_bancaria_origen_id'],$request['cuenta_bancaria_destino_id'],$request['importe']);
            }
            Session::flash('flash_success', 'Movimiento Bancario modificado correctamente.');
        }

        return redirect('fondos/movimiento-bancario');
    }

    public function destroy($id){
        $movimientobancario = MovimientoBancario::findOrFail($id);
        if($movimientobancario->banco->empresa_id == Auth::user()->empresa_id){
            MovimientoBancario::destroy($id);
            Session::flash('flash_success', 'Movimiento Bancario eliminado correctamente.');
        }

        return redirect('fondos/movimiento-bancario');
    }
}
