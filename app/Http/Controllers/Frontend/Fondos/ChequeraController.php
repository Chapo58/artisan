<?php

namespace App\Http\Controllers\Frontend\Fondos;

use App\Http\Controllers\Controller;
use App\Models\Frontend\Fondos\Chequera;
use App\Models\Frontend\Fondos\CuentaBancaria;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;
use DB;

class ChequeraController extends Controller{

    public function index(){
        $empresa = Auth::user()->empresa;

        $chequera = Chequera::join('cuentas_bancarias', 'cuentas_bancarias.id', '=', 'chequeras.cuenta_bancaria_id')
        ->join('bancos', 'bancos.id', '=', 'cuentas_bancarias.banco_id')
        ->select('chequeras.*')
        ->where('bancos.empresa_id', $empresa->id)
        ->get();

        return view('frontend.fondos.chequera.index', compact('chequera'));
    }

    public function create(){
        $empresa = Auth::user()->empresa;
        $cuentaBancaria = CuentaBancaria::getCuentasBancariasPorEmpresaArray($empresa);

        return view('frontend.fondos.chequera.create', compact('cuentaBancaria'));
    }

    public function store(Request $request){
        $this->validate($request, [
    			'cuenta_bancaria_id' => 'required|exists:cuentas_bancarias,id',
    			'desde' => 'required|min:0|integer',
    			'hasta' => 'required|min:0|integer'
    		]);
        $requestData = $request->all();

        Chequera::create($requestData);

        return redirect('fondos/chequera')->withFlashSuccess('Chequera modificada correctamente.');
    }

    public function show($id){
        $chequera = Chequera::findOrFail($id);
        if($chequera->empresa_id == Auth::user()->empresa_id){
            return view('frontend.fondos.chequera.show', compact('chequera'));
        }
    }

    public function edit($id){
        $empresa = Auth::user()->empresa;

        $chequera = Chequera::findOrFail($id);
        $cuentaBancaria = CuentaBancaria::getCuentasBancariasPorEmpresaArray($empresa);

        if($chequera->empresa_id == Auth::user()->empresa_id){
            return view('frontend.fondos.chequera.edit', compact('chequera','cuentaBancaria'));
        }
    }

    public function update($id, Request $request){
        $this->validate($request, [
          'cuenta_bancaria_id' => 'required|exists:cuentas_bancarias,id',
          'desde' => 'required|min:0|integer',
          'hasta' => 'required|min:0|integer'
        ]);
        $requestData = $request->all();

        $chequera = Chequera::findOrFail($id);
        if($chequera->empresa_id == Auth::user()->empresa_id){
            $chequera->update($requestData);
            Session::flash('flash_success', 'Chequera modificada correctamente.');
        }

        return redirect('fondos/chequera');
    }

    public function destroy($id){
        $chequera = Chequera::findOrFail($id);
        if($chequera->empresa_id == Auth::user()->empresa_id){
            Chequera::destroy($id);
            Session::flash('flash_success', 'Chequera eliminada correctamente.');
        }

        return redirect('fondos/chequera');
    }
}
