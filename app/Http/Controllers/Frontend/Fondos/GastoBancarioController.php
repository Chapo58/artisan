<?php

namespace App\Http\Controllers\Frontend\Fondos;

use App\Http\Controllers\Controller;
use App\Models\Frontend\Fondos\GastoBancario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;

class GastoBancarioController extends Controller{

    public function index(){
        $empresa = Auth::user()->empresa;
        $gastobancario = GastoBancario::where('empresa_id', $empresa->id)->get();

        return view('frontend.fondos.gasto-bancario.index', compact('gastobancario'));
    }

    public function create(){
        return view('frontend.fondos.gasto-bancario.create');
    }

    public function store(Request $request){
        $this->validate($request, [
    			'nombre' => 'required'
    		]);
        $requestData = $request->all();

        $nuevoGastoBancario = new GastoBancario($requestData);
        $nuevoGastoBancario->empresa_id = Auth::user()->empresa_id;
        $nuevoGastoBancario->save();

        return redirect('fondos/gasto-bancario')->withFlashSuccess('Gasto Bancario creado correctamente.');
    }

    public function show($id){
        $gastobancario = GastoBancario::findOrFail($id);
        if($gastobancario->empresa_id == Auth::user()->empresa_id){
            return view('frontend.fondos.gasto-bancario.show', compact('gastobancario'));
        }
    }

    public function edit($id){
        $gastobancario = GastoBancario::findOrFail($id);

        return view('frontend.fondos.gasto-bancario.edit', compact('gastobancario'));
    }

    public function update($id, Request $request){
        $this->validate($request, [
    			'nombre' => 'required'
    		]);
        $requestData = $request->all();

        $gastobancario = GastoBancario::findOrFail($id);
        if($gastobancario->empresa_id == Auth::user()->empresa_id){
            $gastobancario->update($requestData);
            Session::flash('flash_success', 'Gasto Bancario modificado correctamente.');
        }

        return redirect('fondos/gasto-bancario');
    }

    public function destroy($id){
        $gastobancario = GastoBancario::findOrFail($id);
        if($gastobancario->empresa_id == Auth::user()->empresa_id){
            GastoBancario::destroy($id);
            Session::flash('flash_success', 'Gasto Bancario eliminado correctamente.');
        }

        return redirect('fondos/gasto-bancario');
    }
}
