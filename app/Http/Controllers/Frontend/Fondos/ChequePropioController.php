<?php

namespace App\Http\Controllers\Frontend\Fondos;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Frontend\Calendario\EventsController;
use App\Http\Controllers\Frontend\Configuraciones\HistorialController;
use App\Models\Frontend\Fondos\ChequePropio;
use App\Models\Frontend\Fondos\Chequera;
use App\Models\Frontend\Fondos\CuentaBancaria;
use App\Models\Frontend\Fondos\EstadoCheque;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\HtmlString;
use Yajra\Datatables\Facades\Datatables;
use DateTime;
use Session;
use DB;

class ChequePropioController extends Controller{

    public function index(){
        return view('frontend.fondos.cheque-propio.index');
    }

    public function datos(){
        $empresa = Auth::user()->empresa;
        $chequesPropios = ChequePropio::listarChequesPorEmpresa($empresa);
        $estados = EstadoCheque::$getArray;

        return Datatables::of($chequesPropios)
            ->addColumn('action', function ($chequPropio) {
                return '
                <a href="'.url('/fondos/cheque-propio/' . $chequPropio->id).'" class="mytooltip">
                    <i class="fa fa-eye text-success m-r-10"></i>
                    <span class="tooltip-content3">Ver Cheque</span>
                </a>
                <a href="'.url('/fondos/cheque-propio/' . $chequPropio->id . '/edit').'" class="mytooltip">
                    <i class="fa fa-pencil text-info m-r-10"></i>
                    <span class="tooltip-content3">Modificar Cheque</span>
                </a>
                <a class="delete mytooltip pointer" data-id="'.$chequPropio->id.'">
                    <i class="fa fa-close text-danger"></i>
                    <span class="tooltip-content3">Eliminar Cheque</span>
                </a>
                ';
            })
            ->editColumn('importe', '$ {{$importe}}')
            ->editColumn('emision', function ($chequPropio) {
                if($chequPropio->emision){
                    return $chequPropio->emision->format('d/m/Y');
                }
            })
            ->editColumn('vencimiento', function ($chequPropio) {
                if($chequPropio->vencimiento){
                    return $chequPropio->vencimiento->format('d/m/Y');
                }
            })
            ->editColumn('estado', function ($chequPropio) use ($estados) {
                switch ($chequPropio->estado) {
                    case EstadoCheque::PENDIENTE:
                        return new HtmlString('<span class="label label-warning">'.$estados[$chequPropio->estado].'</span>');
                        break;
                    case EstadoCheque::COBRADO:
                        return new HtmlString('<span class="label label-success">'.$estados[$chequPropio->estado].'</span>');
                        break;
                    case EstadoCheque::DEPOSITADO:
                        return new HtmlString('<span class="label label-primary">'.$estados[$chequPropio->estado].'</span>');
                        break;
                    default:
                        return new HtmlString('<span class="label label-info">'.$estados[$chequPropio->estado].'</span>');
                }
            })
            ->setRowId('id')
            ->removeColumn('id')
            ->make(true);
    }

    public function create(){
        $empresa = Auth::user()->empresa;

        $fecha_hoy = date('d/m/Y');
        $cuentas_bancarias = CuentaBancaria::getCuentasBancariasPorEmpresaArray($empresa);

        return view('frontend.fondos.cheque-propio.create', compact('fecha_hoy', 'cuentas_bancarias'));
    }

    public function store(Request $request){
        $this->validate($request, [
            'cuenta_bancaria_id'    => 'required',
            'importe'               => 'required',
            'numero'                => 'required'
        ]);

        DB::transaction(function() use ($request) {
            $requestData = $request->all();
            $empresa = Auth::user()->empresa;

            $chequera = Chequera::select('chequeras.*')
                ->join('cuentas_bancarias', 'cuentas_bancarias.id', '=', 'chequeras.cuenta_bancaria_id')
                ->join('bancos', 'bancos.id', '=', 'cuentas_bancarias.banco_id')
                ->where('bancos.empresa_id','=', $empresa->id)
                ->whereRaw("CAST(chequeras.desde AS UNSIGNED) <= $request->numero")
                ->whereRaw("CAST(chequeras.hasta AS UNSIGNED) >= $request->numero")
                ->first();

            if(isset($request->emision)) {
                $date = DateTime::createFromFormat('d/m/Y', $requestData['emision']);
                $requestData['emision'] = $date->format('Y-m-d');
            }
            if(isset($request->vencimiento)){
                $date = DateTime::createFromFormat('d/m/Y', $requestData['vencimiento']);
                $requestData['vencimiento'] = $date->format('Y-m-d');

                $evento = New EventsController();
                $evento->crearEvento($requestData['vencimiento'],'Vencimiento de Cheque Propio Nº '.$request->numero);
            }
            $nuevoChequePropio = New ChequePropio($requestData);
            $nuevoChequePropio->empresa_id = $empresa->id;
            $nuevoChequePropio->estado = EstadoCheque::ENTREGADO;
            if($chequera){
                $nuevoChequePropio->chequera_id = $chequera->id;
            }
            $nuevoChequePropio->save();

            $controllerHistorial = new HistorialController();
            $controllerHistorial->nuevoHistorialCreacion($nuevoChequePropio, $request);

            Session::flash('flash_message', trans('labels.frontend.chequepropio').trans('alerts.frontend.created').'!');
        });

        return redirect('fondos/cheque-propio');
    }

    public function show($id){
        $chequePropio = ChequePropio::findOrFail($id);
        $empresa = Auth::user()->empresa;
        $estados = EstadoCheque::$getArray;

        if($chequePropio->empresa == $empresa){
            return view('frontend.fondos.cheque-propio.show', compact('chequePropio','estados'));
        }

        return redirect('fondos/cheque-propio');
    }

    public function edit($id){
        $chequePropio = ChequePropio::findOrFail($id);
        $empresa = Auth::user()->empresa;

        $fecha_hoy = date('d/m/Y');
        $cuentas_bancarias = CuentaBancaria::getCuentasBancariasPorEmpresaArray($empresa);

        if($chequePropio->empresa == $empresa){
            return view('frontend.fondos.cheque-propio.edit', compact('chequePropio','cuentas_bancarias', 'fecha_hoy'));

        }
    }

    public function update($id, Request $request){
        $this->validate($request, [
    			'importe' => 'required',
    			'numero' => 'required'
    		]);
        $requestData = $request->all();

        $chequepropio = ChequePropio::findOrFail($id);

        if(isset($request->emision)){
          $emision = DateTime::createFromFormat('d/m/Y', $requestData['emision']);
          $requestData['emision'] = $emision->format('Y-m-d');
        }

        if(isset($request->vencimiento)){
          $vencimiento = DateTime::createFromFormat('d/m/Y', $requestData['vencimiento']);
          $requestData['vencimiento'] = $vencimiento->format('Y-m-d');
        }

        if($chequepropio->empresa_id == Auth::user()->empresa_id){
            $chequepropio->update($requestData);
            Session::flash('flash_success', 'Cheque Propio modificado correctamente.');
        }

        return redirect('fondos/cheque-propio');
    }

    public function destroy($id){
        $chequepropio = ChequePropio::findOrFail($id);

        if($chequepropio->empresa_id == Auth::user()->empresa_id){
            ChequePropio::destroy($id);
            Session::flash('flash_success', 'Cheque Propio eliminado correctamente.');
        }

        return redirect('fondos/cheque-propio');
    }
}
