<?php

namespace App\Http\Controllers\Frontend\Fondos;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Frontend\Caja\MovimientoCajaController;
use App\Http\Controllers\Frontend\Calendario\EventsController;
use App\Http\Controllers\Frontend\Configuraciones\HistorialController;
use App\Models\Frontend\Fondos\Cheque;
use App\Models\Frontend\Fondos\CuentaBancaria;
use App\Models\Frontend\Caja\FormaDePago;
use App\Models\Frontend\Caja\ConceptoDeCaja;
use App\Models\Frontend\Fondos\Banco;
use App\Models\Frontend\Fondos\EstadoCheque;
use App\Models\Frontend\Fondos\MovimientoBancario;
use App\Models\Frontend\Fondos\TipoMovimiento;
use DateTime;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\HtmlString;
use Yajra\Datatables\Facades\Datatables;
use Carbon\Carbon;
use Session;
use DB;

class ChequeController extends Controller{

    public function index(){
        $empresa = Auth::user()->empresa;

        $importeTotal = Cheque::where('empresa_id', $empresa->id)->where('estado',EstadoCheque::PENDIENTE)->whereNull('deleted_at')->sum('importe');
        $cantidad = Cheque::where('empresa_id', $empresa->id)->where('estado',EstadoCheque::PENDIENTE)->whereNull('deleted_at')->count();

        $cuentas_bancarias = CuentaBancaria::getCuentasBancariasPorEmpresaArray($empresa);

        return view('frontend.fondos.cheque.index', compact('importeTotal', 'cantidad', 'cuentas_bancarias'));
    }

    public function datos(){
        $empresa = Auth::user()->empresa;
        $cheques = Cheque::select('cheques.id','cheques.numero','bancos.nombre as banco','cheques.importe','cheques.vencimiento','cheques.firmante','cheques.estado')
            ->leftJoin('bancos','bancos.id','=', 'cheques.banco_id')
            ->where('cheques.empresa_id','=', $empresa->id)
            ->orderBy('id','DESC')
            ->get();

        $cuentas_bancarias = CuentaBancaria::getCuentasBancariasPorEmpresaArray($empresa);
        $estados = EstadoCheque::$getArray;

        return Datatables::of($cheques)
            ->addColumn('action', function ($cheque) {
                $return = '
                 <div class="btn-group">
                    <button type="button" class="btn btn-secondary dropdown-toggle btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Acciones
                    </button>
                    <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 33px, 0px); top: 0px; left: 0px; will-change: transform;">
                        <a class="dropdown-item" href="'.url('/fondos/cheque/' . $cheque->id).'">
                            <i class="fa fa-eye" aria-hidden="true"></i> Ver Cheque
                        </a>';
                if($cheque->estado == EstadoCheque::PENDIENTE){
                    $return.='
                        <a class="dropdown-item" href="'.url('/fondos/cheque/'.$cheque->id. '/edit').'">
                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editar Cheque
                        </a>
                        <a class="dropdown-item cobrar" href="#" data-id="'.$cheque->id.'">
                            <i class="fa fa-usd" aria-hidden="true"></i> Cobrar Cheque
                        </a>
                        <a class="dropdown-item" href="#" data-backdrop="static" onclick="pasarId('.$cheque->id.')" data-toggle="modal" data-target="#modalDeposito">
                            <i class="fa fa-ticket" aria-hidden="true"></i> Depositar Cheque
                        </a>';
                }
                $return.='
                        <a class="dropdown-item delete" href="#" data-id="'.$cheque->id.'" data-toggle="tooltip">
                            <i class="fa fa-close"></i> Eliminar Cheque
                        </a>
                    </div>
                </div>
                ';
                return $return;
            })
            ->setRowId('id')
            ->removeColumn('id')
            ->editColumn('vencimiento', function ($cheque) {
                if($cheque->vencimiento){
                    return $cheque->vencimiento->format('d/m/Y');
                }
            })
            ->editColumn('estado', function ($cheque) use ($estados) {
                switch ($cheque->estado) {
                    case EstadoCheque::PENDIENTE:
                        return new HtmlString('<span class="label label-warning">'.$estados[$cheque->estado].'</span>');
                        break;
                    case EstadoCheque::COBRADO:
                        return new HtmlString('<span class="label label-success">'.$estados[$cheque->estado].'</span>');
                        break;
                    case EstadoCheque::DEPOSITADO:
                        return new HtmlString('<span class="label label-primary">'.$estados[$cheque->estado].'</span>');
                        break;
                    default:
                        return new HtmlString('<span class="label label-info">'.$estados[$cheque->estado].'</span>');
                }
            })
            ->make(true);
    }

    public function create(){
        $empresa = Auth::user()->empresa;

        $bancos = Banco::getBancosPorEmpresaArray($empresa);
        $fecha_hoy = date('d/m/Y');

        return view('frontend.fondos.cheque.create', compact('bancos','fecha_hoy'));
    }

    public function store(Request $request){
        $this->validate($request, [
            'importe' => 'required|min:1',
            'numero' => 'required',
    		'banco_id' => 'required',
            'firmante' => 'required'
        ]);

        DB::transaction(function() use ($request) {
            $requestData = $request->all();
            $nuevoCheque = $this->nuevoCheque($requestData);

            $entradaCheque = new MovimientoCajaController();
            $entradaCheque->nuevaEntrada($nuevoCheque, $request->getRequestUri(), formaDePago::CHEQUE, ConceptoDeCaja::INGRESOS_VARIOS);

            $controllerHistorial = new HistorialController();
            $controllerHistorial->nuevoHistorialCreacion($nuevoCheque, $request);

            Session::flash('flash_success', 'Cheque creado correctamente.');
        });

        return redirect('fondos/cheque');
    }

    public function show($id){
        $empresa = Auth::user()->empresa;

        $cheque = Cheque::findOrFail($id);
        if($cheque->empresa == $empresa){
            $estados = EstadoCheque::$getArray;
            $estadoPendiente = EstadoCheque::PENDIENTE;

            return view('frontend.fondos.cheque.show', compact('cheque','estados', 'estadoPendiente'));
        }

        return redirect('fondos/cheque');
    }

    public function edit($id){
        $empresa = Auth::user()->empresa;

        $cheque = Cheque::findOrFail($id);
        if($cheque->empresa == $empresa){
            $cheque = Cheque::findOrFail($id);
            $bancos = Banco::getBancosPorEmpresaArray($empresa);
            $fecha_hoy = date('d/m/Y');

            return view('frontend.fondos.cheque.edit', compact('cheque','bancos', 'fecha_hoy'));
        }

        return redirect('fondos/cheque');
    }

    public function update($id, Request $request){
        $this->validate($request, [
            'importe' => 'required|min:1',
            'numero' => 'required',
            'banco_id' => 'required',
            'firmante' => 'required'
        ]);
        $empresa = Auth::user()->empresa;
        $cheque = Cheque::findOrFail($id);

        if($cheque->empresa == $empresa){
            DB::transaction(function() use ($cheque, $request) {
                $requestData = $request->all();

                if(isset($request['emision'])) {
                    $date = DateTime::createFromFormat('d/m/Y', $requestData['emision']);
                    $cheque->emision = $date->format('Y-m-d H:i:s');
                }
                if(isset($request['vencimiento'])){
                    $date = DateTime::createFromFormat('d/m/Y', $requestData['vencimiento']);
                    $cheque->vencimiento = $date->format('Y-m-d H:i:s');
                }
                $cheque->importe = $requestData['importe'];
                $cheque->numero = $requestData['numero'];
                $cheque->banco_id = $requestData['banco_id'];
                $cheque->firmante = $requestData['firmante'];
                $cheque->sucursal = $requestData['sucursal'];
                $cheque->save();

                $entradaCheque = new MovimientoCajaController();
                $entradaCheque->nuevaEntrada($cheque, $request->getRequestUri(), formaDePago::CHEQUE, ConceptoDeCaja::INGRESOS_VARIOS);

                $controllerHistorial = new HistorialController();
                $controllerHistorial->nuevoHistorialActualizacion($cheque, $request);

                Session::flash('flash_success', 'Cheque modificado correctamente.');
            });
        }

        return redirect('fondos/cheque');
    }

    public function destroy($id){
        $empresa = Auth::user()->empresa;
        $cheque = Cheque::findOrFail($id);

        if($cheque->empresa == $empresa) {
            DB::transaction(function () use ($cheque) {
                $cheque->delete();

                $controllerHistorial = new HistorialController();
                $controllerHistorial->nuevoHistorialEliminacion($cheque);

                Session::flash('flash_success', 'Cheque eliminado correctamente.');
            });
        }

        return redirect('fondos/cheque');
    }

    public function nuevoCheque($request, $importe = 0){
        $nuevoCheque = new Cheque($request);
        if((float)$importe > 0){
            $nuevoCheque->importe = (float)$importe;
        }
        if(isset($request['emision'])) {
            $date = DateTime::createFromFormat('d/m/Y', $request['emision']);
            $nuevoCheque->emision = $date->format('Y-m-d H:i:s');
        }
        if(isset($request['vencimiento'])){
            $date = DateTime::createFromFormat('d/m/Y', $request['vencimiento']);
            $nuevoCheque->vencimiento = $date->format('Y-m-d H:i:s');
        }
        (isset($request['numero_cheque'])) ? $nuevoCheque->numero = $request['numero_cheque'] : $nuevoCheque->numero = $request['numero'];
        $nuevoCheque->estado = EstadoCheque::PENDIENTE;
        $nuevoCheque->empresa_id = Auth::user()->empresa->id;
        $nuevoCheque->caja_id = Auth::user()->empleado->caja_id;
        $nuevoCheque->save();

        if($nuevoCheque->vencimiento){
            $evento = New EventsController();
            $evento->crearEvento($nuevoCheque->vencimiento,'Vencimiento de Cheque Nº '.$nuevoCheque->numero);
        }

        return $nuevoCheque;
    }

    public function cobrar($id){
        $cheque = Cheque::findOrFail($id);
        $cheque->estado = EstadoCheque::COBRADO;
        $cheque->fecha_final = Carbon::now();
        $cheque->save();

        // Salida en Cheque de la caja
        $salidaCheque = new MovimientoCajaController();
        $salidaCheque->nuevaSalida($cheque,'/fondos/cheque/',FormaDePago::CHEQUE,ConceptoDeCaja::COBRO_CHEQUE);

        // Entrada en Efectivo en la caja
        $entradaEfectivo = new MovimientoCajaController();
        $entradaEfectivo->nuevaEntrada($cheque,'/fondos/cheque/',FormaDePago::EFECTIVO,ConceptoDeCaja::COBRO_CHEQUE);

        return redirect('caja/movimiento-caja');
    }

    public function depositar(Request $request){
        $idCheque = $request['idCheque'];
        $idCuentaBancaria = $request['cuenta_bancaria_id'];

        $empresa = Auth::user()->empresa;

        // Cambio de Estado del Cheque
        $cheque = Cheque::findOrFail($idCheque);
        $cheque->estado = EstadoCheque::DEPOSITADO;
        $cheque->fecha_final = Carbon::now();
        $cheque->save();

        // Generacion del movimiento bancario (deposito)
        $MovimientoBancario = new MovimientoBancario;
        $MovimientoBancario->importe = $cheque->importe;
        $MovimientoBancario->cuenta_bancaria_destino_id = $idCuentaBancaria;
        $MovimientoBancario->moneda_id = 1;
        $MovimientoBancario->tipo = TipoMovimiento::DEPOSITO;
        $MovimientoBancario->fecha = Carbon::now();
        $MovimientoBancario->comentario = "Deposito de Cheque N° ".$cheque->numero;
        $MovimientoBancario->entidad_id = $idCheque;
        $MovimientoBancario->empresa_id = $empresa->id;
        $MovimientoBancario->save();

        // Carga del deposito al saldo de la cuenta
        $cuentaBancariaController = new CuentaBancariaController;
        $cuentaBancariaController->deposito($idCuentaBancaria,$cheque->importe);

        // Salida en Cheque de la caja
        $salidaCheque = new MovimientoCajaController();
        $salidaCheque->nuevaSalida($cheque,'/fondos/cheques/',FormaDePago::CHEQUE,ConceptoDeCaja::DEPOSITO_CHEQUE);

        // Entrada en cuenta bancaria de la caja
        $entradaBanco = new MovimientoCajaController();
        $entradaBanco->nuevaEntrada($cheque,'/fondos/cheques/',FormaDePago::CUENTA_BANCARIA,ConceptoDeCaja::DEPOSITO_CHEQUE);

        return redirect('fondos/movimiento-bancario');
    }

    public function datosCheque(Request $request){
        $cheque_id = (int)$request->input('cheque_id');
        $empresa = Auth::user()->empresa;

        $cheque = Cheque::findOrFail($cheque_id);
        if ($cheque->empresa == $empresa) {
            $resultado = $cheque->importe;;
        }else{
            $resultado = 'Sin permisos';
        }
        return response()->json($resultado);
    }
}
