<?php

namespace App\Http\Controllers\Frontend\Fondos;

use App\Http\Controllers\Controller;
use App\Models\Frontend\Fondos\Banco;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;

class BancoController extends Controller{

    public function index(){
        $empresa = Auth::user()->empresa;

        $banco = Banco::where('empresa_id','=', $empresa->id)->get();

        return view('frontend.fondos.banco.index', compact('banco'));
    }

    public function create(){
        return view('frontend.fondos.banco.create');
    }

    public function store(Request $request){
        $this->validate($request, [
    			'nombre' => 'required'
    		]);
        $requestData = $request->all();

        $nuevoBanco = new Banco($requestData);
        $nuevoBanco->empresa_id = Auth::user()->empresa_id;

        $nuevoBanco->save();

        return redirect('fondos/banco')->withFlashSuccess('Banco creado correctamente.');
    }

    public function show($id){
        $banco = Banco::findOrFail($id);
        if($banco->empresa_id == Auth::user()->empresa_id){
            return view('frontend.fondos.banco.show', compact('banco'));
        }
    }

    public function edit($id){
        $banco = Banco::findOrFail($id);
        if($banco->empresa_id == Auth::user()->empresa_id){
            return view('frontend.fondos.banco.edit', compact('banco'));
        }
    }

    public function update($id, Request $request){
        $this->validate($request, [
    			'nombre' => 'required'
    		]);
        $requestData = $request->all();

        $banco = Banco::findOrFail($id);
        if($banco->empresa_id == Auth::user()->empresa_id){
            $banco->update($requestData);
        }

        return redirect('fondos/banco')->withFlashSuccess('Banco modificado correctamente.');
    }

    public function destroy($id){
        $banco = Banco::findOrFail($id);
        if($banco->empresa_id == Auth::user()->empresa_id){
            Banco::destroy($id);
        }

        return redirect('fondos/banco')->withFlashSuccess('Banco eliminado correctamente.');
    }
}
