<?php

namespace App\Http\Controllers\Frontend\Fondos;

use App\Http\Controllers\Controller;
use App\Models\Frontend\Fondos\Banco;
use App\Models\Frontend\Configuraciones\Moneda;
use App\Models\Frontend\Fondos\CuentaBancaria;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;

class CuentaBancariaController extends Controller{

    public function index(){
        $empresa = Auth::user()->empresa;
        $tipos = [1 => 'Cuenta Corriente', 2 => 'Caja de Ahorro'];

        $cuentasbancarias = CuentaBancaria::select('cuentas_bancarias.*')
        ->join('bancos', 'bancos.id', '=', 'cuentas_bancarias.banco_id')
        ->where('bancos.empresa_id', $empresa->id)
        ->get();

        return view('frontend.fondos.cuenta-bancaria.index', compact('cuentasbancarias','tipos'));
    }

    public function create(){
        $empresa = Auth::user()->empresa;

        $bancos = Banco::getBancosPorEmpresaArray($empresa);
        $monedas = Moneda::getMonedasPorEmpresaArray($empresa);
        $tipos = [1 => 'Cuenta Corriente', 2 => 'Caja de Ahorro'];

        return view('frontend.fondos.cuenta-bancaria.create', compact('bancos','monedas','tipos'));
    }

    public function store(Request $request){
        $this->validate($request, [
    			'numero' => 'required|integer',
    			'banco_id' => 'required|exists:bancos,id',
    			'tipo' => 'required',
    			'moneda_id' => 'required|exists:monedas,id'
    		]);
        $requestData = $request->all();

        CuentaBancaria::create($requestData);

        return redirect('fondos/cuenta-bancaria')->withFlashSuccess('Cuenta Bancaria creada correctamente.');

    }

    public function show($id){
        $cuentasbancarias = CuentaBancaria::findOrFail($id);
        $tipos = [1 => 'Cuenta Corriente', 2 => 'Caja de Ahorro'];

        if($cuentasbancarias->banco->empresa_id == Auth::user()->empresa_id){
            return view('frontend.fondos.cuenta-bancaria.show', compact('cuentasbancarias','tipos'));
        }
    }

    public function edit($id){
        $cuentasbancarias = CuentaBancaria::findOrFail($id);
        $empresa = Auth::user()->empresa;

        $bancos = Banco::getBancosPorEmpresaArray($empresa);
        $monedas = Moneda::getMonedasPorEmpresaArray($empresa);
        $tipos = [1 => 'Cuenta Corriente', 2 => 'Caja de Ahorro'];
        if($cuentasbancarias->banco->empresa_id == Auth::user()->empresa_id){
            return view('frontend.fondos.cuenta-bancaria.edit', compact('cuentasbancarias','bancos','monedas','tipos'));
        }
    }

    public function update($id, Request $request){
        $cuentasbancaria = CuentaBancaria::findOrFail($id);

        $this->validate($request, [
          'numero' => 'required|integer',
          'banco_id' => 'required|exists:bancos,id',
          'tipo' => 'required',
          'moneda_id' => 'required|exists:monedas,id'
        ]);
        $requestData = $request->all();

        if($cuentasbancaria->banco->empresa_id == Auth::user()->empresa_id){
            $cuentasbancaria->update($requestData);
            Session::flash('flash_success', 'Cuenta Bancaria modificada correctamente.');
        }

        return redirect('fondos/cuenta-bancaria');
    }

    public function deposito($id, $importe){
      CuentaBancaria::where('id', $id)->increment('saldo', $importe);
    }

    public function extraccion($id, $importe){
      CuentaBancaria::where('id', $id)->decrement('saldo', $importe);
    }

    public function transferencia($origen, $destino, $importe){
      CuentaBancaria::where('id', $origen)->decrement('saldo', $importe);
      CuentaBancaria::where('id', $destino)->increment('saldo', $importe);
    }


    public function destroy($id){
        $cuentasbancaria = CuentaBancaria::findOrFail($id);
        if($cuentasbancaria->banco->empresa_id == Auth::user()->empresa_id){
            CuentaBancaria::destroy($id);
            Session::flash('flash_success', 'Cuenta Bancaria eliminada correctamente.');
        }
        return redirect('fondos/cuenta-bancaria');
    }
}
