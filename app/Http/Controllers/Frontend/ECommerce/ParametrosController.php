<?php

namespace App\Http\Controllers\Frontend\ECommerce;

use App\Http\Controllers\Controller;
use App\Http\Controllers\ImageController;
use App\Models\Backend\Empresas\Ecommerce;
use App\Models\Frontend\Configuraciones\Localidad;
use App\Models\Frontend\Configuraciones\Moneda;
use App\Models\Frontend\Configuraciones\Opcion;
use App\Models\Frontend\Configuraciones\Pais;
use App\Models\Frontend\Configuraciones\Provincia;
use App\Models\Frontend\Personas\Domicilio;
use App\Models\Frontend\Ventas\ListaPrecio;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;

class ParametrosController extends Controller{

    public function index(){
        $empresa = Auth::user()->empresa;
        $paises = ['' => 'Seleccione una opción...'] + Pais::pluck('nombre', 'id')->all();
        $provincias = ['' => 'Seleccione una opción...'] + Provincia::orderBy('nombre')->pluck('nombre', 'id')->all();
        $lista_precios = ListaPrecio::getListasPorEmpresaArray($empresa);
        $lista_precios_defecto = Opcion::where('empresa_id', $empresa->id)
            ->where('nombre', 'lista_precios_defecto_id')
            ->first();
        $monedas = Moneda::getMonedasPorEmpresaArray($empresa);
        $monedaPorDefecto = Moneda::where('empresa_id', $empresa->id)->whereNull('deleted_at')->where('cotizacion','=', 1)->first();

        return view('frontend.ecommerce.parametros.index', compact('empresa','paises','provincias','localidades','lista_precios','lista_precios_defecto','monedaPorDefecto','monedas'));
    }

    public function guardar(Request $request){
        $this->validate($request, [
            'nombre' => 'required',
            'telefono' => 'required',
            'email' => 'required'
		]);

        $empresa = Auth::user()->empresa;
        $requestData = $request->all();

        if(!$requestData['domicilio']['localidad']){
            return redirect('ecommerce/parametros')->withFlashDanger('Debe ingresar su localidad en la pestaña "Información de Contacto"');
        }

        if(!isset($requestData['habilitado'])) $requestData['habilitado'] = 0;
        if(!isset($requestData['mostrar_precios'])) $requestData['mostrar_precios'] = 0;
        if(!isset($requestData['precio_final'])) $requestData['precio_final'] = 0;
        if(!isset($requestData['articulos_sin_stock'])) $requestData['articulos_sin_stock'] = 0;


        if($empresa->ecommerce){
            $controllerImagen = new ImageController();
            $imagen_url = $controllerImagen->guardarImagen($request,'/uploads/logos/',1000,700);

            if($imagen_url != false){
                $empresa->ecommerce->logo_url = $imagen_url;
            }

            if(!isset($empresa->ecommerce->domicilio)){
                $domicilio = new Domicilio($requestData['domicilio']);
                $domicilio->save();
                $empresa->ecommerce->domicilio_id = $domicilio->id;
            }else{
                $empresa->ecommerce->domicilio->update($requestData['domicilio']);
            }

            $empresa->ecommerce->save();
            $empresa->ecommerce->update($requestData);
        } else {
            $nuevoEcommerce = new Ecommerce($requestData);
            $nuevoEcommerce->empresa_id = $empresa->id;

            $controllerImagen = new ImageController();
            $imagen_url = $controllerImagen->guardarImagen($request,'/uploads/logos/',1000,700);

            if($imagen_url != false){
                $nuevoEcommerce->logo_url = $imagen_url;
            }

            $domicilio = new Domicilio($requestData['domicilio']);
            $domicilio->save();
            $nuevoEcommerce->domicilio_id = $domicilio->id;

            $nuevoEcommerce->save();
        }

        return redirect('ecommerce/parametros')->withFlashSuccess('Parametros del E-Commerce actualizados.');
    }

    public function downloadIndex(){
        $empresa = Auth::user()->empresa;

        $view = view('frontend.ecommerce.index_download')
                ->with(["empresa" => $empresa])
                ->render();

        header("Content-type: text/html");
        header("Content-Disposition: attachment; filename=index.html");
        return $view;
    }
}
