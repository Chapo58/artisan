<?php

namespace App\Http\Controllers\Frontend\ECommerce;

use App\Http\Controllers\Controller;
use App\Http\Controllers\ImageController;
use App\Models\Frontend\ECommerce\Slider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;

class SliderController extends Controller{

    public function index(){
        $empresa = Auth::user()->empresa;

        $slider = Slider::where('empresa_id','=', $empresa->id)->get();

        return view('frontend.ecommerce.slider.index', compact('slider'));
    }

    public function create(){
        return view('frontend.ecommerce.slider.create');
    }

    public function store(Request $request){
        $this->validate($request, [
			'image' => 'required'
		]);
        $requestData = $request->all();
        $empresa = Auth::user()->empresa;

        $slider = new Slider($requestData);
        $slider->empresa_id = $empresa->id;

        $controllerImagen = new ImageController();
        $imagen_url = $controllerImagen->guardarImagen($request,'/uploads/sliders/',1920,585);

        if($imagen_url != false){
            $slider->imagen_url = $imagen_url;
        }

        $slider->save();

        return redirect('ecommerce/slider')->withFlashSuccess('Slider creado correctamente.');
    }

    public function show($id){
        $slider = Slider::findOrFail($id);
        if($slider->empresa_id == Auth::user()->empresa_id){
            return view('frontend.ecommerce.slider.show', compact('slider'));
        }
    }

    public function edit($id){
        $slider = Slider::findOrFail($id);

        if($slider->empresa_id == Auth::user()->empresa_id){
            return view('frontend.ecommerce.slider.edit', compact('slider'));
        }
    }

    public function update($id, Request $request){
        $requestData = $request->all();
        
        $slider = Slider::findOrFail($id);

        if($slider->empresa_id == Auth::user()->empresa_id){
            $controllerImagen = new ImageController();
            $imagen_url = $controllerImagen->guardarImagen($request,'/uploads/sliders/',1920,585);

            if($imagen_url != false){
                $slider->imagen_url = $imagen_url;
            }

            $slider->update($requestData);
        }

        return redirect('ecommerce/slider')->withFlashSuccess('Slider modificadp correctamente.');
    }

    public function destroy($id){
        $slider = Slider::findOrFail($id);

        if($slider->empresa_id == Auth::user()->empresa_id){
            Slider::destroy($id);
        }

        return redirect('ecommerce/slider')->withFlashSuccess('Slider eliminado correctamente.');
    }
}
