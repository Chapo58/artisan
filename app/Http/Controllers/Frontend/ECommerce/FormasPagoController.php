<?php

namespace App\Http\Controllers\Frontend\ECommerce;

use App\Http\Controllers\Controller;
use App\Models\Backend\Planes\FormaPago;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;

class FormasPagoController extends Controller{

    public function index(){
        $empresa = Auth::user()->empresa;
        $formasPago = FormaPago::where('empresa_id',$empresa->id)->get();

        if($formasPago->isEmpty()){
            $nuevaFormaPago = New FormaPago();
            $nuevaFormaPago->tipo_forma_pago = 'Efectivo';
            $nuevaFormaPago->empresa_id = $empresa->id;
            $nuevaFormaPago->save();

            $nuevaFormaPago = New FormaPago();
            $nuevaFormaPago->tipo_forma_pago = 'Deposito';
            $nuevaFormaPago->empresa_id = $empresa->id;
            $nuevaFormaPago->save();
        }

        return view('frontend.ecommerce.formas-pago.index', compact('formasPago'));
    }

    public function show($id){
        $formaPago = FormaPago::findOrFail($id);

        if($formaPago->empresa_id == Auth::user()->empresa_id){
            return view('frontend.ecommerce.formas-pago.show', compact('formaPago'));
        }
    }

    public function edit($id){
        $formaPago = FormaPago::findOrFail($id);

        if($formaPago->empresa_id == Auth::user()->empresa_id){
            return view('frontend.ecommerce.formas-pago.edit', compact('formaPago'));
        }
    }

    public function update($id, Request $request){
        $requestData = $request->all();

        if(!isset($requestData['habilitado'])){
            $requestData['habilitado'] = 0;
        }

        $formaPago = FormaPago::findOrFail($id);
        if($formaPago->empresa_id == Auth::user()->empresa_id){
            $formaPago->update($requestData);
        }

        return redirect('ecommerce/formas-pago')->withFlashSuccess('Forma de Pago modificada correctamente.');
    }

}
