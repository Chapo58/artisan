<?php

namespace App\Http\Controllers\Frontend\ECommerce;

use App\Http\Controllers\Controller;
use App\Models\Frontend\ECommerce\Suscripcion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;

class SuscripcionesController extends Controller{

    public function index(){
        $empresa = Auth::user()->empresa;

        $suscripciones = Suscripcion::where('empresa_id','=', $empresa->id)->get();

        return view('frontend.ecommerce.suscripciones.index', compact('suscripciones'));
    }

    public function create(){
        return view('frontend.ecommerce.suscripciones.create');
    }

    public function store(Request $request){
        $this->validate($request, [
			'email' => 'required'
		]);
        $requestData = $request->all();
        $empresa = Auth::user()->empresa;

        $suscripcion = new Suscripcion($requestData);
        $suscripcion->empresa_id = $empresa->id;
        $suscripcion->save();

        return redirect('ecommerce/suscripciones')->withFlashSuccess('Suscripción creada correctamente.');
    }

    public function show($id){
        $suscripcion = Suscripcion::findOrFail($id);
        if($suscripcion->empresa_id == Auth::user()->empresa_id){
            return view('frontend.ecommerce.suscripciones.show', compact('suscripcion'));
        }
    }

    public function edit($id){
        $suscripcion = Suscripcion::findOrFail($id);

        if($suscripcion->empresa_id == Auth::user()->empresa_id){
            return view('frontend.ecommerce.suscripciones.edit', compact('suscripcion'));
        }
    }

    public function update($id, Request $request){
        $this->validate($request, [
			'email' => 'required'
		]);
        $requestData = $request->all();
        
        $suscripcion = Suscripcion::findOrFail($id);
        if($suscripcion->empresa_id == Auth::user()->empresa_id){
            $suscripcion->update($requestData);
        }

        return redirect('ecommerce/suscripciones')->withFlashSuccess('Suscripción modificada correctamente.');
    }

    public function destroy($id){
        $suscripcion = Suscripcion::findOrFail($id);
        if($suscripcion->empresa_id == Auth::user()->empresa_id){
            Suscripcion::destroy($id);
        }

        return redirect('ecommerce/suscripciones')->withFlashSuccess('Suscripción eliminada correctamente.');
    }
}
