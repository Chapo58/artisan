<?php

namespace App\Http\Controllers\Frontend\ECommerce;

use App\Http\Controllers\Controller;
use App\Http\Controllers\ImageController;
use App\Models\Frontend\ECommerce\Caracteristica;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;

class CaracteristicasController extends Controller{

    public function index(){
        $empresa = Auth::user()->empresa;

        $caracteristicas = Caracteristica::where('empresa_id','=', $empresa->id)->get();

        return view('frontend.ecommerce.caracteristicas.index', compact('caracteristicas'));
    }

    public function create(){
        return view('frontend.ecommerce.caracteristicas.create');
    }

    public function store(Request $request){
        $this->validate($request, [
			'titulo' => 'required'
		]);
        $requestData = $request->all();
        $empresa = Auth::user()->empresa;

        $caracteristica = new Caracteristica($requestData);
        $caracteristica->empresa_id = $empresa->id;

        $controllerImagen = new ImageController();
        $imagen_url = $controllerImagen->guardarImagen($request,'/uploads/caracteristicas/',200,200);

        if($imagen_url != false){
            $caracteristica->imagen_url = $imagen_url;
        }

        $caracteristica->save();

        return redirect('ecommerce/caracteristicas')->withFlashSuccess('Caracteristica de E-Commerce creada correctamente.');
    }

    public function show($id){
        $caracteristica = Caracteristica::findOrFail($id);

        if($caracteristica->empresa_id == Auth::user()->empresa_id){
            return view('frontend.ecommerce.caracteristicas.show', compact('caracteristica'));
        }
    }

    public function edit($id){
        $caracteristica = Caracteristica::findOrFail($id);

        if($caracteristica->empresa_id == Auth::user()->empresa_id){
            return view('frontend.ecommerce.caracteristicas.edit', compact('caracteristica'));
        }
    }

    public function update($id, Request $request){
        $this->validate($request, [
			'titulo' => 'required'
		]);
        $requestData = $request->all();
        $caracteristica = Caracteristica::findOrFail($id);

        if($caracteristica->empresa_id == Auth::user()->empresa_id){
            $controllerImagen = new ImageController();
            $imagen_url = $controllerImagen->guardarImagen($request,'/uploads/caracteristicas/',200,200);

            if($imagen_url != false){
                $caracteristica->imagen_url = $imagen_url;
            }

            $caracteristica->update($requestData);
        }

        return redirect('ecommerce/caracteristicas')->withFlashSuccess('Caracteristica modificada correctamente.');
    }

    public function destroy($id){
        $caracteristica = Caracteristica::findOrFail($id);

        if($caracteristica->empresa_id == Auth::user()->empresa_id){
            Caracteristica::destroy($id);
        }

        return redirect('ecommerce/caracteristicas')->withFlashSuccess('Caracteristica eliminada correctamente.');
    }
}
