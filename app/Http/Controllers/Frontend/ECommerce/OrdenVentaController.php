<?php

namespace App\Http\Controllers\Frontend\ECommerce;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Frontend\Configuraciones\HistorialController;
use App\Http\Controllers\Frontend\Ventas\VentaController;
use App\Models\Backend\Planes\FormaPago;
use App\Models\Frontend\Configuraciones\Estado;
use App\Models\Frontend\Configuraciones\Moneda;
use App\Models\Frontend\Configuraciones\Opcion;
use App\Models\Frontend\Configuraciones\PorcentajeIva;
use App\Models\Frontend\Ecommerce\DetalleOrdenVenta;
use App\Models\Frontend\ECommerce\OrdenVenta;
use App\Models\Frontend\Ventas\DetalleVenta;
use App\Models\Frontend\Ventas\ListaPrecio;
use App\Models\Frontend\Ventas\Presupuesto;
use App\Models\Frontend\Configuraciones\TipoComprobante;
use App\Models\Frontend\Ventas\Venta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\HtmlString;
use Yajra\Datatables\Facades\Datatables;
use Session;

class OrdenVentaController extends Controller{

    public function index(){
        return view('frontend.ecommerce.orden-venta.index');
    }

    public function datos(){
        $empresa = Auth::user()->empresa;

        $OrdenesVentas = OrdenVenta::listarOrdenesVentasPorEmpresa($empresa);

        return Datatables::of($OrdenesVentas)
            ->addColumn('action', function ($ordenVenta) {
                return '
                <a href="'.url('/ecommerce/orden-venta/' . $ordenVenta->id).'" class="mytooltip"><i class="fa fa-eye text-success m-r-10"></i>
                    <span class="tooltip-content3">Ver Orden</span>
                </a>
                <a href="'.url('/ecommerce/orden-venta/imprimir/' . $ordenVenta->id).'" target="_blank" class="mytooltip">
                    <i class="fa fa-print text-purple m-r-10"></i>
                    <span class="tooltip-content3">Imprimir Orden</span>
                </a>
                <a href="'.url('/ecommerce/orden-venta/generar-venta/' . $ordenVenta->id).'" target="_blank" class="mytooltip">
                    <i class="fa fa-copy text-warning m-r-10"></i>
                    <span class="tooltip-content3">Facturar Orden</span>
                </a>
                <a class="delete mytooltip pointer" data-id="'.$ordenVenta->id.'">
                    <i class="fa fa-close text-danger"></i>
                    <span class="tooltip-content3">Eliminar Orden</span>
                </a>
                ';
            })
            ->setRowId('id')
            ->removeColumn('id')
            ->removeColumn('signo')
            ->editColumn('created_at', function ($ordenVenta) {
                return $ordenVenta->created_at->format('d/m/Y H:i');
            })
            ->editColumn('total', function ($ordenVenta) {
                return $ordenVenta->signo.' '.$ordenVenta->total;
            })
            ->editColumn('estado', function ($ordenVenta) {
                if($ordenVenta->estado){
                    return new HtmlString( '<span class="text-success font-weight-bold">ACTIVO</span> ');
                } else {
                    return new HtmlString( '<span class="text-danger font-weight-bold">INACTIVO</span> ');
                }
            })
            ->make(true);
    }

    public function create(){
        $empresa = Auth::user()->empresa;

        $numero_orden_venta = Opcion::getValorOpcion('numeracion_orden_venta', Auth::user()->empresa);
        if($numero_orden_venta == null){
            $opcion = new Opcion();
            $opcion->empresa_id = $empresa->id;
            $opcion->nombre = 'numeracion_orden_venta';
            $opcion->presentacion  = 'Numeración Orden de Venta';
            $opcion->valor = '00000001';
            $opcion->save();

            $numero_orden_venta = $opcion->valor;
        }

        $monedas = Moneda::getMonedasPorEmpresaArray($empresa);
        $monedaPorDefecto = Moneda::where('empresa_id', $empresa->id)->whereNull('deleted_at')->where('cotizacion','=', 1)->first();
        $lista_precios = ListaPrecio::getListasPorEmpresaArray($empresa);
        $formas_pagos = FormaPago::getFormasPagoPorEmpresaArray($empresa);

        return view('frontend.ecommerce.orden-venta.create', compact( 'numero_orden_venta', 'monedas', 'lista_precios', 'monedaPorDefecto','formas_pagos'));
    }

    public function store(Request $request){
        $this->validate($request, [
            'cliente_id' => 'required',
            'moneda_id' => 'required',
            'numero' => 'required',
            'total' => 'required'
        ]);

        DB::transaction(function() use ($request) {
            $requestData = $request->all();

            $empresa = Auth::user()->empresa;

            $opcion = Opcion::where('empresa_id', Auth::user()->empresa_id)
                ->where('nombre', 'LIKE', 'numeracion_orden_venta')
                ->first();
            $numero_orden_venta = $opcion->valor;
            $opcion->valor = str_pad($opcion->valor + 1, 8, '0', STR_PAD_LEFT);
            $opcion->save();

            $ordenVenta = New OrdenVenta($requestData);
            $ordenVenta->estado = Estado::ACTIVO;
            $ordenVenta->empresa_id = $empresa->id;
            $ordenVenta->numero = $numero_orden_venta;
            if($ordenVenta->importe_neto == null)
                $ordenVenta->importe_neto = 0;
            if($ordenVenta->importe_iva == null)
                $ordenVenta->importe_iva = 0;
            $ordenVenta->save();

            $ordenVenta->moneda->cotizacion = $requestData['cotizacion'];
            $ordenVenta->moneda->save();

            $porcentajes_iva = PorcentajeIva::$valores;
            if(isset($requestData['detalles'])) {
                $detalles = $requestData['detalles'];
                foreach ($detalles as $detalle) {
                    if ((int)$detalle['estado'] == Estado::ACTIVO && isset($detalle['cantidad']) && isset($detalle['articulo_id'])) {
                        $nuevoDetalle = New DetalleOrdenVenta($detalle);
                        $nuevoDetalle->orden_venta_id = $ordenVenta->id;
                        $nuevoDetalle->precio_iva = $detalle['precio_neto'] * ($porcentajes_iva[$detalle['porcentaje_iva']] / 100);
                        $nuevoDetalle->save();
                    }
                }
            }

            $controllerHistorial = new HistorialController();
            $controllerHistorial->nuevoHistorialCreacion($ordenVenta, $request);

            Session::flash('flash_success', 'Orden de Venta creada correctamente.');
        });

        return redirect('ecommerce/orden-venta');
    }

    public function show($id){
        $ordenVenta = OrdenVenta::findOrFail($id);
        $empresa = Auth::user()->empresa;

        if ($ordenVenta->empresa_id == $empresa->id) {
            $porcentajes_iva = PorcentajeIva::$valores;
            return view('frontend.ecommerce.orden-venta.show', compact('ordenVenta', 'porcentajes_iva','empresa'));
        }

        return redirect('ecommerce/orden-venta');
    }

    public function edit($id){
        return redirect('ecommerce/orden-venta');
    }

    public function update($id, Request $request){
        return redirect('ecommerce/orden-venta');
    }

    public function destroy($id){
        $empresa = Auth::user()->empresa;
        $ordenVenta = OrdenVenta::findOrFail($id);

        if ($ordenVenta->empresa == $empresa) {
            if(!$ordenVenta->detalles->isEmpty()){
                foreach($ordenVenta->detalles as $detalle){
                    $detalle->delete();
                }
            }

            $ordenVenta->delete();

            Session::flash('flash_success', 'Orden de Venta eliminada correctamente.');
        }

        return redirect('ecommerce/orden-venta');
    }

    public function generarVenta($id){
        $empresa = Auth::user()->empresa;
        $ordenVenta = OrdenVenta::findOrFail($id);

        if ($ordenVenta->empresa == $empresa) {
            $nuevaVenta = new Venta();
            $nuevaVenta->cliente = $ordenVenta->cliente;
            $nuevaVenta->cliente_id = $ordenVenta->cliente_id;
            $nuevaVenta->listaPrecios = $ordenVenta->listaPrecio;
            $nuevaVenta->lista_precios_id = $ordenVenta->lista_precio_id;
            $nuevaVenta->fecha = null;
            $nuevaVenta->moneda = $ordenVenta->moneda;
            $nuevaVenta->moneda_id = $ordenVenta->moneda->id;
            $nuevaVenta->cotizacion = $ordenVenta->cotizacion;

            $detallesVenta = null;
            $aux = 1;
            foreach ($ordenVenta->detalles as $detalle){
                $nuevoDetalle = new DetalleVenta();
                $nuevoDetalle->id = $aux*-1;
                $nuevoDetalle->articulo = $detalle->articulo;
                $nuevoDetalle->articulo_id = $detalle->articulo_id;
                $nuevoDetalle->precio_neto = $detalle->precio_neto;
                $nuevoDetalle->porcentaje_iva = $detalle->porcentaje_iva;
                $nuevoDetalle->cantidad = $detalle->cantidad;
                $nuevoDetalle->descripcion = $detalle->descripcion;
                $nuevoDetalle->estado = $detalle->estado;
                $detallesVenta[$aux*-1] = $nuevoDetalle;
                $aux++;
            }
            $nuevaVenta->detalles = $detallesVenta;

            $ventaController = new VentaController();

            return $ventaController->create($nuevaVenta);
        }
    }

    public function imprimir($orden_venta_id){
        $empresa = Auth::user()->empresa;
        $ordenVenta = OrdenVenta::findOrFail($orden_venta_id);

        if ($ordenVenta->empresa_id == $empresa->id) {
            $porcentajes_iva = PorcentajeIva::$valores;

            $pdf = app('dompdf.wrapper');
            $pdf->getDomPDF()->set_option("enable_php", true);
            $pdf->loadView('frontend.ecommerce.orden-venta.imprimir-orden-venta',
                compact('ordenVenta', 'empresa', 'porcentajes_iva'));

            return $pdf->stream('Orden_de_Venta.pdf');
        }
    }

}
