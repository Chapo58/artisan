<?php

namespace App\Http\Controllers\Frontend\ECommerce;

use App\Http\Controllers\Controller;
use App\Http\Controllers\ImageController;
use App\Models\Frontend\ECommerce\Pagina;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;

class PaginasController extends Controller{

    public function index(){
        $empresa = Auth::user()->empresa;

        $paginas = Pagina::where('empresa_id','=', $empresa->id)->get();

        return view('frontend.ecommerce.paginas.index', compact('paginas'));
    }

    public function create(){
        return view('frontend.ecommerce.paginas.create');
    }

    public function store(Request $request){
        $this->validate($request, [
			'titulo' => 'required',
			'contenido' => 'required'
		]);
        $requestData = $request->all();
        $empresa = Auth::user()->empresa;

        $pagina = new Pagina($requestData);
        $pagina->empresa_id = $empresa->id;

        $controllerImagen = new ImageController();
        $imagen_url = $controllerImagen->guardarImagen($request,'/uploads/paginas/',432,520);

        if($imagen_url != false){
            $pagina->imagen_url = $imagen_url;
        }

        $pagina->save();

        return redirect('ecommerce/paginas')->withFlashSuccess('Pagina creada correctamente.');
    }

    public function show($id){
        $pagina = Pagina::findOrFail($id);

        if($pagina->empresa_id == Auth::user()->empresa_id){
            return view('frontend.ecommerce.paginas.show', compact('pagina'));
        }
    }

    public function edit($id){
        $pagina = Pagina::findOrFail($id);

        if($pagina->empresa_id == Auth::user()->empresa_id){
            return view('frontend.ecommerce.paginas.edit', compact('pagina'));
        }
    }

    public function update($id, Request $request){
        $this->validate($request, [
			'titulo' => 'required',
			'contenido' => 'required'
		]);
        $requestData = $request->all();
        
        $pagina = Pagina::findOrFail($id);
        if($pagina->empresa_id == Auth::user()->empresa_id){
            $controllerImagen = new ImageController();
            $imagen_url = $controllerImagen->guardarImagen($request,'/uploads/paginas/',432,520);

            if($imagen_url != false){
                $pagina->imagen_url = $imagen_url;
            }

            $pagina->update($requestData);
        }

        return redirect('ecommerce/paginas')->withFlashSuccess('Pagina modificada correctamente.');
    }

    public function destroy($id){
        $pagina = Pagina::findOrFail($id);

        if($pagina->empresa_id == Auth::user()->empresa_id){
            Pagina::destroy($id);
        }

        return redirect('ecommerce/paginas')->withFlashSuccess('Pagina eliminada correctamente.');
    }
}
