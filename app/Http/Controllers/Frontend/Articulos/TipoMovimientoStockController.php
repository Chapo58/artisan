<?php

namespace App\Http\Controllers\Frontend\Articulos;

use App\Http\Controllers\Controller;
use App\Models\Frontend\Articulos\TipoMovimientoStock;
use Illuminate\Http\Request;
use Session;

class TipoMovimientoStockController extends Controller
{

    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $tipomovimientostock = TipoMovimientoStock::where('nombre', 'LIKE', "%$keyword%")
				->orWhere('descripcion', 'LIKE', "%$keyword%")
				
                ->paginate($perPage);
        } else {
            $tipomovimientostock = TipoMovimientoStock::paginate($perPage);
        }

        return view('frontend.articulos.tipo-movimiento-stock.index', compact('tipomovimientostock'));
    }

    public function create()
    {
        return view('frontend.articulos.tipo-movimiento-stock.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
			'nombre' => 'required'
		]);
        $requestData = $request->all();
        
        TipoMovimientoStock::create($requestData);

        Session::flash('flash_message', trans('labels.frontend.tipomovimientostock').trans('alerts.frontend.created').'!');

        return redirect('articulos/tipo-movimiento-stock');
    }

    public function show($id)
    {
        $tipomovimientostock = TipoMovimientoStock::findOrFail($id);

        return view('frontend.articulos.tipo-movimiento-stock.show', compact('tipomovimientostock'));
    }

    public function edit($id)
    {
        $tipomovimientostock = TipoMovimientoStock::findOrFail($id);

        return view('frontend.articulos.tipo-movimiento-stock.edit', compact('tipomovimientostock'));
    }

    public function update($id, Request $request)
    {
        $this->validate($request, [
			'nombre' => 'required'
		]);
        $requestData = $request->all();
        
        $tipomovimientostock = TipoMovimientoStock::findOrFail($id);
        $tipomovimientostock->update($requestData);

        Session::flash('flash_message', trans('labels.frontend.tipomovimientostock').trans('alerts.frontend.updated').'!');

        return redirect('articulos/tipo-movimiento-stock');
    }

    public function destroy($id)
    {
        TipoMovimientoStock::destroy($id);

        Session::flash('flash_message',  trans('labels.frontend.tipomovimientostock').trans('alerts.frontend.deleted').'!');

        return redirect('articulos/tipo-movimiento-stock');
    }
}
