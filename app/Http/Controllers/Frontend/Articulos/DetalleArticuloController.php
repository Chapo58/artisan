<?php

namespace App\Http\Controllers\Frontend\Articulos;

use App\Http\Controllers\Controller;
use App\Models\Frontend\Articulos\DetalleArticulo;
use Illuminate\Http\Request;
use Session;

class DetalleArticuloController extends Controller
{

    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $detallearticulo = DetalleArticulo::where('costo', 'LIKE', "%$keyword%")
                ->paginate($perPage);
        } else {
            $detallearticulo = DetalleArticulo::paginate($perPage);
        }

        return view('frontend.articulos.detalle-articulo.index', compact('detallearticulo'));
    }

    public function create()
    {
        return view('frontend.articulos.detalle-articulo.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
			'costo' => 'required',
			'cantidad' => 'required',
			'proveedor_id' => 'required',
			'articulo_id' => 'required',
		]);
        $requestData = $request->all();
        
        DetalleArticulo::create($requestData);

        Session::flash('flash_message', trans('labels.frontend.detallearticulo').trans('alerts.frontend.created').'!');

        return redirect('articulos/detalle-articulo');
    }

    public function show($id)
    {
        $detallearticulo = DetalleArticulo::findOrFail($id);

        return view('frontend.articulos.detalle-articulo.show', compact('detallearticulo'));
    }

    public function edit($id)
    {
        $detallearticulo = DetalleArticulo::findOrFail($id);

        return view('frontend.articulos.detalle-articulo.edit', compact('detallearticulo'));
    }

    public function update($id, Request $request)
    {
        $this->validate($request, [
			'costo' => 'required',
			'cantidad' => 'required',
			'proveedor_id' => 'required',
			'articulo_id' => 'required',
		]);
        $requestData = $request->all();
        
        $detallearticulo = DetalleArticulo::findOrFail($id);
        $detallearticulo->update($requestData);

        Session::flash('flash_message', trans('labels.frontend.detallearticulo').trans('alerts.frontend.updated').'!');

        return redirect('articulos/detalle-articulo');
    }

    public function destroy($id)
    {
        DetalleArticulo::destroy($id);

        Session::flash('flash_message',  trans('labels.frontend.detallearticulo').trans('alerts.frontend.deleted').'!');

        return redirect('articulos/detalle-articulo');
    }
}
