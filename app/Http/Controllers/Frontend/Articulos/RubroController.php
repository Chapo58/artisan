<?php

namespace App\Http\Controllers\Frontend\Articulos;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Frontend\Configuraciones\HistorialController;
use App\Http\Controllers\ImageController;
use App\Models\Frontend\Articulos\Rubro;
use App\Models\Frontend\Articulos\Subrubro;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Facades\Datatables;
use Session;

class RubroController extends Controller{

    public function index(){
        return view('frontend.articulos.rubro.index');
    }

    public function datos(){
        $empresa = Auth::user()->empresa;
        $rubros = Rubro::select('id','nombre','descripcion')->where('empresa_id','=', $empresa->id)->get();
        return Datatables::of($rubros)
            ->addColumn('action', function ($rubro) {
                return '
                <a href="'.url('/articulos/rubro/' . $rubro->id).'" class="mytooltip"><i class="fa fa-eye text-success m-r-10"></i>
                    <span class="tooltip-content3">Ver Rubro</span>
                </a>
                <a href="'.url('/articulos/rubro/' . $rubro->id . '/edit').'" class="mytooltip">
                    <i class="fa fa-pencil text-info m-r-10"></i>
                    <span class="tooltip-content3">Modificar Rubro</span>
                </a>
                <a class="delete-rubro mytooltip pointer" data-id="'.$rubro->id.'">
                    <i class="fa fa-close text-danger m-r-10"></i>
                    <span class="tooltip-content3">Eliminar Rubro</span>
                </a>
                <a href="'.url('/articulos/subrubro/create/' . $rubro->id).'" class="mytooltip">
                    <i class="fa fa-plus text-primary"></i>
                    <span class="tooltip-content3">Agregar SubRubro</span>
                </a>
                ';
            })
            ->addColumn('details_url', function($rubro) {
                return route('frontend.articulos.SubRubrosData', ['id' => $rubro->id]);
            })
            ->setRowId('id')
            ->make(true);
    }

    public function create(){
        return view('frontend.articulos.rubro.create');
    }

    public function store(Request $request){
        $this->validate($request, [
			'nombre' => 'required'
		]);
        $requestData = $request->all();
        
        $nuevoRubro = new Rubro($requestData);
        $nuevoRubro->empresa_id = Auth::user()->empresa_id;

        $controllerImagen = new ImageController();
        $imagen_url = $controllerImagen->guardarImagen($request,'/uploads/rubros/',1920,187);
        if($imagen_url != false){
            $nuevoRubro->imagen_url = $imagen_url;
        }

        $nuevoRubro->save();

        $controllerHistorial = new HistorialController();
        $controllerHistorial->nuevoHistorialCreacion($nuevoRubro, $request);

        return redirect('articulos/rubro')->withFlashSuccess('Rubro creado correctamente.');
    }

    public function show($id){
        $rubro = Rubro::findOrFail($id);
        $subRubros = Subrubro::select('id','nombre','descripcion')->where('rubro_id', $rubro->id)->orderBy('nombre','ASC')->get();

        if($rubro->empresa_id == Auth::user()->empresa_id){
            return view('frontend.articulos.rubro.show', compact('rubro','subRubros'));
        }else{
            return redirect('articulos/rubro');
        }
    }

    public function edit($id){
        $rubro = Rubro::findOrFail($id);

        if($rubro->empresa_id == Auth::user()->empresa_id){
            return view('frontend.articulos.rubro.edit', compact('rubro'));
        }else{
            return redirect('articulos/rubro');
        }
    }

    public function update($id, Request $request){
        $this->validate($request, [
            'nombre' => 'required|unique:rubros,nombre,'.$id,
		]);
        $requestData = $request->all();
        
        $rubro = Rubro::findOrFail($id);

        if($rubro->empresa_id == Auth::user()->empresa_id){
            if($rubro->nombre != 'Otro'){
                $controllerImagen = new ImageController();
                $imagen_url = $controllerImagen->guardarImagen($request,'/uploads/rubros/',1920,187);
                if($imagen_url != false){
                    $rubro->imagen_url = $imagen_url;
                }

                $rubro->update($requestData);

                $controllerHistorial = new HistorialController();
                $controllerHistorial->nuevoHistorialActualizacion($rubro, $request);
            } else {
                return redirect('articulos/rubro')->withFlashWarning('Este rubro no puede ser modificado.');
            }
        }
        return redirect('articulos/rubro')->withFlashSuccess('Rubro modificado correctamente.');
    }

    public function destroy($id){
        $rubro = Rubro::findOrFail($id);

        if($rubro->empresa_id == Auth::user()->empresa_id){
            if($rubro->nombre != 'Otro'){
                $rubro->delete();

                $controllerHistorial = new HistorialController();
                $controllerHistorial->nuevoHistorialEliminacion($rubro);
            } else {
                return redirect('articulos/rubro')->withFlashWarning('Este rubro no puede ser eliminado.');
            }
        }
        return redirect('articulos/rubro')->withFlashSuccess('Rubro eliminado correctamente.');
    }

    public function autocomplete(Request $request){
        $term = $request->term;

        $rubros = Rubro::where('nombre', 'LIKE', "%$term%")
            ->where('empresa_id', Auth::user()->empresa_id)
            ->paginate(5);

        foreach ($rubros as $rubro)
        {
            $results[] = ['value' => $rubro->nombre];
        }
        return response()->json($results);
    }

    public function guardarNuevoRubro(Request $request){
        $rubro = New Rubro();

        DB::transaction(function() use ($rubro, $request) {
            $nombre = $request->input('nombre');

            $rubro->nombre = $nombre;
            $rubro->empresa_id = Auth::user()->empresa_id;
            $rubro->save();

            $controllerHistorial = new HistorialController();
            $controllerHistorial->nuevoHistorialCreacion($rubro, $request);
        });

        $resultado = [
            'id'    => $rubro->id,
            'nombre'=> $rubro->nombre
        ];

        return response()->json($resultado);
    }
}
