<?php

namespace App\Http\Controllers\Frontend\Articulos;

use App\Http\Controllers\Controller;
use App\Http\Controllers\ImageController;
use App\Models\Frontend\Articulos\Articulo;
use App\Models\Frontend\Articulos\GrupoArticulos;
use App\Models\Frontend\Articulos\Marca;
use App\Models\Frontend\Articulos\Rubro;
use App\Models\Frontend\Articulos\Stock;
use App\Models\Frontend\Articulos\Subrubro;
use App\Models\Frontend\Configuraciones\Moneda;
use App\Models\Frontend\Configuraciones\PorcentajeIva;
use App\Models\Frontend\Configuraciones\Unidad;
use App\Models\Frontend\Ventas\ListaPrecio;
use App\Models\Frontend\Ventas\Precio;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Facades\Datatables;
use Session;

class GrupoArticuloController extends Controller{

    public function index(){
        return view('frontend.articulos.grupo-articulo.index');
    }

    public function datos(){
        $empresa = Auth::user()->empresa;
        $grupos = GrupoArticulos::listarGruposPorEmpresa($empresa);
        return Datatables::of($grupos)
            ->addColumn('action', function ($grupo) {
                return '
                <a href="'.url('/articulos/grupo-articulo/' . $grupo->id).'" class="mytooltip"><i class="fa fa-eye text-success m-r-10"></i>
                    <span class="tooltip-content3">Ver Grupo</span>
                </a>
                <a href="'.url('/articulos/grupo-articulo/' . $grupo->id . '/edit').'" class="mytooltip">
                    <i class="fa fa-pencil text-info m-r-10"></i>
                    <span class="tooltip-content3">Modificar Grupo</span>
                </a>
                <a class="delete mytooltip pointer" data-id="'.$grupo->id.'">
                    <i class="fa fa-close text-danger"></i>
                    <span class="tooltip-content3">Eliminar Grupo</span>
                </a>
                ';
            })
            ->setRowId('id')
            ->removeColumn('id')
            ->make(true);
    }

    public function create(){
        $empresa = Auth::user()->empresa;

        $rubros = Rubro::getRubrosPorEmpresaArray($empresa);
        $subrubros = Subrubro::getSubrubrosPorEmpresaArray($empresa);
        $marcas = Marca::getMarcasPorEmpresaArray($empresa);
        $monedas = Moneda::getMonedasPorEmpresaArray($empresa);
        $unidades = ['' => 'Seleccione una opción...'] + Unidad::pluck('nombre', 'id')->all();
        $porcentajesIva = PorcentajeIva::$arrayCombo;

        return view('frontend.articulos.grupo-articulo.create', compact('rubros', 'subrubros', 'marcas', 'unidades', 'porcentajesIva', 'monedas'));
    }

    public function store(Request $request){
        $this->validate($request, [
            'nombre_grupo' => 'required',
		]);

        DB::transaction(function() use ($request) {
            $controllerImagen = new ImageController();

            $requestData = $request->all();
            $empresa = Auth::user()->empresa;

            $imagen_url = $controllerImagen->guardarImagenArticulo($request);
            if($imagen_url != false){
                $requestData['imagen_url'] = $imagen_url;
            }

            if(isset($requestData['ultimo_costo']) && isset($requestData['remarcado'])){
                $costo = (float)$requestData['ultimo_costo'];
                $remarcado = (float)$requestData['remarcado'];
                $precio = $costo + ($costo * $remarcado/100);
                $requestData['utilidad'] = number_format(((($precio - $costo)/$precio)*100),2);
            }

            $grupo = New GrupoArticulos($requestData);
            $grupo->empresa_id = $empresa->id;
            $grupo->save();

            if(isset($requestData['articulos'])){
                $articulos = $requestData['articulos'];
                foreach ($articulos as $articulo) {
                    if((int)$articulo['estado'] != 0){//No Borrado
                        if((int)$articulo['articulo_id'] > 0){//Viejo
                            $articuloViejo = Articulo::findOrFail($articulo['articulo_id']);
                            $articuloViejo->grupo_articulos_id = $grupo->id;
                            $articuloViejo->modelo = $articulo['modelo'];
                            $articuloViejo->modelo_secundario = $articulo['modelo_secundario'];
                            if($imagen_url != false){
                                $articuloViejo->imagen_url = $imagen_url;
                            }
                            isset($requestData['codigo']) ? $articuloViejo->codigo = $requestData['codigo'] : null;
                            isset($requestData['codigo_barra']) ? $articuloViejo->codigo_barra = $requestData['codigo_barra'] : null;
                            isset($requestData['unidad_id']) ? $articuloViejo->unidad_id = $requestData['unidad_id'] : null;
                            isset($requestData['cantidad']) ? $articuloViejo->cantidad = $requestData['cantidad'] : null;
                            isset($requestData['nombre']) ? $articuloViejo->nombre = $requestData['nombre'] : null;
                            isset($requestData['rubro_id']) ? $articuloViejo->rubro_id = $requestData['rubro_id'] : null;
                            isset($requestData['subrubro_id']) ? $articuloViejo->subrubro_id = $requestData['subrubro_id'] : null;
                            isset($requestData['marca_id']) ? $articuloViejo->marca_id = $requestData['marca_id'] : null;
                            isset($requestData['porcentaje_iva']) ? $articuloViejo->porcentaje_iva = $requestData['porcentaje_iva'] : null;
                            isset($requestData['ultimo_costo']) ? $articuloViejo->ultimo_costo = $requestData['ultimo_costo'] : null;
                            isset($requestData['moneda_id']) ? $articuloViejo->moneda_id = $requestData['moneda_id'] : null;
                            isset($requestData['porcentaje_impuesto']) ? $articuloViejo->impuesto = $requestData['porcentaje_impuesto'] : null;
                            isset($requestData['utilidad']) ? $articuloViejo->utilidad = $requestData['utilidad'] : null;
                            $articuloViejo->update();

                            $precios = Precio::buscarPreciosPorArticulo($empresa, $articuloViejo);
                            foreach ($precios as $precio) {
                                $costo = (float)$articuloViejo->ultimo_costo;
                                $utilidad = (float)$articuloViejo->utilidad;
                                $precio->precio = number_format($costo/(1-($utilidad/100)), 2,'.','');
                                $precio->update();
                            }
                        }else{//Nuevo
                            if(isset($requestData['unidad_id']) && isset($requestData['cantidad']) && isset($requestData['nombre']) &&
                                isset($requestData['rubro_id']) && isset($requestData['subrubro_id']) && isset($requestData['marca_id']) &&
                                isset($requestData['porcentaje_iva']) && isset($requestData['ultimo_costo']) && isset($requestData['moneda_id']) &&
                                isset($requestData['utilidad'])){

                                $articuloNuevo = New Articulo();
                                $articuloNuevo->grupo_articulos_id = $grupo->id;
                                $articuloNuevo->empresa_id = $empresa->id;
                                $articuloNuevo->modelo = $articulo['modelo'];
                                $articuloNuevo->modelo_secundario = $articulo['modelo_secundario'];
                                if($imagen_url != false){
                                    $articuloNuevo->imagen_url = $imagen_url;
                                }
                                isset($requestData['codigo']) ? $articuloNuevo->codigo = $requestData['codigo'] : null;
                                isset($requestData['codigo_barra']) ? $articuloNuevo->codigo_barra = $requestData['codigo_barra'] : null;
                                isset($requestData['unidad_id']) ? $articuloNuevo->unidad_id = $requestData['unidad_id'] : null;
                                isset($requestData['cantidad']) ? $articuloNuevo->cantidad = $requestData['cantidad'] : null;
                                isset($requestData['nombre']) ? $articuloNuevo->nombre = $requestData['nombre'] : null;
                                isset($requestData['rubro_id']) ? $articuloNuevo->rubro_id = $requestData['rubro_id'] : null;
                                isset($requestData['subrubro_id']) ? $articuloNuevo->subrubro_id = $requestData['subrubro_id'] : null;
                                isset($requestData['marca_id']) ? $articuloNuevo->marca_id = $requestData['marca_id'] : null;
                                isset($requestData['porcentaje_iva']) ? $articuloNuevo->porcentaje_iva = $requestData['porcentaje_iva'] : null;
                                isset($requestData['ultimo_costo']) ? $articuloNuevo->ultimo_costo = $requestData['ultimo_costo'] : null;
                                isset($requestData['moneda_id']) ? $articuloNuevo->moneda_id = $requestData['moneda_id'] : null;
                                isset($requestData['porcentaje_impuesto']) ? $articuloNuevo->impuesto = $requestData['porcentaje_impuesto'] : null;
                                isset($requestData['utilidad']) ? $articuloNuevo->utilidad = $requestData['utilidad'] : null;
                                $articuloNuevo->save();

                                $sucursal = Auth::user()->sucursal;

                                $stock = New Stock();
                                $stock->cantidad = 0;
                                $stock->articulo_id = $articuloNuevo->id;
                                if(isset($sucursal)){
                                    $stock->sucursal_id = $sucursal->id;
                                }
                                $stock->save();

                                if(isset($articuloNuevo->ultimo_costo) && isset($articuloNuevo->utilidad)){
                                    $listas = ListaPrecio::getListasPorEmpresa($empresa);
                                    foreach ($listas as $lista) {
                                        $costo = (float)$articuloNuevo->ultimo_costo;
                                        $utilidad = (float)$articuloNuevo->utilidad;

                                        $precio = New Precio();
                                        $precio->articulo_id = $articuloNuevo->id;
                                        $precio->lista_precio_id = $lista->id;
                                        $precio->precio = number_format($costo/(1-($utilidad/100)), 2,'.','');
                                        $precio->save();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        });

        return redirect('articulos/grupo-articulo');
    }

    public function show($id){
        $grupo = GrupoArticulos::findOrFail($id);
        $empresa = Auth::user()->empresa;

        if($grupo->empresa == $empresa){
            return view('frontend.articulos.grupo-articulo.show', compact('grupo'));
        }else{
            return redirect('articulos/grupo-articulo');
        }
    }

    public function edit($id){
        $empresa = Auth::user()->empresa;
        $grupo = GrupoArticulos::findOrFail($id);

        if($grupo->empresa == $empresa){
            $rubros = Rubro::getRubrosPorEmpresaArray($empresa);
            $subrubros = Subrubro::getSubrubrosPorEmpresaArray($empresa);
            $marcas = Marca::getMarcasPorEmpresaArray($empresa);
            $monedas = Moneda::getMonedasPorEmpresaArray($empresa);
            $unidades = ['' => 'Seleccione una opción...'] + Unidad::pluck('nombre', 'id')->all();
            $porcentajesIva = PorcentajeIva::$arrayCombo;

            return view('frontend.articulos.grupo-articulo.edit', compact('grupo','rubros', 'subrubros', 'marcas', 'unidades', 'porcentajesIva', 'monedas', 'actividades_afip'));
        }else{
            return redirect('articulos/grupo-articulo');
        }
    }

    public function update($id, Request $request){
        $this->validate($request, [
            'nombre_grupo' => 'required',
        ]);
        $empresa = Auth::user()->empresa;
        $grupo = GrupoArticulos::findOrFail($id);

        if($grupo->empresa == $empresa){
            DB::transaction(function () use ($grupo, $request, $empresa) {
                $controllerImagen = new ImageController();

                $requestData = $request->all();

                $imagen_url = $controllerImagen->guardarImagenArticulo($request);
                if($imagen_url != false){
                    $requestData['imagen_url'] = $imagen_url;
                }
                //TODO Si agrego remarcar si o si necesito el costo PERDIR COMO REQUIRIDO
                if(isset($requestData['ultimo_costo']) && isset($requestData['remarcado'])){
                    $costo = (float)$requestData['ultimo_costo'];
                    $remarcado = (float)$requestData['remarcado'];
                    $precio = $costo + ($costo * $remarcado/100);
                    $requestData['utilidad'] = number_format(((($precio - $costo)/$precio)*100),2);
                }
                $grupo->update($requestData);

                if(isset($requestData['articulos'])){
                    $articulos = $requestData['articulos'];
                    foreach ($articulos as $articulo) {
                        if((int)$articulo['estado'] == 0){//Borrado
                            if((int)$articulo['articulo_id'] > 0){//Viejo
                                $articuloViejo = Articulo::findOrFail($articulo['articulo_id']);
                                $articuloViejo->grupo_articulos_id = null;
                                $articuloViejo->update();
                            }
                        }else{
                            if((int)$articulo['articulo_id'] > 0){//Viejo
                                $articuloViejo = Articulo::findOrFail($articulo['articulo_id']);
                                $articuloViejo->grupo_articulos_id = $grupo->id;
                                $articuloViejo->modelo = $articulo['modelo'];
                                $articuloViejo->modelo_secundario = $articulo['modelo_secundario'];
                                if($imagen_url != false){
                                    $articuloViejo->imagen_url = $imagen_url;
                                }
                                isset($requestData['codigo']) ? $articuloViejo->codigo = $requestData['codigo'] : null;
                                isset($requestData['codigo_barra']) ? $articuloViejo->codigo_barra = $requestData['codigo_barra'] : null;
                                isset($requestData['unidad_id']) ? $articuloViejo->unidad_id = $requestData['unidad_id'] : null;
                                isset($requestData['cantidad']) ? $articuloViejo->cantidad = $requestData['cantidad'] : null;
                                isset($requestData['nombre']) ? $articuloViejo->nombre = $requestData['nombre'] : null;
                                isset($requestData['rubro_id']) ? $articuloViejo->rubro_id = $requestData['rubro_id'] : null;
                                isset($requestData['subrubro_id']) ? $articuloViejo->subrubro_id = $requestData['subrubro_id'] : null;
                                isset($requestData['marca_id']) ? $articuloViejo->marca_id = $requestData['marca_id'] : null;
                                isset($requestData['porcentaje_iva']) ? $articuloViejo->porcentaje_iva = $requestData['porcentaje_iva'] : null;
                                isset($requestData['ultimo_costo']) ? $articuloViejo->ultimo_costo = $requestData['ultimo_costo'] : null;
                                isset($requestData['moneda_id']) ? $articuloViejo->moneda_id = $requestData['moneda_id'] : null;
                                isset($requestData['porcentaje_impuesto']) ? $articuloViejo->impuesto = $requestData['porcentaje_impuesto'] : null;
                                isset($requestData['utilidad']) ? $articuloViejo->utilidad = $requestData['utilidad'] : null;
                                $articuloViejo->update();

                                $precios = Precio::buscarPreciosPorArticulo($empresa, $articuloViejo);
                                foreach ($precios as $precio) {
                                    $costo = (float)$articuloViejo->ultimo_costo;
                                    $utilidad = (float)$articuloViejo->utilidad;
                                    $precio->precio = number_format($costo/(1-($utilidad/100)), 2,'.','');
                                    $precio->update();
                                }
                            }else{//Nuevo
                                $articuloNuevo = New Articulo();
                                $articuloNuevo->grupo_articulos_id = $grupo->id;
                                $articuloNuevo->empresa_id = $empresa->id;
                                $articuloNuevo->modelo = $articulo['modelo'];
                                $articuloNuevo->modelo_secundario = $articulo['modelo_secundario'];
                                if($imagen_url != false){
                                    $articuloNuevo->imagen_url = $imagen_url;
                                }
                                isset($requestData['codigo']) ? $articuloNuevo->codigo = $requestData['codigo'] : null;
                                isset($requestData['codigo_barra']) ? $articuloNuevo->codigo_barra = $requestData['codigo_barra'] : null;
                                isset($requestData['unidad_id']) ? $articuloNuevo->unidad_id = $requestData['unidad_id'] : null;
                                isset($requestData['cantidad']) ? $articuloNuevo->cantidad = $requestData['cantidad'] : null;
                                isset($requestData['nombre']) ? $articuloNuevo->nombre = $requestData['nombre'] : null;
                                isset($requestData['rubro_id']) ? $articuloNuevo->rubro_id = $requestData['rubro_id'] : null;
                                isset($requestData['subrubro_id']) ? $articuloNuevo->subrubro_id = $requestData['subrubro_id'] : null;
                                isset($requestData['marca_id']) ? $articuloNuevo->marca_id = $requestData['marca_id'] : null;
                                isset($requestData['porcentaje_iva']) ? $articuloNuevo->porcentaje_iva = $requestData['porcentaje_iva'] : null;
                                isset($requestData['ultimo_costo']) ? $articuloNuevo->ultimo_costo = $requestData['ultimo_costo'] : null;
                                isset($requestData['moneda_id']) ? $articuloNuevo->moneda_id = $requestData['moneda_id'] : null;
                                isset($requestData['porcentaje_impuesto']) ? $articuloNuevo->impuesto = $requestData['porcentaje_impuesto'] : null;
                                isset($requestData['utilidad']) ? $articuloNuevo->utilidad = $requestData['utilidad'] : null;
                                $articuloNuevo->save();

                                $sucursal = Auth::user()->sucursal;

                                $stock = New Stock();
                                $stock->cantidad = 0;
                                $stock->articulo_id = $articuloNuevo->id;
                                if(isset($sucursal)){
                                    $stock->sucursal_id = $sucursal->id;
                                }
                                $stock->save();

                                if(isset($articuloNuevo->ultimo_costo) && isset($articuloNuevo->utilidad)){
                                    $listas = ListaPrecio::getListasPorEmpresa($empresa);
                                    foreach ($listas as $lista) {
                                        $costo = (float)$articuloNuevo->ultimo_costo;
                                        $utilidad = (float)$articuloNuevo->utilidad;

                                        $precio = New Precio();
                                        $precio->articulo_id = $articuloNuevo->id;
                                        $precio->lista_precio_id = $lista->id;
                                        $precio->precio = number_format($costo/(1-($utilidad/100)), 2,'.','');
                                        $precio->save();
                                    }
                                }
                            }
                        }
                    }
                }
            });
        }
        return redirect('articulos/grupo-articulo');
    }

    public function destroy($id){
        $empresa = Auth::user()->empresa;
        $grupo = GrupoArticulos::findOrFail($id);

        if($grupo->empresa == $empresa) {
            DB::transaction(function () use ($id, $grupo) {
                foreach ($grupo->articulos as $articulo){
                    $articulo->grupo_articulos_id = null;
                    $articulo->update();
                }
                $grupo->delete();

                Session::flash('flash_message', trans('labels.frontend.grupo').trans('alerts.frontend.deleted').'!');
            });
        }

        return redirect('articulos/grupo-articulo');
    }
}
