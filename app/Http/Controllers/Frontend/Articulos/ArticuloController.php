<?php

namespace App\Http\Controllers\Frontend\Articulos;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Frontend\Configuraciones\HistorialController;
use App\Http\Controllers\ImageController;
use App\Models\Frontend\Articulos\Articulo;
use App\Models\Frontend\Articulos\DetalleArticulo;
use App\Models\Frontend\Articulos\Marca;
use App\Models\Frontend\Articulos\Rubro;
use App\Models\Frontend\Articulos\Stock;
use App\Models\Frontend\Articulos\Subrubro;
use App\Models\Frontend\Configuraciones\Estado;
use App\Models\Frontend\Configuraciones\Opcion;
use App\Models\Frontend\Configuraciones\TipoComprobante;
use App\Models\Frontend\Personas\Proveedor;
use App\Models\Frontend\Configuraciones\ActividadAfip;
use App\Models\Frontend\Configuraciones\Moneda;
use App\Models\Frontend\Configuraciones\PorcentajeIva;
use App\Models\Frontend\Configuraciones\Unidad;
use App\Models\Frontend\Ventas\ListaPrecio;
use App\Models\Frontend\Ventas\Precio;
use App\Models\Backend\Empresas\Sucursal;
use App\Models\Frontend\Ventas\Venta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\HtmlString;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\Datatables\Facades\Datatables;
use Session;

class ArticuloController extends Controller{

    public function index(){
        $empresa = Auth::user()->empresa;
        $sucursales = Sucursal::getSucursalesPorEmpresa($empresa);
        $sucursalesArray = Sucursal::getSucursalesPorEmpresaArray($empresa);

        return view('frontend.articulos.articulo.index', compact('sucursales','sucursalesArray'));
    }

    public function datos(){
        $empresa = Auth::user()->empresa;
        $sucursal = Auth::user()->sucursal;
        $sucursales = Sucursal::getSucursalesPorEmpresa($empresa);
        $listaPorDefecto = Opcion::where('empresa_id', $empresa->id)
            ->where('nombre', 'lista_precios_defecto_id')
            ->first();
        $porcentajes_iva = PorcentajeIva::$valores;

        $query = Articulo::select('articulos.id',
            'articulos.codigo',
            'articulos.modelo',
            'articulos.nombre',
            'articulos.lleva_stock',
            'articulos.destacado',
            'articulos.porcentaje_iva',
            'rubros.nombre as rubro',
            'subrubros.nombre as subrubro',
            'marcas.nombre as marca',
            'stocks.id as stock_id',
            'stocks.cantidad as stock',
            'stocks.minimo',
            'stocks.alarma',
            'precios.precio');
        $query->leftjoin('rubros', 'rubros.id', '=', 'articulos.rubro_id');
        $query->leftjoin('subrubros', 'subrubros.id', '=', 'articulos.subrubro_id');
        $query->leftjoin('marcas', 'marcas.id', '=', 'articulos.marca_id');
        $query->leftJoin('stocks', function($q) use($sucursal) {
            $q->on('stocks.articulo_id', '=', 'articulos.id')
                ->where('stocks.sucursal_id', '=', $sucursal->id)
                ->whereNull('stocks.deleted_at');
        });
        $query->leftJoin('precios', function($q) use($listaPorDefecto) {
            $q->on('precios.articulo_id', '=', 'articulos.id')
                ->where('precios.lista_precio_id', '=', $listaPorDefecto->valor)
                ->whereNull('precios.deleted_at');
        });
        $query->where('articulos.empresa_id', $empresa->id);
        $query->whereNull('articulos.deleted_at');
        $articulos = $query->get();

        $datatables = Datatables::of($articulos);
        $datatables->addColumn('action', function ($articulo) use ($sucursales) {
                $return = '
                 <div class="btn-group">
                    <button type="button" class="btn btn-secondary dropdown-toggle btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Acciones
                    </button>
                    <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 33px, 0px); top: 0px; left: 0px; will-change: transform;">
                        <a class="dropdown-item" href="'.url('/articulos/articulo/' . $articulo->id).'">
                            <i class="fa fa-eye" aria-hidden="true"></i> Ver Detalles del Articulo
                        </a>
                        <a class="dropdown-item" href="'.url('/articulos/articulo/' . $articulo->id . '/edit').'">
                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Modificar Articulo
                        </a>';
                if($articulo->lleva_stock) {
                    $return .= '
                        <a class="dropdown-item" href="' . url('/articulos/movimiento-stock/' . $articulo->stock_id . '/create') . '">
                            <i class="fa fa-balance-scale" aria-hidden="true"></i> Ajuste de Stock
                        </a>';
                }
                if($sucursales->count() > 1 && $articulo->lleva_stock){
                    $return .= '
                        <a href="#" data-toggle="modal" data-target="#modalSolicitarStock" data-backdrop="static" class="dropdown-item" onclick="pasarId('.$articulo->stock_id.')">
                            <i class="fa fa-close"></i> Solicitar Stock
                        </a>
                    ';
                }
                $return .= '
                        <a href="#" class="dropdown-item delete" data-id="'.$articulo->id.'" data-toggle="tooltip">
                            <i class="fa fa-close"></i> Eliminar Articulo
                        </a>
                    </div>
                </div>
                ';
                return $return;
            });
        $datatables->setRowId('id');
        $datatables->removeColumn('id');
        $datatables->removeColumn('stock_id');
        $datatables->removeColumn('minimo');
        $datatables->removeColumn('alarma');
        $datatables->removeColumn('lleva_stock');
        $datatables->removeColumn('destacado');
        $datatables->removeColumn('porcentaje_iva');
        $datatables->editColumn('nombre', function ($articulo) {
            if($articulo->destacado){
                return new HtmlString( '<span class="text-warning font-weight-bold">[DESTACADO]</span> ' . $articulo->nombre);
            } else {
                return $articulo->nombre;
            }
        });
        $datatables->addColumn('precio', function ($articulo) use ($porcentajes_iva) {
            $precio = $articulo->precio + ($articulo->precio * $porcentajes_iva[$articulo->porcentaje_iva] / 100);
            return '$ '.number_format($precio,2,',','.');
        });
        $datatables->editColumn('stock', function ($articulo) {
            if($articulo->lleva_stock){
                if($articulo->stock >= $articulo->minimo && $articulo->stock > 0){
                    return new HtmlString('<span class="text-success font-weight-bold">'.number_format($articulo->stock, 0).'</span>');
                } elseif($articulo->stock < $articulo->minimo && $articulo->alarma == 1 || $articulo->stock < 0) {
                    return new HtmlString('<span class="text-danger font-weight-bold">'.number_format($articulo->stock, 0).'</span>');
                } else {
                    return new HtmlString('<span class="font-weight-bold">'.number_format($articulo->stock, 0).'</span>');
                }
            } else {
                return '';
            }
        });
        return $datatables->make(true);
    }

    public function create(){
        $empresa = Auth::user()->empresa;

        $listas_sin_usar = ListaPrecio::getListasPorEmpresa($empresa);

        $rubros = Rubro::getRubrosPorEmpresaArray($empresa);
        $subrubros = Subrubro::getSubrubrosPorEmpresaArray($empresa);
        $marcas = Marca::getMarcasPorEmpresaArray($empresa);
        $monedas = Moneda::getMonedasPorEmpresaArray($empresa);
        $actividades_afip = ActividadAfip::getActividadesAfipPorEmpresaArray($empresa);

        $unidades = ['' => 'Seleccione una opción...'] + Unidad::pluck('nombre', 'id')->all();
        $porcentajesIva = PorcentajeIva::$arrayCombo;

        $porcentajeRemarcado = 0;

        $codigo = Articulo::where('empresa_id',$empresa->id)->max('codigo') + 1;

        return view('frontend.articulos.articulo.create', compact('rubros', 'subrubros', 'marcas', 'unidades', 'porcentajesIva', 'monedas', 'actividades_afip', 'listas_sin_usar', 'porcentajeRemarcado','codigo'));
    }

    public function store(Request $request){
        $empresa = Auth::user()->empresa;
        $sucursal = Auth::user()->sucursal;

        $this->validate($request, [
            'codigo' => 'required',
            'porcentaje_iva' => 'required',
            'nombre' => 'required',
            'moneda_id' => 'required',
            'image' => 'mimes:jpeg,png,jpg,gif|image|max:1024',
		]);

        $validarCodigo = Articulo::where('empresa_id',$empresa->id)->where('codigo',$request->codigo)->first();
        if($validarCodigo){
            Session::flash('flash_message', 'Codigo Interno Repetido!');
            return redirect('articulos/articulo');
        }

        DB::transaction(function() use ($request,$empresa,$sucursal) {
            $requestData = $request->all();

            (!isset($requestData['lleva_stock'])) ? $requestData['lleva_stock'] = 0 : $requestData['lleva_stock'] = 1;
            (!isset($requestData['destacado'])) ? $requestData['destacado'] = 0 : $requestData['destacado'] = 1;

            $controllerHistorial = new HistorialController();

            $articulo = new Articulo($requestData);
            $articulo->empresa_id = $empresa->id;

            $controllerImagen = new ImageController();
//            $imagen_url = $controllerImagen->uploadImages($request,'/uploads/articulos/',500,500);
            $imagen_url = $controllerImagen->guardarImagen($request,'/uploads/articulos/',500,500);

            if($imagen_url != false){
                $articulo->imagen_url = $imagen_url;
            }

            $articulo->save();

            if($sucursal && $requestData['lleva_stock']){
                $stock = New Stock($requestData['stock']);
                $stock->articulo_id = $articulo->id;
                $stock->sucursal_id = $sucursal->id;
                $stock->save();
            }

            if(isset($requestData['precios'])){
                $precios = $requestData['precios'];
                foreach ($precios as $precio) {
                    if($precio['precio_neto'] != null){
                        $precioNuevo = new Precio($precio);
                        $precioNuevo->precio = $precio['precio_neto'];
                        $precioNuevo->articulo_id = $articulo->id;
                        $precioNuevo->save();
                        $controllerHistorial->nuevoHistorialCreacion($precioNuevo, $request);
                    }
                }
            }

            $controllerHistorial->nuevoHistorialCreacion($articulo, $request);

            Session::flash('flash_message', trans('labels.frontend.articulo').trans('alerts.frontend.created').'!');
        });

        return redirect('articulos/articulo');
    }

    public function show($id){
        $articulo = Articulo::findOrFail($id);
        $empresa = Auth::user()->empresa;
        $sucursal = Auth::user()->sucursal;

        if($articulo->empresa == $empresa){
            $stock = null;
            if(isset($sucursal) && $articulo->lleva_stock){
                $stock = Stock::where('articulo_id', $articulo->id)
                    ->where('sucursal_id', $sucursal->id)
                    ->whereNull('deleted_at')
                    ->first();
            }

            $query = Venta::join('clientes', 'clientes.id', '=', 'ventas.cliente_id');
            $query->join('detalles_ventas','detalles_ventas.venta_id','ventas.id');
            $query->join('puntos_de_ventas', 'puntos_de_ventas.id', '=', 'ventas.punto_venta_id');
            $query->join('monedas', 'monedas.id', '=', 'ventas.moneda_id');
            $query->join('lista_precios', 'lista_precios.id', '=', 'ventas.lista_precios_id');
            $query->select('ventas.*', 'clientes.razon_social', 'puntos_de_ventas.numero as punto_venta', 'monedas.signo', 'lista_precios.nombre as lista');
            $query->whereNull('ventas.deleted_at');
            $query->where('detalles_ventas.articulo_id',$articulo->id);
            $query->where('ventas.estado','<>', Estado::PLANTILLA);
            $query->where('ventas.estado','<>', Estado::FACTURA_AUTOMATICA_PENDIENTE);
            $query->orderBy('ventas.fecha','DESC');
            if (Auth::user()->hasRole('Usuario Vendedor')) {
                $query->where('ventas.sucursal_id', $sucursal->id);
            }
            $ventas = $query->get();

            $tipos_comprobantes = TipoComprobante::$comprobantes;

            return view('frontend.articulos.articulo.show', compact('articulo','stock','ventas','tipos_comprobantes'));
        }else{
            return redirect('articulos/articulo')->withFlashDanger('Acción Ilegal.');
        }
    }

    public function edit($id){
        $empresa = Auth::user()->empresa;
        $articulo = Articulo::findOrFail($id);

        if($articulo->empresa == $empresa){
            $sucursal = Auth::user()->sucursal;
            $stock = null;
            if(isset($sucursal)){
                $stock = $articulo->stockPorSucursal($sucursal);
            }

            $listas_precio_empresa = ListaPrecio::getListasPorEmpresa($empresa);
            $listas_usadas = ListaPrecio::join('precios', 'lista_precios.id', 'precios.lista_precio_id')
                ->where('precios.articulo_id', $articulo->id)
                ->whereNull('precios.deleted_at')
                ->select('lista_precios.*')
                ->get();
            $listas_sin_usar = $listas_precio_empresa->diff($listas_usadas);

            $rubros = Rubro::getRubrosPorEmpresaArray($empresa);
            $subrubros = Subrubro::getSubrubrosPorEmpresaArray($empresa);
            $marcas = Marca::getMarcasPorEmpresaArray($empresa);
            $monedas = Moneda::getMonedasPorEmpresaArray($empresa);
            $actividades_afip = ActividadAfip::getActividadesAfipPorEmpresaArray($empresa);

            $unidades = Unidad::pluck('nombre', 'id')->all();
            $porcentajesIva = PorcentajeIva::$arrayCombo;

            if($articulo->codigo){
                $codigo = $articulo->codigo;
            } else {
                $codigo = Articulo::where('empresa_id',$empresa->id)->max('codigo') + 1;
            }

            return view('frontend.articulos.articulo.edit', compact('articulo','stock', 'rubros', 'subrubros', 'marcas', 'unidades', 'porcentajesIva', 'monedas', 'actividades_afip', 'listas_sin_usar', 'codigo'));
        }else{
            return redirect('articulos/articulo')->withFlashDanger('Acción Ilegal.');
        }
    }

    public function update($id, Request $request){
        $this->validate($request, [
            'codigo' => 'required',
            'porcentaje_iva' => 'required',
            'nombre' => 'required',
            'moneda_id' => 'required',
            'image' => 'mimes:jpeg,png,jpg,gif|image|max:1024',
        ]);
        $empresa = Auth::user()->empresa;
        $articulo = Articulo::findOrFail($id);

        if($articulo->codigo != $request->codigo){
            $validarCodigo = Articulo::where('empresa_id',$empresa->id)->where('codigo',$request->codigo)->first();
            if($validarCodigo){
                Session::flash('flash_message', 'Codigo Interno Repetido!');
                return redirect('articulos/articulo');
            }
        }

        if($articulo->empresa == $empresa) {
            DB::transaction(function () use ($articulo, $request) {
                $requestData = $request->all();
                $sucursal = Auth::user()->sucursal;

                (!isset($requestData['lleva_stock'])) ? $requestData['lleva_stock'] = 0 : $requestData['lleva_stock'] = 1;
                (!isset($requestData['destacado'])) ? $requestData['destacado'] = 0 : $requestData['destacado'] = 1;

                $controllerHistorial = new HistorialController();
                $controllerImagen = new ImageController();

                if($sucursal && $requestData['lleva_stock']) {
                    $stock = $articulo->stockPorSucursal($sucursal);
                    if(isset($stock)){
                        if(!isset($requestData['stock']['alarma'])){
                            $requestData['stock']['alarma'] = 0;
                        }
                        $stock->update($requestData['stock']);
                    }else{
                        $stock = New Stock($requestData['stock']);
                        $stock->articulo_id = $articulo->id;
                        $stock->sucursal_id = $sucursal->id;
                        $stock->save();
                    }
                }

                if(isset($requestData['precios'])){
                    $precios = $requestData['precios'];
                    foreach ($precios as $precio) {
                        if((int)$precio['id'] > 0){//Editar detalles
                            $precioGuardado = Precio::findOrFail($precio['id']);
                            if($precio['precio_neto'] == null){//Eliminar Precio
                                $precioGuardado->delete();
                            }else{//Actualizar
                                $precioGuardado->precio = $precio['precio_neto'];
                                $precioGuardado->update();
                                $controllerHistorial->nuevoHistorialActualizacion($precioGuardado, $request);
                            }
                        }else{//Nuevo Precio
                            if($precio['precio_neto'] != null){
                                $precioNuevo = new Precio($precio);
                                $precioNuevo->precio = $precio['precio_neto'];
                                $precioNuevo->articulo_id = $articulo->id;
                                $precioNuevo->save();
                                $controllerHistorial->nuevoHistorialCreacion($precioNuevo, $request);
                            }
                        }
                    }
                }

                $imagen_url = $controllerImagen->guardarImagen($request,'/uploads/articulos/',500,500);
                if($imagen_url != false){
                    $articulo->imagen_url = $imagen_url;
                }
                $articulo->update($requestData);

                $controllerHistorial->nuevoHistorialActualizacion($articulo, $request);


            });
        }

        return redirect('articulos/articulo')->withFlashSuccess('Artículo modificado correctamente.');
    }

    public function destroy($id){
        $articulo = Articulo::findOrFail($id);

        if($articulo->empresa_id == Auth::user()->empresa_id) {
            DB::transaction(function () use ($id, $articulo) {
                $sucursal = Auth::user()->sucursal;

                $stock = $articulo->stockPorSucursal($sucursal);
                if(isset($stock)){
                    $stock->delete();
                }

                foreach ($articulo->precios as $precio){
                    $precio->delete();
                }

                foreach ($articulo->detalleArticulos as $detalle){
                    $detalle->delete();
                }

                $articulo->delete();

                Session::flash('flash_message', trans('labels.frontend.articulo').trans('alerts.frontend.deleted').'!');
            });
        }

        return redirect('articulos/articulo');
    }

    public function createRapido(){
        $empresa = Auth::user()->empresa;
        $listas_sin_usar = ListaPrecio::getListasPorEmpresa($empresa);
        $porcentajesIva = PorcentajeIva::$arrayCombo;

        return view('frontend.articulos.articulo.create_rapido', compact('porcentajesIva','listas_sin_usar'));
    }

    public function storeRapido(Request $request){
        $this->validate($request, [
            'nombre' => 'required',
            'stock' => 'required',
            'ultimo_costo' => 'required',
            'porcentaje_iva' => 'required'
        ]);

        DB::transaction(function() use ($request) {
            $requestData = $request->all();
            $empresa = Auth::user()->empresa;
            $sucursal = Auth::user()->sucursal;

            (!isset($requestData['lleva_stock'])) ? $requestData['lleva_stock'] = 0 : $requestData['lleva_stock'] = 1;

            $articulo = new Articulo($requestData);
            $articulo->unidad_id = 2;//Unidad: Unidad
            $articulo->cantidad = 1;
            $articulo->empresa_id = $empresa->id;
            $moneda = Moneda::where('empresa_id', $empresa->id)->where('cotizacion', 1)->first();
            if(!$moneda){
                return redirect('articulos/articulo')->withFlashDanger('Necesita tener una moneda con cotizacion 1 para cargar un articulo rapido!');
            }
            $articulo->moneda_id = $moneda->id;
            $codigoArticulo = Articulo::where('empresa_id',$empresa->id)->max('codigo') + 1;
            $articulo->codigo = $codigoArticulo;
            $articulo->save();

            if(isset($requestData['precios'])){
                $precios = $requestData['precios'];
                foreach ($precios as $precio) {
                    if($precio['precio_neto'] != null){
                        $precioNuevo = new Precio($precio);
                        $precioNuevo->precio = $precio['precio_neto'];
                        $precioNuevo->articulo_id = $articulo->id;
                        $precioNuevo->save();
                    }
                }
            }

            if($requestData['lleva_stock']){
                $stock = New Stock();
                $stock->cantidad = $request->stock;
                $stock->maximo = 100;
                $stock->minimo = 0;
                $stock->alarma = true;
                $stock->articulo_id = $articulo->id;
                $stock->sucursal_id = $sucursal->id;
                $stock->save();
            }

        });
        return redirect('articulos/articulo')->withFlashSuccess('¡Articulo creado correctamente!');
    }


    public function autocomplete(Request $request){
        $term = $request->term;

        $query = Articulo::where('empresa_id', Auth::user()->empresa_id);
        $query->where(function($q) use ($term) {
            $q->where('nombre', 'LIKE', "%$term%");
            $q->orWhere('codigo', '=', $term);
            $q->orWhere('codigo_barra', '=', $term);
        });
        $articulos = $query->paginate(5);

        $results = [];
        foreach ($articulos as $articulo)
        {
            $results[] = [
                'id' => $articulo->id,
                'value' => "(" . $articulo->codigo .") ". $articulo->nombre,
                'porcentaje_iva' => $articulo->porcentaje_iva
            ];
        }
        return response()->json($results);
    }

    public function obtenerArticuloConPorcentajeIva(Request $request){
        $term = $request->term;

        $articulos = Articulo::listarArticulos(Auth::user()->empresa, 5, $term);

        $results = [];
        foreach ($articulos as $articulo) {
            $results[] = [
                'id' => $articulo->id,
                'value' => "(" . $articulo->codigo .") ". $articulo->nombre." - ".$articulo->marca." - ".$articulo->modelo." - ".$articulo->modelo_secundario,
                'porcentaje_iva' => (int)$articulo->porcentaje_iva
            ];
        }
        return response()->json($results);
    }

    public function obtenerPrecioArticulo($lista_id, Request $request){
        $term = $request->term;

        $lista_precios_id = (integer)$lista_id;
        $empresa = Auth::user()->empresa;
        $sucursal = Auth::user()->sucursal;

        $articuloPrecios = Precio::listarPreciosPorEmpresaSucursalListaEnStock($empresa, $sucursal, $lista_precios_id, true, 5, $term);

        $results = [];
        if(isset($articuloPrecios)){
            foreach ($articuloPrecios as $item) {
                if(!$item->lleva_stock){//Ignorar la existencia si no lleva stock
                    $item->existencia = null;
                }else{
                    $item->existencia = (int)$item->existencia;
                }

                $precio = 0;
                if(isset($item->precio) && $item->precio != 0){//Precio segun lista
                    $precio = $item->precio;
                }

                $results[] = [
                    'id' => $item->id,
                    'value' => "(" . $item->codigo .") ". $item->nombre." - ".$item->marca." - ".$item->modelo." - ".$item->modelo_secundario,
                    'porcentaje_iva' => $item->porcentaje_iva,
                    'iva' => PorcentajeIva::$valores[(integer)$item->porcentaje_iva],
                    'precio' => (float)$precio,
                    'impuesto_interno' => (float)$item->impuesto,
                    'existencia' => $item->existencia,
                    'moneda_id' => $item->moneda_id,
                    'cotizacion' => (float)$item->cotizacion,
                ];
            }
        }

        return response()->json($results);
    }

    public function obtenerPrecioArticuloNoStock($lista_id, Request $request){
        $term = $request->term;

        $lista_precios_id = (integer)$lista_id;
        $empresa = Auth::user()->empresa;
        $sucursal = Auth::user()->sucursal;

        $articuloPrecios = Precio::listarPreciosPorEmpresaSucursalListaEnStock($empresa, $sucursal, $lista_precios_id, false, 5, $term);

        $results = [];
        foreach ($articuloPrecios as $item) {
            if(!$item->lleva_stock){//Ignorar la existencia si no lleva stock
                $item->existencia = null;
            }else{
                $item->existencia = (int)$item->existencia;
            }

            $precio = 0;
            if(isset($item->precio) && $item->precio != 0){//Precio segun lista
                $precio = $item->precio;
            }

            $results[] = [
                'id' => $item->id,
                'value' => "(" . $item->codigo .") ". $item->nombre." - ".$item->marca." - ".$item->modelo." - ".$item->modelo_secundario,
                'porcentaje_iva' => $item->porcentaje_iva,
                'iva' => PorcentajeIva::$valores[(integer)$item->porcentaje_iva],
                'precio' => (float)$precio,
                'impuesto_interno' => (float)$item->impuesto,
                'existencia' => $item->existencia,
                'moneda_id' => $item->moneda_id,
                'cotizacion' => (float)$item->cotizacion,
            ];
        }
        return response()->json($results);
    }

    public function obtenerArticulo(Request $request){
        $term = $request->term;

        $empresa = Auth::user()->empresa;

        $articulos = Articulo::listarArticulos($empresa, 5, $term, false);

        $results = [];
        foreach ($articulos as $articulo) {
            $results[] = [
                'id'                => $articulo->id,
                'value'             => "(" . $articulo->codigo .") ". $articulo->nombre." - ".$articulo->marca." - ".$articulo->modelo." - ".$articulo->modelo_secundario,
                'modelo'            => $articulo->modelo,
                'modelo_secundario' => $articulo->modelo_secundario,
                'unidad'            => $articulo->unidad->nombre,
                'cantidad'          => number_format($articulo->cantidad, 0),
                'rubro'             => $articulo->rubro->nombre,
                'subrubro'          => $articulo->subrubro->nombre,
                'marca'             => isset($articulo->marca) ? $articulo->marca->nombre : '',
                'porcentaje_iva'    => $articulo->porcentajeIva(),
                'costo'             => $articulo->ultimo_costo,
                'moneda'            => $articulo->moneda->nombre,
                'impuesto'          => $articulo->impuesto,
                'remarcar'          => $articulo->porcentajeRemarcado(),
            ];
        }
        return response()->json($results);
    }

    public function obtenerStockArticulo($sucursal_id,Request $request){
        $term = $request->term;

        $empresa = Auth::user()->empresa;

        $articulos = Articulo::listarArticulosConStock($empresa,$sucursal_id, 5, $term);

        $results = [];
        foreach ($articulos as $articulo) {
            $results[] = [
                'id'                => $articulo->id,
                'value'             => "(" . $articulo->codigo .") ". $articulo->nombre." - ".$articulo->marca." - ".$articulo->modelo." - ".$articulo->modelo_secundario,
                'existencia'        => $articulo->existencia,
            ];
        }
        return response()->json($results);
    }

    public function plantillaArticulos(){
        Excel::create('Articulos', function($excel) {

            // Set Excel Information
            $excel->setTitle('Articulos')
                ->setDescription('Lista de Articulos de la empresa')
                ->setCreator('Artisan')
                ->setCompany('Artisan');

            $excel->sheet('Articulos', function($sheet) {

                // Creo la primera fila con los titulos
                $sheet->row(1, array(
                    'Codigo Interno', 'Nombre', 'Modelo', 'Codigo de Barras', 'Costo', 'IVA', 'Stock', 'Marca', 'Rubro', 'Subrubro'
                ));

                // Color de fondo, tamaño y bordes de la fila de titulos
                $sheet->row(1, function($row) {
                    $row->setBackground('#EBEBEB');
                    $row->setFontSize(16);
                    $row->setBorder('none', 'none', 'solid', 'none');
                });

                // Ancho de las columnas
                $sheet->setWidth(array(
                    'A'     =>  20, // Codigo Interno
                    'B'     =>  50, // Nombre
                    'C'     =>  20, // Modelo
                    'D'     =>  22, // Codigo de Barras
                    'E'     =>  15, // Costo
                    'F'     =>  10, // IVA
                    'G'     =>  20, // Stock
                    'H'     =>  25, // Marca
                    'I'     =>  25, // Rubro
                    'J'     =>  25, // Subrubro
                ));

                // Congelar fila de titulos para que sea siempre visible
                $sheet->freezeFirstRow();

            });

        })->download('xlsx');

    }

    public function exportarArticulos(){
        Excel::create('Articulos', function($excel) {

            // Set Excel Information
            $excel->setTitle('Articulos')
                ->setDescription('Lista de Articulos de la empresa')
                ->setCreator('Artisan')
                ->setCompany('Artisan');

            $excel->sheet('Articulos', function($sheet) {

                $porcentajesIva = PorcentajeIva::$valores;
                $articulos = Articulo::where('empresa_id', Auth::user()->empresa_id)->get();
                if(Auth::user()->sucursal){
                    $sucursal = Auth::user()->sucursal;
                } else {
                    $sucursal = Sucursal::where('empresa_id', Auth::user()->empresa_id)->first();
                }

                $sheet->loadView('frontend.articulos.articulo.exportar-articulos',
                    compact('articulos','porcentajesIva','sucursal'));

                // Ancho de las columnas
                $sheet->setWidth(array(
                    'A'     =>  20, // Codigo Interno
                    'B'     =>  50, // Nombre
                    'C'     =>  20, // Modelo
                    'D'     =>  22, // Codigo de Barras
                    'E'     =>  15, // Costo
                    'F'     =>  10, // IVA
                    'G'     =>  20, // Stock
                    'H'     =>  25, // Marca
                    'I'     =>  25, // Rubro
                    'J'     =>  25, // Subrubro
                ));
                // Congelar fila de titulos para que sea siempre visible
                $sheet->freezeFirstRow();

            });

        })->download('xlsx');

    }

    public function importarArticulos(Request $request){

        $empresa = Auth::user()->empresa;

        $this->validate($request, [
            'archivo' => 'required|max:10240|mimes:xlsx,xls'
        ]);

        // Almaceno el archivo excel en el storage en una carpeta con el nombre de la empresa y completo el path con la ruta
        $path = $request->file('archivo')->store(
            'importaciones/'.$empresa->razon_social
        );
        $path = 'storage/app/'.$path;

        $hayError = false;
        $mensaje = "Los articulos se han actualizado correctamente";

        // Cargo el excel
        Excel::load($path, function($reader) use (&$hayError,&$mensaje,$empresa) {

            // Tomo solamente las primeras 16 columnas del Excel
            $reader->takeColumns(11);

            // Pregunto si el archivo no esta vacio
            if($reader->first()){

                // Valido que los encabezados esten bien nombrados y ordenados
                $excelData = $reader->all();
                $headers = $excelData->first()->keys()->toArray();
                if (
                    $headers[0] == 'codigo_interno' &&
                    $headers[1] == 'nombre' &&
                    $headers[2] == 'modelo' &&
                    $headers[3] == 'codigo_de_barras' &&
                    $headers[4] == 'costo' &&
                    $headers[5] == 'iva' &&
                    $headers[6] == 'stock' &&
                    $headers[7] == 'marca' &&
                    $headers[8] == 'rubro' &&
                    $headers[9] == 'subrubro'
                ){
                    $rows = $reader->all();
                    DB::transaction(function () use ($rows,&$hayError,&$mensaje,$empresa) {

                        foreach ($rows as $row) {

                            // Si el articulo tiene codigo interno lo busco
                            if($row->codigo_interno){
                                $articulo = Articulo::where('codigo',$row->codigo_interno)
                                    ->where('empresa_id',$empresa->id)
                                    ->get();
                            } else {
                                $articulo = null;
                            }

                            if($articulo && $articulo->count()){ // Si el articulo ya existe lo actualizo

                                /*if($articulo->count() > 1){ // Pertenece a un Grupo
                                    $query = Articulo::where('codigo',$row->codigo_interno);
                                    $query->where('empresa_id',$empresa->id);
                                    $query->where('modelo',$row->modelo);
                                    if($row->modelo_2){
                                        $query->where('modelo_secundario',$row->modelo_2);
                                    }
                                    $articulo = $query->first();
                                } else {*/
                                    $articulo = $articulo->first();
                              //  }

                                if($articulo){
                                    if($row->nombre){
                                        $articulo->nombre = $row->nombre;
                                    } else {
                                        $hayError = true;
                                        $mensaje = "ERROR. Uno o mas articulos no poseen un nombre!";
                                        return false;
                                    }
                                    if($row->codigo_de_barras){
                                        $articulo->codigo_barra = substr($row->codigo_de_barras, 0, 30);
                                    }

                                    if(!$row->costo){
                                        $row->costo = 0;
                                    }
                                    $costoAnterior = $articulo->ultimo_costo;
                                    $articulo->ultimo_costo = $row->costo;

                                    // Busco la moneda que corresponde a esa empresa y ese numero

                                    $moneda = Moneda::where('empresa_id', $empresa->id)->where('cotizacion', 1)->first();
                                    if(!$moneda){
                                        $hayError = true;
                                        $mensaje = "ERROR. No posee ninguna moneda por defecto para asignar (Cotización 1)";
                                        return false;
                                    }
                                    $articulo->moneda_id = $moneda->id;

                                    // Busco la marca que corresponde a esa empresa y con ese nombre
                                    $marca = Marca::where('empresa_id', $empresa->id)->where('nombre', $row->marca)->first();
                                    ($marca) ? $articulo->marca_id = $marca->id : $articulo->marca_id = NULL;

                                    $rubro = Rubro::where('empresa_id', $empresa->id)->where('nombre', $row->rubro)->first();
                                    if($rubro){ // Si encontre el rubro que ingresaron se lo asigno al producto
                                        $articulo->rubro_id = $rubro->id;
                                    }

                                    $subrubro = Subrubro::where('empresa_id', $empresa->id)->where('nombre', $row->subrubro)->first();
                                    if($subrubro){ // Si encontre el subrubro que ingresaron se lo asigno al producto
                                        $articulo->subrubro_id = $subrubro->id;
                                    }

                                    // Busco en el array de los porcentajes del IVA si el valor ingresado existe
                                    $porcentajesIva = PorcentajeIva::$valores;
                                    $iva = array_search($row->iva,$porcentajesIva);
                                    ($iva) ? $articulo->porcentaje_iva = $iva : $iva = $articulo->porcentaje_iva = 0;

                                    if($row->stock === null){
                                        $articulo->lleva_stock = 0;
                                    } else {
                                        $articulo->lleva_stock = 1;
                                    }

                                    $articulo->save();

                                    if(Auth::user()->sucursal){
                                        $sucursal = Auth::user()->sucursal;
                                    } else {
                                        $sucursal = Sucursal::where('empresa_id', $empresa->id)->first();
                                    }

                                    if($row->stock != null){
                                        $stock = Stock::where('articulo_id', $articulo->id)->where('sucursal_id',$sucursal->id)->first();
                                        if($stock){
                                            if($row->stock){
                                                $stock->cantidad = $row->stock;
                                            } else {
                                                $stock->cantidad = 0;
                                            }
                                            $stock->save();
                                        } else {
                                            $stock = New Stock();
                                            if($row->stock){
                                                $stock->cantidad = $row->stock;
                                            } else {
                                                $stock->cantidad = 0;
                                            }
                                            $stock->articulo_id = $articulo->id;
                                            $stock->sucursal_id = $sucursal->id;
                                            $stock->save();
                                        }
                                    }

                                    $costo = DetalleArticulo::where('articulo_id', $articulo->id)->first();
                                    if($costo){
                                        $costo->costo = $row->costo;
                                        $costo->cantidad = 1;
                                        $costo->moneda_id = $moneda->id;
                                        $costo->importe_iva = $row->costo * $porcentajesIva[$iva]/100;
                                        $costo->save();
                                    }

                                    if($row->costo > 0) {
                                        $precios = Precio::where('articulo_id', '=', $articulo->id)
                                            ->whereNull('deleted_at')
                                            ->whereNull('fecha_hasta')
                                            ->get();
                                        foreach($precios as $precio){
                                            $porcentaje = (($precio->precio - $costoAnterior)/$costoAnterior)*100; // Calculo el procentaje de ganancia que tenia en esa lista anteriormente
                                            $precio->precio = ($articulo->ultimo_costo * $porcentaje)/100 + $articulo->ultimo_costo;
                                            $precio->save();
                                        }
                                    }
                                }

                            } else { // Si el articulo no existe lo creo
                                $articulo = new Articulo();
                                $articulo->empresa_id = $empresa->id;
                                $articulo->cantidad = 1;

                                if($row->nombre){
                                    $articulo->nombre = $row->nombre;
                                } else {
                                    $hayError = true;
                                    $mensaje = "ERROR. Uno o mas articulos no poseen un nombre!";
                                    return false;
                                }
                                if($row->codigo_interno){
                                    $articulo->codigo = $row->codigo_interno;
                                } else {
                                    $codigoArticulo = Articulo::where('empresa_id',$empresa->id)->max('codigo') + 1;
                                    $articulo->codigo = $codigoArticulo;
                                }

                                $articulo->codigo_barra = substr($row->codigo_de_barras, 0, 30);
                                if(!$row->costo){
                                    $row->costo = 0;
                                }
                                $articulo->ultimo_costo = $row->costo;

                                $moneda = Moneda::where('empresa_id', $empresa->id)->where('cotizacion', 1)->first();
                                if(!$moneda){
                                    $hayError = true;
                                    $mensaje = "ERROR. No posee ninguna moneda por defecto para asignar (Cotización 1)";
                                    return false;
                                }
                                $articulo->moneda_id = $moneda->id;

                                // Busco la marca que corresponde a esa empresa y con ese nombre
                                $marca = Marca::where('empresa_id', $empresa->id)->where('nombre', $row->marca)->first();
                                ($marca) ? $articulo->marca_id = $marca->id : $articulo->marca_id = NULL;

                                $rubro = Rubro::where('empresa_id', $empresa->id)->where('nombre', $row->rubro)->first();
                                if($rubro){ // Si encontre el rubro que ingresaron se lo asigno al producto
                                    $articulo->rubro_id = $rubro->id;
                                }

                                $subrubro = Subrubro::where('empresa_id', $empresa->id)->where('nombre', $row->subrubro)->first();
                                if($subrubro){ // Si encontre el subrubro que ingresaron se lo asigno al producto
                                    $articulo->subrubro_id = $subrubro->id;
                                }

                                // Busco en el array de los porcentajes del IVA si el valor ingresado existe
                                $porcentajesIva = PorcentajeIva::$valores;
                                $iva = array_search($row->iva,$porcentajesIva);
                                ($iva) ? $articulo->porcentaje_iva = $iva : $iva = $articulo->porcentaje_iva = 0;

                                if($row->stock === null){
                                    $articulo->lleva_stock = 0;
                                } else {
                                    $articulo->lleva_stock = 1;
                                }

                                $articulo->save();

                                if($row->stock != null){
                                    $stock = New Stock();
                                    if($row->stock){
                                        $stock->cantidad = $row->stock;
                                    } else {
                                        $stock->cantidad = 0;
                                    }
                                    $stock->articulo_id = $articulo->id;
                                    if(Auth::user()->sucursal){
                                        $sucursal = Auth::user()->sucursal;
                                    } else {
                                        $sucursal = Sucursal::where('empresa_id', $empresa->id)->first();
                                    }
                                    $stock->sucursal_id = $sucursal->id;
                                    $stock->save();
                                }

                                $costo = New DetalleArticulo();
                                $costo->articulo_id = $articulo->id;
                                $costo->costo = $row->costo;
                                $costo->cantidad = 1;
                                $proveedor = Proveedor::where('empresa_id', $empresa->id)->where('razon_social', 'Otro')->first();
                                $costo->proveedor_id = $proveedor->id;
                                $costo->moneda_id = $moneda->id;
                                $costo->importe_iva = $row->costo * $porcentajesIva[$iva]/100;
                                $costo->save();

                                if($row->costo > 0) {
                                    $listasPrecios = ListaPrecio::where('empresa_id',$empresa->id)->get();
                                    foreach($listasPrecios as $lista){
                                        $precioNuevo = new Precio();
                                        $precioNuevo->precio = $row->costo;
                                        $precioNuevo->precio = ($row->costo * $lista->porcentaje)/100 + $row->costo;
                                        $precioNuevo->articulo_id = $articulo->id;
                                        $precioNuevo->lista_precio_id = $lista->id;
                                        $precioNuevo->save();
                                    }
                                }
                            }
                        }
                    });
                } else {
                    $hayError = true;
                    $mensaje = "Los encabezados del archivo son incorrectos";
                }
            } else {
                $hayError = true;
                $mensaje = "El archivo esta vacio";
            }

        });

      if ($hayError) {
        Session::flash('flash_danger', $mensaje.'!');
      } else {
        Session::flash('flash_success', $mensaje);
      }
      return redirect('articulos/articulo');

    }

/*    public function uploadImages(Request $request){
        $imgName = $request->file('image')->getClientOriginalName();
        $request->file->move(public_path('images'), $imgName);
        return response()->json(['uploaded' => '/images/'.$imgName]);
    }*/

}
