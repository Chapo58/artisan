<?php

namespace App\Http\Controllers\Frontend\Articulos;

use App\Http\Controllers\Controller;
use App\Models\Backend\Empresas\Sucursal;
use App\Models\Frontend\Articulos\EstadoMovimientoStock;
use App\Models\Frontend\Articulos\MovimientoStock;
use App\Models\Frontend\Articulos\Stock;
use App\Models\Frontend\Articulos\TipoMovimientoStock;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\HtmlString;
use Yajra\Datatables\Facades\Datatables;
use Session;

class MovimientoStockController extends Controller{

    public function index(){
        $empresa = Auth::user()->empresa;
        $sucursales = Sucursal::getSucursalesPorEmpresa($empresa);

        return view('frontend.articulos.movimiento-stock.index', compact('sucursales'));
    }

    public function datos(){
        $empresa = Auth::user()->empresa;
        $estadoMovimiento = EstadoMovimientoStock::$getArray;
        $sucursales = Sucursal::getSucursalesPorEmpresa($empresa);

        $query = MovimientoStock::select('movimientos_stock.id', 'movimientos_stock.cantidad', 'movimientos_stock.created_at', 'movimientos_stock.estado', 'sucursalOrigen.nombre as sucursal_origen', 'sucursalDestino.nombre as sucursal_destino', 'articulos.nombre as articulo', 'articulos.id as articulo_id', 'tipos_movimientos_stock.nombre as tipo_movimiento');
        $query->leftjoin('sucursales as sucursalOrigen', 'sucursalOrigen.id', '=', 'movimientos_stock.sucursal_origen_id');
        $query->leftjoin('sucursales as sucursalDestino', 'sucursalDestino.id', '=', 'movimientos_stock.sucursal_destino_id');
        $query->leftjoin('stocks', 'stocks.id', '=', 'movimientos_stock.stock_id');
        $query->leftjoin('articulos', 'articulos.id', '=', 'stocks.articulo_id');
        $query->leftjoin('tipos_movimientos_stock', 'tipos_movimientos_stock.id', '=', 'movimientos_stock.tipo_movimiento_stock_id');
        $query->where('articulos.empresa_id', $empresa->id);
        $query->whereNull('articulos.deleted_at');
        $query->orderBy('movimientos_stock.id', 'DESC');

        $movimientostock = $query->get();

        $datatables = Datatables::of($movimientostock);
        $datatables->setRowId('id');
        $datatables->removeColumn('id');
        $datatables->removeColumn('articulo_id');
        $datatables->editColumn('created_at', function ($movimiento) {
            return $movimiento->created_at->format('d/m/Y');
        });
       $datatables->editColumn('tipo_movimiento', function ($movimiento) {
            switch ($movimiento->tipo_movimiento) {
                case 'Rotura':
                    return new HtmlString('<span class="label label-info">'.$movimiento->tipo_movimiento.'</span>');
                    break;
                case 'Robo':
                    return new HtmlString('<span class="label label-danger">'.$movimiento->tipo_movimiento.'</span>');
                    break;
                case 'Vencimiento':
                    return new HtmlString('<span class="label label-warning">'.$movimiento->tipo_movimiento.'</span>');
                    break;
                case 'Ajuste':
                    return new HtmlString('<span class="label label-primary">'.$movimiento->tipo_movimiento.'</span>');
                    break;
                default:
                    return new HtmlString('<span class="label label-success">'.$movimiento->tipo_movimiento.'</span>');
            }
        });
       if($sucursales->count() > 1){
           $datatables->addColumn('action', function ($movimiento) {
               if($movimiento->estado == EstadoMovimientoStock::PENDIENTE){
                   return '
                    <a href="#" class="mytooltip aprobar" data-id="'.$movimiento->id.'">
                        <i class="fa fa-thumbs-o-up text-success m-r-10"></i>
                        <span class="tooltip-content3">Aprobar Solicitud</span>
                    </a>
                    <a href="#" class="mytooltip rechazar" data-id="'.$movimiento->id.'">
                        <i class="fa fa-thumbs-o-down text-danger"></i>
                        <span class="tooltip-content3">Rechazar Solicitud</span>
                    </a>
                    ';
               }
           });
           $datatables->editColumn('estado', function ($movimiento) use ($estadoMovimiento) {
               switch ($movimiento->estado) {
                   case EstadoMovimientoStock::PENDIENTE:
                       return new HtmlString('<span class="label label-warning">'.$estadoMovimiento[$movimiento->estado].'</span>');
                       break;
                   case EstadoMovimientoStock::APROBADO:
                       return new HtmlString('<span class="label label-success">'.$estadoMovimiento[$movimiento->estado].'</span>');
                       break;
                   case EstadoMovimientoStock::RECHAZADO:
                       return new HtmlString('<span class="label label-danger">'.$estadoMovimiento[$movimiento->estado].'</span>');
                       break;
                   default:
                       return "";
               }
           });
       } else {
           $datatables->removeColumn('sucursal_origen');
           $datatables->removeColumn('sucursal_destino');
           $datatables->removeColumn('estado');
       }
        $datatables->editColumn('cantidad', function ($movimiento) {
            if($movimiento->cantidad > 0){
                return new HtmlString('<strong class="text-success font-weight-bold">'.number_format($movimiento->cantidad, 0).'</strong>');
            } else {
                return new HtmlString('<strong class="text-danger font-weight-bold">'.number_format($movimiento->cantidad, 0).'</strong>');
            }
        });
        $datatables->editColumn('articulo', function ($movimiento) {
            return new HtmlString('<a href="'.url('/articulos/articulo/'.$movimiento->articulo_id).'">'.$movimiento->articulo.'</a>');
        });
       return  $datatables->make(true);
    }

    public function create(Request $request){
        $stock_id = $request->id;
        $tiposMovimientoStock = ['' => 'Seleccione una opción...'] + TipoMovimientoStock::pluck('nombre', 'id')->all();

        return view('frontend.articulos.movimiento-stock.create', compact('tiposMovimientoStock', 'stock_id'));
    }

    public function store(Request $request){
        $this->validate($request, [
			'cantidad' => 'required',
			'tipo_movimiento_stock_id' => 'required'
		]);
        $requestData = $request->all();

        $movimiento = new MovimientoStock($requestData);
        $movimiento->stock_id = (int)$requestData['stock_id'];
        $movimiento->tipo_movimiento_stock_id = (int)$requestData['tipo_movimiento_stock_id'];
        $movimiento->cantidad = (int)$requestData['cantidad'];
        $movimiento->usuario_id = Auth::user()->id;
        $movimiento->save();

        $stockUpdate = Stock::findOrFail($requestData['stock_id']);
        $stockUpdate->cantidad+= (int)$requestData['cantidad'];
        $stockUpdate->update();

        return redirect('articulos/movimiento-stock')->withFlashSuccess('Ajuste de stock creado correctamente.');
    }

    public function show($id){
        $movimientostock = MovimientoStock::findOrFail($id);

        if($movimientostock->stock->articulo->empresa_id == Auth::user()->empresa_id){
            return view('frontend.articulos.movimiento-stock.show', compact('movimientostock'));
        }
    }

    public function edit($id){
        $movimientostock = MovimientoStock::findOrFail($id);

        if($movimientostock->stock->articulo->empresa_id == Auth::user()->empresa_id){
            return view('frontend.articulos.movimiento-stock.edit', compact('movimientostock'));
        }
    }

    public function update($id, Request $request){
        $this->validate($request, [
			'cantidad' => 'required'
		]);
        $requestData = $request->all();
        
        $movimientostock = MovimientoStock::findOrFail($id);
        $movimientostock->update($requestData);

        return redirect('articulos/movimiento-stock')->withFlashSuccess('Ajuste de stock modificado correctamente.');
    }

    public function destroy($id){
        $movimientostock = MovimientoStock::findOrFail($id);

        if($movimientostock->stock->articulo->empresa_id == Auth::user()->empresa_id){
            MovimientoStock::destroy($id);
            return redirect('articulos/movimiento-stock')->withFlashSuccess('Movimiento de stock eliminado correctamente.');
        }
    }

    public function aprobar($id){
        $movimientostock = MovimientoStock::findOrFail($id);
        $movimientostock->estado = EstadoMovimientoStock::APROBADO;
        $movimientostock->save();

        $stockRestar = Stock::where('articulo_id',$movimientostock->stock->articulo_id)
        ->where('sucursal_id',$movimientostock->sucursalOrigen->id)->first();
        if($stockRestar) {
            $stockRestar->cantidad -= (int)$movimientostock->cantidad;
            $stockRestar->update();
        } else {
            $stock = New Stock();
            $stock->cantidad = -(int)$movimientostock->cantidad;
            $stock->articulo_id = $movimientostock->stock->articulo_id;
            $stock->sucursal_id = $movimientostock->sucursalOrigen->id;
            $stock->save();
        }

        $stockSumar = Stock::where('articulo_id',$movimientostock->stock->articulo_id)
            ->where('sucursal_id',$movimientostock->sucursalDestino->id)->first();
        if($stockSumar){
            $stockSumar->cantidad+= (int)$movimientostock->cantidad;
            $stockSumar->update();
        } else {
            $stock = New Stock();
            $stock->cantidad = (int)$movimientostock->cantidad;
            $stock->articulo_id = $movimientostock->stock->articulo_id;
            $stock->sucursal_id = $movimientostock->sucursalDestino->id;
            $stock->save();
        }

        return redirect('articulos/movimiento-stock');
    }

    public function rechazar($id){
        $movimientostock = MovimientoStock::findOrFail($id);
        $movimientostock->estado = EstadoMovimientoStock::RECHAZADO;
        $movimientostock->save();

        return redirect('articulos/movimiento-stock');
    }

}
