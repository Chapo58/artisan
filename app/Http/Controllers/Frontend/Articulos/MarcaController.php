<?php

namespace App\Http\Controllers\Frontend\Articulos;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Frontend\Configuraciones\HistorialController;
use App\Http\Controllers\ImageController;
use App\Models\Frontend\Articulos\Marca;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Facades\Datatables;
use Session;

class MarcaController extends Controller{

    public function index(){
        return view('frontend.articulos.marca.index');
    }

    public function datos(){
        $empresa = Auth::user()->empresa;
        $marcas = Marca::select('id','nombre','descripcion')->where('empresa_id', $empresa->id)->get();
        return Datatables::of($marcas)
            ->addColumn('action', function ($marca) {
                return '
                <a href="'.url('/articulos/marca/' . $marca->id).'" class="mytooltip"><i class="fa fa-eye text-success m-r-10"></i>
                    <span class="tooltip-content3">Ver Marca</span>
                </a>
                <a href="'.url('/articulos/marca/' . $marca->id . '/edit').'" class="mytooltip">
                    <i class="fa fa-pencil text-info m-r-10"></i>
                    <span class="tooltip-content3">Modificar Marca</span>
                </a>
                <a class="delete mytooltip pointer" data-id="'.$marca->id.'">
                    <i class="fa fa-close text-danger"></i>
                    <span class="tooltip-content3">Eliminar Marca</span>
                </a>
                ';
            })
            ->setRowId('id')
            ->removeColumn('id')
            ->make(true);
    }

    public function create(){
        return view('frontend.articulos.marca.create');
    }

    public function store(Request $request){
        $this->validate($request, [
			'nombre' => 'required'
		]);
        $requestData = $request->all();
        
        $nuevaMarca = new Marca($requestData);
        $nuevaMarca->empresa_id = Auth::user()->empresa_id;

        $controllerImagen = new ImageController();
        $imagen_url = $controllerImagen->guardarImagen($request,'/uploads/marcas/',170,98);
        if($imagen_url != false){
            $nuevaMarca->imagen_url = $imagen_url;
        }

        $nuevaMarca->save();

        return redirect('articulos/marca')->withFlashSuccess('Marca creada correctamente.');
    }

    public function show($id){
        $marca = Marca::findOrFail($id);

        if($marca->empresa_id == Auth::user()->empresa_id){
            return view('frontend.articulos.marca.show', compact('marca'));
        }else{
            return redirect('articulos/marca');
        }
    }

    public function edit($id){
        $marca = Marca::findOrFail($id);

        if($marca->empresa_id == Auth::user()->empresa_id){
            return view('frontend.articulos.marca.edit', compact('marca'));
        }else{
            return redirect('articulos/marca');
        }

    }

    public function update($id, Request $request){
        $this->validate($request, [
			'nombre' => 'required'
		]);
        $requestData = $request->all();

        $marca = Marca::findOrFail($id);

        if($marca->empresa_id == Auth::user()->empresa_id){
            if($marca->nombre != 'Otra'){
                $controllerImagen = new ImageController();
                $imagen_url = $controllerImagen->guardarImagen($request,'/uploads/marcas/',170,98);
                if($imagen_url != false){
                    $marca->imagen_url = $imagen_url;
                }
                $marca->update($requestData);
            } else {
                return redirect('articulos/marca')->withFlashWarning('Esta marca no puede ser modificada.');
            }
        }
        return redirect('articulos/marca')->withFlashSuccess('Marca modificada correctamente.');
    }

    public function destroy($id){
        $marca = Marca::findOrFail($id);

        if($marca->empresa_id == Auth::user()->empresa_id){
            if($marca->nombre != 'Otra'){
                $marca->delete();
            } else {
                return redirect('articulos/marca')->withFlashWarning('Esta marca no puede ser eliminada.');
            }
        }
        return redirect('articulos/marca')->withFlashSuccess('Marca eliminada correctamente.');
    }

    public function guardarNuevaMarca(Request $request){
        $marca = New Marca();

        DB::transaction(function() use ($marca, $request) {
            $nombre = $request->input('nombre');

            $marca->nombre = $nombre;
            $marca->empresa_id = Auth::user()->empresa_id;
            $marca->save();

            $controllerHistorial = new HistorialController();
            $controllerHistorial->nuevoHistorialCreacion($marca, $request);
        });

        $resultado = [
            'id'    => $marca->id,
            'nombre'=> $marca->nombre
        ];

        return response()->json($resultado);
    }
}
