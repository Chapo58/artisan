<?php

namespace App\Http\Controllers\Frontend\Articulos;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Frontend\Configuraciones\HistorialController;
use App\Models\Backend\Empresas\Sucursal;
use App\Models\Frontend\Articulos\Articulo;
use App\Models\Frontend\Articulos\EstadoMovimientoStock;
use App\Models\Frontend\Articulos\MovimientoStock;
use App\Models\Frontend\Articulos\Stock;
use App\Models\Frontend\Configuraciones\Estado;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Session;

class StockController extends Controller{

    public function index(Request $request){
        $keyword = $request->get('search');
        $filtro = $request->get('filtro');
        $perPage = 25;
        $empresa = Auth::user()->empresa;

        $query = Stock::select('stocks.*');
        $query->join('articulos', 'articulos.id', '=', 'stocks.articulo_id');
        $query->where('articulos.empresa_id', $empresa->id);
        $query->whereNull('stocks.deleted_at');


        if (!empty($keyword)) {
            $query->where(function($q) use ($keyword) {
                $q->where('articulos.nombre', 'LIKE', "%$keyword%")
                    ->orWhere('articulos.codigo', 'LIKE', "%$keyword%")
                    ->orWhere('articulos.codigo_barra', "$keyword");
            });
        }

        if($filtro){
            switch ($request->filtro) {
                case 'sinStock':
                    $query->where('stocks.cantidad', 0);
                    break;
                case 'bajasExistencias':
                    $query->whereRaw('stocks.cantidad <= stocks.minimo');
                    break;
                case 'conAlarma':
                    $query->whereRaw('stocks.cantidad <= stocks.minimo');
                    $query->where('stocks.alarma', 1);
                    break;
            }
            $perPage = 600;
        }

        $query->orderBy('articulos.nombre', 'ASC');

        $stock = $query->paginate($perPage);

        $sucursales = Sucursal::getSucursalesPorEmpresa($empresa);
        $sucursalesArray = Sucursal::getSucursalesPorEmpresaArray($empresa);

        $filtros = ['' => 'Sin Filtros',
            'sinStock' => 'Sin Stock',
            'bajasExistencias' => 'Bajas Existencias',
            'conAlarma' => 'Alarma'];

        return view('frontend.articulos.stock.index', compact('stock','filtros','sucursales','sucursalesArray'));
    }

    public function create(){
        return redirect('articulos/stock');
    }

    public function store(Request $request){
        $this->validate($request, [
			'cantidad' => 'required'
		]);
        $requestData = $request->all();
        
        Stock::create($requestData);

        return redirect('articulos/stock')->withFlashSuccess('Stock creado correctamente.');
    }

    public function show($id){
        $stock = Stock::findOrFail($id);

        if(Auth::user()->hasRole('Usuario Admin')){
            if($stock->articulo->empresa_id == Auth::user()->empresa_id){
                return view('frontend.articulos.stock.show', compact('stock'));
            }
        }elseif($stock->sucursal_id == Auth::user()->sucursal_id){
            return view('frontend.articulos.stock.show', compact('stock'));
        }

        return redirect('articulos/stock');
    }

    public function edit($id){
        $stock = Stock::findOrFail($id);

        if(Auth::user()->hasRole('Usuario Admin')){
            if($stock->articulo->empresa_id == Auth::user()->empresa_id){
                return view('frontend.articulos.stock.edit', compact('stock'));
            }
        }elseif($stock->sucursal_id == Auth::user()->sucursal_id){
            return view('frontend.articulos.stock.edit', compact('stock'));
        }

        return redirect('articulos/stock');
    }

    public function update($id, Request $request){
        $this->validate($request, [
			'cantidad' => 'required'
		]);
        $stock = Stock::findOrFail($id);

        if(Auth::user()->hasRole('Usuario Admin')){
            if($stock->articulo->empresa_id != Auth::user()->empresa_id){
                return redirect('articulos/stock');
            }
        }elseif($stock->sucursal_id != Auth::user()->sucursal_id){
            return redirect('articulos/stock');
        }

        DB::transaction(function () use ($stock, $request) {
            $requestData = $request->all();
            $controllerHistorial = new HistorialController();

            if(!isset($requestData["alarma"])){
                $stock->alarma = false;
            }

            $stock->update($requestData);
            $controllerHistorial->nuevoHistorialActualizacion($stock, $request);
        });

        return redirect('articulos/stock')->withFlashSuccess('Stock modificado correctamente.');
    }

    public function destroy($id){
        $stock = Stock::findOrFail($id);

        if(Auth::user()->hasRole('Usuario Admin')){
            if($stock->articulo->empresa_id != Auth::user()->empresa_id){
                return redirect('articulos/stock');
            }
        }elseif($stock->sucursal_id != Auth::user()->sucursal_id){
            return redirect('articulos/stock');
        }

        DB::transaction(function () use ($stock) {
            $stock->delete();
            $controllerHistorial = new HistorialController();

            $controllerHistorial->nuevoHistorialEliminacion($stock);
        });

        return redirect('articulos/stock')->withFlashSuccess('Stock eliminado correctamente.');
    }

    public function solicitudMasiva(){
        $empresa = Auth::user()->empresa;
        $sucursalesArray = Sucursal::getSucursalesPorEmpresaArray($empresa);

        return view('frontend.articulos.stock.solicitud_stock_masiva', compact('sucursalesArray'));
    }

    public function guardarSolicitudMasiva(Request $request){

        DB::transaction(function() use ($request) {
            $requestData = $request->all();
            $empresa = Auth::user()->empresa;
            $sucursalOrigen = (int)$request->sucursal_id; // Donde piden el articulo
            $sucursalDestino = Auth::user()->sucursal->id; // Desde donde lo piden

            if (isset($requestData['detalles'])) {
                $detalles = $requestData['detalles'];
                foreach ($detalles as $detalle) {
                    if ((int)$detalle['estado'] == Estado::ACTIVO && isset($detalle['articulo_id']) &&
                        isset($detalle['cantidad'])) {

                        $movimiento = new MovimientoStock();
                        $stockId = Stock::where('articulo_id',$detalle['articulo_id'])->where('sucursal_id',$sucursalOrigen)->first();
                        if($stockId){
                            $movimiento->stock_id = $stockId->id;
                            $movimiento->tipo_movimiento_stock_id = 5; // Transferencia
                            $movimiento->cantidad = (int)$detalle['cantidad'];
                            $movimiento->sucursal_origen_id = $sucursalOrigen;
                            $movimiento->sucursal_destino_id = $sucursalDestino;
                            $movimiento->estado = EstadoMovimientoStock::PENDIENTE;
                            $movimiento->save();
                        }
                    }
                }
            }
        });
        return redirect('articulos/movimiento-stock')->withFlashSuccess('Solicitudes creadas correctamente.');
    }

    public function transferir(Request $request){

        $this->validate($request, [
            'sucursal' => 'required',
            'cantidad' => 'required'
        ]);

        $sucursalOrigen = (int)$request->sucursal; // Donde piden el articulo
        $sucursalDestino = Auth::user()->sucursal->id; // Desde donde lo piden
        $cantidad = (int)$request->cantidad;

        $movimiento = new MovimientoStock();
        $movimiento->stock_id = (int)$request->idStock;
        $movimiento->tipo_movimiento_stock_id = 5; // Transferencia
        $movimiento->cantidad = $cantidad;
        $movimiento->sucursal_origen_id = $sucursalOrigen;
        $movimiento->sucursal_destino_id = $sucursalDestino;
        $movimiento->estado = EstadoMovimientoStock::PENDIENTE;
        $movimiento->save();

        return redirect('articulos/movimiento-stock');

    }
}
