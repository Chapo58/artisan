<?php

namespace App\Http\Controllers\Frontend\Articulos;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Frontend\Configuraciones\HistorialController;
use App\Models\Frontend\Articulos\Rubro;
use App\Models\Frontend\Articulos\Subrubro;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Facades\Datatables;
use Session;

class SubrubroController extends Controller{

    public function index(Request $request){
        $keyword = $request->get('search');
        $perPage = 20;

        if (!empty($keyword)) {
            $subrubro = Subrubro::where('nombre', 'LIKE', "%$keyword%")
                ->where('empresa_id', Auth::user()->empresa_id)
                ->paginate($perPage);
        } else {
            $subrubro = Subrubro::where('empresa_id', Auth::user()->empresa_id)
                ->paginate($perPage);
        }

        return view('frontend.articulos.subrubro.index', compact('subrubro','keyword'));
    }

    public function datos($id){
        $subRubros = Subrubro::select('id','nombre','descripcion')->where('rubro_id', $id)->orderBy('nombre','ASC')->get();

        return Datatables::of($subRubros)
            ->addColumn('action', function ($subRubro) {
                return '
                <a href="'.url('/articulos/subrubro/' . $subRubro->id).'" class="mytooltip"><i class="fa fa-eye text-success m-r-10"></i>
                    <span class="tooltip-content3">Ver Subrubro</span>
                </a>
                <a href="'.url('/articulos/subrubro/' . $subRubro->id . '/edit').'" class="mytooltip">
                    <i class="fa fa-pencil text-info m-r-10"></i>
                    <span class="tooltip-content3">Modificar Subrubro</span>
                </a>
                <a class="delete-subrubro mytooltip pointer" data-id="'.$subRubro->id.'">
                    <i class="fa fa-close text-danger"></i>
                    <span class="tooltip-content3">Eliminar Subrubro</span>
                </a>
                ';
            })
            ->setRowId(function ($subRubro) {
                return 'subrubro-'.$subRubro->id;
            })
            ->removeColumn('id')
            ->make(true);
    }

    public function create($id = null){
        $rubros = ['' => 'Seleccione una opción...'] + Rubro::where('empresa_id', Auth::user()->empresa_id)->orderBy('nombre','ASC')->get()->pluck('nombre', 'id')->toArray();
        $subrubro = New Subrubro();
        if($id){
            $subrubro->rubro_id = $id;
        }
        return view('frontend.articulos.subrubro.create', compact('rubros','subrubro'));
    }

    public function store(Request $request){
        $this->validate($request, [
            'nombre'   => 'required',
            'rubro_id' => 'required',
        ]);
        $requestData = $request->all();

        $nuevoSubrubro = new Subrubro($requestData);
        $nuevoSubrubro->empresa_id = Auth::user()->empresa_id;

        $nuevoSubrubro->save();

        return redirect('articulos/subrubro')->withFlashSuccess('SubRubro creado correctamente.');
    }

    public function show($id){
        $subrubro = Subrubro::findOrFail($id);

        if($subrubro->empresa_id == Auth::user()->empresa_id){
            return view('frontend.articulos.subrubro.show', compact('subrubro'));
        }else{
            return redirect('articulos/subrubro');
        }
    }

    public function edit($id){
        $subrubro = Subrubro::findOrFail($id);

        if($subrubro->empresa_id == Auth::user()->empresa_id){
            $rubros = ['' => 'Seleccione una opción...'] + Rubro::where('empresa_id', Auth::user()->empresa_id)->get()->pluck('nombre', 'id')->toArray();

            return view('frontend.articulos.subrubro.edit', compact('subrubro', 'rubros'));
        }else{
            return redirect('articulos/subrubro');
        }
    }

    public function update($id, Request $request){
        $this->validate($request, [
            'nombre' => 'required',
            'rubro_id' => 'required',
        ]);
        $requestData = $request->all();
        
        $subrubro = Subrubro::findOrFail($id);

        if($subrubro->empresa_id == Auth::user()->empresa_id) {
            if($subrubro->nombre != 'Otro'){
                $subrubro->update($requestData);

                Session::flash('flash_success', 'SubRubro modificado correctamente.');
            }
        }

        return redirect('articulos/subrubro');
    }

    public function destroy($id){
        $subrubro = Subrubro::findOrFail($id);

        if($subrubro->empresa_id == Auth::user()->empresa_id) {
            if($subrubro->nombre != 'Otro'){
                $subrubro->delete();

                Session::flash('flash_success', 'SubRubro eliminado correctamente.');
            }
        }

        return redirect('articulos/subrubro');
    }

    public function autocomplete(Request $request){
        $term = $request->term;

        $subrubros = Subrubro::where('nombre', 'LIKE', "%$term%")
            ->where('empresa_id', Auth::user()->empresa_id)
            ->paginate(5);

        foreach ($subrubros as $subrubro)
        {
            $results[] = ['value' => $subrubro->nombre];
        }
        return response()->json($results);
    }

    public function guardarNuevoSubrubro(Request $request){
        $subrubro = New Subrubro();

        DB::transaction(function() use ($subrubro, $request) {
            $nombre = $request->input('nombre');
            $rubro_id = $request->input('rubro_id');

            $subrubro->nombre = $nombre;
            $subrubro->empresa_id = Auth::user()->empresa_id;
            $subrubro->rubro_id = $rubro_id;
            $subrubro->save();

            $controllerHistorial = new HistorialController();
            $controllerHistorial->nuevoHistorialCreacion($subrubro, $request);
        });

        $resultado = [
            'id'    => $subrubro->id,
            'nombre'=> $subrubro->nombre
        ];

        return response()->json($resultado);
    }
}
