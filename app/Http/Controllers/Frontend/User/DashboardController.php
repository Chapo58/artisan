<?php

namespace App\Http\Controllers\Frontend\User;

use App\Http\Controllers\Controller;
use App\Models\Frontend\Calendario\Event;
use App\Models\Frontend\Ventas\Venta;
use App\Models\Frontend\Ventas\DetalleVenta;
use App\Models\Frontend\Articulos\Articulo;
use App\Models\Frontend\Caja\CierreCaja;
use App\Models\Frontend\Fondos\Cheque;
use App\Models\Frontend\Fondos\EstadoCheque;
use App\Models\Frontend\Personas\Cliente;
use App\Models\Frontend\Configuraciones\TipoComprobante;
use ConsoleTVs\Charts\Facades\Charts;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;

class DashboardController extends Controller{

    public function index(Request $request){
        $empleado = Auth::user()->empleado;
        $empresa = Auth::user()->empresa;

        $rol = Auth::user()->roles->first()->name;
        if($rol == 'Administrador' || $rol == 'Distribuidor'){
            return redirect('admin/dashboard');
        }

        $cantArts = Articulo::where('empresa_id',$empresa->id)->count();
        if($cantArts == 1){
            return redirect('welcome');
        }

        $periodoArticulos = null;
        $periodoClientes = null;

        $cierreCaja = CierreCaja::getUltimoCierreCajaPorEmpleado($empleado);

        $dineroCaja = "Cerrada";
        if(isset($cierreCaja)){
            if($cierreCaja->fecha_cierre != null){
                $dineroCaja = "Cerrada";
            }else{
                $dineroCaja = DB::table('movimiento_cajas')
                    ->select(DB::raw('sum(entrada-salida) AS total'))
                    ->where('created_at',">=",$cierreCaja->fecha_apertura)
                    ->where('caja_id',"=",$cierreCaja->caja_id)
                    ->where('empresa_id', $empresa->id)
                    ->whereNull('deleted_at')
                    ->first();
                $dineroCaja = $dineroCaja->total;
            }
        }

        $totalVentas = Venta::where('fecha', '>=', Carbon::now()->startOfMonth())
        ->where('empresa_id','=', $empresa->id)
        ->sum('total');

        $start = new Carbon('first day of last month');
        $end = new Carbon('last day of last month');
        $totalVentasMesPasado = Venta::where('fecha', '>=', $start)
        ->where('fecha', '<=', $end)
        ->where('empresa_id','=', $empresa->id)
        ->sum('total');

        $articulos = Articulo::where('empresa_id','=', $empresa->id)->count();

        $importesCuentasCorrientes = Cliente::where('empresa_id','=', $empresa->id)->sum('saldo_actual');

        $query = Venta::join('clientes', 'clientes.id', '=', 'ventas.cliente_id');
        $query->join('puntos_de_ventas', 'puntos_de_ventas.id', '=', 'ventas.punto_venta_id');
        $query->join('monedas', 'monedas.id', '=', 'ventas.moneda_id');
        $query->join('lista_precios', 'lista_precios.id', '=', 'ventas.lista_precios_id');
        $query->select('ventas.*', 'clientes.razon_social', 'puntos_de_ventas.numero as punto_venta', 'monedas.signo', 'lista_precios.nombre as lista');
        $query->where('ventas.deleted_at', '=', Null);
        $query->where('ventas.empresa_id', $empresa->id);
        $query->orderBy('ventas.fecha','DESC');
        if (Auth::user()->hasRole('Usuario Vendedor')) {
            $query->where('ventas.sucursal_id', Auth::user()->sucursal_id);
        }
        $venta = $query->paginate(5);
        $tipos_comprobantes = TipoComprobante::$comprobantes;

        $query = Cheque::where('empresa_id', $empresa->id);
        $query->where('vencimiento',">=",date('Y-m-d'));
        $query->where('estado',EstadoCheque::PENDIENTE);
        $cheques = $query->paginate(10);

        $arts = DetalleVenta::select(DB::raw('COUNT(articulo_id) as cantidad'), 'articulos.nombre');
        $arts->join('articulos','articulos.id','=','detalles_ventas.articulo_id');
        $arts->where('articulos.empresa_id', $empresa->id);
        $arts->whereNull('articulos.deleted_at');
        if(isset($request->ventasArticulos) && $request->ventasArticulos != 'Siempre'){
            $periodoArticulos = $request->ventasArticulos;
            if($periodoArticulos == 1){ // Articulos mas vendidos de esta semana
                $start = Carbon::now()->startOfWeek();
                $end = Carbon::now()->endOfWeek();
            } else if($periodoArticulos == 2){ // Articulos mas vendidos de ese mes
                $start = new Carbon('first day of this month');
                $end = new Carbon('last day of this month');
            } else { // Articulos mas vendidos de este año
                $start = Carbon::parse('first day of January');
                $end = Carbon::parse('last day of December');
            }
            $arts->whereBetween('detalles_ventas.created_At', [$start,$end]);
        }
        $arts->groupBy('articulos.nombre');
        $arts->orderBy('cantidad','DESC');
        $arts->limit(10);
        $articulosVendidos = $arts->get();

        $cantidadesArticulos = $articulosVendidos->pluck('cantidad');
        $nombresArticulos = $articulosVendidos->pluck('nombre');

        for($i = 0; $i < count($nombresArticulos) ; $i++) {
            $nombresArticulos[$i] = substr($nombresArticulos[$i],0,15).'..';
        }

        $colores = array('#FF0000','#FFB300','#FFFB00','#64FA00','#00FAF6','#0075FA','#F062FC','#D67DFF','#B1FFAD','#FFC9D1');

        $chartArticulos = Charts::create('pie', 'highcharts')
                ->title('Articulos')
                ->labels($nombresArticulos)
                ->values($cantidadesArticulos)
                ->colors($colores)
                ->dimensions(0,0);

        $cli = Venta::select(DB::raw('SUM(total) as total'), 'clientes.razon_social','clientes.id');
        $cli->join('clientes','clientes.id','=','ventas.cliente_id');
        $cli->where('ventas.empresa_id','=', $empresa->id);
        $cli->where('clientes.razon_social','<>', 'Consumidor Final');
        if(isset($request->rankingClientes) && $request->rankingClientes != 'Siempre'){
            $periodoClientes = $request->rankingClientes;
            if($periodoClientes == 1){ // Articulos mas vendidos de esta semana
                $start = Carbon::now()->startOfWeek();
                $end = Carbon::now()->endOfWeek();
            } else if($periodoClientes == 2){ // Articulos mas vendidos de ese mes
                $start = new Carbon('first day of this month');
                $end = new Carbon('last day of this month');
            } else { // Articulos mas vendidos de este año
                $start = Carbon::parse('first day of January');
                $end = Carbon::parse('last day of December');
            }
            $cli->whereBetween('ventas.fecha', [$start,$end]);
        }
        $cli->groupBy('clientes.razon_social','clientes.id');
        $cli->orderBy('total','DESC');
        $cli->limit(6);

        $rankingClientes = $cli->get();

        if(!$rankingClientes->count()){ // Si no hay ranking de clientes es porque seguramente sea una nueva empresa, que solamente tiene a consumidor final
            $cli = Venta::select(DB::raw('SUM(total) as total'), 'clientes.razon_social','clientes.id');
            $cli->join('clientes','clientes.id','=','ventas.cliente_id');
            $cli->where('ventas.empresa_id','=', $empresa->id);
            $cli->groupBy('clientes.razon_social','clientes.id');
            $rankingClientes = $cli->get();
        }

        $query = Event::where('empresa_id','=', $empresa->id);
        if(Auth::user()->hasRole('Usuario Vendedor')){
            $query->where('empleado_id','=', $empleado->id);
        }
        $query->whereBetween('fecha_inicio', [Carbon::now()->startOfDay(),Carbon::now()->endOfWeek()]);
        $query->limit(10);
        $eventos = $query->get();

        $mostrarAlertaVencimiento = false;
        if($empresa->fecha_vencimiento && $empresa->fecha_vencimiento <= Carbon::now()->addDays(15)){
            $mostrarAlertaVencimiento = true;
        }

        $diasSemana = [
            0 => 'Domingo',
            1 => 'Lunes',
            2 => 'Martes',
            3 => 'Miercoles',
            4 => 'Jueves',
            5 => 'Viernes',
            6 => 'Sabado',
        ];

        return view('frontend.user.dashboard', compact('dineroCaja','totalVentas','articulos','importesCuentasCorrientes','venta','tipos_comprobantes','cheques','totalVentasMesPasado','chartArticulos','rankingClientes','eventos','diasSemana','periodoArticulos','periodoClientes','mostrarAlertaVencimiento'));
    }
}
