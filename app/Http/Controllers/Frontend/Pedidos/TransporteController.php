<?php

namespace App\Http\Controllers\Frontend\Pedidos;

use App\Http\Controllers\Controller;
use App\Models\Frontend\Pedidos\Transporte;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;

class TransporteController extends Controller
{

    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $transporte = Transporte::where('codigo_interno', 'LIKE', "%$keyword%")
				->orWhere('nombre', 'LIKE', "%$keyword%")
                ->orWhere('direccion', 'LIKE', "%$keyword%")
                ->orWhere('codigo_postal', 'LIKE', "%$keyword%")
                ->orWhere('telefono', 'LIKE', "%$keyword%")
                ->orWhere('email', 'LIKE', "%$keyword%")
                ->orWhere('observaciones', 'LIKE', "%$keyword%")
                ->orWhere('empresa_id', 'LIKE', "%$keyword%")
                ->paginate($perPage);
        } else {
            $transporte = Transporte::paginate($perPage);
        }

        return view('frontend.pedidos.transporte.index', compact('transporte'));
    }

    public function create()
    {
        return view('frontend.pedidos.transporte.create');
    }

    public function store(Request $request)
    {
        
        $requestData = $request->all();

       
        $nuevoTransporte = new Transporte($requestData);
        $nuevoTransporte->empresa_id = Auth::user()->empresa_id;
        $nuevoTransporte->save();
        

        Session::flash('flash_message', trans('labels.frontend.transporte').trans('alerts.frontend.created').'!');

        return redirect('pedidos/transporte');
    }

    public function show($id)
    {
        $transporte = Transporte::findOrFail($id);

        return view('frontend.pedidos.transporte.show', compact('transporte'));
    }

    public function edit($id)
    {
        $transporte = Transporte::findOrFail($id);

        return view('frontend.pedidos.transporte.edit', compact('transporte'));
    }

    public function update($id, Request $request)
    {
        
        $requestData = $request->all();
        
        $transporte = Transporte::findOrFail($id);
        $transporte->update($requestData);

        Session::flash('flash_message', trans('labels.frontend.transporte').trans('alerts.frontend.updated').'!');

        return redirect('pedidos/transporte');
    }

    public function destroy($id)
    {
        Transporte::destroy($id);

        Session::flash('flash_message',  trans('labels.frontend.transporte').trans('alerts.frontend.deleted').'!');

        return redirect('pedidos/transporte');
    }
}
