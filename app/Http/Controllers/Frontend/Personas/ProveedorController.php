<?php

namespace App\Http\Controllers\Frontend\Personas;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Frontend\Configuraciones\HistorialController;
use App\Models\Frontend\Articulos\DetalleArticulo;
use App\Models\Frontend\Caja\EstadoCaja;
use App\Models\Frontend\Compras\Compra;
use App\Models\Frontend\Configuraciones\CondicionIva;
use App\Models\Frontend\Configuraciones\Localidad;
use App\Models\Frontend\Configuraciones\Pais;
use App\Models\Frontend\Configuraciones\Provincia;
use App\Models\Frontend\Configuraciones\TipoComprobante;
use App\Models\Frontend\Personas\Domicilio;
use App\Models\Frontend\Personas\Proveedor;
use App\Models\Frontend\Ventas\CuentaCorriente;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Facades\Datatables;
use Session;

class ProveedorController extends Controller{

    public function index(){
        return view('frontend.personas.proveedor.index');
    }

    public function datos(){
        $empresa = Auth::user()->empresa;
        $proveedores = Proveedor::select('proveedores.id','condiciones_iva.nombre as condicioniva','proveedores.razon_social','proveedores.cuit','proveedores.email','proveedores.telefono')
            ->leftjoin('condiciones_iva','condiciones_iva.id','proveedores.condicion_iva_id')
            ->where('empresa_id','=', $empresa->id)
            ->get();

        $estadoCaja = EstadoCaja::CERRADA;
        if(isset(Auth::user()->empleado->caja)){
            if(Auth::user()->empleado->caja->estado == EstadoCaja::ABIERTA) {
                $estadoCaja = EstadoCaja::ABIERTA;
            }
        }
        return Datatables::of($proveedores)
            ->addColumn('action', function ($proveedor) use ($estadoCaja) {
                $return = '
                 <div class="btn-group">
                    <button type="button" class="btn btn-secondary dropdown-toggle btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Acciones
                    </button>
                    <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 33px, 0px); top: 0px; left: 0px; will-change: transform;">
                        <a class="dropdown-item" href="'.url('/personas/proveedor/' . $proveedor->id).'">
                            <i class="fa fa-eye" aria-hidden="true"></i> Ver Proveedor
                        </a>
                        <a class="dropdown-item" href="'.url('/personas/proveedor/' . $proveedor->id . '/edit').'">
                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Modificar Proveedor
                        </a>
                        <a class="dropdown-item" href="'.url('/personas/proveedor/' . $proveedor->id . '/showArticulos').'">
                            <i class="fa fa-eye" aria-hidden="true"></i> Ver Articulos
                        </a>
                        <a class="dropdown-item" href="'.url('/ventas/cuenta-corriente/show-proveedor/' . $proveedor->id).'">
                            <i class="fa fa-id-card" aria-hidden="true"></i> Cuenta Corriente
                        </a>';
                if($estadoCaja == EstadoCaja::ABIERTA){
                    $return.='
                        <a class="dropdown-item" href="'.url('/compras/orden-pago/crearOrdenPagoProveedor/'.$proveedor->id).'">
                            <i class="fa fa-plus" aria-hidden="true"></i> Orden de Pago
                        </a>';
                } else {
                    $return.='
                        <a class="dropdown-item" href="#" disabled>
                            <i class="fa fa-plus" aria-hidden="true"></i> Orden de Pago
                        </a>';
                }
                $return.='
                        <a href="#" class="dropdown-item delete" data-id="'.$proveedor->id.'" data-toggle="tooltip">
                            <i class="fa fa-close"></i> Eliminar Proveedor
                        </a>
                    </div>
                </div>
                ';
                return $return;
            })
            ->setRowId('id')
            ->removeColumn('id')
            ->make(true);

    }

    public function create(){
        $condiciones_iva = CondicionIva::getCondicionesIvaArray();
        $paises = ['' => 'Seleccione una opción...'] + Pais::pluck('nombre', 'id')->all();
        $provincias = ['' => 'Seleccione una opción...'] + Provincia::orderBy('nombre')->pluck('nombre', 'id')->all();

        return view('frontend.personas.proveedor.create', compact('condiciones_iva', 'paises', 'provincias'));
    }

    public function store(Request $request){
        $this->validate($request, [
			'razon_social' => 'required',
			'cuit' => 'required',
            'domicilio.direccion' => 'required',
            'domicilio.pais_id' => 'required',
            'domicilio.provincia_id' => 'required',
            'domicilio.localidad' => 'required'
		]);

        DB::transaction(function() use ($request) {
            $requestData = $request->all();
            $proveedor = New Proveedor($requestData);

            $domicilio = New Domicilio($requestData['domicilio']);
            $domicilio->save();

            $proveedor->domicilio_id = $domicilio->id;
            $proveedor->empresa_id = Auth::user()->empresa_id;
            $proveedor->save();

            $controllerHistorial = new HistorialController();
            $controllerHistorial->nuevoHistorialCreacion($proveedor, $request);

            Session::flash('flash_success', 'Proveedor creado correctamente.');
        });

        return redirect('personas/proveedor');
    }

    public function show($id){
        $proveedor = Proveedor::findOrFail($id);
        $empresa = Auth::user()->empresa;

        if($proveedor->empresa_id == Auth::user()->empresa_id){
            $compras = Compra::where('proveedor_id',$id)->get();
            $tipos_comprobantes = TipoComprobante::$comprobantes;
            $cuentasCorrientes = CuentaCorriente::listarCuentasCorrientesPorProveedorEmpresa($proveedor, $empresa);
            $articulosProveedor = DetalleArticulo::where('proveedor_id', $id)->get();

            return view('frontend.personas.proveedor.show', compact('proveedor','compras','tipos_comprobantes','cuentasCorrientes','articulosProveedor'));
        }else{
            return redirect('personas/proveedor');
        }
    }

    public function showArticulos($id){
        $proveedor = Proveedor::findOrFail($id);

        if($proveedor->empresa_id == Auth::user()->empresa_id) {
            $articulosProveedor = DetalleArticulo::where('proveedor_id', $id)->get();

            return view('frontend.personas.proveedor.show_articulos', compact('proveedor', 'articulosProveedor'));
        }else{
            return redirect('personas/proveedor');
        }
    }

    public function edit($id){
        $proveedor = Proveedor::findOrFail($id);

        if($proveedor->empresa_id == Auth::user()->empresa_id){
            if($proveedor->razon_social != 'Otro'){
                $condiciones_iva = CondicionIva::getCondicionesIvaArray();
                $paises = ['' => 'Seleccione una opción...'] + Pais::pluck('nombre', 'id')->all();
                $provincias = ['' => 'Seleccione una opción...'] + Provincia::orderBy('nombre')->pluck('nombre', 'id')->all();

                return view('frontend.personas.proveedor.edit', compact('proveedor', 'condiciones_iva', 'paises', 'provincias'));
            }else{
                return redirect('personas/proveedor');
            }
        }else{
            return redirect('personas/proveedor');
        }
    }

    public function update($id, Request $request){
        $this->validate($request, [
			'razon_social' => 'required',
			'cuit' => 'required',
            'domicilio.direccion' => 'required',
            'domicilio.pais_id' => 'required',
            'domicilio.provincia_id' => 'required',
            'domicilio.localidad' => 'required'
		]);
        $proveedor = Proveedor::findOrFail($id);

        if($proveedor->empresa_id == Auth::user()->empresa_id) {
            if($proveedor->razon_social != 'Otro'){
                DB::transaction(function() use ($proveedor, $request) {
                    $requestData = $request->all();

                    if(isset($proveedor->domicilio)){
                        $proveedor->domicilio->update($requestData['domicilio']);
                    }else{
                        $domicilio = New Domicilio($requestData['domicilio']);
                        $domicilio->save();
                        $proveedor->domicilio_id = $domicilio->id;
                    }
                    $proveedor->domicilio->update($requestData['domicilio']);
                    $proveedor->update($requestData);

                    $controllerHistorial = new HistorialController();
                    $controllerHistorial->nuevoHistorialActualizacion($proveedor, $request);

                    Session::flash('flash_success', 'Proveedor modificado correctamente.');
                });
            }
        }

        return redirect('personas/proveedor');
    }

    public function destroy($id){
        $proveedor = Proveedor::findOrFail($id);

        if($proveedor->empresa_id == Auth::user()->empresa_id) {
            if($proveedor->razon_social != 'Otro'){
                DB::transaction(function() use ($proveedor) {
                    $proveedor->delete();

                    $controllerHistorial = new HistorialController();
                    $controllerHistorial->nuevoHistorialEliminacion($proveedor);

                    Session::flash('flash_success', 'Proveedor eliminado correctamente.');
                });
            }
        }

        return redirect('personas/proveedor');
    }

    public function autocomplete(Request $request){
        $term = $request->term;

        $proveedores = Proveedor::where('razon_social', 'LIKE', "%$term%")
            ->where('empresa_id', Auth::user()->empresa_id)
            ->paginate(5);

        foreach ($proveedores as $proveedor)
        {
            $results[] = ['value' => $proveedor->razon_social];
        }
        return response()->json($results);
    }

    public function obtenerProveedorParaCompra(Request $request){
        $term = $request->term;

        $proveedores = DB::table('proveedores')
            ->where('empresa_id', Auth::user()->empresa_id)
            ->whereNull('deleted_at')
            ->where(function($q) use ($term) {
                $q->where('razon_social', 'LIKE', "%$term%")
                    ->orWhere('cuit', 'LIKE', "%$term%");
            })
            ->paginate(3);

        $condicion_iva_empresa = Auth::user()->empresa->condicion_iva_id;

        $tipo_comprobante_id = null;
        foreach ($proveedores as $proveedor) {
            if($proveedor->condicion_iva_id == 1) {//Proveedor Monotributo
                $tipo_comprobante_id = TipoComprobante::FACTURA_C;
            }elseif($proveedor->condicion_iva_id == 2){//Proveedor RI
                if($condicion_iva_empresa == 2){//Yo RI
                    $tipo_comprobante_id = TipoComprobante::FACTURA_A;
                }else{
                    $tipo_comprobante_id = TipoComprobante::FACTURA_B;
                }
            }
            $results[] = [
                'id'                    => $proveedor->id,
                'value'                 => $proveedor->razon_social,
                'tipo_comprobante_id'   => $tipo_comprobante_id
            ];
        }
        return response()->json($results);
    }

    public function guardarNuevoProveedor(Request $request){
        $razon_social = $request->input('razon_social');
        $cuit = $request->input('cuit');

        $proveedor = New Proveedor();
        $proveedor->razon_social = $razon_social;
        $proveedor->cuit = $cuit;
        $proveedor->empresa_id = Auth::user()->empresa_id;
        $proveedor->save();

        $controllerHistorial = new HistorialController();
        $controllerHistorial->nuevoHistorialActualizacion($proveedor, $request);

        $resultado = $proveedor->id;

        return response()->json($resultado);
    }
}
