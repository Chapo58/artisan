<?php

namespace App\Http\Controllers\Frontend\Personas;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Frontend\Configuraciones\HistorialController;
use App\Models\Frontend\Caja\EstadoCaja;
use App\Models\Frontend\Configuraciones\CondicionIva;
use App\Models\Frontend\Configuraciones\Localidad;
use App\Models\Frontend\Configuraciones\Pais;
use App\Models\Frontend\Configuraciones\Provincia;
use App\Models\Frontend\Configuraciones\PuntoDeVenta;
use App\Models\Frontend\Configuraciones\TipoComprobante;
use App\Models\Frontend\Personas\Cliente;
use App\Models\Frontend\Personas\Domicilio;
use App\Models\Frontend\Ventas\CuentaCorriente;
use App\Models\Frontend\Ventas\ListaPrecio;
use App\Models\Frontend\Ventas\Venta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\Datatables\Facades\Datatables;
use Session;

class ClienteController extends Controller{

    public function index(){
        return view('frontend.personas.cliente.index');
    }

    public function datos(){
        $empresa = Auth::user()->empresa;
        $clientes = Cliente::select('clientes.id','condiciones_iva.nombre as condicioniva','clientes.razon_social','clientes.dni','clientes.email','clientes.telefono')
            ->leftjoin('condiciones_iva','condiciones_iva.id','clientes.condicion_iva_id')
            ->where('empresa_id','=', $empresa->id)
            ->get();

        $estadoCaja = EstadoCaja::CERRADA;
        if(isset(Auth::user()->empleado->caja)){
            if(Auth::user()->empleado->caja->estado == EstadoCaja::ABIERTA) {
                $estadoCaja = EstadoCaja::ABIERTA;
            }
        }
        return Datatables::of($clientes)
            ->addColumn('action', function ($cliente) use ($estadoCaja) {
                $return = '
                 <div class="btn-group">
                    <button type="button" class="btn btn-secondary dropdown-toggle btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Acciones
                    </button>
                    <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 33px, 0px); top: 0px; left: 0px; will-change: transform;">
                        <a class="dropdown-item" href="'.url('/personas/cliente/' . $cliente->id).'">
                            <i class="fa fa-eye" aria-hidden="true"></i> Ver Cliente
                        </a>
                        <a class="dropdown-item" href="'.url('/personas/cliente/' . $cliente->id . '/edit').'">
                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Modificar Cliente
                        </a>
                        <a class="dropdown-item" href="'.url('/ventas/cuenta-corriente/show-cliente/' . $cliente->id).'">
                            <i class="fa fa-id-card" aria-hidden="true"></i> Cuenta Corriente
                        </a>';
                if($estadoCaja == EstadoCaja::ABIERTA){
                    $return.='
                        <a class="dropdown-item" href="'.url('/ventas/recibo/crearReciboCliente/'.$cliente->id).'">
                            <i class="fa fa-plus" aria-hidden="true"></i> Recibo
                        </a>';
                } else {
                    $return.='
                        <a class="dropdown-item" href="#" disabled>
                            <i class="fa fa-plus" aria-hidden="true"></i> Recibo
                        </a>';
                }
                    $return.='
                        <a href="#" class="dropdown-item delete" data-id="'.$cliente->id.'" data-toggle="tooltip">
                            <i class="fa fa-close"></i> Eliminar Cliente
                        </a>
                    </div>
                </div>
                ';
                return $return;
            })
            ->editColumn('dni', function ($cliente) {
                if(strlen($cliente->dni) == 8){
                    return substr($cliente->dni, 0, 2).substr($cliente->dni, 2, 3).substr($cliente->dni, 5, 3);
                } else {
                    return substr($cliente->dni, 0, 2).'-'.substr($cliente->dni, 2, 8).'-'.substr($cliente->dni, 10, 1);
                }
            })
            ->setRowId('id')
            ->removeColumn('id')
            ->make(true);
    }

    public function create(){
        $empresa = Auth::user()->empresa;
        $condiciones_iva = CondicionIva::getCondicionesIvaArray();
        $lista_precios = ListaPrecio::getListasPorEmpresaArray($empresa);

        $paises = ['' => 'Seleccione una opción...'] + Pais::pluck('nombre', 'id')->all();
        $provincias = ['' => 'Seleccione una opción...'] + Provincia::orderBy('nombre')->pluck('nombre', 'id')->all();

        return view('frontend.personas.cliente.create', compact('condiciones_iva', 'lista_precios', 'paises', 'provincias'));
    }

    public function store(Request $request){
        $this->validate($request, [
			'razon_social' => 'required',
            'dni' => 'required',
            'domicilio.direccion' => 'required',
            'domicilio.pais_id' => 'required',
            'domicilio.provincia_id' => 'required',
            'domicilio.localidad' => 'required'
		]);

        DB::transaction(function() use ($request) {
            $requestData = $request->all();
            $cliente = New Cliente($requestData);

            $domicilio = New Domicilio($requestData['domicilio']);
            $domicilio->save();

            $cliente->domicilio_id = $domicilio->id;
            $cliente->empresa_id = Auth::user()->empresa_id;
            $cliente->save();

            $controllerHistorial = new HistorialController();
            $controllerHistorial->nuevoHistorialCreacion($cliente, $request);

            Session::flash('flash_success', 'Cliente creado correctamente.');
        });

        return redirect('personas/cliente');
    }

    public function show($id){
        $cliente = Cliente::findOrFail($id);
        $empresa = Auth::user()->empresa;

        if($cliente->empresa_id == $empresa->id){
            $ventas = Venta::where('cliente_id',$id)->get();
            $tipos_comprobantes = TipoComprobante::$comprobantes;
            $cuentasCorrientes = CuentaCorriente::listarCuentasCorrientesPorClienteEmpresa($cliente, $empresa);

            return view('frontend.personas.cliente.show', compact('cliente','ventas','tipos_comprobantes','cuentasCorrientes'));
        }else{
            return redirect('personas/cliente');
        }
    }

    public function edit($id){
        $cliente = Cliente::findOrFail($id);
        $empresa = Auth::user()->empresa;

        if($cliente->empresa_id == $empresa->id){
            if($cliente->razon_social != 'Consumidor Final'){
                $condiciones_iva = CondicionIva::getCondicionesIvaArray();
                $lista_precios = ListaPrecio::getListasPorEmpresaArray($empresa);
                $paises = ['' => 'Seleccione una opción...'] + Pais::pluck('nombre', 'id')->all();
                $provincias = ['' => 'Seleccione una opción...'] + Provincia::orderBy('nombre')->pluck('nombre', 'id')->all();

                return view('frontend.personas.cliente.edit', compact('cliente','condiciones_iva', 'lista_precios', 'paises', 'provincias'));
            }else{
                return redirect('personas/cliente');
            }
        }else{
            return redirect('personas/cliente');
        }
    }

    public function update($id, Request $request){
        $this->validate($request, [
            'razon_social' => 'required',
            'dni' => 'required',
            'domicilio.direccion' => 'required',
            'domicilio.pais_id' => 'required',
            'domicilio.provincia_id' => 'required',
            'domicilio.localidad' => 'required'
		]);
        $cliente = Cliente::findOrFail($id);

        if($cliente->empresa_id == Auth::user()->empresa_id) {
            if($cliente->razon_social != 'Consumidor Final'){
                DB::transaction(function() use ($cliente, $request) {
                    $requestData = $request->all();

                    if(isset($cliente->domicilio)){
                        $cliente->domicilio->update($requestData['domicilio']);
                    }else{
                        $domicilio = New Domicilio($requestData['domicilio']);
                        $domicilio->save();
                        $cliente->domicilio_id = $domicilio->id;
                    }
                    $cliente->update($requestData);

                    $controllerHistorial = new HistorialController();
                    $controllerHistorial->nuevoHistorialActualizacion($cliente, $request);

                    Session::flash('flash_success', 'Cliente modificado correctamente.');
                });
            }
        }

        return redirect('personas/cliente');
    }

    public function destroy($id){
        $cliente = Cliente::findOrFail($id);

        if($cliente->empresa_id == Auth::user()->empresa_id) {
            if($cliente->razon_social != 'Consumidor Final'){
                DB::transaction(function() use ($cliente) {
                    $cliente->delete();

                    $controllerHistorial = new HistorialController();
                    $controllerHistorial->nuevoHistorialEliminacion($cliente);

                    Session::flash('flash_success', 'Cliente eliminado correctamente.');
                });
            }
        }

        return redirect('personas/cliente');
    }

    public function obtenerClienteParaVenta(Request $request){
        $term = $request->term;

        $clientes = DB::table('clientes')
            ->where('empresa_id', Auth::user()->empresa_id)
            ->whereNull('deleted_at')
            ->where(function($q) use ($term) {
                $q->where('razon_social', 'LIKE', "%$term%")
                    ->orWhere('dni', 'LIKE', "%$term%");
            })
            ->paginate(3);

        foreach ($clientes as $cliente) {
            $results[] = [
                'id'                    => $cliente->id,
                'value'                 => $cliente->razon_social,
                'lista_precios_id'      => $cliente->lista_precios_id,
                'saldo_maximo'          => $cliente->saldo_maximo,
                'saldo_actual'          => $cliente->saldo_actual
            ];
        }
        return response()->json($results);
    }

    public function guardarNuevoCliente(Request $request){
        $razon_social = $request->input('razon_social');
        $dni = $request->input('dni');
        $telefono = $request->input('telefono');
        $condicion_iva_id = $request->input('condicion_iva_id');

        $cliente = New Cliente();
        $cliente->razon_social = $razon_social;
        $cliente->dni = $dni;
        $cliente->telefono = $telefono;
        $cliente->condicion_iva_id = $condicion_iva_id;
        $cliente->empresa_id = Auth::user()->empresa_id;
        $cliente->save();

        $controllerHistorial = new HistorialController();
        $controllerHistorial->nuevoHistorialActualizacion($cliente, $request);

        $resultado = $cliente->id;

        return response()->json($resultado);
    }

    public function plantillaClientes(){

        Excel::create('Clientes', function($excel) {

            // Set Excel Information
            $excel->setTitle('Clientes')
                ->setDescription('Lista de Clientes de la empresa')
                ->setCreator('Artisan')
                ->setCompany('Artisan');

            $excel->sheet('Clientes', function($sheet) {

                // Creo la primera fila con los titulos
                $sheet->row(1, array(
                    'Razon Social', 'DNI/CUIT', 'Condición IVA', 'Dirección', 'Localidad', 'Provincia', 'Telefono', 'Email'
                ));
                // Color de fondo, tamaño y bordes de la fila de titulos
                $sheet->row(1, function($row) {
                    $row->setBackground('#EBEBEB');
                    $row->setFontSize(16);
                    $row->setBorder('none', 'none', 'solid', 'none');
                });
                // Ancho de las columnas
                $sheet->setWidth(array(
                    'A'     =>  30, // Razon Social
                    'B'     =>  15, // CUIT
                    'C'     =>  20, // Condicion IVA
                    'D'     =>  25, // Direccion
                    'E'     =>  25, // Localidad
                    'F'     =>  15, // Provincia
                    'G'     =>  20, // Telefono
                    'H'     =>  30 // Email
                ));
                // Congelar fila de titulos para que sea siempre visible
                $sheet->freezeFirstRow();

            });

        })->download('xlsx');

    }

    public function exportarClientes(){

        Excel::create('Clientes', function($excel) {

            // Set Excel Information
            $excel->setTitle('Clientes')
                ->setDescription('Lista de Clientes de la empresa')
                ->setCreator('Artisan')
                ->setCompany('Artisan');

            $excel->sheet('Clientes', function($sheet) {

                $clientes = Cliente::where('empresa_id', Auth::user()->empresa_id)->get();

                $sheet->loadView('frontend.personas.cliente.exportar-clientes',
                    compact('clientes'));

                // Ancho de las columnas
                $sheet->setWidth(array(
                    'A'     =>  30, // Razon Social
                    'B'     =>  15, // CUIT
                    'C'     =>  20, // Condicion IVA
                    'D'     =>  25, // Direccion
                    'E'     =>  25, // Localidad
                    'F'     =>  15, // Provincia
                    'G'     =>  20, // Telefono
                    'H'     =>  30 // Email
                ));
                // Congelar fila de titulos para que sea siempre visible
                $sheet->freezeFirstRow();

            });

        })->download('xlsx');

    }

    public function importarClientes(Request $request){

        $empresa = Auth::user()->empresa;

        $this->validate($request, [
            'archivo' => 'required|max:10240|mimes:xlsx,xls'
        ]);

        // Almaceno el archivo excel en el storage en una carpeta con el nombre de la empresa y completo el path con la ruta
        $path = $request->file('archivo')->store(
            'importaciones/'.$empresa->razon_social
        );
        $path = 'storage/app/'.$path;

        $hayError = false;
        $mensaje = "Los clientes se han actualizado correctamente";

        // Cargo el excel
        Excel::load($path, function($reader) use (&$hayError,&$mensaje,$empresa) {

            // Tomo solamente las primeras 8 columnas del Excel
            $reader->takeColumns(8);

            // Pregunto si el archivo no esta vacio
            if($reader->first()){

                // Valido que los encabezados esten bien nombrados y ordenados
                $excelData = $reader->all();
                $headers = $excelData->first()->keys()->toArray();
                if (
                    $headers[0] == 'razon_social' &&
                    $headers[1] == 'dnicuit' &&
                    $headers[2] == 'condicion_iva' &&
                    $headers[3] == 'direccion' &&
                    $headers[4] == 'localidad' &&
                    $headers[5] == 'provincia' &&
                    $headers[6] == 'telefono' &&
                    $headers[7] == 'email'
                ){
                    $rows = $reader->all();
                    DB::transaction(function () use ($rows,&$hayError,&$mensaje,$empresa) {

                        foreach ($rows as $row) {

                            $row->dnicuit = (string)$row->dnicuit;
                            // Si el cliente no tiene un dni valido paso directamente al siguiente
                            if($row->dnicuit && strlen($row->dnicuit) >= 8) {

                                $cliente = Cliente::where('dni',$row->dnicuit)
                                    ->where('empresa_id',$empresa->id)
                                    ->first();

                                if($cliente){
                                    if($cliente->empresa_id == $empresa->id){
                                        $cliente->razon_social = ucfirst($row->razon_social);
                                        $cliente->email = $row->email;
                                        $cliente->telefono = $row->telefono;
                                        $cliente->dni = $row->dnicuit;
                                        $condicionIva = CondicionIva::where('nombre', $row->condicion_iva)->first();
                                        ($condicionIva) ? $cliente->condicion_iva_id = $condicionIva->id : $cliente->condicion_iva_id = NULL;

                                        if($cliente->domicilio){

                                            $domicilio = Domicilio::findOrFail($cliente->domicilio->id);
                                            $domicilio->direccion = ucwords($row->direccion);
                                            $provincia = Provincia::where('nombre', 'like', '%' . $row->provincia . '%')->first();
                                            if ($provincia) {
                                                $provincia_id = $domicilio->provincia_id = $provincia->id;
                                            } else {
                                                $provincia_id = $domicilio->provincia_id = 1;
                                            }
                                            $domicilio->localidad = $row->localidad;
                                            $domicilio->save();

                                        } else {

                                            $domicilio = New Domicilio();
                                            $provincia = Provincia::where('nombre', 'like', '%' . $row->provincia . '%')->first();
                                            if ($provincia) {
                                                $provincia_id = $domicilio->provincia_id = $provincia->id;
                                            } else {
                                                $provincia_id = $domicilio->provincia_id = 1;
                                            }
                                            $domicilio->localidad = $row->localidad;
                                            $domicilio->direccion = ucwords($row->direccion);
                                            $domicilio->pais_id = 1;
                                            $domicilio->save();

                                            $cliente->domicilio_id = $domicilio->id;

                                        }

                                        $cliente->save();
                                    }
                                } else {
                                    $cliente = New Cliente();
                                    $cliente->empresa_id = $empresa->id;
                                    $cliente->razon_social = ucfirst($row->razon_social);
                                    $cliente->dni = $row->dnicuit;
                                    $cliente->email = $row->email;
                                    $cliente->telefono = $row->telefono;
                                    $condicionIva = CondicionIva::where('nombre', $row->condicion_iva)->first();
                                    ($condicionIva) ? $cliente->condicion_iva_id = $condicionIva->id : $cliente->condicion_iva_id = NULL;

                                    $domicilio = New Domicilio();
                                    $provincia = Provincia::where('nombre', 'like', '%' . $row->provincia . '%')->first();
                                    if ($provincia) {
                                        $provincia_id = $domicilio->provincia_id = $provincia->id;
                                    } else {
                                        $provincia_id = $domicilio->provincia_id = 1;
                                    }
                                    $domicilio->localidad = $row->localidad;
                                    $domicilio->direccion = ucwords($row->direccion);
                                    $domicilio->pais_id = 1;
                                    $domicilio->save();

                                    $cliente->domicilio_id = $domicilio->id;
                                    $cliente->save();
                                }

                            }
                        }
                    });
                } else {
                    $hayError = true;
                    $mensaje = "Los encabezados del archivo son incorrectos";
                }
            } else {
                $hayError = true;
                $mensaje = "El archivo esta vacio";
            }

        });

        if($hayError){
            return redirect('personas/cliente')->withFlashDanger($mensaje);
        } else {
            return redirect('personas/cliente')->withFlashSuccess($mensaje);
        }

    }
}
