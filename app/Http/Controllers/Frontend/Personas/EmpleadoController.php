<?php

namespace App\Http\Controllers\Frontend\Personas;

use App\Http\Controllers\Controller;
use App\Models\Backend\Empresas\Sucursal;
use App\Models\Frontend\Caja\Caja;
use App\Models\Frontend\Configuraciones\Localidad;
use App\Models\Frontend\Configuraciones\Pais;
use App\Models\Frontend\Configuraciones\Provincia;
use App\Models\Frontend\Personas\Domicilio;
use App\Models\Frontend\Personas\Empleado;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;

class EmpleadoController extends Controller{

    public function index(Request $request){
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $empleado = Empleado::where('empresa_id', Auth::user()->empresa_id)
				->orWhere('nombre', 'LIKE', "%$keyword%")
				->orWhere('cuil', 'LIKE', "%$keyword%")

                ->paginate($perPage);
        } else {
            $empleado = Empleado::where('empresa_id', Auth::user()->empresa_id)
                ->paginate($perPage);
        }

        return view('frontend.personas.empleado.index', compact('empleado', 'keyword'));
    }

    public function create(){
        $paises = ['' => 'Seleccione una opción...'] + Pais::pluck('nombre', 'id')->all();
        $provincias = ['' => 'Seleccione una opción...'] + Provincia::pluck('nombre', 'id')->all();
        $sucursales = ['' => 'Seleccione una opción...'] + Sucursal::where('empresa_id', Auth::user()->empresa_id)->get()->pluck('nombre', 'id')->toArray();
        $cajas = ['' => 'Seleccione una opción...'] + Caja::where('empresa_id', Auth::user()->empresa_id)->get()->pluck('nombre', 'id')->toArray();

        return view('frontend.personas.empleado.create', compact('paises', 'provincias', 'sucursales', 'cajas'));
    }

    public function store(Request $request){
        $this->validate($request, [
			'nombre' => 'required',
            'cuil' => 'required',
            'domicilio.direccion' => 'required',
            'domicilio.pais_id' => 'required',
            'domicilio.provincia_id' => 'required',
            'domicilio.localidad' => 'required'
		]);
        $requestData = $request->all();

        $empleado = new Empleado($requestData);


        $domicilio = New Domicilio($requestData['domicilio']);
        $domicilio->save();

        $empleado->domicilio_id = $domicilio->id;
        $empleado->empresa_id = Auth::user()->empresa_id;
        $empleado->save();

        Session::flash('flash_message', trans('labels.frontend.empleado').trans('alerts.frontend.created').'!');

        return redirect('personas/empleado');
    }

    public function show($id){
        $empleado = Empleado::findOrFail($id);

        if($empleado->empresa_id == Auth::user()->empresa_id){
            return view('frontend.personas.empleado.show', compact('empleado'));
        }else{
            return redirect('personas/empleado');
        }
    }

    public function edit($id){
        $empleado = Empleado::findOrFail($id);

        if($empleado->empresa_id == Auth::user()->empresa_id){
            $paises = ['' => 'Seleccione una opción...'] + Pais::pluck('nombre', 'id')->all();
            $provincias = ['' => 'Seleccione una opción...'] + Provincia::pluck('nombre', 'id')->all();
            $sucursales = ['' => 'Seleccione una opción...'] + Sucursal::where('empresa_id', Auth::user()->empresa_id)->get()->pluck('nombre', 'id')->toArray();
            $cajas = ['' => 'Seleccione una opción...'] + Caja::where('empresa_id', Auth::user()->empresa_id)->get()->pluck('nombre', 'id')->toArray();

            return view('frontend.personas.empleado.edit', compact('empleado', 'sucursales', 'paises', 'provincias', 'cajas'));
        }else{
            return redirect('personas/empleado');
        }
    }

    public function update($id, Request $request){
        $this->validate($request, [
			'nombre' => 'required',
            'cuil' => 'required',
            'domicilio.direccion' => 'required',
            'domicilio.pais_id' => 'required',
            'domicilio.provincia_id' => 'required',
            'domicilio.localidad' => 'required'
		]);
        $requestData = $request->all();
        
        $empleado = Empleado::findOrFail($id);

        if($empleado->empresa_id == Auth::user()->empresa_id) {
            $empleado->domicilio->update($requestData['domicilio']);
            $empleado->update($requestData);

            Session::flash('flash_message', trans('labels.frontend.empleado').trans('alerts.frontend.updated').'!');
        }

        return redirect('personas/empleado');
    }

    public function destroy($id){
        $empleado = Empleado::findOrFail($id);

        if($empleado->empresa_id == Auth::user()->empresa_id) {
            $empleado->delete();

            Session::flash('flash_message',  trans('labels.frontend.empleado').trans('alerts.frontend.deleted').'!');
        }

        return redirect('personas/empleado');
    }
}
