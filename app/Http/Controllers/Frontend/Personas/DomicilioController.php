<?php

namespace App\Http\Controllers\Frontend\Personas;

use App\Http\Controllers\Controller;
use App\Models\Frontend\Personas\Domicilio;
use Illuminate\Http\Request;
use Session;

class DomicilioController extends Controller
{

    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $domicilio = Domicilio::where('direccion', 'LIKE', "%$keyword%")
				->orWhere('codigo_postal', 'LIKE', "%$keyword%")
				->orWhere('pais_id', 'LIKE', "%$keyword%")
				->orWhere('provincia_id', 'LIKE', "%$keyword%")
				->orWhere('localidad', 'LIKE', "%$keyword%")
				->orWhere('telefono', 'LIKE', "%$keyword%")
				->orWhere('telefono2', 'LIKE', "%$keyword%")
				
                ->paginate($perPage);
        } else {
            $domicilio = Domicilio::paginate($perPage);
        }

        return view('frontend.personas.domicilio.index', compact('domicilio'));
    }

    public function create()
    {
        return view('frontend.personas.domicilio.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
			'direccion' => 'required',
			'localidad_id' => 'required'
		]);
        $requestData = $request->all();
        
        Domicilio::create($requestData);

        Session::flash('flash_message', trans('labels.frontend.domicilio').trans('alerts.frontend.created').'!');

        return redirect('personas/domicilio');
    }

    public function show($id)
    {
        $domicilio = Domicilio::findOrFail($id);

        return view('frontend.personas.domicilio.show', compact('domicilio'));
    }

    public function edit($id)
    {
        $domicilio = Domicilio::findOrFail($id);

        return view('frontend.personas.domicilio.edit', compact('domicilio'));
    }

    public function update($id, Request $request)
    {
        $this->validate($request, [
			'direccion' => 'required',
			'localidad_id' => 'required'
		]);
        $requestData = $request->all();
        
        $domicilio = Domicilio::findOrFail($id);
        $domicilio->update($requestData);

        Session::flash('flash_message', trans('labels.frontend.domicilio').trans('alerts.frontend.updated').'!');

        return redirect('personas/domicilio');
    }

    public function destroy($id)
    {
        Domicilio::destroy($id);

        Session::flash('flash_message',  trans('labels.frontend.domicilio').trans('alerts.frontend.deleted').'!');

        return redirect('personas/domicilio');
    }
}
