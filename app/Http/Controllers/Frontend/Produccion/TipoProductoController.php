<?php

namespace App\Http\Controllers\Frontend\Produccion;

use App\Http\Controllers\Controller;
use App\Models\Frontend\Produccion\TipoCorte;
use App\Models\Frontend\Produccion\TipoProducto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;

class TipoProductoController extends Controller{

    public function index(Request $request){
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $tipoproducto = TipoProducto::where('nombre', 'LIKE', "%$keyword%")
                ->paginate($perPage);
        } else {
            $tipoproducto = TipoProducto::paginate($perPage);
        }

        return view('frontend.produccion.tipo-producto.index', compact('tipoproducto', 'keyword'));
    }

    public function create(){
        $empresa = Auth::user()->empresa;
        $tipos_cortes = TipoCorte::getTiposCortesPorEmpresaArray($empresa);

        return view('frontend.produccion.tipo-producto.create', compact('tipos_cortes'));
    }

    public function store(Request $request){
        $this->validate($request, [
			'nombre' => 'required',
			'tipo_corte_id' => 'required'
		]);
        $requestData = $request->all();

        $nuevoTipoProducto = new TipoProducto($requestData);
        $nuevoTipoProducto->empresa_id = Auth::user()->empresa_id;
        $nuevoTipoProducto->save();

        Session::flash('flash_message', trans('labels.frontend.tipoproducto').trans('alerts.frontend.created').'!');

        return redirect('produccion/tipo-producto');
    }

    public function show($id){
        $tipoproducto = TipoProducto::findOrFail($id);

        return view('frontend.produccion.tipo-producto.show', compact('tipoproducto'));
    }

    public function edit($id){
        $tipoproducto = TipoProducto::findOrFail($id);
        $empresa = Auth::user()->empresa;
        $tipos_cortes = TipoCorte::getTiposCortesPorEmpresaArray($empresa);

        return view('frontend.produccion.tipo-producto.edit', compact('tipoproducto','tipos_cortes'));
    }

    public function update($id, Request $request){
        $this->validate($request, [
			'nombre' => 'required',
			'tipo_corte_id' => 'required'
		]);
        $requestData = $request->all();
        
        $tipoproducto = TipoProducto::findOrFail($id);
        $tipoproducto->update($requestData);

        Session::flash('flash_message', trans('labels.frontend.tipoproducto').trans('alerts.frontend.updated').'!');

        return redirect('produccion/tipo-producto');
    }

    public function destroy($id){
        TipoProducto::destroy($id);

        Session::flash('flash_message',  trans('labels.frontend.tipoproducto').trans('alerts.frontend.deleted').'!');

        return redirect('produccion/tipo-producto');
    }
}
