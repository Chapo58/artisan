<?php

namespace App\Http\Controllers\Frontend\Produccion;

use App\Http\Controllers\Controller;
use App\Models\Frontend\Produccion\OrdenPedido;
use App\Models\Frontend\Produccion\TipoCorte;
use App\Models\Frontend\Produccion\TipoTela;
use App\Models\Frontend\Produccion\Color;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;

class OrdenPedidoController extends Controller{

    public function index(Request $request){
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $ordenpedido = OrdenPedido::where('numero_opm', 'LIKE', "%$keyword%")
				->orWhere('observaciones', 'LIKE', "%$keyword%")
				->orWhere('total_unidades', 'LIKE', "%$keyword%")
                ->paginate($perPage);
        } else {
            $ordenpedido = OrdenPedido::paginate($perPage);
        }

        return view('frontend.produccion.orden-pedido.index', compact('ordenpedido', 'keyword'));
    }

    public function create(){
        $empresa = Auth::user()->empresa;

        $tipos_telas = TipoTela::getTiposPorEmpresaArray($empresa);
        $colores = Color::getColoresPorEmpresaArray($empresa);
        $tipos_cortes = TipoCorte::getTiposCortesPorEmpresaArray($empresa);

        return view('frontend.produccion.orden-pedido.create', compact('tipos_telas', 'colores','tipos_cortes'));
    }

    public function store(Request $request){
        $this->validate($request, [
			'cliente_id' => 'required',
			'tipo_corte_id' => 'required',
			'tipo_tela_id' => 'required',
			'color_id' => 'required',
			'total_unidades' => 'required'
		]);
        $requestData = $request->all();
        
        OrdenPedido::create($requestData);

        Session::flash('flash_message', trans('labels.frontend.ordenpedido').trans('alerts.frontend.created').'!');

        return redirect('produccion/orden-pedido');
    }

    public function show($id){
        $ordenpedido = OrdenPedido::findOrFail($id);

        return view('frontend.produccion.orden-pedido.show', compact('ordenpedido'));
    }

    public function edit($id){
        $ordenpedido = OrdenPedido::findOrFail($id);

        return view('frontend.produccion.orden-pedido.edit', compact('ordenpedido'));
    }

    public function update($id, Request $request){
        $this->validate($request, [
			'cliente_id' => 'required',
			'tipo_corte_id' => 'required',
			'tipo_tela_id' => 'required',
			'color_id' => 'required',
			'total_unidades' => 'required'
		]);
        $requestData = $request->all();
        
        $ordenpedido = OrdenPedido::findOrFail($id);
        $ordenpedido->update($requestData);

        Session::flash('flash_message', trans('labels.frontend.ordenpedido').trans('alerts.frontend.updated').'!');

        return redirect('produccion/orden-pedido');
    }

    public function destroy($id){
        OrdenPedido::destroy($id);

        Session::flash('flash_message',  trans('labels.frontend.ordenpedido').trans('alerts.frontend.deleted').'!');

        return redirect('produccion/orden-pedido');
    }
}
