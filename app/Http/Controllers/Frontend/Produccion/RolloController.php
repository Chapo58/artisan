<?php

namespace App\Http\Controllers\Frontend\Produccion;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Frontend\Configuraciones\HistorialController;
use App\Models\Frontend\Compras\Compra;
use App\Models\Frontend\Configuraciones\Estado;
use App\Models\Frontend\Configuraciones\Opcion;
use App\Models\Frontend\Configuraciones\PorcentajeIva;
use App\Models\Frontend\Produccion\EstadoRollo;
use App\Models\Frontend\Produccion\Rollo;
use App\Models\Frontend\Produccion\TipoTela;
use App\Models\Frontend\Produccion\Color;
use App\Models\Frontend\Configuraciones\Moneda;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\HtmlString;
use Yajra\Datatables\Facades\Datatables;
use Session;

class RolloController extends Controller{

    public function index(){
        return view('frontend.produccion.rollo.index');
    }

    public function datos(){
        $empresa = Auth::user()->empresa;
        $estados = EstadoRollo::$getArray;

        $query = Rollo::select('rollos.id','rollos.codigo','rollos.partida','rollos.peso','rollos.tintoreria','rollos.costo','tipos_telas.nombre as tipo_tela','colores.nombre as color','rollos.estado');
        $query->leftjoin('tipos_telas', 'tipos_telas.id', '=', 'rollos.tipo_tela_id');
        $query->leftjoin('colores', 'colores.id', '=', 'rollos.color_id');
        $query->where('rollos.empresa_id', $empresa->id);
        $query->whereNull('rollos.deleted_at');
        $rollos = $query->get();

        return Datatables::of($rollos)
            ->addColumn('action', function ($rollo) {
                return '
                <a href="'.url('/produccion/rollo/' . $rollo->id).'" class="mytooltip"><i class="fa fa-eye text-success m-r-10"></i>
                    <span class="tooltip-content3">Ver Rollo</span>
                </a>
                <a href="'.url('/produccion/rollo/' . $rollo->id . '/edit').'" class="mytooltip">
                    <i class="fa fa-pencil text-info m-r-10"></i>
                    <span class="tooltip-content3">Modificar Rollo</span>
                </a>
                <a class="delete mytooltip pointer" data-id="'.$rollo->id.'">
                    <i class="fa fa-close text-danger"></i>
                    <span class="tooltip-content3">Eliminar Rollo</span>
                </a>
                ';
            })
            ->setRowId('id')
            ->removeColumn('id')
            ->editColumn('estado', function ($rollo) use ($estados) {
                switch ($rollo->estado) {
                    case EstadoRollo::DISPONIBLE:
                        return new HtmlString('<span class="label label-success">'.$estados[$rollo->estado].'</span>');
                        break;
                    case EstadoRollo::RESTO:
                        return new HtmlString('<span class="label label-warning">'.$estados[$rollo->estado].'</span>');
                        break;
                    case EstadoRollo::FINALIZADO:
                        return new HtmlString('<span class="label label-danger">'.$estados[$rollo->estado].'</span>');
                        break;
                    default:
                        return new HtmlString('<span class="label label-info">'.$estados[$rollo->estado].'</span>');
                }
            })
            ->make(true);
    }

    public function create(){
        $empresa = Auth::user()->empresa;

        $estados = EstadoRollo::$getArray;
        $monedas = Moneda::getMonedasPorEmpresaArray($empresa);
        $tipos_telas = TipoTela::getTiposPorEmpresaArray($empresa);
        $colores = Color::getColoresPorEmpresaArray($empresa);
        $porcentajes_iva = PorcentajeIva::$arrayCombo;

        $valor_numeracion_rollo = (int)Opcion::getValorOpcion('numeracion_rollo', $empresa);

        return view('frontend.produccion.rollo.create', compact('monedas','estados', 'tipos_telas', 'colores', 'porcentajes_iva', 'valor_numeracion_rollo'));
    }

    public function store(Request $request){
        $this->validate($request, [
    			'tipo_tela_id' => 'required',
    			'color_id' => 'required',
    			'codigo' => 'required',
    			'porcentaje_iva' => 'required'
    		]);

        DB::transaction(function() use ($request) {
            $requestData = $request->all();
            $empresa = Auth::user()->empresa;

            $nuevoRollo = new Rollo($requestData);
            $nuevoRollo->empresa_id = $empresa->id;
            $nuevoRollo->save();

            $ultimo_codigo = str_pad($nuevoRollo->codigo + 1, 8, '0', STR_PAD_LEFT);
            Opcion::setValorOpcion('numeracion_rollo', $ultimo_codigo, $empresa);

            $controllerHistorial = new HistorialController();
            $controllerHistorial->nuevoHistorialCreacion($nuevoRollo, $request);

            Session::flash('flash_message', trans('labels.frontend.rollo').trans('alerts.frontend.created').'!');
        });

        return redirect('produccion/rollo');
    }

    public function show($id){
        $empresa = Auth::user()->empresa;
        $rollo = Rollo::findOrFail($id);

        if($rollo->empresa == $empresa) {
            $estados = EstadoRollo::$getArray;
            return view('frontend.produccion.rollo.show', compact('rollo', 'estados'));
        }

        return redirect('produccion/rollo');
    }

    public function edit($id){
        $empresa = Auth::user()->empresa;
        $rollo = Rollo::findOrFail($id);

        if($rollo->empresa == $empresa) {
            $estados = EstadoRollo::$getArray;
            $monedas = Moneda::getMonedasPorEmpresaArray($empresa);
            $tipos_telas = TipoTela::getTiposPorEmpresaArray($empresa);
            $colores = Color::getColoresPorEmpresaArray($empresa);
            $porcentajes_iva = PorcentajeIva::$arrayCombo;

            $valor_numeracion_rollo = $rollo->codigo;

            return view('frontend.produccion.rollo.edit', compact('rollo','monedas', 'estados', 'colores', 'tipos_telas', 'porcentajes_iva', 'valor_numeracion_rollo'));
        }

        return redirect('ventas/cupon-tarjeta');
    }

    public function update($id, Request $request){
        $this->validate($request, [
    			'tipo_tela_id' => 'required',
    			'color_id' => 'required',
    			'codigo' => 'required',
                'porcentaje_iva' => 'required'
    		]);
        $rollo = Rollo::findOrFail($id);

        DB::transaction(function() use ($rollo, $request) {
            $requestData = $request->all();

            $rollo->update($requestData);

            $controllerHistorial = new HistorialController();
            $controllerHistorial->nuevoHistorialActualizacion($rollo, $request);

            Session::flash('flash_message', trans('labels.frontend.rollo') . trans('alerts.frontend.updated') . '!');
        });

        return redirect('produccion/rollo');
    }

    public function destroy($id){
        $empresa = Auth::user()->empresa;
        $rollo = Rollo::findOrFail($id);

        if($rollo->empresa == $empresa) {
            DB::transaction(function() use ($rollo) {
                $rollo->delete();

                $controllerHistorial = new HistorialController();
                $controllerHistorial->nuevoHistorialEliminacion($rollo);

                Session::flash('flash_message',  trans('labels.frontend.rollo').trans('alerts.frontend.deleted').'!');
            });
        }
        return redirect('produccion/rollo');
    }

    public function ingresoMasivoDeRollos(){
        $empresa = Auth::user()->empresa;

        $compras_activas = Compra::getComprasPorEmpresaArray($empresa);
        $colores = Color::getColoresPorEmpresaArray($empresa);
        $tipos_telas = TipoTela::getTiposPorEmpresaArray($empresa);
        $monedas = Moneda::getMonedasPorEmpresaArray($empresa);
        $porcentajes_iva = PorcentajeIva::$arrayCombo;
        $valor_numeracion_rollo = Opcion::getValorOpcion('numeracion_rollo', $empresa);

        return view('frontend.produccion.rollo.ingreso_masivo', compact('compras_activas','colores', 'tipos_telas', 'monedas', 'porcentajes_iva', 'valor_numeracion_rollo'));
    }

    public function guardarIngresoMasivo(Request $request){
        $this->validate($request, [
            'compra_id' => 'required',
            'moneda_id' => 'required',
        ]);

        DB::transaction(function() use ($request) {
            $requestData = $request->all();
            $empresa = Auth::user()->empresa;
            $ultimo_codigo = (int)$requestData['numeracion-rollo'];

            if (isset($requestData['detalles'])) {
                $detalles = $requestData['detalles'];
                foreach ($detalles as $detalle) {
                    if ((int)$detalle['estado'] == Estado::ACTIVO && isset($detalle['tipo_tela_id']) &&
                        isset($detalle['color_id']) && isset($detalle['codigo']) && isset($detalle['porcentaje_iva'])) {
                        $nuevoRollo = new Rollo($detalle);
                        $nuevoRollo->empresa_id = $empresa->id;
                        $nuevoRollo->moneda_id = $requestData['moneda_id'];
                        $nuevoRollo->save();

                        $controllerHistorial = new HistorialController();
                        $controllerHistorial->nuevoHistorialCreacion($nuevoRollo, $request);

                        if((int)$detalle['codigo'] > $ultimo_codigo){
                            $ultimo_codigo = (int)$detalle['codigo'];
                        }
                    }
                }
            }
            $ultimo_codigo = str_pad($ultimo_codigo + 1, 8, '0', STR_PAD_LEFT);
            Opcion::setValorOpcion('numeracion_rollo', $ultimo_codigo, $empresa);

            $compra = Compra::find($requestData['compra_id']);
            $compra->estado = Estado::FINALIZADO;
            $compra->update();

            Session::flash('flash_message', 'Los Rollos han sido creados!');
        });

        return redirect('produccion/rollo');
    }

    public function obtenerRollo(Request $request){
        $term = $request->term;
        $empresa = Auth::user()->empresa;

        $rollos = Rollo::where('rollos.empresa_id', $empresa->id)
            ->join('colores', 'colores.id', '=', 'rollos.color_id')
            ->join('tipos_telas', 'tipos_telas.id', '=', 'rollos.tipo_tela_id')
            ->where('rollos.estado','<>',EstadoRollo::FINALIZADO)
            ->whereNull('rollos.deleted_at')
            ->where(function($q) use ($term)  {
                $q->where('rollos.codigo', 'LIKE', "%$term%")
                    ->orWhere('colores.nombre', 'LIKE', "%$term%")
                    ->orWhere('tipos_telas.nombre', 'LIKE', "%$term%");
                })
            ->paginate(5);


        foreach ($rollos as $rollo)
        {
            $results[] = [
                'id' => $rollo->id,
                'value' => "(" . $rollo->codigo .") ". $rollo->color. " - ". $rollo->tipoTela. " - ". $rollo->peso. " kg.",
                'codigo' => $rollo->codigo,
                'color' => $rollo->color->nombre,
                'tela' => $rollo->tipoTela->nombre,
                'peso' => $rollo->peso,
            ];
        }
        return response()->json($results);
    }
}
