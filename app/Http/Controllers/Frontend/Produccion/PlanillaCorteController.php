<?php

namespace App\Http\Controllers\Frontend\Produccion;

use App\Http\Controllers\Controller;
use App\Models\Frontend\Configuraciones\Estado;
use App\Models\Frontend\Produccion\DetallePlanillaCorte;
use App\Models\Frontend\Produccion\EstadoRollo;
use App\Models\Frontend\Produccion\PlanillaCorte;
use App\Models\Frontend\Produccion\Rollo;
use App\Models\Frontend\Produccion\Talleres;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Session;
use DateTime;

class PlanillaCorteController extends Controller{

    public function index(Request $request){
        $keyword = $request->get('search');
        $perPage = 25;
        $empresa = Auth::user()->empresa;

        $planillas = PlanillaCorte::listarPlanillasPorEmpresa($empresa, $perPage, $keyword);

        return view('frontend.produccion.planilla-corte.index', compact('planillas', 'keyword'));
    }

    public function create(){
        $fecha_hoy = date('d/m/Y');

        return view('frontend.produccion.planilla-corte.create', compact('fecha_hoy'));
    }

    public function store(Request $request){
        $this->validate($request, [
            'numero' => 'required',
            'fecha' => 'required'
        ]);

        DB::transaction(function() use ($request) {
            $requestData = $request->all();

            $empresa = Auth::user()->empresa;

            $planilla = New PlanillaCorte($requestData);
            $date = DateTime::createFromFormat('d/m/Y', $requestData['fecha']);
            $planilla->fecha = $date->format('Y-m-d H:i:s');
            $planilla->empresa_id = $empresa->id;
            $planilla->save();

            if(isset($requestData['detalles'])) {
                $detalles = $requestData['detalles'];
                foreach ($detalles as $detalle) {
                    if ((int)$detalle['estado'] == Estado::ACTIVO && isset($detalle['rollo_id'])) {
                        $nuevoDetalle = New DetallePlanillaCorte($detalle);
                        $nuevoDetalle->planillas_corte_id = $planilla->id;
                        $nuevoDetalle->save();

                        $rollo = Rollo::findOrFail($nuevoDetalle->rollo_id);
                        if($nuevoDetalle->restos == 0 || $nuevoDetalle->restos == null){
                            $rollo->peso = 0;
                            $rollo->estado = EstadoRollo::FINALIZADO;
                        }else{
                            $rollo->peso = $nuevoDetalle->restos;
                            $rollo->estado = EstadoRollo::RESTO;
                        }
                        $rollo->update();
                    }
                }
            }

            Session::flash('flash_message', trans('labels.frontend.planillacorte').trans('alerts.frontend.created').'!');
        });

        return redirect('produccion/planilla-corte');
    }

    public function show($id){
        $planillaCorte = PlanillaCorte::findOrFail($id);
        $empresa = Auth::user()->empresa;

        if($planillaCorte->empresaSistema == $empresa) {
            return view('frontend.produccion.planilla-corte.show', compact('planillaCorte'));
        }
        return redirect('produccion/planilla-corte');
    }

    public function edit($id){
        $planillaCorte = PlanillaCorte::findOrFail($id);
        $empresa = Auth::user()->empresa;

        if($planillaCorte->empresaSistema == $empresa) {
            return view('frontend.produccion.planilla-corte.edit', compact('planillaCorte'));
        }
        return redirect('produccion/planilla-corte');
    }

    public function update($id, Request $request){
        $this->validate($request, [
            'numero' => 'required',
            'fecha' => 'required'
        ]);
        $planillaCorte = PlanillaCorte::findOrFail($id);
        $empresa = Auth::user()->empresa;

        if($planillaCorte->empresaSistema == $empresa) {
            DB::transaction(function() use ($planillaCorte, $empresa, $request) {
                $requestData = $request->all();

                $planillaCorte->update($requestData);

                if(isset($requestData['detalles'])) {
                    $detalles = $requestData['detalles'];
                    foreach ($detalles as $detalle) {
                        if ((int)$detalle['id'] < 0 && (int)$detalle['estado'] == Estado::ACTIVO && isset($detalle['rollo_id'])) {
                            $nuevoDetalle = New DetallePlanillaCorte($detalle);
                            $nuevoDetalle->planillas_corte_id = $planillaCorte->id;
                            $nuevoDetalle->save();

                            $rollo = Rollo::findOrFail($nuevoDetalle->rollo_id);
                            if($nuevoDetalle->restos == 0 || $nuevoDetalle->restos == null){
                                $rollo->peso = 0;
                                $rollo->estado = EstadoRollo::FINALIZADO;
                            }else{
                                $rollo->peso = $nuevoDetalle->restos;
                                $rollo->estado = EstadoRollo::RESTO;
                            }
                            $rollo->update();
                        }
                    }
                }

                Session::flash('flash_message', trans('labels.frontend.planillacorte').trans('alerts.frontend.updated').'!');
            });
        }

        return redirect('produccion/planilla-corte');

    }

    public function destroy($id){
        PlanillaCorte::destroy($id);

        Session::flash('flash_message',  trans('labels.frontend.planillacorte').trans('alerts.frontend.deleted').'!');

        return redirect('produccion/planilla-corte');
    }

    public function asignarTaller($id){
        $empresa = Auth::user()->empresa;

        $talleres = Talleres::getTalleresPorEmpresaArray($empresa);

        return view('frontend.produccion.planilla-corte.asignacion', compact('talleres'));
    }
}
