<?php

namespace App\Http\Controllers\Frontend\Produccion;

use App\Http\Controllers\Controller;
use App\Models\Frontend\Produccion\TipoTela;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;

class TipoTelaController extends Controller{

    public function index(Request $request){
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $tipotela = TipoTela::where('nombre', 'LIKE', "%$keyword%")
                                ->paginate($perPage);
        } else {
            $tipotela = TipoTela::paginate($perPage);
        }

        return view('frontend.produccion.tipo-tela.index', compact('tipotela'));
    }

    public function create(){
        return view('frontend.produccion.tipo-tela.create');
    }

    public function store(Request $request){
        $this->validate($request, [
    			'nombre' => 'required'
    		]);
        $requestData = $request->all();

        $nuevoTipoTela = new TipoTela($requestData);
        $nuevoTipoTela->empresa_id = Auth::user()->empresa_id;
        $nuevoTipoTela->save();

        Session::flash('flash_message', trans('labels.frontend.tipotela').trans('alerts.frontend.created').'!');

        return redirect('produccion/tipo-tela');
    }

    public function show($id){
        $tipotela = TipoTela::findOrFail($id);

        return view('frontend.produccion.tipo-tela.show', compact('tipotela'));
    }

    public function edit($id){
        $tipotela = TipoTela::findOrFail($id);

        return view('frontend.produccion.tipo-tela.edit', compact('tipotela'));
    }

    public function update($id, Request $request){
        $this->validate($request, [
    			'nombre' => 'required'
    		]);
        $requestData = $request->all();

        $tipotela = TipoTela::findOrFail($id);
        $tipotela->update($requestData);

        Session::flash('flash_message', trans('labels.frontend.tipotela').trans('alerts.frontend.updated').'!');

        return redirect('produccion/tipo-tela');
    }

    public function destroy($id){
        TipoTela::destroy($id);

        Session::flash('flash_message',  trans('labels.frontend.tipotela').trans('alerts.frontend.deleted').'!');

        return redirect('produccion/tipo-tela');
    }
}
