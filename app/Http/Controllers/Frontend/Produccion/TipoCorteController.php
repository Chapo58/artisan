<?php

namespace App\Http\Controllers\Frontend\Produccion;

use App\Http\Controllers\Controller;
use App\Models\Frontend\Produccion\TipoCorte;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;

class TipoCorteController extends Controller{

    public function index(Request $request){
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $tipocorte = TipoCorte::where('nombre', 'LIKE', "%$keyword%")
                ->paginate($perPage);
        } else {
            $tipocorte = TipoCorte::paginate($perPage);
        }

        return view('frontend.produccion.tipo-corte.index', compact('tipocorte', 'keyword'));
    }

    public function create(){
        return view('frontend.produccion.tipo-corte.create');
    }

    public function store(Request $request){
        $this->validate($request, [
			'nombre' => 'required'
		]);
        $requestData = $request->all();

        $nuevoTipoCorte = new TipoCorte($requestData);
        $nuevoTipoCorte->empresa_id = Auth::user()->empresa_id;
        $nuevoTipoCorte->save();

        Session::flash('flash_message', trans('labels.frontend.tipocorte').trans('alerts.frontend.created').'!');

        return redirect('produccion/tipo-corte');
    }

    public function show($id){
        $tipocorte = TipoCorte::findOrFail($id);

        return view('frontend.produccion.tipo-corte.show', compact('tipocorte'));
    }

    public function edit($id){
        $tipocorte = TipoCorte::findOrFail($id);

        return view('frontend.produccion.tipo-corte.edit', compact('tipocorte'));
    }

    public function update($id, Request $request){
        $this->validate($request, [
			'nombre' => 'required'
		]);
        $requestData = $request->all();
        
        $tipocorte = TipoCorte::findOrFail($id);
        $tipocorte->update($requestData);

        Session::flash('flash_message', trans('labels.frontend.tipocorte').trans('alerts.frontend.updated').'!');

        return redirect('produccion/tipo-corte');
    }

    public function destroy($id){
        TipoCorte::destroy($id);

        Session::flash('flash_message',  trans('labels.frontend.tipocorte').trans('alerts.frontend.deleted').'!');

        return redirect('produccion/tipo-corte');
    }
}
