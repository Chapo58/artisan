<?php

namespace App\Http\Controllers\Frontend\Produccion;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Frontend\Configuraciones\HistorialController;
use App\Models\Frontend\Configuraciones\CondicionIva;
use App\Models\Frontend\Configuraciones\Localidad;
use App\Models\Frontend\Configuraciones\Pais;
use App\Models\Frontend\Configuraciones\Provincia;
use App\Models\Frontend\Personas\Domicilio;
use App\Models\Frontend\Produccion\Talleres;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Session;

class TalleresController extends Controller{

    public function index(Request $request){
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $talleres = Talleres::where('razon_social', 'LIKE', "%$keyword%")
				->orWhere('cuit', 'LIKE', "%$keyword%")
				->orWhere('email', 'LIKE', "%$keyword%")
				->orWhere('telefono', 'LIKE', "%$keyword%")
                ->paginate($perPage);
        } else {
            $talleres = Talleres::paginate($perPage);
        }

        return view('frontend.produccion.talleres.index', compact('talleres', 'keyword'));
    }

    public function create(){
        $condiciones_iva = ['' => 'Seleccione una opción...'] + CondicionIva::pluck('nombre', 'id')->all();
        $paises = ['' => 'Seleccione una opción...'] + Pais::pluck('nombre', 'id')->all();
        $provincias = ['' => 'Seleccione una opción...'] + Provincia::orderBy('nombre')->pluck('nombre', 'id')->all();
        $localidades = ['' => 'Seleccione una opción...'] + Localidad::orderBy('nombre')->pluck('nombre', 'id')->all();

        return view('frontend.produccion.talleres.create', compact('condiciones_iva', 'paises', 'provincias', 'localidades'));
    }

    public function store(Request $request){
        $this->validate($request, [
			'condicion_iva_id' => 'required',
			'razon_social' => 'required',
			'cuit' => 'required'
		]);

        DB::transaction(function() use ($request) {
            $requestData = $request->all();
            $taller = New Talleres($requestData);

            $domicilio = New Domicilio($requestData['domicilio']);
            $domicilio->save();

            $taller->domicilio_id = $domicilio->id;
            $taller->empresa_id = Auth::user()->empresa_id;
            $taller->save();

            $controllerHistorial = new HistorialController();
            $controllerHistorial->nuevoHistorialCreacion($taller, $request);

            Session::flash('flash_message', trans('labels.frontend.talleres').trans('alerts.frontend.created').'!');
        });

        return redirect('produccion/talleres');
    }

    public function show($id){
        $taller = Talleres::findOrFail($id);

        return view('frontend.produccion.talleres.show', compact('taller'));
    }

    public function edit($id){
        $taller = Talleres::findOrFail($id);
        $condiciones_iva = ['' => 'Seleccione una opción...'] + CondicionIva::pluck('nombre', 'id')->all();
        $paises = ['' => 'Seleccione una opción...'] + Pais::pluck('nombre', 'id')->all();
        $provincias = ['' => 'Seleccione una opción...'] + Provincia::orderBy('nombre')->pluck('nombre', 'id')->all();
        $localidades = ['' => 'Seleccione una opción...'] + Localidad::orderBy('nombre')->pluck('nombre', 'id')->all();

        return view('frontend.produccion.talleres.edit', compact('taller','condiciones_iva', 'paises', 'provincias', 'localidades'));
    }

    public function update($id, Request $request){
        $this->validate($request, [
			'condicion_iva_id' => 'required',
			'razon_social' => 'required',
			'cuit' => 'required'
		]);
        $requestData = $request->all();
        
        $taller = Talleres::findOrFail($id);

        DB::transaction(function() use ($taller, $request) {
            $requestData = $request->all();

            if(isset($proveedor->domicilio)){
                $taller->domicilio->update($requestData['domicilio']);
            }else{
                $domicilio = New Domicilio($requestData['domicilio']);
                $domicilio->save();
                $taller->domicilio_id = $domicilio->id;
            }
            $taller->domicilio->update($requestData['domicilio']);
            $taller->update($requestData);

            $controllerHistorial = new HistorialController();
            $controllerHistorial->nuevoHistorialActualizacion($taller, $request);

            Session::flash('flash_message', trans('labels.frontend.talleres') . trans('alerts.frontend.updated') . '!');
        });

        return redirect('produccion/talleres');
    }

    public function destroy($id){
        Talleres::destroy($id);

        Session::flash('flash_message',  trans('labels.frontend.talleres').trans('alerts.frontend.deleted').'!');

        return redirect('produccion/talleres');
    }
}
