<?php

namespace App\Http\Controllers\Frontend\Produccion;

use App\Http\Controllers\Controller;
use App\Models\Frontend\Produccion\Color;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;

class ColorController extends Controller{

    public function index(Request $request){
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $color = Color::where('nombre', 'LIKE', "%$keyword%")
                          ->paginate($perPage);
        } else {
            $color = Color::paginate($perPage);
        }

        return view('frontend.produccion.color.index', compact('color'));
    }

    public function create(){
        return view('frontend.produccion.color.create');
    }

    public function store(Request $request){
        $this->validate($request, [
    			'nombre' => 'required'
    		]);

        $requestData = $request->all();

        $nuevoColor = new Color($requestData);
        $nuevoColor->empresa_id = Auth::user()->empresa_id;
        $nuevoColor->save();

        Session::flash('flash_message', trans('labels.frontend.color').trans('alerts.frontend.created').'!');

        return redirect('produccion/color');
    }

    public function show($id){
        $color = Color::findOrFail($id);

        return view('frontend.produccion.color.show', compact('color'));
    }

    public function edit($id){
        $color = Color::findOrFail($id);

        return view('frontend.produccion.color.edit', compact('color'));
    }

    public function update($id, Request $request){
        $this->validate($request, [
    			'nombre' => 'required'
    		]);
        $requestData = $request->all();

        $color = Color::findOrFail($id);
        $color->update($requestData);

        Session::flash('flash_message', trans('labels.frontend.color').trans('alerts.frontend.updated').'!');

        return redirect('produccion/color');
    }

    public function destroy($id){
        Color::destroy($id);

        Session::flash('flash_message',  trans('labels.frontend.color').trans('alerts.frontend.deleted').'!');

        return redirect('produccion/color');
    }
}
