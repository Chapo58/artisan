<?php

namespace App\Http\Controllers\Frontend\Calendario;

use App\Http\Controllers\Controller;
use App\Models\Frontend\Calendario\Event;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;

class EventsController extends Controller{

    public function index(){
        $empresa = Auth::user()->empresa;
        $empleado = Auth::user()->empleado;

        $query = Event::where('empresa_id','=', $empresa->id);
        if(Auth::user()->hasRole('Usuario Vendedor')){
            $query->where('empleado_id','=', $empleado->id);
        }
        $eventos = $query->get();

        return view('frontend.calendario.tareas.calendario',   compact('eventos'));

    }

    public function store(Request $request){
        $empleado = Auth::user()->empleado;
        $empresa = Auth::user()->empresa;
        $sucursal = Auth::user()->sucursal;

        $requestData = $request->all();

        $nuevoEvento = new Event($requestData);
        $nuevoEvento->empresa_id = $empresa->id;
        $nuevoEvento->sucursal_id = $sucursal->id;
        $nuevoEvento->empleado_id = $empleado->id;
        $nuevoEvento->save();

        return redirect('calendario/tareas')->withFlashSuccess('Evento creado correctamente.');
    }

    public function show($id){
        //
    }

    public function edit($id){
     //
    }

    public function update(Request $request, $id){
      $event = Event::findOrFail($id);

      $requestData = $request->all();

      $event->update($requestData);

      return redirect('calendario/tareas')->withFlashSuccess('Evento modificado correctamente.');
    }

    public function crearEvento($fecha,$titulo){
        $empresa = Auth::user()->empresa;
        $sucursal = Auth::user()->sucursal;

        $nuevoEvento = new Event();
        $nuevoEvento->fecha_inicio = $fecha;
        $nuevoEvento->titulo = $titulo;
        $nuevoEvento->empresa_id = $empresa->id;
        $nuevoEvento->sucursal_id = $sucursal->id;
        $nuevoEvento->save();
    }

    public function destroy($id){
      $event = Event::findOrFail($id);
      $empresa = Auth::user()->empresa;

      if($event == null)
          return Response()->json([
              'message'   =>  'Error al intentar eliminar el evento.'
          ]);

      if($event->empresa == $empresa) {
          $event->delete();
      }
    }
}
