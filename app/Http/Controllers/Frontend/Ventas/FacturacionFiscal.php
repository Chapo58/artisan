<?php

namespace App\Http\Controllers\Frontend\Ventas;

use App\Http\Controllers\Controller;
use App\Models\Frontend\Caja\MovimientoCaja;
use App\Models\Frontend\Configuraciones\PorcentajeIva;
use App\Models\Frontend\Configuraciones\TipoComprobante;
use App\Models\Frontend\Ventas\NotaCredito;
use App\Models\Frontend\Ventas\NotaDebito;
use App\Models\Frontend\Ventas\Venta;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Session;

class FacturacionFiscal extends Controller{


    public function facturarVenta($venta_id,$ventaRapida = false){
        $venta = Venta::findOrFail($venta_id);
        $porcentajes_iva = PorcentajeIva::$valores;
        $tipos_comprobantes = TipoComprobante::$comprobantes;
        $empresa = Auth::user()->empresa;

        // Si los movimientos de caja estaban eliminados por un previo fallo en la facturacion, ahora los restauro
        $movimientosCaja = MovimientoCaja::withTrashed()->where('entidad_clase', 'Venta')->where('entidad_id',$venta->id);
        if($movimientosCaja){
            $movimientosCaja->update(array("created_at" => Carbon::now(), "updated_at" => Carbon::now()));
            $movimientosCaja->restore();
        }

        return view('frontend.ventas.venta.imprimir-fiscal', compact('venta','porcentajes_iva','tipos_comprobantes','empresa','ventaRapida'));
    }

    public function facturarNotaDeCredito($nota_credito_id){
        $notaCredito = NotaCredito::findOrFail($nota_credito_id);
        $porcentajes_iva = PorcentajeIva::$valores;
        $tipos_comprobantes = TipoComprobante::$comprobantes;
        $empresa = Auth::user()->empresa;

        return view('frontend.ventas.nota-credito.imprimir-fiscal', compact('notaCredito','porcentajes_iva','tipos_comprobantes','empresa'));
    }

    public function facturarNotaDeDebito($nota_debito_id){
        $notaDebito = NotaDebito::findOrFail($nota_debito_id);
        $porcentajes_iva = PorcentajeIva::$valores;
        $tipos_comprobantes = TipoComprobante::$comprobantes;
        $empresa = Auth::user()->empresa;

        return view('frontend.ventas.nota-debito.imprimir-fiscal', compact('notaDebito','porcentajes_iva','tipos_comprobantes','empresa'));
    }


}
