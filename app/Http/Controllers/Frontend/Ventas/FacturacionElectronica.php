<?php

namespace App\Http\Controllers\Frontend\Ventas;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Frontend\Informes\CalculosInformes;
use App\Models\Frontend\Caja\MovimientoCaja;
use App\Models\Frontend\Configuraciones\Opcion;
use App\Models\Frontend\Configuraciones\PorcentajeIva;
use App\Models\Frontend\Configuraciones\TipoComprobante;
use App\Models\Frontend\Ventas\DetalleNotaCredito;
use App\Models\Frontend\Ventas\DetalleNotaDebito;
use App\Models\Frontend\Ventas\DetalleVenta;
use App\Models\Frontend\Ventas\EstadoFacturacion;
use App\Models\Frontend\Ventas\NotaCredito;
use App\Models\Frontend\Ventas\NotaDebito;
use App\Models\Frontend\Ventas\Venta;
use App\FacturacionElectronica\Afip;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Session;

class FacturacionElectronica extends Controller{


    public function facturarVenta($venta_id){
        $venta = Venta::findOrFail($venta_id);
        $empresa = Auth::user()->empresa;
        $puntoDeVenta = $venta->puntoVenta;
        $calculosInformes = New CalculosInformes();

        $porcentajesIvaVenta = DetalleVenta::select('porcentaje_iva')
            ->where('detalles_ventas.venta_id', $venta->id)
            ->groupBy('porcentaje_iva')
            ->get();

        $resultadosComprobante = $calculosInformes->calcularComprobante($venta);

        $sumatoriaNeto = 0; 
        $sumatoriaIva = 0;
        // Calculo el total de iva
        foreach($porcentajesIvaVenta as $item){
            $item->BaseImp = round($resultadosComprobante['neto'][$item->porcentaje_iva], 2, PHP_ROUND_HALF_UP);
            $item->Importe = round($resultadosComprobante['iva'][$item->porcentaje_iva], 2, PHP_ROUND_HALF_UP);

            $sumatoriaNeto += $item->BaseImp;
            $sumatoriaIva += $item->Importe;

            switch ($item->porcentaje_iva) { // Alicuota de IVA (4)
                case PorcentajeIva::CERO:
                    $item->Id = 3;
                    break;
                case PorcentajeIva::DIEZCOMACINCO:
                    $item->Id = 4;
                    break;
                case PorcentajeIva::VEINTIUNO:
                    $item->Id = 5;
                    break;
                case PorcentajeIva::DOSCOMACINCO:
                    $item->Id = 9;
                    break;
                case PorcentajeIva::CINCO:
                    $item->Id = 8;
                    break;
                case PorcentajeIva::VEINTISIETE:
                    $item->Id = 6;
            }
        }

        foreach($porcentajesIvaVenta as $item){
            unset($item->porcentaje_iva);
        }
        $arrayIvas = $porcentajesIvaVenta->toArray();

        if($venta->tipo_comprobante_id == TipoComprobante::FACTURA_A){
            $tipoComprobante = 1;
        } elseif($venta->tipo_comprobante_id == TipoComprobante::FACTURA_B) {
            $tipoComprobante = 6;
        } else {
            $tipoComprobante = 11;
            $sumatoriaNeto = $sumatoriaNeto + $sumatoriaIva;
            $sumatoriaIva = 0;
            $arrayIvas = NULL;
        }

        $cantidadItems = DetalleVenta::where('venta_id',$venta_id)->count();

        if(strlen($venta->cliente->dni) > 8) { // Codigo de documento del comprador
            if($venta->cliente->dni == '27000000006'){
                $tipoDocumento = 99;
                $clienteDNI = 0;
            } else {
                $tipoDocumento = 80;
                $clienteDNI = str_replace('-', '', $venta->cliente->dni); 
                $clienteDNI = (float)$clienteDNI; 
            }
        } else {
            $tipoDocumento = 96;
            $clienteDNI = intval($venta->cliente->dni);
        }

        $cuit = str_replace('-', '', $empresa->cuit);
        $cuit = (float)$cuit;

        // DATOS Y CERTIFICADOS DE LA EMPRESA PARA FACTURAR
        $afip = new Afip(array('CUIT' => $cuit, 'cert' => storage_path($puntoDeVenta->cert_electronica), 'key' => storage_path($puntoDeVenta->key_electronica), 'production' => TRUE));

        $numeroComprobante = $afip->ElectronicBilling->GetLastVoucher($puntoDeVenta->numero,$tipoComprobante);
        $numeroComprobante++;

        $data = array(
            'CantReg' 		=> $cantidadItems, // Cantidad de items del/los comprobante/s
            'PtoVta' 		=> $puntoDeVenta->numero, // Punto de venta
            'CbteTipo' 		=> $tipoComprobante, // Tipo de comprobante (ver tipos disponibles)
            'Concepto' 		=> 1, // Concepto del Comprobante: (1)Productos, (2)Servicios, (3)Productos y Servicios
            'DocTipo' 		=> $tipoDocumento, // Tipo de documento del comprador (ver tipos disponibles)
            'DocNro' 		=> $clienteDNI, // Numero de documento del comprador
            'CbteDesde' 	=> $numeroComprobante, // Numero de comprobante o numero del primer comprobante en caso de ser mas de uno
            'CbteHasta' 	=> $numeroComprobante, // Numero de comprobante o numero del ultimo comprobante en caso de ser mas de uno
            'CbteFch' 		=> intval(date('Ymd')), // (Opcional) Fecha del comprobante (yyyymmdd) o fecha actual si es nulo
            'ImpTotal' 		=> $venta->total, // Importe total del comprobante
            'ImpTotConc' 	=> 0, // Importe neto no gravado
            'ImpNeto' 		=> $sumatoriaNeto, // Importe neto gravado
            'ImpOpEx' 		=> 0, // Importe exento de IVA
            'ImpIVA' 		=> $sumatoriaIva, //Importe total de IVA
            'ImpTrib' 		=> 0, //Importe total de tributos
            'MonId' 		=> 'PES', //Tipo de moneda usada en el comprobante (ver tipos disponibles)('PES' para pesos argentinos)
            'MonCotiz' 		=> 1, // Cotización de la moneda usada (1 para pesos argentinos)
            'Iva' 			=> $arrayIvas, // (Opcional) Alícuotas asociadas al comprobante
        );

        $res = $afip->ElectronicBilling->CreateVoucher($data);

        $CAE = $res['CAE']; // CAE asignado el comprobante
        $fechaVencimiento =  $res['CAEFchVto']; // Fecha de vencimiento del CAE (yyyy-mm-dd)

        $venta->cae = $CAE;
        $venta->fecha_vencimiento = $fechaVencimiento;
        $venta->numero = str_pad($numeroComprobante, 8, "0", STR_PAD_LEFT);
        $venta->estado_facturacion = EstadoFacturacion::FACTURADO;
        $venta->save();

        $movimientosCaja = MovimientoCaja::where('entidad_clase', 'Venta')->where('entidad_id',$venta->id)->first();
        $movimientosCaja->detalle = 'Venta: '.$venta->puntoVenta->numero.'-'.$venta->numero;
        $movimientosCaja->save();

        $nombresClaves = [
            TipoComprobante::FACTURA_A => 'numeracion_factura_a',
            TipoComprobante::FACTURA_B => 'numeracion_factura_b',
            TipoComprobante::FACTURA_C => 'numeracion_factura_c'
        ];
        $opcion = Opcion::where('empresa_id', $empresa->id)
            ->where('nombre', 'LIKE', $nombresClaves[(int)$venta->tipo_comprobante_id])
            ->where('punto_venta_id', $venta->puntoVenta->id)
            ->first();
        $opcion->valor = str_pad($numeroComprobante + 1, 8, "0", STR_PAD_LEFT);
        $opcion->save();

        return redirect('ventas/venta');
    }

    public function facturarNotaDeCredito($nota_credito_id){
        $notaCredito = NotaCredito::findOrFail($nota_credito_id);
        $empresa = Auth::user()->empresa;
        $puntoDeVenta = $notaCredito->puntoVenta;
        $calculosInformes = New CalculosInformes();

        $porcentajesIvaNotaCredito = DetalleNotaCredito::select('porcentaje_iva')
            ->where('detalles_notas_credito.nota_credito_id', $notaCredito->id)
            ->groupBy('porcentaje_iva')
            ->get();

        $resultadosComprobante = $calculosInformes->calcularComprobante($notaCredito);

        $sumatoriaNeto = 0;
        $sumatoriaIva = 0;
        // Calculo el total de iva
        foreach($porcentajesIvaNotaCredito as $item){
            $item->BaseImp = round($resultadosComprobante['neto'][$item->porcentaje_iva], 2, PHP_ROUND_HALF_UP);
            $item->Importe = round($resultadosComprobante['iva'][$item->porcentaje_iva], 2, PHP_ROUND_HALF_UP);

            $sumatoriaNeto += $item->BaseImp;
            $sumatoriaIva += $item->Importe;

            switch ($item->porcentaje_iva) { // Alicuota de IVA (4)
                case PorcentajeIva::CERO:
                    $item->Id = 3;
                    break;
                case PorcentajeIva::DIEZCOMACINCO:
                    $item->Id = 4;
                    break;
                case PorcentajeIva::VEINTIUNO:
                    $item->Id = 5;
                    break;
                case PorcentajeIva::DOSCOMACINCO:
                    $item->Id = 9;
                    break;
                case PorcentajeIva::CINCO:
                    $item->Id = 8;
                    break;
                case PorcentajeIva::VEINTISIETE:
                    $item->Id = 6;
            }
        }

        foreach($porcentajesIvaNotaCredito as $item){
            unset($item->porcentaje_iva);
        }
        $arrayIvas = $porcentajesIvaNotaCredito->toArray();

        if($notaCredito->tipo_comprobante_id == TipoComprobante::NOTA_CREDITO_A){
            $tipoComprobante = 3;
        } elseif($notaCredito->tipo_comprobante_id == TipoComprobante::NOTA_CREDITO_B) {
            $tipoComprobante = 8;
        } else {
            $tipoComprobante = 13;
            $sumatoriaNeto = $sumatoriaNeto + $sumatoriaIva;
            $sumatoriaIva = 0;
            $arrayIvas = NULL;
        }

        $cantidadItems = DetalleNotaCredito::where('nota_credito_id',$nota_credito_id)->count();
        if(strlen($notaCredito->cliente->dni) > 8) { // Codigo de documento del comprador
            if($notaCredito->cliente->dni == '27000000006'){
                $tipoDocumento = 99;
                $clienteDNI = 0;
            } else {
                $tipoDocumento = 80;
                $clienteDNI = str_replace('-', '', $notaCredito->cliente->dni);
                $clienteDNI = (float)$clienteDNI;
            }
        } else {
            $tipoDocumento = 96;
            $clienteDNI = intval($notaCredito->cliente->dni);
        }

        $cuit = str_replace('-', '', $empresa->cuit);
        $cuit = (float)$cuit;

        // DATOS Y CERTIFICADOS DE LA EMPRESA PARA FACTURAR
        $afip = new Afip(array('CUIT' => $cuit, 'cert' => storage_path($puntoDeVenta->cert_electronica), 'key' => storage_path($puntoDeVenta->key_electronica), 'production' => TRUE));

        $numeroComprobante = $afip->ElectronicBilling->GetLastVoucher($puntoDeVenta->numero,$tipoComprobante);
        $numeroComprobante++;

        if($notaCredito->venta) { // Si tiene una venta asociada
            if($notaCredito->venta->tipo_comprobante_id == TipoComprobante::FACTURA_A){
                $tipoComprobanteAsociado = 1;
            } elseif($notaCredito->venta->tipo_comprobante_id == TipoComprobante::FACTURA_B) {
                $tipoComprobanteAsociado = 6;
            } else {
                $tipoComprobanteAsociado = 11;
            }
            $data = array(
                'CantReg' => $cantidadItems, // Cantidad de items del/los comprobante/s
                'PtoVta' => $puntoDeVenta->numero, // Punto de venta
                'CbteTipo' => $tipoComprobante, // Tipo de comprobante (ver tipos disponibles)
                'Concepto' => 1, // Concepto del Comprobante: (1)Productos, (2)Servicios, (3)Productos y Servicios
                'DocTipo' => $tipoDocumento, // Tipo de documento del comprador (ver tipos disponibles)
                'DocNro' => $clienteDNI, // Numero de documento del comprador
                'CbteDesde' => $numeroComprobante, // Numero de comprobante o numero del primer comprobante en caso de ser mas de uno
                'CbteHasta' => $numeroComprobante, // Numero de comprobante o numero del ultimo comprobante en caso de ser mas de uno
                'CbteFch' => intval(date('Ymd')), // (Opcional) Fecha del comprobante (yyyymmdd) o fecha actual si es nulo
                'ImpTotal' => $notaCredito->total, // Importe total del comprobante
                'ImpTotConc' => 0, // Importe neto no gravado
                'ImpNeto' => $sumatoriaNeto, // Importe neto gravado
                'ImpOpEx' => 0, // Importe exento de IVA
                'ImpIVA' => $sumatoriaIva, //Importe total de IVA
                'ImpTrib' => 0, //Importe total de tributos
                'MonId' => 'PES', //Tipo de moneda usada en el comprobante (ver tipos disponibles)('PES' para pesos argentinos)
                'MonCotiz' => 1, // Cotización de la moneda usada (1 para pesos argentinos)
                'CbtesAsoc' => array( // (Opcional) Comprobantes asociados
                    array(
                        'Tipo' 		=> $tipoComprobanteAsociado, // Tipo de comprobante (ver tipos disponibles)
                        'PtoVta' 	=> $notaCredito->venta->puntoVenta->numero, // Punto de venta
                        'Nro' 		=> $notaCredito->venta->numero // Numero de comprobante
                    )
                ),
                'Iva' => $arrayIvas, // (Opcional) Alícuotas asociadas al comprobante
            );
        } else {
            $data = array(
                'CantReg' => $cantidadItems, // Cantidad de items del/los comprobante/s
                'PtoVta' => $puntoDeVenta->numero, // Punto de venta
                'CbteTipo' => $tipoComprobante, // Tipo de comprobante (ver tipos disponibles)
                'Concepto' => 1, // Concepto del Comprobante: (1)Productos, (2)Servicios, (3)Productos y Servicios
                'DocTipo' => $tipoDocumento, // Tipo de documento del comprador (ver tipos disponibles)
                'DocNro' => intval($clienteDNI), // Numero de documento del comprador
                'CbteDesde' => $numeroComprobante, // Numero de comprobante o numero del primer comprobante en caso de ser mas de uno
                'CbteHasta' => $numeroComprobante, // Numero de comprobante o numero del ultimo comprobante en caso de ser mas de uno
                'CbteFch' => intval(date('Ymd')), // (Opcional) Fecha del comprobante (yyyymmdd) o fecha actual si es nulo
                'ImpTotal' => $notaCredito->total, // Importe total del comprobante
                'ImpTotConc' => 0, // Importe neto no gravado
                'ImpNeto' => $sumatoriaNeto, // Importe neto gravado
                'ImpOpEx' => 0, // Importe exento de IVA
                'ImpIVA' => $sumatoriaIva, //Importe total de IVA
                'ImpTrib' => 0, //Importe total de tributos
                'MonId' => 'PES', //Tipo de moneda usada en el comprobante (ver tipos disponibles)('PES' para pesos argentinos)
                'MonCotiz' => 1, // Cotización de la moneda usada (1 para pesos argentinos)
                'Iva' => $arrayIvas, // (Opcional) Alícuotas asociadas al comprobante
            );
        }


        $res = $afip->ElectronicBilling->CreateVoucher($data);

        $CAE = $res['CAE']; // CAE asignado el comprobante
        $fechaVencimiento =  $res['CAEFchVto']; // Fecha de vencimiento del CAE (yyyy-mm-dd)

        $notaCredito->cae = $CAE;
        $notaCredito->fecha_vencimiento = $fechaVencimiento;
        $notaCredito->numero = str_pad($numeroComprobante, 8, "0", STR_PAD_LEFT);
        $notaCredito->estado_facturacion = EstadoFacturacion::FACTURADO;
        $notaCredito->save();

        $movimientosCaja = MovimientoCaja::where('entidad_clase', 'NotaCredito')->where('entidad_id',$notaCredito->id)->first();
        $movimientosCaja->detalle = 'Nota de Credito: '.$notaCredito->puntoVenta->numero.'-'.$notaCredito->numero;
        $movimientosCaja->save();

        $nombresClaves = [
            TipoComprobante::NOTA_CREDITO_A => 'numeracion_nc_a',
            TipoComprobante::NOTA_CREDITO_B => 'numeracion_nc_b',
            TipoComprobante::NOTA_CREDITO_C => 'numeracion_nc_c'
        ];
        $opcion = Opcion::where('empresa_id', $empresa->id)
            ->where('nombre', 'LIKE', $nombresClaves[(int)$notaCredito->tipo_comprobante_id])
            ->where('punto_venta_id', $notaCredito->puntoVenta->id)
            ->first();
        $opcion->valor = str_pad($numeroComprobante + 1, 8, "0", STR_PAD_LEFT);
        $opcion->save();

        return redirect('ventas/nota-credito');
    }

    public function facturarNotaDeDebito($nota_debito_id){
        $notaDebito = NotaDebito::findOrFail($nota_debito_id);
        $empresa = Auth::user()->empresa;
        $puntoDeVenta = $notaDebito->puntoVenta;
        $calculosInformes = New CalculosInformes();

        $porcentajesIvaNotaDebito = DetalleNotaDebito::select('porcentaje_iva')
            ->where('detalles_notas_debito.nota_debito_id', $notaDebito->id)
            ->groupBy('porcentaje_iva')
            ->get();

        $resultadosComprobante = $calculosInformes->calcularComprobante($notaDebito);

        $sumatoriaNeto = 0;
        $sumatoriaIva = 0;
        // Calculo el total de iva
        foreach($porcentajesIvaNotaDebito as $item){
            $item->BaseImp = round($resultadosComprobante['neto'][$item->porcentaje_iva], 2, PHP_ROUND_HALF_UP);
            $item->Importe = round($resultadosComprobante['iva'][$item->porcentaje_iva], 2, PHP_ROUND_HALF_UP);

            $sumatoriaNeto += $item->BaseImp;
            $sumatoriaIva += $item->Importe;

            switch ($item->porcentaje_iva) { // Alicuota de IVA (4)
                case PorcentajeIva::CERO:
                    $item->Id = 3;
                    break;
                case PorcentajeIva::DIEZCOMACINCO:
                    $item->Id = 4;
                    break;
                case PorcentajeIva::VEINTIUNO:
                    $item->Id = 5;
                    break;
                case PorcentajeIva::DOSCOMACINCO:
                    $item->Id = 9;
                    break;
                case PorcentajeIva::CINCO:
                    $item->Id = 8;
                    break;
                case PorcentajeIva::VEINTISIETE:
                    $item->Id = 6;
            }
        }

        foreach($porcentajesIvaNotaDebito as $item){
            unset($item->porcentaje_iva);
        }
        $arrayIvas = $porcentajesIvaNotaDebito->toArray();

        if($notaDebito->tipo_comprobante_id == TipoComprobante::NOTA_DEBITO_A){
            $tipoComprobante = 2;
        } elseif($notaDebito->tipo_comprobante_id == TipoComprobante::NOTA_DEBITO_B) {
            $tipoComprobante = 7;
        } else {
            $tipoComprobante = 12;
            $sumatoriaNeto = $sumatoriaNeto + $sumatoriaIva;
            $sumatoriaIva = 0;
            $arrayIvas = NULL;
        }

        $cantidadItems = DetalleNotaDebito::where('nota_debito_id',$nota_debito_id)->count();
        if(strlen($notaDebito->cliente->dni) > 8) { // Codigo de documento del comprador
            if($notaDebito->cliente->dni == '27000000006'){
                $tipoDocumento = 99;
                $clienteDNI = 0;
            } else {
                $tipoDocumento = 80;
                $clienteDNI = str_replace('-', '', $notaDebito->cliente->dni);
                $clienteDNI = (float)$clienteDNI;
            }
        } else {
            $tipoDocumento = 96;
            $clienteDNI = intval($notaDebito->cliente->dni);
        }

        $cuit = str_replace('-', '', $empresa->cuit);
        $cuit = (float)$cuit;

        // DATOS Y CERTIFICADOS DE LA EMPRESA PARA FACTURAR
        $afip = new Afip(array('CUIT' => $cuit, 'cert' => storage_path($puntoDeVenta->cert_electronica), 'key' => storage_path($puntoDeVenta->key_electronica), 'production' => TRUE));

        $numeroComprobante = $afip->ElectronicBilling->GetLastVoucher($puntoDeVenta->numero,$tipoComprobante);
        $numeroComprobante++;

        $data = array(
            'CantReg' => $cantidadItems, // Cantidad de items del/los comprobante/s
            'PtoVta' => $puntoDeVenta->numero, // Punto de venta
            'CbteTipo' => $tipoComprobante, // Tipo de comprobante (ver tipos disponibles)
            'Concepto' => 1, // Concepto del Comprobante: (1)Productos, (2)Servicios, (3)Productos y Servicios
            'DocTipo' => $tipoDocumento, // Tipo de documento del comprador (ver tipos disponibles)
            'DocNro' => intval($clienteDNI), // Numero de documento del comprador
            'CbteDesde' => $numeroComprobante, // Numero de comprobante o numero del primer comprobante en caso de ser mas de uno
            'CbteHasta' => $numeroComprobante, // Numero de comprobante o numero del ultimo comprobante en caso de ser mas de uno
            'CbteFch' => intval(date('Ymd')), // (Opcional) Fecha del comprobante (yyyymmdd) o fecha actual si es nulo
            'ImpTotal' => $notaDebito->total, // Importe total del comprobante
            'ImpTotConc' => 0, // Importe neto no gravado
            'ImpNeto' => $sumatoriaNeto, // Importe neto gravado
            'ImpOpEx' => 0, // Importe exento de IVA
            'ImpIVA' => $sumatoriaIva, //Importe total de IVA
            'ImpTrib' => 0, //Importe total de tributos
            'MonId' => 'PES', //Tipo de moneda usada en el comprobante (ver tipos disponibles)('PES' para pesos argentinos)
            'MonCotiz' => 1, // Cotización de la moneda usada (1 para pesos argentinos)
            'Iva' => $arrayIvas, // (Opcional) Alícuotas asociadas al comprobante
        );



        $res = $afip->ElectronicBilling->CreateVoucher($data);

        $CAE = $res['CAE']; // CAE asignado el comprobante
        $fechaVencimiento =  $res['CAEFchVto']; // Fecha de vencimiento del CAE (yyyy-mm-dd)

        $notaDebito->cae = $CAE;
        $notaDebito->fecha_vencimiento = $fechaVencimiento;
        $notaDebito->numero = str_pad($numeroComprobante, 8, "0", STR_PAD_LEFT);
        $notaDebito->estado_facturacion = EstadoFacturacion::FACTURADO;
        $notaDebito->save();

        $movimientosCaja = MovimientoCaja::where('entidad_clase', 'NotaDebito')->where('entidad_id',$notaDebito->id)->first();
        $movimientosCaja->detalle = 'Nota de Debito: '.$notaDebito->puntoVenta->numero.'-'.$notaDebito->numero;
        $movimientosCaja->save();

        $nombresClaves = [
            TipoComprobante::NOTA_CREDITO_A => 'numeracion_nd_a',
            TipoComprobante::NOTA_CREDITO_B => 'numeracion_nd_b',
            TipoComprobante::NOTA_CREDITO_C => 'numeracion_nd_c'
        ];
        $opcion = Opcion::where('empresa_id', $empresa->id)
            ->where('nombre', 'LIKE', $nombresClaves[(int)$notaDebito->tipo_comprobante_id])
            ->where('punto_venta_id', $notaDebito->puntoVenta->id)
            ->first();
        $opcion->valor = str_pad($numeroComprobante + 1, 8, "0", STR_PAD_LEFT);
        $opcion->save();

        return redirect('ventas/nota-debito');
    }

}
