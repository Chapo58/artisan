<?php

namespace App\Http\Controllers\Frontend\Ventas;

use App\Http\Controllers\Controller;
use App\Models\Frontend\Articulos\Articulo;
use App\Models\Frontend\Articulos\Rubro;
use App\Models\Frontend\Configuraciones\Estado;
use App\Models\Frontend\Configuraciones\Opcion;
use App\Models\Frontend\Configuraciones\PorcentajeIva;
use App\Models\Frontend\Ventas\ListaPrecio;
use App\Models\Frontend\Ventas\Precio;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;
use DB;

class listaPreciosController extends Controller{

    public function index(){
        $empresa = Auth::user()->empresa;
        $listaprecios = ListaPrecio::getListasPorEmpresa($empresa);

        return view('frontend.ventas.lista-precios.index', compact('listaprecios'));
    }

    public function create(){
        return view('frontend.ventas.lista-precios.create');
    }

    public function store(Request $request){
        $this->validate($request, [
			'nombre' => 'required'
		]);
        $requestData = $request->all();
        $empresa = Auth::user()->empresa;

        $listaPrecio = new ListaPrecio($requestData);
        $listaPrecio->empresa_id = Auth::user()->empresa_id;
        $listaPrecio->estado = Estado::ACTIVO;
        $listaPrecio->save();

        // Agrego los precios de la lista a todos los articulos ya existentes

        $coleccionArticulos = Articulo::where('empresa_id',$empresa->id)
            ->whereNull('articulos.deleted_at')->get();
        foreach($coleccionArticulos as $articulo){
            $precioNuevo = new Precio();
            $precioNuevo->precio = ($articulo->ultimo_costo * $listaPrecio->porcentaje)/100 + $articulo->ultimo_costo;
            $precioNuevo->articulo_id = $articulo->id;
            $precioNuevo->lista_precio_id = $listaPrecio->id;
            $precioNuevo->save();
        }

        return redirect('ventas/lista-precios')->withFlashSuccess('Lista de precios creada correctamente.');

    }

    public function show($id){
        $listaprecio = ListaPrecio::findOrFail($id);

        if ($listaprecio->empresa_id == Auth::user()->empresa_id) {
            return view('frontend.ventas.lista-precios.show', compact('listaprecio'));
        } else {
            return redirect('ventas/lista-precios');
        }
    }

    public function edit($id){
        $listaprecio = ListaPrecio::findOrFail($id);

        if ($listaprecio->empresa_id == Auth::user()->empresa_id) {
            return view('frontend.ventas.lista-precios.edit', compact('listaprecio'));
        } else {
            return redirect('ventas/lista-precios');
        }
    }

    public function update($id, Request $request){
        $this->validate($request, [
			'nombre' => 'required'
		]);
        $requestData = $request->all();
        $empresa = Auth::user()->empresa;

        $listaPrecio = listaPrecio::findOrFail($id);

        if ($listaPrecio->empresa_id == Auth::user()->empresa_id) {
            $listaPrecio->update($requestData);

            $coleccionArticulos = Articulo::where('empresa_id',$empresa->id)
                ->whereNull('articulos.deleted_at')
                ->get();

            if(isset($requestData['actualizar'])){
                foreach($coleccionArticulos as $articulo){
                    $precio = Precio::where('articulo_id',$articulo->id)
                        ->where('lista_precio_id',$listaPrecio->id)
                        ->whereNull('deleted_at')
                        ->first();

                    if($precio){
                        $precio->precio = ($precio->articulo->ultimo_costo * $listaPrecio->porcentaje)/100 + $precio->articulo->ultimo_costo;
                        $precio->save();
                    } else {
                        $precioNuevo = new Precio();
                        $precioNuevo->precio = ($articulo->ultimo_costo * $listaPrecio->porcentaje)/100 + $articulo->ultimo_costo;
                        $precioNuevo->articulo_id = $articulo->id;
                        $precioNuevo->lista_precio_id = $listaPrecio->id;
                        $precioNuevo->save();
                    }
                }
            }
            if($requestData['porcentaje_acumulativo']){
                foreach($coleccionArticulos as $articulo){
                    $precio = Precio::where('articulo_id',$articulo->id)
                        ->where('lista_precio_id',$listaPrecio->id)
                        ->whereNull('deleted_at')
                        ->first();

                    $precio->precio = ($precio->precio * $requestData['porcentaje_acumulativo'])/100 + $precio->precio;
                    $precio->save();
                }
            }
            Session::flash('flash_success', 'Lista de precios modificada correctamente.');
        }

        return redirect('ventas/lista-precios');
    }

    public function destroy($id){
        if(Auth::user()->email != 'demo@ciatt.com.ar') {
            $listaprecio = ListaPrecio::findOrFail($id);
            $empresa = Auth::user()->empresa;
            $listapreciodefecto = Opcion::where('empresa_id', $empresa->id)
                ->where('nombre', 'lista_precios_defecto_id')
                ->first();

            if ($listaprecio->empresa_id == Auth::user()->empresa_id) {
                if ($listaprecio->id != $listapreciodefecto->id) {
                    ListaPrecio::destroy($id);

                    $coleccionPrecios = Precio::where('lista_precio_id', $id)->wherenull('deleted_at')->get();
                    foreach ($coleccionPrecios as $precio) {
                        $precio->delete();
                    }

                    Session::flash('flash_success', 'Lista de precios eliminada correctamente.');
                }
            }
        }

        return redirect('ventas/lista-precios');
    }

    public function imprimir(){
        $empresa = Auth::user()->empresa;
        $listas = ListaPrecio::getListasPorEmpresaArray($empresa);
        $rubros = ['' => 'Todos'] + Rubro::getRubrosPorEmpresaArray($empresa);

        return view('frontend.ventas.lista-precios.index-impresion', compact('listas','rubros'));
    }

    public function generarPdf(Request $request){
        $empresa = Auth::user()->empresa;
        $sucursal = Auth::user()->sucursal;

        $listaprecio = ListaPrecio::findOrFail($request->lista_precios_id);
        if ($listaprecio->empresa_id == $empresa->id) {

            $query = Articulo::select('articulos.*');
            $query->where('articulos.empresa_id',$empresa->id);
            $query->leftJoin('stocks', function ($join) use ($sucursal) {
                $join->on('articulos.id', '=', 'stocks.articulo_id')
                    ->where('stocks.sucursal_id', '=', $sucursal->id);
            });
            if($request->con_stock){
                $query->where(function($q){
                    $q->where('stocks.cantidad','>=',1)
                        ->orWhere('articulos.lleva_stock','=',0);
                });
            }
            if($request->rubro_id){
                $query->where('articulos.rubro_id','=',$request->rubro_id);
            }
            $query->orderBy($request->ordenamiento,'ASC');

            $articulos = $query->get();

            $porcentajes_iva = PorcentajeIva::$valores;

            $pdf = app('dompdf.wrapper');
            $pdf->getDomPDF()->set_option("enable_php", true);
            $pdf->loadView('frontend.ventas.lista-precios.imprimir-lista',
                compact('articulos','listaprecio','request','porcentajes_iva'));

            return $pdf->stream('Lista de Precios.pdf');
        } else {
            return redirect('ventas/lista-precios');
        }
    }
}
