<?php

namespace App\Http\Controllers\Frontend\Ventas;

use App\Http\Controllers\Controller;
use App\Models\Frontend\Configuraciones\Estado;
use App\Models\Frontend\Configuraciones\Opcion;
use App\Models\Frontend\Configuraciones\PorcentajeIva;
use App\Models\Frontend\Configuraciones\TipoComprobante;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;
use Session;
use DateTime;

class CalculosVistas extends Controller{

    protected $detalles;
    protected $importeDescuento;
    protected $tipoComprobante;
    protected $montoAdicional = 0; // Montos de cobros y pagos
    protected $interesAdicional = 0; // Intereses de cobros y pagos

    public function __construct(Request $request){
        $this->detalles = $request->detalles;
        $this->importeDescuento = $request->importe_descuento;
        $this->tipoComprobante = $request->tipo_comprobante_id;
        if($request->detalles_cobro){
            foreach($request->detalles_cobro as $cobro){
                $this->montoAdicional += $cobro['monto'];
                $this->interesAdicional += $cobro['interes'];
            }
        }
        if($request->detalles_pago){
            foreach($request->detalles_pago as $pago){
                $this->montoAdicional += $pago['monto'];
                $this->interesAdicional += $pago['interes'];
            }
        }
    }

    public function calcularTotales(Request $request){
        $calculos = New CalculosVistas($request);
        $resultados['netos'] = $calculos->calcularNetos();
        $resultados['ivas'] = $calculos->calcularIvas();
        $resultados['impuestosInternos'] = $calculos->calcularImpuestosInternos();
        $resultados['bruto'] = round($resultados['netos']['total'] + $resultados['ivas']['total'] + $resultados['impuestosInternos'],2);
        $resultados['total'] = $calculos->calcularTotal();
        $resultados['totalFinal'] = $calculos->calcularTotalFinal();

        return response()->json($resultados);
    }

    public function calcularNetos(){
        $constantes_iva = PorcentajeIva::$constantes;

        $neto['total'] = 0;
        foreach($constantes_iva as $porcentajeIva){
            $neto[$porcentajeIva] = 0;
        }

        foreach ($this->detalles as $detalle) {
            if ((int)$detalle['estado'] == Estado::ACTIVO && isset($detalle['cantidad']) && isset($detalle['articulo_id'])) {
                $neto[$detalle['porcentaje_iva']] += $detalle['precio_neto'] * $detalle['cantidad'];
                $neto['total'] += $detalle['precio_neto'] * $detalle['cantidad'];
            }
        }
        $neto['total'] = round($neto['total'],2);

        return $neto;
    }

    public function calcularIvas(){
        $constantes_iva = PorcentajeIva::$constantes;
        $porcentajes_iva = PorcentajeIva::$valores;

        $iva['total'] = 0;
        foreach($constantes_iva as $porcentajeIva){
            $iva[$porcentajeIva] = 0;
        }

        foreach ($this->detalles as $detalle) {
            if ((int)$detalle['estado'] == Estado::ACTIVO && isset($detalle['cantidad']) && isset($detalle['articulo_id'])) {
                $importeNeto = $detalle['precio_neto'] * $detalle['cantidad'];
                $iva[$detalle['porcentaje_iva']] += $importeNeto * $porcentajes_iva[$detalle['porcentaje_iva']] / 100;
                $iva['total'] += round($importeNeto * $porcentajes_iva[$detalle['porcentaje_iva']] / 100,2);
            }
        }

        $iva['total'] = round($iva['total'],2);

        return $iva;
    }

    public function calcularImpuestosInternos(){
        $importeImpuestosInternos = 0;

        foreach ($this->detalles as $detalle) {
            if ((int)$detalle['estado'] == Estado::ACTIVO && isset($detalle['cantidad']) && isset($detalle['articulo_id'])) {
                $importeImpuestosInternos += $detalle['importe_impuesto_interno'] * $detalle['cantidad'];
            }
        }

        return round($importeImpuestosInternos,2);
    }

    public function calcularTotal($interesesCobro = false){
        $porcentajes_iva = PorcentajeIva::$valores;
        $constantes_iva = PorcentajeIva::$constantes;
        $netos = $this->calcularNetos();
        $ivas = $this->calcularIvas();

        if($interesesCobro){
            $this->importeDescuento += $this->interesAdicional;
        }
        $porcentajeDescuento = (($this->importeDescuento * -1) / $netos['total']) * 100;
        $totalNeto = 0;
        $totalIva = 0;

        foreach($constantes_iva as $porcentajeIva) {
            $importeDescuento = ($netos[$porcentajeIva] * $porcentajeDescuento) / 100;
            $calcularNeto = $netos[$porcentajeIva] - $importeDescuento;
            $totalNeto += $calcularNeto;
            if ($this->tipoComprobante == TipoComprobante::FACTURA_A) {
                $calcularIva = $calcularNeto * $porcentajes_iva[$porcentajeIva] / 100;
                $totalIva += $calcularIva;
            } else {
                $totalIva = $ivas['total'];
            }
        }
        $total = $totalNeto + $totalIva + $this->calcularImpuestosInternos();
        return round($total,2);
    }

    public function calcularTotalFinal(){
        $totalSinIntereses = $this->calcularTotal();
        $totalConIntereses = $this->calcularTotal(true);
        $diferenciaIntereses = $totalConIntereses - $totalSinIntereses;
        $totalCobro = $this->montoAdicional + $diferenciaIntereses;

        return round($totalCobro,2);
    }

    public function redondeo($n,$x) {
        return (round($n)%$x === 0) ? round($n) : round(($n+$x/2)/$x)*$x;
    }

}
