<?php

namespace App\Http\Controllers\Frontend\Ventas;

use App\Http\Controllers\Controller;
use App\Models\Frontend\Configuraciones\Estado;
use App\Models\Frontend\Configuraciones\Moneda;
use App\Models\Frontend\Configuraciones\PorcentajeIva;
use App\Models\Frontend\Configuraciones\PuntoDeVenta;
use App\Models\Frontend\Configuraciones\TipoComprobante;
use App\Models\Frontend\Ventas\DetalleVencimientoFacturaAutomatica;
use App\Models\Frontend\Ventas\DetalleVenta;
use App\Models\Frontend\Ventas\FacturaVentaAutomatica;
use App\Models\Frontend\Ventas\ListaPrecio;
use App\Models\Frontend\Ventas\Venta;
use Carbon\Carbon;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Session;

class FacturaVentaAutomaticaController extends Controller{

    public function index(){
        if(isset(Auth::user()->empresa) == false) {
            return redirect('dashboard');
        }

        $empresa = Auth::user()->empresa;

        $facturaventaautomatica = FacturaVentaAutomatica::where('empresa_id',$empresa->id)->get();

        return view('frontend.ventas.factura-venta-automatica.index', compact('facturaventaautomatica'));
    }

    public function create(){
        if(isset(Auth::user()->empresa) == false) {
            return redirect('dashboard');
        }

        $empresa = Auth::user()->empresa;

        $fecha_hoy = date('d/m/Y');

        if($empresa->condicion_iva_id == 1){//Monotributista
            $tipo_comprobante_por_defecto = TipoComprobante::FACTURA_C;
        }else{//RI
            $tipo_comprobante_por_defecto = TipoComprobante::FACTURA_B;
        }

        $punto_de_venta_por_defecto = null;
        if(isset(Auth::user()->empleado->puntoVenta)){
            $punto_de_venta_por_defecto = Auth::user()->empleado->puntoVenta;
        }

        $tipos_comprobantes = TipoComprobante::$arrayComboFacturas;
        $monedas = Moneda::getMonedasPorEmpresaArray($empresa);
        $porcentajes_iva = PorcentajeIva::$valores;
        $monedaPorDefecto = Moneda::where('empresa_id', $empresa->id)->whereNull('deleted_at')->where('cotizacion','=', 1)->first();

        $lista_precios = ListaPrecio::getListasPorEmpresaArray($empresa);

        if (Auth::user()->hasRole('Usuario Vendedor')) {
            $puntos_de_ventas = PuntoDeVenta::where('sucursal_id', Auth::user()->sucursal_id)->get()->pluck('numero', 'id')->toArray();
        }else{
            $puntos_de_ventas = PuntoDeVenta::where('empresa_id', Auth::user()->empresa_id)->get()->pluck('numero', 'id')->toArray();
        }

        return view('frontend.ventas.factura-venta-automatica.create', compact('fecha_hoy','tipo_comprobante_por_defecto', 'tipos_comprobantes', 'monedas', 'punto_de_venta_por_defecto', 'monedaPorDefecto', 'lista_precios', 'puntos_de_ventas', 'porcentajes_iva'));
    }

    public function store(Request $request){
        DB::transaction(function() use ($request) {
            $requestData = $request->all();

            $empresa = Auth::user()->empresa;
            $sucursal = Auth::user()->sucursal;

            $requestData['fecha_inicio'] = DateTime::createFromFormat('d/m/Y H:i:s', $requestData['fecha_inicio'].' 00:00:00');
            $requestData['fecha_fin'] = DateTime::createFromFormat('d/m/Y H:i:s', $requestData['fecha_fin'].' 00:00:00');

            $facturaVentaAutomatica = new FacturaVentaAutomatica($requestData);
            $facturaVentaAutomatica->empresa_id = $empresa->id;
            if((int)$facturaVentaAutomatica->dia_facturacion > 28){
                $facturaVentaAutomatica->dia_facturacion = 28;
            }elseif((int)$facturaVentaAutomatica->dia_facturacion < 1){
                $facturaVentaAutomatica->dia_facturacion = 1;
            }
            $facturaVentaAutomatica->empresa_id = $empresa->id;
            $facturaVentaAutomatica->estado = Estado::ACTIVO;

            $ventaPlantilla = new Venta($requestData);
            $ventaPlantilla->empresa_id = $empresa->id;
            $ventaPlantilla->sucursal_id = $sucursal->id;
            $ventaPlantilla->numero = '00000000';
            $ventaPlantilla->total_cobro = $ventaPlantilla->total;
            $ventaPlantilla->estado = Estado::PLANTILLA;
            if($ventaPlantilla->importe_neto == null)
                $ventaPlantilla->importe_neto = 0;
            if($ventaPlantilla->importe_iva == null)
                $ventaPlantilla->importe_iva = 0;
            $ventaPlantilla->save();

            if(isset($requestData['detalles'])) {
                $detalles = $requestData['detalles'];
                foreach ($detalles as $detalle) {
                    if((int)$detalle['estado'] == Estado::ACTIVO && isset($detalle['cantidad']) && isset($detalle['articulo_id'])) {
                        $nuevoDetalle = New DetalleVenta($detalle);
                        $nuevoDetalle->venta_id = $ventaPlantilla->id;
                        $nuevoDetalle->save();
                    }
                }
            }

            $facturaVentaAutomatica->venta_id = $ventaPlantilla->id;
            $facturaVentaAutomatica->save();

            if(isset($requestData['intervalos'])) {
                $intervalos = $requestData['intervalos'];
                foreach ($intervalos as $intervalo) {
                    if((int)$intervalo['estado'] == Estado::ACTIVO) {
                        $nuevoIntervalo = New DetalleVencimientoFacturaAutomatica();
                        $nuevoIntervalo->desde = $intervalo['dias_desde'];
                        $nuevoIntervalo->hasta = $intervalo['dias_hasta'];
                        $nuevoIntervalo->porcentaje_recargo = $intervalo['recargo'];
                        $nuevoIntervalo->factura_automatica_id = $facturaVentaAutomatica->id;
                        $nuevoIntervalo->save();
                    }
                }
            }

            Session::flash('flash_message', trans('labels.frontend.facturaventaautomatica').trans('alerts.frontend.created').'!');
        });

        return redirect('ventas/factura-venta-automatica');
    }

    public function show($id){
        if(isset(Auth::user()->empresa) == false) {
            return redirect('dashboard');
        }

        $facturaVentaAutomatica = FacturaVentaAutomatica::findOrFail($id);

        return view('frontend.ventas.factura-venta-automatica.show', compact('facturaVentaAutomatica'));
    }

    public function edit($id){
        if(isset(Auth::user()->empresa) == false) {
            return redirect('dashboard');
        }

        $facturaVentaAutomatica = FacturaVentaAutomatica::findOrFail($id);

        $empresa = Auth::user()->empresa;

        $tipos_comprobantes = TipoComprobante::$arrayComboFacturas;
        $monedas = Moneda::getMonedasPorEmpresaArray($empresa);
        $porcentajes_iva = PorcentajeIva::$valores;
        $lista_precios = ListaPrecio::getListasPorEmpresaArray($empresa);

        $monto_recargo = $facturaVentaAutomatica->totalIntervalo(count($facturaVentaAutomatica->intervalos)) - $facturaVentaAutomatica->venta->total;
        $ultimo_dia_invervalo = $facturaVentaAutomatica->intervalos[count($facturaVentaAutomatica->intervalos)-1]->hasta;

        if (Auth::user()->hasRole('Usuario Vendedor')) {
            $puntos_de_ventas = PuntoDeVenta::where('sucursal_id', Auth::user()->sucursal_id)->get()->pluck('numero', 'id')->toArray();
        }else{
            $puntos_de_ventas = PuntoDeVenta::where('empresa_id', Auth::user()->empresa_id)->get()->pluck('numero', 'id')->toArray();
        }

        return view('frontend.ventas.factura-venta-automatica.edit', compact('facturaVentaAutomatica','tipos_comprobantes', 'monedas', 'porcentajes_iva', 'lista_precios', 'puntos_de_ventas', 'monto_recargo', 'ultimo_dia_invervalo'));
    }

    public function update($id, Request $request){
        DB::transaction(function() use ($id, $request) {
            $requestData = $request->all();

            $requestData['fecha_inicio'] = DateTime::createFromFormat('d/m/Y H:i:s', $requestData['fecha_inicio'].' 00:00:00');
            $requestData['fecha_fin'] = DateTime::createFromFormat('d/m/Y H:i:s', $requestData['fecha_fin'].' 00:00:00');

            $facturaVentaAutomatica = FacturaVentaAutomatica::findOrFail($id);
            $facturaVentaAutomatica->fecha_inicio = $requestData['fecha_inicio'];
            $facturaVentaAutomatica->fecha_fin = $requestData['fecha_fin'];
            $facturaVentaAutomatica->dia_facturacion = $requestData['dia_facturacion'];
            if((int)$facturaVentaAutomatica->dia_facturacion > 28){
                $facturaVentaAutomatica->dia_facturacion = 28;
            }elseif((int)$facturaVentaAutomatica->dia_facturacion < 1){
                $facturaVentaAutomatica->dia_facturacion = 1;
            }
            $facturaVentaAutomatica->update();

            $facturaVentaAutomatica->venta->importe_neto = $requestData['importe_neto'];
            if ($facturaVentaAutomatica->venta->importe_neto == null)
                $facturaVentaAutomatica->venta->importe_neto = 0;
            $facturaVentaAutomatica->venta->importe_iva = $requestData['importe_iva'];
            if ($facturaVentaAutomatica->venta->importe_iva == null)
                $facturaVentaAutomatica->venta->importe_iva = 0;
            $facturaVentaAutomatica->venta->impuesto = $requestData['impuesto'];
            $facturaVentaAutomatica->venta->importe_impuesto_interno = $requestData['importe_impuesto_interno'];
            $facturaVentaAutomatica->venta->total = $requestData['total'];
            $facturaVentaAutomatica->venta->total_cobro = $facturaVentaAutomatica->venta->total;
            $facturaVentaAutomatica->venta->descripcion = $requestData['descripcion'];
            $facturaVentaAutomatica->venta->total = $requestData['total'];
            $facturaVentaAutomatica->venta->update();

            if (isset($requestData['detalles'])) {
                $detalles = $requestData['detalles'];
                foreach ($detalles as $detalle) {
                    if ((int)$detalle['estado'] == Estado::ACTIVO && isset($detalle['cantidad']) && isset($detalle['articulo_id'])) {
                        if((int)$detalle['id'] > 0){
                            $viejoDetalle = DetalleVenta::findOrFail($detalle['id']);
                            $viejoDetalle->update($detalle);
                        }else{
                            $nuevoDetalle = New DetalleVenta($detalle);
                            $nuevoDetalle->venta_id = $facturaVentaAutomatica->venta->id;
                            $nuevoDetalle->save();
                        }
                    }else{
                        if((int)$detalle['id'] > 0){
                            DetalleVenta::destroy($detalle['id']);
                        }
                    }
                }
            }

            if (isset($requestData['intervalos'])) {
                $intervalos = $requestData['intervalos'];
                foreach ($intervalos as $intervalo) {
                    if ((int)$intervalo['estado'] == Estado::ACTIVO) {
                        if((int)$intervalo['id'] > 0){
                            $viejoIntervalo = DetalleVencimientoFacturaAutomatica::findOrFail($intervalo['id']);
                            $viejoIntervalo->desde = $intervalo['dias_desde'];
                            $viejoIntervalo->hasta = $intervalo['dias_hasta'];
                            $viejoIntervalo->porcentaje_recargo = $intervalo['recargo'];
                            $viejoIntervalo->update();
                        }else{
                            $nuevoIntervalo = New DetalleVencimientoFacturaAutomatica();
                            $nuevoIntervalo->desde = $intervalo['dias_desde'];
                            $nuevoIntervalo->hasta = $intervalo['dias_hasta'];
                            $nuevoIntervalo->porcentaje_recargo = $intervalo['recargo'];
                            $nuevoIntervalo->factura_automatica_id = $facturaVentaAutomatica->id;
                            $nuevoIntervalo->save();
                        }
                    }else{
                        if((int)$intervalo['id'] > 0){
                            DetalleVencimientoFacturaAutomatica::destroy($intervalo['id']);
                        }
                    }
                }
            }

            Session::flash('flash_message', trans('labels.frontend.facturaventaautomatica').trans('alerts.frontend.updated').'!');
        });

        return redirect('ventas/factura-venta-automatica');
    }

    public function destroy($id){
        if(isset(Auth::user()->empresa) == false) {
            return redirect('dashboard');
        }

        FacturaVentaAutomatica::destroy($id);

        Session::flash('flash_message',  trans('labels.frontend.facturaventaautomatica').trans('alerts.frontend.deleted').'!');

        return redirect('ventas/factura-venta-automatica');
    }

    public function generarFacturas(){
        if(!Auth::user()->hasRole('Administrador')) {
            return redirect('dashboard');
        }

        DB::transaction(function() {
            $facturasAutomaticas = FacturaVentaAutomatica::get();

            $actualizadas = 0;
            $generadas = 0;

            $fecha_hoy = Carbon::now();

            foreach ($facturasAutomaticas as $facturaAutomatica) {

                //Actualizar las ya generadas
                if(isset($facturaAutomatica->ventasGeneradas)){
                    foreach ($facturaAutomatica->ventasGeneradas as $facturaGenerada){
                        if($facturaGenerada->estado == Estado::FACTURA_AUTOMATICA_PENDIENTE){
                            if(FacturaVentaAutomatica::actualizarFacturaVencimientos($facturaAutomatica, $facturaGenerada)){
                                $actualizadas++;
                            }
                        }
                    }
                }

                //Generar las nuevas
                if($facturaAutomatica->fecha_inicio < $fecha_hoy and $facturaAutomatica->fecha_fin > $fecha_hoy) {
                    if($fecha_hoy->day >= $facturaAutomatica->dia_facturacion) {
                        if (!FacturaVentaAutomatica::facturaMensualGenerada($facturaAutomatica)) {
                            $nuevaFacturaAutomatica = FacturaVentaAutomatica::generarFacturaSegunTemplate($facturaAutomatica);
                            $generadas++;

                            if(FacturaVentaAutomatica::actualizarFacturaVencimientos($facturaAutomatica, $nuevaFacturaAutomatica)){
                                $actualizadas++;
                            }
                        }
                    }
                }
            }

            print_r('Actualizadas: '.$actualizadas);
            print_r(' Generadas: '.$generadas);
        });

        $actualizadas = 0;
        $generadas = 0;

        return view('backend.generar-facturas-automaticas.index', compact('actualizadas', 'generadas'));
    }
}
