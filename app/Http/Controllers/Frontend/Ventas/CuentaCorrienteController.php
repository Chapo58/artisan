<?php

namespace App\Http\Controllers\Frontend\Ventas;

use App\Http\Controllers\Controller;
use App\Models\Frontend\Personas\Proveedor;
use App\Models\Frontend\Ventas\CuentaCorriente;
use App\Models\Frontend\Configuraciones\TipoComprobante;
use App\Models\Frontend\Personas\Cliente;
use App\Models\Frontend\Ventas\EstadoCuentaCorriente;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\HtmlString;
use Yajra\Datatables\Facades\Datatables;
use Session;
use DB;

class CuentaCorrienteController extends Controller{

    public function index(){
        return view('frontend.ventas.cuenta-corriente.index');
    }

    public function datos(){
        $empresa = Auth::user()->empresa;
        $cuentasCorrientes = CuentaCorriente::listarCuentasCorrientesPorEmpresa($empresa);
        $tipos_comprobantes = TipoComprobante::$comprobantes;

        return Datatables::of($cuentasCorrientes)
            ->addColumn('action', function ($cuentaCorriente) {
                return '
                <a href="'.url($cuentaCorriente->url).'/'.$cuentaCorriente->entidad_id.'" class="mytooltip"><i class="fa fa-eye text-success m-r-10"></i>
                    <span class="tooltip-content3">Ver Comprobante</span>
                </a>
                <a class="delete mytooltip pointer" data-id="'.$cuentaCorriente->id.'">
                    <i class="fa fa-close text-danger"></i>
                    <span class="tooltip-content3">Eliminar Comprobante</span>
                </a>
                ';
            })
            ->addColumn('persona', function ($cuentaCorriente) {
                if($cuentaCorriente->cliente){
                    return 'Cliente '.$cuentaCorriente->cliente;
                } elseif($cuentaCorriente->proveedor) {
                    return 'Proveedor '.$cuentaCorriente->proveedor;
                } else {
                    return '';
                }
            })
            ->addColumn('saldo', function ($cuentaCorriente) {
                if($cuentaCorriente->cliente){
                    $saldo = $cuentaCorriente->debe - $cuentaCorriente->haber;
                    return new HtmlString('<span class="text-success">$ '.$saldo.'</span>');
                } elseif($cuentaCorriente->proveedor) {
                    $saldo = $cuentaCorriente->haber - $cuentaCorriente->debe;
                    return new HtmlString('<span class="text-danger">$ '.$saldo.'</span>');
                } else {
                    return '';
                }
            })
            ->editColumn('proximo_vencimiento', function ($cuentaCorriente) {
                if($cuentaCorriente->proximo_vencimiento){
                    return $cuentaCorriente->proximo_vencimiento->format('d/m/Y');
                }
            })
            ->editColumn('detalle', function ($cuentaCorriente) use ($tipos_comprobantes) {
                if($cuentaCorriente->tipo_comprobante){
                    return $tipos_comprobantes[$cuentaCorriente->tipo_comprobante].' '.$cuentaCorriente->detalle;
                } else {
                    return $cuentaCorriente->detalle;
                }
            })
            ->setRowId('id')
            ->removeColumn('id')
            ->removeColumn('url')
            ->removeColumn('entidad_id')
            ->removeColumn('cliente')
            ->removeColumn('proveedor')
            ->removeColumn('tipo_comprobante')
            ->removeColumn('debe')
            ->removeColumn('haber')
            ->make(true);
    }

    public function showCuentaCorrienteCliente($cliente_id){
        $empresa = Auth::user()->empresa;

        $cliente = Cliente::findOrFail($cliente_id);
        if($cliente->empresa == $empresa){
            $tipos_comprobantes = TipoComprobante::$comprobantes;
            $cuentasCorrientes = CuentaCorriente::listarCuentasCorrientesPorClienteEmpresa($cliente, $empresa);

            return view('frontend.ventas.cuenta-corriente.show-cliente', compact('cuentasCorrientes','tipos_comprobantes' , 'cliente'));
        }
        return redirect('personas/cliente');
    }

    public function showCuentaCorrienteProveedor($proveedor_id){
        $empresa = Auth::user()->empresa;

        $proveedor = Proveedor::findOrFail($proveedor_id);
        if($proveedor->empresa == $empresa){
            $tipos_comprobantes = TipoComprobante::$comprobantes;
            $cuentasCorrientes = CuentaCorriente::listarCuentasCorrientesPorProveedorEmpresa($proveedor, $empresa);

            return view('frontend.ventas.cuenta-corriente.show-proveedor', compact('cuentasCorrientes','tipos_comprobantes', 'proveedor'));
        }
        return redirect('personas/proveedor');
    }

    public function debe($objeto, $request, $tipo_cuenta_corriente = NULL, $monto = 0){
      return $this->guardarComprobante($objeto, $request, $tipo_cuenta_corriente, 1, $monto);
    }

    public function haber($objeto, $request, $tipo_cuenta_corriente = NULL, $monto = 0){
      return $this->guardarComprobante($objeto, $request, $tipo_cuenta_corriente, 2, $monto);
    }

    private function guardarComprobante($objeto, $request, $tipo_cuenta_corriente, $tipo, $monto){
        $nuevoComprobante = new CuentaCorriente();
        $nuevoComprobante->cliente_id = $request->cliente_id;
        $nuevoComprobante->proveedor_id = $request->proveedor_id;
        $nuevoComprobante->entidad_id = $objeto->id;
        $nuevoComprobante->url = $request->getRequestUri();
        $nuevoComprobante->detalle = $objeto->__toString();
        $nuevoComprobante->tipo_comprobante = $request->tipo_comprobante_id;
        $nuevoComprobante->tipo_cuenta_corriente_id = $tipo_cuenta_corriente;
        if($tipo == 1){
            $nuevoComprobante->debe = $monto;
            $nuevoComprobante->haber = 0;
            Cliente::where('id', $request->cliente_id)->increment('saldo_actual', $monto);
        }else{
            $nuevoComprobante->haber = $monto;
            $nuevoComprobante->debe = 0;
            Cliente::where('id', $request->cliente_id)->decrement('saldo_actual', $monto);
        }
        $nuevoComprobante->empresa_id = Auth::user()->empresa_id;
        $nuevoComprobante->estado = EstadoCuentaCorriente::PENDIENTE;
        $nuevoComprobante->save();
        return $nuevoComprobante;
    }

    public function destroy($id){
        CuentaCorriente::destroy($id);
        Session::flash('flash_message',  trans('labels.frontend.cuentacorriente').trans('alerts.frontend.deleted').'!');
        return redirect('ventas/cuenta-corriente');
    }
}
