<?php

namespace App\Http\Controllers\Frontend\Ventas;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Frontend\Configuraciones\HistorialController;
use App\Models\Frontend\Caja\EstadoCaja;
use App\Models\Frontend\Configuraciones\CondicionIva;
use App\Models\Frontend\Configuraciones\Estado;
use App\Models\Frontend\Configuraciones\Moneda;
use App\Models\Frontend\Configuraciones\Opcion;
use App\Models\Frontend\Configuraciones\PorcentajeIva;
use App\Models\Frontend\Ventas\DetallePresupuesto;
use App\Models\Frontend\Ventas\DetalleVenta;
use App\Models\Frontend\Ventas\ListaPrecio;
use App\Models\Frontend\Ventas\Presupuesto;
use App\Models\Frontend\Configuraciones\TipoComprobante;
use App\Models\Frontend\Ventas\Venta;
use Carbon\Carbon;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Facades\Datatables;
use Session;

class PresupuestoController extends Controller{

    public function index(Request $request){
        $estadoCaja = EstadoCaja::CERRADA;
        if(isset(Auth::user()->empleado->caja)){
            if(Auth::user()->empleado->caja->estado == EstadoCaja::ABIERTA) {
                $estadoCaja = EstadoCaja::ABIERTA;
            }
        }

        return view('frontend.ventas.presupuesto.index', compact('estadoCaja'));
    }

    public function datos(){
        $empresa = Auth::user()->empresa;
        $sucursal = Auth::user()->sucursal;

        $presupuestos = Presupuesto::listarPresupuestosPorEmpresaSucursal($empresa, $sucursal);

        $tipos_comprobantes = TipoComprobante::$comprobantes;

        return Datatables::of($presupuestos)
            ->addColumn('action', function ($presupuesto) {
                return '
                <a href="'.url('/ventas/presupuesto/' . $presupuesto->id).'" class="mytooltip"><i class="fa fa-eye text-success m-r-10"></i>
                    <span class="tooltip-content3">Ver Presupuesto</span>
                </a>
                <a href="'.url('/ventas/presupuesto/imprimir/' . $presupuesto->id).'" target="_blank" class="mytooltip">
                    <i class="fa fa-print text-purple m-r-10"></i>
                    <span class="tooltip-content3">Imprimir Presupuesto</span>
                </a>
                <a href="'.url('/ventas/presupuesto/generar-venta/' . $presupuesto->id).'" target="_blank" class="mytooltip">
                    <i class="fa fa-copy text-warning m-r-10"></i>
                    <span class="tooltip-content3">Facturar Presupuesto</span>
                </a>
                <a class="delete mytooltip pointer" data-id="'.$presupuesto->id.'">
                    <i class="fa fa-close text-danger"></i>
                    <span class="tooltip-content3">Eliminar Presupuesto</span>
                </a>
                ';
            })
            ->setRowId('id')
            ->removeColumn('id')
            ->removeColumn('tipo_comprobante_id')
            ->removeColumn('signo')
            ->editColumn('fecha', function ($presupuesto) {
                return $presupuesto->fecha->format('d/m/Y H:i');
            })
            ->editColumn('total', function ($presupuesto) {
                return $presupuesto->signo.' '.$presupuesto->total;
            })
            ->editColumn('numero', function ($presupuesto) use ($tipos_comprobantes) {
                return $tipos_comprobantes[$presupuesto->tipo_comprobante_id].' '.$presupuesto->numero;
            })
            ->make(true);
    }

    public function create(){
        if(isset(Auth::user()->empleado->caja)){
            if(Auth::user()->empleado->caja->estado != EstadoCaja::ABIERTA) {
                return redirect('ventas/presupuesto');
            }
        }else{
            return redirect('ventas/presupuesto');
        }

        $empresa = Auth::user()->empresa;

        $fecha_hoy = Carbon::now()->format('d/m/Y');
        $fecha_vencimiento = Carbon::now()->addDays(30)->format('d/m/Y');
        $tipos_comprobantes = TipoComprobante::$arrayComboPresupuestos;

        $numero_presupuesto = Opcion::getValorOpcion('numeracion_presupuesto', Auth::user()->empresa);
        if($numero_presupuesto == null){
            $opcion = new Opcion();
            $opcion->empresa_id = $empresa->id;
            $opcion->nombre = 'numeracion_presupuesto';
            $opcion->presentacion  = 'Numeración Presupuesto';
            $opcion->valor = '00000001';
            $opcion->save();

            $numero_presupuesto = $opcion->valor;
        }

        $monedas = Moneda::getMonedasPorEmpresaArray($empresa);
        $monedaPorDefecto = Moneda::where('empresa_id', $empresa->id)->whereNull('deleted_at')->where('cotizacion','=', 1)->first();
        $lista_precios = ListaPrecio::getListasPorEmpresaArray($empresa);
        $condiciones_iva = CondicionIva::getCondicionesIvaArray();

        return view('frontend.ventas.presupuesto.create', compact('fecha_hoy', 'fecha_vencimiento', 'numero_presupuesto', 'tipos_comprobantes', 'monedas', 'lista_precios', 'monedaPorDefecto','condiciones_iva'));
    }

    public function store(Request $request){
        $this->validate($request, [
            'cliente_id' => 'required',
            'moneda_id' => 'required',
            'fecha' => 'required',
            'numero' => 'required',
            'tipo_comprobante_id' => 'required',
            'total' => 'required'
        ]);

        DB::transaction(function() use ($request) {
            $requestData = $request->all();

            $empresa = Auth::user()->empresa;
            $sucursal = Auth::user()->sucursal;

            $opcion = Opcion::where('empresa_id', Auth::user()->empresa_id)
                ->where('nombre', 'LIKE', 'numeracion_presupuesto')
                ->first();
            $numero_presupuesto = $opcion->valor;
            $opcion->valor = str_pad($opcion->valor + 1, 8, '0', STR_PAD_LEFT);
            $opcion->save();

            $presupuesto = New Presupuesto($requestData);
            $date = DateTime::createFromFormat('d/m/Y', $requestData['fecha']);
            $presupuesto->fecha = $date->format('Y-m-d H:i:s');
            if(isset($requestData['fecha_vencimiento'])){
                $date2 = DateTime::createFromFormat('d/m/Y', $requestData['fecha_vencimiento']);
                $presupuesto->fecha_vencimiento = $date2->format('Y-m-d H:i:s');
            }
            $presupuesto->estado = Estado::ACTIVO;
            $presupuesto->empresa_id = $empresa->id;
            $presupuesto->sucursal_id = $sucursal->id;
            $presupuesto->tipo_comprobante_id = $requestData['tipo_comprobante_id'];
            $presupuesto->numero = $numero_presupuesto;
            if($presupuesto->importe_neto == null)
                $presupuesto->importe_neto = 0;
            if($presupuesto->importe_iva == null)
                $presupuesto->importe_iva = 0;
            $presupuesto->save();

            $presupuesto->moneda->cotizacion = $requestData['cotizacion'];
            $presupuesto->moneda->save();

            $porcentajes_iva = PorcentajeIva::$valores;
            if(isset($requestData['detalles'])) {
                $detalles = $requestData['detalles'];
                foreach ($detalles as $detalle) {
                    if ((int)$detalle['estado'] == Estado::ACTIVO && isset($detalle['cantidad']) && isset($detalle['articulo_id'])) {
                        $nuevoDetalle = New DetallePresupuesto($detalle);
                        $nuevoDetalle->presupuesto_id = $presupuesto->id;
                        $nuevoDetalle->precio_iva = $detalle['precio_neto'] * ($porcentajes_iva[$detalle['porcentaje_iva']] / 100);
                        $nuevoDetalle->save();
                    }
                }
            }

            $controllerHistorial = new HistorialController();
            $controllerHistorial->nuevoHistorialCreacion($presupuesto, $request);

            Session::flash('flash_success', 'Presupuesto creado correctamente.');
        });

        return redirect('ventas/presupuesto');
    }

    public function show($id){
        $presupuesto = Presupuesto::findOrFail($id);

        if ($presupuesto->empresa_id == Auth::user()->empresa_id) {
            $porcentajes_iva = PorcentajeIva::$valores;
            $tipos_comprobantes = TipoComprobante::$comprobantes;
            if (Auth::user()->hasRole('Usuario Vendedor')) {
                if ($presupuesto->sucursal_id == Auth::user()->sucursal_id) {
                    return view('frontend.ventas.presupuesto.show', compact('presupuesto', 'tipos_comprobantes', 'porcentajes_iva'));
                }
            } else {
                return view('frontend.ventas.presupuesto.show', compact('presupuesto', 'tipos_comprobantes', 'porcentajes_iva'));
            }
        }

        return redirect('ventas/presupuesto');
    }

    public function edit($id){
        return redirect('ventas/presupuesto');

        $presupuesto = Presupuesto::findOrFail($id);
        $editar = TRUE;
        $empresa = Auth::user()->empresa;

        if ($presupuesto->empresa == $empresa) {
            $porcentajes_iva = PorcentajeIva::$valores;
            $tipos_comprobantes = TipoComprobante::$arrayComboPresupuestos;
            $monedas = Moneda::getMonedasPorEmpresaArray($empresa);
            $lista_precios = ListaPrecio::getListasPorEmpresaArray($empresa);
            $condiciones_iva = CondicionIva::getCondicionesIvaArray();

            if (Auth::user()->hasRole('Usuario Vendedor')) {
                $sucursal = Auth::user()->sucursal;
                if ($presupuesto->sucursal_id == $sucursal->id) {
                    return view('frontend.ventas.presupuesto.edit', compact('presupuesto', 'porcentajes_iva', 'tipos_comprobantes', 'monedas', 'lista_precios', 'editar','condiciones_iva'));
                }
            } else {
                return view('frontend.ventas.presupuesto.edit', compact('presupuesto', 'porcentajes_iva', 'tipos_comprobantes', 'monedas', 'lista_precios', 'editar','condiciones_iva'));
            }
        }

        return redirect('ventas/presupuesto');
    }

    public function update($id, Request $request){
        return redirect('ventas/presupuesto');
    }

    public function destroy($id){
        $empresa = Auth::user()->empresa;
        $presupuesto = Presupuesto::findOrFail($id);

        if ($presupuesto->empresa == $empresa) {
            if(!$presupuesto->detalles->isEmpty()){
                foreach($presupuesto->detalles as $detalle){
                    $detalle->delete();
                }
            }

            $presupuesto->delete();

            Session::flash('flash_success', 'Presupuesto eliminado correctamente.');
        }

        return redirect('ventas/presupuesto');
    }

    public function generarVenta($id){
        $empresa = Auth::user()->empresa;
        $presupuesto = Presupuesto::findOrFail($id);

        if ($presupuesto->empresa == $empresa) {
            $nuevaVenta = new Venta();
            $nuevaVenta->cliente = $presupuesto->cliente;
            $nuevaVenta->cliente_id = $presupuesto->cliente_id;
            $nuevaVenta->listaPrecios = $presupuesto->listaPrecios;
            $nuevaVenta->lista_precios_id = $presupuesto->lista_precios_id;
            $nuevaVenta->fecha = null;
            $nuevaVenta->moneda = $presupuesto->moneda;
            $nuevaVenta->moneda_id = $presupuesto->moneda->id;
            $nuevaVenta->cotizacion = $presupuesto->cotizacion;

            $detallesVenta = null;
            $aux = 1;
            foreach ($presupuesto->detalles as $detalle){
                $nuevoDetalle = new DetalleVenta();
                $nuevoDetalle->id = $aux*-1;
                $nuevoDetalle->articulo = $detalle->articulo;
                $nuevoDetalle->articulo_id = $detalle->articulo_id;
                $nuevoDetalle->precio_neto = $detalle->precio_neto;
                $nuevoDetalle->porcentaje_iva = $detalle->porcentaje_iva;
                $nuevoDetalle->cantidad = $detalle->cantidad;
                $nuevoDetalle->descripcion = $detalle->descripcion;
                $nuevoDetalle->estado = $detalle->estado;
                $detallesVenta[$aux*-1] = $nuevoDetalle;
                $aux++;
            }
            $nuevaVenta->detalles = $detallesVenta;

            $ventaController = new VentaController();

            return $ventaController->create($nuevaVenta);
        }
    }

    public function imprimir($presupuesto_id){
        $empresa = Auth::user()->empresa;
        $presupuesto = Presupuesto::findOrFail($presupuesto_id);

        if ($presupuesto->empresa_id == $empresa->id) {
            $tipos_comprobantes = TipoComprobante::$comprobantes;
            $porcentajes_iva = PorcentajeIva::$valores;

            $pdf = app('dompdf.wrapper');
            $pdf->getDomPDF()->set_option("enable_php", true);
            $pdf->loadView('frontend.ventas.presupuesto.imprimir-presupuesto',
                compact('presupuesto', 'empresa', 'tipos_comprobantes', 'porcentajes_iva'));

            if (Auth::user()->hasRole('Usuario Vendedor')) {
                if ($presupuesto->sucursal_id == Auth::user()->sucursal_id) {
                    return $pdf->stream('Presupuesto.pdf');
                }
            } else {
                return $pdf->stream('Presupuesto.pdf');
            }

        }
    }

}
