<?php

namespace App\Http\Controllers\Frontend\Ventas;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Frontend\Caja\MovimientoCajaController;
use App\Http\Controllers\Frontend\Calendario\EventsController;
use App\Http\Controllers\Frontend\Configuraciones\HistorialController;
use App\Http\Controllers\Frontend\Fondos\ChequeController;
use App\Models\Access\User\User;
use App\Models\Frontend\Articulos\Articulo;
use App\Models\Frontend\Caja\CierreCaja;
use App\Models\Frontend\Caja\ConceptoDeCaja;
use App\Models\Frontend\Caja\EstadoCaja;
use App\Models\Frontend\Caja\FormaDePago;
use App\Models\Frontend\Caja\MovimientoCaja;
use App\Models\Frontend\Configuraciones\CondicionIva;
use App\Models\Frontend\Configuraciones\EntidadRelacionadaLink;
use App\Models\Frontend\Configuraciones\Opcion;
use App\Models\Frontend\Configuraciones\PorcentajeIva;
use App\Models\Frontend\Configuraciones\PuntoDeVenta;
use App\Models\Frontend\Configuraciones\Tarjeta;
use App\Models\Frontend\Configuraciones\TipoComprobante;
use App\Models\Frontend\Configuraciones\TipoCuentaCorriente;
use App\Models\Frontend\Fondos\Banco;
use App\Models\Frontend\Fondos\Cheque;
use App\Models\Frontend\Fondos\ChequePropio;
use App\Models\Frontend\Fondos\CuentaBancaria;
use App\Models\Frontend\Fondos\MovimientoBancario;
use App\Models\Frontend\Fondos\TipoMovimiento;
use App\Models\Frontend\Ventas\CuentaCorriente;
use App\Models\Frontend\Ventas\CuponTarjeta;
use App\Models\Frontend\Ventas\DetalleCobroVenta;
use App\Models\Frontend\Ventas\DetalleVenta;
use App\Models\Frontend\Configuraciones\Estado;
use App\Models\Frontend\Configuraciones\Moneda;
use App\Models\Frontend\Personas\Cliente;
use App\Models\Frontend\Ventas\EstadoFacturacion;
use App\Models\Frontend\Ventas\ListaPrecio;
use App\Models\Frontend\Ventas\Venta;
use Milon\Barcode\DNS1D;
use Yajra\Datatables\Facades\Datatables;
use DateTime;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use Session;

class VentaController extends Controller{

    public function index(){
        $estadoCaja = EstadoCaja::CERRADA;
        if(isset(Auth::user()->empleado->caja)){
            if(Auth::user()->empleado->caja->estado == EstadoCaja::ABIERTA) {
                $estadoCaja = EstadoCaja::ABIERTA;
            }
        }

        return view('frontend.ventas.venta.index',compact('estadoCaja'));
    }

    public function datos(){
        $empresa = Auth::user()->empresa;
        $sucursal = Auth::user()->sucursal;

        $ventas = Venta::listarVentasPorEmpresaSucursal($empresa, $sucursal, null, null);

        $tipos_comprobantes = TipoComprobante::$comprobantes;

        return Datatables::of($ventas)
            ->addColumn('action', function ($venta) {
                $return = '
                <a href="'.url('/ventas/venta/' . $venta->id).'" class="mytooltip"><i class="fa fa-eye text-success m-r-10"></i>
                    <span class="tooltip-content3">Ver Detalles Venta</span>
                </a>';
                if($venta->tipo_punto_id != 1 && $venta->estado != Estado::GUARDADO){
                    if (!isset($venta->estado_facturacion) || $venta->estado_facturacion == EstadoFacturacion::FACTURADO) {
                        $return .= '
                        <a href="' . url('/ventas/venta/imprimir/' . $venta->id) . '" class="mytooltip">
                            <i class="fa fa-print text-purple m-r-10"></i>
                            <span class="tooltip-content3">Imprimir Comprobante</span>
                        </a>';
                    }
                }
                if($venta->estado == Estado::GUARDADO){
                    $return.='
                        <a class="mytooltip" href="'.url('/ventas/venta/'.$venta->id. '/edit').'">
                            <i class="fa fa-mail-forward text-primary m-r-10"></i>
                            <span class="tooltip-content3">Continuar Venta Guardada</span>
                        </a>';
                }
                if($venta->estado_facturacion == EstadoFacturacion::ERROR || $venta->estado_facturacion == EstadoFacturacion::PENDIENTE){
                    if($venta->tipo_punto_id == 1 && Auth::user()->controladoraFiscal()){ // Si es una controladora fiscal y el usuario la tiene
                        $return .='
                        <a href="'.url('/ventas/facturacion-fiscal/facturarVenta/' . $venta->id).'" class="mytooltip">
                            <i class="fa fa-ticket text-green m-r-10"></i>
                            <span class="tooltip-content3">Facturar</span>
                        </a>';
                    } elseif($venta->tipo_punto_id == 3) {  // Si es una factura electronica
                        $return .='
                        <a href="'.url('/ventas/facturacion-electronica/facturarVenta/' . $venta->id).'" class="mytooltip">
                            <i class="fa fa-ticket text-green m-r-10"></i>
                            <span class="tooltip-content3">Facturar</span>
                        </a>';
                    }
                }
                if($venta->estado_facturacion != EstadoFacturacion::FACTURADO){
                    $return .='
                        <a class="delete mytooltip pointer" data-id="'.$venta->id.'">
                            <i class="fa fa-close text-danger"></i>
                            <span class="tooltip-content3">Eliminar Venta</span>
                        </a>';
                }
                return $return;
            })
            ->setRowId('id')
            ->removeColumn('id')
            ->removeColumn('estado_facturacion')
            ->removeColumn('estado')
            ->removeColumn('tipo_punto_id')
            ->removeColumn('tipo_comprobante_id')
            ->removeColumn('signo')
            ->editColumn('fecha', function ($venta) {
                return $venta->fecha->format('d/m/Y H:i');
            })
            ->editColumn('total', function ($venta) {
                return $venta->signo.' '.$venta->total;
            })
            ->editColumn('numero', function ($venta) use ($tipos_comprobantes) {
                return $tipos_comprobantes[$venta->tipo_comprobante_id].' '.$venta->numero;
            })
            ->setRowClass(function ($venta) {
                if($venta->estado_facturacion == EstadoFacturacion::ERROR){
                    return 'table-danger';
                } elseif($venta->estado_facturacion == EstadoFacturacion::PENDIENTE){
                    return 'table-warning';
                } elseif($venta->estado == Estado::GUARDADO){
                    return 'table-info';
                }
            })
            ->make(true);
    }

    public function create($venta = null){
        if(isset(Auth::user()->empleado->caja)){
            if(Auth::user()->empleado->caja->estado != EstadoCaja::ABIERTA) {
                return redirect('ventas/venta');
            }
        }else{
            Session::flash('flash_message', 'Usted no posee una caja asignada por defecto!');
            return redirect('ventas/venta');
        }

        $empresa = Auth::user()->empresa;

        $puntoDeVenta = null;
        if(isset(Auth::user()->empleado->puntoVenta) && Auth::user()->empleado->puntoVenta->tipo->id == 1 || isset(Auth::user()->empleado->puntoVenta) && Auth::user()->empleado->puntoVenta->tipo->id == 3){ // Si el usuario tiene asignado un punto de venta fiscal, lo especifico en la buscqueda en opciones
            $puntoDeVenta = Auth::user()->empleado->punto_venta_id;
        }

        if($empresa->condicion_iva_id == 1){//Monotributista
            $numero_factura = Opcion::getValorOpcion('numeracion_factura_c', Auth::user()->empresa, $puntoDeVenta);
            $tipo_comprobante_por_defecto = TipoComprobante::FACTURA_C;
        }else{//RI
            $numero_factura = Opcion::getValorOpcion('numeracion_factura_b', Auth::user()->empresa, $puntoDeVenta);
            $tipo_comprobante_por_defecto = TipoComprobante::FACTURA_B;
        }

        $punto_de_venta_por_defecto = null;
        if(isset(Auth::user()->empleado->puntoVenta)){
            $punto_de_venta_por_defecto = Auth::user()->empleado->puntoVenta;
        }

        $tipos_comprobantes = TipoComprobante::getArrayComprobantes($empresa,'Factura');
        $monedas = Moneda::getMonedasPorEmpresaArray($empresa);
        $porcentajes_iva = PorcentajeIva::$valores;
        $monedaPorDefecto = Moneda::where('empresa_id', $empresa->id)->whereNull('deleted_at')->where('cotizacion','=', 1)->first();
        $clientePorDefecto = Cliente::where('empresa_id', $empresa->id)->whereNull('deleted_at')->where('razon_social','=', 'Consumidor Final')->first();

        $lista_precios = ListaPrecio::getListasPorEmpresaArray($empresa);
        $lista_precios_defecto = Opcion::where('empresa_id', $empresa->id)
            ->where('nombre', 'lista_precios_defecto_id')
            ->first();

        $vendedores = User::getUsuariosPorEmpresaArray($empresa,true);

        $formas_cobro = FormaDePago::$getArrayVenta;
        $bancos = Banco::getBancosPorEmpresa($empresa)->pluck('nombre', 'id')->all();
        $tarjetas = Tarjeta::getTarjetasPorEmpresaArray($empresa);
        $tipos_cuenta_corriente = TipoCuentaCorriente::getTiposArray($empresa);
        $cuentas_bancarias = CuentaBancaria::getCuentasBancariasPorEmpresaArray($empresa);
        $condiciones_iva = CondicionIva::getCondicionesIvaArray();

        if (Auth::user()->hasRole('Usuario Vendedor')) {
            $puntos_de_ventas = PuntoDeVenta::where('sucursal_id', Auth::user()->sucursal_id)->get()->pluck('numero', 'id')->toArray();
        }else{
            $puntos_de_ventas = PuntoDeVenta::where('empresa_id', Auth::user()->empresa_id)->get()->pluck('numero', 'id')->toArray();
        }

        return view('frontend.ventas.venta.create', compact('venta','numero_factura', 'tipo_comprobante_por_defecto', 'tipos_comprobantes', 'monedas', 'punto_de_venta_por_defecto', 'monedaPorDefecto', 'clientePorDefecto', 'lista_precios', 'formas_cobro', 'bancos', 'tarjetas', 'cuentas_bancarias', 'tipos_cuenta_corriente', 'puntos_de_ventas', 'porcentajes_iva','vendedores','lista_precios_defecto','condiciones_iva'));
    }

    public function store(Request $request){
      /*  $calculos = New CalculosVistas($request);
        dd($calculos);*/
        $this->validate($request, [
			'cliente_id' => 'required',
			'moneda_id' => 'required',
			'fecha' => 'required',
			'numero' => 'required',
			'tipo_comprobante_id' => 'required',
			'punto_venta_id' => 'required',
			'total' => 'required'
		]);

        DB::transaction(function() use ($request, &$venta) {
            $requestData = $request->all();
            $empresa = Auth::user()->empresa;
            $sucursal = Auth::user()->sucursal;

            $nombresClaves = [
                TipoComprobante::FACTURA_A => 'numeracion_factura_a',
                TipoComprobante::FACTURA_B => 'numeracion_factura_b',
                TipoComprobante::FACTURA_C => 'numeracion_factura_c',
                TipoComprobante::TIQUE_FACTURA_A => 'numeracion_factura_a',
                TipoComprobante::TIQUE_FACTURA_B => 'numeracion_factura_b',
                TipoComprobante::TIQUE_FACTURA_C => 'numeracion_factura_c'
            ];

            $venta = New Venta($requestData);

            if(!$request->guardar){
                if($venta->puntoVenta->esBlanco()){ // Si es en blanco busco la numeracion segun el punto de venta
                    $opcion = Opcion::where('empresa_id', $empresa->id)
                        ->where('nombre', 'LIKE', $nombresClaves[(int)$requestData['tipo_comprobante_id']])
                        ->where('punto_venta_id', $venta->puntoVenta->id)
                        ->first();
                    $numero_factura = $opcion->valor;
                    $opcion->valor = str_pad($opcion->valor + 1, 8, '0', STR_PAD_LEFT);
                    $opcion->save();

                    $venta->estado_facturacion = EstadoFacturacion::PENDIENTE; // Seteo el comprobante en pendiente por defecto
                }else{ //si es Manual busco la numeración genérica
                    $opcion = Opcion::where('empresa_id', $empresa->id)
                        ->where('nombre', 'LIKE', $nombresClaves[(int)$requestData['tipo_comprobante_id']])
                        ->first();
                    $numero_factura = $opcion->valor;
                    $opcion->valor = str_pad($opcion->valor + 1, 8, '0', STR_PAD_LEFT);
                    $opcion->save();
                }
                $venta->numero = $numero_factura;
            }

            $date = date("Y-m-d H:i:s");
            $venta->fecha = $date;
            if($request->guardar){
                $venta->estado = Estado::GUARDADO;
            } else {
                $venta->estado = Estado::ACTIVO;
            }
            $venta->empresa_id = $empresa->id;
            $venta->sucursal_id = $sucursal->id;
            $venta->tipo_comprobante_id = $requestData['tipo_comprobante_id'];
            if(!isset($requestData['usuario_id'])){
                $venta->usuario_id = Auth::user()->id;
            }

            if($venta->importe_neto == null)
                $venta->importe_neto = 0;
            if($venta->importe_iva == null)
                $venta->importe_iva = 0;
            $venta->save();

            $entidadRelacionadaLink = EntidadRelacionadaLink::crearNueva($venta, $request, null, 'Venta Nº '.$venta->numero);

            $moneda = Moneda::findOrFail($venta->moneda->id);
            $moneda->cotizacion = $requestData['cotizacion'];
            $moneda->save();

            $porcentajes_iva = PorcentajeIva::$valores;
            if(isset($requestData['detalles'])) {
                $detalles = $requestData['detalles'];
                foreach ($detalles as $detalle) {
                    if ((int)$detalle['estado'] == Estado::ACTIVO && isset($detalle['cantidad']) && isset($detalle['articulo_id'])) {
                        $nuevoDetalle = New DetalleVenta($detalle);
                        $nuevoDetalle->venta_id = $venta->id;
                        $nuevoDetalle->precio_iva = $detalle['precio_neto'] * ($porcentajes_iva[$detalle['porcentaje_iva']] / 100);
                        $nuevoDetalle->save();

                        $nuevoDetalle->articulo->descontarStock($nuevoDetalle->cantidad,$sucursal);
                    }
                }
            }

            if(isset($requestData['detalles_cobro'])) {
                $detallesCobro = $requestData['detalles_cobro'];
                $controllerMovimientoCaja = new MovimientoCajaController();
                foreach ($detallesCobro as $cobro) {
                    if ((int)$cobro['estado'] == Estado::ACTIVO) {
                        $nuevoCobro = New DetalleCobroVenta($cobro);
                        $nuevoCobro->fecha = $date;
                        $nuevoCobro->venta_id = $venta->id;
                        $nuevoCobro->estado = Estado::ACTIVO;

                        $monto = (float)$cobro['monto'] + (float)$cobro['interes'];
                        if(!$request->guardar)
                            $controllerMovimientoCaja->nuevaEntrada($venta, 'ventas/venta', (int)$cobro['forma_cobro'], ConceptoDeCaja::VENTA, $monto);
                        if((int)$cobro['forma_cobro'] == FormaDePago::EFECTIVO) {
                            $nuevoCobro->descripcion = 'Cobro en Efectivo';
                            $nuevoCobro->forma_cobro = FormaDePago::EFECTIVO;
                        }else if((int)$cobro['forma_cobro'] == FormaDePago::MERCADO_PAGO) {
                            $nuevoCobro->descripcion = 'Cobro en Mercado Pago';
                            $nuevoCobro->forma_cobro = FormaDePago::MERCADO_PAGO;
                        }else if((int)$cobro['forma_cobro'] == FormaDePago::CHEQUE) {
                            $controllerCheque = new ChequeController();
                            $nuevoCheque = $controllerCheque->nuevoCheque($cobro, (float)$cobro['monto']);

                            $nuevoCobro->entidad_id = $nuevoCheque->id;
                            $nuevoCobro->descripcion = 'Cobro con Cheque';
                            $nuevoCobro->forma_cobro = FormaDePago::CHEQUE;
                        }else if((int)$cobro['forma_cobro'] == FormaDePago::CUENTA_BANCARIA) {
                            $nuevoMovimientoBancario = new MovimientoBancario($cobro);
                            $nuevoMovimientoBancario->importe = (float)$cobro['monto'];
                            $nuevoMovimientoBancario->cuenta_bancaria_destino_id = $cobro['cuenta_bancaria_id'];
                            $nuevoMovimientoBancario->moneda_id = $venta->moneda_id;
                            $nuevoMovimientoBancario->tipo = TipoMovimiento::DEPOSITO;
                            $nuevoMovimientoBancario->fecha = $date;
                            $nuevoMovimientoBancario->empresa_id = $empresa->id;
                            $nuevoMovimientoBancario->entidad_id = $venta->id;
                            $nuevoMovimientoBancario->entidad_clase = class_basename($venta);
                            $nuevoMovimientoBancario->url = 'ventas/venta';
                            $nuevoMovimientoBancario->save();

                            $nuevoCobro->entidad_id = $nuevoMovimientoBancario->id;
                            $nuevoCobro->descripcion = 'Cobro con Depósito Bancario';
                            $nuevoCobro->forma_cobro = FormaDePago::CUENTA_BANCARIA;
                        }else if((int)$cobro['forma_cobro'] == FormaDePago::TARJETA) {
                            $nuevoCuponTarjeta = new CuponTarjeta($cobro);
                            $nuevoCuponTarjeta->fecha = $date;
                            $nuevoCuponTarjeta->entidad_relacionada_link_id = $entidadRelacionadaLink->id;
                            $nuevoCuponTarjeta->importe = (float)$cobro['monto'] + (float)$cobro['interes'];
                            $nuevoCuponTarjeta->autorizacion = 'Autorizada';
                            $nuevoCuponTarjeta->estado = Estado::PENDIENTE;
                            $nuevoCuponTarjeta->save();

                            if($nuevoCuponTarjeta->planTarjeta->dias_acreditacion){
                                $evento = New EventsController();
                                $evento->crearEvento(Carbon::now()->addDays($nuevoCuponTarjeta->planTarjeta->dias_acreditacion),'Acreditación Cupon Nº '.$nuevoCuponTarjeta->lote.' / '.$nuevoCuponTarjeta->cupon);
                            }

                            $nuevoCobro->entidad_id = $nuevoCuponTarjeta->id;
                            $nuevoCobro->descripcion = 'Cobro con Tarjeta';
                            $nuevoCobro->forma_cobro = FormaDePago::TARJETA;
                        }else if((int)$cobro['forma_cobro'] == FormaDePago::CUENTA_CORRIENTE) {
                            $controllerCuentaCorriente = new CuentaCorrienteController();
                            $controllerCuentaCorriente->debe($venta, $request, $cobro['tipo_cuenta_corriente_id'],$cobro['monto'] + $cobro['interes']);

                            $nuevoCobro->descripcion = 'A Cuenta Corriente';
                            $nuevoCobro->forma_cobro = FormaDePago::CUENTA_CORRIENTE;
                        }
                        $nuevoCobro->save();
                    }
                }
            }

            $controllerHistorial = new HistorialController();
            $controllerHistorial->nuevoHistorialCreacion($venta, $request);

            Session::flash('flash_success', 'Venta creada correctamente.');
        });

        $porcentajes_iva = PorcentajeIva::$valores;

        if(!$request->guardar){
            if($venta->puntoVenta->tipo->nombre == "Impresora Fiscal"){
                if(Auth::user()->controladoraFiscal()){
                    $facturar = New FacturacionFiscal();
                    return $facturar->facturarVenta($venta->id);
                } else {
                    return redirect('ventas/venta/create');
                }
            } elseif($venta->puntoVenta->tipo->nombre == "Comandera"){
                $empresa = Auth::user()->empresa;

                return view('frontend.ventas.venta.imprimir-ticket', compact('venta','empresa','porcentajes_iva'));

            } elseif($venta->puntoVenta->tipo->nombre == "Factura Electrónica"){

                $facturar = New FacturacionElectronica();
                return $facturar->facturarVenta($venta->id);

            } else { // Punto de venta de facturacion manual
                return redirect('ventas/venta/create');
            }
        } else {
            return redirect('ventas/venta/create');
        }

    }

    public function show($id){
        $venta = Venta::findOrFail($id);
        $empresa = Auth::user()->empresa;

        if ($venta->empresa_id == $empresa->id) {
            $porcentajes_iva = PorcentajeIva::$valores;
            $tipos_comprobantes = TipoComprobante::$comprobantes;
            if($venta->cae){
                $cuit = str_replace('-', '', $empresa->cuit);
                $tipoComprobante = $venta->tipo_comprobante_id;
                $puntoVenta = $venta->puntoVenta->numero;
                $cae = $venta->cae;
                $fechaVencimiento = $venta->fecha_vencimiento->format('Ymd');
                $numero = $cuit . $tipoComprobante. $puntoVenta . $cae . $fechaVencimiento;
                $digitoVerificador = $this->digitoVerificador($numero);
                $numeroCodigoBarras = $numero.$digitoVerificador;
                $codigoBarras = DNS1D::getBarcodeHTML($numeroCodigoBarras, "UPCA",3,50);
            } else {
                $codigoBarras = NULL;
                $numeroCodigoBarras = NULL;
                $cae = NULL;
            }
            if (Auth::user()->hasRole('Usuario Vendedor')) {
                if ($venta->sucursal_id == Auth::user()->sucursal_id) {
                    foreach ($venta->detallesCobros as $detalleCobro){
                        if($detalleCobro->forma_cobro != FormaDePago::EFECTIVO && $detalleCobro->forma_cobro != FormaDePago::CUENTA_CORRIENTE){
                            $detalleCobro->entidad = $this->buscarEntidadCobro($detalleCobro->entidad_id, $detalleCobro->forma_cobro);
                        }
                    }
                    return view('frontend.ventas.venta.show', compact('venta', 'porcentajes_iva','empresa','codigoBarras','numeroCodigoBarras','cae'));
                }
            } else {
                return view('frontend.ventas.venta.show', compact('venta', 'porcentajes_iva','empresa','codigoBarras','numeroCodigoBarras','cae'));
            }
        }
        return redirect('ventas/venta');
    }

    public function edit($id){
        $venta = Venta::findOrFail($id);
        $editar = TRUE;
        $empresa = Auth::user()->empresa;

        if ($venta->empresa == $empresa) {
            $porcentajes_iva = PorcentajeIva::$valores;
            $tipos_comprobantes = TipoComprobante::$arrayComboFacturas;
            $monedas = Moneda::getMonedasPorEmpresaArray($empresa);
            $lista_precios = ListaPrecio::getListasPorEmpresaArray($empresa);

            $formas_cobro = FormaDePago::$getArrayVenta;
            $bancos = Banco::getBancosPorEmpresaArray($empresa);
            $tarjetas = Tarjeta::getTarjetasPorEmpresaArray($empresa);
            $tipos_cuenta_corriente = TipoCuentaCorriente::getTiposArray($empresa);
            $cuentas_bancarias = CuentaBancaria::getCuentasBancariasPorEmpresaArray($empresa);
            $condiciones_iva = CondicionIva::getCondicionesIvaArray();

            $vendedores = User::getUsuariosPorEmpresaArray($empresa,true);

            if (Auth::user()->hasRole('Usuario Vendedor')) {
                $sucursal = Auth::user()->sucursal;
                if ($venta->sucursal_id == $sucursal->id) {
                    $puntos_de_ventas = PuntoDeVenta::where('sucursal_id', Auth::user()->sucursal_id)->get()->pluck('numero', 'id')->toArray();

                    return view('frontend.ventas.venta.edit', compact('venta', 'monedas', 'tipos_comprobantes', 'puntos_de_ventas', 'lista_precios', 'editar', 'formas_cobro', 'bancos', 'tarjetas', 'cuentas_bancarias', 'porcentajes_iva','tipos_cuenta_corriente','vendedores','condiciones_iva'));
                }
            } else {
                $puntos_de_ventas = PuntoDeVenta::where('empresa_id', Auth::user()->empresa_id)->get()->pluck('numero', 'id')->toArray();

                return view('frontend.ventas.venta.edit', compact('venta', 'monedas', 'tipos_comprobantes', 'puntos_de_ventas', 'lista_precios', 'editar', 'formas_cobro', 'bancos', 'tarjetas', 'cuentas_bancarias', 'porcentajes_iva','tipos_cuenta_corriente','vendedores','condiciones_iva'));
            }
        }
        return redirect('ventas/venta');
    }

    public function update($id, Request $request){
        $this->validate($request, [
            'total' => 'required',
            'numero' => 'required',
            'punto_venta_id' => 'required'
		]);
        $venta = Venta::findOrFail($id);

        $empresa = Auth::user()->empresa;
        $sucursal = Auth::user()->sucursal;
        if(!$venta->empresa == $empresa) {
            return redirect('ventas/venta');
        }else{
            if(Auth::user()->hasRole('Usuario Vendedor')) {
                if (!$venta->sucursal == $sucursal) {
                    return redirect('ventas/venta');
                }
            }
        }

        DB::transaction(function() use (&$venta, $empresa, $sucursal, $request) {
            $requestData = $request->all();

            $nombresClaves = [
                TipoComprobante::FACTURA_A => 'numeracion_factura_a',
                TipoComprobante::FACTURA_B => 'numeracion_factura_b',
                TipoComprobante::FACTURA_C => 'numeracion_factura_c',
                TipoComprobante::TIQUE_FACTURA_A => 'numeracion_factura_a',
                TipoComprobante::TIQUE_FACTURA_B => 'numeracion_factura_b',
                TipoComprobante::TIQUE_FACTURA_C => 'numeracion_factura_c'
            ];

            $date = date("Y-m-d H:i:s");
            $venta->importe_neto = $requestData['importe_neto'];
            $venta->importe_iva = $requestData['importe_iva'];
            $venta->importe_impuesto_interno = $requestData['importe_impuesto_interno'];
            $venta->importe_descuento = $requestData['importe_descuento'];
            $venta->porcentaje_descuento = $requestData['porcentaje_descuento'];
            $venta->total = $requestData['total'];
            $venta->descripcion = $requestData['descripcion'];
            $venta->punto_venta_id = $requestData['punto_venta_id'];
            $venta->numero = $requestData['numero'];
            $venta->cliente_id = $requestData['cliente_id'];
            $venta->tipo_comprobante_id = $requestData['tipo_comprobante_id'];
            $venta->lista_precios_id = $requestData['lista_precios_id'];
            if(isset($requestData['usuario_id'])){
                $venta->usuario_id = $requestData['usuario_id'];
            } else {
                $venta->usuario_id = Auth::user()->id;
            }
            if($request->guardar){
                $venta->estado = Estado::GUARDADO;
            } else {
                $venta->estado = Estado::ACTIVO;

                if($venta->puntoVenta->esBlanco()){ // Si es en blanco busco la numeracion segun el punto de venta
                    $opcion = Opcion::where('empresa_id', $empresa->id)
                        ->where('nombre', 'LIKE', $nombresClaves[(int)$requestData['tipo_comprobante_id']])
                        ->where('punto_venta_id', $venta->puntoVenta->id)
                        ->first();
                    $numero_factura = $opcion->valor;
                    $opcion->valor = str_pad($opcion->valor + 1, 8, '0', STR_PAD_LEFT);
                    $opcion->save();

                    $venta->estado_facturacion = EstadoFacturacion::PENDIENTE; // Seteo el comprobante en pendiente por defecto
                }else{ //si es Manual busco la numeración genérica
                    $opcion = Opcion::where('empresa_id', $empresa->id)
                        ->where('nombre', 'LIKE', $nombresClaves[(int)$requestData['tipo_comprobante_id']])
                        ->first();
                    $numero_factura = $opcion->valor;
                    $opcion->valor = str_pad($opcion->valor + 1, 8, '0', STR_PAD_LEFT);
                    $opcion->save();
                }
                $venta->numero = $numero_factura;
            }
            $venta->update();

            $entidadRelacionadaLink = EntidadRelacionadaLink::buscar($venta);
            if($entidadRelacionadaLink == null){
                $entidadRelacionadaLink = EntidadRelacionadaLink::crearNueva($venta, $request, null, 'Venta Nº '.$venta->numero);
            }

            $porcentajes_iva = PorcentajeIva::$valores;
            if(isset($requestData['detalles'])){
                $detalles = $requestData['detalles'];
                foreach ($detalles as $detalle) {
                    if((int)$detalle['id'] > 0){//Editar detalles
                        $detalleGuardado = DetalleVenta::findOrFail((int)$detalle['id']);
                        if((int)$detalle['estado'] == Estado::ACTIVO && isset($detalle['cantidad']) && isset($detalle['articulo_id'])){
                            if(((int)$detalleGuardado->cantidad != (int)$detalle['cantidad']) or ($detalleGuardado->articulo_id != (int)$detalle['articulo_id'])){
                                //Aumento el stock de los articulos no vendidos
                                $stockArticuloDetalleGuardado = $detalleGuardado->articulo->stockPorSucursal($sucursal);
                                if(isset($stockArticuloDetalleGuardado)){
                                    $stockArticuloDetalleGuardado->cantidad += $detalleGuardado->cantidad;
                                    $stockArticuloDetalleGuardado->save();
                                }

                                $detalleGuardado->cantidad = $detalle['cantidad'];
                                $detalleGuardado->articulo_id = $detalle['articulo_id'];
                                $articuloNuevo = Articulo::findOrFail((int)$detalle['articulo_id']);

                                //Resto la nueva cantidad
                                $articuloNuevo->descontarStock($detalleGuardado->cantidad,$sucursal);
                            }else{
                                $detalleGuardado->cantidad = $detalle['cantidad'];
                                $detalleGuardado->articulo_id = $detalle['articulo_id'];
                            }
                            $detalleGuardado->porcentaje_iva = $detalle['porcentaje_iva'];
                            $detalleGuardado->precio_neto = $detalle['precio_neto'];
                            $detalleGuardado->precio_iva = $detalle['precio_neto'] * ($porcentajes_iva[$detalle['porcentaje_iva']] / 100);

                            $detalleGuardado->update($detalle);
                        }else{
                            $stockArticuloDetalleGuardado = $detalleGuardado->articulo->stockPorSucursal($sucursal);
                            if(isset($stockArticuloDetalleGuardado)){
                                $stockArticuloDetalleGuardado->cantidad += $detalleGuardado->cantidad;
                                $stockArticuloDetalleGuardado->save();
                            }

                            $detalleGuardado->delete();
                        }
                    }else{//Guardar nuevos detalles
                        if((int)$detalle['estado'] == Estado::ACTIVO && isset($detalle['cantidad']) && isset($detalle['articulo_id'])){
                            $nuevoDetalle = New DetalleVenta($detalle);
                            $nuevoDetalle->venta_id = $venta->id;
							$nuevoDetalle->precio_iva = $detalle['precio_neto'] * ($porcentajes_iva[$detalle['porcentaje_iva']] / 100);
                            $nuevoDetalle->save();

                            $stockArticulo = $nuevoDetalle->articulo->stockPorSucursal($sucursal);
                            if(isset($stockArticulo)){
                                $stockArticulo->cantidad -= $nuevoDetalle->cantidad;
                                $stockArticulo->save();
                            }
                        }
                    }
                }
            }
            if(isset($requestData['detalles_cobro'])){
                $detallesCobro = $requestData['detalles_cobro'];
                $controllerMovimientoCaja = new MovimientoCajaController();
                foreach ($detallesCobro as $cobro) {
                    if((int)$cobro['id'] > 0){//Borrado de detalles
                        if((int)$cobro['estado'] == Estado::BORRADO){
                            DetalleCobroVenta::destroy((int)$cobro['id']);
                        } else {
                            if(!$request->guardar)
                                $controllerMovimientoCaja->nuevaEntrada($venta, 'ventas/venta', (int)$cobro['forma_cobro'], ConceptoDeCaja::VENTA, (float)$cobro['monto']);
                        }
                    }else{//Guardar nuevos detalles
                        if ((int)$cobro['estado'] == Estado::ACTIVO) {
                            $nuevoCobro = New DetalleCobroVenta($cobro);
                            $nuevoCobro->fecha = $date;
                            $nuevoCobro->venta_id = $venta->id;
                            $nuevoCobro->estado = Estado::ACTIVO;

                            if(!$request->guardar)
                                $controllerMovimientoCaja->nuevaEntrada($venta, 'ventas/venta', (int)$cobro['forma_cobro'], ConceptoDeCaja::VENTA, (float)$cobro['monto']);
                            if((int)$cobro['forma_cobro'] == FormaDePago::EFECTIVO) {
                                $nuevoCobro->descripcion = 'Cobro en Efectivo';
                                $nuevoCobro->forma_cobro = FormaDePago::EFECTIVO;
                            }else if((int)$cobro['forma_cobro'] == FormaDePago::MERCADO_PAGO) {
                                $nuevoCobro->descripcion = 'Cobro en Mercado Pago';
                                $nuevoCobro->forma_cobro = FormaDePago::MERCADO_PAGO;
                            }else if((int)$cobro['forma_cobro'] == FormaDePago::CHEQUE) {
                                $controllerCheque = new ChequeController();
                                $nuevoCheque = $controllerCheque->nuevoCheque($cobro, (float)$cobro['monto']);

                                $nuevoCobro->entidad_id = $nuevoCheque->id;
                                $nuevoCobro->descripcion = 'Cobro con Cheque';
                                $nuevoCobro->forma_cobro = FormaDePago::CHEQUE;
                            }else if((int)$cobro['forma_cobro'] == FormaDePago::CUENTA_BANCARIA) {
                                $nuevoMovimientoBancario = new MovimientoBancario($cobro);
                                $nuevoMovimientoBancario->importe = (float)$cobro['monto'];
                                $nuevoMovimientoBancario->cuenta_bancaria_destino_id = $cobro['cuenta_bancaria_id'];
                                $nuevoMovimientoBancario->moneda_id = $venta->moneda_id;
                                $nuevoMovimientoBancario->tipo = TipoMovimiento::DEPOSITO;
                                $nuevoMovimientoBancario->fecha = $date;
                                $nuevoMovimientoBancario->venta_id = $venta->id;
                                $nuevoMovimientoBancario->save();

                                $nuevoCobro->entidad_id = $nuevoMovimientoBancario->id;
                                $nuevoCobro->descripcion = 'Cobro con Depósito Bancario';
                                $nuevoCobro->forma_cobro = FormaDePago::CUENTA_BANCARIA;
                            }else if((int)$cobro['forma_cobro'] == FormaDePago::TARJETA) {
                                $nuevoCuponTarjeta = new CuponTarjeta($cobro);
                                $nuevoCuponTarjeta->fecha = $date;

                                $nuevoCuponTarjeta->entidad_relacionada_link_id = $entidadRelacionadaLink->id;
                                $nuevoCuponTarjeta->importe = (float)$cobro['monto'] + (float)$cobro['interes'];
                                $nuevoCuponTarjeta->autorizacion = 'Autorizada';
                                $nuevoCuponTarjeta->estado = Estado::PENDIENTE;
                                $nuevoCuponTarjeta->save();

                                $nuevoCobro->entidad_id = $nuevoCuponTarjeta->id;
                                $nuevoCobro->descripcion = 'Cobro con Tarjeta';
                                $nuevoCobro->forma_cobro = FormaDePago::TARJETA;
                            }else if((int)$cobro['forma_cobro'] == FormaDePago::CUENTA_CORRIENTE) {
                                $controllerCuentaCorriente = new CuentaCorrienteController();
                                $controllerCuentaCorriente->debe($venta, $request, null, $cobro['monto']);

                                $nuevoCobro->descripcion = 'Cobro a Cuenta Corriente';
                                $nuevoCobro->forma_cobro = FormaDePago::CUENTA_CORRIENTE;
                            }
                            $nuevoCobro->save();
                        }
                    }
                }
            }

            $controllerHistorial = new HistorialController();
            $controllerHistorial->nuevoHistorialActualizacion($venta, $request);

            Session::flash('flash_success', 'Venta modificada correctamente.');
        });

        $porcentajes_iva = PorcentajeIva::$valores;

        if(!$request->guardar){
            if($venta->puntoVenta->tipo->nombre == "Impresora Fiscal"){
                if(Auth::user()->controladoraFiscal()){
                    $facturar = New FacturacionFiscal();
                    return $facturar->facturarVenta($venta->id);
                } else {
                    return redirect('ventas/venta/create');
                }
            } elseif($venta->puntoVenta->tipo->nombre == "Comandera"){
                $empresa = Auth::user()->empresa;

                return view('frontend.ventas.venta.imprimir-ticket', compact('venta','empresa','porcentajes_iva'));

            } elseif($venta->puntoVenta->tipo->nombre == "Factura Electrónica"){

                $facturar = New FacturacionElectronica();
                return $facturar->facturarVenta($venta->id);

            } else { // Punto de venta de facturacion manual
                return redirect('ventas/venta/create');
            }
        } else {
            return redirect('ventas/venta/create');
        }
    }

    public function destroy($id){
        $venta = Venta::findOrFail($id);

        if (!$venta->empresa_id == Auth::user()->empresa_id) {
            return redirect('ventas/venta');
        }else{
            if($venta->estado_facturacion == EstadoFacturacion::FACTURADO){ // Si la venta ya fue favturada, no se puede eliminar
                return redirect('ventas/venta');
            }
            if (Auth::user()->hasRole('Usuario Vendedor')) {
                if (!$venta->sucursal_id == Auth::user()->sucursal_id) {
                    return redirect('ventas/venta');
                }
            }

        }

        DB::transaction(function() use ($venta) {
            $movimientosCaja = MovimientoCaja::where('entidad_clase', 'Venta')->where('entidad_id',$venta->id);
            $cierreCaja = CierreCaja::getCierreCajaPorMovimiento($movimientosCaja);
            if(!$cierreCaja){ // Si los movimientos no pertenecen a una caja ya cerrada los elimino
                $movimientosCaja->delete();
            }

            $sucursal = $venta->sucursal;
            if(!$venta->detalles->isEmpty() && $sucursal){
                foreach($venta->detalles as $detalle){
                    if(isset($detalle->articulo)){
                        $detalle->articulo->sumarStock($detalle->cantidad,$sucursal);
                    }
                    $detalle->delete();
                }
            }

            $venta->delete();

            $controllerHistorial = new HistorialController();
            $controllerHistorial->nuevoHistorialEliminacion($venta);

            Session::flash('flash_success', 'Venta eliminada correctamente.');
        });

        return redirect('ventas/venta');
    }

    public function imprimir($venta_id){
        $empresa = Auth::user()->empresa;
        $venta = Venta::findOrFail($venta_id);

        if ($venta->empresa_id == $empresa->id) {
            $tipos_comprobantes = TipoComprobante::$comprobantes;
            $porcentajes_iva = PorcentajeIva::$valores;
            if($venta->cae){
                $cuit = str_replace('-', '', $empresa->cuit);
                $tipoComprobante = $venta->tipo_comprobante_id;
                $puntoVenta = $venta->puntoVenta->numero;
                $cae = $venta->cae;
                $fechaVencimiento = $venta->fecha_vencimiento->format('Ymd');
                $numero = $cuit . $tipoComprobante. $puntoVenta . $cae . $fechaVencimiento;
                $digitoVerificador = $this->digitoVerificador($numero);
                $numeroCodigoBarras = $numero.$digitoVerificador;
                $codigoBarras = DNS1D::getBarcodeHTML($numeroCodigoBarras, "UPCA");
            } else {
                $codigoBarras = NULL;
                $numeroCodigoBarras = NULL;
                $cae = NULL;
            }

            $pdf = app('dompdf.wrapper');
            $pdf->getDomPDF()->set_option("enable_php", true);
            $pdf->loadView('frontend.ventas.venta.imprimir-venta',
                compact('venta','empresa','tipos_comprobantes', 'porcentajes_iva','codigoBarras','numeroCodigoBarras','cae'));
            if (Auth::user()->hasRole('Usuario Vendedor')) {
                if ($venta->sucursal_id == Auth::user()->sucursal_id) {
                    if($venta->puntoVenta->tipo->nombre == "Comandera"){
                        return view('frontend.ventas.venta.imprimir-ticket', compact('venta','empresa','tipos_comprobantes','porcentajes_iva'));
                    } else {
                        return $pdf->stream('Venta.pdf');
                    }

                }
            } else {
                if($venta->puntoVenta->tipo->nombre == "Comandera"){
                    return view('frontend.ventas.venta.imprimir-ticket', compact('venta','empresa','tipos_comprobantes','porcentajes_iva'));
                } else {
                    return $pdf->stream('Venta.pdf');
                }
            }
        }
    }

    public function setEstadoFacturacion(Request $request){
        $venta_id = $request['venta_id'];
        $venta = Venta::findOrFail($venta_id);

        $empresa = Auth::user()->empresa;

        if ($venta->empresa_id == $empresa->id) {
            if ($request['estado'] == "FACTURADO") {

                $venta->numero = str_pad($request['numeroComprobante'], 8, "0", STR_PAD_LEFT);
                $nombresClaves = [
                    TipoComprobante::FACTURA_A => 'numeracion_factura_a',
                    TipoComprobante::FACTURA_B => 'numeracion_factura_b',
                    TipoComprobante::FACTURA_C => 'numeracion_factura_c',
                    TipoComprobante::TIQUE_FACTURA_A => 'numeracion_factura_a',
                    TipoComprobante::TIQUE_FACTURA_B => 'numeracion_factura_b',
                    TipoComprobante::TIQUE_FACTURA_C => 'numeracion_factura_c'
                ];
                $opcion = Opcion::where('empresa_id', $empresa->id)
                    ->where('nombre', 'LIKE', $nombresClaves[(int)$venta->tipo_comprobante_id])
                    ->where('punto_venta_id', $venta->puntoVenta->id)
                    ->first();
                $opcion->valor = str_pad($request['numeroComprobante'] + 1, 8, "0", STR_PAD_LEFT);
                $opcion->save();

                // Actualizo el detalle del movimiento de caja que se genero junto con la venta
                $movimientosCaja = MovimientoCaja::where('entidad_clase', 'Venta')->where('entidad_id',$venta->id)->first();
                $movimientosCaja->detalle = 'Venta: '.$venta->puntoVenta->numero.'-'.str_pad($request['numeroComprobante'], 8, "0", STR_PAD_LEFT);
                $movimientosCaja->save();

                $venta->estado_facturacion = EstadoFacturacion::FACTURADO;
            } else {
                $venta->estado_facturacion = EstadoFacturacion::ERROR;

                // Si la impresion del comprobante fallo no debe impactar en la caja
                $movimientosCaja = MovimientoCaja::where('entidad_clase', 'Venta')->where('entidad_id',$venta->id);
                $movimientosCaja->delete();
            }

            if(Auth::user()->hasRole('Usuario Vendedor')){
                $sucursal = Auth::user()->sucursal;
                if ($venta->sucursal_id == $sucursal->id) {
                    $venta->save();
                }
            } else {
                $venta->save();
            }
        }
    }

    public function obtenerVenta(Request $request){
        $term = $request->term;

        $empresa = Auth::user()->empresa;
        if(Auth::user()->hasRole('Usuario Vendedor')){
            $sucursal = Auth::user()->sucursal;
            $ventas = Venta::listarVentasPorEmpresaSucursal($empresa, $sucursal, 5, $term);
        }else{
            $ventas = Venta::listarVentasPorEmpresaSucursal($empresa, null, 5, $term);
        }

        foreach ($ventas as $item) {
            $results[] = [
                'id' => $item->id,
                'value' => $item->numero." Cliente: ".$item->razon_social." Total: $".$item->total_cobro
            ];
        }
        return response()->json($results);
    }

    public function obtenerEstadoVenta(Request $request){
        $venta_id = $request->input('venta_id');
        $empresa = Auth::user()->empresa;

        $venta = Venta::where('empresa_id', $empresa->id)
            ->where('id', '=', $venta_id)
            ->first();
        if($venta == null){
            return response()->json('No existe la venta.');
        }

        $resultado = $venta->estado_facturacion;

        return response()->json($resultado);
    }

    public function buscarEntidadCobro($entidad_id, $forma_cobro){
        $entidad = null;
        if($forma_cobro == FormaDePago::CHEQUE){
            $entidad = Cheque::findOrFail($entidad_id);
            $entidad->url = '/fondos/cheque/'.$entidad_id;
        }elseif($forma_cobro == FormaDePago::CUENTA_BANCARIA){
            $entidad = CuentaCorriente::findOrFail($entidad_id);
            $entidad->url = '/ventas/cuenta-corriente/'.$entidad_id;
        }elseif($forma_cobro == FormaDePago::TARJETA){
            $entidad = CuponTarjeta::findOrFail($entidad_id);
            $entidad->url = '/ventas/cupon-tarjeta/'.$entidad_id;
        }elseif($forma_cobro == FormaDePago::CHEQUE_PROPIO){
            $entidad = ChequePropio::findOrFail($entidad_id);
            $entidad->url = '/fondos/cheque-propio/'.$entidad_id;
        }
        return $entidad;
    }

    public function listadoVentasAutomaticas(){

        $estadoCaja = EstadoCaja::CERRADA;
        if(isset(Auth::user()->empleado->caja)){
            if(Auth::user()->empleado->caja->estado == EstadoCaja::ABIERTA) {
                $estadoCaja = EstadoCaja::ABIERTA;
            }
        }

        return view('frontend.ventas.venta.listado-ventas-automaticas', compact('estadoCaja'));
    }

    public function facturasAutomaticasDatos(){
        $empresa = Auth::user()->empresa;
        $sucursal = Auth::user()->sucursal;

        $ventas = Venta::listarVentasAutomaticasPorEmpresaSucursal($empresa, $sucursal);

        $tipos_comprobantes = TipoComprobante::$comprobantes;

        return Datatables::of($ventas)
            ->addColumn('action', function ($venta) {
                $return = '
                <a href="'.url('/ventas/venta/' . $venta->id).'" class="mytooltip"><i class="fa fa-eye text-success m-r-10"></i>
                    <span class="tooltip-content3">Ver Detalles Venta</span>
                </a>';
                if($venta->tipo_punto_id == 2 || $venta->tipo_punto_id == 4){
                    $return .='
                        <a href="'.url('/ventas/venta/imprimir/' . $venta->id).'" class="mytooltip">
                            <i class="fa fa-print text-primary m-r-10"></i>
                            <span class="tooltip-content3">Imprimir Comprobante</span>
                        </a>
                        <a class="delete mytooltip pointer" data-id="'.$venta->id.'">
                            <i class="fa fa-close text-danger"></i>
                            <span class="tooltip-content3">Eliminar Venta</span>
                        </a>';
                }
                if($venta->estado_facturacion == EstadoFacturacion::ERROR || $venta->estado_facturacion == EstadoFacturacion::PENDIENTE){
                    $return .='
                        <a href="'.url('/ventas/facturacion-fiscal/facturarVenta/' . $venta->id).'" class="mytooltip">
                            <i class="fa fa-print text-danger m-r-10"></i>
                            <span class="tooltip-content3">Facturar</span>
                        </a>';
                }
                return $return;
            })
            ->setRowId('id')
            ->removeColumn('id')
            ->removeColumn('estado_facturacion')
            ->removeColumn('tipo_punto_id')
            ->removeColumn('tipo_comprobante_id')
            ->removeColumn('signo')
            ->editColumn('fecha', function ($venta) {
                return $venta->fecha->format('d/m/Y H:i');
            })
            ->editColumn('total_cobro', function ($venta) {
                return $venta->signo.' '.$venta->total_cobro;
            })
            ->editColumn('numero', function ($venta) use ($tipos_comprobantes) {
                return $tipos_comprobantes[$venta->tipo_comprobante_id].' '.$venta->numero;
            })
            ->setRowClass(function ($venta) {
                if($venta->estado_facturacion == EstadoFacturacion::ERROR){
                    return 'table-danger';
                } elseif($venta->estado_facturacion == EstadoFacturacion::PENDIENTE){
                    return 'table-warning';
                }
            })
            ->make(true);
    }

    public function createRapido(){
        if(isset(Auth::user()->empleado->caja)){
            if(Auth::user()->empleado->caja->estado != EstadoCaja::ABIERTA) {
                return redirect('ventas/venta');
            }
        }else{
            Session::flash('flash_message', 'Usted no posee una caja asignada por defecto!');
            return redirect('ventas/venta');
        }

        $empresa = Auth::user()->empresa;

        $moneda = Moneda::where('empresa_id', $empresa->id)->where('cotizacion', 1)->first();
        $listaPrecio = Opcion::where('empresa_id', $empresa->id)
            ->where('nombre', 'lista_precios_defecto_id')
            ->first();

        return view('frontend.ventas.venta.create_rapido', compact('moneda', 'listaPrecio'));
    }

    public function storeRapido(Request $request){
        $this->validate($request, [
            'total' => 'required'
        ]);

        DB::transaction(function() use ($request, &$venta) {
            $requestData = $request->all();
            $empresa = Auth::user()->empresa;
            $sucursal = Auth::user()->sucursal;

            $venta = New Venta();

            $moneda = Moneda::where('empresa_id', $empresa->id)->where('cotizacion', 1)->first();
            $listaPrecio = Opcion::where('empresa_id', $empresa->id)
                ->where('nombre', 'lista_precios_defecto_id')
                ->first();
            $cliente = Cliente::where('empresa_id', $empresa->id)->whereNull('deleted_at')->where('razon_social','=', 'Consumidor Final')->first();

            if(Auth::user()->empleado->puntoVenta){ // Si el usuario ya tiene un punto de venta seteado
                $puntoDeVenta = Auth::user()->empleado->puntoVenta;
            } else { // Si no lo tiene, busco el primer punto de venta en negro que encuentre en la empresa o sucursal
                $query = PuntoDeVenta::where('empresa_id',$empresa->id);
                if($sucursal){
                    $query->where('sucursal_id', $sucursal->id);
                }
                $query->where('tipo_punto_id', 2); // Impresora Manual
                $query->orWhere('tipo_punto_id', 4); // Comandera
                $puntoDeVenta = $query->first();
                if(!$puntoDeVenta){ // Si no hay en negro, busco en blanco o todos
                    $query = PuntoDeVenta::where('empresa_id',$empresa->id);
                    if($sucursal){
                        $query->where('sucursal_id', $sucursal->id);
                    }
                    $puntoDeVenta = $query->first();
                }
            }

            if($empresa->condicion_iva_id == 1){//Monotributista
                $tipo_comprobante = TipoComprobante::FACTURA_C;
            }else{//RI
                $tipo_comprobante = TipoComprobante::FACTURA_B;
            }

            $venta->cliente_id = $cliente->id;
            $venta->tipo_comprobante_id = $tipo_comprobante;
            $venta->punto_venta_id = $puntoDeVenta->id;
            $venta->lista_precios_id = $listaPrecio->valor;
            $venta->fecha = date("Y-m-d H:i:s");
            $venta->moneda_id = $moneda->id;
            $venta->cotizacion = 1;
            $venta->importe_neto = $request->importe_neto;
            $venta->importe_iva = $request->importe_iva;
            $venta->importe_descuento = $request->importe_descuento;
            $venta->porcentaje_descuento = $request->porcentaje_descuento;
            $venta->importe_impuesto_interno = $request->importe_impuesto_interno;
            $venta->total = $request->total;
            $venta->total_cobro = $request->total;
            if($request->guardar){
                $venta->estado = Estado::GUARDADO;
            } else {
                $venta->estado = Estado::ACTIVO;
            }
            $venta->empresa_id = $empresa->id;
            $venta->sucursal_id = $sucursal->id;
            $venta->usuario_id = Auth::user()->id;

            if($venta->importe_neto == null)
                $venta->importe_neto = 0;
            if($venta->importe_iva == null)
                $venta->importe_iva = 0;

            $nombresClaves = [
                TipoComprobante::FACTURA_A => 'numeracion_factura_a',
                TipoComprobante::FACTURA_B => 'numeracion_factura_b',
                TipoComprobante::FACTURA_C => 'numeracion_factura_c',
                TipoComprobante::TIQUE_FACTURA_A => 'numeracion_factura_a',
                TipoComprobante::TIQUE_FACTURA_B => 'numeracion_factura_b',
                TipoComprobante::TIQUE_FACTURA_C => 'numeracion_factura_c'
            ];

            if($venta->puntoVenta->esBlanco()){ // Si es en blanco busco la numeracion segun el punto de venta
                $opcion = Opcion::where('empresa_id', $empresa->id)
                    ->where('nombre', 'LIKE', $nombresClaves[$tipo_comprobante])
                    ->where('punto_venta_id', $venta->puntoVenta->id)
                    ->first();
                $numero_factura = $opcion->valor;
                $opcion->valor = str_pad($opcion->valor + 1, 8, '0', STR_PAD_LEFT);
                $opcion->save();

                $venta->estado_facturacion = EstadoFacturacion::PENDIENTE;
            }else{ //si es Manual busco la numeración genérica
                $opcion = Opcion::where('empresa_id', $empresa->id)
                    ->where('nombre', 'LIKE', $nombresClaves[$tipo_comprobante])
                    ->first();
                $numero_factura = $opcion->valor;
                $opcion->valor = str_pad($opcion->valor + 1, 8, '0', STR_PAD_LEFT);
                $opcion->save();
            }
            $venta->numero = $numero_factura;

            $venta->save();

            $porcentajes_iva = PorcentajeIva::$valores;
            if(isset($requestData['detalles'])) {
                $detalles = $requestData['detalles'];
                foreach ($detalles as $detalle) {
                    if ((int)$detalle['estado'] == Estado::ACTIVO && isset($detalle['cantidad']) && isset($detalle['articulo_id'])) {
                        $nuevoDetalle = New DetalleVenta($detalle);
                        $nuevoDetalle->venta_id = $venta->id;
                        $nuevoDetalle->precio_iva = $detalle['precio_neto'] * ($porcentajes_iva[$detalle['porcentaje_iva']] / 100);
                        $nuevoDetalle->save();

                        $nuevoDetalle->articulo->descontarStock($nuevoDetalle->cantidad,$sucursal);
                    }
                }
            }

            $controllerMovimientoCaja = new MovimientoCajaController();
            $nuevoCobro = New DetalleCobroVenta();
            $nuevoCobro->fecha = date("Y-m-d H:i:s");
            $nuevoCobro->venta_id = $venta->id;
            $nuevoCobro->estado = Estado::ACTIVO;

            $monto = (float)$venta->total;
            if(!$request->guardar)
                $controllerMovimientoCaja->nuevaEntrada($venta, 'ventas/venta', FormaDePago::EFECTIVO, ConceptoDeCaja::VENTA, $monto);
            $nuevoCobro->descripcion = 'Cobro en Efectivo';
            $nuevoCobro->forma_cobro = FormaDePago::EFECTIVO;
            $nuevoCobro->monto = $monto;
            $nuevoCobro->save();

            $controllerHistorial = new HistorialController();
            $controllerHistorial->nuevoHistorialCreacion($venta, $request);

            Session::flash('flash_success', 'Venta creada correctamente.');
        });

        $porcentajes_iva = PorcentajeIva::$valores;

        if(!$request->guardar){
            if($venta->puntoVenta->tipo->nombre == "Impresora Fiscal"){
                if(Auth::user()->controladoraFiscal()){
                    $facturar = New FacturacionFiscal();
                    $ventaRapida = true;
                    return $facturar->facturarVenta($venta->id,$ventaRapida);
                } else {
                    $venta->estado_facturacion = EstadoFacturacion::PENDIENTE;
                    $venta->save();
                    return redirect('ventas/venta/create_rapido');
                }
            } elseif($venta->puntoVenta->tipo->nombre == "Comandera"){
                $empresa = Auth::user()->empresa;
                $ventaRapida = true;

                return view('frontend.ventas.venta.imprimir-ticket', compact('venta','empresa','porcentajes_iva','ventaRapida'));

            } elseif($venta->puntoVenta->tipo->nombre == "Factura Electrónica"){

                $facturar = New FacturacionElectronica();
                return $facturar->facturarVenta($venta->id);

            } else { // Punto de venta de facturacion manual
                return redirect('ventas/venta/create_rapido');
            }
        } else {
            return redirect('ventas/venta/create_rapido');
        }

    }

    public function digitoVerificador($numero){
        $j=strlen($numero);
        $par=0;$impar=0;
        for ($i=0; $i < $j ; $i++) {
            if ($i%2==0){
                $par=$par+$numero[$i];
            }else{
                $impar=$impar+$numero[$i];

            }

        }

        $par=$par*3;
        $suma=$par+$impar;
        for ($i=0; $i < 9; $i++) {
            if ( fmod(($suma +$i),10) == 0) {
                $verificador=$i;
            }
        }

        $digito = 10 - ($suma - (intval($suma / 10) * 10));
        if ($digito == 10){
            $digito = 0;
        }
        return $digito;
    }

}
