<?php

namespace App\Http\Controllers\Frontend\Ventas;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Frontend\Caja\MovimientoCajaController;
use App\Http\Controllers\Frontend\Configuraciones\HistorialController;
use App\Http\Controllers\Frontend\Fondos\ChequeController;
use App\Models\Frontend\Caja\ConceptoDeCaja;
use App\Models\Frontend\Caja\EstadoCaja;
use App\Models\Frontend\Caja\FormaDePago;
use App\Models\Frontend\Configuraciones\EntidadRelacionadaLink;
use App\Models\Frontend\Configuraciones\Estado;
use App\Models\Frontend\Configuraciones\Moneda;
use App\Models\Frontend\Configuraciones\Opcion;
use App\Models\Frontend\Configuraciones\PorcentajeIva;
use App\Models\Frontend\Configuraciones\PuntoDeVenta;
use App\Models\Frontend\Configuraciones\Tarjeta;
use App\Models\Frontend\Configuraciones\TipoCuentaCorriente;
use App\Models\Frontend\Fondos\Banco;
use App\Models\Frontend\Fondos\CuentaBancaria;
use App\Models\Frontend\Fondos\MovimientoBancario;
use App\Models\Frontend\Fondos\TipoMovimiento;
use App\Models\Frontend\Ventas\CuponTarjeta;
use App\Models\Frontend\Ventas\DetalleCobroNotaDebito;
use App\Models\Frontend\Ventas\DetalleNotaDebito;
use App\Models\Frontend\Ventas\ListaPrecio;
use App\Models\Frontend\Ventas\NotaDebito;
use App\Models\Frontend\Ventas\EstadoFacturacion;
use App\Models\Frontend\Configuraciones\TipoComprobante;
use Yajra\Datatables\Facades\Datatables;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use Session;

class NotaDebitoController extends Controller{

    public function index(){

        $estadoCaja = EstadoCaja::CERRADA;
        if(isset(Auth::user()->empleado->caja)){
            if(Auth::user()->empleado->caja->estado == EstadoCaja::ABIERTA) {
                $estadoCaja = EstadoCaja::ABIERTA;
            }
        }

        return view('frontend.ventas.nota-debito.index', compact('estadoCaja'));
    }

    public function datos(){
        $empresa = Auth::user()->empresa;
        $sucursal = Auth::user()->sucursal;

        $notasDebito = NotaDebito::listarNotasDeDebitoPorEmpresaSucursal($empresa, $sucursal);

        $tipos_comprobantes = TipoComprobante::$comprobantes;

        return Datatables::of($notasDebito)
            ->addColumn('action', function ($notaDebito) {
                $return = '
                <a href="'.url('/ventas/nota-debito/' . $notaDebito->id).'" class="mytooltip"><i class="fa fa-eye text-success m-r-10"></i>
                    <span class="tooltip-content3">Ver Detalles de la Nota De Debito</span>
                </a>';
                if($notaDebito->tipo_punto_id == 2 || $notaDebito->tipo_punto_id == 4){
                    $return .='
                        <a href="'.url('/ventas/nota-debito/imprimir/' . $notaDebito->id).'" class="mytooltip">
                            <i class="fa fa-print text-purple m-r-10"></i>
                            <span class="tooltip-content3">Imprimir Comprobante</span>
                        </a>
                        <a class="delete mytooltip pointer" data-id="'.$notaDebito->id.'">
                            <i class="fa fa-close text-danger"></i>
                            <span class="tooltip-content3">Eliminar Nota De Debito</span>
                        </a>';
                }
                if($notaDebito->estado_facturacion == EstadoFacturacion::ERROR){
                    $return .='
                        <a href="'.url('/ventas/facturacion-fiscal/facturarNotaDeDebito/' . $notaDebito->id).'" class="mytooltip">
                            <i class="fa fa-print text-danger m-r-10"></i>
                            <span class="tooltip-content3">Facturar</span>
                        </a>';
                }
                return $return;
            })
            ->setRowId('id')
            ->removeColumn('id')
            ->removeColumn('estado_facturacion')
            ->removeColumn('tipo_punto_id')
            ->removeColumn('tipo_comprobante_id')
            ->removeColumn('signo')
            ->editColumn('fecha', function ($notaDebito) {
                return $notaDebito->fecha->format('d/m/Y H:i');
            })
            ->editColumn('total_cobro', function ($notaDebito) {
                return $notaDebito->signo.' '.$notaDebito->total_cobro;
            })
            ->editColumn('numero', function ($notaDebito) use ($tipos_comprobantes) {
                return $tipos_comprobantes[$notaDebito->tipo_comprobante_id].' '.$notaDebito->numero;
            })
            ->setRowClass(function ($notaDebito) {
                if($notaDebito->estado_facturacion == EstadoFacturacion::ERROR){
                    return 'table-danger';
                }
            })
            ->make(true);
    }

    public function create(){
        if(isset(Auth::user()->empleado->caja)){
            if(Auth::user()->empleado->caja->estado != EstadoCaja::ABIERTA) {
                return redirect('ventas/nota-debito');
            }
        }else{
            return redirect('ventas/nota-debito');
        }

        $empresa = Auth::user()->empresa;
        $sucursal = Auth::user()->sucursal;

        $fecha_hoy = date('d/m/Y');

        if($empresa->condicion_iva_id == 1){//Monotributista
            $numero_comprobante = Opcion::getValorOpcion('numeracion_nd_c', $empresa);
            $tipo_comprobante_por_defecto = TipoComprobante::NOTA_DEBITO_C;
        }else{//RI
            $numero_comprobante = Opcion::getValorOpcion('numeracion_nd_b', $empresa);
            $tipo_comprobante_por_defecto = TipoComprobante::NOTA_DEBITO_B;
        }

        $punto_de_venta_por_defecto = null;
        if(isset(Auth::user()->empleado->puntoVenta)){
            $punto_de_venta_por_defecto = Auth::user()->empleado->puntoVenta;
        }

        $tipos_comprobantes = TipoComprobante::getArrayComprobantes($empresa,'NotaDebito');
        $monedas = Moneda::getMonedasPorEmpresaArray($empresa);
        $monedaPorDefecto = Moneda::where('empresa_id', $empresa->id)->whereNull('deleted_at')->where('cotizacion','=', 1)->first();
        $lista_precios = ListaPrecio::getListasPorEmpresaArray($empresa);
        $lista_precios_defecto = Opcion::where('empresa_id', $empresa->id)
            ->where('nombre', 'lista_precios_defecto_id')
            ->first();

        $formas_cobro = FormaDePago::$getArrayVenta;
        $bancos = Banco::getBancosPorEmpresa($empresa)->pluck('nombre', 'id')->all();
        $tarjetas = Tarjeta::getTarjetasPorEmpresaArray($empresa);
        $tipos_cuenta_corriente = TipoCuentaCorriente::getTiposArray($empresa);
        $cuentas_bancarias = CuentaBancaria::getCuentasBancariasPorEmpresaArray($empresa);

        if (Auth::user()->hasRole('Usuario Vendedor')) {
            $puntos_de_ventas = PuntoDeVenta::where('sucursal_id', $sucursal->id)->get()->pluck('numero', 'id')->toArray();
        }else{
            $puntos_de_ventas = PuntoDeVenta::where('empresa_id', $empresa->id)->get()->pluck('numero', 'id')->toArray();
        }

        return view('frontend.ventas.nota-debito.create', compact('fecha_hoy','numero_comprobante', 'tipos_comprobantes', 'tipo_comprobante_por_defecto', 'punto_de_venta_por_defecto', 'monedas', 'lista_precios', 'formas_cobro', 'bancos', 'tarjetas', 'tipos_cuenta_corriente', 'cuentas_bancarias', 'puntos_de_ventas', 'monedaPorDefecto','lista_precios_defecto'));
    }

    public function store(Request $request){
        $this->validate($request, [
            'cliente_id' => 'required',
            'moneda_id' => 'required',
            'fecha' => 'required',
            'numero' => 'required',
            'tipo_comprobante_id' => 'required',
            'punto_venta_id' => 'required',
            'total' => 'required'
        ]);

        DB::transaction(function() use ($request, &$notaDebito) {
            $requestData = $request->all();
            $empresa = Auth::user()->empresa;
            $sucursal = Auth::user()->sucursal;

            $nombresClaves = [
                TipoComprobante::NOTA_DEBITO_A => ['numeracion_nd_a'],
                TipoComprobante::NOTA_DEBITO_B => ['numeracion_nd_b'],
                TipoComprobante::NOTA_DEBITO_C => ['numeracion_nd_c'],
                TipoComprobante::TIQUE_NOTA_DEBITO_A => ['numeracion_nd_a'],
                TipoComprobante::TIQUE_NOTA_DEBITO_B => ['numeracion_nd_b'],
                TipoComprobante::TIQUE_NOTA_DEBITO_C => ['numeracion_nd_c']
            ];

            $notaDebito = New NotaDebito($requestData);

            if($notaDebito->puntoVenta->esBlanco()){//si es en blanco busco la numeración del punto de venta
                $opcion = Opcion::where('empresa_id', $empresa->id)
                    ->where('nombre', 'LIKE', $nombresClaves[(int)$requestData['tipo_comprobante_id']])
                    ->where('punto_venta_id', $notaDebito->puntoVenta->id)
                    ->first();
                $numero_factura = $opcion->valor;
                $opcion->valor = str_pad($opcion->valor + 1, 8, '0', STR_PAD_LEFT);
                $opcion->save();

                $notaDebito->estado_facturacion = EstadoFacturacion::PENDIENTE; // Seteo el comprobante en pendiente por defecto
            }else{//si es Manual busco la numeración genérica
                $opcion = Opcion::where('empresa_id', $empresa->id)
                    ->where('nombre', 'LIKE', $nombresClaves[(int)$requestData['tipo_comprobante_id']])
                    ->first();

                $numero_factura = $opcion->valor;
                $opcion->valor = str_pad($opcion->valor + 1, 8, '0', STR_PAD_LEFT);
                $opcion->save();
            }
            $notaDebito->numero = $numero_factura;

            $date = DateTime::createFromFormat('d/m/Y', $requestData['fecha']);
            $notaDebito->fecha = $date->format('Y-m-d H:i:s');
            $notaDebito->estado = Estado::ACTIVO;
            $notaDebito->empresa_id = $empresa->id;
            $notaDebito->sucursal_id = $sucursal->id;
            $notaDebito->tipo_comprobante_id = $requestData['tipo_comprobante_id'];
            if($notaDebito->importe_neto == null)
                $notaDebito->importe_neto = 0;
            if($notaDebito->importe_iva == null)
                $notaDebito->importe_iva = 0;
            $notaDebito->save();

            $entidadRelacionadaLink = EntidadRelacionadaLink::crearNueva($notaDebito, $request, null, 'Nota de Débito Nº '.$notaDebito->numero);

            $moneda = Moneda::findOrFail($notaDebito->moneda->id);
            $moneda->cotizacion = $requestData['cotizacion'];
            $moneda->save();

            $porcentajes_iva = PorcentajeIva::$valores;
            if(isset($requestData['detalles'])) {
                $detalles = $requestData['detalles'];
                foreach ($detalles as $detalle) {
                    if ((int)$detalle['estado'] == Estado::ACTIVO && isset($detalle['cantidad']) && isset($detalle['articulo_id'])) {
                        $nuevoDetalle = New DetalleNotaDebito($detalle);
                        $nuevoDetalle->nota_debito_id = $notaDebito->id;
                        $nuevoDetalle->precio_iva = $detalle['precio_neto'] * ($porcentajes_iva[$detalle['porcentaje_iva']] / 100);
                        $nuevoDetalle->save();

                        $nuevoDetalle->articulo->descontarStock($nuevoDetalle->cantidad,$sucursal);
                    }
                }
            }

            if(isset($requestData['detalles_cobro'])) {
                $detallesCobro = $requestData['detalles_cobro'];
                $controllerMovimientoCaja = new MovimientoCajaController();
                foreach ($detallesCobro as $cobro) {
                    if ((int)$cobro['estado'] == Estado::ACTIVO) {
                        $nuevoCobro = New DetalleCobroNotaDebito($cobro);
                        $nuevoCobro->fecha = $date;
                        $nuevoCobro->nota_debito_id = $notaDebito->id;
                        $nuevoCobro->estado = Estado::ACTIVO;

                        $monto = (float)$cobro['monto'] + (float)$cobro['interes'];
                        $controllerMovimientoCaja->nuevaEntrada($notaDebito, 'ventas/nota-debito', (int)$cobro['forma_cobro'], ConceptoDeCaja::NOTA_DEBITO, $monto);
                        if((int)$cobro['forma_cobro'] == FormaDePago::EFECTIVO) {
                            $nuevoCobro->descripcion = 'Cobro en Efectivo';
                            $nuevoCobro->forma_cobro = FormaDePago::EFECTIVO;
                        }else if((int)$cobro['forma_cobro'] == FormaDePago::MERCADO_PAGO) {
                            $nuevoCobro->descripcion = 'Cobro en Mercado Pago';
                            $nuevoCobro->forma_cobro = FormaDePago::MERCADO_PAGO;
                        }else if((int)$cobro['forma_cobro'] == FormaDePago::CHEQUE) {
                            $controllerCheque = new ChequeController();
                            $nuevoCheque = $controllerCheque->nuevoCheque($cobro, (float)$cobro['monto']);

                            $nuevoCobro->entidad_id = $nuevoCheque->id;
                            $nuevoCobro->descripcion = 'Cobro con Cheque';
                            $nuevoCobro->forma_cobro = FormaDePago::CHEQUE;
                        }else if((int)$cobro['forma_cobro'] == FormaDePago::CUENTA_BANCARIA) {
                            $nuevoMovimientoBancario = new MovimientoBancario($cobro);
                            $nuevoMovimientoBancario->importe = (float)$cobro['monto'];
                            $nuevoMovimientoBancario->cuenta_bancaria_destino_id = $cobro['cuenta_bancaria_id'];
                            $nuevoMovimientoBancario->moneda_id = $notaDebito->moneda_id;
                            $nuevoMovimientoBancario->tipo = TipoMovimiento::DEPOSITO;
                            $nuevoMovimientoBancario->fecha = $date;
                            $nuevoMovimientoBancario->empresa_id = $empresa->id;
                            $nuevoMovimientoBancario->entidad_id = $notaDebito->id;
                            $nuevoMovimientoBancario->entidad_clase = class_basename($notaDebito);
                            $nuevoMovimientoBancario->url = 'ventas/nota-debito';
                            $nuevoMovimientoBancario->save();

                            $nuevoCobro->entidad_id = $nuevoMovimientoBancario->id;
                            $nuevoCobro->descripcion = 'Cobro con Depósito Bancario';
                            $nuevoCobro->forma_cobro = FormaDePago::CUENTA_BANCARIA;
                        }else if((int)$cobro['forma_cobro'] == FormaDePago::TARJETA) {
                            $nuevoCuponTarjeta = new CuponTarjeta($cobro);
                            $nuevoCuponTarjeta->fecha = $date;
                            $nuevoCuponTarjeta->entidad_relacionada_link_id = $entidadRelacionadaLink->id;
                            $nuevoCuponTarjeta->importe = (float)$cobro['monto'] + (float)$cobro['interes'];
                            $nuevoCuponTarjeta->autorizacion = 'Autorizada';
                            $nuevoCuponTarjeta->estado = Estado::PENDIENTE;
                            $nuevoCuponTarjeta->save();

                            $nuevoCobro->entidad_id = $nuevoCuponTarjeta->id;
                            $nuevoCobro->descripcion = 'Cobro con Tarjeta';
                            $nuevoCobro->forma_cobro = FormaDePago::TARJETA;
                        }else if((int)$cobro['forma_cobro'] == FormaDePago::CUENTA_CORRIENTE) {
                            $controllerCuentaCorriente = new CuentaCorrienteController();
                            $controllerCuentaCorriente->debe($notaDebito, $request, $cobro['tipo_cuenta_corriente_id'],$cobro['monto'] + $cobro['interes']);

                            $nuevoCobro->descripcion = 'Cobro a Cuenta Corriente';
                            $nuevoCobro->forma_cobro = FormaDePago::CUENTA_CORRIENTE;
                        }
                        $nuevoCobro->save();
                    }
                }
            }

            $controllerHistorial = new HistorialController();
            $controllerHistorial->nuevoHistorialCreacion($notaDebito, $request);

            Session::flash('flash_success', 'Nota de debito creada correctamente.');
        });

        $porcentajes_iva = PorcentajeIva::$valores;

        if($notaDebito->puntoVenta->tipo->nombre == "Impresora Fiscal"){
            if(Auth::user()->controladoraFiscal()){
                $facturar = New FacturacionFiscal();
                return $facturar->facturarNotaDeDebito($notaDebito->id);
            } else {
                return redirect('ventas/nota-debito');
            }

        } elseif($notaDebito->puntoVenta->tipo->nombre == "Comandera"){

            $empresa = Auth::user()->empresa;
            return view('frontend.ventas.nota-debito.imprimir-ticket', compact('notaDebito','empresa','porcentajes_iva'));

        } elseif($notaDebito->puntoVenta->tipo->nombre == "Factura Electrónica"){
               $facturar = New FacturacionElectronica();
               return $facturar->facturarNotaDeDebito($notaDebito->id);
        } else { // Punto de venta de facturacion manual
            return redirect('ventas/nota-debito');
        }

        return redirect('ventas/nota-debito');
    }

    public function show($id){
        $notadebito = NotaDebito::findOrFail($id);
        $empresa = Auth::user()->empresa;

        if ($notadebito->empresa_id == $empresa->id) {
            $porcentajes_iva = PorcentajeIva::$valores;
            $tipos_comprobantes = TipoComprobante::$comprobantes;
            if (Auth::user()->hasRole('Usuario Vendedor')) {
                $sucursal = Auth::user()->sucursal;
                if ($notadebito->sucursal_id == $sucursal->id) {
                    return view('frontend.ventas.nota-debito.show', compact('notadebito', 'tipos_comprobantes', 'porcentajes_iva'));
                }
            } else {
                return view('frontend.ventas.nota-debito.show', compact('notadebito', 'tipos_comprobantes', 'porcentajes_iva'));
            }
        }
        return redirect('ventas/nota-debito');
    }

    public function edit($id){
        return redirect('ventas/nota-debito');
    }

    public function update($id, Request $request){
        return redirect('ventas/nota-debito');
    }

    public function destroy($id){
        $notaDebito = NotaDebito::findOrFail($id);
        $empresa = Auth::user()->empresa;

        if (!$notaDebito->empresa_id == $empresa->id) {
            return redirect('ventas/nota-debito');
        }else{
            if (Auth::user()->hasRole('Usuario Vendedor')) {
                $sucursal = Auth::user()->sucursal;
                if (!$notaDebito->sucursal_id == $sucursal->id) {
                    return redirect('ventas/nota-debito');
                }
            }
        }

        DB::transaction(function() use ($notaDebito) {
            $sucursal = $notaDebito->sucursal;
            if(!$notaDebito->detalles->isEmpty() && $sucursal){
                foreach($notaDebito->detalles as $detalle){
                    if(isset($detalle->articulo)){
                        $detalle->articulo->sumarStock($detalle->cantidad,$sucursal);
                    }
                    $detalle->delete();
                }
            }

            $notaDebito->delete();

            $controllerHistorial = new HistorialController();
            $controllerHistorial->nuevoHistorialEliminacion($notaDebito);

            Session::flash('flash_success', 'Nota de debito eliminada correctamente.');
        });

        return redirect('ventas/nota-debito');
    }

    public function imprimir($nota_id){
        $empresa = Auth::user()->empresa;
        $notaDebito = NotaDebito::findOrFail($nota_id);

        if ($notaDebito->empresa_id == $empresa->id) {
            $tipos_comprobantes = TipoComprobante::$comprobantes;
            $porcentajes_iva = PorcentajeIva::$valores;

            $pdf = app('dompdf.wrapper');
            $pdf->getDomPDF()->set_option("enable_php", true);
            $pdf->loadView('frontend.ventas.nota-debito.imprimir-nota-debito',
                compact('notaDebito','empresa','tipos_comprobantes', 'porcentajes_iva'));

            if (Auth::user()->hasRole('Usuario Vendedor')) {
                if ($notaDebito->sucursal_id == Auth::user()->sucursal_id) {
                    if($notaDebito->puntoVenta->tipo->nombre == "Comandera"){
                        return view('frontend.ventas.nota-debito.imprimir-ticket', compact('notaDebito','empresa','tipos_comprobantes','porcentajes_iva'));
                    } else {
                        return $pdf->stream('Nota de Debito.pdf');
                    }

                }
            } else {
                if($notaDebito->puntoVenta->tipo->nombre == "Comandera"){
                    return view('frontend.ventas.nota-debito.imprimir-ticket', compact('notaDebito','empresa','tipos_comprobantes','porcentajes_iva'));
                } else {
                    return $pdf->stream('Nota de Debito.pdf');
                }
            }
        }
    }

    public function setEstadoFacturacion(Request $request){
        $nota_debito_id = $request['nota_id'];
        $notaDebito = NotaDebito::findOrFail($nota_debito_id);

        $empresa = Auth::user()->empresa;

        if ($notaDebito->empresa_id == $empresa->id) {
            if ($request['estado'] == "FACTURADO") {

                $notaDebito->numero = str_pad($request['numeroComprobante'], 8, "0", STR_PAD_LEFT);
                $nombresClaves = [
                    TipoComprobante::NOTA_DEBITO_A => ['numeracion_nd_a'],
                    TipoComprobante::NOTA_DEBITO_B => ['numeracion_nd_b'],
                    TipoComprobante::NOTA_DEBITO_C => ['numeracion_nd_c'],
                    TipoComprobante::TIQUE_NOTA_DEBITO_A => ['numeracion_nd_a'],
                    TipoComprobante::TIQUE_NOTA_DEBITO_B => ['numeracion_nd_b'],
                    TipoComprobante::TIQUE_NOTA_DEBITO_C => ['numeracion_nd_c']
                ];
                $opcion = Opcion::where('empresa_id', $empresa->id)
                    ->where('nombre', 'LIKE', $nombresClaves[(int)$notaDebito->tipo_comprobante_id])
                    ->where('punto_venta_id', $notaDebito->puntoVenta->id)
                    ->first();
                $opcion->valor = str_pad($request['numeroComprobante'] + 1, 8, "0", STR_PAD_LEFT);
                $opcion->save();

                $notaDebito->estado_facturacion = EstadoFacturacion::FACTURADO;
            } else {
                $notaDebito->estado_facturacion = EstadoFacturacion::ERROR;
            }

            if(Auth::user()->hasRole('Usuario Vendedor')){
                $sucursal = Auth::user()->sucursal;
                if ($notaDebito->sucursal_id == $sucursal->id) {
                    $notaDebito->save();
                }
            } else {
                $notaDebito->save();
            }
        }
    }

}
