<?php

namespace App\Http\Controllers\Frontend\Ventas;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Frontend\Caja\MovimientoCajaController;
use App\Http\Controllers\Frontend\Configuraciones\HistorialController;
use App\Http\Controllers\Frontend\Fondos\ChequeController;
use App\Models\Frontend\Caja\ConceptoDeCaja;
use App\Models\Frontend\Caja\EstadoCaja;
use App\Models\Frontend\Caja\FormaDePago;
use App\Models\Frontend\Configuraciones\EntidadRelacionadaLink;
use App\Models\Frontend\Configuraciones\Estado;
use App\Models\Frontend\Configuraciones\Moneda;
use App\Models\Frontend\Configuraciones\Opcion;
use App\Models\Frontend\Configuraciones\PorcentajeIva;
use App\Models\Frontend\Configuraciones\Tarjeta;
use App\Models\Frontend\Configuraciones\TipoCuentaCorriente;
use App\Models\Frontend\Fondos\Banco;
use App\Models\Frontend\Fondos\CuentaBancaria;
use App\Models\Frontend\Fondos\MovimientoBancario;
use App\Models\Frontend\Fondos\TipoMovimiento;
use App\Models\Frontend\Personas\Cliente;
use App\Models\Frontend\Ventas\CuentaCorriente;
use App\Models\Frontend\Ventas\CuponTarjeta;
use App\Models\Frontend\Ventas\DetalleCobroRecibo;
use App\Models\Frontend\Ventas\DetalleRecibo;
use App\Models\Frontend\Ventas\EstadoCuentaCorriente;
use App\Models\Frontend\Ventas\FacturaVentaAutomatica;
use App\Models\Frontend\Ventas\Recibo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\Datatables\Facades\Datatables;
use DateTime;
use Session;
use DB;

class ReciboController extends Controller{

    public function index(){
        $estadoCaja = EstadoCaja::CERRADA;
        if(isset(Auth::user()->empleado->caja)){
            if(Auth::user()->empleado->caja->estado == EstadoCaja::ABIERTA) {
                $estadoCaja = EstadoCaja::ABIERTA;
            }
        }

        return view('frontend.ventas.recibo.index', compact('estadoCaja'));
    }

    public function datos(){
        $empresa = Auth::user()->empresa;
        $sucursal = Auth::user()->sucursal;

        $recibos = Recibo::listarRecibosPorEmpresaSucursal($empresa, $sucursal);

        return Datatables::of($recibos)
            ->addColumn('action', function ($recibo) {
                return '
                <a href="'.url('/ventas/recibo/' . $recibo->id).'" class="mytooltip"><i class="fa fa-eye text-success m-r-10"></i>
                    <span class="tooltip-content3">Ver Recibo</span>
                </a>
                <a href="'.url('/ventas/recibo/imprimir/' . $recibo->id).'" target="_blank" class="mytooltip">
                    <i class="fa fa-print text-purple m-r-10"></i>
                    <span class="tooltip-content3">Imprimir Recibo</span>
                </a>
                <a class="delete mytooltip pointer" data-id="'.$recibo->id.'">
                    <i class="fa fa-close text-danger"></i>
                    <span class="tooltip-content3">Eliminar Recibo</span>
                </a>
                ';
            })
            ->setRowId('id')
            ->removeColumn('id')
            ->removeColumn('signo')
            ->editColumn('fecha', function ($recibo) {
                return $recibo->fecha->format('d/m/Y H:i');
            })
            ->editColumn('importe', function ($recibo) {
                return $recibo->signo.' '.$recibo->importe;
            })
            ->editColumn('numero', function ($recibo) {
                return 'Recibo N° '.$recibo->numero;
            })
            ->make(true);
    }

    public function create(){
        return redirect('ventas/recibo');
    }

    public function store(Request $request){
        $this->validate($request, [
			'fecha' => 'required'
		]);

        DB::transaction(function() use ($request) {
            $requestData = $request->all();

            $empresa = Auth::user()->empresa;
            $sucursal = Auth::user()->sucursal;

            $recibo = new Recibo($requestData);
            $date = DateTime::createFromFormat('d/m/Y', $requestData['fecha']);
            $recibo->fecha = $date->format('Y-m-d H:i:s');
            $recibo->importe = $requestData['total'];
            $recibo->empresa_id = $empresa->id;
            $recibo->sucursal_id = $sucursal->id;
            $recibo->estado = Estado::ACTIVO;
            $recibo->save();

            $ultimo_codigo = str_pad((int)$recibo->numero + 1, 8, '0', STR_PAD_LEFT);
            Opcion::setValorOpcion('numeracion_recibo', $ultimo_codigo, $empresa);

            $moneda = $recibo->moneda;
            $moneda->cotizacion = $requestData['cotizacion'];
            $moneda->update();

            $entidadRelacionadaLink = EntidadRelacionadaLink::crearNueva($recibo, $request, null, 'Recibo Nº '.$recibo->numero);

            $auxiliarCobro = (float)$requestData['total'];

            if(isset($requestData['detalles'])) {
                $detalles = $requestData['detalles'];
                foreach ($detalles as $detalle) {
                    if ((int)$detalle['estado'] == Estado::ACTIVO && $auxiliarCobro > 0) {
                        $nuevoDetalle = New DetalleRecibo($detalle);
                        $nuevoDetalle->recibo_id = $recibo->id;
                        if ($detalle['tipo'] == 'cuentas_corrientes') {
                            $nuevoDetalle->cuenta_corriente_id = $detalle['id'];
                            $nuevoDetalle->descripcion = "Cobro de Cuenta Corriente";
                        } elseif ($detalle['tipo'] == 'notas_debitos') {
                            //$nuevoDetalle->nota_debito_id = $detalle['id'];
                            //$nuevoDetalle->descripcion = "Cobro de Noda de Débito.";
                        }

                        $cuentaCorriente = $nuevoDetalle->cuentaCorriente;
                        $saldo = (float)$cuentaCorriente->debe - (float)$cuentaCorriente->haber; // Lo que le falta pagar de esa factura
                        if ($auxiliarCobro >= $saldo) { // Si lo que esta pagando es mayor a lo que debe de esa factura
                            $nuevoDetalle->monto = $saldo;
                            $cuentaCorriente->haber = $cuentaCorriente->debe; // Iguala el debe y el haber para cancelar ese comprobante y lo finaliza
                            $cuentaCorriente->estado = EstadoCuentaCorriente::FINALIZADA;

                            if(FacturaVentaAutomatica::esFacturaAutomatica($cuentaCorriente->entidad_id, $cuentaCorriente->url)){
                                FacturaVentaAutomatica::cancelar($cuentaCorriente->entidad_id);
                            }

                            $auxiliarCobro -= $saldo; // Guarda la plata que queda a favor en la variable para restarsela al siguiente detalle (comprobante)
                        } else {
                            $nuevoDetalle->monto = $auxiliarCobro;
                            $cuentaCorriente->haber = $cuentaCorriente->haber + $auxiliarCobro;
                            $auxiliarCobro = 0;
                        }
                        $nuevoDetalle->save();
                        $cuentaCorriente->update();

                        // Resto este importe tambien al saldo del cliente
                        $recibo->cliente->saldo_actual = $recibo->cliente->saldo_actual - $nuevoDetalle->monto;
                        $recibo->cliente->save();
                    }
                }
            }

            if($auxiliarCobro > 0){ // Si pago mas de lo que debia, guardo el recibo en la cuenta corriente como dinero a favor
                $controllerCuentaCorriente = new CuentaCorrienteController();
                $cuentaCorriente = $controllerCuentaCorriente->haber($recibo, $request, null, $auxiliarCobro);

                // Le creo el detalle
                $nuevoDetalle = New DetalleRecibo();
                $nuevoDetalle->recibo_id = $recibo->id;
                $nuevoDetalle->cuenta_corriente_id = $cuentaCorriente->id;
                $nuevoDetalle->descripcion = "Cobro de Cuenta Corriente";
                $nuevoDetalle->monto = $auxiliarCobro;
                $nuevoDetalle->save();
            }

            if(isset($requestData['detalles_cobro'])) {
                $detallesCobro = $requestData['detalles_cobro'];
                $controllerMovimientoCaja = new MovimientoCajaController();
                foreach ($detallesCobro as $cobro) {
                    if ((int)$cobro['estado'] == Estado::ACTIVO) {
                        $nuevoCobro = New DetalleCobroRecibo($cobro);
                        $nuevoCobro->recibo_id = $recibo->id;
                        $nuevoCobro->estado = Estado::ACTIVO;

                        //TODO ver q hacer con el interes, cambiar este método, no ayuda...
                        $controllerMovimientoCaja->nuevaEntrada($recibo, 'ventas/recibo', (int)$cobro['forma_cobro'], ConceptoDeCaja::COBRO_RECIBO, (float)$cobro['monto']);
                        if((int)$cobro['forma_cobro'] == FormaDePago::EFECTIVO) {
                            $nuevoCobro->descripcion = 'Cobro en Efectivo';
                            $nuevoCobro->forma_cobro = FormaDePago::EFECTIVO;
                        }else if((int)$cobro['forma_cobro'] == FormaDePago::MERCADO_PAGO) {
                            $nuevoCobro->descripcion = 'Cobro en Mercado Pago';
                            $nuevoCobro->forma_cobro = FormaDePago::MERCADO_PAGO;
                        }else if((int)$cobro['forma_cobro'] == FormaDePago::CHEQUE) {
                            $controllerCheque = new ChequeController();
                            $nuevoCheque = $controllerCheque->nuevoCheque($cobro, (float)$cobro['monto']);

                            $nuevoCobro->entidad_id = $nuevoCheque->id;
                            $nuevoCobro->descripcion = 'Cobro con Cheque';
                            $nuevoCobro->forma_cobro = FormaDePago::CHEQUE;
                        }else if((int)$cobro['forma_cobro'] == FormaDePago::CUENTA_BANCARIA) {
                            $nuevoMovimientoBancario = new MovimientoBancario($cobro);
                            $nuevoMovimientoBancario->importe = (float)$cobro['monto'];
                            $nuevoMovimientoBancario->cuenta_bancaria_destino_id = $cobro['cuenta_bancaria_id'];
                            $nuevoMovimientoBancario->moneda_id = $recibo->moneda_id;
                            $nuevoMovimientoBancario->tipo = TipoMovimiento::DEPOSITO;
                            $nuevoMovimientoBancario->fecha = $date;
                            $nuevoMovimientoBancario->empresa_id = $empresa->id;
                            $nuevoMovimientoBancario->entidad_id = $recibo->id;
                            $nuevoMovimientoBancario->entidad_clase = class_basename($recibo);
                            $nuevoMovimientoBancario->url = 'ventas/recibo';
                            $nuevoMovimientoBancario->save();

                            $nuevoCobro->entidad_id = $nuevoMovimientoBancario->id;
                            $nuevoCobro->descripcion = 'Cobro con Depósito Bancario';
                            $nuevoCobro->forma_cobro = FormaDePago::CUENTA_BANCARIA;
                        }else if((int)$cobro['forma_cobro'] == FormaDePago::TARJETA) {
                            $nuevoCuponTarjeta = new CuponTarjeta($cobro);
                            $nuevoCuponTarjeta->fecha = $date;
                            $nuevoCuponTarjeta->entidad_relacionada_link_id = $entidadRelacionadaLink->id;
                            $nuevoCuponTarjeta->importe = (float)$cobro['monto'] + (float)$cobro['interes'];
                            $nuevoCuponTarjeta->autorizacion = 'Autorizada';
                            $nuevoCuponTarjeta->estado = Estado::PENDIENTE;
                            $nuevoCuponTarjeta->save();

                            $nuevoCobro->entidad_id = $nuevoCuponTarjeta->id;
                            $nuevoCobro->descripcion = 'Cobro con Tarjeta';
                            $nuevoCobro->forma_cobro = FormaDePago::TARJETA;
                        }
                        $nuevoCobro->save();
                    }
                }
            }

            $controllerHistorial = new HistorialController();
            $controllerHistorial->nuevoHistorialCreacion($recibo, $request);

            Session::flash('flash_success', 'Recibo creado correctamente.');
        });

        return redirect('ventas/recibo');
    }

    public function show($id){
        $empresa = Auth::user()->empresa;

        $recibo = Recibo::findOrFail($id);

        if ($recibo->empresa == $empresa) {
            return view('frontend.ventas.recibo.show', compact('recibo','empresa'));
        }
        return redirect('ventas/recibo');
    }

    public function edit($id){
        return redirect('ventas/recibo');
    }

    public function update($id, Request $request){
        return redirect('ventas/recibo');
    }

    public function destroy($id){
        $empresa = Auth::user()->empresa;

        $recibo = Recibo::findOrFail($id);

        if ($recibo->empresa == $empresa) {
            $recibo->delete();

            Session::flash('flash_success', 'Recibo eliminado correctamente.');
        }
        return redirect('ventas/recibo');
    }

    public function crearReciboCliente($cliente_id){
        if(isset(Auth::user()->empleado->caja)){
            if(Auth::user()->empleado->caja->estado != EstadoCaja::ABIERTA) {
                return redirect('ventas/recibo');
            }
        }

        $empresa = Auth::user()->empresa;

        $cliente = Cliente::findOrFail($cliente_id);

        if ($cliente->empresa == $empresa) {
            $fecha_hoy = date('d/m/Y');
            $monedas = Moneda::getMonedasPorEmpresaArray($empresa);
            $monedaPorDefecto = Moneda::where('empresa_id', $empresa->id)->whereNull('deleted_at')->where('cotizacion','=', 1)->first();
            $numero_recibo = Opcion::getValorOpcion('numeracion_recibo', $empresa);
            $detalles = CuentaCorriente::getCuentasCorrientesParaRecibo($cliente);

            $formas_cobro = FormaDePago::$getArrayRecibo;
            $bancos = Banco::getBancosPorEmpresaArray($empresa);
            $tarjetas = Tarjeta::getTarjetasPorEmpresaArray($empresa);
            $tipos_cuenta_corriente = TipoCuentaCorriente::getTiposArray($empresa);
            $cuentas_bancarias = CuentaBancaria::getCuentasBancariasPorEmpresaArray($empresa);

            //TODO combinar de alguna forma con las ND en la misma coleccion

            return view('frontend.ventas.recibo.create', compact('cliente', 'fecha_hoy', 'monedas', 'numero_recibo', 'detalles', 'formas_cobro', 'bancos', 'tarjetas', 'cuentas_bancarias', 'tipos_cuenta_corriente', 'monedaPorDefecto'));
        }

        return redirect('ventas/recibo');
    }

    public function imprimir($recibo_id){
        $empresa = Auth::user()->empresa;
        $recibo = Recibo::findOrFail($recibo_id);

        if ($recibo->empresa_id == $empresa->id) {
            $porcentajes_iva = PorcentajeIva::$valores;

            $pdf = app('dompdf.wrapper');
            $pdf->getDomPDF()->set_option("enable_php", true);
            $pdf->loadView('frontend.ventas.recibo.imprimir-recibo',
                compact('recibo','empresa', 'porcentajes_iva'));

            if (Auth::user()->hasRole('Usuario Vendedor')) {
                if ($recibo->sucursal_id == Auth::user()->sucursal_id) {
                    return $pdf->stream('Recibo.pdf');
                }
            } else {
                return $pdf->stream('Recibo.pdf');
            }
        }
    }
}
