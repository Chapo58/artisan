<?php

namespace App\Http\Controllers\Frontend\Ventas;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Frontend\Configuraciones\HistorialController;
use App\Http\Controllers\Frontend\Fondos\CuentaBancariaController;
use App\Models\Frontend\Fondos\TipoMovimiento;
use App\Models\Frontend\Ventas\CuponTarjeta;
use App\Models\Frontend\Fondos\CuentaBancaria;
use App\Models\Frontend\Fondos\MovimientoBancario;
use App\Models\Frontend\Caja\FormaDePago;
use App\Models\Frontend\Caja\ConceptoDeCaja;
use App\Http\Controllers\Frontend\Caja\MovimientoCajaController;
use App\Models\Frontend\Configuraciones\Tarjeta;
use App\Models\Frontend\Configuraciones\PlanTarjeta;
use App\Models\Frontend\Configuraciones\Estado;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\HtmlString;
use Yajra\Datatables\Facades\Datatables;
use Carbon\Carbon;
use DB;
use Session;
use DateTime;

class CuponTarjetaController extends Controller{

    public function index(){
        $empresa = Auth::user()->empresa;

        $cuentaBancaria = CuentaBancaria::getCuentasBancariasPorEmpresaArray($empresa);

        return view('frontend.ventas.cupon-tarjeta.index', compact('cuentaBancaria'));
    }

    public function datos(){
        $empresa = Auth::user()->empresa;
        $estados = Estado::$getArray;

        $query = CuponTarjeta::select('cupon_tarjetas.id', 'cupon_tarjetas.fecha', DB::raw("CONCAT(cupon_tarjetas.lote,' / ',cupon_tarjetas.cupon) AS lote"), 'cupon_tarjetas.importe', 'cupon_tarjetas.estado', 'tarjetas.nombre as tarjeta', 'plan_tarjetas.nombre as planTarjeta');
        $query->join('tarjetas', 'tarjetas.id', '=', 'cupon_tarjetas.tarjeta_id');
        $query->join('plan_tarjetas', 'plan_tarjetas.id', '=', 'cupon_tarjetas.plan_tarjeta_id');
        $query->where('tarjetas.empresa_id', $empresa->id);
        $query->orderBy('cupon_tarjetas.id', 'DESC');
        $cuponesTarjetas = $query->get();

        return Datatables::of($cuponesTarjetas)
            ->addColumn('action', function ($cuponTarjeta) {
                $return = '
                 <a href="'.url('/ventas/cupon-tarjeta/' . $cuponTarjeta->id).'" class="mytooltip"><i class="fa fa-eye text-success m-r-10"></i>
                    <span class="tooltip-content3">Ver Cupon</span>
                 </a>';
                if($cuponTarjeta->estado == Estado::PENDIENTE){
                    $return.='
                        <a href="javascript:void(0)" class="mytooltip" data-backdrop="static" onclick="pasarId('.$cuponTarjeta->id.')" data-toggle="modal" data-target="#modalAcreditar">
                            <i class="fa fa-ticket text-warning m-r-10"></i>
                            <span class="tooltip-content3">Acreditar Cupon</span>
                        </a>';
                }
                $return.='
                <a href="'.url('/ventas/cupon-tarjeta/' . $cuponTarjeta->id . '/edit').'" class="mytooltip">
                    <i class="fa fa-pencil text-info m-r-10"></i>
                    <span class="tooltip-content3">Modificar Cupon</span>
                </a>
                <a class="delete mytooltip pointer" data-id="'.$cuponTarjeta->id.'">
                    <i class="fa fa-close text-danger"></i>
                    <span class="tooltip-content3">Eliminar Cupon</span>
                </a>';
                return $return;
            })
            ->setRowId('id')
            ->removeColumn('id')
            ->editColumn('fecha', function ($cuponTarjeta) {
                return $cuponTarjeta->fecha->format('d/m/Y');
            })
            ->editColumn('importe', '$ {{$importe}}')
            ->editColumn('estado', function ($cuponTarjeta) use ($estados) {
                switch ($cuponTarjeta->estado) {
                    case Estado::PENDIENTE:
                        return new HtmlString('<span class="label label-warning">'.$estados[$cuponTarjeta->estado].'</span>');
                        break;
                    case Estado::ACREDITADO:
                        return new HtmlString('<span class="label label-success">'.$estados[$cuponTarjeta->estado].'</span>');
                        break;
                    default:
                        return new HtmlString('<span class="label label-info">'.$estados[$cuponTarjeta->estado].'</span>');
                }
            })
            ->make(true);
    }

    public function create(){
        $empresa = Auth::user()->empresa;

        $tarjetas = Tarjeta::getTarjetasPorEmpresaArray($empresa);
        $planes_tarjeta = PlanTarjeta::getPlanesPorEmpresaArray($empresa);
        $fecha = Carbon::now();

        return view('frontend.ventas.cupon-tarjeta.create', compact('tarjetas', 'planes_tarjeta', 'fecha'));
    }

    public function store(Request $request){
        $this->validate($request, [
            'fecha' => 'required',
            'importe' => 'required',
            'tarjeta_id' => 'required',
            'plan_tarjeta_id' => 'required',
            'lote' => 'required',
            'cupon' => 'required'
        ]);

        DB::transaction(function() use ($request) {
            $requestData = $request->all();

            $cupon = CuponTarjeta::create($requestData);

            // Entrada en tarjeta de la caja
            $entradaTarjeta = new MovimientoCajaController();
            $entradaTarjeta->nuevaEntrada($cupon, $request->getRequestUri(),FormaDePago::TARJETA,ConceptoDeCaja::INGRESOS_VARIOS);

            $controllerHistorial = new HistorialController();
            $controllerHistorial->nuevoHistorialCreacion($cupon, $request);

            Session::flash('flash_success', 'Cupon de Tarjeta creado correctamente.');
        });

        return redirect('ventas/cupon-tarjeta');
    }

    public function acreditar(Request $request){
        $empresa = Auth::user()->empresa;

        $idCupon = $request['idCupon'];
        $idCuentaBancaria = $request['cuenta_bancaria_id'];

        // Cambio de Estado del Cupon
        $cupon = CuponTarjeta::findOrFail($idCupon);
        $cupon->estado = Estado::ACREDITADO;
        $cupon->fecha_acreditacion = Carbon::now();
        $cupon->cuenta_bancaria_id = $idCuentaBancaria;
        $cupon->save();

        // Generacion del movimiento bancario (deposito)
        $MovimientoBancario = new MovimientoBancario;
        $MovimientoBancario->importe = $cupon->importe;
        $MovimientoBancario->cuenta_bancaria_destino_id = $idCuentaBancaria;
        $MovimientoBancario->moneda_id = 1;
        $MovimientoBancario->tipo = TipoMovimiento::DEPOSITO;
        $MovimientoBancario->fecha = Carbon::now();
        $MovimientoBancario->comentario = "Acreditacion Cupon: ".$cupon->lote." / ".$cupon->cupon;
        $MovimientoBancario->entidad_id = $idCupon;
        $MovimientoBancario->entidad_clase = class_basename($cupon);
        $MovimientoBancario->empresa_id = $empresa->id;
        $MovimientoBancario->save();

        // Carga del deposito al saldo de la cuenta
        $cuentaBancariaController = new CuentaBancariaController;
        $cuentaBancariaController->deposito($idCuentaBancaria,$cupon->importe);

        // Entrada en cuenta bancaria de la caja
        $entradaBanco = new MovimientoCajaController();
        $entradaBanco->nuevaEntrada($cupon,'ventas/cupon-tarjeta',FormaDePago::CUENTA_BANCARIA,ConceptoDeCaja::ACREDITACION_CUPON);

        return redirect('fondos/movimiento-bancario');
    }

    public function show($id){
        $empresa = Auth::user()->empresa;
        $cuponTarjeta = CuponTarjeta::findOrFail($id);

        if($cuponTarjeta->tarjeta->empresa == $empresa) {
            $estados = Estado::$getArray;
            if(Auth::user()->hasRole('Usuario Vendedor')) {
                //TODO ver q hacer si cargo un cupon sin venta
//                if($cuponTarjeta->sucursal_id == Auth::user()->sucursal_id) {
                    return view('frontend.ventas.cupon-tarjeta.show', compact('cuponTarjeta','estados'));
//                }
            }else{
                return view('frontend.ventas.cupon-tarjeta.show', compact('cuponTarjeta','estados'));
            }
        }

        return redirect('ventas/cupon-tarjeta');
    }

    public function edit($id){
        $empresa = Auth::user()->empresa;
        $cuponTarjeta = CuponTarjeta::findOrFail($id);

        if($cuponTarjeta->tarjeta->empresa == $empresa) {
            $tarjetas = Tarjeta::getTarjetasPorEmpresaArray($empresa);
            $planes_tarjeta = PlanTarjeta::getPlanesPorEmpresaArray($empresa);
            $fecha = Carbon::now();

            if(Auth::user()->hasRole('Usuario Vendedor')) {
                //TODO ver q hacer si cargo un cupon sin venta
//                if($cuponTarjeta->sucursal_id == Auth::user()->sucursal_id) {
                    return view('frontend.ventas.cupon-tarjeta.edit', compact('cuponTarjeta', 'tarjetas', 'planes_tarjeta', 'fecha'));
//                }
            }else{
                return view('frontend.ventas.cupon-tarjeta.edit', compact('cuponTarjeta', 'tarjetas', 'planes_tarjeta', 'fecha'));
            }
        }

        return redirect('ventas/cupon-tarjeta');
    }

    public function update($id, Request $request){
        $this->validate($request, [
            'fecha' => 'required',
            'importe' => 'required',
            'tarjeta_id' => 'required',
            'plan_tarjeta_id' => 'required',
            'lote' => 'required',
            'cupon' => 'required'
        ]);
        $cuponTarjeta = CuponTarjeta::findOrFail($id);

        DB::transaction(function() use ($cuponTarjeta, $request) {
            $requestData = $request->all();

            $date = DateTime::createFromFormat('d/m/Y', $requestData['fecha']);
            $cuponTarjeta->fecha = $date->format('Y-m-d H:i:s');
            $cuponTarjeta->importe = $requestData['importe'];
            $cuponTarjeta->tarjeta_id = $requestData['tarjeta_id'];
            $cuponTarjeta->plan_tarjeta_id = $requestData['plan_tarjeta_id'];
            $cuponTarjeta->lote = $requestData['lote'];
            $cuponTarjeta->cupon = $requestData['cupon'];
            $cuponTarjeta->autorizacion = $requestData['autorizacion'];
            $cuponTarjeta->titular = $requestData['titular'];
            $cuponTarjeta->update();

            $controllerHistorial = new HistorialController();
            $controllerHistorial->nuevoHistorialActualizacion($cuponTarjeta, $request);

            Session::flash('flash_success', 'Cupon de Tarjeta modificado correctamente.');
        });

        return redirect('ventas/cupon-tarjeta');
    }

    public function destroy($id){
        $empresa = Auth::user()->empresa;
        $cuponTarjeta = CuponTarjeta::findOrFail($id);

        if($cuponTarjeta->tarjeta->empresa == $empresa) {
            DB::transaction(function() use ($cuponTarjeta) {
                $cuponTarjeta->delete();

                $controllerHistorial = new HistorialController();
                $controllerHistorial->nuevoHistorialEliminacion($cuponTarjeta);
            });

            Session::flash('flash_success', 'Cupon de Tarjeta eliminado correctamente.');
        }

        return redirect('ventas/cupon-tarjeta');
    }
}
