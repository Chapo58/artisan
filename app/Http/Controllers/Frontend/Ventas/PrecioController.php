<?php

namespace App\Http\Controllers\Frontend\Ventas;

use App\Http\Controllers\Controller;
use App\Models\Frontend\Ventas\ListaPrecio;
use App\Models\Frontend\Ventas\Precio;
use Illuminate\Http\Request;
use Session;

class PrecioController extends Controller{

    public function index(Request $request){
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $precio = Precio::where('precio', 'LIKE', "%$keyword%")
				->orWhere('fecha_hasta', 'LIKE', "%$keyword%")
				->orWhere('articulo_id', 'LIKE', "%$keyword%")
				->orWhere('lista_precio_id', 'LIKE', "%$keyword%")
				
                ->paginate($perPage);
        } else {
            $precio = Precio::paginate($perPage);
        }

        return view('frontend.ventas.precio.index', compact('precio'));
    }

    public function create(Request $request){
        $id = $request->route('id');
        $lista_precios = ListaPrecio::findOrFail($id);

        return view('frontend.ventas.precio.create', compact('lista_precios'));
    }

    public function store(Request $request){
        $this->validate($request, [
			'precio' => 'required',
			'articulo_id' => 'required',
			'lista_precio_id' => 'required'
		]);
        $requestData = $request->all();
        
        $precio = Precio::create($requestData);

        return redirect('ventas/lista-precios/'.$precio->listaPrecio->id)->withFlashSuccess('Precio creado correctamente.');

    }

    public function show($id){
        $precio = Precio::findOrFail($id);

        if($precio->articulo->empresa_id == Auth::user()->empresa_id){
            return view('frontend.ventas.precio.show', compact('precio'));
        }
    }

    public function edit($id){
        $precio = Precio::findOrFail($id);

        if($precio->articulo->empresa_id == Auth::user()->empresa_id){
            return view('frontend.ventas.precio.edit', compact('precio'));
        }
    }

    public function update($id, Request $request){
        $this->validate($request, [
			'precio' => 'required',
			'articulo_id' => 'required',
			'lista_precio_id' => 'required'
		]);
        $requestData = $request->all();

        $precio = Precio::findOrFail($id);
        if($precio->articulo->empresa_id == Auth::user()->empresa_id){
            $precio->numero = $requestData['numero'];
            $precio->precio = $requestData['precio'];
            $precio->update();

            Session::flash('flash_success', 'Precio modificado correctamente.');
        }

        return redirect('ventas/lista-precios/'.$precio->listaPrecio->id);
    }

    public function destroy($id){
        $precio = Precio::findOrFail($id);
        if($precio->articulo->empresa_id == Auth::user()->empresa_id){
            Precio::destroy($id);
            Session::flash('flash_success', 'Precio eliminado correctamente.');
        }
        return redirect('ventas/precio');
    }
}
