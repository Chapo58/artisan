<?php

namespace App\Http\Controllers\Frontend\Ventas;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Frontend\Caja\MovimientoCajaController;
use App\Http\Controllers\Frontend\Configuraciones\HistorialController;
use App\Models\Frontend\Articulos\Articulo;
use App\Models\Frontend\Caja\ConceptoDeCaja;
use App\Models\Frontend\Caja\EstadoCaja;
use App\Models\Frontend\Caja\FormaDePago;
use App\Models\Frontend\Configuraciones\Estado;
use App\Models\Frontend\Configuraciones\Moneda;
use App\Models\Frontend\Configuraciones\Opcion;
use App\Models\Frontend\Configuraciones\PorcentajeIva;
use App\Models\Frontend\Configuraciones\PuntoDeVenta;
use App\Models\Frontend\Configuraciones\Tarjeta;
use App\Models\Frontend\Fondos\ChequePropio;
use App\Models\Frontend\Fondos\MovimientoBancario;
use App\Models\Frontend\Fondos\TipoMovimiento;
use App\Models\Frontend\Ventas\ConceptoNotaCredito;
use App\Models\Frontend\Ventas\DetalleNotaCredito;
use App\Models\Frontend\Ventas\DetallePagoNotaCredito;
use App\Models\Frontend\Ventas\EstadoFacturacion;
use App\Models\Frontend\Configuraciones\TipoComprobante;
use App\Models\Frontend\Fondos\Cheque;
use App\Models\Frontend\Fondos\Chequera;
use App\Models\Frontend\Fondos\CuentaBancaria;
use App\Models\Frontend\Fondos\EstadoCheque;
use App\Models\Frontend\Ventas\ListaPrecio;
use App\Models\Frontend\Ventas\NotaCredito;
use Milon\Barcode\DNS1D;
use Yajra\Datatables\Facades\Datatables;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;
use DB;

class NotaCreditoController extends Controller{

    public function index(){

        $estadoCaja = EstadoCaja::CERRADA;
        if(isset(Auth::user()->empleado->caja)){
            if(Auth::user()->empleado->caja->estado == EstadoCaja::ABIERTA) {
                $estadoCaja = EstadoCaja::ABIERTA;
            }
        }

        return view('frontend.ventas.nota-credito.index', compact('estadoCaja'));
    }

    public function datos(){
        $empresa = Auth::user()->empresa;
        $sucursal = Auth::user()->sucursal;

        $notasCredito = NotaCredito::listarNotasDeCreditoPorEmpresaSucursal($empresa, $sucursal);

        $tipos_comprobantes = TipoComprobante::$comprobantes;

        return Datatables::of($notasCredito)
            ->addColumn('action', function ($notaCredito) {
                $return = '
                <a href="'.url('/ventas/nota-credito/' . $notaCredito->id).'" class="mytooltip"><i class="fa fa-eye text-success m-r-10"></i>
                    <span class="tooltip-content3">Ver Detalles de la Nota De Credito</span>
                </a>';
                if($notaCredito->tipo_punto_id != 1) {
                    if (!isset($notaCredito->estado_facturacion) || $notaCredito->estado_facturacion == EstadoFacturacion::FACTURADO){
                        $return .= '
                        <a href="' . url('/ventas/nota-credito/imprimir/' . $notaCredito->id) . '" class="mytooltip">
                            <i class="fa fa-print text-purple m-r-10"></i>
                            <span class="tooltip-content3">Imprimir Comprobante</span>
                        </a>';
                    }
                }
                if($notaCredito->estado_facturacion == EstadoFacturacion::ERROR || $notaCredito->estado_facturacion == EstadoFacturacion::PENDIENTE){
                    if($notaCredito->tipo_punto_id == 1 && Auth::user()->controladoraFiscal()){ // Si es una controladora fiscal y el usuario la tiene
                        $return .='
                            <a href="'.url('/ventas/facturacion-fiscal/facturarNotaDeCredito/' . $notaCredito->id).'" class="mytooltip">
                                <i class="fa fa-print text-green m-r-10"></i>
                                <span class="tooltip-content3">Facturar</span>
                            </a>';
                    } elseif($notaCredito->tipo_punto_id == 3) {  // Si es una factura electronica
                        $return .='
                        <a href="'.url('/ventas/facturacion-electronica/facturarNotaDeCredito/' . $notaCredito->id).'" class="mytooltip">
                            <i class="fa fa-ticket text-green m-r-10"></i>
                            <span class="tooltip-content3">Facturar</span>
                        </a>';
                    }
                }
                if($notaCredito->estado_facturacion != EstadoFacturacion::FACTURADO){
                    $return .='
                        <a class="delete mytooltip pointer" data-id="'.$notaCredito->id.'">
                            <i class="fa fa-close text-danger"></i>
                            <span class="tooltip-content3">Eliminar Nota De Credito</span>
                        </a>';
                }
                return $return;
            })
            ->setRowId('id')
            ->removeColumn('id')
            ->removeColumn('estado_facturacion')
            ->removeColumn('tipo_punto_id')
            ->removeColumn('tipo_comprobante_id')
            ->removeColumn('signo')
            ->editColumn('fecha', function ($notaCredito) {
                return $notaCredito->fecha->format('d/m/Y H:i');
            })
            ->editColumn('total_pago', function ($notaCredito) {
                return $notaCredito->signo.' '.$notaCredito->total_pago;
            })
            ->editColumn('numero', function ($notaCredito) use ($tipos_comprobantes) {
                return $tipos_comprobantes[$notaCredito->tipo_comprobante_id].' '.$notaCredito->numero;
            })
            ->setRowClass(function ($notaCredito) {
                if($notaCredito->estado_facturacion == EstadoFacturacion::ERROR){
                    return 'table-danger';
                } elseif($notaCredito->estado_facturacion == EstadoFacturacion::PENDIENTE){
                    return 'table-warning';
                }
            })
            ->make(true);
    }

    public function create(){
        if(isset(Auth::user()->empleado->caja)){
            if(Auth::user()->empleado->caja->estado != EstadoCaja::ABIERTA) {
                return redirect('ventas/nota-credito');
            }
        }else{
            return redirect('ventas/nota-debito');
        }

        $empresa = Auth::user()->empresa;
        $sucursal = Auth::user()->sucursal;

        $fecha_hoy = date('d/m/Y');

        if($empresa->condicion_iva_id == 1){//Monotributista
            $numero_comprobante = Opcion::getValorOpcion('numeracion_nc_c', $empresa);
            $tipo_comprobante_por_defecto = TipoComprobante::NOTA_CREDITO_C;
        }else{//RI
            $numero_comprobante = Opcion::getValorOpcion('numeracion_nc_b', $empresa);
            $tipo_comprobante_por_defecto = TipoComprobante::NOTA_CREDITO_B;
        }

        $punto_de_venta_por_defecto = null;
        if(isset(Auth::user()->empleado->puntoVenta)){
            $punto_de_venta_por_defecto = Auth::user()->empleado->puntoVenta;
        }

        $tipos_comprobantes = TipoComprobante::getArrayComprobantes($empresa,'NotaCredito');
        $monedas = Moneda::getMonedasPorEmpresaArray($empresa);
        $monedaPorDefecto = Moneda::where('empresa_id', $empresa->id)->whereNull('deleted_at')->where('cotizacion','=', 1)->first();
        $lista_precios = ListaPrecio::getListasPorEmpresaArray($empresa);
        $lista_precios_defecto = Opcion::where('empresa_id', $empresa->id)
            ->where('nombre', 'lista_precios_defecto_id')
            ->first();
        $conceptos = ConceptoNotaCredito::$getArray;

        $formas_pagos = FormaDePago::$getArray;
        $tarjetas = Tarjeta::getTarjetasPorEmpresaArray($empresa);
        $cuentas_bancarias = CuentaBancaria::getCuentasBancariasPorEmpresaArray($empresa);
        $cheques_terceros = Cheque::getChequesPorEmpresaArray($empresa, EstadoCheque::PENDIENTE);
        $chequeras = Chequera::getChequerasPorEmpresaArray($empresa);

        if (Auth::user()->hasRole('Usuario Vendedor')) {
            $puntos_de_ventas = PuntoDeVenta::where('sucursal_id', $sucursal->id)->get()->pluck('numero', 'id')->toArray();
        }else{
            $puntos_de_ventas = PuntoDeVenta::where('empresa_id', $empresa->id)->get()->pluck('numero', 'id')->toArray();
        }

        return view('frontend.ventas.nota-credito.create', compact('fecha_hoy','numero_comprobante', 'tipos_comprobantes', 'tipo_comprobante_por_defecto', 'punto_de_venta_por_defecto', 'monedas', 'lista_precios', 'formas_pagos', 'bancos', 'tarjetas', 'cuentas_bancarias', 'puntos_de_ventas', 'cheques_terceros', 'chequeras', 'monedaPorDefecto', 'conceptos','lista_precios_defecto'));
    }

    public function store(Request $request){
        $this->validate($request, [
            'cliente_id' => 'required',
            'moneda_id' => 'required',
            'fecha' => 'required',
            'numero' => 'required',
            'tipo_comprobante_id' => 'required',
            'punto_venta_id' => 'required',
            'total' => 'required'
        ]);

        DB::transaction(function() use ($request, &$notaCredito) {
            $requestData = $request->all();
            $empresa = Auth::user()->empresa;
            $sucursal = Auth::user()->sucursal;

            $nombresClaves = [
                TipoComprobante::NOTA_CREDITO_A => ['numeracion_nc_a'],
                TipoComprobante::NOTA_CREDITO_B => ['numeracion_nc_b'],
                TipoComprobante::NOTA_CREDITO_C => ['numeracion_nc_c'],
                TipoComprobante::TIQUE_NOTA_CREDITO_A => ['numeracion_nc_a'],
                TipoComprobante::TIQUE_NOTA_CREDITO_B => ['numeracion_nc_b'],
                TipoComprobante::TIQUE_NOTA_CREDITO_C => ['numeracion_nc_c']
            ];

            $notaCredito = New NotaCredito($requestData);

            if($notaCredito->puntoVenta->esBlanco()){//si es en blanco busco la numeración del punto de venta
                $opcion = Opcion::where('empresa_id', $empresa->id)
                    ->where('nombre', 'LIKE', $nombresClaves[(int)$requestData['tipo_comprobante_id']])
                    ->where('punto_venta_id', $notaCredito->puntoVenta->id)
                    ->first();
                $numero_factura = $opcion->valor;
                $opcion->valor = str_pad($opcion->valor + 1, 8, '0', STR_PAD_LEFT);
                $opcion->save();

                $notaCredito->estado_facturacion = EstadoFacturacion::PENDIENTE; // Seteo el comprobante en pendiente por defecto
            }else{//si es Manual busco la numeración genérica
                $opcion = Opcion::where('empresa_id', $empresa->id)
                    ->where('nombre', 'LIKE', $nombresClaves[(int)$requestData['tipo_comprobante_id']])
                    ->first();
                $numero_factura = $opcion->valor;
                $opcion->valor = str_pad($opcion->valor + 1, 8, '0', STR_PAD_LEFT);
                $opcion->save();
            }
            $notaCredito->numero = $numero_factura;

            $date = DateTime::createFromFormat('d/m/Y', $requestData['fecha']);
            $notaCredito->fecha = $date->format('Y-m-d H:i:s');
            $notaCredito->estado = Estado::ACTIVO;
            $notaCredito->empresa_id = $empresa->id;
            $notaCredito->sucursal_id = $sucursal->id;
            $notaCredito->tipo_comprobante_id = $requestData['tipo_comprobante_id'];
            if($notaCredito->importe_neto == null)
                $notaCredito->importe_neto = 0;
            if($notaCredito->importe_iva == null)
                $notaCredito->importe_iva = 0;
            $notaCredito->save();

            if($notaCredito->concepto_nota_credito == ConceptoNotaCredito::ANULACION){
                if(isset($notaCredito->venta)){
                    if($notaCredito->venta->total_cobro == $notaCredito->total_pago){
                        $notaCredito->venta->estado_facturacion = EstadoFacturacion::ANULADO;
                        $notaCredito->update();
                    }
                }
            }

            $moneda = Moneda::findOrFail($notaCredito->moneda->id);
            $moneda->cotizacion = $requestData['cotizacion'];
            $moneda->save();

            $porcentajes_iva = PorcentajeIva::$valores;
            if(isset($requestData['detalles'])) {
                $detalles = $requestData['detalles'];
                foreach ($detalles as $detalle) {
                    if((int)$detalle['estado'] == Estado::ACTIVO && isset($detalle['cantidad']) && isset($detalle['articulo_id'])){
                        $nuevoDetalle = New DetalleNotaCredito($detalle);
                        $nuevoDetalle->nota_credito_id = $notaCredito->id;
                        $nuevoDetalle->precio_iva = $detalle['precio_neto'] * ($porcentajes_iva[$detalle['porcentaje_iva']] / 100);
                        $nuevoDetalle->save();

                        $articulo = Articulo::findOrFail((int)$detalle['articulo_id']);
                        $articulo->sumarStock((int)$detalle['cantidad'],$sucursal);
                    }
                }
            }

            if(isset($requestData['detalles_pago'])) {
                $detalles_pago = $requestData['detalles_pago'];
                $controllerMovimientoCaja = new MovimientoCajaController();
                foreach ($detalles_pago as $pago) {
                    if ((int)$pago['estado'] == Estado::ACTIVO) {
                        $nuevoPago = New DetallePagoNotaCredito($pago);
                        $nuevoPago->fecha = $date;
                        $nuevoPago->nota_credito_id = $notaCredito->id;
                        $nuevoPago->estado = Estado::ACTIVO;

                        $controllerMovimientoCaja->nuevaSalida($notaCredito, 'ventas/nota-credito', (int)$pago['forma_pago'], ConceptoDeCaja::NOTA_CREDITO, (float)$pago['monto']);
                        if((int)$pago['forma_pago'] == FormaDePago::EFECTIVO) {
                            $nuevoPago->descripcion = 'Pago en Efectivo';
                            $nuevoPago->forma_pago = FormaDePago::EFECTIVO;
                        }else if((int)$pago['forma_pago'] == FormaDePago::MERCADO_PAGO) {
                            $nuevoPago->descripcion = 'Pago en Mercado Pago';
                            $nuevoPago->forma_pago = FormaDePago::MERCADO_PAGO;
                        }else if((int)$pago['forma_pago'] == FormaDePago::CHEQUE) {//Cheque de terceros
                            $cheque = Cheque::findOrFail((int)$pago['cheque_id']);

                            $cheque->estado = EstadoCheque::ENTREGADO;
                            $cheque->save();

                            $nuevoPago->entidad_id = $cheque->id;
                            $nuevoPago->descripcion = 'Pago con Cheque';
                            $nuevoPago->forma_pago = FormaDePago::CHEQUE;
                        }else if((int)$pago['forma_pago'] == FormaDePago::CUENTA_BANCARIA) {
                            $nuevoMovimientoBancario = new MovimientoBancario($pago);
                            $nuevoMovimientoBancario->importe = (float)$pago['monto'];
                            $nuevoMovimientoBancario->cuenta_bancaria_origen_id = $pago['cuenta_bancaria_id'];
                            $nuevoMovimientoBancario->moneda_id = $notaCredito->moneda_id;
                            $nuevoMovimientoBancario->tipo = TipoMovimiento::TRANSFERENCIA;
                            $nuevoMovimientoBancario->fecha = $date;
                            $nuevoMovimientoBancario->empresa_id = $empresa->id;
                            $nuevoMovimientoBancario->entidad_id = $notaCredito->id;
                            $nuevoMovimientoBancario->entidad_clase = class_basename($notaCredito);
                            $nuevoMovimientoBancario->url = 'ventas/nota-credito';
                            $nuevoMovimientoBancario->save();

                            $nuevoPago->entidad_id = $nuevoMovimientoBancario->id;
                            $nuevoPago->descripcion = 'Pago con Tranferencia Bancaria';
                            $nuevoPago->forma_pago = FormaDePago::CUENTA_BANCARIA;
                        }else if((int)$pago['forma_pago'] == FormaDePago::TARJETA) {
                            $nuevoPago->descripcion = 'Pago con Tarjeta';
                            $nuevoPago->forma_pago = FormaDePago::TARJETA;
                        }else if((int)$pago['forma_pago'] == FormaDePago::CUENTA_CORRIENTE) {
                            $controllerCuentaCorriente = new CuentaCorrienteController();
                            $controllerCuentaCorriente->haber($notaCredito, $request, null, $pago['monto']);

                            $nuevoPago->descripcion = 'Pago en Cuenta Corriente';
                            $nuevoPago->forma_pago = FormaDePago::CUENTA_CORRIENTE;
                        }else if((int)$pago['forma_pago'] == FormaDePago::CHEQUE_PROPIO) {
                            $chequePropio = new ChequePropio($pago);
                            if(isset($pago['emision'])) {
                                $date = DateTime::createFromFormat('d/m/Y', $pago['emision']);
                                $chequePropio->emision = $date->format('Y-m-d H:i:s');
                            }
                            if(isset($pago['vencimiento'])){
                                $date = DateTime::createFromFormat('d/m/Y', $pago['vencimiento']);
                                $chequePropio->vencimiento = $date->format('Y-m-d H:i:s');
                            }
                            $chequePropio->estado = EstadoCheque::ENTREGADO;
                            $chequePropio->cuenta_bancaria_id = $chequePropio->chequera->cuenta_bancaria_id;
                            $chequePropio->empresa_id = $empresa->id;
                            $chequePropio->save();

                            $nuevoPago->descripcion = 'Pago con Cheque Propio Nº '.$chequePropio->numero;
                            $nuevoPago->forma_pago = FormaDePago::CHEQUE_PROPIO;
                        }
                        $nuevoPago->save();
                    }
                }
            }

            $controllerHistorial = new HistorialController();
            $controllerHistorial->nuevoHistorialCreacion($notaCredito, $request);

            Session::flash('flash_success', 'Nota de credito creada correctamente.');
        });

        $porcentajes_iva = PorcentajeIva::$valores;

        if($notaCredito->puntoVenta->tipo->nombre == "Impresora Fiscal"){
            if(Auth::user()->controladoraFiscal()){
                $facturar = New FacturacionFiscal();
                return $facturar->facturarNotaDeCredito($notaCredito->id);
            } else {
                return redirect('ventas/nota-credito');
            }
        } elseif($notaCredito->puntoVenta->tipo->nombre == "Comandera"){

            $empresa = Auth::user()->empresa;
            return view('frontend.ventas.nota-credito.imprimir-ticket', compact('notaCredito','empresa','porcentajes_iva'));

        } elseif($notaCredito->puntoVenta->tipo->nombre == "Factura Electrónica"){

            $facturar = New FacturacionElectronica();
            return $facturar->facturarNotaDeCredito($notaCredito->id);

        } else { // Punto de venta de facturacion manual
            return redirect('ventas/nota-credito');
        }

    }

    public function show($id){
        $notacredito = NotaCredito::findOrFail($id);
        $empresa = Auth::user()->empresa;

        if ($notacredito->empresa_id == $empresa->id) {
            $porcentajes_iva = PorcentajeIva::$valores;
            $tipos_comprobantes = TipoComprobante::$comprobantes;
            $conceptos = ConceptoNotaCredito::$getArray;
            if($notacredito->cae){
                $cuit = str_replace('-', '', $empresa->cuit);
                $tipoComprobante = $notacredito->tipo_comprobante_id;
                $puntoVenta = $notacredito->puntoVenta->numero;
                $cae = $notacredito->cae;
                $fechaVencimiento = $notacredito->fecha_vencimiento->format('Ymd');
                $numero = $cuit . $tipoComprobante. $puntoVenta . $cae . $fechaVencimiento;
                $ventaController = New VentaController();
                $digitoVerificador = $ventaController->digitoVerificador($numero);
                $numeroCodigoBarras = $numero.$digitoVerificador;
                $codigoBarras = DNS1D::getBarcodeHTML($numeroCodigoBarras, "UPCA",3,50);
            } else {
                $codigoBarras = NULL;
                $numeroCodigoBarras = NULL;
                $cae = NULL;
            }
            if (Auth::user()->hasRole('Usuario Vendedor')) {
                $sucursal = Auth::user()->sucursal;
                if ($notacredito->sucursal_id == $sucursal->id) {
                    return view('frontend.ventas.nota-credito.show', compact('notacredito', 'tipos_comprobantes', 'porcentajes_iva', 'conceptos', 'empresa','codigoBarras','numeroCodigoBarras','cae'));
                }
            } else {
                return view('frontend.ventas.nota-credito.show', compact('notacredito', 'tipos_comprobantes', 'porcentajes_iva', 'conceptos', 'empresa','codigoBarras','numeroCodigoBarras','cae'));
            }
        }
        return redirect('ventas/nota-credito');
    }

    public function edit($id){
        return redirect('ventas/nota-credito');
    }

    public function update($id, Request $request){
        return redirect('ventas/nota-credito');
    }

    public function destroy($id){
        $notaCredito = NotaCredito::findOrFail($id);
        $empresa = Auth::user()->empresa;

        if (!$notaCredito->empresa_id == $empresa->id) {
            return redirect('ventas/nota-credito');
        }else{
            if (Auth::user()->hasRole('Usuario Vendedor')) {
                $sucursal = Auth::user()->sucursal;
                if (!$notaCredito->sucursal_id == $sucursal->id) {
                    return redirect('ventas/nota-credito');
                }
            }
        }

        DB::transaction(function() use ($notaCredito) {
            $sucursal = $notaCredito->sucursal;
            if(!$notaCredito->detalles->isEmpty() && $sucursal){
                foreach($notaCredito->detalles as $detalle){
                    if(isset($detalle->articulo)){
                        $detalle->articulo->descontarStock($detalle->cantidad,$sucursal);
                    }
                    $detalle->delete();
                }
            }

            $notaCredito->delete();

            $controllerHistorial = new HistorialController();
            $controllerHistorial->nuevoHistorialEliminacion($notaCredito);

            Session::flash('flash_success', 'Nota de credito eliminada correctamente.');
        });

        return redirect('ventas/nota-credito');
    }

    public function imprimir($nota_id){
        $empresa = Auth::user()->empresa;
        $notaCredito = NotaCredito::findOrFail($nota_id);

        if ($notaCredito->empresa_id == $empresa->id) {
            $tipos_comprobantes = TipoComprobante::$comprobantes;
            $porcentajes_iva = PorcentajeIva::$valores;
            if($notaCredito->cae){
                $cuit = str_replace('-', '', $empresa->cuit);
                $tipoComprobante = $notaCredito->tipo_comprobante_id;
                $puntoVenta = $notaCredito->puntoVenta->numero;
                $cae = $notaCredito->cae;
                $fechaVencimiento = $notaCredito->fecha_vencimiento->format('Ymd');
                $numero = $cuit . $tipoComprobante. $puntoVenta . $cae . $fechaVencimiento;
                $ventaController = New VentaController();
                $digitoVerificador = $ventaController->digitoVerificador($numero);
                $numeroCodigoBarras = $numero.$digitoVerificador;
                $codigoBarras = DNS1D::getBarcodeHTML($numeroCodigoBarras, "UPCA");
            } else {
                $codigoBarras = NULL;
                $numeroCodigoBarras = NULL;
                $cae = NULL;
            }

            $pdf = app('dompdf.wrapper');
            $pdf->getDomPDF()->set_option("enable_php", true);
            $pdf->loadView('frontend.ventas.nota-credito.imprimir-nota-credito',
                compact('notaCredito','empresa','tipos_comprobantes', 'porcentajes_iva','codigoBarras','numeroCodigoBarras','cae'));

            if (Auth::user()->hasRole('Usuario Vendedor')) {
                if ($notaCredito->sucursal_id == Auth::user()->sucursal_id) {
                    if($notaCredito->puntoVenta->tipo->nombre == "Comandera"){
                        return view('frontend.ventas.nota-credito.imprimir-ticket', compact('notaCredito','empresa','tipos_comprobantes','porcentajes_iva'));
                    } else {
                        return $pdf->stream('Nota de Credito.pdf');
                    }

                }
            } else {
                if($notaCredito->puntoVenta->tipo->nombre == "Comandera"){
                    return view('frontend.ventas.nota-credito.imprimir-ticket', compact('notaCredito','empresa','tipos_comprobantes','porcentajes_iva'));
                } else {
                    return $pdf->stream('Nota de Credito.pdf');
                }
            }
        }
    }

    public function setEstadoFacturacion(Request $request){
        $nota_credito_id = $request['nota_id'];
        $notaCredito = NotaCredito::findOrFail($nota_credito_id);

        $empresa = Auth::user()->empresa;

        if ($notaCredito->empresa_id == $empresa->id) {
            if ($request['estado'] == "FACTURADO") {

                $notaCredito->numero = str_pad($request['numeroComprobante'], 8, "0", STR_PAD_LEFT);
                $nombresClaves = [
                    TipoComprobante::NOTA_CREDITO_A => ['numeracion_nc_a'],
                    TipoComprobante::NOTA_CREDITO_B => ['numeracion_nc_b'],
                    TipoComprobante::NOTA_CREDITO_C => ['numeracion_nc_c'],
                    TipoComprobante::TIQUE_NOTA_CREDITO_A => ['numeracion_nc_a'],
                    TipoComprobante::TIQUE_NOTA_CREDITO_B => ['numeracion_nc_b'],
                    TipoComprobante::TIQUE_NOTA_CREDITO_C => ['numeracion_nc_c']
                ];
                $opcion = Opcion::where('empresa_id', $empresa->id)
                    ->where('nombre', 'LIKE', $nombresClaves[(int)$notaCredito->tipo_comprobante_id])
                    ->where('punto_venta_id', $notaCredito->puntoVenta->id)
                    ->first();
                $opcion->valor = str_pad($request['numeroComprobante'] + 1, 8, "0", STR_PAD_LEFT);
                $opcion->save();

                $notaCredito->estado_facturacion = EstadoFacturacion::FACTURADO;
            } else {
                $notaCredito->estado_facturacion = EstadoFacturacion::ERROR;
            }

            if(Auth::user()->hasRole('Usuario Vendedor')){
                $sucursal = Auth::user()->sucursal;
                if ($notaCredito->sucursal_id == $sucursal->id) {
                    $notaCredito->save();
                }
            } else {
                $notaCredito->save();
            }
        }
    }

}
