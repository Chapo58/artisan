<?php

namespace App\Http\Controllers\Frontend\Configuraciones;

use App\Http\Controllers\Controller;
use App\Models\Frontend\Configuraciones\Moneda;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;

class MonedaController extends Controller{

    public function index(){
        $empresa = Auth::user()->empresa;
        $monedas = Moneda::where('empresa_id', $empresa->id)->get();

        return view('frontend.configuraciones.moneda.index', compact('monedas'));
    }

    public function create(){
        return view('frontend.configuraciones.moneda.create');
    }

    public function store(Request $request){

        $empresa = Auth::user()->empresa;

        $this->validate($request, [
    			'numero' => 'required',
    			'nombre' => 'required',
    			'cotizacion' => 'required|numeric'
    		]);

        $requestData = $request->all();

        $validarNumeroUnico = Moneda::where('empresa_id', $empresa->id)->where('numero', $request['numero'])->first();
        if($validarNumeroUnico){
            return redirect('configuraciones/moneda')->withFlashDanger('El numero ingresado ya existe.');
        }

        $nuevaMoneda = new Moneda($requestData);
        $nuevaMoneda->empresa_id = $empresa->id;

        $nuevaMoneda->save();

        return redirect('configuraciones/moneda')->withFlashSuccess('Moneda creada correctamente.');
    }

    public function show($id){
        $moneda = Moneda::findOrFail($id);

        if($moneda->empresa_id == Auth::user()->empresa_id){
            return view('frontend.configuraciones.moneda.show', compact('moneda'));
        }else{
            return redirect('configuraciones/moneda');
        }
    }

    public function edit($id){
        $moneda = Moneda::findOrFail($id);

        if($moneda->empresa_id == Auth::user()->empresa_id){
            return view('frontend.configuraciones.moneda.edit', compact('moneda'));
        }else{
            return redirect('configuraciones/moneda');
        }
    }

    public function update($id, Request $request){
        $empresa = Auth::user()->empresa;

        $moneda = Moneda::findOrFail($id);

        $this->validate($request, [
          'numero' => 'required',
          'nombre' => 'required',
          'cotizacion' => 'required'
        ]);
        $requestData = $request->all();

        $validarNumeroUnico = Moneda::where('empresa_id', $empresa->id)->where('numero', $request['numero'])->where('numero','!=', $moneda->numero)->first();
        if($validarNumeroUnico){
            return redirect('configuraciones/moneda')->withFlashDanger('El numero ingresado ya existe.');
        }

        if($moneda->empresa_id == $empresa->id){
            $moneda->update($requestData);
        }

        return redirect('configuraciones/moneda')->withFlashSuccess('Moneda modificada correctamente.');
    }

    public function destroy($id){
        $moneda = Moneda::findOrFail($id);

        if($moneda->empresa_id == Auth::user()->empresa_id && Auth::user()->email != 'demo@ciatt.com.ar'){
            $moneda->delete();
        }

        return redirect('configuraciones/moneda')->withFlashSuccess('Moneda eliminada correctamente.');
    }

    public function obtenerCotizacion(Request $request){
        $moneda_id = $request->input('moneda_id');

        $moneda = Moneda::where('empresa_id', Auth::user()->empresa_id)
            ->where('id', '=', $moneda_id)
            ->first();
        if($moneda == null){
            return response()->json('No existe la moneda.');
        }

        $resultado = $moneda->cotizacion;

        return response()->json($resultado);
    }

    public function actualizarDolar($id){
        $moneda = Moneda::findOrFail($id);

        $dolar = New Moneda();
        $valor = $dolar->getCotizacionDolar();

        $moneda->cotizacion = $valor;
        $moneda->save();

        return redirect('configuraciones\moneda');
    }
}
