<?php

namespace App\Http\Controllers\Frontend\Configuraciones;

use App\Http\Controllers\Controller;
use App\Models\Backend\Planes\FormaPago;
use App\Models\Backend\Planes\Pago;
use App\Models\Backend\Planes\Plan;
use App\Models\Frontend\Configuraciones\EstadoPago;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Session;

class PagoController extends Controller{

    public function index(){
        $usuario = Auth::user();
        $pagos = Pago::where('usuario_id', $usuario->id)->orderBy('id','DESC')->get();
        $estadosPagos = EstadoPago::$getArray;

        if ($usuario->hasRole('Usuario Admin')) {
            return view('frontend.configuraciones.pago.index', compact('pagos','estadosPagos'));
        } else {
            return redirect('/dashboard');
        }

    }

    public function create(){
        $distribuidor = Auth::user()->empresa->distribuidor;

        $planes = Plan::get();
        $formasPago = FormaPago::getFormasPagoPorDistribuidor($distribuidor);

        return view('frontend.configuraciones.pago.create', compact('planes','formasPago'));
    }

    public function store(Request $request){
        $this->validate($request, [
            'forma_pago_id' => 'required',
            'plan_id' => 'required',
            'periodo' => 'required'
        ]);
        $usuario = Auth::user();
        $empresa = Auth::user()->empresa;
        $distribuidor = Auth::user()->empresa->distribuidor;

        $plan = Plan::findOrFail($request->plan_id);
        if($request->periodo == 'Mensual'){
            $importe = $plan->precio;
        } else {
            $importe = $plan->precio * 11;
        }

        $nuevoPago = New Pago();
        $nuevoPago->usuario_id = $usuario->id;
        $nuevoPago->empresa_id = $empresa->id;
        if($distribuidor){
            $nuevoPago->distribuidor_id = $distribuidor->id;
        } else {
            $nuevoPago->distribuidor_id = 0;
        }
        $nuevoPago->forma_pago_id = $request->forma_pago_id;
        $nuevoPago->plan_id = $request->plan_id;
        $nuevoPago->periodo = $request->periodo;
        $nuevoPago->importe = $importe;
        $nuevoPago->nota = $request->nota;
        $nuevoPago->save();

        if($request->forma_pago_id == 0){ // Mercado Pago
            if($request->periodo == 'Mensual'){
                $url = $nuevoPago->plan->link_mercadopago_mensual;
            } else {
                $url = $nuevoPago->plan->link_mercadopago_anual;
            }
            $resultado['titulo'] = 'Pago Guardado';
            $resultado['mensaje'] = 'El pago ha sido guardado en el sistema. Redireccionando a Mercado Pago...';
            $resultado['url'] = $url;
        } else {
            $resultado['titulo'] = 'Pago Realizado!';
            $resultado['mensaje'] = trans('labels.frontend.pago').trans('alerts.frontend.created').'!';
            $resultado['url'] = url('configuraciones/pago');
        }

        return response()->json($resultado);
    }

    public function show($id){
        $pago = Pago::findOrFail($id);

        $empresa = Auth::user()->empresa;

        if ($pago->empresa_id == $empresa->id) {
            $estadosPagos = EstadoPago::$getArray;

            return view('frontend.configuraciones.pago.show', compact('pago','estadosPagos'));
        } else {
            return redirect('configuraciones/pago');
        }
    }

    public function destroy($id){
        $pago = Pago::findOrFail($id);
        $empresa = Auth::user()->empresa;

        if ($pago->empresa_id == $empresa->id && isset($pago->estado)){
            $pago->delete();
            Session::flash('flash_message',  trans('labels.frontend.pago').trans('alerts.frontend.deleted').'!');
        }

        return redirect('configuraciones/pago');
    }

    public function obtenerInformacion(Request $request){
        $periodo = $request->periodo;
        $formaPago = $request->forma_pago_id;
        $plan_id = $request->plan_id;

        $plan = Plan::findOrFail($plan_id);
        if($periodo == 'Mensual'){
            $importe = $plan->precio;
        } else {
            $importe = $plan->precio * 11;
        }
        $pago = FormaPago::find($formaPago);

        $resultado['instrucciones'] = ($pago) ? $pago->instrucciones : '';
        $resultado['importe'] = number_format($importe,2,',','.');

        return response()->json($resultado);
    }

}
