<?php

namespace App\Http\Controllers\Frontend\Configuraciones;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;

class SoporteController extends Controller{

    public function index(Request $request){
        return view('frontend.configuraciones.soporte.index');
    }
}
