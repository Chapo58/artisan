<?php

namespace App\Http\Controllers\Frontend\Configuraciones;

use App\Http\Controllers\Controller;
use App\Models\Access\User\User;
use App\Models\Backend\Empresas\Sucursal;
use App\Models\Backend\Planes\Plan;
use App\Models\Frontend\Configuraciones\CondicionIva;
use App\Models\Frontend\Configuraciones\Opcion;
use App\Models\Frontend\Configuraciones\PuntoDeVenta;
use App\Models\Frontend\Configuraciones\TipoControladoraFiscal;
use App\Models\Frontend\Configuraciones\TipoPuntoVenta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;
use DB;

class PuntoDeVentaController extends Controller{

    public function index(){
        $empresa = Auth::user()->empresa;

        if (Auth::user()->hasRole('Usuario Vendedor')) {
            $sucursal = Auth::user()->sucursal;
            $puntos_de_ventas = PuntoDeVenta::listarPuntosDeVentaPorEmpresaSucursal($empresa, $sucursal);
        }else{
            $puntos_de_ventas = PuntoDeVenta::listarPuntosDeVentaPorEmpresaSucursal($empresa, null);
        }

        return view('frontend.configuraciones.punto-de-venta.index', compact('puntos_de_ventas'));
    }

    public function create(){
        if(Auth::user()->hasRole('Usuario Vendedor')){
            return redirect('configuraciones/punto-de-venta');
        }else{
            $empresa = Auth::user()->empresa;
            $tipos_punto_venta= ['' => 'Seleccione una opción...'] + TipoPuntoVenta::pluck('nombre', 'id')->all();
            $sucursales = ['' => 'Seleccione una opción...'] + Sucursal::where('empresa_id', $empresa->id)->get()->pluck('nombre', 'id')->toArray();
            $tiposControladoras = TipoControladoraFiscal::$arrayCombo;
            $usuarios = User::getUsuariosPorEmpresaArray($empresa);

            return view('frontend.configuraciones.punto-de-venta.create', compact('tipos_punto_venta', 'sucursales', 'tiposControladoras','usuarios'));
        }
    }

    public function store(Request $request){
        $empresa = Auth::user()->empresa;
        $sucursal = Auth::user()->sucursal;
        
        $this->validate($request, [
			'numero' => 'required',
			'tipo_punto_id' => 'required',
			'sucursal_id' => 'required',
		]);

        if($request['tipo_punto_id'] == 1 || $request['tipo_punto_id'] == 3){
            if($empresa->plan){
                $cantidadPuntosDeVentaActual = $puntosDeVenta = PuntoDeVenta::getPuntosDeVentaAPorSucursal($sucursal);
                $limitePlan = Plan::findOrFail($empresa->plan->id);
                if($cantidadPuntosDeVentaActual->count() >= $limitePlan->cantidad_puntos_venta){
                    return redirect('configuraciones/punto-de-venta')->withFlashDanger('Limite de puntos de venta alcanzado! Para agregar mas puntos de venta mejore su <a href="'.url('configuraciones/pago/create').'">Plan de Empresa.</a>');
                }
            }
        }

        if($request['tipo_punto_id'] == 1) { // Si es una impresora fiscal
            $this->validate($request, [
                'host_fiscal' => 'required',
                'puerto_fiscal' => 'required',
                'modelo_impresora_fiscal' => 'required',
                'baudios_fiscal' => 'required',
                'usuario_id_fiscal' => 'required',
            ]);
        } elseif($request['tipo_punto_id'] == 3) { // Si es facturacion electronica
            $this->validate($request, [
                'cert' => 'required',
                'key' => 'required',
            ]);

            $path = $request->file('cert')->store(
                'certificados/'.str_replace(' ', '_', $empresa->razon_social)
            );
            $request['cert_electronica'] = 'app/'.$path;

            $path = $request->file('key')->store(
                'certificados/'.str_replace(' ', '_', $empresa->razon_social)
            );
            $request['key_electronica'] = 'app/'.$path;
        } elseif($request['tipo_punto_id'] == 4) { // Si es una comandera
            $this->validate($request, [
                'anchura_papel_comandera' => 'required',
            ]);
        } else { // Si no es fiscal ni comandera, los paso en NULL por si igualmente los rellenaron en la vista
            $request['host_fiscal'] = NULL;
            $request['puerto_fiscal'] = NULL;
            $request['modelo_impresora_fiscal'] = NULL;
            $request['baudios_fiscal'] = NULL;
            $request['usuario_id_fiscal'] = NULL;
            $request['anchura_papel_comandera'] = NULL;
            $request['cert_electronica'] = NULL;
            $request['key_electronica'] = NULL;
        }

        $validarPuntoDeVenta = PuntoDeVenta::where('empresa_id', $empresa->id)
            ->where('sucursal_id', $request['sucursal_id'])
            ->where('numero', $request['numero'])->first();
        if($validarPuntoDeVenta){ // Si existe el punto de venta
            return redirect('configuraciones/punto-de-venta')->withFlashDanger('El punto de venta '.$request['numero'].' ya existe!');
        }

        DB::transaction(function () use ($request, $empresa) {
            $requestData = $request->all();

            $nuevoPuntoVenta = new PuntoDeVenta($requestData);
            $nuevoPuntoVenta->empresa_id = $empresa->id;
            $nuevoPuntoVenta->save();

            if($nuevoPuntoVenta->tipo->nombre == "Impresora Fiscal" || $nuevoPuntoVenta->tipo->nombre == "Factura Electrónica"){
                $this->crearOpciones($nuevoPuntoVenta);
            }

            Session::flash('flash_success', 'Punto de venta creado correctamente.');
        });

        return redirect('configuraciones/punto-de-venta');
    }

    public function show($id){
        $puntodeventa= PuntoDeVenta::findOrFail($id);

        $tiposControladoras = TipoControladoraFiscal::$arrayCombo;

        if($puntodeventa->empresa_id == Auth::user()->empresa_id){
            return view('frontend.configuraciones.punto-de-venta.show', compact('puntodeventa','tiposControladoras'));
        }else{
            return redirect('configuraciones/punto-de-venta');
        }
    }

    public function edit($id){
        if(Auth::user()->hasRole('Usuario Vendedor')){
            return redirect('configuraciones/punto-de-venta');
        }else{
            $puntodeventa = PuntoDeVenta::findOrFail($id);
            $empresa = Auth::user()->empresa;
            $tiposControladoras = TipoControladoraFiscal::$arrayCombo;
            $usuarios = User::getUsuariosPorEmpresaArray($empresa);

            if($puntodeventa->empresa_id == $empresa->id){
                $tipos_punto_venta = ['' => 'Seleccione una opción...'] + TipoPuntoVenta::pluck('nombre', 'id')->all();
                $sucursales = ['' => 'Seleccione una opción...'] + Sucursal::where('empresa_id', $empresa->id)->get()->pluck('nombre', 'id')->toArray();

                return view('frontend.configuraciones.punto-de-venta.edit', compact('puntodeventa', 'tipos_punto_venta', 'sucursales', 'tiposControladoras','usuarios'));
            }else{
                return redirect('configuraciones/punto-de-venta');
            }
        }
    }

    public function update($id, Request $request){
        $empresa = Auth::user()->empresa;

        $this->validate($request, [
            'numero' => 'required',
            'tipo_punto_id' => 'required',
            'sucursal_id' => 'required',
        ]);

        if($request['tipo_punto_id'] == 1) { // Si es una impresora fiscal
            $this->validate($request, [
                'host_fiscal' => 'required',
                'puerto_fiscal' => 'required',
                'modelo_impresora_fiscal' => 'required',
                'baudios_fiscal' => 'required',
                'usuario_id_fiscal' => 'required',
            ]);

            $request['anchura_papel_comandera'] = NULL;
            $request['cert_electronica'] = NULL;
            $request['key_electronica'] = NULL;
        } elseif($request['tipo_punto_id'] == 3) { // Si es facturacion electronica
            $this->validate($request, [
                'cert' => 'required',
                'key' => 'required',
            ]);

            $request['host_fiscal'] = NULL;
            $request['puerto_fiscal'] = NULL;
            $request['modelo_impresora_fiscal'] = NULL;
            $request['baudios_fiscal'] = NULL;
            $request['usuario_id_fiscal'] = NULL;
            $request['anchura_papel_comandera'] = NULL;

            $path = $request->file('cert')->store(
                'certificados/'.str_replace(' ', '_', $empresa->razon_social)
            );
            $request['cert_electronica'] = 'app/'.$path;

            $path = $request->file('key')->store(
                'certificados/'.str_replace(' ', '_', $empresa->razon_social)
            );
            $request['key_electronica'] = 'app/'.$path;

        } elseif($request['tipo_punto_id'] == 4) { // Si es una comandera
            $this->validate($request, [
                'anchura_papel_comandera' => 'required',
            ]);

            $request['host_fiscal'] = NULL;
            $request['puerto_fiscal'] = NULL;
            $request['modelo_impresora_fiscal'] = NULL;
            $request['baudios_fiscal'] = NULL;
            $request['usuario_id_fiscal'] = NULL;
            $request['cert_electronica'] = NULL;
            $request['key_electronica'] = NULL;
        } else { // Si no es fiscal ni comandera, los paso en NULL por si igualmente los rellenaron en la vista
            $request['host_fiscal'] = NULL;
            $request['puerto_fiscal'] = NULL;
            $request['modelo_impresora_fiscal'] = NULL;
            $request['baudios_fiscal'] = NULL;
            $request['usuario_id_fiscal'] = NULL;
            $request['anchura_papel_comandera'] = NULL;
            $request['cert_electronica'] = NULL;
            $request['key_electronica'] = NULL;
        }

        DB::transaction(function () use ($request, $id) {
            $requestData = $request->all();
            $empresa = Auth::user()->empresa;

            $puntodeventa = PuntoDeVenta::findOrFail($id);

            if($puntodeventa->empresa_id == $empresa->id) {
                if($requestData['tipo_punto_id'] == 1 || $requestData['tipo_punto_id'] == 3){//Se cambia a Fiscal o Electronica
                    if($puntodeventa->tipo->id == 2 || $requestData['tipo_punto_id'] == 4){//Era Manual o Comandera
                        $puntodeventa->update($requestData);
                        $this->crearOpciones($puntodeventa);
                    }else{
                        $puntodeventa->update($requestData);
                    }
                }else{//Se cambia a Manual o Comandera 
                    if($puntodeventa->tipo->id == 1 || $puntodeventa->tipo->id == 3){//Era Fiscal o Electronica
                        $puntodeventa->update($requestData);
                        $this->borrarOpciones($puntodeventa);
                    }else{
                        $puntodeventa->update($requestData);
                    }
                }
            }

            Session::flash('flash_success', 'Punto de venta modificado correctamente.');
        });

        return redirect('configuraciones/punto-de-venta');
    }

    public function destroy($id){
        if(Auth::user()->hasRole('Usuario Vendedor')){
            return redirect('configuraciones/punto-de-venta');
        }else{
            $puntoDeVenta = PuntoDeVenta::findOrFail($id);
            if($puntoDeVenta->empresa_id == Auth::user()->empresa_id) {
                $opciones = Opcion::where('punto_venta_id',$puntoDeVenta->id)->get();
                foreach($opciones as $opcion){
                    $opcion->delete();
                }
                $puntoDeVenta->delete();
                Session::flash('flash_success', 'Punto de venta eliminado correctamente.');
            }
            return redirect('configuraciones/punto-de-venta');
        }
    }

    private function crearOpciones($puntoVenta){
        $empresa = Auth::user()->empresa;

        if($empresa->condicionIva->id == CondicionIva::RESPONSABLE_INSCRIPTO){
            $numeracionFacturaA = new Opcion();
            $numeracionFacturaA->empresa_id = $empresa->id;
            $numeracionFacturaA->punto_venta_id = $puntoVenta->id;
            if($puntoVenta->tipo_punto_id == 1){ // Controladora Fiscal
                $numeracionFacturaA->presentacion = 'Numeración de Tique Factura A - Punto Venta Fiscal '.$puntoVenta;
            } else {
                $numeracionFacturaA->presentacion = 'Numeración de Factura A - Punto Venta Fiscal '.$puntoVenta;
            }
            $numeracionFacturaA->nombre = 'numeracion_factura_a';
            $numeracionFacturaA->valor = '00000001';
            $numeracionFacturaA->save();

            $numeracionNDA = new Opcion();
            $numeracionNDA->empresa_id = $empresa->id;
            $numeracionNDA->punto_venta_id = $puntoVenta->id;
            if($puntoVenta->tipo_punto_id == 1){
                $numeracionNDA->presentacion = 'Numeración de Tique Nota de Débito A - Punto Venta Fiscal '.$puntoVenta;
            } else {
                $numeracionNDA->presentacion = 'Numeración de Nota de Débito A - Punto Venta Fiscal '.$puntoVenta;
            }
            $numeracionNDA->nombre = 'numeracion_nd_a';
            $numeracionNDA->valor = '00000001';
            $numeracionNDA->save();

            $numeracionNCA = new Opcion();
            $numeracionNCA->empresa_id = $empresa->id;
            $numeracionNCA->punto_venta_id = $puntoVenta->id;
            if($puntoVenta->tipo_punto_id == 1){
                $numeracionNCA->presentacion = 'Numeración de Tique Nota de Crédito A - Punto Venta Fiscal '.$puntoVenta;
            } else {
                $numeracionNCA->presentacion = 'Numeración de Nota de Crédito A - Punto Venta Fiscal '.$puntoVenta;
            }
            $numeracionNCA->nombre = 'numeracion_nc_a';
            $numeracionNCA->valor = '00000001';
            $numeracionNCA->save();

            $numeracionFacturaB = new Opcion();
            $numeracionFacturaB->empresa_id = $empresa->id;
            $numeracionFacturaB->punto_venta_id = $puntoVenta->id;
            if($puntoVenta->tipo_punto_id == 1){
                $numeracionFacturaB->presentacion = 'Numeración de Tique Factura B - Punto Venta Fiscal '.$puntoVenta;
            } else {
                $numeracionFacturaB->presentacion = 'Numeración de Factura B - Punto Venta Fiscal '.$puntoVenta;
            }
            $numeracionFacturaB->nombre = 'numeracion_factura_b';
            $numeracionFacturaB->valor = '00000001';
            $numeracionFacturaB->save();

            $numeracionNDB = new Opcion();
            $numeracionNDB->empresa_id = $empresa->id;
            $numeracionNDB->punto_venta_id = $puntoVenta->id;
            if($puntoVenta->tipo_punto_id == 1){
                $numeracionNDB->presentacion = 'Numeración de Tique Nota de Débito B - Punto Venta Fiscal '.$puntoVenta;
            } else {
                $numeracionNDB->presentacion = 'Numeración de Nota de Débito B - Punto Venta Fiscal '.$puntoVenta;
            }
            $numeracionNDB->nombre = 'numeracion_nd_b';
            $numeracionNDB->valor = '00000001';
            $numeracionNDB->save();

            $numeracionNCB = new Opcion();
            $numeracionNCB->empresa_id = $empresa->id;
            $numeracionNCB->punto_venta_id = $puntoVenta->id;
            if($puntoVenta->tipo_punto_id == 1){
                $numeracionNCB->presentacion = 'Numeración de Tique Nota de Crédito B - Punto Venta Fiscal '.$puntoVenta;
            } else {
                $numeracionNCB->presentacion = 'Numeración de Nota de Crédito B - Punto Venta Fiscal '.$puntoVenta;
            }
            $numeracionNCB->nombre = 'numeracion_nc_b';
            $numeracionNCB->valor = '00000001';
            $numeracionNCB->save();
        }else{
            $numeracionFacturaC = new Opcion();
            $numeracionFacturaC->empresa_id = $empresa->id;
            $numeracionFacturaC->punto_venta_id = $puntoVenta->id;
            if($puntoVenta->tipo_punto_id == 1){
                $numeracionFacturaC->presentacion = 'Numeración de Tique Factura C - Punto Venta Fiscal '.$puntoVenta;
            } else {
                $numeracionFacturaC->presentacion = 'Numeración de Factura C - Punto Venta Fiscal '.$puntoVenta;
            }
            $numeracionFacturaC->nombre = 'numeracion_factura_c';
            $numeracionFacturaC->valor = '00000001';
            $numeracionFacturaC->save();

            $numeracionNDC = new Opcion();
            $numeracionNDC->empresa_id = $empresa->id;
            $numeracionNDC->punto_venta_id = $puntoVenta->id;
            if($puntoVenta->tipo_punto_id == 1){
                $numeracionNDC->presentacion = 'Numeración de Tique Nota de Débito C - Punto Venta Fiscal '.$puntoVenta;
            } else {
                $numeracionNDC->presentacion = 'Numeración de Nota de Débito C - Punto Venta Fiscal '.$puntoVenta;
            }
            $numeracionNDC->nombre = 'numeracion_nd_c';
            $numeracionNDC->valor = '00000001';
            $numeracionNDC->save();

            $numeracionNCC = new Opcion();
            $numeracionNCC->empresa_id = $empresa->id;
            $numeracionNCC->punto_venta_id = $puntoVenta->id;
            if($puntoVenta->tipo_punto_id == 1){
                $numeracionNCC->presentacion = 'Numeración de Tique Nota de Crédito C - Punto Venta Fiscal '.$puntoVenta;
            } else {
                $numeracionNCC->presentacion = 'Numeración de Nota de Crédito C - Punto Venta Fiscal '.$puntoVenta;
            }
            $numeracionNCC->nombre = 'numeracion_nc_c';
            $numeracionNCC->valor = '00000001';
            $numeracionNCC->save();
        }
    }

    private function borrarOpciones($puntoVenta){
        $opciones = Opcion::where('punto_venta_id',$puntoVenta->id)->get();

        foreach ($opciones as $opcion){
            $opcion->delete();
        }
    }

    public function generarCSR(){

        // Fill in data for the distinguished name to be used in the cert
// You must change the values of these keys to match your name and
// company, or more precisely, the name and company of the person/site
// that you are generating the certificate for.
// For SSL certificates, the commonName is usually the domain name of
// that will be using the certificate, but for S/MIME certificates,
// the commonName will be the name of the individual who will use the
// certificate.
        $dn = array(
            "countryName" => "UK",
            "stateOrProvinceName" => "Somerset",
            "localityName" => "Glastonbury",
            "organizationName" => "The Brain Room Limited",
            "organizationalUnitName" => "PHP Documentation Team",
            "commonName" => "Wez Furlong",
            "emailAddress" => "wez@example.com"
        );

// Generate a new private (and public) key pair
        $privkey = openssl_pkey_new();

// Generate a certificate signing request
        $csr = openssl_csr_new($dn, $privkey);


// Now you will want to preserve your private key, CSR and self-signed
// cert so that they can be installed into your web server, mail server
// or mail client (depending on the intended use of the certificate).
// This example shows how to get those things into variables, but you
// can also store them directly into files.
// Typically, you will send the CSR on to your CA who will then issue
// you with the "real" certificate.
        openssl_csr_export($csr, $csrout) and debug_zval_dump($csrout);
        openssl_pkey_export($privkey, $pkeyout, "mypassword") and debug_zval_dump($pkeyout);

// Show any errors that occurred here
        while (($e = openssl_error_string()) !== false) {
            echo $e . "\n";
        }

    }
}
