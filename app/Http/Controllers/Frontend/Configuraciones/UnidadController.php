<?php

namespace App\Http\Controllers\Frontend\Configuraciones;

use App\Http\Controllers\Controller;

use App\Models\Frontend\Configuraciones\Unidad;
use Illuminate\Http\Request;
use Session;

class UnidadController extends Controller{

    public function index(Request $request){
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $unidad = Unidad::where('nombre', 'LIKE', "%$keyword%")
				->orWhere('presentacion', 'LIKE', "%$keyword%")
				
                ->paginate($perPage);
        } else {
            $unidad = Unidad::paginate($perPage);
        }

        return view('backend.configuraciones.unidad.index', compact('unidad'));
    }

    public function create(){
        return view('backend.configuraciones.unidad.create');
    }

    public function store(Request $request){
        $this->validate($request, [
			'nombre' => 'required',
			'presentacion' => 'required'
		]);
        $requestData = $request->all();

        Unidad::create($requestData);

        Session::flash('flash_message', trans('labels.frontend.unidad').trans('alerts.frontend.created').'!');

        return redirect('admin/configuraciones/unidad');
    }

    public function show($id){
        $unidad = Unidad::findOrFail($id);

        return view('backend.configuraciones.unidad.show', compact('unidad'));
    }

    public function edit($id){
        $unidad = Unidad::findOrFail($id);

        return view('backend.configuraciones.unidad.edit', compact('unidad'));
    }

    public function update($id, Request $request){
        $this->validate($request, [
			'nombre' => 'required',
			'presentacion' => 'required'
		]);
        $requestData = $request->all();
        
        $unidad = Unidad::findOrFail($id);
        $unidad->update($requestData);

        Session::flash('flash_message', trans('labels.frontend.unidad').trans('alerts.frontend.updated').'!');

        return redirect('admin/configuraciones/unidad');
    }

    public function destroy($id){
        Unidad::destroy($id);

        Session::flash('flash_message',  trans('labels.frontend.unidad').trans('alerts.frontend.deleted').'!');

        return redirect('admin/configuraciones/unidad');
    }
}
