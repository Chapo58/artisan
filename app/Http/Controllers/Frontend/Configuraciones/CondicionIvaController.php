<?php

namespace App\Http\Controllers\Frontend\Configuraciones;

use App\Http\Controllers\Controller;
use App\Models\Frontend\Configuraciones\CondicionIva;
use Illuminate\Http\Request;
use Session;

class CondicionIvaController extends Controller{

    public function index(Request $request){
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $condicioniva = CondicionIva::where('nombre', 'LIKE', "%$keyword%")
				->orWhere('alicuota', 'LIKE', "%$keyword%")
				
                ->paginate($perPage);
        } else {
            $condicioniva = CondicionIva::paginate($perPage);
        }

        return view('frontend.configuraciones.condicion-iva.index', compact('condicioniva'));
    }

    public function create(){
        return view('frontend.configuraciones.condicion-iva.create');
    }

    public function store(Request $request){
        $this->validate($request, [
			'nombre' => 'required',
			'alicuota' => 'required'
		]);
        $requestData = $request->all();
        
        CondicionIva::create($requestData);

        Session::flash('flash_message', trans('labels.frontend.condicioniva').trans('alerts.frontend.created').'!');

        return redirect('configuraciones/condicion-iva');
    }

    public function show($id)
    {
        $condicioniva = CondicionIva::findOrFail($id);

        return view('frontend.configuraciones.condicion-iva.show', compact('condicioniva'));
    }

    public function edit($id)
    {
        $condicioniva = CondicionIva::findOrFail($id);

        return view('frontend.configuraciones.condicion-iva.edit', compact('condicioniva'));
    }

    public function update($id, Request $request)
    {
        $this->validate($request, [
			'nombre' => 'required',
			'alicuota' => 'required'
		]);
        $requestData = $request->all();
        
        $condicioniva = CondicionIva::findOrFail($id);
        $condicioniva->update($requestData);

        Session::flash('flash_message', trans('labels.frontend.condicioniva').trans('alerts.frontend.updated').'!');

        return redirect('configuraciones/condicion-iva');
    }

    public function destroy($id)
    {
        CondicionIva::destroy($id);

        Session::flash('flash_message',  trans('labels.frontend.condicioniva').trans('alerts.frontend.deleted').'!');

        return redirect('configuraciones/condicion-iva');
    }
}
