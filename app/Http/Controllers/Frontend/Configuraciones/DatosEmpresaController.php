<?php

namespace App\Http\Controllers\Frontend\Configuraciones;

use App\Models\Frontend\Configuraciones\Localidad;
use App\Models\Frontend\Configuraciones\Pais;
use App\Models\Frontend\Configuraciones\Provincia;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ImageController;
use App\Models\Access\User\User;
use App\Models\Backend\Empresas\Empresa;
use App\Models\Backend\Empresas\Sucursal;
use App\Models\Frontend\Caja\Caja;
use App\Models\Frontend\Caja\EstadoCaja;
use App\Models\Frontend\Configuraciones\CondicionIva;
use App\Models\Frontend\Configuraciones\Estado;
use App\Models\Frontend\Configuraciones\Moneda;
use App\Models\Frontend\Configuraciones\PuntoDeVenta;
use App\Models\Frontend\Personas\Domicilio;
use App\Models\Frontend\Personas\Empleado;
use App\Repositories\Backend\Access\Role\RoleRepository;
use App\Repositories\Backend\Access\User\UserRepository;
use Illuminate\Http\Request;
use Session;
use DB;

class DatosEmpresaController extends Controller{

    public function edit($id){
        $empresa = Empresa::findOrFail($id);

        if ($empresa == Auth::user()->empresa || Auth::user()->hasRole('Administrador')) {
            $condiciones_iva = CondicionIva::condicionesEmpresasArray();
            $paises = ['' => 'Seleccione una opción...'] + Pais::pluck('nombre', 'id')->all();
            $provincias = ['' => 'Seleccione una opción...'] + Provincia::orderBy('nombre')->pluck('nombre', 'id')->all();

            return view('frontend.configuraciones.datos-empresa.edit', compact('empresa', 'condiciones_iva', 'paises', 'provincias'));
        }
        return redirect('dashboard');
    }

    public function update($id, Request $request){
        $this->validate($request, [
			'razon_social'              => 'required',
            'cuit'                      => 'required',
			'condicion_iva_id'          => 'required',
		]);

        if(Auth::user()->empresa->id == $id && Auth::user()->email != 'demo@ciatt.com.ar') {
            DB::transaction(function () use ($id, $request) {
                $requestData = $request->all();

                $empresa = Empresa::findOrFail($id);
                $empresa->razon_social = $requestData['razon_social'];
                $empresa->nombre = $requestData['nombre'];
                $empresa->cuit = $requestData['cuit'];
                $empresa->email = $requestData['email'];
                $empresa->telefono = $requestData['telefono'];
                $empresa->web = $requestData['web'];
                $empresa->ingresos_brutos = $requestData['ingresos_brutos'];
                $empresa->condicion_iva_id = $requestData['condicion_iva_id'];
                $empresa->inicio_actividad = $requestData['inicio_actividad'];

                $controllerImagen = new ImageController();
                $imagen_url = $controllerImagen->guardarImagen($request,'/uploads/empresas/',480,200);

                if ($imagen_url != false) {
                    $empresa->imagen_url = $imagen_url;
                }

                if($requestData['domicilio']['localidad'] && $requestData['domicilio']['direccion'] && $requestData['domicilio']['provincia_id']){
                    if (!isset($empresa->domicilio)) {
                        $domicilio = new Domicilio($requestData['domicilio']);
                        $domicilio->save();
                        $empresa->domicilio_id = $domicilio->id;
                    } else {
                        $empresa->domicilio->update($requestData['domicilio']);
                    }
                }

                $empresa->update();

                Session::flash('flash_success', 'Empresa modificada correctamente.');
            });
        }

        return redirect('configuraciones/datos-empresa/'.$id.'/edit');
    }
}
