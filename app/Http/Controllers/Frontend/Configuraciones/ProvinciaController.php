<?php

namespace App\Http\Controllers\Frontend\Configuraciones;

use App\Http\Controllers\Controller;
use App\Models\Frontend\Configuraciones\Pais;
use App\Models\Frontend\Configuraciones\Provincia;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;

class ProvinciaController extends Controller{

    public function index(Request $request){
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $provincias = Provincia::where('nombre', 'LIKE', "%$keyword%")
				->orWhere('pais_id', 'LIKE', "%$keyword%")
				
                ->paginate($perPage);
        } else {
            $provincias = Provincia::paginate($perPage);
        }

        return view('backend.configuraciones.provincia.index', compact('provincias'));
    }

    public function create(){
        $paises = Pais::pluck('nombre', 'id')->toArray();

        return view('backend.configuraciones.provincia.create', compact('paises'));
    }

    public function store(Request $request){
        $this->validate($request, [
			'nombre' => 'required',
			'pais_id' => 'required'
		]);
        $requestData = $request->all();
        
        Provincia::create($requestData);

        Session::flash('flash_message', trans('labels.frontend.provincia').trans('alerts.frontend.created').'!');

        return redirect('admin/configuraciones/provincia');
    }

    public function show($id){
        $provincia = Provincia::findOrFail($id);

        return view('backend.configuraciones.provincia.show', compact('provincia'));
    }

    public function edit($id){
        $provincia = Provincia::findOrFail($id);

        $paises = Pais::pluck('nombre', 'id')->toArray();

        return view('backend.configuraciones.provincia.edit', compact('provincia', 'paises'));
    }

    public function update($id, Request $request){
        $this->validate($request, [
			'nombre' => 'required',
			'pais_id' => 'required'
		]);
        $requestData = $request->all();
        
        $provincium = Provincia::findOrFail($id);
        $provincium->update($requestData);

        Session::flash('flash_message', trans('labels.frontend.provincia').trans('alerts.frontend.updated').'!');

        return redirect('admin/configuraciones/provincia');
    }

    public function destroy($id){
        Provincia::destroy($id);

        Session::flash('flash_message',  trans('labels.frontend.provincia').trans('alerts.frontend.deleted').'!');

        return redirect('admin/configuraciones/provincia');
    }

    public function guardarNuevaProvincia(Request $request){
        $provincia = New Provincia();

        DB::transaction(function() use ($provincia, $request) {
            $nombre = $request->input('nombre');
            $pais_id = $request->input('pais_id');

            $provincia->nombre = $nombre;
            $provincia->pais_id = $pais_id;

            $provincia->save();

            $controllerHistorial = new HistorialController();
            $controllerHistorial->nuevoHistorialCreacion($provincia, $request);
        });

        $resultado = [
            'id'    => $provincia->id,
            'nombre'=> $provincia->nombre
        ];

        return response()->json($resultado);
    }
}
