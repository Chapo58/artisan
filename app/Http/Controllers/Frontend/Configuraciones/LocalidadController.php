<?php

namespace App\Http\Controllers\Frontend\Configuraciones;

use App\Http\Controllers\Controller;
use App\Models\Frontend\Configuraciones\Localidad;
use App\Models\Frontend\Configuraciones\Provincia;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;

class LocalidadController extends Controller{

    public function index(Request $request){
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $localidad = Localidad::where('nombre', 'LIKE', "%$keyword%")
                ->paginate($perPage);
        } else {
            $localidad = Localidad::paginate($perPage);
        }

        return view('backend.configuraciones.localidad.index', compact('localidad'));
    }

    public function create(){
        $provincias = Provincia::pluck('nombre', 'id')->toArray();

        return view('backend.configuraciones.localidad.create', compact('provincias'));
    }

    public function store(Request $request){
        $this->validate($request, [
			'nombre' => 'required',
			'provincia_id' => 'required'
		]);
        $requestData = $request->all();
        
        Localidad::create($requestData);

        Session::flash('flash_message', trans('labels.frontend.localidad').trans('alerts.frontend.created').'!');

        return redirect('admin/configuraciones/localidad');
    }

    public function show($id){
        $localidad = Localidad::findOrFail($id);

        return view('backend.configuraciones.localidad.show', compact('localidad'));
    }

    public function edit($id){
        $localidad = Localidad::findOrFail($id);

        $provincias = Provincia::pluck('nombre', 'id')->toArray();

        return view('backend.configuraciones.localidad.edit', compact('localidad', 'provincias'));
    }

    public function update($id, Request $request){
        $this->validate($request, [
			'nombre' => 'required',
			'provincia_id' => 'required'
		]);
        $requestData = $request->all();
        
        $localidad = Localidad::findOrFail($id);
        $localidad->update($requestData);

        Session::flash('flash_message', trans('labels.frontend.localidad').trans('alerts.frontend.updated').'!');

        return redirect('admin/configuraciones/localidad');
    }

    public function destroy($id){
        Localidad::destroy($id);

        Session::flash('flash_message',  trans('labels.frontend.localidad').trans('alerts.frontend.deleted').'!');

        return redirect('admin/configuraciones/localidad');
    }

    public function guardarNuevaLocalidad(Request $request){
    $localidad = New Localidad();

    DB::transaction(function() use ($localidad, $request) {
        $nombre = $request->input('nombre');
        $provincia_id = $request->input('provincia_id');

        $localidad->nombre = $nombre;
        $localidad->provincia_id = $provincia_id;

        $localidad->save();

        $controllerHistorial = new HistorialController();
        $controllerHistorial->nuevoHistorialCreacion($localidad, $request);
    });

    $resultado = [
        'id'    => $localidad->id,
        'nombre'=> $localidad->nombre
    ];

    return response()->json($resultado);
}
}
