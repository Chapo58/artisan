<?php

namespace App\Http\Controllers\Frontend\Configuraciones;

use App\Http\Controllers\Controller;
use App\Models\Frontend\Configuraciones\Tarjeta;
use App\Models\Frontend\Configuraciones\PlanTarjeta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\Datatables\Facades\Datatables;
use Session;

class PlanTarjetaController extends Controller{

    public function index(){
        return view('frontend.configuraciones.plan-tarjeta.index');
    }

    public function datos(){
        $empresa = Auth::user()->empresa;

        $planesTarjeta = PlanTarjeta::listarPlanesTarjetasPorEmpresa($empresa);

        return Datatables::of($planesTarjeta)
            ->addColumn('action', function ($planTarjeta) {
                return '
                <a href="'.url('/configuraciones/plan-tarjeta/' . $planTarjeta->id).'" class="mytooltip"><i class="fa fa-eye text-success m-r-10"></i>
                    <span class="tooltip-content3">Ver Plan de Tarjeta</span>
                </a>
                <a href="'.url('/configuraciones/plan-tarjeta/' . $planTarjeta->id . '/edit').'" class="mytooltip">
                    <i class="fa fa-pencil text-info m-r-10"></i>
                    <span class="tooltip-content3">Modificar Plan de Tarjeta</span>
                </a>
                <a class="delete mytooltip pointer" data-id="'.$planTarjeta->id.'">
                    <i class="fa fa-close text-danger"></i>
                    <span class="tooltip-content3">Eliminar Plan de Tarjeta</span>
                </a>
                ';
            })
            ->setRowId('id')
            ->removeColumn('id')
            ->make(true);

    }

    public function create(){
        $empresa = Auth::user()->empresa;

        $tarjetas = Tarjeta::getTarjetasPorEmpresaArray($empresa);

        return view('frontend.configuraciones.plan-tarjeta.create', compact('tarjetas'));
    }

    public function store(Request $request){
        $this->validate($request, [
            'nombre' => 'required',
            'tarjeta_id' => 'required'
        ]);
        $requestData = $request->all();

        PlanTarjeta::create($requestData);

        return redirect('configuraciones/plan-tarjeta')->withFlashSuccess('Plan de tarjeta creado correctamente.');
    }

    public function show($id){
        $planTarjeta = PlanTarjeta::findOrFail($id);

        $empresa = Auth::user()->empresa;

        if($planTarjeta->tarjeta->empresa_id == $empresa->id) {
            return view('frontend.configuraciones.plan-tarjeta.show', compact('planTarjeta'));
        } else {
            return redirect('configuraciones/plan-tarjeta');
        }
    }

    public function edit($id){
        $planTarjeta = PlanTarjeta::findOrFail($id);

        $empresa = Auth::user()->empresa;

        if($planTarjeta->tarjeta->empresa_id == $empresa->id) {
            $tarjetas = Tarjeta::getTarjetasPorEmpresaArray($empresa);

            return view('frontend.configuraciones.plan-tarjeta.edit', compact('planTarjeta','tarjetas'));
        } else {
            return redirect('configuraciones/plan-tarjeta');
        }
    }

    public function update($id, Request $request){
        $this->validate($request, [
            'nombre' => 'required',
            'tarjeta_id' => 'required'
        ]);
        $requestData = $request->all();

        $planTarjeta = PlanTarjeta::findOrFail($id);

        $empresa = Auth::user()->empresa;

        if($planTarjeta->tarjeta->empresa_id == $empresa->id) {
            $planTarjeta->update($requestData);

            Session::flash('flash_success', 'Plan de tarjeta modificado correctamente.');
        }
        return redirect('configuraciones/plan-tarjeta');
    }

    public function destroy($id){
        $planTarjeta = PlanTarjeta::findOrFail($id);

        $empresa = Auth::user()->empresa;

        if($planTarjeta->tarjeta->empresa_id == $empresa->id) {
            $planTarjeta->delete();

            Session::flash('flash_success', 'Plan de tarjeta eliminado correctamente.');
        }

        return redirect('configuraciones/plan-tarjeta');
    }

    public function datosDePlanTarjeta(Request $request){
        $plan_tarjeta_id = (int)$request->input('plan_tarjeta_id');

        $planTarjeta = PlanTarjeta::findOrFail($plan_tarjeta_id);

        $empresa = Auth::user()->empresa;
        if ($planTarjeta->tarjeta->empresa_id == $empresa->id) {
            $resultado = [
                'interes' => (float)$planTarjeta->interes,
                'cuotas' => $planTarjeta->cuotas
            ];
        }else{
            $resultado = 'Sin permisos';
        }
        return response()->json($resultado);
    }
}
