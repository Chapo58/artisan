<?php

namespace App\Http\Controllers\Frontend\Configuraciones;

use App\Http\Controllers\Controller;
use App\Models\Frontend\Configuraciones\CondicionIva;
use App\Models\Frontend\Configuraciones\Opcion;
use App\Models\Frontend\Configuraciones\PuntoDeVenta;
use App\Models\Frontend\Configuraciones\TipoComprobante;
use App\Models\Frontend\Personas\Cliente;
use App\Models\Frontend\Ventas\ListaPrecio;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Facades\Datatables;
use Session;

class OpcionController extends Controller{

    public function index(){
        $empresa = Auth::user()->empresa;
        $listas_precios = ListaPrecio::getListasPorEmpresa($empresa);

        return view('frontend.configuraciones.opcion.index', compact('listas_precios'));
    }
    public function datos(){
        $empresa = Auth::user()->empresa;
        $opcion = Opcion::where('empresa_id', $empresa->id);

       return Datatables::of($opcion)
            ->addColumn('action', function ($opcion) {
                return '
                <a href="'.url('/configuraciones/opcion/' . $opcion->id).'" class="mytooltip"><i class="fa fa-eye text-success m-r-10"></i>
                    <span class="tooltip-content3">Ver Opcion</span>
                </a>
                <a href="'.url('/configuraciones/opcion/' . $opcion->id . '/edit').'" class="mytooltip">
                    <i class="fa fa-pencil text-info m-r-10"></i>
                    <span class="tooltip-content3">Modificar Opcion</span>
                </a>
                ';
            })
            ->setRowId('id')
            ->removeColumn('id')
            ->editColumn('sucursal', function ($opcion) {
               return isset($opcion->sucursal) ? $opcion->sucursal : 'Todas';
             })
           ->editColumn('valor', function ($opcion) use ($empresa) {
               if($opcion->nombre == 'lista_precios_defecto_id'){
                   $array = ListaPrecio::getListasPorEmpresaArray($empresa);
                   return $array[$opcion->valor];
               } else {
                   return $opcion->valor;
               }
           })
            ->make(true);
    }


    public function show($id){
        $empresa = Auth::user()->empresa;
        $opcion = Opcion::findOrFail($id);

        if($opcion->empresa_id == $empresa->id){
            return view('frontend.configuraciones.opcion.show', compact('opcion'));
        }else{
            return redirect('configuraciones/opcion');
        }
    }

    public function edit($id){
        $empresa = Auth::user()->empresa;
        $opcion = Opcion::findOrFail($id);

        if(Auth::user()->hasRole('Usuario Admin')) {
            if($opcion->empresa_id == $empresa->id){
                $array = null;
                $esNumeracion = '';
                if(strpos($opcion->nombre, 'numeracion') !== false){
                    $esNumeracion = 'numeracion';
                }else if(strpos($opcion->nombre, 'valor') !== false){

                } else {
                    $array = ListaPrecio::getListasPorEmpresaArray($empresa);
                }
                return view('frontend.configuraciones.opcion.edit', compact('opcion', 'esNumeracion','array'));
            }
        }
        return redirect('configuraciones/opcion');
    }

    public function update($id, Request $request){
        $this->validate($request, [
			'presentacion' => 'required',
			'valor' => 'required'
		]);

        $empresa = Auth::user()->empresa;

        $opcion = Opcion::findOrFail($id);
        if($opcion->empresa_id == $empresa->id){
            DB::transaction(function() use ($opcion, $request) {
                $requestData = $request->all();
                $requestData['valor'] = str_replace(',','.',$request->valor);
                $opcion->update($requestData);

                $controllerHistorial = new HistorialController();
                $controllerHistorial->nuevoHistorialActualizacion($opcion, $request);

                Session::flash('flash_success', 'Opción modificada correctamente.');
            });
        }

        return redirect('configuraciones/opcion');
    }

    public function destroy($id){
        $opcion = Opcion::findOrFail($id);

        if(Auth::user()->hasRole('Usuario Admin')) {
            if($opcion->empresa_id == Auth::user()->empresa_id){
                DB::transaction(function() use ($opcion) {
                    $opcion->delete();

                    $controllerHistorial = new HistorialController();
                    $controllerHistorial->nuevoHistorialEliminacion($opcion);

                    Session::flash('flash_success', 'Opción eliminada correctamente.');
                });
            }
        }

        return redirect('configuraciones/opcion');
    }

    public function obtenerNumeracionPorTipoComprobante(Request $request){
        $empresa = Auth::user()->empresa;
        $identificador = $request->input('tipo_comprobante');
        $punto_venta_id = $request->input('punto_venta_id');
        $numeracion = $request->input('numeracion');
        $cliente_id = $request->input('cliente_id');
        $nombresClaves = null;

        $cliente = Cliente::findOrFail($cliente_id);

        if($numeracion == 'venta'){
            $nombresClaves = [
                TipoComprobante::FACTURA_A => ['numeracion_factura_a'],
                TipoComprobante::FACTURA_B => ['numeracion_factura_b'],
                TipoComprobante::FACTURA_C => ['numeracion_factura_c'],
                TipoComprobante::TIQUE_FACTURA_A => ['numeracion_factura_a'],
                TipoComprobante::TIQUE_FACTURA_B => ['numeracion_factura_b'],
                TipoComprobante::TIQUE_FACTURA_C => ['numeracion_factura_c']
            ];
        }elseif($numeracion == 'nota_credito'){
            $nombresClaves = [
                TipoComprobante::NOTA_CREDITO_A => ['numeracion_nc_a'],
                TipoComprobante::NOTA_CREDITO_B => ['numeracion_nc_b'],
                TipoComprobante::NOTA_CREDITO_C => ['numeracion_nc_c'],
                TipoComprobante::TIQUE_NOTA_CREDITO_A => ['numeracion_nc_a'],
                TipoComprobante::TIQUE_NOTA_CREDITO_B => ['numeracion_nc_b'],
                TipoComprobante::TIQUE_NOTA_CREDITO_C => ['numeracion_nc_c']
            ];
        }elseif($numeracion == 'nota_debito'){
            $nombresClaves = [
                TipoComprobante::NOTA_DEBITO_A => ['numeracion_nd_a'],
                TipoComprobante::NOTA_DEBITO_B => ['numeracion_nd_b'],
                TipoComprobante::NOTA_DEBITO_C => ['numeracion_nd_c'],
                TipoComprobante::TIQUE_NOTA_DEBITO_A => ['numeracion_nd_a'],
                TipoComprobante::TIQUE_NOTA_DEBITO_B => ['numeracion_nd_b'],
                TipoComprobante::TIQUE_NOTA_DEBITO_C => ['numeracion_nd_c']
            ];
        }

        $puntoVenta = PuntoDeVenta::where('empresa_id', $empresa->id)
            ->where('id', $punto_venta_id)
            ->whereNull('deleted_at')
            ->first();

        if(isset($puntoVenta) && $puntoVenta->tipo->nombre == "Impresora Fiscal" || $puntoVenta->tipo->nombre == "Factura Electrónica"){
            $opcion = Opcion::where('empresa_id', $empresa->id)
                ->where('punto_venta_id', $puntoVenta->id)
                ->where('nombre', 'LIKE', $nombresClaves[(int)$identificador])
                ->first();
        }else{//Manual
            $opcion = Opcion::where('empresa_id', $empresa->id)
                ->where('nombre', 'LIKE', $nombresClaves[(int)$identificador])
                ->first();
        }


        if($opcion == null){
            return response()->json('No existe la configuración.');
        }

        $respuesta = [];
        $respuesta['numero'] = $opcion->valor;

        if($empresa->condicion_iva_id == CondicionIva::MONOTRIBUTISTA){
            if($puntoVenta->tipo_punto_id == 1){
                if($numeracion == 'venta'){$tipo_comprobante_id = TipoComprobante::TIQUE_FACTURA_C;}
                elseif($numeracion == 'nota_credito'){$tipo_comprobante_id = TipoComprobante::TIQUE_NOTA_CREDITO_C;}
                else{$tipo_comprobante_id = TipoComprobante::TIQUE_NOTA_DEBITO_C;}
            } else {
                if($numeracion == 'venta'){$tipo_comprobante_id = TipoComprobante::FACTURA_C;}
                elseif($numeracion == 'nota_credito'){$tipo_comprobante_id = TipoComprobante::NOTA_CREDITO_C;}
                else{$tipo_comprobante_id = TipoComprobante::NOTA_DEBITO_C;}
            }
        }elseif($empresa->condicion_iva_id == CondicionIva::RESPONSABLE_INSCRIPTO){
            if($cliente->condicion_iva_id == CondicionIva::RESPONSABLE_INSCRIPTO){
                if($puntoVenta->tipo_punto_id == 1){
                    if($numeracion == 'venta'){$tipo_comprobante_id = TipoComprobante::TIQUE_FACTURA_A;}
                    elseif($numeracion == 'nota_credito'){$tipo_comprobante_id = TipoComprobante::TIQUE_NOTA_CREDITO_A;}
                    else{$tipo_comprobante_id = TipoComprobante::TIQUE_NOTA_DEBITO_A;}
                } else {
                    if($numeracion == 'venta'){$tipo_comprobante_id = TipoComprobante::FACTURA_A;}
                    elseif($numeracion == 'nota_credito'){$tipo_comprobante_id = TipoComprobante::NOTA_CREDITO_A;}
                    else{$tipo_comprobante_id = TipoComprobante::NOTA_DEBITO_A;}
                }
            }else{
                if($puntoVenta->tipo_punto_id == 1){
                    if($numeracion == 'venta'){$tipo_comprobante_id = TipoComprobante::TIQUE_FACTURA_B;}
                    elseif($numeracion == 'nota_credito'){$tipo_comprobante_id = TipoComprobante::TIQUE_NOTA_CREDITO_B;}
                    else{$tipo_comprobante_id = TipoComprobante::TIQUE_NOTA_DEBITO_B;}
                } else {
                    if($numeracion == 'venta'){$tipo_comprobante_id = TipoComprobante::FACTURA_B;}
                    elseif($numeracion == 'nota_credito'){$tipo_comprobante_id = TipoComprobante::NOTA_CREDITO_B;}
                    else{$tipo_comprobante_id = TipoComprobante::NOTA_DEBITO_B;}
                }
            }
        }

        $respuesta['tipo_comprobante'] = $tipo_comprobante_id;

        return response()->json($respuesta);
    }
}
