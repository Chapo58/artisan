<?php

namespace App\Http\Controllers\Frontend\Configuraciones;

use App\Http\Controllers\Controller;
use App\Models\Frontend\Configuraciones\Historial;
use App\Models\Frontend\Configuraciones\TipoHistorial;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;

class HistorialController extends Controller{

    public function index(Request $request){
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $historial = Historial::where('user_id', 'LIKE', "%$keyword%")
				->orWhere('entidad_id', 'LIKE', "%$keyword%")
                ->paginate($perPage);
        } else {
            $historial = Historial::paginate($perPage);
        }

        return view('frontend.configuraciones.historial.index', compact('historial'));
    }

    public function create(){
        return view('frontend.configuraciones.historial.create');
    }

    public function store(Request $request){
        $this->validate($request, [
			'user_id' => 'required',
			'entidad_id' => 'required',
			'tipo_historial' => 'required'
		]);
        $requestData = $request->all();
        
        Historial::create($requestData);

        Session::flash('flash_message', trans('labels.frontend.historial').trans('alerts.frontend.created').'!');

        return redirect('configuraciones/historial');
    }

    public function show($id){
        $historial = Historial::findOrFail($id);

        return view('frontend.configuraciones.historial.show', compact('historial'));
    }

    public function edit($id){
        $historial = Historial::findOrFail($id);

        return view('frontend.configuraciones.historial.edit', compact('historial'));
    }

    public function update($id, Request $request){
        $this->validate($request, [
			'user_id' => 'required',
			'entidad_id' => 'required',
			'tipo_historial' => 'required'
		]);
        $requestData = $request->all();
        
        $historial = Historial::findOrFail($id);
        $historial->update($requestData);

        Session::flash('flash_message', trans('labels.frontend.historial').trans('alerts.frontend.updated').'!');

        return redirect('configuraciones/historial');
    }

    public function destroy($id){
        Historial::destroy($id);

        Session::flash('flash_message',  trans('labels.frontend.historial').trans('alerts.frontend.deleted').'!');

        return redirect('configuraciones/historial');
    }

    public function nuevoHistorialCreacion($objeto, $request){//Ver su puedo sacar la url del objeto
        $objetoUrl = $request->getRequestUri();
        if(class_basename($objeto) == 'Rollo'){
            $objetoUrl = '/produccion/rollo/'.$objeto->id;
        }
        $this->guardarHistorial($objeto, TipoHistorial::CREADO, $objetoUrl);
    }

    public function nuevoHistorialActualizacion($objeto, $request){//Ver su puedo sacar la url del objeto
        $this->guardarHistorial($objeto, TipoHistorial::ACTUALIZADO, $request->getRequestUri());
    }

    public function nuevoHistorialEliminacion($objeto){//Ver su puedo sacar la url del objeto
        $this->guardarHistorial($objeto, TipoHistorial::BORRADO, '');
    }

    private function guardarHistorial($objeto, $tipoHistorial, $url){
        $nuevoHistorial = new Historial();
        $nuevoHistorial->user_id = Auth::user()->id;
        $nuevoHistorial->empresa_id = Auth::user()->empresa_id;
        $nuevoHistorial->entidad_id = $objeto->id;
        $nuevoHistorial->tipo_historial = $tipoHistorial;
        if(isNonEmptyString($objeto->__toString())){
            $nuevoHistorial->texto = trans('labels.frontend.'.strtolower(class_basename($objeto))).': '.$objeto->__toString();
        }else{
            $nuevoHistorial->texto = $objeto->id;
        }
        $nuevoHistorial->url = $url;
        $nuevoHistorial->save();
    }
}
