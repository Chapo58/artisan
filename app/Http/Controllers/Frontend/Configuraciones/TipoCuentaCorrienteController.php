<?php

namespace App\Http\Controllers\Frontend\Configuraciones;

use App\Http\Controllers\Controller;
use App\Models\Frontend\Configuraciones\TipoCuentaCorriente;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;

class TipoCuentaCorrienteController extends Controller{

    public function index(){
        $empresa = Auth::user()->empresa;
        $tipocuentacorriente = TipoCuentaCorriente::where('empresa_id', $empresa->id)->get();

        return view('frontend.configuraciones.tipo-cuenta-corriente.index', compact('tipocuentacorriente'));
    }

    public function create(){
        return view('frontend.configuraciones.tipo-cuenta-corriente.create');
    }

    public function store(Request $request){
        $this->validate($request, [
    			'nombre' => 'required'
    		]);
        
        $requestData = $request->all();

        $nuevaTipoCuenta = new TipoCuentaCorriente($requestData);
        $nuevaTipoCuenta->empresa_id = Auth::user()->empresa_id;
        $nuevaTipoCuenta->save();

        return redirect('configuraciones/tipo-cuenta-corriente')->withFlashSuccess('Tipo de cuenta corriente creado correctamente.');
    }

    public function show($id){
        $tipocuentacorriente = TipoCuentaCorriente::findOrFail($id);

        if($tipocuentacorriente->empresa_id == Auth::user()->empresa_id){
            return view('frontend.configuraciones.tipo-cuenta-corriente.show', compact('tipocuentacorriente'));
        }
    }

    public function edit($id){
        $tipocuentacorriente = TipoCuentaCorriente::findOrFail($id);

        if($tipocuentacorriente->empresa_id == Auth::user()->empresa_id){
            return view('frontend.configuraciones.tipo-cuenta-corriente.edit', compact('tipocuentacorriente'));
        }
    }

    public function update($id, Request $request){
        $this->validate($request, [
    			'nombre' => 'required'
    		]);
        $requestData = $request->all();

        $tipocuentacorriente = TipoCuentaCorriente::findOrFail($id);
        if($tipocuentacorriente->empresa_id == Auth::user()->empresa_id){
            $tipocuentacorriente->update($requestData);
        }

        return redirect('configuraciones/tipo-cuenta-corriente')->withFlashSuccess('Tipo de cuenta corriente modificado correctamente.');
    }

    public function destroy($id){
        $tipocuentacorriente = TipoCuentaCorriente::findOrFail($id);

        if($tipocuentacorriente->empresa_id == Auth::user()->empresa_id){
            TipoCuentaCorriente::destroy($id);
            return redirect('configuraciones/tipo-cuenta-corriente')->withFlashSuccess('Tipo de cuenta corriente eliminado correctamente.');
        } else {
            return redirect('configuraciones/tipo-cuenta-corriente');
        }

    }

    public function datosTipo(Request $request){
        $tipo_cuenta_corriente_id = (int)$request->input('tipo_cuenta_corriente_id');

        $tipoCuentaCorriente = TipoCuentaCorriente::findOrFail($tipo_cuenta_corriente_id);

        $empresa = Auth::user()->empresa;
        if ($tipoCuentaCorriente->empresa_id == $empresa->id) {
            $resultado = [
                'interes' => (float)$tipoCuentaCorriente->interes,
                'cuotas' => $tipoCuentaCorriente->cuotas
            ];
        }else{
            $resultado = 'Sin permisos';
        }
        return response()->json($resultado);
    }
}
