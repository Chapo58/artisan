<?php

namespace App\Http\Controllers\Frontend\Configuraciones;

use App\Http\Controllers\Controller;
use App\Models\Frontend\Configuraciones\Pais;
use Illuminate\Http\Request;
use Session;

class PaisController extends Controller{

    public function index(Request $request){
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $paises = Pais::where('nombre', 'LIKE', "%$keyword%")
				
                ->paginate($perPage);
        } else {
            $paises = Pais::paginate($perPage);
        }

        return view('backend.configuraciones.pais.index', compact('paises'));
    }

    public function create(){
        return view('backend.configuraciones.pais.create');
    }

    public function store(Request $request){
        $this->validate($request, [
			'nombre' => 'required'
		]);
        $requestData = $request->all();
        
        Pais::create($requestData);

        Session::flash('flash_message', trans('labels.frontend.pais').trans('alerts.frontend.created').'!');

        return redirect('admin/configuraciones/pais');
    }

    public function show($id){
        $pais = Pais::findOrFail($id);

        return view('backend.configuraciones.pais.show', compact('pais'));
    }

    public function edit($id){
        $pais = Pais::findOrFail($id);

        return view('backend.configuraciones.pais.edit', compact('pais'));
    }

    public function update($id, Request $request){
        $this->validate($request, [
			'nombre' => 'required'
		]);
        $requestData = $request->all();
        
        $pais = Pais::findOrFail($id);
        $pais->update($requestData);

        Session::flash('flash_message', trans('labels.frontend.pais').trans('alerts.frontend.updated').'!');

        return redirect('admin/configuraciones/pais');
    }

    public function destroy($id)
    {
        Pais::destroy($id);

        Session::flash('flash_message',  trans('labels.frontend.pais').trans('alerts.frontend.deleted').'!');

        return redirect('admin/configuraciones/pais');
    }
}
