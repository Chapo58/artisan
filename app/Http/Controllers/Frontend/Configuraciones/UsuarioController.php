<?php

namespace App\Http\Controllers\Frontend\Configuraciones;

use App\Http\Controllers\Controller;
use App\Http\Controllers\ImageController;
use App\Models\Access\User\User;
use App\Models\Backend\Configuraciones\Tip;
use App\Models\Backend\Empresas\Sucursal;
use App\Models\Backend\Planes\Plan;
use App\Models\Frontend\Caja\Caja;
use App\Models\Frontend\Configuraciones\Estado;
use App\Models\Frontend\Configuraciones\Historial;
use App\Models\Frontend\Configuraciones\Localidad;
use App\Models\Frontend\Configuraciones\Pais;
use App\Models\Frontend\Configuraciones\Provincia;
use App\Models\Frontend\Configuraciones\PuntoDeVenta;
use App\Models\Frontend\Configuraciones\TipoHistorial;
use App\Models\Frontend\Personas\Domicilio;
use App\Models\Frontend\Personas\Empleado;
use App\Repositories\Backend\Access\Role\RoleRepository;
use App\Repositories\Backend\Access\User\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Facades\Datatables;
use Session;

class UsuarioController extends Controller{

    protected $users;
    protected $roles;

    public function __construct(UserRepository $users, RoleRepository $roles){
        $this->users = $users;
        $this->roles = $roles;
    }

    public function index(){
        return view('frontend.configuraciones.usuario.index');
    }
    public function datos(){

            $query = DB::table('users');
            $query->join('role_user', 'role_user.user_id', '=', 'users.id');
            $query->join('roles', 'roles.id', '=', 'role_user.role_id');
            $query->join('empresas', 'empresas.id', '=', 'users.empresa_id');
            $query->leftjoin('empleados', 'empleados.id', '=', 'users.empleado_id');
            $query->leftjoin('sucursales', 'sucursales.id', '=', 'users.sucursal_id');
            $query->select('users.*', 'empresas.razon_social as empresa', 'sucursales.nombre as sucursal', 'empleados.nombre as empleado', 'roles.name as rol');
            $query->where('users.empresa_id', Auth::user()->empresa_id);
            $query->where('users.email', '<>' , 'demo@ciatt.com.ar');
            $query->where('users.deleted_at', '=', Null);
            if(Auth::user()->hasRole('Usuario Vendedor')){
                $query->where('users.sucursal_id', Auth::user()->sucursal_id);
            }
            $usuarios = $query;

       return Datatables::of($usuarios)
            ->addColumn('action', function ($usuarios) {
                return '
                <a href="'.url('/configuraciones/usuario/' . $usuarios->id).'" class="mytooltip"><i class="fa fa-eye text-success m-r-10"></i>
                    <span class="tooltip-content3">Ver Usuario</span>
                </a>
                <a href="'.url('/configuraciones/usuario/' . $usuarios->id . '/edit').'" class="mytooltip">
                    <i class="fa fa-pencil text-info m-r-10"></i>
                    <span class="tooltip-content3">Modificar Usuario</span>
                </a>
                <a class="delete mytooltip pointer" data-id="'.$usuarios->id.'">
                    <i class="fa fa-close text-danger"></i>
                    <span class="tooltip-content3">Eliminar Usuario</span>
                </a>
                ';
            })
            ->setRowId('id')
            ->removeColumn('id')
            ->editColumn('status', function ($usuarios) {
              return ($usuarios->status == 1) ? 'Activado' : 'Inhabilitado';
             })
            ->editColumn('empleado', function ($usuarios) {
               return isset($usuarios->empleado) ? $usuarios->empleado : 'Sin empleado asociado.';
             })
            ->editColumn('sucursal', function ($usuarios) {
               return isset($usuarios->sucursal) ? $usuarios->sucursal : 'Sin sucursal asociada.';
             })
            ->make(true);
    }
    public function create(){
        if(Auth::user()->hasRole('Usuario Vendedor') || Auth::user()->hasRole('Usuario Gerente')){
            return redirect('configuraciones/usuario');
        }else{
            $empresa = Auth::user()->empresa;
            if($empresa->plan){
                $cantidadUsuariosActual = User::where('empresa_id',$empresa->id)->count();
                $limitePlan = Plan::findOrFail($empresa->plan->id);
                if($cantidadUsuariosActual >= $limitePlan->cantidad_usuarios){
                    return redirect('configuraciones/usuario')->withFlashDanger('Limite de usuarios alcanzado! Para agregar mas usuarios mejore su <a href="'.url('configuraciones/pago/create').'">Plan de Empresa.</a>');
                }
            }

            $roles = ['' => 'Seleccione un rol...', 3 => 'Usuario Admin', 4 => 'Usuario Gerente', 5 => 'Usuario Vendedor'];
            $sucursales = ['' => 'Seleccione una opción...'] + Sucursal::getSucursalesPorEmpresaArray($empresa);
            $cajas = ['' => 'Seleccione una opción...'] + Caja::getCajasPorEmpresaArray($empresa);
            $puntos_venta = ['' => 'Seleccione una opción...'] + PuntoDeVenta::where('empresa_id', $empresa->id)->get()->pluck('numero', 'id')->toArray();

            $paises = ['' => 'Seleccione una opción...'] + Pais::pluck('nombre', 'id')->all();
            $provincias = ['' => 'Seleccione una opción...'] + Provincia::orderBy('nombre')->pluck('nombre', 'id')->all();

            return view('frontend.configuraciones.usuario.create', compact('sucursales','cajas', 'puntos_venta', 'roles','paises','provincias'));
        }
    }

    public function store(Request $request){
        $this->validate($request, [
			'name' => 'required',
			'email' => 'required|email|unique:users',
			'password' => 'required|min:8|confirmed',
			'password_confirmation' => 'required|min:8'
		]);

        if($request['rol_id'] == '5'){
            $this->validate($request, [
                'sucursal_id' => 'required'
            ]);
        }

        if(Auth::user()->hasRole('Usuario Admin')){
            DB::transaction(function() use ($request) {
                $requestData = $request->all();
                $empresa = Auth::user()->empresa;
                $sucursal = Sucursal::find((int)$requestData['sucursal_id']);

                $domicilio = New Domicilio($requestData['domicilio']);
                $domicilio->save();

                $empleado = New Empleado($requestData['empleado']);
                $empleado->empresa_id = $empresa->id;
                if(isset($sucursal)){
                    $empleado->sucursal_id = $sucursal->id;
                }
                $empleado->domicilio_id = $domicilio->id;
                $empleado->save();

                if($requestData['rol_id'] == '3'){
                    $rolUsuario = 'Usuario Admin';
                }elseif($requestData['rol_id'] == '4'){
                    $rolUsuario = 'Usuario Gerente';
                }else{
                    $rolUsuario = 'Usuario Vendedor';
                }

                $this->nuevoUsuario($requestData['name'], $requestData['email'], $requestData['password'], $requestData['password_confirmation'], $empresa, $sucursal, $empleado, $rolUsuario);

                Session::flash('flash_success', 'Usuario creado correctamente.');
            });
        }

        return redirect('configuraciones/usuario');
    }

    public function show($id){
        $usuario = User::findOrFail($id);
        $empresa = Auth::user()->empresa;
        $sucursales = ['' => 'Seleccione una opción...'] + Sucursal::getSucursalesPorEmpresaArray($empresa);
        $cajas = ['' => 'Seleccione una opción...'] + Caja::getCajasPorEmpresaArray($empresa);
        $puntos_venta = PuntoDeVenta::getPuntosVentaPorEmpresaSucursalArray($empresa);
        $logs = Historial::where('user_id', $id)->orderBy('created_at','DESC')->limit(200)->get();
        $tiposLog = [TipoHistorial::CREADO => 'Creación', TipoHistorial::ACTUALIZADO => 'Actualización', TipoHistorial::BORRADO => 'Eliminación'];

        $paises = ['' => 'Seleccione una opción...'] + Pais::pluck('nombre', 'id')->all();
        $provincias = ['' => 'Seleccione una opción...'] + Provincia::orderBy('nombre')->pluck('nombre', 'id')->all();

        if($usuario->empresa_id != Auth::user()->empresa_id){
            return redirect('configuraciones/usuario');
        }elseif(Auth::user()->hasRole('Usuario Vendedor')) {
            if ($usuario->sucursal_id != Auth::user()->sucursal_id) {
                return redirect('configuraciones/usuario');
            }
        }
        return view('frontend.configuraciones.usuario.show', compact('usuario','sucursales','cajas','puntos_venta','logs','tiposLog','paises','provincias'));
    }

    public function showLog($id){
        if(Auth::user()->hasRole('Usuario Vendedor') || Auth::user()->hasRole('Usuario Gerente')){
            return redirect('configuraciones/usuario');
        }else{
            $usuario = User::findOrFail($id);
            $empresa = Auth::user()->empresa;

            if($usuario->empresa_id != $empresa->id){
                return redirect('configuraciones/usuario');
            }

            $logs = Historial::where('user_id', $id)->orderBy('created_at','DESC')->limit(200)->get();

            $tiposLog = [TipoHistorial::CREADO => 'Creación', TipoHistorial::ACTUALIZADO => 'Actualización', TipoHistorial::BORRADO => 'Eliminación'];

            return view('frontend.configuraciones.usuario.show_logs', compact('usuario', 'logs', 'tiposLog'));
        }
    }

    public function edit($id){
        $usuario = User::findOrFail($id);
        if(Auth::user()->hasRole('Usuario Admin')){
            $empresa = Auth::user()->empresa;

            if($usuario->empresa_id == $empresa->id){
                $sucursales = ['' => 'Seleccione una opción...'] + Sucursal::getSucursalesPorEmpresaArray($empresa);
                $cajas = ['' => 'Seleccione una opción...'] + Caja::getCajasPorEmpresaArray($empresa);
                $puntos_venta = PuntoDeVenta::getPuntosVentaPorEmpresaSucursalArray($empresa);

                $paises = ['' => 'Seleccione una opción...'] + Pais::pluck('nombre', 'id')->all();
                $provincias = ['' => 'Seleccione una opción...'] + Provincia::orderBy('nombre')->pluck('nombre', 'id')->all();

                return view('frontend.configuraciones.usuario.edit', compact('usuario', 'cajas', 'sucursales', 'puntos_venta','paises','provincias'));
            }
        }
        return redirect('configuraciones/usuario');
    }

    public function update($id, Request $request){
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.$id,
		]);
        $usuario = User::findOrFail($id);

        if(Auth::user()->hasRole('Usuario Admin') && $usuario->email != 'demo@ciatt.com.ar') {
            if($usuario->empresa_id == Auth::user()->empresa_id) {
                DB::transaction(function () use ($usuario, $request) {
                    $requestData = $request->all();

                    if(isset($usuario->empleado->domicilio)){
                        $usuario->empleado->domicilio->update($requestData['domicilio']);
                    }else{
                        $domicilio = New Domicilio($requestData['domicilio']);
                        $domicilio->save();
                        $usuario->empleado->domicilio_id = $domicilio->id;
                    }

                    $usuario->empleado->nombre = $requestData['empleado']['nombre'];
                    $usuario->empleado->cuil = $requestData['empleado']['cuil'];
                    $usuario->empleado->caja_id = $requestData['empleado']['caja_id'];
                    $usuario->empleado->punto_venta_id = $requestData['empleado']['punto_venta_id'];
                    $usuario->empleado->telefono = $requestData['empleado']['telefono'];
                    $usuario->empleado->update();

                    if($usuario->hasRole('Usuario Vendedor')){
                        $assignees_roles[5] = '5';
                    }elseif($usuario->hasRole('Usuario Gerente')){
                        $assignees_roles[4] = '4';
                    }elseif($usuario->hasRole('Usuario Admin')){
                        $assignees_roles[3] = '3';
                    }

                    $controllerImagen = new ImageController();
                    $imagen_url = $controllerImagen->guardarImagen($request,'/uploads/usuarios/',500,500);

                    $request->request->add(['empresa_id' => $usuario->empresa_id]);
                    $request->request->add(['confirmed' => TRUE]);
                    $request->request->add(['assignees_roles' => $assignees_roles]);
                    if($imagen_url != false){
                        $request->request->add(['avatar' => $imagen_url]);
                    }

                    $this->users->update($usuario, ['data' => $request->except('assignees_roles'), 'roles' => $request->only('assignees_roles')]);

                    Session::flash('flash_success', 'Usuario modificado correctamente.');
                });
            }
        }

        return redirect('configuraciones/usuario');
    }

    public function destroy($id){
        $usuario = User::findOrFail($id);

        if(Auth::user()->hasRole('Usuario Admin') && $usuario->email != 'demo@ciatt.com.ar') {
            if($usuario->empresa_id == Auth::user()->empresa_id) {
                $usuario->delete();

                Session::flash('flash_success', 'Usuario eliminado correctamente.');
            }
        }

        return redirect('configuraciones/usuario');
    }

    public function nuevoUsuario($nombre, $email, $password, $password_confirmation, $empresa, $sucursal, $empleado, $rol){
        $assignees_roles = null;
        if($rol == 'Usuario Vendedor'){
            $assignees_roles[5] = '5';
        }elseif($rol == 'Usuario Gerente'){
            $assignees_roles[4] = '4';
        }elseif($rol == 'Usuario Admin'){
            $assignees_roles[3] = '3';
        }

        $parametrosDatos = [
            'name' => $nombre,
            'email' => $email,
            'password' => $password,
            'password_confirmation' => $password_confirmation,
            'status' => Estado::ACTIVO,
            'empresa_id' => $empresa->id,
            'empleado_id' => $empleado->id,
            'sucursal_id' => $sucursal->id,
            'confirmed' => TRUE
        ];

        $parametrosRoles = [
            'assignees_roles' => $assignees_roles
        ];

        $nuevoUsuario = $this->users->create(['data' => $parametrosDatos, 'roles' => $parametrosRoles]);

        $tips = Tip::get();

        foreach($tips as $tip){
            $tip->users()->attach($nuevoUsuario->id);
        }

        if(!Auth::user()){ // Cuando se crean una empresa nueva
            access()->login($nuevoUsuario);
        }
    }

    public function marcarTipLeido(Request $request){
        $tip = Tip::where('ruta','=',$request->ruta)->first();
        $tip->users()->where('user_id', Auth::user()->id)->update(['entendido' => "1"]);
    }

}
