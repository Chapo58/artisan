<?php

namespace App\Http\Controllers\Frontend\Configuraciones;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;

class AparienciaController extends Controller{

    public function index(){
        return view('frontend.configuraciones.apariencia.index');
    }

    public function actualizar(Request $request){
        $usuario = Auth::user();
        $color = $request->color;
        $usuario->panel_color = $color;
        $orientacion = $request->menu;
        $usuario->panel_orientacion = $orientacion;
        $usuario->save();

        return redirect('configuraciones/apariencia');
    }


}
