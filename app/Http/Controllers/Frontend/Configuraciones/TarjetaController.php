<?php

namespace App\Http\Controllers\Frontend\Configuraciones;

use App\Http\Controllers\Controller;
use App\Models\Frontend\Configuraciones\PlanTarjeta;
use App\Models\Frontend\Configuraciones\Tarjeta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Session;

class TarjetaController extends Controller{

    public function index(){
        $empresa = Auth::user()->empresa;
        $tarjeta = Tarjeta::where('tarjetas.empresa_id', $empresa->id)->get();

        return view('frontend.configuraciones.tarjeta.index', compact('tarjeta'));
    }

    public function create(){
        return view('frontend.configuraciones.tarjeta.create');
    }

    public function store(Request $request){
        $this->validate($request, [
            'numero' => 'required|integer',
            'nombre' => 'required'
        ]);
        $requestData = $request->all();

        $nuevaTarjeta = new Tarjeta($requestData);
        $nuevaTarjeta->empresa_id = Auth::user()->empresa_id;

        $nuevaTarjeta->save();

        return redirect('configuraciones/tarjeta')->withFlashSuccess('Tarjeta creada correctamente.');
    }

    public function show($id){
        $tarjeta = Tarjeta::findOrFail($id);
        $empresa = Auth::user()->empresa;

        if ($tarjeta->empresa_id == $empresa->id) {
            $planesTarjeta = PlanTarjeta::where('tarjeta_id', $id)->get();
            return view('frontend.configuraciones.tarjeta.show', compact('tarjeta','planesTarjeta'));
        } else {
            return redirect('configuraciones/tarjeta');
        }
    }

    public function edit($id){
        $tarjeta = Tarjeta::findOrFail($id);

        $empresa = Auth::user()->empresa;

        if ($tarjeta->empresa_id == $empresa->id) {
            return view('frontend.configuraciones.tarjeta.edit', compact('tarjeta'));
        } else {
            return redirect('configuraciones/tarjeta');
        }
    }

    public function update($id, Request $request){
        $this->validate($request, [
            'numero' => 'required|integer',
            'nombre' => 'required'
        ]);
        $requestData = $request->all();

        $tarjeta = Tarjeta::findOrFail($id);

        $empresa = Auth::user()->empresa;

        if ($tarjeta->empresa_id == $empresa->id){
            $tarjeta->update($requestData);

            Session::flash('flash_success', 'Tarjeta modificada correctamente.');
        }

        return redirect('configuraciones/tarjeta');
    }

    public function destroy($id){
        $tarjeta = Tarjeta::findOrFail($id);

        $empresa = Auth::user()->empresa;

        if ($tarjeta->empresa_id == $empresa->id){
            $tarjeta->delete();

            Session::flash('flash_success', 'Tarjeta eliminada correctamente.');
        }

        return redirect('configuraciones/tarjeta');
    }

    public function planesDeTarjeta(Request $request){
        $tarjeta_id = (int)$request->input('tarjeta_id');

        $tarjeta = Tarjeta::findOrFail($tarjeta_id);

        $empresa = Auth::user()->empresa;
        if ($tarjeta->empresa_id == $empresa->id) {
            $planes = $tarjeta->planesTarjeta;

            $resultado = $planes;
        }else{
            $resultado = 'Sin permisos';
        }
        return response()->json($resultado);
    }
}
