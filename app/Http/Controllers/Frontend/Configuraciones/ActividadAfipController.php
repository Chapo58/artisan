<?php

namespace App\Http\Controllers\Frontend\Configuraciones;

use App\Http\Controllers\Controller;
use App\Models\Frontend\Configuraciones\ActividadAfip;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;

class ActividadAfipController extends Controller{

    public function index(){
        $actividadafip = ActividadAfip::where('empresa_id', Auth::user()->empresa_id)->get();

        return view('frontend.configuraciones.actividad-afip.index', compact('actividadafip'));
    }

    public function create(){
        return view('frontend.configuraciones.actividad-afip.create');
    }

    public function store(Request $request){
        $this->validate($request, [
			'codigo' => 'required',
			'descripcion' => 'required'
		]);
        $requestData = $request->all();

        $actividadAfip = new ActividadAfip($requestData);
        $actividadAfip->empresa_id = Auth::user()->empresa_id;

        $actividadAfip->save();

        return redirect('configuraciones/actividad-afip')->withFlashSuccess('Actividad fiscal creada correctamente.');
    }

    public function show($id){
        $actividadafip = ActividadAfip::findOrFail($id);

        if($actividadafip->empresa_id == Auth::user()->empresa_id){
            return view('frontend.configuraciones.actividad-afip.show', compact('actividadafip'));
        }else{
            return redirect('configuraciones/actividad-afip');
        }
    }

    public function edit($id){
        $actividadafip = ActividadAfip::findOrFail($id);

        if($actividadafip->empresa_id == Auth::user()->empresa_id){
            return view('frontend.configuraciones.actividad-afip.edit', compact('actividadafip'));
        }else{
            return redirect('configuraciones/actividad-afip');
        }
    }

    public function update($id, Request $request){
        $this->validate($request, [
			'codigo' => 'required',
			'descripcion' => 'required'
		]);
        $requestData = $request->all();

        $actividadafip = ActividadAfip::findOrFail($id);

        if($actividadafip->empresa_id == Auth::user()->empresa_id){
            $actividadafip->update($requestData);
        }

        return redirect('configuraciones/actividad-afip')->withFlashSuccess('Actividad fiscal eliminada correctamente.');
    }

    public function destroy($id){
        $actividadafip = ActividadAfip::findOrFail($id);

        if($actividadafip->empresa_id == Auth::user()->empresa_id){
            $actividadafip->delete();
        }

        return redirect('configuraciones/actividad-afip')->withFlashSuccess('Actividad fiscal eliminada correctamente.');
    }
}
