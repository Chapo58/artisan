<?php

namespace App\Http\Controllers\Frontend\Configuraciones;

use App\Http\Controllers\Controller;
use App\Models\Frontend\Configuraciones\Tarjeta;
use App\Models\Frontend\Configuraciones\TarjetaPropia;
use App\Models\Frontend\Configuraciones\TipoTarjeta;
use App\Models\Frontend\Fondos\Banco;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Session;

class TarjetaPropiaController extends Controller{

    public function index(){
        $empresa = Auth::user()->empresa;

        $tarjetasPropias = TarjetaPropia::listarTarjetasPorEmpresa($empresa);

        $tiposTarjetas = [
            TipoTarjeta::CREDITO => 'Credito',
            TipoTarjeta::DEBITO => 'Debito'
        ];

        return view('frontend.configuraciones.tarjeta-propia.index', compact('tarjetasPropias','tiposTarjetas'));
    }

    public function create(){
        $empresa = Auth::user()->empresa;

        $bancos = Banco::getBancosPorEmpresaArray($empresa);

        $tiposTarjetas = [
            TipoTarjeta::CREDITO => 'Credito',
            TipoTarjeta::DEBITO => 'Debito'
        ];

        return view('frontend.configuraciones.tarjeta-propia.create', compact('tiposTarjetas','bancos'));
    }

    public function store(Request $request){
        $this->validate($request, [
            'nombre' => 'required',
            'banco_id' => 'required',
            'tipo' => 'required'
        ]);
        $requestData = $request->all();

        $empresa = Auth::user()->empresa;
        $sucursal = Auth::user()->sucursal;

        if(isset($request->fecha_vencimiento)){
            $requestData['fecha_vencimiento'] = Carbon::createFromFormat('Y-m', $request->fecha_vencimiento);
        }

        $nuevaTarjetaPropia = new TarjetaPropia($requestData);
        $nuevaTarjetaPropia->empresa_id = $empresa->id;
        $nuevaTarjetaPropia->sucursal_id = $sucursal->id;

        $nuevaTarjetaPropia->save();

        return redirect('configuraciones/tarjeta-propia')->withFlashSuccess('Tarjeta propia creada correctamente.');
    }

    public function show($id){
        $tarjetaPropia = TarjetaPropia::findOrFail($id);

        $empresa = Auth::user()->empresa;

        if ($tarjetaPropia->empresa_id == $empresa->id) {
            $tiposTarjetas = [
                TipoTarjeta::CREDITO => 'Credito',
                TipoTarjeta::DEBITO => 'Debito'
            ];

            return view('frontend.configuraciones.tarjeta-propia.show', compact('tarjetaPropia','tiposTarjetas'));
        } else {
            return redirect('configuraciones/tarjeta-propia');
        }
    }

    public function edit($id){
        $tarjetaPropia = TarjetaPropia::findOrFail($id);

        $empresa = Auth::user()->empresa;

        if ($tarjetaPropia->empresa_id == $empresa->id) {
            $bancos = Banco::getBancosPorEmpresaArray($empresa);

            $tiposTarjetas = [
                TipoTarjeta::CREDITO => 'Credito',
                TipoTarjeta::DEBITO => 'Debito'
            ];

            return view('frontend.configuraciones.tarjeta-propia.edit', compact('tarjetaPropia','tiposTarjetas', 'bancos'));
        } else {
            return redirect('configuraciones/tarjeta-propia');
        }
    }

    public function update($id, Request $request){
        $this->validate($request, [
            'nombre' => 'required',
            'banco_id' => 'required',
            'tipo' => 'required'
        ]);
        $requestData = $request->all();

        $empresa = Auth::user()->empresa;

        $tarjetaPropia = TarjetaPropia::findOrFail($id);

        if ($tarjetaPropia->empresa_id == $empresa->id) {
            if(isset($request->fecha_vencimiento)){
                $requestData['fecha_vencimiento'] = Carbon::createFromFormat('m/Y', $request->fecha_vencimiento);
            }
            $tarjetaPropia->update($requestData);

            Session::flash('flash_success', 'Tarjeta propia modificada correctamente.');
        }

        return redirect('configuraciones/tarjeta-propia');
    }

    public function destroy($id){
        $tarjetaPropia = TarjetaPropia::findOrFail($id);

        $empresa = Auth::user()->empresa;

        if ($tarjetaPropia->empresa_id == $empresa->id) {
            $tarjetaPropia->delete();

            Session::flash('flash_success', 'Tarjeta propia eliminada correctamente.');
        }

        return redirect('configuraciones/tarjeta-propia');
    }
}
