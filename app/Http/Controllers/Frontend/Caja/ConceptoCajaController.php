<?php

namespace App\Http\Controllers\Frontend\Caja;

use App\Http\Controllers\Controller;
use App\Models\Frontend\Caja\ConceptoPersonalizado;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\Datatables\Facades\Datatables;
use Session;

class ConceptoCajaController extends Controller{

    public function index(){
        return view('frontend.caja.concepto-caja.index');
    }

    public function datos(){
        $empresa = Auth::user()->empresa;
        $conceptosCaja = ConceptoPersonalizado::select('id','nombre')->where('empresa_id','=', $empresa->id)->get();
        return Datatables::of($conceptosCaja)
        ->addColumn('action', function ($conceptoCaja) {
                return '
                <a href="'.url('/caja/concepto-caja/' . $conceptoCaja->id).'" class="mytooltip"><i class="fa fa-eye text-success m-r-10"></i>
                    <span class="tooltip-content3">Ver Concepto de Caja</span>
                </a>
                <a href="'.url('/caja/concepto-caja/' . $conceptoCaja->id . '/edit').'" class="mytooltip">
                    <i class="fa fa-pencil text-info m-r-10"></i>
                    <span class="tooltip-content3">Modificar Concepto de Caja</span>
                </a>
                <a class="delete mytooltip pointer" data-id="'.$conceptoCaja->id.'">
                    <i class="fa fa-close text-danger"></i>
                    <span class="tooltip-content3">Eliminar Concepto de Caja</span>
                </a>
                ';
            })
        ->setRowId('id')
        ->removeColumn('id')
        ->make(true);
    }

    public function create(){
        return view('frontend.caja.concepto-caja.create');
    }

    public function store(Request $request){
        $this->validate($request, [
    			'nombre' => 'required'
    		]);
        $requestData = $request->all();

        $conceptoCaja = New ConceptoPersonalizado($requestData);
        $conceptoCaja->empresa_id = Auth::user()->empresa_id;

        $conceptoCaja->save();

        return redirect('caja/concepto-caja')->withFlashSuccess('Concepto de caja creado correctamente.');
    }

    public function show($id){
        $conceptocaja = ConceptoPersonalizado::findOrFail($id);

        if($conceptocaja->empresa_id == Auth::user()->empresa_id){
            return view('frontend.caja.concepto-caja.show', compact('conceptocaja'));
        }
    }

    public function edit($id){
        $conceptocaja = ConceptoPersonalizado::findOrFail($id);

        if($conceptocaja->empresa_id == Auth::user()->empresa_id){
            return view('frontend.caja.concepto-caja.edit', compact('conceptocaja'));
        }
    }

    public function update($id, Request $request){
        $this->validate($request, [
			'nombre' => 'required'
		]);
        $requestData = $request->all();

        $conceptocaja = ConceptoPersonalizado::findOrFail($id);
        $conceptocaja->update($requestData);

        return redirect('caja/concepto-caja')->withFlashSuccess('Concepto de caja modificado correctamente.');
    }

    public function destroy($id){
        $conceptocaja = ConceptoPersonalizado::findOrFail($id);

        if($conceptocaja->empresa_id == Auth::user()->empresa_id){
            ConceptoPersonalizado::destroy($id);
            return redirect('caja/concepto-caja')->withFlashSuccess('Concepto de caja eliminado correctamente.');
        }
    }
}
