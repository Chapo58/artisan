<?php

namespace App\Http\Controllers\Frontend\Caja;

use App\Http\Controllers\Controller;
use App\Models\Frontend\Caja\Caja;
use App\Models\Frontend\Caja\EstadoCaja;
use App\Models\Frontend\Caja\MovimientoCaja;
use App\Models\Frontend\Caja\ConceptoPersonalizado;
use App\Models\Frontend\Caja\CierreCaja;
use App\Models\Frontend\Caja\FormaDePago;
use App\Models\Frontend\Caja\ConceptoDeCaja;
use App\Models\Frontend\Configuraciones\PuntoDeVenta;
use App\Models\Frontend\Fondos\Cheque;
use App\Models\Frontend\Configuraciones\Moneda;
use App\Models\Frontend\Fondos\EstadoCheque;
use App\Models\Frontend\Caja\TipoMovimientoCaja;
use App\Models\Frontend\Personas\Empleado;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;
use DB;

class MovimientoCajaController extends Controller{

    public function index(){
        $empleado = Auth::user()->empleado;
        $empresa = Auth::user()->empresa;

        $cierreCaja = null;
        $totales = null;
        if($empleado->caja_id) {
            $cierreCaja = CierreCaja::getUltimoCierreCajaPorEmpleado($empleado);
            if(isset($cierreCaja)){
                if($cierreCaja->fecha_cierre != null){
                    $cierreCaja = null;
                    $movimientosDeCaja = null;
                }else{
                    $movimientosDeCaja = MovimientoCaja::listarMovimientosPorCierreDeCaja($cierreCaja);
                }
            }
        }

        $cajas = Caja::getCajasPorEmpresaArray($empresa);
        $formasDePago = FormaDePago::$getArray;
        $conceptosDeCaja = ConceptoDeCaja::$formasDePago;

        if(isset($cierreCaja)){
            $totales = MovimientoCaja::select(DB::raw('SUM(entrada) as entrada'),DB::raw('SUM(salida) as salida'), 'forma_de_pago')
                ->where('created_at',">=",$cierreCaja->fecha_apertura)
                ->where('caja_id',"=",$cierreCaja->caja_id)
                ->where('empresa_id', $empresa->id)
                ->groupBy('forma_de_pago')
                ->get();
        } else {
            $cierreCajaPorEmpleado = CierreCaja::getUltimoCierreCajaPorEmpleado($empleado);

            if($cierreCajaPorEmpleado){
                $totales = MovimientoCaja::select(DB::raw('SUM(entrada - salida) as total'))
                    ->where('created_at',">=",$cierreCajaPorEmpleado->fecha_apertura)
                    ->where('caja_id',"=",$cierreCajaPorEmpleado->caja_id)
                    ->where('forma_de_pago',FormaDePago::EFECTIVO)
                    ->whereNull('deleted_at')
                    ->first();
            }
        }



        return view('frontend.caja.movimiento-caja.index', compact('cierreCaja', 'movimientosDeCaja', 'empleado', 'formasDePago', 'conceptosDeCaja', 'totales', 'cajas'));
    }

    public function create($tipo){
        $empresa = Auth::user()->empresa;

        $monedas = Moneda::getMonedasPorEmpresaArray($empresa);
        $conceptosPersonalizados = ConceptoPersonalizado::getConceptosPersonalizadosPorEmpresaArray($empresa);
        $cajas = Caja::getCajasPorEmpresaArray($empresa);

        return view('frontend.caja.movimiento-caja.create', compact('monedas','tipo', 'conceptosPersonalizados', 'cajas'));
    }

    public function store(Request $request){

        if($request['tipo'] == TipoMovimientoCaja::TRANSFERENCIA){ // Si es una transferencia de saldo, valido primero que la caja destino este abierta
            $cajaDestino = $request['caja_destino'];
            $obtenerCaja = Caja::findOrFail($cajaDestino);
            if($obtenerCaja->estado != EstadoCaja::ABIERTA){
                Session::flash('flash_message', 'La caja destino esta cerrada!');
                return redirect('caja/movimiento-caja');
            }
        } else {
            $this->validate($request, [
                'concepto_de_caja' => 'required'
            ]);
        }

        $requestData = $request->all();

        $nuevoMovimiento = new MovimientoCaja($requestData);
        $nuevoMovimiento->usuario_id = Auth::user()->id;
        $nuevoMovimiento->empresa_id = Auth::user()->empresa_id;
        $nuevoMovimiento->entidad_id = $nuevoMovimiento->id;
        $nuevoMovimiento->forma_de_pago = FormaDePago::EFECTIVO;
        $nuevoMovimiento->caja_id = Auth::user()->empleado->caja_id;
        $nuevoMovimiento->url = '';
        $nuevoMovimiento->moneda_id = Auth::user()->empleado->caja->moneda->id;
        if($request['tipo'] == TipoMovimientoCaja::TRANSFERENCIA) {
            $nuevoMovimiento->concepto_personalizado_id = NULL;
            $nuevoMovimiento->concepto_de_caja = ConceptoDeCaja::TRANSFERENCIA_SALDO;
        } else {
            $nuevoMovimiento->concepto_personalizado_id = $request['concepto_de_caja'];
            $nuevoMovimiento->concepto_de_caja = NULL;
        }
        $nuevoMovimiento->entidad_clase = class_basename($nuevoMovimiento);
        if($request['tipo'] == TipoMovimientoCaja::INGRESO){
            $nuevoMovimiento->entrada = $request['importe'];
            $nuevoMovimiento->salida = 0;
        }else{ // En el caso de que sea una transferencia, tambien tiene que guardar el primer movimiento como salida
            $nuevoMovimiento->salida = $request['importe'];
            $nuevoMovimiento->entrada = 0;
        }
        $nuevoMovimiento->save();

        if($request['tipo'] == TipoMovimientoCaja::TRANSFERENCIA) { // Si es una transferencia, guardo el movimiento de entrada en la caja destino tambien
            $nuevoMovimiento = new MovimientoCaja($requestData);
            $nuevoMovimiento->usuario_id = Auth::user()->id;
            $nuevoMovimiento->empresa_id = Auth::user()->empresa_id;
            $nuevoMovimiento->entidad_id = $nuevoMovimiento->id;
            $nuevoMovimiento->forma_de_pago = FormaDePago::EFECTIVO;
            $nuevoMovimiento->caja_id = $cajaDestino;
            $nuevoMovimiento->url = '';
            $nuevoMovimiento->moneda_id = Auth::user()->empleado->caja->moneda->id;
            $nuevoMovimiento->concepto_personalizado_id = NULL;
            $nuevoMovimiento->concepto_de_caja = ConceptoDeCaja::TRANSFERENCIA_SALDO;
            $nuevoMovimiento->entidad_clase = class_basename($nuevoMovimiento);
            $nuevoMovimiento->entrada = $request['importe'];
            $nuevoMovimiento->salida = 0;
            $nuevoMovimiento->save();
        }

        return redirect('caja/movimiento-caja')->withFlashSuccess('Movimiento de caja creado correctamente.');
    }

    public function show($id){
        $movimientocaja = MovimientoCaja::findOrFail($id);
        $empresa = Auth::user()->empresa;
        $cajas = Caja::getCajasPorEmpresaArray($empresa);

        if($movimientocaja->empresa == $empresa) {
            $formasDePago = FormaDePago::$getArray;
            $conceptosDeCaja = ConceptoDeCaja::$formasDePago;

            return view('frontend.caja.movimiento-caja.show', compact('movimientocaja','formasDePago', 'conceptosDeCaja','conceptosPersonalizados','cajas'));
        }

        return redirect('caja/movimiento-caja');
    }

    public function edit($id){
        $movimientocaja = MovimientoCaja::findOrFail($id);
        $empresa = Auth::user()->empresa;
        $cajas = Caja::getCajasPorEmpresaArray($empresa);

        if($movimientocaja->empresa == $empresa) {
            $monedas = Moneda::getMonedasPorEmpresaArray($empresa);
            $conceptosDeCaja = ConceptoDeCaja::$formasDePago;
            $conceptosPersonalizados = ConceptoPersonalizado::getConceptosPersonalizadosPorEmpresaArray($empresa);
            $tipo = $movimientocaja->tipo;

            ($tipo == TipoMovimientoCaja::INGRESO) ? $movimientocaja->importe = $movimientocaja->entrada : $movimientocaja->importe = $movimientocaja->salida;

            return view('frontend.caja.movimiento-caja.edit', compact('movimientocaja','monedas', 'tipo', 'conceptosDeCaja','conceptosPersonalizados','cajas'));
        }

        return redirect('caja/movimiento-caja');
    }

    public function update($id, Request $request){
        $this->validate($request, [
    			'concepto_de_caja' => 'required'
    		]);
        $requestData = $request->all();
        if($request->tipo == TipoMovimientoCaja::INGRESO){
            $requestData['entrada'] = $request['importe'];
        } else {
            $requestData['salida'] = $request['importe'];
        }

        $movimientocaja = MovimientoCaja::findOrFail($id);
        $empresa = Auth::user()->empresa;

        if($movimientocaja->concepto_personalizado_id){
            $requestData['concepto_personalizado_id'] = $request['concepto_de_caja'];
            $requestData['concepto_de_caja'] = NULL;
        } else {
            $requestData['concepto_personalizado_id'] = NULL;
            $requestData['concepto_de_caja'] = $movimientocaja->concepto_de_caja;
        }

        if($movimientocaja->empresa == $empresa) {
            $movimientocaja->update($requestData);
        } else {
            return redirect('caja/movimiento-caja')->withFlashDanger('Acción ilegal.');
        }

        return redirect('caja/movimiento-caja')->withFlashSuccess('Movimiento de caja modificado correctamente.');
    }

    public function destroy($id){
        $movimientocaja = MovimientoCaja::findOrFail($id);
        $empresa = Auth::user()->empresa;

        if($movimientocaja->empresa == $empresa) {
            $movimientocaja->delete();
        } else {
            return redirect('caja/movimiento-caja')->withFlashDanger('Acción ilegal.');
        }

        return redirect('caja/movimiento-caja')->withFlashSuccess('Movimiento de caja eliminado correctamente.');
    }

    public function listar($id){
        $empresa = Auth::user()->empresa;

        $cierreCaja =  CierreCaja::findOrFail($id);

        $movimientosDeCaja = MovimientoCaja::listarMovimientosPorCierreDeCaja($cierreCaja);

        $formasDePago = FormaDePago::$getArray;
        $conceptosDeCaja = ConceptoDeCaja::$formasDePago;

        if($cierreCaja){
            $totales = MovimientoCaja::select(DB::raw('SUM(entrada) as entrada'),DB::raw('SUM(salida) as salida'), 'forma_de_pago')
                ->where('created_at',">=",$cierreCaja->fecha_apertura)
                ->where('caja_id',"=",$cierreCaja->caja_id)
                ->where('empresa_id', $empresa->id)
                ->groupBy('forma_de_pago')
                ->get();
        }

        $pdf = app('dompdf.wrapper');
        $pdf->getDomPDF()->set_option("enable_php", true);
        $pdf->loadView('frontend.informes.movimiento-caja', compact('cierreCaja', 'movimientosDeCaja','formasDePago', 'conceptosDeCaja', 'totales'));

        return $pdf->stream('Movimientos_Caja.pdf');
    }

    public function asignarCaja(Request $request){

        $empleado = Auth::user()->empleado;

        $modEmpleado = Empleado::findOrFail($empleado->id);
        $modEmpleado->caja_id = $request['caja_id'];
        $modEmpleado->save();

        return redirect('caja/movimiento-caja');

    }

    public function abrirCaja(Request $request){
        DB::transaction(function() use ($request) {
            $empleado = Auth::user()->empleado;
            $empresa = Auth::user()->empresa;

            $cajaEmpleado = $empleado->caja;
            if($cajaEmpleado->estado == EstadoCaja::CERRADA){
                $cajaEmpleado->estado = EstadoCaja::ABIERTA;
                $cajaEmpleado->save();

                //TODO TOTAL DE CHEQUES POR SUCURSAL????
                $importeCheques = Cheque::getChequesPorEmpresa($empresa, EstadoCheque::PENDIENTE)->sum('importe');

                if(isset($cajaEmpleado->cuentaBancaria)){
                    $saldoBancario = $cajaEmpleado->cuentaBancaria->saldo;
                }else{
                    $saldoBancario = 0;
                }

                $cierreCajaAnterior = CierreCaja::getUltimoCierreCajaPorEmpleado($empleado);

                if($cierreCajaAnterior){
                    $totalEfectivoUltimoCierre = MovimientoCaja::select(DB::raw('SUM(entrada - salida) as total'))
                        ->where('created_at',">=",$cierreCajaAnterior->fecha_apertura)
                        ->where('caja_id',"=",$cierreCajaAnterior->caja_id)
                        ->where('forma_de_pago',FormaDePago::EFECTIVO)
                        ->whereNull('deleted_at')
                        ->first();

                    $totalEfectivoUltimoCierre = $totalEfectivoUltimoCierre->total;
                } else {
                    $totalEfectivoUltimoCierre = $request['saldoEfectivo'];
                }


                $importeEfectivo = $request['saldoEfectivo'];

                $saldoTotal = $importeCheques + $saldoBancario + $importeEfectivo;

                CierreCaja::crearNuevoCierre($cajaEmpleado, $saldoTotal);

                $this->nuevaEntrada($totalEfectivoUltimoCierre,'',FormaDePago::EFECTIVO,ConceptoDeCaja::SALDO_INICIAL,$totalEfectivoUltimoCierre);
                if($importeCheques > 0){
                    $this->nuevaEntrada($importeCheques,'',FormaDePago::CHEQUE,ConceptoDeCaja::SALDO_INICIAL,$importeCheques);
                }
                if($saldoBancario > 0){
                    $this->nuevaEntrada($saldoBancario,'',FormaDePago::CUENTA_BANCARIA,ConceptoDeCaja::SALDO_INICIAL,$saldoBancario);
                }

                if($importeEfectivo != $totalEfectivoUltimoCierre){
                    $diferencia = abs($totalEfectivoUltimoCierre - $importeEfectivo);
                    if($importeEfectivo < $totalEfectivoUltimoCierre){
                        $this->nuevaSalida($diferencia,'',FormaDePago::EFECTIVO,ConceptoDeCaja::AJUSTE_SALDO,$diferencia);
                    } else {
                        $this->nuevaEntrada($diferencia,'',FormaDePago::EFECTIVO,ConceptoDeCaja::AJUSTE_SALDO,$diferencia);
                    }
                }
            }

        });

        return redirect('caja/movimiento-caja');
    }

    public function cerrarCaja(Request $request){
        DB::transaction(function() use ($request) {
            $empleado = Auth::user()->empleado;
            $empresa = Auth::user()->empresa;
            $cajaEmpleado = $empleado->caja;
            if($cajaEmpleado->estado == EstadoCaja::ABIERTA){
                $cajaEmpleado->estado = EstadoCaja::CERRADA;
                $cajaEmpleado->save();

                $importeEfectivo = $request['saldoEfectivo'];

                $cierreCaja = CierreCaja::getUltimoCierreCajaPorEmpleado($empleado);

                $totalEfectivo = MovimientoCaja::select(DB::raw('SUM(entrada - salida) as total'))
                    ->where('created_at',">=",$cierreCaja->fecha_apertura)
                    ->where('caja_id',"=",$cierreCaja->caja_id)
                    ->where('forma_de_pago',FormaDePago::EFECTIVO)
                    ->whereNull('deleted_at')
                    ->first();

                if($importeEfectivo != $totalEfectivo->total){
                    $diferencia = abs($totalEfectivo->total - $importeEfectivo);
                    if($importeEfectivo < $totalEfectivo->total){
                        $this->nuevaSalida($diferencia,'',FormaDePago::EFECTIVO,ConceptoDeCaja::AJUSTE_SALDO,$diferencia);
                    } else {
                        $this->nuevaEntrada($diferencia,'',FormaDePago::EFECTIVO,ConceptoDeCaja::AJUSTE_SALDO,$diferencia);
                    }
                }

                $sumatoriaDineroCaja = MovimientoCaja::select(DB::raw('SUM(entrada - salida) as total'))
                    ->where('created_at',">=",$cierreCaja->fecha_apertura)
                    ->where('caja_id',"=",$cierreCaja->caja_id)
                    ->whereNull('deleted_at')
                    ->first();
                $saldoTotal = $sumatoriaDineroCaja->total;

                CierreCaja::cerrarNuevoCierre($cajaEmpleado, $saldoTotal);
            }
        });

        return redirect('caja/cierre-caja');
    }

    public function imprimir($id){
        $movimientoCaja = MovimientoCaja::findOrFail($id);
        $empresa = Auth::user()->empresa;
        $cajas = Caja::getCajasPorEmpresaArray($empresa);

        if($movimientoCaja->empresa == $empresa) {
            $formasDePago = FormaDePago::$getArray;
            $conceptosDeCaja = ConceptoDeCaja::$formasDePago;

            return view('frontend.caja.movimiento-caja.impresion', compact('movimientoCaja','formasDePago', 'conceptosDeCaja','conceptosPersonalizados','cajas','empresa'));
        }

        return redirect('caja/movimiento-caja');
    }

    public function nuevaEntrada($objeto, $url, $formaDePago, $concepto, $importe = 0, $detalle = NULL){
        $this->generarMovimientoCaja($objeto, TipoMovimientoCaja::INGRESO, $url, $formaDePago, $concepto, $importe,$detalle);
    }

    public function nuevaSalida($objeto, $url, $formaDePago, $concepto, $importe = 0, $detalle = NULL){
        $this->generarMovimientoCaja($objeto, TipoMovimientoCaja::EGRESO, $url, $formaDePago, $concepto, $importe,$detalle);
    }

    private function generarMovimientoCaja($objeto, $tipoMovimiento, $url, $formaDePago, $concepto, $importe, $detalle){
        $caja = Auth::user()->empleado->caja;
        $empresa = Auth::user()->empresa;

        if($importe == 0){
            if(isset($objeto->importe)){
                $importe = $objeto->importe;
            }
        }

        $nuevoMovimiento = new MovimientoCaja();
        $nuevoMovimiento->usuario_id = Auth::user()->id;
        $nuevoMovimiento->empresa_id = $empresa->id;
        if($detalle){
            $nuevoMovimiento->detalle = $detalle;
        } else {
            if(isset($objeto->id)){
                $nuevoMovimiento->entidad_id = $objeto->id;
                if(isNonEmptyString($objeto->__toString())){
                    $nuevoMovimiento->detalle = trans('labels.frontend.'.strtolower(class_basename($objeto))).': '.$objeto->__toString();
                }
            }
        }
        $nuevoMovimiento->tipo = $tipoMovimiento;
        $nuevoMovimiento->concepto_de_caja = $concepto;
        $nuevoMovimiento->forma_de_pago = $formaDePago;
        $nuevoMovimiento->moneda_id = $caja->moneda->id;
        $nuevoMovimiento->caja_id = $caja->id;
        $nuevoMovimiento->url = $url;
        $nuevoMovimiento->entidad_clase = class_basename($objeto);

        if(isset($objeto->moneda)){
            if($objeto->moneda != $caja->moneda){
                if($objeto->moneda->cotizion == 1){
                    $importe = $importe / $caja->moneda->cotizacion;
                }else{
                    $importe = $importe * $objeto->moneda->cotizacion;
                }
            }
        }


        if($tipoMovimiento == TipoMovimientoCaja::INGRESO){
            $nuevoMovimiento->entrada = $importe;
            $nuevoMovimiento->salida = 0;
        }else{
            $nuevoMovimiento->salida = $importe;
            $nuevoMovimiento->entrada = 0;
        }
        $nuevoMovimiento->save();
    }
}
