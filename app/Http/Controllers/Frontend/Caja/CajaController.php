<?php

namespace App\Http\Controllers\Frontend\Caja;

use App\Http\Controllers\Controller;
use App\Models\Backend\Empresas\Sucursal;
use App\Models\Frontend\Caja\Caja;
use App\Models\Frontend\Caja\EstadoCaja;
use App\Models\Frontend\Configuraciones\Moneda;
use App\Models\Frontend\Configuraciones\Estado;
use App\Models\Frontend\Fondos\CuentaBancaria;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;

class CajaController extends Controller{

    public function index(){
        $empresa = Auth::user()->empresa;
        $sucursal = Auth::user()->sucursal;

        $query = Caja::join('sucursales', 'sucursales.id', '=', 'cajas.sucursal_id');
        $query->where('sucursales.empresa_id', $empresa->id);
        if(Auth::user()->hasRole('Usuario Vendedor')){
            $query->where('cajas.sucursal_id', $sucursal->id);
        }
        $query->select('cajas.*');
        $cajas = $query->get();

        $estadosCaja = EstadoCaja::$getArray;

        return view('frontend.caja.cajas.index', compact('cajas','estadosCaja'));
    }

    public function create(){
        $empresa = Auth::user()->empresa;

        $monedas = Moneda::getMonedasPorEmpresaArray($empresa);
        $cuentaBancaria = CuentaBancaria::getCuentasBancariasPorEmpresaArray($empresa);
        $sucursales = Sucursal::getSucursalesPorEmpresaArray($empresa);

        return view('frontend.caja.cajas.create', compact('monedas','cuentaBancaria', 'sucursales'));
    }

    public function store(Request $request){
        $this->validate($request, [
    			'numero' => 'required|integer',
    			'nombre' => 'required',
    			'moneda_id' => 'required|exists:monedas,id'
    		]);
        $requestData = $request->all();

        $nuevaCaja = new Caja($requestData);
        $nuevaCaja->empresa_id = Auth::user()->empresa_id;
        $nuevaCaja->estado = EstadoCaja::CERRADA;

        $nuevaCaja->save();

        return redirect('caja/cajas')->withFlashSuccess('Caja creada correctamente.');
    }

    public function show($id){
        $caja = Caja::findOrFail($id);

        if($caja->empresa_id == Auth::user()->empresa_id){
            return view('frontend.caja.cajas.show', compact('caja'));
        }
    }

    public function edit($id){
        $empresa = Auth::user()->empresa;

        $caja = Caja::findOrFail($id);
        $monedas = Moneda::pluck('nombre', 'id')->all();
        $cuentaBancaria = CuentaBancaria::getCuentasBancariasPorEmpresaArray($empresa);
        $sucursales = Sucursal::getSucursalesPorEmpresaArray($empresa);

        if($caja->empresa_id == Auth::user()->empresa_id){
            return view('frontend.caja.cajas.edit', compact('caja','monedas','cuentaBancaria','sucursales'));
        }
    }

    public function update($id, Request $request){
        $caja = Caja::findOrFail($id);
        $this->validate($request, [
    			'numero' => 'required|integer',
    			'nombre' => 'required',
    			'moneda_id' => 'required|exists:monedas,id'
    		]);
        $requestData = $request->all();

        $caja->update($requestData);

        return redirect('caja/cajas')->withFlashSuccess('Caja modificada correctamente.');
    }

    public function destroy($id){
        $caja = Caja::findOrFail($id);

        if($caja->empresa_id == Auth::user()->empresa_id && Auth::user()->email != 'demo@ciatt.com.ar'){
            Caja::destroy($id);
        } else {
            return redirect('caja/cajas');
        }
        return redirect('caja/cajas')->withFlashSuccess('Caja eliminada correctamente.');
    }
}
