<?php

namespace App\Http\Controllers\Frontend\Caja;

use App\Http\Controllers\Controller;
use App\Models\Frontend\Caja\CierreCaja;
use App\Models\Frontend\Caja\ConceptoDeCaja;
use App\Models\Frontend\Caja\MovimientoCaja;
use App\Models\Frontend\Caja\FormaDePago;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\Datatables\Facades\Datatables;
use Session;
use DB;

class CierreCajaController extends Controller{

    public function index(){
        return view('frontend.caja.cierre-caja.index');
    }

    public function datos(){
        $empresa = Auth::user()->empresa;
        $cierresCaja = CierreCaja::join('cajas', 'cajas.id', '=', 'cierre_cajas.caja_id')
            ->select('cierre_cajas.id','cierre_cajas.fecha_apertura','cierre_cajas.fecha_cierre','cierre_cajas.saldo_apertura','cierre_cajas.saldo_cierre','cajas.nombre as caja')
            ->where('cajas.empresa_id','=', $empresa->id)
            ->whereNotNull('cierre_cajas.fecha_cierre')
            ->orderBy('cierre_cajas.id','DESC')
            ->get();
        return Datatables::of($cierresCaja)
            ->addColumn('action', function ($cierreCaja) {
                return '
                <a href="'.url('/caja/cierre-caja/' . $cierreCaja->id).'" class="mytooltip"><i class="fa fa-eye text-success m-r-10"></i>
                    <span class="tooltip-content3">Ver Cierre de Caja</span>
                </a>
                <a href="'.url('/caja/movimiento-caja/listar/' . $cierreCaja->id).'" class="mytooltip" target="_blank">
                    <i class="fa fa-print text-primary m-r-10"></i>
                    <span class="tooltip-content3">Imprimir Cierre de Caja</span>
                </a>
                <a class="delete mytooltip pointer" data-id="'.$cierreCaja->id.'">
                    <i class="fa fa-close text-danger"></i>
                    <span class="tooltip-content3">Eliminar Cierre de Caja</span>
                </a>
                ';
            })
            ->editColumn('fecha_apertura', function ($cierreCaja) {
                return $cierreCaja->fecha_apertura->format('d/m/Y H:i:s');
            })
            ->editColumn('fecha_cierre', function ($cierreCaja) {
                return $cierreCaja->fecha_cierre->format('d/m/Y H:i:s');
            })
            ->setRowId('id')
            ->removeColumn('id')
            ->make(true);
    }

    public function show($id){
        $cierreCaja = CierreCaja::findOrFail($id);
        $perPage = 25;
        $empresa = Auth::user()->empresa;
        $sucursal = Auth::user()->sucursal;

        if($cierreCaja->caja->empresa_id == $empresa->id){
            if($cierreCaja->fecha_cierre){
                $movimientocaja = MovimientoCaja::where('empresa_id', Auth::user()->empresa_id)
                    ->where('created_at','>=',$cierreCaja->fecha_apertura)
                    ->where('created_at','<=',$cierreCaja->fecha_cierre)
                    ->where('caja_id',"=",$cierreCaja->caja_id)
                    ->paginate($perPage);
            } else {
                $movimientocaja = MovimientoCaja::where('empresa_id', Auth::user()->empresa_id)
                    ->where('created_at','>=',$cierreCaja->fecha_apertura)
                    ->where('caja_id',"=",$cierreCaja->caja_id)
                    ->paginate($perPage);
            }
            $formasDePago = FormaDePago::$getArray;
            $totales = MovimientoCaja::select(DB::raw('SUM(entrada) as entrada'),DB::raw('SUM(salida) as salida'), 'forma_de_pago')
                ->where('created_at',">=",$cierreCaja->fecha_apertura)
                ->where('created_at','<=',$cierreCaja->fecha_cierre)
                ->where('caja_id',"=",$cierreCaja->caja_id)
                ->groupBy('forma_de_pago')
                ->get();
            $conceptosDeCaja = ConceptoDeCaja::$formasDePago;

            if (Auth::user()->hasRole('Usuario Vendedor')) {
                if ($cierreCaja->caja->sucursal_id == $sucursal->id) {
                    return view('frontend.caja.cierre-caja.show', compact('cierreCaja','movimientocaja','formasDePago','totales','conceptosDeCaja'));
                } else {
                    return redirect('caja/cierre-caja');
                }
            }

            return view('frontend.caja.cierre-caja.show', compact('cierreCaja','movimientocaja','formasDePago','totales','conceptosDeCaja'));

        } else {
            return redirect('caja/cierre-caja');
        }


    }

    public function destroy($id){
        $cierreCaja = CierreCaja::findOrFail($id);

        if($cierreCaja->caja->empresa_id == Auth::user()->empresa_id){
            CierreCaja::destroy($id);
            return redirect('caja/cierre-caja')->withFlashSuccess('Cierre de Caja eliminado correctamente.');
        }
    }
}
