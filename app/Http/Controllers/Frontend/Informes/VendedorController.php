<?php

namespace App\Http\Controllers\Frontend\Informes;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Frontend\Caja\MovimientoCajaController;
use App\Models\Frontend\Articulos\Rubro;
use App\Models\Frontend\Caja\ConceptoDeCaja;
use App\Models\Frontend\Caja\FormaDePago;
use App\Models\Frontend\Configuraciones\TipoComprobante;
use App\Models\Frontend\Ventas\DetalleVenta;
use App\Models\Frontend\Ventas\EstadoFacturacion;
use App\Models\Frontend\Ventas\Venta;
use Carbon\Carbon;
use App\Models\Access\User\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;
use DB;

class VendedorController extends Controller{

    public function index(Request $request){
        $empresa = Auth::user()->empresa;

        $vendedores = User::getUsuariosPorEmpresaArray($empresa,true);
        $rubros = Rubro::getRubrosPorEmpresa($empresa);
        $tipos_comprobantes = TipoComprobante::$comprobantes;

        if($request->desde){
            $desde = $request->desde;
        } else {
            $desde = Carbon::now();
        }
        if($request->hasta){
            $hasta = $request->hasta;
        } else {
            $hasta = NULL;
        }
        if($request->comision){
            $comision = $request->comision;
        } else {
            $comision = NULL;
        }
        $encabezadosVentas = NULL;
        $detallesVentas = NULL;
        $vendedor = NULL;

        if($request->vendedor_id){
             $vendedor = User::findOrFail($request->vendedor_id);

            // Busco los encabezados de cada venta en el periodo especificado
            $query = Venta::where('empresa_id', $empresa->id);
            $query->where('usuario_id', $request->vendedor_id);
            $query->where('fecha', '>=', $desde);
            if($hasta){
                $query->where('fecha',"<=",$hasta);
            }
            $query->where(function($q){
                $q->where('estado_facturacion', '<>', EstadoFacturacion::ERROR);
                $q->orWhereNull('estado_facturacion');
            });
            $query->orderBy('fecha', 'asc');
            $encabezadosVentas = $query->get();
            if($encabezadosVentas->isEmpty()){
                Session::flash('flash_message', 'No posee comprobantes de venta en este rango de fechas!');
                return redirect('informes/vendedor');
            }

            // Busco los detalles de cada venta en el periodo especificado para calcular el total de cada factura manualmente
            $query = DetalleVenta::select(DB::raw('sum((detalles_ventas.precio_neto+detalles_ventas.importe_impuesto_interno)*detalles_ventas.cantidad) AS total'),'detalles_ventas.venta_id');
            $query->join('ventas','ventas.id','=','detalles_ventas.venta_id');
            $query->join('articulos','articulos.id','=','detalles_ventas.articulo_id');
            $query->where('ventas.empresa_id', $empresa->id);
            $query->where('ventas.usuario_id', $request->vendedor_id);
            $query->where(function($q){
                $q->where('ventas.estado_facturacion', '<>', EstadoFacturacion::ERROR);
                $q->orWhereNull('ventas.estado_facturacion');
            });
            if (!empty($request->input("rubros"))) {
                $rubrosFiltrados = $request->input("rubros");
                foreach($rubrosFiltrados as $item){
                    $query->where('articulos.rubro_id', '<>', $item);
                }
            }
            $query->where('ventas.fecha', '>=', $desde);
            if($hasta){
                $query->where('ventas.fecha',"<=",$hasta);
            }
            $query->groupBy('detalles_ventas.venta_id');
            $detallesVentas = $query->get();
        }

        if($request->impresion){
            return view('frontend.informes.vendedores.imprimir', compact( 'vendedor','desde','hasta','rubros','encabezadosVentas','detallesVentas','tipos_comprobantes','comision'));
        } else if ($request->liquidar){
            $importe = ($comision * $detallesVentas->sum('total')) / 100;
            $salidaEfectivo = new MovimientoCajaController();
            $salidaEfectivo->nuevaSalida($request, '', formaDePago::EFECTIVO, ConceptoDeCaja::EGRESOS_VARIOS,$importe,'Liquidación de Comisión de '.$vendedor);
            return redirect('caja/movimiento-caja');
        } else {
            return view('frontend.informes.vendedores.index', compact( 'vendedores','desde','hasta','rubros','encabezadosVentas','detallesVentas','tipos_comprobantes','comision'));
        }


    }

}
