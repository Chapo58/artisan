<?php

namespace App\Http\Controllers\Frontend\Informes;

use App\Http\Controllers\Controller;
use App\Models\Frontend\Configuraciones\ActividadAfip;
use App\Models\Frontend\Configuraciones\CondicionIva;
use App\Models\Frontend\Configuraciones\PuntoDeVenta;
use App\Models\Frontend\Ventas\DetalleNotaCredito;
use App\Models\Frontend\Ventas\EstadoFacturacion;
use App\Models\Frontend\Ventas\Venta;
use App\Models\Frontend\Ventas\DetalleVenta;
use App\Models\Frontend\Configuraciones\TipoComprobante;
use App\Models\Frontend\Configuraciones\PorcentajeIva;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;
use DB;

class ActividadController extends Controller{

    public function index(Request $request){
        return view('frontend.informes.actividad.index');
    }

    public function imprimir(Request $request){
      $empresa = Auth::user()->empresa;
      $sucursal = Auth::user()->sucursal;
      $periodo = $request->periodo;
      $year = substr($periodo, -4);
      $month = substr($periodo, 0, 2);
      $tipos_comprobantes = TipoComprobante::$comprobantes;
      $porcentajes_iva = PorcentajeIva::$arrayCombo;
      $constantes_iva = PorcentajeIva::$constantes;
      $actividades = ActividadAfip::getActividadesAfipPorEmpresa($empresa);
      $condicionesIva = CondicionIva::get();

      $puntosDeVenta = PuntoDeVenta::getPuntosDeVentaAPorSucursal($sucursal);
      if($puntosDeVenta->isEmpty()){
          Session::flash('flash_message', 'Su Empresa o Sucursal no posee puntos de venta validos!');
          return view('frontend.informes.actividad.index');
      }

      $query = DetalleVenta::select('detalles_ventas.precio_neto','detalles_ventas.cantidad','articulos.actividad_afip_id','detalles_ventas.porcentaje_iva',DB::raw('IFNULL(clientes.condicion_iva_id,5) as condicion_iva_id'),'ventas.tipo_comprobante_id','detalles_ventas.importe_impuesto_interno','ventas.importe_neto as importe_neto_total_venta','ventas.total as total_venta', 'ventas.importe_descuento');
      $query->join('ventas','ventas.id','=','detalles_ventas.venta_id');
      $query->join('clientes','clientes.id','=','ventas.cliente_id');
      $query->join('articulos','articulos.id','=','detalles_ventas.articulo_id');
      $query->where('ventas.empresa_id', $empresa->id);
      $query->where('ventas.estado_facturacion','<>', EstadoFacturacion::ERROR);
      $query->where('ventas.estado_facturacion','<>', EstadoFacturacion::PENDIENTE);
      $query->whereNull('ventas.deleted_at');
      $query->whereYear('ventas.fecha', '=', $year);
      $query->whereMonth('ventas.fecha', '=', $month);
      $query->where(function($q) use ($puntosDeVenta) {
          foreach ($puntosDeVenta as $key => $p){
              if($key == 0){
                  $q->Where('ventas.punto_venta_id', $p->id);
              } else {
                  $q->orWhere('ventas.punto_venta_id', $p->id);
              }
          }
      });
      $detallesVentas = $query->get();

      if($detallesVentas->isEmpty()){
          Session::flash('flash_message', 'No posee comprobantes de venta validos en este periodo!');
          return view('frontend.informes.actividad.index');
      }

      $query = DetalleNotaCredito::select('detalles_notas_credito.precio_neto','detalles_notas_credito.cantidad','articulos.actividad_afip_id','detalles_notas_credito.porcentaje_iva', DB::raw('IFNULL(clientes.condicion_iva_id,5) as condicion_iva_id'),'notas_credito.tipo_comprobante_id','detalles_notas_credito.importe_impuesto_interno','notas_credito.importe_neto as importe_neto_total_nota_credito','notas_credito.total as total_nota_credito','notas_credito.importe_descuento');
      $query->join('notas_credito','notas_credito.id','=','detalles_notas_credito.nota_credito_id');
      $query->join('clientes','clientes.id','=','notas_credito.cliente_id');
      $query->join('articulos','articulos.id','=','detalles_notas_credito.articulo_id');
      $query->where('notas_credito.empresa_id', $empresa->id);
      $query->where('notas_credito.estado_facturacion','<>', EstadoFacturacion::ERROR);
      $query->where('notas_credito.estado_facturacion','<>', EstadoFacturacion::PENDIENTE);
      $query->whereNull('notas_credito.deleted_at');
      $query->whereYear('notas_credito.fecha', '=', $year);
      $query->whereMonth('notas_credito.fecha', '=', $month);
      $query->where(function($q) use ($puntosDeVenta) {
          foreach ($puntosDeVenta as $key => $p){
              if($key == 0){
                  $q->Where('notas_credito.punto_venta_id', $p->id);
              } else {
                  $q->orWhere('notas_credito.punto_venta_id', $p->id);
              }
          }
      });
      $detallesNotasCredito = $query->get();

      foreach($actividades as $actividad){
          foreach($condicionesIva as $condicionIva){
              $detallesV = $detallesVentas
                  ->where('actividad_afip_id',$actividad->id)
                  ->where('condicion_iva_id',$condicionIva->id);

              $detallesN = $detallesNotasCredito
                  ->where('actividad_afip_id',$actividad->id)
                  ->where('condicion_iva_id',$condicionIva->id);

              $impuestosInternosVentas = 0;
              $impuestosInternosNotasCredito = 0;
              foreach($detallesV as $detalle){
                  $impuestosInternosVentas += $detalle->importe_impuesto_interno * $detalle->cantidad;
              }
              foreach($detallesN as $detalle){
                  $impuestosInternosNotasCredito += $detalle->importe_impuesto_interno * $detalle->cantidad;
              }
                // Importe impuestos internos de esta actividad y esta condicion de iva
              $impuestosInternosVentas = $impuestosInternosVentas - $impuestosInternosNotasCredito;
              $importes[$actividad->id][$condicionIva->id]['impInt'] = $impuestosInternosVentas;

              $importeTotalIvaVentas = 0;
              $importeTotalNetoVentas = 0;
              foreach($constantes_iva as $porcentajeIva){
                  $detallesV = $detallesVentas
                      ->where('actividad_afip_id',$actividad->id)
                      ->where('condicion_iva_id',$condicionIva->id)
                      ->where('porcentaje_iva',$porcentajeIva);

                  $detallesN = $detallesNotasCredito
                      ->where('actividad_afip_id',$actividad->id)
                      ->where('condicion_iva_id',$condicionIva->id)
                      ->where('porcentaje_iva',$porcentajeIva);

                  if($detallesV->isNotEmpty()){
                      $importeNetoVentas = 0;
                      $importeIvaVentas = 0;
                      $importeNetoNotasCredito = 0;
                      $importeIvaNotasCredito = 0;

                      foreach($detallesV as $detalle){
                          $calculoNeto = $detalle->precio_neto * $detalle->cantidad;

                          $porcentajeDescuento = (($detalle->importe_descuento * -1) / $detalle->importe_neto_total_venta) * 100;
                          $importeDescuento = ($calculoNeto * $porcentajeDescuento) / 100;
                          $calculoNetoDescuento = $calculoNeto - $importeDescuento;

                          if ($detalle->tipo_comprobante_id == 0) {
                              $calcularIva = $calculoNetoDescuento * ($porcentajes_iva[$porcentajeIva] / 100);
                          } else {
                              $calcularIva = $calculoNeto * ($porcentajes_iva[$porcentajeIva] / 100);
                          }

                          $importeNetoVentas += $calculoNetoDescuento;
                          $importeIvaVentas += $calcularIva;
                      }

                      foreach($detallesN as $detalle){
                          $calculoNeto = $detalle->precio_neto * $detalle->cantidad;

                          $porcentajeDescuento = (($detalle->importe_descuento * -1) / $detalle->importe_neto_total_nota_credito) * 100;
                          $importeDescuento = ($calculoNeto * $porcentajeDescuento) / 100;
                          $calculoNetoDescuento = $calculoNeto - $importeDescuento;

                          if ($detalle->tipo_comprobante_id == 0) {
                              $calcularIva = $calculoNetoDescuento * ($porcentajes_iva[$porcentajeIva] / 100);
                          } else {
                              $calcularIva = $calculoNeto * ($porcentajes_iva[$porcentajeIva] / 100);
                          }

                          $importeNetoNotasCredito += $calculoNetoDescuento;
                         $importeIvaNotasCredito += $calcularIva;
                      }

                      // Importe total neto de esta actividad, esta condicion de iva y este porcentaje de iva
                      $importes[$actividad->id][$condicionIva->id]['neto'.$porcentajeIva] = $importeNetoVentas - $importeNetoNotasCredito;
                      $importeTotalNetoVentas += $importeNetoVentas - $importeNetoNotasCredito;
                      $importes[$actividad->id][$condicionIva->id][$porcentajeIva] = $importeIvaVentas - $importeIvaNotasCredito;
                      $importeTotalIvaVentas += $importeIvaVentas - $importeIvaNotasCredito;
                  } else {
                      $importes[$actividad->id][$condicionIva->id]['neto'.$porcentajeIva] = 0;
                      $importes[$actividad->id][$condicionIva->id][$porcentajeIva] = 0;
                  }
              }
              $importes[$actividad->id][$condicionIva->id]['total'] = $importeTotalNetoVentas + $importeTotalIvaVentas + $impuestosInternosVentas;
          }
      }

      $venta = New Venta();
      $porcentajes = $venta->obtenerPorcentajesPeriodo($periodo);

      return view('frontend.informes.actividad.impresion', compact('periodo','tipos_comprobantes','porcentajes','porcentajes_iva','importes','actividades','condicionesIva','empresa'));
    }

}
