<?php

namespace App\Http\Controllers\Frontend\Informes;

use App\Http\Controllers\Controller;
use App\Models\Frontend\Compras\Compra;
use App\Models\Frontend\Compras\DetalleCompra;
use App\Models\Frontend\Configuraciones\PorcentajeIva;
use App\Models\Frontend\Configuraciones\TipoComprobante;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;
use DB;

class LibroIvaCompraController extends Controller{

    public function index(Request $request){
        return view('frontend.informes.libro-iva-compra.index');
    }

    public function imprimir(Request $request){
      $empresa = Auth::user()->empresa;
      $periodo = $request->periodo;
      $year = substr($periodo, -4);
      $month = substr($periodo, 0, 2);
      $tipos_comprobantes = TipoComprobante::$comprobantes;
      $porcentajes_iva = PorcentajeIva::$arrayCombo;

      // Busco los encabezados de cada compra en el año y mes seleccionados
      $compras = Compra::where('empresa_id', $empresa->id)
      ->where('punto_venta','<>', '9999')
      ->whereYear('fecha', '=', $year)
      ->whereMonth('fecha', '=', $month)
      ->whereNull('deleted_at')
      ->orderBy('fecha', 'asc')
      ->get();

      if($compras->isEmpty()){
          return redirect('informes/libro-iva-compra')
              ->withFlashDanger('No posee comprobantes de compra validos en este periodo!');
      }

        $compra = New Compra();
        $calculosInformes = New CalculosInformes();

      // Busco todos los porcentajes de IVA que se utilizaron en esas compras para crear las correspondientes columnas
        $porcentajes = $compra->obtenerPorcentajesPeriodo($periodo);

        foreach($porcentajes as $porcentaje){
            $importes['sumatoriaNeto'][$porcentaje->porcentaje_iva] = 0;
            $importes['sumatoriaIva'][$porcentaje->porcentaje_iva] = 0;
        }
        $importes['sumatoriaTotal'] = 0;

        foreach($compras as $compra){
            $resultado = $calculosInformes->calcularComprobante($compra);
            foreach($porcentajes as $porcentaje){
              if ($compra->tipo_comprobante_id == 1 || $compra->tipo_comprobante_id == 6 || $compra->tipo_comprobante_id == 11) {
                $importes['sumatoriaNeto'][$porcentaje->porcentaje_iva] += $importes[$compra->id]['neto'][$porcentaje->porcentaje_iva] = $resultado['neto'][$porcentaje->porcentaje_iva];
                $importes['sumatoriaIva'][$porcentaje->porcentaje_iva] += $importes[$compra->id]['iva'][$porcentaje->porcentaje_iva] = $resultado['iva'][$porcentaje->porcentaje_iva];
              }else{
                $importes['sumatoriaNeto'][$porcentaje->porcentaje_iva] -= $importes[$compra->id]['neto'][$porcentaje->porcentaje_iva] = $resultado['neto'][$porcentaje->porcentaje_iva];
                $importes['sumatoriaIva'][$porcentaje->porcentaje_iva] -= $importes[$compra->id]['iva'][$porcentaje->porcentaje_iva] = $resultado['iva'][$porcentaje->porcentaje_iva];
              }
            }
            if ($compra->tipo_comprobante_id == 1 || $compra->tipo_comprobante_id == 6 || $compra->tipo_comprobante_id == 11) {
              $importes['sumatoriaTotal'] += $compra->total;
            }else{
              $importes['sumatoriaTotal'] -= $compra->total;
            }

        }

      return view('frontend.informes.libro-iva-compra.impresion', compact('periodo','tipos_comprobantes','compras','porcentajes','porcentajes_iva','empresa','importes'));

    }

}
