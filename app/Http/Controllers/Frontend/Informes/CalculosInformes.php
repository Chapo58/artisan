<?php

namespace App\Http\Controllers\Frontend\Informes;

use App\Http\Controllers\Controller;
use App\Models\Frontend\Configuraciones\Estado;
use App\Models\Frontend\Configuraciones\Opcion;
use App\Models\Frontend\Configuraciones\PorcentajeIva;
use App\Models\Frontend\Ventas\DetalleNotaCredito;
use App\Models\Frontend\Ventas\DetalleVenta;
use App\Models\Frontend\Ventas\NotaCredito;
use App\Models\Frontend\Ventas\Venta;
use App\Models\Frontend\Compras\Compra;
use App\Models\Frontend\Compras\DetalleCompra;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;
use Session;
use DateTime;

class CalculosInformes extends Controller{
    protected $porcentajes_iva;
    protected $constantes_iva;

    public function __construct(){
        $this->porcentajes_iva = PorcentajeIva::$valores;
        $this->constantes_iva = PorcentajeIva::$constantes;
    }

    public function calcularComprobante($comprobante){
        if ($comprobante['table'] == 'ventas') {
            $detalles = DetalleVenta::where('detalles_ventas.venta_id', $comprobante->id)
                ->get();
        } else if ($comprobante['table'] == 'notas_credito') {
            $detalles = DetalleNotaCredito::where('detalles_notas_credito.nota_credito_id', $comprobante->id)
                ->get();
        } else {
            $detalles = DetalleCompra::where('detalles_compras.compra_id', $comprobante->id)
                ->get();
        }

        foreach ($this->constantes_iva as $porcentajeIva) {
            $resultados['neto'][$porcentajeIva] = 0;
            $resultados['iva'][$porcentajeIva] = 0;
        }

        foreach ($detalles as $detalle) {
            $importeNeto = $detalle->precio_neto * $detalle->cantidad;
            $resultados['neto'][$detalle->porcentaje_iva] += $importeNeto;
            $resultados['iva'][$detalle->porcentaje_iva] += round($importeNeto * $this->porcentajes_iva[$detalle->porcentaje_iva] / 100, 2);
        }

        $porcentajeDescuento = (($comprobante->importe_descuento * -1) / $comprobante->importe_neto) * 100;
        $totalNeto = 0;
        $totalIva = 0;

        foreach($this->constantes_iva as $porcentajeIva){
            $importeDescuento = ($resultados['neto'][$porcentajeIva] * $porcentajeDescuento) / 100;
            $calcularNeto = $resultados['neto'][$porcentajeIva] - $importeDescuento;
            if ($comprobante->tipo_comprobante_id == 1) { // Es Factura A
                $calcularIva = $calcularNeto * $this->porcentajes_iva[$porcentajeIva] / 100;
                $resultados['neto'][$porcentajeIva] = round($calcularNeto,2);
                $resultados['iva'][$porcentajeIva] = round($calcularIva,2);
            } else {
                $resultados['neto'][$porcentajeIva] = round($calcularNeto,2);
            }
        }

        return $resultados;
    }

}
