<?php

namespace App\Http\Controllers\Frontend\Informes;

use App\Http\Controllers\Controller;
use App\Models\Frontend\Caja\Caja;
use App\Models\Frontend\Caja\ConceptoPersonalizado;
use App\Models\Frontend\Caja\MovimientoCaja;
use ConsoleTVs\Charts\Facades\Charts;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;
use DB;

class ConceptoCajaController extends Controller{

    public function index(Request $request){
        $empleado = Auth::user()->empleado;
        $empresa = Auth::user()->empresa;

        if($request->desde){
            $desde = $request->desde;
            $filtrodesde = DateTime::createFromFormat('Y-m-d', $desde);
        } else {
            $desde = null;
            $filtrodesde = $empresa->created_at;
        }

        $conceptoSeleccionado = false;
        $chartEntrada = NULL;
        $chartSalida = NULL;
        $chartConcepto = NULL;

        if($request->concepto_personalizado_id){
            $query = MovimientoCaja::where('created_at',">=",$filtrodesde);
            $query->where('concepto_personalizado_id',$request->concepto_personalizado_id);
            $conceptoSeleccionado = $request->concepto_personalizado_id;

            $movs = MovimientoCaja::select(DB::raw('SUM((entrada - salida) * -1) as aggregate'),'concepto_personalizado_id','created_at')
                ->where('created_at',">=",$filtrodesde)
                ->where('concepto_personalizado_id',$request->concepto_personalizado_id)
                ->groupBy('concepto_personalizado_id','created_at')
                ->get();
            $mes = $filtrodesde->format('m');
            $año = $filtrodesde->format('Y');
            $chartConcepto = Charts::database($movs, 'bar', 'highcharts')
                ->elementLabel("Totales")
                ->title('Movimientos')
                ->dimensions(0, 0)
                ->preaggregated(true)
                ->groupByDay("$mes", "$año", true);

        } else {
            $query = MovimientoCaja::select(DB::raw('SUM(entrada) as entrada'),DB::raw('SUM(salida) as salida'), 'concepto_personalizado_id');
            $query->join('concepto_cajas','concepto_cajas.id','=','movimiento_cajas.concepto_personalizado_id');
            $query->where('movimiento_cajas.created_at',">=",$filtrodesde);
            $query->where('concepto_cajas.empresa_id', $empresa->id);
            $query->groupBy('concepto_personalizado_id');

            $movs = MovimientoCaja::select(DB::raw('SUM(entrada) as entrada'), DB::raw('SUM(salida) as salida'), 'concepto_cajas.nombre');
            $movs->join('concepto_cajas','concepto_cajas.id','=','movimiento_cajas.concepto_personalizado_id');
            $movs->where('movimiento_cajas.created_at',">=",$filtrodesde);
            $movs->where('concepto_cajas.empresa_id', $empresa->id);
            $movs->groupBy('concepto_cajas.nombre');
            $importes = $movs->get();

            $importesEntrada = $importes->where('entrada', '>' , 0)->pluck('entrada');
            $conceptosEntrada = $importes->where('entrada', '>' , 0)->pluck('nombre');

            $importesSalida = $importes->where('salida', '>' , 0)->pluck('salida');
            $conceptosSalida = $importes->where('salida', '>' , 0)->pluck('nombre');

            $coloresEntrada = array();
            $coloresSalida = array();
            $rand = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f');

            foreach($importesEntrada as $item){
                $color = '#'.$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)];
                array_push($coloresEntrada, $color);
            }
            foreach($importesSalida as $item){
                $color = '#'.$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)];
                array_push($coloresSalida, $color);
            }
            $chartEntrada = Charts::create('pie', 'highcharts')
                ->title('Entrada')
                ->labels($conceptosEntrada)
                ->values($importesEntrada)
                ->colors($coloresEntrada)
                ->dimensions(0,0);

            $chartSalida = Charts::create('pie', 'highcharts')
                ->title('Salida')
                ->labels($conceptosSalida)
                ->values($importesSalida)
                ->colors($coloresSalida)
                ->dimensions(0,0);
        }

        if($request->caja_id){
            $query->where('caja_id',$request->caja_id);
        }
        if($request->hasta){
            $hasta = $request->hasta;
            $filtrohasta = DateTime::createFromFormat('d/m/Y', $request->hasta);
            $query->where('movimiento_cajas.created_at',"<=",$hasta);
        } else {
            $hasta = null;
        }

        $conceptosLista = $query->paginate(20);

        $cajas = ['' => 'Todas'] + Caja::getCajasPorEmpresaArray($empresa);
        $conceptosPersonalizados = [0 => 'Todos'] + ConceptoPersonalizado::getConceptosPersonalizadosPorEmpresaArray($empresa);

        if($request->impresion){
            return view('frontend.informes.conceptos-caja.imprimir', compact( 'conceptosPersonalizados','cajas','empleado','desde','conceptosLista','conceptoSeleccionado','hasta','empresa'));
        } else {
            return view('frontend.informes.conceptos-caja.index', compact( 'conceptosPersonalizados','cajas','empleado','desde','conceptosLista','conceptoSeleccionado','chartEntrada','chartSalida','chartConcepto'));
        }

    }

}
