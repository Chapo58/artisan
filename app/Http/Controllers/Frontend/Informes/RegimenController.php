<?php

namespace App\Http\Controllers\Frontend\Informes;

use App\Http\Controllers\Controller;
use App\Models\Frontend\Configuraciones\PuntoDeVenta;
use App\Models\Frontend\Ventas\DetalleNotaCredito;
use App\Models\Frontend\Ventas\EstadoFacturacion;
use App\Models\Frontend\Ventas\NotaCredito;
use App\Models\Frontend\Ventas\Venta;
use App\Models\Frontend\Ventas\DetalleVenta;
use App\Models\Frontend\Compras\Compra;
use App\Models\Frontend\Compras\DetalleCompra;
use App\Models\Frontend\Configuraciones\TipoComprobante;
use App\Models\Frontend\Configuraciones\PorcentajeIva;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Session;
use DB;

class RegimenController extends Controller{

    public function index(Request $request){
        return view('frontend.informes.regimen');
    }

    public function comprobantesVentas(Request $request){
      $sucursal = Auth::user()->sucursal;
      $periodo = $request->periodo;

      $puntosDeVenta = PuntoDeVenta::getPuntosDeVentaAPorSucursal($sucursal);
      if($puntosDeVenta->isEmpty()){
          return redirect('informes/regimen')->withFlashDanger('Su Empresa o Sucursal no posee puntos de venta validos!');
      }

        // Busco los encabezados de cada venta en el año y mes seleccionados
        $venta = New Venta();
        $encabezadoVenta = $venta->obtenerVentasPeriodo($periodo,$puntosDeVenta);
          if($encabezadoVenta->isEmpty()){
              return redirect('informes/regimen')->withFlashDanger('No posee comprobantes de venta validos en este periodo!');
          }

        // Busco los encabezados de cada nota de credito en el año y mes seleccionados
        $encabezadoNotaCredito = New NotaCredito();
        $encabezadoNotaCredito = $encabezadoNotaCredito->obtenerNotasCreditoPeriodo($periodo,$puntosDeVenta);

      $comprobantes = $encabezadoVenta->merge($encabezadoNotaCredito);

      $saltoLinea="\r\n";

      $contador = 0;
      foreach($comprobantes as $comprobante){

        if($comprobante['table'] == 'ventas'){
            $porcentajesDeIva = DetalleVenta::select('detalles_ventas.porcentaje_iva')
                ->join('ventas','ventas.id','=','detalles_ventas.venta_id')
                ->where('ventas.id', $comprobante->id)
                ->groupBy('detalles_ventas.porcentaje_iva')
                ->get();
        } else {
          $porcentajesDeIva = DetalleNotaCredito::select('detalles_notas_credito.porcentaje_iva')
            ->join('notas_credito','notas_credito.id','=','detalles_notas_credito.nota_credito_id')
            ->where('notas_credito.id', $comprobante->id)
            ->groupBy('detalles_notas_credito.porcentaje_iva')
            ->get();
        }

          if($comprobante->puntoVenta->tipo == 'Impresora Fiscal'){
              switch ($comprobante->tipo_comprobante_id) {
                  case TipoComprobante::FACTURA_A:
                      $tipoComprobante = '081';
                      $tipoDocumento = '80';
                      break;
                  case TipoComprobante::FACTURA_B:
                      $tipoComprobante = '082';
                      ($comprobante->total > 1000) ? $tipoDocumento = '96' : $tipoDocumento = '99';
                      break;
                  case TipoComprobante::FACTURA_C:
                      $tipoComprobante = '111';
                      ($comprobante->total > 1000) ? $tipoDocumento = '96' : $tipoDocumento = '99';
                      break;
                  case TipoComprobante::NOTA_CREDITO_A:
                      $tipoComprobante = '112';
                      $tipoDocumento = '80';
                      break;
                  case TipoComprobante::NOTA_CREDITO_B:
                      $tipoComprobante = '113';
                      ($comprobante->total > 1000) ? $tipoDocumento = '96' : $tipoDocumento = '99';
                      break;
                  case TipoComprobante::NOTA_CREDITO_C:
                      $tipoComprobante = '114';
                      ($comprobante->total > 1000) ? $tipoDocumento = '96' : $tipoDocumento = '99';
                      break;
              }
          } else {
              switch ($comprobante->tipo_comprobante_id) {
                  case TipoComprobante::FACTURA_A:
                      $tipoComprobante = '001';
                      $tipoDocumento = '80';
                      break;
                  case TipoComprobante::FACTURA_B:
                      $tipoComprobante = '006';
                      ($comprobante->total > 1000) ? $tipoDocumento = '96' : $tipoDocumento = '99';
                      break;
                  case TipoComprobante::FACTURA_C:
                      $tipoComprobante = '011';
                      ($comprobante->total > 1000) ? $tipoDocumento = '96' : $tipoDocumento = '99';
                      break;
                  case TipoComprobante::NOTA_CREDITO_A:
                      $tipoComprobante = '003';
                      $tipoDocumento = '80';
                      break;
                  case TipoComprobante::NOTA_CREDITO_B:
                      $tipoComprobante = '008';
                      ($comprobante->total > 1000) ? $tipoDocumento = '96' : $tipoDocumento = '99';
                      break;
                  case TipoComprobante::NOTA_CREDITO_C:
                      $tipoComprobante = '013';
                      ($comprobante->total > 1000) ? $tipoDocumento = '96' : $tipoDocumento = '99';
                      break;
              }
          }
          
          if(!$comprobante->cliente->dni){ // Si el cliente no tiene dni le pongo el por defecto a consumidor final
              $comprobante->cliente->dni = '0';
          }

          if($contador){
              $fila .= $comprobante->fecha->format('Ymd'); // Fecha de comprobante (8)
          } else {
              $fila = $comprobante->fecha->format('Ymd'); // Fecha de comprobante (8)
          }

          $fila .= $tipoComprobante; // Tipo de comprobante (3)
          $fila .= '0'.$comprobante->puntoVenta->numero; // Punto de Venta (5)
          $fila .= '000000000000'.$comprobante->numero; // Numero de comprobante (20)
          $fila .= '000000000000'.$comprobante->numero; // Numero de comprobante hasta (20)
          $fila .= $tipoDocumento; // Codigo de documento del comprador (2)
          $fila .= str_pad($comprobante->cliente->dni, 20, "0", STR_PAD_LEFT); // Numero de documento (20)
          $fila .= str_pad(substr(trim(strtoupper(utf8_decode($comprobante->cliente->razon_social))), 0, 30), 30, ' ', STR_PAD_RIGHT); // Apellido y nombre del comprador (30)
          $fila .= str_pad(str_replace('.','',$comprobante->total), 15, "0", STR_PAD_LEFT); // Importe total (15, 13 enteros y 2 decimales)
          $fila .= str_pad('', 15, "0", STR_PAD_LEFT); // Importe conceptos que no integran el neto gravado (15, 13 enteros y 2 decimales)
          $fila .= str_pad('', 15, "0", STR_PAD_LEFT); // Percepcion a no categorizados (15, 13 enteros y 2 decimales)
          $fila .= str_pad('', 15, "0", STR_PAD_LEFT); // Importe operaciones excentas (15, 13 enteros y 2 decimales)
          $fila .= str_pad('', 15, "0", STR_PAD_LEFT); // Importe de percepciones o pagos a cuenta de impuestos nacionales (15, 13 enteros y 2 decimales)
          $fila .= str_pad('', 15, "0", STR_PAD_LEFT); // Importe de percepciones de ingresos brutos (15, 13 enteros y 2 decimales)
          $fila .= str_pad('', 15, "0", STR_PAD_LEFT); // Importe de percepciones impuestos municipales (15, 13 enteros y 2 decimales)
          $fila .= str_pad(str_replace('.','',$comprobante->importe_impuesto_interno), 15, "0", STR_PAD_LEFT); // Importe impuestos internos (15, 13 enteros y 2 decimales)
          $fila .= 'PES'; // Codigo de Moneda (3)
          $fila .= '0001000000'; // Tipo de cambio (10, 4 enteros y 6 decimales)
          $fila .= $porcentajesDeIva->count(); // Cantidad alicuotas de IVA (1)
          $fila .= '0'; // Codigo de operacion (1)
          $fila .= str_pad('', 15, "0", STR_PAD_LEFT); // Otros Tributos (15)
          if($comprobante->puntoVenta->tipo == 'Impresora Fiscal'){
              $fila .= '00000000'; // Fecha de vencimiento de pago (8)
          } else {
              $fila .= $comprobante->fecha->format('Ymd'); // Fecha de vencimiento de pago (8)
          }

          $fila .= $saltoLinea;

          $contador++;
      }

      $nombreArchivo = "ComprobantesVentas.txt";
      $headers = ['Content-type'=>'text/plain', 'Content-Disposition'=>sprintf('attachment; filename="%s"', $nombreArchivo)];
      return Response::make($fila, 200, $headers);

    }

    public function alicuotasVentas(Request $request){
        $sucursal = Auth::user()->sucursal;
        $periodo = $request->periodo;
        $calculosInformes = New CalculosInformes();

        $puntosDeVenta = PuntoDeVenta::getPuntosDeVentaAPorSucursal($sucursal);
        if($puntosDeVenta->isEmpty()){
            return redirect('informes/regimen')->withFlashDanger('Su Empresa o Sucursal no posee puntos de venta validos!');
        }

        // Busco los encabezados de cada venta en el año y mes seleccionados
        $venta = New Venta();
        $encabezadoVenta = $venta->obtenerVentasPeriodo($periodo,$puntosDeVenta);
        if($encabezadoVenta->isEmpty()){
            return redirect('informes/regimen')->withFlashDanger('No posee comprobantes de venta validos en este periodo!');
        }

        // Busco los encabezados de cada nota de credito en el año y mes seleccionados
        $encabezadoNotaCredito = New NotaCredito();
        $encabezadoNotaCredito = $encabezadoNotaCredito->obtenerNotasCreditoPeriodo($periodo,$puntosDeVenta);

        $comprobantes = $encabezadoVenta->merge($encabezadoNotaCredito);

        // Busco todos los porcentajes de IVA que se utilizaron en esas ventas para crear las correspondientes columnas
        $porcentajesIvaUtilizados = $venta->obtenerPorcentajesPeriodo($periodo);

        $saltoLinea="\r\n";
        $contador = 0;
        foreach($comprobantes as $comprobante){
            $contadorAlicuotas = 0;
            $sumatoriaNeto = 0; $sumatoriaIva = 0;

            foreach($porcentajesIvaUtilizados as $ivaUtilizado){

                $resultadosComprobante = $calculosInformes->calcularComprobante($comprobante);
                $contadorAlicuotas++;

                if($resultadosComprobante['neto'][$ivaUtilizado->porcentaje_iva]){ // Si esta factura tiene este iva

                    if($comprobante->puntoVenta->tipo == 'Impresora Fiscal'){
                        switch ($comprobante->tipo_comprobante_id) {
                            case TipoComprobante::FACTURA_A:
                                $tipoComprobante = '081';
                                break;
                            case TipoComprobante::FACTURA_B:
                                $tipoComprobante = '082';
                                break;
                            case TipoComprobante::FACTURA_C:
                                $tipoComprobante = '111';
                                break;
                            case TipoComprobante::NOTA_CREDITO_A:
                                $tipoComprobante = '112';
                                break;
                            case TipoComprobante::NOTA_CREDITO_B:
                                $tipoComprobante = '113';
                                break;
                            case TipoComprobante::NOTA_CREDITO_C:
                                $tipoComprobante = '114';
                                break;
                        }
                    } else {
                        switch ($comprobante->tipo_comprobante_id) {
                            case TipoComprobante::FACTURA_A:
                                $tipoComprobante = '001';
                                break;
                            case TipoComprobante::FACTURA_B:
                                $tipoComprobante = '006';
                                break;
                            case TipoComprobante::FACTURA_C:
                                $tipoComprobante = '011';
                                break;
                            case TipoComprobante::NOTA_CREDITO_A:
                                $tipoComprobante = '003';
                                break;
                            case TipoComprobante::NOTA_CREDITO_B:
                                $tipoComprobante = '008';
                                break;
                            case TipoComprobante::NOTA_CREDITO_C:
                                $tipoComprobante = '013';
                                break;
                        }
                    }

                    $importeNeto = $resultadosComprobante['neto'][$ivaUtilizado->porcentaje_iva];
                    $importeIva = $resultadosComprobante['iva'][$ivaUtilizado->porcentaje_iva];

                    $sumatoriaNeto += $importeNeto;
                    $sumatoriaIva += $importeIva;

                    $comprobante->total = $comprobante->total - $comprobante->importe_impuesto_interno;

                    if($porcentajesIvaUtilizados->count() == $contadorAlicuotas){ // Estoy calculando el ultimo iva de la factura
                        $format_sumatoriaNeto = number_format($sumatoriaNeto,2,'','');
                        $format_sumatoriaIva = number_format($sumatoriaIva,2,'','');
                        $format_total = number_format($comprobante->total,2,'','');

                        if(($format_sumatoriaNeto + $format_sumatoriaIva) > $format_total){ // Si el total que se calculo es mayor al total de la venta le resto la diferencia
                            $importeNeto -= ($sumatoriaNeto + $sumatoriaIva) - $comprobante->total;
                        } elseif(($format_sumatoriaNeto + $format_sumatoriaIva) < $format_total){ // Si es total calculado es menor, le sumo la diferencia al neto
                            $importeNeto += $comprobante->total - ($sumatoriaNeto + $sumatoriaIva);
                        }
                    }

                    // Le doy el formato correcto a los importes
                    $importeNeto = number_format($importeNeto,2,'','');
                    $importeIva = number_format($importeIva,2,'','');

                    if($contador){
                        $fila .= $tipoComprobante; // Tipo de comprobante (3)
                    } else {
                        $fila = $tipoComprobante; // Tipo de comprobante (3)
                    }
                    $fila .= '0'.$comprobante->puntoVenta->numero; // Punto de Venta (5)
                    $fila .= '000000000000'.$comprobante->numero; // Numero de comprobante (20)
                    $fila .= str_pad($importeNeto, 15, "0", STR_PAD_LEFT); // Importe neto gravado (15, 13 enteros y 2 decimales)
                    switch ($ivaUtilizado->porcentaje_iva) { // Alicuota de IVA (4)
                        case PorcentajeIva::CERO:
                            $fila .= '0003';
                            break;
                        case PorcentajeIva::DIEZCOMACINCO:
                            $fila .= '0004';
                            break;
                        case PorcentajeIva::VEINTIUNO:
                            $fila .= '0005';
                            break;
                        case PorcentajeIva::DOSCOMACINCO:
                            $fila .= '0009';
                            break;
                        case PorcentajeIva::CINCO:
                            $fila .= '0008';
                            break;
                        case PorcentajeIva::VEINTISIETE:
                            $fila .= '0006';
                    }
                    $fila .= str_pad($importeIva, 15, "0", STR_PAD_LEFT); // Impuesto Liquidado (15, 13 enteros y 2 decimales)
                    $fila .= $saltoLinea;
                }
            }
            $contador++;
        }

        $nombreArchivo = "AlicuotasVentas.txt";
        $headers = ['Content-type'=>'text/plain', 'Content-Disposition'=>sprintf('attachment; filename="%s"', $nombreArchivo)];
        return Response::make($fila, 200, $headers);

    }

    public function comprobantesCompras(Request $request){
      $empresa = Auth::user()->empresa;
      $periodo = $request->periodo;
      $year = substr($periodo, -4);
      $month = substr($periodo, 0, 2);

        // Busco los encabezados de cada compra en el año y mes seleccionados
        $encabezadoCompra = Compra::where('empresa_id', $empresa->id)
        ->where('punto_venta','<>', '9999')
        ->whereYear('fecha', '=', $year)
        ->whereMonth('fecha', '=', $month)
        ->whereNull('deleted_at')
        ->orderBy('fecha', 'asc')
        ->get();

        if($encabezadoCompra->isEmpty()){
            return redirect('informes/regimen')->withFlashDanger('No posee comprobantes de compra validos en este periodo!');
        }

      $saltoLinea="\r\n";
      $contador = 0;
      foreach($encabezadoCompra as $compra){
          $porcentajesDeIva = DetalleCompra::select('detalles_compras.porcentaje_iva')
              ->join('compras','compras.id','=','detalles_compras.compra_id')
              ->where('compras.id', $compra->id)
              ->groupBy('detalles_compras.porcentaje_iva')
              ->get();

          switch ($compra->tipo_comprobante_id) {
              case TipoComprobante::FACTURA_A:
                  $tipoComprobante = '001';
                  break;
              case TipoComprobante::FACTURA_B:
                  $tipoComprobante = '006';
                  break;
              case TipoComprobante::FACTURA_C:
                  $tipoComprobante = '011';
                  break;
              case TipoComprobante::NOTA_CREDITO_A:
                  $tipoComprobante = '003';
                  break;
              case TipoComprobante::NOTA_CREDITO_B:
                  $tipoComprobante = '008';
                  break;
              case TipoComprobante::NOTA_CREDITO_C:
                  $tipoComprobante = '013';
                  break;
          }
          $tipoDocumento = '80';
          $cuit = str_replace('-', '', $compra->proveedor->cuit);

          if($contador){
              $fila .= $compra->fecha->format('Ymd'); // Fecha de comprobante (8)
          } else {
              $fila = $compra->fecha->format('Ymd'); // Fecha de comprobante (8)
          }

          $fila .= $tipoComprobante; // Tipo de comprobante (3)
          $fila .= '0'.$compra->punto_venta; // Punto de Venta (5)
          $fila .= '000000000000'.$compra->numero; // Numero de comprobante (20)
          $fila .= str_pad('', 16, " ", STR_PAD_LEFT); // N° Despacho importacion (16)
          $fila .= $tipoDocumento; // Codigo de documento del vendedor (2)
          $fila .= str_pad($cuit, 20, "0", STR_PAD_LEFT); // Numero de documento (20)
          $fila .= str_pad(substr(trim(strtoupper(utf8_decode($compra->proveedor->razon_social))), 0, 30), 30, ' ', STR_PAD_RIGHT); // Apellido y nombre del vendedor (30)
          $fila .= str_pad(str_replace('.','',$compra->total), 15, "0", STR_PAD_LEFT); // Importe total (15, 13 enteros y 2 decimales)
          $fila .= str_pad(str_replace('.','',$compra->concepto_no_gravado), 15, "0", STR_PAD_LEFT); // Importe conceptos que no integran el neto gravado (15, 13 enteros y 2 decimales)
          $fila .= str_pad('', 15, "0", STR_PAD_LEFT); // Importe operaciones excentas (15, 13 enteros y 2 decimales)
          $fila .= str_pad('', 15, "0", STR_PAD_LEFT); // Importe de percepciones o pagos a cuenta del impuesto al valor agregado (15, 13 enteros y 2 decimales)
          $fila .= str_pad('', 15, "0", STR_PAD_LEFT); // Importe de percepciones o pagos a cuenta de impuestos nacionales (15, 13 enteros y 2 decimales)
          $fila .= str_pad(str_replace('.','',$compra->retencion), 15, "0", STR_PAD_LEFT); // Importe de percepciones de ingresos brutos (15, 13 enteros y 2 decimales)
          $fila .= str_pad('', 15, "0", STR_PAD_LEFT); // Importe de percepciones impuestos municipales (15, 13 enteros y 2 decimales)
          $fila .= str_pad(str_replace('.','',$compra->impuesto_interno), 15, "0", STR_PAD_LEFT); // Importe impuestos internos (15, 13 enteros y 2 decimales)
          $fila .= 'PES'; // Codigo de Moneda (3)
          $fila .= '0001000000'; // Tipo de cambio (10, 4 enteros y 6 decimales)
          if($compra->proveedor->condicion_iva_id == 1){ // Si el proveedor es monotributista no informo iva
              $fila .= '0'; // Cantidad alicuotas de IVA (1)
          } else {
              $fila .= $porcentajesDeIva->count(); // Cantidad alicuotas de IVA (1)
          }
          $fila .= '0'; // Codigo de operacion (1)
          $fila .= str_pad('', 15, "0", STR_PAD_LEFT); // Credito fiscal computable (15)
          $fila .= str_pad('', 15, "0", STR_PAD_LEFT); // Otros Tributos (15)
          $fila .= str_pad('', 11, "0", STR_PAD_LEFT); // CUIT Emisor (11)
          $fila .= str_pad('', 30, " ", STR_PAD_LEFT); // Denominacion del emisor (30)
          $fila .= str_pad('', 15, "0", STR_PAD_LEFT); // IVA Comision (15)

          $fila .= $saltoLinea;

          $contador++;
      }

      $nombreArchivo = "ComprobantesCompras.txt";
      $headers = ['Content-type'=>'text/plain', 'Content-Disposition'=>sprintf('attachment; filename="%s"', $nombreArchivo)];
      return Response::make($fila, 200, $headers);

    }

    public function alicuotasCompras(Request $request){
        $empresa = Auth::user()->empresa;
        $periodo = $request->periodo;
        $year = substr($periodo, -4);
        $month = substr($periodo, 0, 2);
        $calculosInformes = New CalculosInformes();

        // Busco los encabezados de cada compra en el año y mes seleccionados
        $encabezadoCompra = Compra::where('empresa_id', $empresa->id)
        ->where('punto_venta','<>', '9999')
        ->whereYear('fecha', '=', $year)
        ->whereMonth('fecha', '=', $month)
        ->whereNull('deleted_at')
        ->orderBy('fecha', 'asc')
        ->get();

        if($encabezadoCompra->isEmpty()){
            return redirect('informes/regimen')->withFlashDanger('No posee comprobantes de compra validos en este periodo!');
        }

        // Busco todos los porcentajes de IVA que se utilizaron en esas compras para crear las correspondientes columnas
        $compra = New Compra();
        $porcentajesIvaUtilizados = $compra->obtenerPorcentajesPeriodo($periodo);

        $saltoLinea="\r\n";
        $contador = 0;
        foreach($encabezadoCompra as $compra){
            $contadorAlicuotas = 0;
            $sumatoriaNeto = 0; $sumatoriaIva = 0;

            foreach($porcentajesIvaUtilizados as $ivaUtilizado){
                $resultadosComprobante = $calculosInformes->calcularComprobante($compra);
                $contadorAlicuotas++;

                if($resultadosComprobante['neto'][$ivaUtilizado->porcentaje_iva]){ // Si esta factura tiene este iva

                    $importeNeto = $resultadosComprobante['neto'][$ivaUtilizado->porcentaje_iva];
                    $importeIva = $resultadosComprobante['iva'][$ivaUtilizado->porcentaje_iva];

                    $sumatoriaNeto += $importeNeto;
                    $sumatoriaIva += $importeIva;

                    if($porcentajesIvaUtilizados->count() == $contadorAlicuotas){ // Estoy calculando el ultimo iva de la factura
                        $compra->total = number_format($compra->total - $compra->concepto_no_gravado - $compra->impuesto_interno,2,'.','');
                        $format_sumatoriaNeto = number_format($sumatoriaNeto,2,'','');
                        $format_sumatoriaIva = number_format($sumatoriaIva,2,'','');
                        $format_total = number_format($compra->total,2,'','');

                        if(($format_sumatoriaNeto + $format_sumatoriaIva) > $format_total){ // Si el total que se calculo es mayor al total de la compra le resto la diferencia
                            $importeNeto -= ($sumatoriaNeto + $sumatoriaIva) - $compra->total;
                        } elseif(($format_sumatoriaNeto + $format_sumatoriaIva) < $format_total){ // Si es total calculado es menor, le sumo la diferencia al neto
                            $importeNeto += $compra->total - ($sumatoriaNeto + $sumatoriaIva);
                        }
                    }

                    // Le doy el formato correcto a los importes
                    $importeNeto = number_format($importeNeto,2,'','');
                    $importeIva = number_format($importeIva,2,'','');

                    switch ($compra->tipo_comprobante_id) {
                        case TipoComprobante::FACTURA_A:
                            $tipoComprobante = '001';
                            break;
                        case TipoComprobante::FACTURA_B:
                            $tipoComprobante = '006';
                            break;
                        case TipoComprobante::FACTURA_C:
                            $tipoComprobante = '011';
                            break;
                        case TipoComprobante::NOTA_CREDITO_A:
                            $tipoComprobante = '003';
                            break;
                        case TipoComprobante::NOTA_CREDITO_B:
                            $tipoComprobante = '008';
                            break;
                        case TipoComprobante::NOTA_CREDITO_C:
                            $tipoComprobante = '013';
                            break;
                    }
                    $tipoDocumento = '80';
                    $cuit = str_replace('-', '', $compra->proveedor->cuit);

                    if($contador){
                        $fila .= $tipoComprobante; // Tipo de comprobante (3)
                    } else {
                        $fila = $tipoComprobante; // Tipo de comprobante (3)
                    }
                    $fila .= '0'.$compra->punto_venta; // Punto de Venta (5)
                    $fila .= '000000000000'.$compra->numero; // Numero de comprobante (20)
                    $fila .= $tipoDocumento; // Codigo de documento del vendedor (2)
                    $fila .= str_pad($cuit, 20, "0", STR_PAD_LEFT); // Numero de documento (20)
                    $fila .= str_pad($importeNeto, 15, "0", STR_PAD_LEFT); // Importe neto gravado (15, 13 enteros y 2 decimales)
                    switch ($ivaUtilizado->porcentaje_iva) { // Alicuota de IVA (4)
                        case PorcentajeIva::CERO:
                            $fila .= '0003';
                            break;
                        case PorcentajeIva::DIEZCOMACINCO:
                            $fila .= '0004';
                            break;
                        case PorcentajeIva::VEINTIUNO:
                            $fila .= '0005';
                            break;
                        case PorcentajeIva::DOSCOMACINCO:
                            $fila .= '0009';
                            break;
                        case PorcentajeIva::CINCO:
                            $fila .= '0008';
                            break;
                        case PorcentajeIva::VEINTISIETE:
                            $fila .= '0006';
                    }
                    $fila .= str_pad($importeIva, 15, "0", STR_PAD_LEFT); // Impuesto Liquidado (15, 13 enteros y 2 decimales)
                    $fila .= $saltoLinea;
                }
            }
            $contador++;
        }

        $nombreArchivo = "AlicuotasCompras.txt";
        $headers = ['Content-type'=>'text/plain', 'Content-Disposition'=>sprintf('attachment; filename="%s"', $nombreArchivo)];
        return Response::make($fila, 200, $headers);

    }

}
