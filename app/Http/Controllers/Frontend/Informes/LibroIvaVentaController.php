<?php

namespace App\Http\Controllers\Frontend\Informes;

use App\Http\Controllers\Controller;
use App\Models\Frontend\Configuraciones\PuntoDeVenta;
use App\Models\Frontend\Ventas\EstadoFacturacion;
use App\Models\Frontend\Ventas\NotaCredito;
use App\Models\Frontend\Ventas\Venta;
use App\Models\Frontend\Ventas\DetalleVenta;
use App\Models\Frontend\Configuraciones\TipoComprobante;
use App\Models\Frontend\Configuraciones\PorcentajeIva;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;
use DB;

class LibroIvaVentaController extends Controller{

    public function index(Request $request){
        return view('frontend.informes.libro-iva-venta.index');
    }

    public function imprimir(Request $request){
      $empresa = Auth::user()->empresa;
      $sucursal = Auth::user()->sucursal;
      $periodo = $request->periodo;
      $tipos_comprobantes = TipoComprobante::$comprobantes;
      $porcentajes_iva = PorcentajeIva::$arrayCombo;

      $puntosDeVenta = PuntoDeVenta::getPuntosDeVentaAPorSucursal($sucursal);
      if($puntosDeVenta->isEmpty()){
          return redirect('informes/libro-iva-venta')->withFlashDanger('Su Empresa o Sucursal no posee puntos de venta validos!');
      }

      // Busco los encabezados de cada venta en el año y mes seleccionados
        $venta = New Venta();
        $encabezadoVenta = $venta->obtenerVentasPeriodo($periodo,$puntosDeVenta);
        if($encabezadoVenta->isEmpty()){
            Session::flash('flash_message', 'No posee comprobantes de venta validos en este periodo!');
            return view('frontend.informes.libro-iva-venta.index');
        }

        // Busco los encabezados de cada nota de credito en el año y mes seleccionados
        $encabezadoNotaCredito = New NotaCredito();
        $encabezadoNotaCredito = $encabezadoNotaCredito->obtenerNotasCreditoPeriodo($periodo,$puntosDeVenta);

        $comprobantes = $encabezadoVenta->merge($encabezadoNotaCredito);

        $calculosInformes = New CalculosInformes();

        $porcentajes = $venta->obtenerPorcentajesPeriodo($periodo);

        foreach($porcentajes as $porcentaje){
            $importes['sumatoriaNeto'][$porcentaje->porcentaje_iva] = 0;
            $importes['sumatoriaIva'][$porcentaje->porcentaje_iva] = 0;
        }
        $importes['sumatoriaTotal'] = 0;

        foreach($comprobantes as $comprobante){
            $resultado = $calculosInformes->calcularComprobante($comprobante);
            foreach($porcentajes as $porcentaje){
                if($comprobante['table'] == 'ventas'){
                    $importes['sumatoriaNeto'][$porcentaje->porcentaje_iva] += $importes['venta'.$comprobante->id]['neto'][$porcentaje->porcentaje_iva] = $resultado['neto'][$porcentaje->porcentaje_iva];
                    $importes['sumatoriaIva'][$porcentaje->porcentaje_iva] += $importes['venta'.$comprobante->id]['iva'][$porcentaje->porcentaje_iva] = $resultado['iva'][$porcentaje->porcentaje_iva];
                } else {
                    $importes['sumatoriaNeto'][$porcentaje->porcentaje_iva] -= $importes['notacredito'.$comprobante->id]['neto'][$porcentaje->porcentaje_iva] = $resultado['neto'][$porcentaje->porcentaje_iva];
                    $importes['sumatoriaIva'][$porcentaje->porcentaje_iva] -= $importes['notacredito'.$comprobante->id]['iva'][$porcentaje->porcentaje_iva] = $resultado['iva'][$porcentaje->porcentaje_iva];
                }
            }
            if($comprobante['table'] == 'ventas'){
                $importes['sumatoriaTotal'] += $comprobante->total;
            } else {
                $importes['sumatoriaTotal'] -= $comprobante->total;
            }
        }

      return view('frontend.informes.libro-iva-venta.impresion', compact('periodo','tipos_comprobantes','comprobantes','porcentajes','porcentajes_iva','empresa','importes'));
    }

}
