<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Backend\Planes\Plan;
use App\Models\Frontend\Articulos\Articulo;
use App\Models\Frontend\Articulos\Stock;
use App\Models\Frontend\Configuraciones\Moneda;
use App\Models\Frontend\Configuraciones\PorcentajeIva;
use App\Models\Frontend\Ventas\ListaPrecio;
use App\Models\Frontend\Ventas\Precio;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

/**
 * Class FrontendController.
 */
class FrontendController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index(){
        if(Auth::user()){
            $rol = Auth::user()->roles->first()->name;
            if($rol == 'Administrador' || $rol == 'Distribuidor'){
                return redirect('admin/dashboard');
            } else if($rol == 'Cliente') {
                return redirect('ecommerce/dashboard');
            } else {
                return redirect('dashboard');
            }
        } else {
            $planes = Plan::get();
            return view('frontend.auth.login', compact('planes'));
        }
    }

    /**
     * @return \Illuminate\View\View
     */
    public function macros(){
        return view('frontend.macros');
    }

    public function welcome(){
        $empresa = Auth::user()->empresa;
        $listas_sin_usar = ListaPrecio::getListasPorEmpresa($empresa);
        $porcentajesIva = PorcentajeIva::$arrayCombo;

        return view('frontend.welcome', compact('porcentajesIva','listas_sin_usar'));
    }

    public function guardarDatos(Request $request){
        $this->validate($request, [
            'nombre' => 'required',
            'stock' => 'required',
            'ultimo_costo' => 'required',
            'porcentaje_iva' => 'required'
        ]);

        DB::transaction(function() use ($request) {
            $requestData = $request->all();
            $empresa = Auth::user()->empresa;
            $sucursal = Auth::user()->sucursal;

            $usuario = Auth::user();
            $color = $request->color;
            $usuario->panel_color = $color;
            $orientacion = $request->menu;
            $usuario->panel_orientacion = $orientacion;
            $usuario->save();

            (!isset($requestData['lleva_stock'])) ? $requestData['lleva_stock'] = 0 : $requestData['lleva_stock'] = 1;

            $articulo = new Articulo($requestData);
            $articulo->unidad_id = 2;//Unidad: Unidad
            $articulo->cantidad = 1;
            $articulo->empresa_id = $empresa->id;
            $moneda = Moneda::where('empresa_id', $empresa->id)->where('cotizacion', 1)->first();
            if(!$moneda){
                return redirect('articulos/articulo')->withFlashDanger('Necesita tener una moneda con cotizacion 1 para cargar un articulo rapido!');
            }
            $articulo->moneda_id = $moneda->id;
            $codigoArticulo = Articulo::where('empresa_id',$empresa->id)->max('codigo') + 1;
            $articulo->codigo = $codigoArticulo;
            $articulo->save();

            if(isset($requestData['precios'])){
                $precios = $requestData['precios'];
                foreach ($precios as $precio) {
                    if($precio['precio_neto'] != null){
                        $precioNuevo = new Precio($precio);
                        $precioNuevo->precio = $precio['precio_neto'];
                        $precioNuevo->articulo_id = $articulo->id;
                        $precioNuevo->save();
                    }
                }
            }

            if($requestData['lleva_stock']){
                $stock = New Stock();
                $stock->cantidad = $request->stock;
                $stock->maximo = 100;
                $stock->minimo = 0;
                $stock->alarma = true;
                $stock->articulo_id = $articulo->id;
                $stock->sucursal_id = $sucursal->id;
                $stock->save();
            }
        });

        return redirect('dashboard');

    }

}
