<?php

namespace App\Http\Controllers\Frontend\Auth;

use App\Http\Controllers\Controller;
use App\Repositories\Frontend\Access\User\UserRepository;
use App\Http\Requests\Frontend\User\ChangePasswordRequest;
use Illuminate\Support\Facades\Auth;

/**
 * Class ChangePasswordController.
 */
class ChangePasswordController extends Controller
{
    /**
   * @var UserRepository
   */
  protected $user;

  /**
   * ChangePasswordController constructor.
   *
   * @param UserRepository $user
   */
  public function __construct(UserRepository $user)
  {
      $this->user = $user;
  }

  /**
   * @param ChangePasswordRequest $request
   *
   * @return mixed
   */
  public function changePassword(ChangePasswordRequest $request)
  {
      if(Auth::user()->email != 'demo@ciatt.com.ar'){
          $this->user->changePassword($request->all());

          return redirect('configuraciones/usuario')->withFlashSuccess(trans('strings.frontend.user.password_updated'));
      } else {
          return redirect('configuraciones/usuario')->withFlashDanger('La contraseña del usuario Demo no puede cambiarse.');
      }
  }
}
