<?php

namespace App\Http\Controllers\Frontend\Auth;

use App\Http\Controllers\Backend\Empresas\EmpresaController;
use App\Http\Controllers\Controller;
use App\Events\Frontend\Auth\UserRegistered;
use App\Models\Frontend\Configuraciones\CondicionIva;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Http\Requests\Frontend\Auth\RegisterRequest;
use App\Repositories\Frontend\Access\User\UserRepository;

/**
 * Class RegisterController.
 */
class RegisterController extends Controller
{
    use RegistersUsers;

    /**
     * @var UserRepository
     */
    protected $user;

    /**
     * RegisterController constructor.
     *
     * @param UserRepository $user
     */
    public function __construct(UserRepository $user)
    {
        // Where to redirect users after registering
        $this->redirectTo = route('frontend.auth.login');

        $this->user = $user;
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm(){
        $condiciones_iva = ['' => 'Condición IVA'] + CondicionIva::pluck('nombre', 'id')->all();

        return view('frontend.auth.register', compact('condiciones_iva'));
    }

    /**
     * @param RegisterRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function register(RegisterRequest $request){
       /* if (config('access.users.confirm_email')) {
            EmpresaController::creacionAutomatica($request);

            return redirect($this->redirectPath())->withFlashSuccess(trans('exceptions.frontend.auth.confirmation.created_confirm'));
        } else {
            access()->login($this->user->create($request->all()));
            event(new UserRegistered(access()->user()));

            return redirect($this->redirectPath());
        }*/

        $this->validate($request, [
            'razon_social' => 'required',
            'condicion_iva_id' => 'required'
        ]);

        EmpresaController::creacionAutomatica($request);
        return redirect($this->redirectPath());
    }
}
