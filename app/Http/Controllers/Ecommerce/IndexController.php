<?php

namespace App\Http\Controllers\Ecommerce;

use App\Models\Frontend\Articulos\Articulo;
use App\Models\Frontend\Articulos\Marca;
use App\Models\Frontend\Articulos\Rubro;
use App\Models\Frontend\Articulos\Subrubro;
use App\Models\Frontend\ECommerce\Caracteristica;
use App\Models\Frontend\ECommerce\Pagina;
use App\Models\Frontend\ECommerce\Slider;
use App\Models\Frontend\ECommerce\Suscripcion;
use App\Models\Frontend\Ventas\DetalleVenta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Session;
use DB;

class IndexController extends BaseController {

    public function index(){
        $this->data['sliders'] = Slider::where('empresa_id', $this->data['empresa']->id)->get();
        $this->data['productosDestacados'] = Articulo::where('empresa_id', $this->data['empresa']->id)->where('destacado',1)->limit(12)->get();

        $masVendidos = DetalleVenta::select(DB::raw('COUNT(detalles_ventas.articulo_id) as cantidad'), 'articulos.nombre', 'articulos.imagen_url', 'articulos.id');
        $masVendidos->join('articulos','articulos.id','=','detalles_ventas.articulo_id');
        $masVendidos->where('articulos.empresa_id', $this->data['empresa']->id);
        $masVendidos->whereNull('articulos.deleted_at');
        $masVendidos->groupBy('articulos.nombre', 'articulos.imagen_url', 'articulos.id');
        $masVendidos->orderBy('cantidad','DESC');
        $masVendidos->limit(12);
        $articulosMasVendidos = $masVendidos->get();
        $this->data['productosMasVendidos'] = $articulosMasVendidos;
        $this->data['objetoArticulo'] = New Articulo();

        $this->data['caracteristicas'] = Caracteristica::where('empresa_id', $this->data['empresa']->id)->get();
        $this->data['marcas'] = Marca::where('empresa_id', $this->data['empresa']->id)->whereNotNull('imagen_url')->get();

        return view('ecommerce.'.$this->data['estilo'].'.index', $this->data);
    }

    public function categoria($id){
        $rubro = Rubro::find($id);
        $this->data['articulos'] = Articulo::where('rubro_id',$rubro->id)->limit(9)->get();
        $this->data['titulo'] = $rubro;
        $this->data['ruta'] = '<li><a href="/ecommerce">Inicio</a> /</li>
                               <li><a href="#">Categorias</a> /</li>
                               <li>'.$rubro.'</li>';
        $this->data['listadoDestacados'] = Articulo::where('empresa_id', $this->data['empresa']->id)->where('destacado',1)->limit(5)->get();

        return view('ecommerce.'.$this->data['estilo'].'.listado', $this->data);
    }

    public function subcategoria($id){
        $subrubro = Subrubro::find($id);
        $this->data['articulos'] = Articulo::where('subrubro_id',$subrubro->id)->limit(10)->get();
        $this->data['titulo'] = $subrubro;
        $this->data['ruta'] = '<li><a href="/ecommerce">Inicio</a> /</li>
                               <li><a href="#">Categorias</a> /</li>
                               <li><a href="#">'.$subrubro->rubro.'</a> /</li>
                               <li>'.$subrubro.'</li>';
        $this->data['listadoDestacados'] = Articulo::where('empresa_id', $this->data['empresa']->id)->where('destacado',1)->limit(5)->get();

        return view('ecommerce.'.$this->data['estilo'].'.listado', $this->data);
    }

    public function marca($id){
        $marca = Marca::find($id);
        $this->data['articulos'] = Articulo::where('marca_id',$marca->id)->limit(10)->get();
        $this->data['titulo'] = $marca;
        $this->data['ruta'] = '<li><a href="/ecommerce">Inicio</a> /</li>
                               <li><a href="#">Marcas</a> /</li>
                               <li>'.$marca.'</li>';
        $this->data['listadoDestacados'] = Articulo::where('empresa_id', $this->data['empresa']->id)->where('destacado',1)->limit(5)->get();

        return view('ecommerce.'.$this->data['estilo'].'.listado', $this->data);
    }

    public function buscar(Request $request){
        $keyword = $request->palabra_clave;

        $query = Articulo::where('articulos.empresa_id', $this->data['empresa']->id);
        $query->select('articulos.*');
        $query->leftjoin('rubros', 'rubros.id', '=', 'articulos.rubro_id');
        $query->leftjoin('subrubros', 'subrubros.id', '=', 'articulos.subrubro_id');
        $query->leftjoin('marcas', 'marcas.id', '=', 'articulos.marca_id');
        if(isset($keyword)){
            $query->where(function($q) use ($keyword) {
                $q->orWhere('rubros.nombre', 'LIKE', "%$keyword%");
                $q->orWhere('subrubros.nombre', 'LIKE', "%$keyword%");
                $q->orWhere(DB::raw('CONCAT_WS(" ", articulos.nombre, marcas.nombre, articulos.modelo)'), 'LIKE', "%$keyword%");
                $q->orWhere(DB::raw('CONCAT_WS(" ", articulos.nombre, marcas.nombre, articulos.modelo_secundario)'), 'LIKE', "%$keyword%");
                $q->orWhere(DB::raw('CONCAT_WS(" ", articulos.nombre, articulos.modelo, articulos.modelo_secundario)'), 'LIKE', "%$keyword%");
                $q->orWhere(DB::raw('CONCAT_WS(" ", articulos.nombre, articulos.modelo_secundario, articulos.modelo)'), 'LIKE', "%$keyword%");
                $q->orWhere(DB::raw('CONCAT_WS(" ", marcas.nombre, articulos.nombre, articulos.modelo)'), 'LIKE', "%$keyword%");
                $q->orWhere(DB::raw('CONCAT_WS(" ", marcas.nombre, articulos.modelo, articulos.modelo_secundario)'), 'LIKE', "%$keyword%");
                $q->orWhere(DB::raw('CONCAT_WS(" ", marcas.nombre, articulos.modelo_secundario, articulos.modelo)'), 'LIKE', "%$keyword%");
                $q->orWhere(DB::raw('CONCAT_WS(" ", articulos.codigo, articulos.modelo, articulos.modelo_secundario)'), 'LIKE', "%$keyword%");
                $q->orWhere(DB::raw('CONCAT_WS(" ", articulos.codigo, articulos.modelo_secundario, articulos.modelo)'), 'LIKE', "%$keyword%");
                if(strlen($keyword) >= 4){
                    $q->orWhere('articulos.codigo_barra', 'LIKE', "%$keyword%");
                }
            });
        }
        $query->limit(10);

        $this->data['articulos'] = $query->get();
        $this->data['titulo'] = 'Busqueda de Articulos';
        $this->data['ruta'] = '<li><a href="/ecommerce">Inicio</a> /</li>
                               <li><a href="#">Buscar</a> /</li>
                               <li>'.$keyword.'</li>';
        $this->data['listadoDestacados'] = Articulo::where('empresa_id', $this->data['empresa']->id)->where('destacado',1)->limit(5)->get();

        return view('ecommerce.'.$this->data['estilo'].'.listado', $this->data);
    }

    public function contacto(){
        return view('ecommerce.'.$this->data['estilo'].'.contacto', $this->data);
    }

    public function politicaDePrivacidad(){
        return view('ecommerce.'.$this->data['estilo'].'.politica_privacidad', $this->data);
    }

    public function terminosYCondiciones(){
        return view('ecommerce.'.$this->data['estilo'].'.terminos_condiciones', $this->data);
    }

    public function verPagina($id){
        $pagina = Pagina::find($id);
        $this->data['pagina'] = $pagina;

        return view('ecommerce.'.$this->data['estilo'].'.pagina', $this->data);
    }

    public function nuevaSuscripcion(Request $request){
        $this->validate($request, [
            'email' => 'required|email'
        ]);

        $suscripcion = New Suscripcion();
        $suscripcion->empresa_id = $this->data['empresa']->id;
        $suscripcion->email = $request->email;
        $suscripcion->save();
        return redirect('/ecommerce');
    }

    public function mailContacto(Request $request){
        $empresa = $this->data['empresa'];

        Mail::send('mails.contacto-ecommerce', ['request' => $request,], function ($mensaje) use ($request,$empresa) {
            $mensaje->from($request->email, $request->nombre);
            $mensaje->to($empresa->ecommerce->email, $empresa->ecommerce);
            $mensaje->subject('Contacto desde tu E-Commerce en Artisan');
        });
        if (count(Mail::failures()) > 0) {
            Session::flash('flash_message', 'El mail no pudo ser enviado, por favor utilice el chat para contactarnos.');
            return redirect('ecommerce\contacto');
        }

        Session::flash('flash_message', 'Hemos recibido tu mensaje correctamente, nos contactaremos contigo a la brevedad.');
        return redirect('ecommerce\contacto');
    }

}
