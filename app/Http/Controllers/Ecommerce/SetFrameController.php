<?php

namespace App\Http\Controllers\Ecommerce;

use App\Http\Controllers\Controller;

class SetFrameController extends Controller{

    public function setEmpresaId($empresa_id){
        session(['empresa_id' => $empresa_id]);

        return redirect('/ecommerce');
    }

}
