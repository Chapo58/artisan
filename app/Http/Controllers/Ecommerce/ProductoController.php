<?php

namespace App\Http\Controllers\Ecommerce;

use App\Models\Frontend\Articulos\Articulo;
use App\Models\Frontend\Articulos\Marca;
use Illuminate\Http\Request;
use DB;

class ProductoController extends BaseController {

    public function modalProducto(Request $request){
        $articulo = Articulo::find($request->id);

        $producto = [];
        $producto['titulo'] = $articulo->nombre;
        if($articulo->descripcion){
            $producto['descripcion'] = $articulo->descripcion;
        } else {
            $producto['descripcion'] = 'Sin Descripción';
        }
        $producto['codigo'] = $articulo->codigo;
        $producto['stock'] = $articulo->getStock();
        if($articulo->marca){
            $producto['marca'] = $articulo->marca->nombre;
        }
        $producto['modelo'] = $articulo->modelo;
        $producto['precio'] = $articulo->getPrecio();
        $producto['imagen1'] = url($articulo->imagen_url);

        return response()->json($producto);
    }

    public function verProducto($id){
        $articulo = Articulo::findOrFail($id);
        $this->data['producto'] = $articulo;
        $this->data['listadoDestacados'] = Articulo::where('empresa_id', $this->data['empresa']->id)->limit(5)->get();
        $this->data['listadoRelacionados'] = Articulo::where('subrubro_id',$articulo->subrubro->id)
            ->limit(5)
            ->get();

        return view('ecommerce.'.$this->data['estilo'].'.producto.index', $this->data);
    }

}
