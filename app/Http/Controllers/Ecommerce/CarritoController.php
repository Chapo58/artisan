<?php

namespace App\Http\Controllers\Ecommerce;

use App\Models\Backend\Planes\FormaPago;
use App\Models\Frontend\Articulos\Articulo;
use App\Models\Frontend\Configuraciones\Estado;
use App\Models\Frontend\Configuraciones\Opcion;
use App\Models\Frontend\Configuraciones\Pais;
use App\Models\Frontend\Configuraciones\PorcentajeIva;
use App\Models\Frontend\Configuraciones\Provincia;
use App\Models\Frontend\Ecommerce\DetalleOrdenVenta;
use App\Models\Frontend\ECommerce\OrdenVenta;
use App\Models\Frontend\Personas\Cliente;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CarritoController extends BaseController {

    public function index(){
        return view('ecommerce.'.$this->data['estilo'].'.carrito.index', $this->data);
    }

    public function checkout(){
        $this->data['paises'] = Pais::get();
        $this->data['provincias'] = Provincia::get();

        $this->data['deposito'] = FormaPago::where('empresa_id',$this->data['empresa']->id)->where('tipo_forma_pago','Deposito')->first();
        $this->data['efectivo'] = FormaPago::where('empresa_id',$this->data['empresa']->id)->where('tipo_forma_pago','Efectivo')->first();

        return view('ecommerce.'.$this->data['estilo'].'.carrito.checkout', $this->data);
    }

    public function agregar(Request $request){
        $articulo = Articulo::find($request->id);
        $cantidad = $request->cantidad;

        $cart = Cart::add($articulo->id, $articulo->nombre, $cantidad, $articulo->getPrecio())->associate('App\Models\Frontend\Articulos\Articulo');

        $carrito = [];
        $carrito['titulo'] = $articulo->nombre;
        $carrito['imagen'] = $articulo->imagen_url;
        $carrito['cantidad'] = $cantidad;
        $carrito['precio'] = $articulo->getPrecio();
        $carrito['rowId'] = $cart->rowId;

        $carrito['cantidad-articulos'] = Cart::count();
        $carrito['subtotal'] = '$'.Cart::subtotal(2, ',', '.');
//        $carrito['iva'] = '$'.Cart::tax(2, ',', '.');
        $carrito['total'] = '$'.Cart::total(2, ',', '.');

        return response()->json($carrito);
    }

    public function eliminar(Request $request){

        Cart::remove($request->id);

        $carrito = [];
        $carrito['cantidad-articulos'] = Cart::count();
        $carrito['subtotal'] = '$'.Cart::subtotal(2, ',', '.');
        $carrito['iva'] = '$'.Cart::tax(2, ',', '.');
        $carrito['total'] = '$'.Cart::total(2, ',', '.');

        return response()->json($carrito);
    }

    public function actualizar(Request $request){

        $requestData = $request->all();

        if(isset($requestData['detalles'])) {
            $detalles = $requestData['detalles'];
            foreach ($detalles as $detalle) {
                Cart::update($detalle['rowId'], $detalle['cantidad']);
            }
        }

        return redirect('ecommerce/carrito');

    }

    public function nuevaOrdenDeVenta(Request $request){
        $this->validate($request, [
            'razon_social' => 'required',
            'email' => 'required',
            'telefono' => 'required',
            'pais_id' => 'required',
            'direccion' => 'required',
            'localidad' => 'required',
            'provincia_id' => 'required',
            'codigo_postal' => 'required',
            'pago' => 'required'
        ]);

        DB::transaction(function () use ($request) {
            $requestData = $request->all();
            $empresa = $this->data['empresa'];

            $numero_orden_venta = Opcion::getValorOpcion('numeracion_orden_venta', $empresa);
            if($numero_orden_venta == null){
                $opcion = new Opcion();
                $opcion->empresa_id = $empresa->id;
                $opcion->nombre = 'numeracion_orden_venta';
                $opcion->presentacion  = 'Numeración Orden de Venta';
                $opcion->valor = '00000001';
                $opcion->save();

                $numero_orden_venta = $opcion->valor;
            }

            $opcion = Opcion::where('empresa_id', $empresa->id)
                ->where('nombre', 'LIKE', 'numeracion_orden_venta')
                ->first();
            $opcion->valor = str_pad($opcion->valor + 1, 8, '0', STR_PAD_LEFT);
            $opcion->save();

            if(Auth::user()){
                $cliente = Auth::user()->cliente;
            } else {
                $buscarCliente = Cliente::where('razon_social','=',$requestData['razon_social'])->first();
                if($buscarCliente){ // Si la empresa tiene un cliente con este nombre, lo asigno, si no, lo creo
                    $cliente = $buscarCliente;
                } else {
                    $cliente = New Cliente($requestData);
                    $cliente->empresa_id = $this->data['empresa']->id;
                    $cliente->condicion_iva_id = 5 ; //Condicion Iva Consumidor Final
                    $cliente->dni = '27000000006';
                    $cliente->save();
                }
            }

            $ordenVenta = New OrdenVenta();
            $ordenVenta->estado = Estado::ACTIVO;
            $ordenVenta->empresa_id = $empresa->id;
            $ordenVenta->cliente_id = $cliente->id;
            $ordenVenta->forma_pago_id = $requestData['pago'];
            $ordenVenta->moneda_id = $empresa->ecommerce->moneda->id;
            $ordenVenta->cotizacion = $empresa->ecommerce->moneda->cotizacion;
            $ordenVenta->lista_precio_id = $empresa->ecommerce->listaPrecio->id;
            $ordenVenta->mensaje = $requestData['nota'];
            $ordenVenta->numero = $numero_orden_venta;
            $ordenVenta->importe_neto = 0;
            $ordenVenta->importe_iva = 0;
            $ordenVenta->total = 0;
            $ordenVenta->save();

            $porcentajes_iva = PorcentajeIva::$valores;
            if(Cart::content()) {
                foreach (Cart::content() as $detalle) {
                    $nuevoDetalle = New DetalleOrdenVenta();
                    $nuevoDetalle->orden_venta_id = $ordenVenta->id;
                    $nuevoDetalle->articulo_id = $detalle->model->id;
                    $nuevoDetalle->precio_neto = $detalle->model->getPrecio(null,false) * $detalle->qty;
                    $nuevoDetalle->precio_iva = $nuevoDetalle->precio_neto * ($porcentajes_iva[$detalle->model->porcentaje_iva] / 100);
                    $nuevoDetalle->porcentaje_iva = $detalle->model->porcentaje_iva;
                    $nuevoDetalle->cantidad = $detalle->qty;
                    $nuevoDetalle->estado = 1;
                    $nuevoDetalle->save();

                    $ordenVenta->importe_neto += $nuevoDetalle->precio_neto;
                    $ordenVenta->importe_iva += $nuevoDetalle->precio_iva;
                    $ordenVenta->total += $nuevoDetalle->precio_neto + $nuevoDetalle->precio_iva;
                }
            }

            $ordenVenta->save();

        });

        return redirect('ecommerce');

    }

}
