<?php

namespace App\Http\Controllers\Ecommerce;

use App\Http\Controllers\Controller;
use App\Models\Frontend\Articulos\Rubro;
use App\Models\Frontend\ECommerce\Pagina;
use App\Models\Backend\Empresas\Empresa;
use App\Repositories\Backend\Access\Role\RoleRepository;
use App\Repositories\Backend\Access\User\UserRepository;
use Illuminate\Support\Facades\Auth;

class BaseController extends Controller{

    public $data = [];
    protected $users;
    protected $roles;

    public function __construct(UserRepository $users, RoleRepository $roles){
        $this->middleware(function ($request, $next){
            $empresa_id = session('empresa_id');
            if($empresa_id){
                $empresa = Empresa::find($empresa_id);
            } else {
                $empresa = Empresa::find(69);
            }
            $this->data['empresa'] = $empresa;
            $this->data['estilo'] = $empresa->ecommerce->estilo;
            $this->data['rubros'] = Rubro::where('empresa_id', $empresa->id)->get();
            $this->data['paginas'] = Pagina::where('empresa_id', $empresa->id)->get();

            return $next($request);
        });
        $this->users = $users;
        $this->roles = $roles;
    }

}
