<?php

namespace App\Http\Controllers\Ecommerce;

use App\Models\Frontend\Configuraciones\Estado;
use App\Models\Frontend\Configuraciones\Provincia;
use App\Models\Frontend\Personas\Cliente;
use App\Models\Frontend\Personas\Domicilio;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends BaseController {

    use AuthenticatesUsers;

    public function login(){
        if(Auth::user()){
            return redirect('ecommerce/dashboard');
        } else {
            return view('ecommerce.login',$this->data );
        }
    }

    public function registro(Request $request){
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:8|confirmed',
            'password_confirmation' => 'required|min:8'
        ]);

        DB::transaction(function() use ($request) {
            $requestData = $request->all();

            $user = $this->nuevoUsuario($requestData['name'], $requestData['email'], $requestData['password'], $requestData['password_confirmation']);

            $cliente = New Cliente($requestData);
            $cliente->empresa_id = $this->data['empresa']->id;
            $cliente->razon_social = $requestData['name'];
            $cliente->condicion_iva_id = 5 ; //Condicion Iva Consumidor Final
            $cliente->dni = '27000000006';
            $cliente->user_id = $user->id;
            $cliente->save();
        });

        return redirect('/ecommerce');
    }

    public function dashboard(){
        $this->data['provincias'] = Provincia::get();

        return view('ecommerce.user.dashboard',$this->data );
    }

    public function actualizarDatos(Request $request){
        $this->validate($request, [
            'email'=>'required|email',
            'razon_social' => 'required'
        ]);


        DB::transaction(function () use ($request) {
            $requestData = $request->all();
            $user = Auth::user();

            $user->cliente->razon_social = $requestData['razon_social'];
            $user->cliente->email = $requestData['email'];
            $user->cliente->dni = $requestData['dni'];
            $user->cliente->telefono = $requestData['telefono'];
            $user->cliente->save();

            $user->email = $requestData['email'];;
            $user->save();
        });

        return redirect('ecommerce/dashboard');
    }

    public function actualizarDireccion(Request $request){
        $this->validate($request, [
            'direccion'=>'required',
            'localidad' => 'required',
            'codigo_postal' => 'required',
            'provincia_id' => 'required'
        ]);

        $requestData = $request->all();
        $user = Auth::user();

        if(isset($user->cliente->domicilio)){
            $user->cliente->domicilio->update($requestData);
        }else{
            $domicilio = New Domicilio($requestData);
            $domicilio->save();
            $user->cliente->domicilio_id = $domicilio->id;
            $user->cliente->save();
        }

        return redirect('ecommerce/dashboard');
    }

    public function logout(Request $request){
        if (app('session')->has(config('access.socialite_session_name'))) {
            app('session')->forget(config('access.socialite_session_name'));
        }

        $this->guard()->logout();
        $request->session()->flush();
        $request->session()->regenerate();

        return redirect('/ecommerce');
    }

    public function nuevoUsuario($nombre, $email, $password, $password_confirmation){
        $assignees_roles[6] = '6'; // Rol de cliente

        $parametrosDatos = [
            'name' => $nombre,
            'email' => $email,
            'password' => $password,
            'password_confirmation' => $password_confirmation,
            'status' => Estado::ACTIVO,
            'empresa_id' => $this->data['empresa']->id,
            'confirmed' => TRUE
        ];

        $parametrosRoles = [
            'assignees_roles' => $assignees_roles
        ];

        $nuevoUsuario = $this->users->create(['data' => $parametrosDatos, 'roles' => $parametrosRoles]);
        access()->login($nuevoUsuario);
        return $nuevoUsuario;
    }




 }

