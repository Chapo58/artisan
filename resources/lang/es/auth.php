<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'        => 'Usuario o Contraseña Incorrecto.',
    'general_error' => 'No tienes suficientes permisos..',
    'socialite'     => [
        'unacceptable' => ':provider no es un tipo de autenticación válida.',
    ],
    'throttle' => 'Demasiados intentos de inicio de sesión. Vuelva a intentarlo en :seconds segundos.',
    'unknown'  => 'Se ha producido un error desconocido.',
];
