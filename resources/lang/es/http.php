<?php

return [

    /*
    |--------------------------------------------------------------------------
    | HTTP Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the views/errors files.
    |
    */

    '403' => [
        'title'       => 'Error de permisos.',
        'description' => 'No posees los permisos necesarios para estar aqui.',
    ],

    '404' => [
        'title'       => 'La Página que intento acceder no ha sido encontrada.',
        'description' => 'Parece ser que la página que buscas no existe.',
    ],

    '503' => [
        'title'       => 'Servicio no disponible.',
        'description' => 'Volveremos en breve.',
    ],

];
