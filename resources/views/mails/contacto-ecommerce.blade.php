<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<img src="https://artisan.com.ar/landing/img/logo2.png" style="max-width: 500px;"/>

<h2>Nuevo contacto desde tu E-Commerce de Artisan</h2>

<div>
    <p>
        {{$request->nombre}} ha enviado el siguiente mensaje:
    </p>

    <p>
        {{$request->mensaje}}
    </p>

    <p>Sus datos de contacto son:</p>

    <ul>
        <li><strong>Email:</strong> {{$request->email}}</li>
    </ul>
</div>
</body>
</html>
