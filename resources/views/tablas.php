@section('after-styles')
    {{ Html::style("css/frontend/plugins/datatables/responsive.dataTables.min.css") }}
@endsection

<table id="venta" class="table selectable table-bordered table-striped color-table  form-material" style="width: 100%">
                     <thead>
                       <tr style="background:#99abb4;color:#fff;">
                            <th></th>



                             <td>&nbsp;&nbsp;</td>



    {{ Html::script("js/frontend/plugins/datatables/jquery.dataTables.min.js") }}
    {{ Html::script("js/frontend/plugins/datatables/dataTables.responsive.min.js") }}
    {{ Html::script("js/frontend/plugins/datatables/dataTables.select.min.js") }}



      $(document).ready(function() {
            $('#venta').DataTable({
                "aaSorting": [1,'asc'],
                "info":     false,
                "searching":   false,
                responsive: {
                    details: {
                        type: 'column',
                        target: 'tr'
                    }
                },
                "columnDefs": [{
                    targets: 0,
                    className: 'control',
                    orderable: false,
                    searchable: false,
                    defaultContent: '&nbsp;&nbsp;'
                },
                { responsivePriority: 1, targets: -1 },
                { responsivePriority: 1, targets: -2 },
                { responsivePriority: 1, targets: 1 }],
            });
        } );


        "info":     false,
        "searching":   false, // botones de busqueda
        "ordering":false,   //ordenamiento