<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>{{ trans('http.403.title') }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{{ asset('favicon.png') }}}">

    <!-- Bootstrap Core CSS -->
    {{ Html::style("css/frontend/plugins/bootstrap/css/bootstrap.min.css") }}

    {{ Html::style("css/frontend/vertical_style.css") }}

    <link rel="stylesheet" type="text/css" href="{{ asset('css/frontend/colors/blue.css') }}" id="theme">

</head>
<body class="fix-header card-no-border">
    <section id="wrapper" class="error-page">
        <div class="error-box" style="background: url({{url('images/frontend/background/error-bg.jpg')}}) no-repeat center center #fff;
  width: 100%;">
            <div class="error-body text-center">
                <h1>403</h1>
                <h3 class="text-uppercase">{{ trans('http.403.title') }}</h3>
                <p class="text-muted m-t-30 m-b-30">{{ trans('http.403.description') }}</p>
                <a href="/login" class="btn btn-info btn-rounded waves-effect waves-light m-b-40">Volver a Inicio</a> </div>
            <footer class="footer text-center">© {{date('Y')}} <a href="http://ciatt.com.ar" target="_blank">Ciatt Software</a>.</footer>
        </div>
    </section>

    {{ Html::script("js/frontend/plugins/jquery/jquery.min.js") }}
    <!-- Bootstrap tether Core JavaScript -->
    {{ Html::script("js/frontend/plugins/bootstrap/popper.min.js") }}
    {{ Html::script("js/frontend/plugins/bootstrap/bootstrap.min.js") }}
    <!--Wave Effects -->
    {{ Html::script("js/frontend/waves.js") }}
</body>
</html>
<!-- IE needs 512+ bytes: http://blogs.msdn.com/b/ieinternals/archive/2010/08/19/http-error-pages-in-internet-explorer.aspx -->