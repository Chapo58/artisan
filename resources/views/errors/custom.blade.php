<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Artisan Gestión</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{{ asset('favicon.png') }}}">

    {{ Html::style("css/frontend/error_style.css") }}
</head>
<body>
<div id="level">
    <div id="content">
        <div id="gears">
            <div id="gears-static"></div>
            <div id="gear-system-1">
                <div id="gear15"></div>
                <div id="gear14"></div>
                <div id="gear13"></div>
            </div>
            <div id="gear-system-2">
                <div id="gear10"></div>
                <div id="gear3"></div>
            </div>
            <div id="gear-system-3">
                <div id="gear9"></div>
                <div id="gear7"></div>
            </div>
            <div id="gear-system-4">
                <div id="gear6"></div>
                <div id="gear4"></div>
            </div>
            <div id="gear-system-5">
                <div id="gear12"></div>
                <div id="gear11"></div>
                <div id="gear8"></div>
            </div>
            <div id="gear1"></div>
            <div id="gear-system-6">
                <div id="gear5"></div>
                <div id="gear2"></div>
            </div>
            <div id="chain-circle"></div>
            <div id="chain"></div>
            <div id="weight"></div>
        </div>
        <div id="title">
            <h1>Hemos tenido un incidente...</h1>
            <p>¡Lo sentimos! Por favor comunícate con nosotros para que lo solucionemos cuanto antes</p>
            <p>{{$exception->getMessage()}}</p>
            <a href="/dashboard" class="btn btn-info btn-rounded waves-effect waves-light m-b-40">Volver a Inicio</a> </div>
        </div>
    </div>
</div>
</body>
</html>
