@extends('frontend.layouts.app')

@section ('title', trans('labels.frontend.grupo_titulo')." - ".trans('buttons.general.crud.show'))

@section('content')
    <div class="row page-titles">
        <div class="col-md-8 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('navs.frontend.user.administration') }} {{ trans('labels.frontend.grupo_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item"><a href="{{url('/articulos/grupo-articulo')}}">{{ trans('labels.frontend.grupo_titulo') }}</a></li>
                <li class="breadcrumb-item">{{ trans('buttons.general.crud.show') }} {{ trans('labels.frontend.grupo') }}</li>
                <li class="breadcrumb-item active">{{ $grupo }}</li>
            </ol>
        </div>
        <div class="col-md-4">
            <div class="pull-right">
                <a href="{{ url('/articulos/grupo-articulo') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i> {{ trans('buttons.general.crud.back') }}
                </a>
                <a href="{{ url('/articulos/grupo-articulo/' . $grupo->id . '/edit') }}" title="{{ trans('buttons.general.crud.edit') }}" class="btn btn-info">
                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i> {{ trans('buttons.general.crud.edit') }}
                </a>
                <a href="javascript:void(0)" title="{{trans("buttons.general.crud.delete")." ".trans("labels.frontend.grupo")}}" class="btn btn-danger delete">
                    <i class="fa fa-trash" aria-hidden="true"></i> {{ trans("buttons.general.crud.delete") }}
                </a>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
            <h4 class="font-medium m-t-30">{{ $grupo }}</h4>
            <hr>
            <div class="row">
                <div class="col-md-3">
                    <img src="{{ isset($grupo->imagen_url) ? $grupo->imagen_url : '/articulos/no-image.jpg' }}" alt="{{ isset($grupo->nombre_grupo) ? $grupo->nombre_grupo : '' }}" width="300" style="border: 2px solid lightslategrey;">
                </div>
                <div class="col-md-9">
                    <div class="row">
                        <div class="col-md-3 font-weight-bold">Codigo Interno</div>
                        <div class="col-md-3">{{ $grupo->codigo }}</div>

                        <div class="col-md-3 font-weight-bold">Código de barras</div>
                        <div class="col-md-3">{{ $grupo->codigo_barra }}</div>
                    </div><hr>
                    <div class="row">
                        <div class="col-md-3 font-weight-bold">{{ trans('validation.attributes.nombre') }}</div>
                        <div class="col-md-3">{{ $grupo->nombre }}</div>

                        <div class="col-md-3 font-weight-bold">{{ trans('validation.attributes.marca_id') }}</div>
                        <div class="col-md-3">
                            @if(isset($grupo->marca))
                                <a href="{{url('articulos/marca/'.$grupo->marca->id)}}">{{$grupo->marca}}</a>
                            @else
                                Ninguna
                            @endif
                        </div>
                    </div><hr>
                    <div class="row">
                        <div class="col-md-3 font-weight-bold">Modelo</div>
                        <div class="col-md-3">{{ $grupo->modelo }}</div>

                        <div class="col-md-3 font-weight-bold">Modelo Secundario</div>
                        <div class="col-md-3">{{ $grupo->modelo_secundario }}</div>
                    </div><hr>
                    <div class="row">
                        <div class="col-md-3 font-weight-bold">{{ trans('validation.attributes.unidad_id') }}</div>
                        <div class="col-md-3">{{ $grupo->unidad }}</div>

                        <div class="col-md-3 font-weight-bold">Unidades por bulto</div>
                        <div class="col-md-3">{{ number_format($grupo->cantidad,0) }} {{ $grupo->unidad->presentacion }}</div>
                    </div><hr><div class="row">
                        <div class="col-md-3 font-weight-bold">{{ trans('validation.attributes.rubro_id') }}</div>
                        <div class="col-md-3">
                            @if(isset($grupo->rubro))
                                <a href="{{url('articulos/rubro/'.$grupo->rubro->id)}}">{{$grupo->rubro}}</a>
                            @else
                                Ninguno
                            @endif
                        </div>

                        <div class="col-md-3 font-weight-bold">{{ trans('validation.attributes.subrubro_id') }}</div>
                        <div class="col-md-3">
                            @if(isset($grupo->subrubro))
                                <a href="{{url('articulos/subrubro/'.$grupo->subrubro->id)}}">{{$grupo->subrubro}}</a>
                            @else
                                Ninguno
                            @endif
                        </div>
                    </div><hr>
                    <div class="row">
                        <div class="col-md-3 font-weight-bold">Porcentaje IVA</div>
                        <div class="col-md-3">{{ $grupo->porcentajeIva() }}</div>

                        <div class="col-md-3 font-weight-bold">Actividad AFIP</div>
                        <div class="col-md-3">
                            @if(isset($grupo->actividadAfip))
                                <a href="{{url('configuraciones/actividad-afip/'.$grupo->actividadAfip->id)}}">{{$grupo->actividadAfip}}</a>
                            @endif
                        </div>
                    </div><hr>
                    <div class="row">
                        <div class="col-md-3 font-weight-bold">Moneda</div>
                        <div class="col-md-3">
                            @if(isset($grupo->moneda))
                                <a href="{{url('configuraciones/moneda/'.$grupo->moneda->id)}}">{{ $grupo->moneda }}</a>
                            @endif
                        </div>

                        @roles(['Usuario Admin', 'Usuario Gerente'])
                        <div class="col-md-3 font-weight-bold">Último Costo</div>
                        <div class="col-md-3">
                            @if(isset($grupo->ultimo_costo))
                                @if(isset($grupo->moneda))
                                    {{ $grupo->moneda->signo }} {{ number_format($grupo->ultimo_costo, 2, ',', '') }}
                                @else
                                    $ {{ number_format($grupo->ultimo_costo, 2, ',', '') }}
                                @endif
                            @endif
                        </div>
                        @endauth
                    </div><hr>
                    <div class="row">
                        <div class="col-md-3 font-weight-bold">Creado el</div>
                        <div class="col-md-3">{{ $grupo->created_at->format('d/m/Y H:m') }}</div>

                        <div class="col-md-3 font-weight-bold">Ultima Modificación</div>
                        <div class="col-md-3">{{ $grupo->updated_at->format('d/m/Y H:m') }}</div>
                    </div>
                </div>
            </div>
            <br>
            <div class="table-responsive">
                <table class="table table-bordered table-striped col-md-6">
                    <thead class="thin-border-bottom">
                    <tr>
                        <th>
                            Nombre Artículo
                        </th>
                        <th>
                            Modelo
                        </th>
                        <th>
                            Modelo Secundario
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($grupo->articulos as $articulo)
                        <tr>
                            <td>{{ $articulo->nombre }}</td>
                            <td>{{ $articulo->modelo }}</td>
                            <td>{{ $articulo->modelo_secundario }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection

@section('after-scripts')

<script>
    $("body").on("click", ".delete", function () {
        swal({
            title: "Eliminar Grupo",
            text: "¿Realmente desea eliminar este registro?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#ff8726',
            confirmButtonText: "Eliminar"
        }).then((result) => {
            if (result.value) {
            $.ajax({
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ url('/articulos/grupo-articulo/'.$grupo->id) }}',
                data: {"_method" : 'DELETE'},
                success: function (msg) {
                    swal("Eliminado!", "El registro ha sido correctamente eliminado.", "success").then((result) => { window.location.href = '{{url('/articulos/grupo-articulo')}}'; });
                }
            });
        }
    });
    });
</script>

@endsection