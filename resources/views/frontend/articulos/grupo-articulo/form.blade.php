@section('after-styles')
    {{ Html::style("css/frontend/plugins/jquery/jquery-ui.min.css") }}
@endsection

<div class="card">
    <div class="card-header">
        <div class="card-actions">
            <a class="mytooltip" data-action="collapse"><i class="ti-minus"></i><span class="tooltip-content3">Minimizar Ventana</span></a>
            <a class="btn-minimize mytooltip" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
        </div>
        <h4 class="card-title m-b-0">Información del Grupo</h4>
    </div>
    <div class="card-body collapse show b-t">
        <div class="row">
            <div class="form-group col-md-5">
                <div class="input-group">
                    <span class="input-group-addon">{!! Form::label('nombre_grupo', 'Nombre Grupo') !!}</span>
                    {!! Form::text('nombre_grupo', null, ['class' => 'form-control', 'required' => 'required']) !!}
                </div>
                {!! $errors->first('nombre_grupo', '<p class="text-danger">:message</p>') !!}
            </div>
            <div class="form-group col-md-7">
                <div class="row">
                    <div class="col-md-3">
                        {!! Form::label('imagen_url', 'Imagen Actual') !!}
                        <img src="{{ isset($grupo->imagen_url) ? $grupo->imagen_url : '/articulos/no-image.jpg' }}" alt="{{ isset($grupo->nombre) ? $grupo->nombre : '' }}" width="150" style="border: 2px solid lightslategrey;">
                    </div>
                    <div class="form-group col-md-8">
                        <label>Seleccionar Imagen Principal</label>
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="input-group image-preview">
                                    <input type="text" class="form-control image-preview-filename" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
                                    <span class="input-group-btn">
                                        <!-- image-preview-clear button -->
                                        <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                                          <span class="glyphicon glyphicon-remove"></span> Limpiar
                                        </button>
                                        <!-- image-preview-input -->
                                        <div class="btn btn-default image-preview-input">
                                          <span class="glyphicon glyphicon-folder-open"></span>
                                          <span class="image-preview-input-title">Buscar</span>
                                          <input type="file" name="image" accept=".png, .jpg, .jpeg" required>
                                        </div>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-header">
        <div class="card-actions">
            <a class="mytooltip" data-action="collapse"><i class="ti-minus"></i><span class="tooltip-content3">Minimizar Ventana</span></a>
            <a class="btn-minimize mytooltip" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
        </div>
        <h4 class="card-title m-b-0">Datos Compartidos</h4>
        <h6 class="card-subtitle">(*) Requeridos para crear nuevos Artículos</h6>
    </div>
    <div class="card-body collapse show b-t">
        <div class="row form-group">
            <div class="input-group col-md-6">
                <span class="input-group-addon">{!! Form::label('codigo', 'Código Interno:') !!}</span>
                {!! Form::number('codigo', isset($grupo->codigo) ? $grupo->codigo : '', ['class' => 'form-control']) !!}
            </div>
            <div class="input-group col-md-6">
                <span class="input-group-addon">{!! Form::label('codigo_barra', 'Código de Barra:') !!}</span>
                {!! Form::number('codigo_barra', isset($grupo->codigo_barra) ? $grupo->codigo_barra : '', ['class' => 'form-control']) !!}
            </div>
        </div>
        <div class="row form-group">
            <div class="input-group col-md-6">
                <span class="input-group-addon">{!! Form::label('unidad_id', 'Unidad de medida*') !!}</span>
                {!! Form::select('unidad_id', $unidades, isset($grupo->unidad) ? $grupo->unidad->id : '', ['class' => 'form-control campo-nuevo-articulo']) !!}
            </div>
            <div class="input-group col-md-6">
                <span class="input-group-addon">{!! Form::label('cantidad', 'Uni. por bulto*') !!}</span>
                {!! Form::number('cantidad', isset($grupo->cantidad) ? number_format($grupo->cantidad, 0, '.', '') : '', ['class' => 'form-control campo-nuevo-articulo']) !!}
            </div>
        </div>
        <div class="row form-group">
            <div class="input-group col-md-9">
                <span class="input-group-addon">{!! Form::label('nombre', 'Nombre*') !!}</span>
                {!! Form::text('nombre', null, ['class' => 'form-control campo-nuevo-articulo']) !!}
            </div>
        </div>
        <div class="row form-group">
            <div class="input-group col-md-4">
                <span class="input-group-addon">{!! Form::label('rubro_id', 'Rubro*') !!}</span>
                {!! Form::select('rubro_id', $rubros, isset($grupo->rubro) ? $grupo->rubro->id : '', ['class' => 'form-control campo-nuevo-articulo']) !!}
            </div>
            <div class="input-group col-md-4">
                <span class="input-group-addon">{!! Form::label('subrubro_id', 'Subrubro*') !!}</span>
                {!! Form::select('subrubro_id', $subrubros, isset($grupo->subrubro) ? $grupo->subrubro->id : '', ['class' => 'form-control campo-nuevo-articulo']) !!}
            </div>
            <div class="input-group col-md-4">
                <span class="input-group-addon">{!! Form::label('marca_id', 'Marca*') !!}</span>
                {!! Form::select('marca_id', $marcas, isset($grupo->marca) ? $grupo->marca->id : '', ['class' => 'form-control campo-nuevo-articulo']) !!}
            </div>
        </div>
        <div class="row form-group">
            <div class="input-group col-md-4">
                <span class="input-group-addon">{!! Form::label('porcentaje_iva', 'Porcentaje IVA*') !!}</span>
                {!! Form::select('porcentaje_iva', $porcentajesIva, isset($grupo->porcentaje_iva) ? $grupo->porcentaje_iva : '', ['id' => 'porcentaje_iva', 'class' => 'form-control campo-nuevo-articulo']) !!}
            </div>
            <div class="input-group col-md-4">
                <span class="input-group-addon">{!! Form::label('ultimo_costo', 'Costo Neto*') !!}</span>
                {!! Form::number('ultimo_costo', isset($grupo->ultimo_costo) ? $grupo->ultimo_costo : '', ['class' => 'form-control campo-nuevo-articulo', 'step' => '0.01']) !!}
            </div>
            <div class="input-group col-md-4">
                <span class="input-group-addon">{!! Form::label('moneda_id', 'Moneda*') !!}</span>
                {!! Form::select('moneda_id', $monedas, isset($grupo->moneda) ? $grupo->moneda->id : '', ['class' => 'form-control campo-nuevo-articulo']) !!}
            </div>
        </div>
        <div class="row form-group">
            <div class="input-group col-md-4">
                <span class="input-group-addon">{!! Form::label('porcentaje_impuesto', 'Impuesto') !!}</span>
                {!! Form::number('porcentaje_impuesto', isset($grupo->ultimo_costo) ? ($grupo->impuesto*100)/$grupo->ultimo_costo : '', ['id' => 'porcentaje_impuesto','class' => 'form-control', 'step' => '0.01']) !!}
            </div>
            <div class="input-group col-md-4">
                <span class="input-group-addon">{!! Form::label('remarcado', 'Remarcar*') !!}</span>
                {!! Form::number('remarcado', (isset($grupo) && $grupo->porcentajeRemarcado() != 0) ? $grupo->porcentajeRemarcado() : '', ['class' => 'form-control campo-nuevo-articulo', 'step' => '0.01', 'min' =>'0.0']) !!}
            </div>
            <div>
                <div class="col-md-4">
                    <button class="btn btn-info" id="agregar-nuevo-articulo">
                        <i class="fa fa-plus" aria-hidden="true"></i> Nuevo Artículo
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-header">
        <div class="card-actions">
            <a class="mytooltip" data-action="collapse"><i class="ti-minus"></i><span class="tooltip-content3">Minimizar Ventana</span></a>
            <a class="btn-minimize mytooltip" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
        </div>
        <h4 class="card-title m-b-0">Articulos que componen el grupo</h4>
    </div>
    <div class="card-body collapse show b-t">
        <div class="table-responsive">
        <table id="tabla-articulos" class="table color-table info-table form-material">
            <thead>
            <tr>
                <th>Artículo</th>
                <th>Modelo</th>
                <th>Modelo Secundario</th>
                <th>Datos</th>
                <th>Acciones</th>
            </tr>
            </thead>
            <tbody>
            @if(isset($grupo->articulos))
                @foreach($grupo->articulos as $articulo)
                    <tr>
                        <td>
                            {!! Form::text('articulos['.$articulo->id.'][articulo]', '('.$articulo->codigo.') '.$articulo->nombre, ['class' => 'form-control', 'readonly']) !!}
                            <input class="articulo-id" type="hidden" name="articulos[{{ $articulo->id }}][articulo_id]" value="{{ $articulo->id }}">
                        </td>
                        <td>
                            {!! Form::text('articulos['.$articulo->id.'][modelo]', $articulo->modelo, ['class' => 'form-control modelo']) !!}
                        </td>
                        <td>
                            {!! Form::text('articulos['.$articulo->id.'][modelo_secundario]', $articulo->modelo_secundario, ['class' => 'form-control modelo-secundario']) !!}
                        </td>
                        <td class="datos">
                            {!! "<b>Unidad:</b> ".$articulo->unidad." <b>Uni. por Bulto:</b> ".number_format($articulo->cantidad, 0)." <b>Rubro:</b> ".$articulo->rubro." <b>Subrubro:</b> ".$articulo->subrubro." <b>Marca:</b> ".$articulo->marca." <b>IVA:</b> ".$articulo->porcentajeIva()."% <b>Costo:</b> $".$articulo->ultimo_costo." <b>Moneda:</b> ".$articulo->moneda." <b>Impuesto:</b> $".$articulo->impuesto." <b>Remarcar:</b> ".$articulo->porcentajeRemarcado()."%" !!}
                        </td>
                        <td align="center">
                            <button type="button" class="btn btn-danger btn-circle borrar-articulo" name="articulos[{{ $articulo->id }}][eliminar]">
                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </button>
                            <input type="hidden" class="estado" name="articulos[{{ $articulo->id }}][estado]" value="1">
                        </td>
                    </tr>
                @endforeach
            @endif
            </tbody>
            <tfoot>
            <tr>
                <td>{!! Form::text('', '', ['class' => 'form-control agregar-articulo']) !!}</td>
                <td>{!! Form::text('', '', ['class' => 'form-control agregar-articulo']) !!}</td>
                <td>{!! Form::text('', '', ['class' => 'form-control agregar-articulo']) !!}</td>
                <td>...</td>
                <td align="center">
                    <button type="button" class="btn btn-danger btn-circle btn-sm disabled">
                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                    </button>
                </td>
            </tr>
            </tfoot>
        </table>
        </div>
    </div>
</div>

<div class="card">
    <div class="card-body">
        <div class="form-group">
                {{ Form::submit(isset($submitButtonText) ? $submitButtonText : trans('buttons.general.save'), ['class' => 'btn btn-themecolor btn-lg pull-right']) }}
        </div>
    </div>
</div>

@section('after-scripts')
    {{ Html::script("js/frontend/plugins/jquery/jquery-ui.min.js") }}
    <script>
        $( document ).ready(function() {
            permitirNuevoArticulo();

            $(document).on('click', '.borrar-articulo', function () {
                var fila = $(this).parent().parent();

                fila.hide('slow');
                fila.find('td input.estado').val(0);
                fila.find('input').removeAttr('required');
            });
        });

        function permitirNuevoArticulo() {
            $('#agregar-nuevo-articulo').removeClass('disabled')
            if($('#unidad_id').val() == '' ||
                $('#cantidad').val() == '' ||
                $('#nombre').val() == '' ||
                $('#rubro_id option:selected').val() == '' ||
                $('#subrubro_id option:selected').val() == '' ||
                $('#marca_id option:selected').val() == '' ||
                $('#porcentaje_iva option:selected').val() == '' ||
                $('#ultimo_costo').val() == '' ||
                $('#moneda_id option:selected').val() == '' ||
                $('#remarcado').val() == '') {

                $('#agregar-nuevo-articulo').addClass('disabled')
                return false;
            }else{
                return true;
            }
        }

        $('#agregar-nuevo-articulo').click(function(){
            if(permitirNuevoArticulo() == true){
                agregarNuevoArticulo();
            }
        });

        $('.campo-nuevo-articulo').change(function(){
            permitirNuevoArticulo();
        });

        $('.agregar-articulo').click(function(){
            agregarArticulo();
        });

        function agregarNuevoArticulo() {
            var numero_orden = $('table#tabla-articulos tbody tr').length+ 1;
            var nombre = $('#nombre').val();
            $("#tabla-articulos").append('<tr>' +
                '<td>' +
                    '<input class="form-control" readonly="" name="articulos['+(numero_orden*-1)+'][articulo]" value="'+nombre+'" type="text">' +
                    '<input class="articulo-id" name="articulos['+(numero_orden*-1)+'][articulo_id]" value="'+(numero_orden*-1)+'" type="hidden">' +
                '</td>' +
                '<td>' +
                    '<input class="form-control modelo" name="articulos['+(numero_orden*-1)+'][modelo]" value="" type="text">' +
                '</td>' +
                '<td>' +
                    '<input class="form-control modelo-secundario" name="articulos['+(numero_orden*-1)+'][modelo_secundario]" value="" type="text">' +
                '</td>' +
                '<td class="datos">' +
                    '<b>Nuevo Artículo...</b> ' +
                '</td>' +
                '<td align="center">' +
                    '<button type="button" class="btn btn-danger btn-circle borrar-articulo" name="articulos['+(numero_orden*-1)+'][eliminar]">' +
                        '<i class="fa fa-trash-o" aria-hidden="true"></i>' +
                    '</button>' +
                    '<input type="hidden" class="estado" name="articulos['+(numero_orden*-1)+'][estado]" value="1">' +
                '</td>' +
            '</tr>');
            $("input[name='articulos["+(numero_orden*-1)+"][articulo]']").focus();
        }

        function agregarArticulo() {
            var numero_orden = $('table#tabla-articulos tbody tr').length+ 1;
            var nombre = $('#nombre').val();
            $("#tabla-articulos").append('<tr>' +
                '<td>' +
                    '<input class="form-control buscar-articulo" name="articulos['+(numero_orden*-1)+'][articulo]" placeholder="Buscar..." type="text" required="required">' +
                    '<input class="articulo-id" name="articulos['+(numero_orden*-1)+'][articulo_id]" type="hidden" value="">' +
                '</td>' +
                '<td>' +
                    '<input class="form-control modelo" name="articulos['+(numero_orden*-1)+'][modelo]" value="" type="text">' +
                '</td>' +
                '<td>' +
                    '<input class="form-control modelo-secundario" name="articulos['+(numero_orden*-1)+'][modelo_secundario]" value="" type="text">' +
                '</td>' +
                '<td class="datos"></td>' +
                '<td align="center">' +
                    '<button type="button" class="btn btn-danger btn-circle btn-sm borrar-articulo" name="articulos['+(numero_orden*-1)+'][eliminar]">' +
                        '<i class="fa fa-trash-o" aria-hidden="true"></i>' +
                    '</button>' +
                    '<input type="hidden" class="estado" name="articulos['+(numero_orden*-1)+'][estado]" value="1">' +
                '</td>' +
            '</tr>');
            $("input[name='articulos["+(numero_orden*-1)+"][articulo]']").focus();
        }

        $(document).on("keydown.autocomplete", ".buscar-articulo", function (e) {
            if (e.keyCode == 13) {
                e.preventDefault();
            }else{
                $(this).autocomplete({
                    source: '{{ url('/articulos/articulo/obtenerArticulo') }}',
                    minLength: 2,
                    autoFocus: true,
                    select: function (e, ui) {
                        var fila = $(this).parent().parent();

                        fila.find('input.articulo-id').val(ui.item.id);
                        fila.find('input.buscar-articulo').attr('readonly', true);
                        fila.find('input.modelo').val(ui.item.modelo);
                        fila.find('input.modelo-secundario').val(ui.item.modelo_secundario);
                        fila.find('.datos').append(
                            ' <b>Unidad:</b> '+ ui.item.unidad +
                            ' <b>Uni. por Bulto:</b> '+ ui.item.cantidad +
                            ' <b>Rubro:</b> '+ ui.item.rubro +
                            ' <b>Subrubro:</b> '+ ui.item.subrubro +
                            ' <b>Marca:</b> '+ ui.item.marca +
                            ' <b>IVA:</b> '+ ui.item.porcentaje_iva + '%' +
                            ' <b>Costo:</b> $'+ ui.item.costo +
                            ' <b>Moneda:</b> '+ ui.item.moneda +
                            ' <b>Impuesto:</b> $'+ ui.item.impuesto +
                            ' <b>Remarcar:</b> '+ ui.item.remarcar +'%');
                    }
                });
            }
        });



        $(document).on('change', 'input#ultimo_costo', function () {
            $('input#ultimo_costo').val(this.value);
            cambiarImpuesto();
            cambiarPrecios();
        });

        $(document).on('change', 'input#impuesto', function () {
            $('input#impuesto').val(this.value);
            cambiarPorcentajeImpuesto();
            cambiarPrecios();
        });

        $(document).on('change', 'input#porcentaje_impuesto', function () {
            $('input#porcentaje_impuesto').val(this.value);
            cambiarImpuesto();
            cambiarPrecios();
        });

        $(document).on('change', 'input#utilidad', function () {
            $('input#utilidad').val(this.value);
            cambiarRemarcado();
            cambiarPrecios();
        });

        $(document).on('change', 'input#remarcado', function () {
            $('input#remarcado').val(this.value);
            cambiarUtilidad();
        });

        function cambiarUtilidad() {
            var costo = parseFloat($('input#ultimo_costo').val());
            var remarcar = parseFloat($('input#remarcado').val());

            var precio = costo + costo * (remarcar/100);
            var utilidad = (((precio - costo)/precio)*100).toFixed(2);

            $('input#utilidad').val(utilidad);
            cambiarPrecios();
        }

        function cambiarRemarcado() {
            var costo = parseFloat($('input#ultimo_costo').val());
            var utilidad = parseFloat($('input#utilidad').val());

            var precio = (costo/(1-(utilidad/100))).toFixed(2);
            var remarcado = (((precio - costo)*100)/costo).toFixed(2);

            $('input#remarcado').val(remarcado);
            cambiarPrecios();
        }

        function cambiarPrecios() {
            var utilidad = parseFloat($('input#utilidad').val());
            var costo = parseFloat($('input#ultimo_costo').val());

            var precio = (costo/(1-(utilidad/100))).toFixed(2);

            $('input.costo-utilidad').val(precio).change();
        }

        function cambiarImpuesto() {
            var costo = parseFloat($('input#ultimo_costo').val());
            var porcentaje_impuesto = parseFloat($('input#porcentaje_impuesto').val());

            var impuesto = (costo * porcentaje_impuesto / 100).toFixed(2);

            $('input#impuesto').val(impuesto);
        }

        function cambiarPorcentajeImpuesto() {
            var costo = parseFloat($('input#ultimo_costo').val());
            var impuesto = parseFloat($('input#impuesto').val());
            if(costo != 0){
                var porcentaje_impuesto = (impuesto * 100 / costo).toFixed(2);
                $('input#porcentaje_impuesto').val(porcentaje_impuesto);
            }
        }
    </script>
@endsection