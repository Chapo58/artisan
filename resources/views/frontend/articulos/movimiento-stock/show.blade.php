@extends('frontend.layouts.app')

@section ('title', trans('labels.frontend.movimientostock_titulo')." - ".trans('buttons.general.crud.show'))

@section('content')
    <div class="main-content">
        <div class="main-content-inner">
            @include ('frontend.includes.breadcrumbs')

            <div class="page-header">
                <h1>
                    {{ trans('labels.frontend.movimientostock_titulo') }}
                    <small>
                        <i class="ace-icon fa fa-angle-double-right"></i>
                        <small>{{ trans('buttons.general.crud.show') }} MovimientoStock</small>
                        <small><i class="ace-icon fa fa-angle-double-right"></i></small>
                        <small>{{ $movimientostock->id }}# {{ $MovimientoStock }}</small>
                    </small>

                    <div class="box-tools pull-right">
                        <a href="{{ url('/articulos/movimiento-stock') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning btn-sm">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> {{ trans('buttons.general.crud.back') }}
                        </a>
                        <a href="{{ url('/articulos/movimiento-stock/' . $movimientostock->id . '/edit') }}" title="{{ trans('buttons.general.crud.edit') }}" class="btn btn-primary btn-sm">
                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i> {{ trans('buttons.general.crud.edit') }}
                        </a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['articulos/movimientostock', $movimientostock->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> '.trans("buttons.general.crud.delete"), array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-sm',
                                    'title' => trans("buttons.general.crud.delete")." ".trans("labels.frontend.movimientostock"),
                                    'onclick'=>'return confirm("Confirm delete?")'
                            ))!!}
                        {!! Form::close() !!}
                    </div>
                </h1>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-borderless">
                        <tbody>
                            <tr>
                                <th>ID</th><td>{{ $movimientostock->id }}</td>
                            </tr>
                            <tr><th> Stock Id </th><td> {{ $movimientostock->stock_id }} </td></tr><tr><th> Tipo Movimiento Stock Id </th><td> {{ $movimientostock->tipo_movimiento_stock_id }} </td></tr><tr><th> Cantidad </th><td> {{ $movimientostock->cantidad }} </td></tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
