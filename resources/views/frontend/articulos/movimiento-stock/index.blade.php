@extends ('frontend.layouts.app')

@section ('title', trans('labels.frontend.movimientostock_titulo')." - ".trans('navs.frontend.user.administration'))

@section('after-styles')
    {{ Html::style("css/frontend/plugins/datatables/responsive.dataTables.min.css") }}
@endsection

@section('content')
    <div class="row page-titles">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('labels.frontend.movimientostock_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item active">{{ trans('labels.frontend.movimientostock_titulo') }}</li>
            </ol>
        </div>
    </div>

    @tips($_SERVER['REQUEST_URI'])

    <div class="card">
        <div class="card-body">
            <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
            @include('includes.partials.messages')
            <div class="table-responsive">
                <table id="movimientostock-table" class="table selectable table-striped table-hover table-sm">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Tipo</th>
                            <th>Fecha</th>
                            <th>Articulo</th>
                            <th>Cantidad</th>
                            @if($sucursales->count() > 1)
                                <th>Origen</th>
                                <th>Destino</th>
                                <th>Estado</th>
                                <th>{{ trans('labels.general.actions') }}</th>
                            @endif
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

@endsection

@section('after-scripts')
    {{ Html::script("js/frontend/plugins/datatables/jquery.dataTables.min.js") }}
    {{ Html::script("js/frontend/plugins/datatables/dataTables.responsive.min.js") }}
    {{ Html::script("js/frontend/plugins/datatables/dataTables.select.min.js") }}

<script>
    $(document).ready(function() {
        $('#movimientostock-table').DataTable({
            processing: true,
            serverSide: true,
            responsive: {
                details: {
                    type: 'column',
                    target: 'tr'
                }
            },
            "columnDefs": [{
                targets: 0,
                className: 'control',
                orderable: false,
                searchable: false,
                defaultContent: '&nbsp;&nbsp;'
            },
                { responsivePriority: 1, targets: -1 },
                { responsivePriority: 1, targets: -2 }],
            "aaSorting": [],
            ajax: '{!! route('frontend.articulos.MovimientosStockData') !!}',
            columns: [
                {data: null, name: ''},
                {data: 'tipo_movimiento', name: 'tipo_movimiento'},
                {data: 'created_at', name: 'created_at'},
                {data: 'articulo', name: 'articulo'},
                {data: 'cantidad', name: 'cantidad'},
                @if($sucursales->count() > 1)
                    {data: 'sucursal_origen', name: 'sucursal_origen'},
                    {data: 'sucursal_destino', name: 'sucursal_destino'},
                    {data: 'estado', name: 'estado'},
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                @endif
            ]
        });
    });

    $("body").on("click", ".aprobar", function () {
        var id = $(this).attr("data-id");
        swal({
            title: "Aprobar Solicitud",
            text: "¿Realmente desea aprobar esta solicitud?",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: '#2EB800',
            confirmButtonText: "Aprobar"
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: 'GET',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/articulos/movimiento-stock/aprobar/'+id,
                    data: {"_method" : 'GET'},
                    success: function (msg) {
                        swal("Aprobado!", "Solicitud Aprobada.", "success").then((result) => { window.location.reload(); });
                    }
                });
            }
        });
    })

    $("body").on("click", ".rechazar", function () {
        var id = $(this).attr("data-id");
        swal({
            title: "Rechazar Solicitud",
            text: "¿Realmente desea rechazar esta solicitud?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#ff8726',
            confirmButtonText: "Rechazar"
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: 'GET',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/articulos/movimiento-stock/rechazar/'+id,
                    data: {"_method" : 'GET'},
                    success: function (msg) {
                        swal("Rechazado", "Solicitud Rechazada.", "danger").then((result) => { window.location.reload(); });
                    }
                });
            }
        });
    })
</script>

@endsection