<div class="card">
    <div class="card-body">
        <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
        <input hidden name="stock_id" value="{{ $stock_id }}">
        <br>
        <div class="row form-group">
            <div class="col-md-6">
                {!! Form::label('tipo_movimiento_stock_id', 'Tipo de Movimiento:') !!}
                {!! Form::select('tipo_movimiento_stock_id', $tiposMovimientoStock, isset($movimientoStock->tipoMovimiento) ? $movimientoStock->tipoMovimiento->id : 0, array('class' => 'form-control')) !!}
                {!! $errors->first('tipo_movimiento_stock_id', '<p class="text-danger">:message</p>') !!}
            </div>
            <div class="col-md-6">
                {!! Form::label('cantidad', 'Ajuste:') !!}
                <i class="fa fa-question-circle text-info mytooltip">
                    <span class="tooltip-content3">Cantidad de artículos a restar o sumar al stock. Números negativos restan, positivos suman, indistintamente del tipo de movimiento seleccionado</span>
                </i>
                {!! Form::number('cantidad', null, ['class' => 'form-control', 'step' => '0.01','placeholder' => 'Ej: -3']) !!}
                {!! $errors->first('cantidad', '<p class="text-danger">:message</p>') !!}
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-12">
                {!! Form::label('descripcion', 'Descripcion') !!}
                {!! Form::textarea('descripcion', null, ['class' => 'form-control','size' => '10x3','placeholder' => 'Ej: Articulos perdidos']) !!}
                {!! $errors->first('descripcion', '<p class="text-danger">:message</p>') !!}
            </div>
        </div>
        <hr>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-12">
                    {{ Form::submit(isset($submitButtonText) ? $submitButtonText : trans('buttons.general.save'), ['class' => 'btn btn-lg btn-themecolor pull-right']) }}
                </div>
                <div class="col-md-6"> </div>
            </div>
        </div>
    </div>
</div>
