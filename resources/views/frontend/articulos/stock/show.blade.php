@extends('frontend.layouts.app')

@section ('title', trans('labels.frontend.stock_titulo')." - ".trans('buttons.general.crud.show'))

@section('content')
    <div class="row page-titles">
        <div class="col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('navs.frontend.user.administration') }} {{ trans('labels.frontend.stock_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item"><a href="{{url('/articulos/stock')}}">{{ trans('labels.frontend.stock_titulo') }}</a></li>
                <li class="breadcrumb-item">{{ trans('buttons.general.crud.show') }} {{ trans('labels.frontend.stock_titulo') }}</li>
                <li class="breadcrumb-item active">{{ $stock->articulo->codigo }}# {{ $stock->articulo }}</li>
            </ol>
        </div>
        <div class="col-4">
            <div class="pull-right">
                <a href="{{ url('/articulos/stock') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans('buttons.general.crud.back') }}</span>
                </a>
                <a href="{{ url('/articulos/stock/' . $stock->id . '/edit') }}" title="{{ trans('buttons.general.crud.edit') }}" class="btn btn-info">
                    <i class="fa fa-trash" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans('buttons.general.crud.edit') }}</span>
                </a>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
            <div class="table-responsive">
                <table class="table table-borderless">
                    <tbody>
                        <tr>
                            <th> Artículo </th><td> {{ $stock->articulo }} </td>
                        </tr>
                        <tr>
                            <th> Existencia Actual </th><td> {{ number_format($stock->cantidad) }} </td>
                        </tr>
                        <tr>
                            <th> Límite Máximo </th><td> {{ number_format($stock->maximo) }} </td>
                        </tr>
                        <tr>
                            <th> Límite Mínimo </th><td> {{ number_format($stock->minimo) }} </td>
                        </tr>
                    </tbody>
                </table>
                <table class="table table-bordered table-striped color-table muted-table col-md-6">
                    <thead class="thin-border-bottom">
                    <tr>
                        <th>
                            Fecha
                        </th>
                        <th>
                            Tipo de Movimiento
                        </th>
                        <th>
                            Cantidad
                        </th>
                        <th>
                            Usuario
                        </th>
                        <th>
                            Descripción
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($stock->movimientosStock as $movimiento)
                        <tr>
                            <td>{{ $movimiento->created_at->format('d/m/Y H:i') }}</td>
                            <td>{{ $movimiento->tipoMovimiento->nombre }}</td>
                            <td><b>{{ number_format($movimiento->cantidad) }}</b></td>
                            <td> {{($movimiento->usuario) ? $movimiento->usuario : '' }} </td>
                            <td>{{ $movimiento->descripcion }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection
