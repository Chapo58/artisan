@extends ('frontend.layouts.app')

@section ('title', trans('labels.frontend.stock_titulo')." - ".trans('navs.frontend.user.administration'))

@section('after-styles')
    {{ Html::style("css/frontend/plugins/datatables/responsive.dataTables.min.css") }}
@endsection

@section('content')
    <div class="row page-titles">
        <div class="col-6 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('navs.frontend.user.administration') }} {{ trans('labels.frontend.stock_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item active">{{ trans('labels.frontend.stock_titulo') }}</li>
            </ol>
        </div>
        <div class="col-6">
            <div class="pull-right">
                @if($sucursales->count() > 1)
                <a href="{{ url('/articulos/stock/solicitud-masiva') }}" class="btn btn-primary btn-lg" title="Solicitud Masiva">
                    <i class="fa fa-exchange" aria-hidden="true"></i> S
                    <span class="hidden-xs-down">olicitud Masiva</span>
                </a>
                @endif
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
            @include('includes.partials.messages')
            <div class="table-responsive">
                <table id="stock-table" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Codigo</th>
                            <th>Artículo</th>
                            @if($sucursales->count() > 1)
                                @foreach($sucursales as $sucursal)
                                    <th>{{ $sucursal }}</th>
                                @endforeach
                            @else
                                <th>Existencias</th>
                            @endif
                            <th>Máximo</th>
                            <th>Mínimo</th>
                            <th>Alarma</th>
                            <th>{{ trans('labels.general.actions') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($stock->unique('articulo_id') as $item)
                        @if($item->articulo)
                        <tr>
                            <td>{{ $item->articulo->codigo }}</td>
                            <td>{{ $item->articulo }} {{ isset($item->articulo->marca) ? ' - '.$item->articulo->marca : '' }} {{ isset($item->articulo->modelo) ? ' - '.$item->articulo->modelo : '' }}{{ isset($item->articulo->modelo_secundario) ? ' - '.$item->articulo->modelo_secundario : '' }}</td>
                            @if($sucursales->count() > 1)
                                @foreach($sucursales as $sucursal)
                                    <td>
                                        @if($item->articulo->stockPorSucursal($sucursal))
                                            <b class="@if($item->articulo->stockPorSucursal($sucursal)->cantidad > $item->minimo) green @else red @endif">
                                                {{number_format($item->articulo->stockPorSucursal($sucursal)->cantidad, 0)}}
                                            </b>
                                        @else
                                            <b class="text-danger">0</b>
                                        @endif
                                    </td>
                                @endforeach
                            @else
                                <td>
                                    <b class="@if($item->cantidad > $item->minimo) text-success @else text-danger @endif">
                                        {{number_format($item->cantidad, 0)}}
                                    </b>
                                </td>
                            @endif
                            <td><b>{{ number_format($item->maximo, 0) }}</b></td>
                            <td><b>{{ number_format($item->minimo, 0) }}</b></td>
                            <td>
                                @if($item->alarma == 1)
                                    Activa
                                @else
                                    Desactivada
                                @endif
                            </td>
                            <td>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-secondary dropdown-toggle btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Acciones
                                    </button>
                                    <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 33px, 0px); top: 0px; left: 0px; will-change: transform;">
                                        <a class="dropdown-item" href="{{ url('/articulos/stock/' . $item->id) }}">
                                            <i class="fa fa-eye" aria-hidden="true"></i> {{ trans('buttons.general.crud.show') }} {{ trans('labels.frontend.stock') }}
                                        </a>
                                        @roles(['Usuario Admin', 'Usuario Gerente'])
                                        <a class="dropdown-item" href="{{ url('/articulos/stock/' . $item->id . '/edit') }}">
                                            <i class="fa fa-cogs" aria-hidden="true"></i> Configurar
                                        </a>
                                        <a class="dropdown-item" href="{{ url('/articulos/movimiento-stock/' . $item->id . '/create') }}">
                                            <i class="fa fa-balance-scale" aria-hidden="true"></i> Ajuste de Stock
                                        </a>
                                        @endauth
                                        @if($sucursales->count() > 1)
                                        <div class="dropdown-divider"></div>
                                        <a onclick="pasarId({{$item->id}})" data-toggle="modal" data-target="#modalSolicitarStock" data-backdrop="static" class="dropdown-item" href="javascript:void(0)">
                                            <i class="fa fa-undo" aria-hidden="true"></i> Solicitar Stock
                                        </a>
                                        @endif
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endif
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @include('frontend.articulos.stock.solicitar_stock_dialog')
@endsection

@section('after-scripts')
    {{ Html::script("js/frontend/plugins/datatables/jquery.dataTables.min.js") }}
    {{ Html::script("js/frontend/plugins/datatables/dataTables.responsive.min.js") }}
    {{ Html::script("js/frontend/plugins/datatables/dataTables.select.min.js") }}

<script>
    $(document).ready(function() {
        $('#stock-table').DataTable({responsive: true,});
    } );
</script>

@endsection