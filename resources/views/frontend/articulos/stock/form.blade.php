<div class="card">
    <div class="card-body">
        <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
        <br>
        <div class="row form-group">
            <div class="input-group col-md-12">
                <span class="input-group-addon">{!! Form::label('articulo', 'Artículo:') !!}</span>
                {!! Form::text('articulo', $stock->articulo, ['class' => 'form-control', 'readonly']) !!}
            </div>
        </div>
        <div class="row form-group">
            <div class="input-group col-md-6">
                <span class="input-group-addon">{!! Form::label('cantidad', 'Existencia') !!}</span>
                {!! Form::number('cantidad', null, ['class' => 'form-control', 'required' => 'required']) !!}
                {!! $errors->first('cantidad', '<p class="text-danger">:message</p>') !!}
            </div>
            <div class="col-md-4">
                {{ $stock->articulo->unidad->presentacion }}
            </div>
        </div>
        <div class="row form-group">
            <div class="input-group col-md-6">
                <span class="input-group-addon">{!! Form::label('maximo', 'Límite Máximo') !!}</span>
                {!! Form::number('maximo', null, ['class' => 'form-control']) !!}
                {!! $errors->first('maximo', '<p class="text-danger">:message</p>') !!}
            </div>
            <div class="input-group col-md-6">
                <span class="input-group-addon">{!! Form::label('minimo', 'Límite Mínimo') !!}</span>
                {!! Form::number('minimo', null, ['class' => 'form-control']) !!}
                {!! $errors->first('minimo', '<p class="text-danger">:message</p>') !!}
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-12">
                {!! Form::label('alarma', 'Alerta de stock') !!}
                <div class="switch">
                    <label>DESACTIVADO
                        {!! Form::checkbox('alarma', true, $stock->alarma) !!}
                        <span class="lever"></span>
                        ACTIVADO</label>
                </div>
            </div>
        </div>
        <hr>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-12">
                    {{ Form::submit(isset($submitButtonText) ? $submitButtonText : trans('buttons.general.save'), ['class' => 'btn btn-lg btn-themecolor pull-right']) }}
                </div>
            </div>
        </div>
    </div>
</div>
