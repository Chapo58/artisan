<div class="modal fade" id="modalSolicitarStock" tabindex="-1" role="dialog" aria-labelledby="solicitarStockModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="solicitarStockModalLabel">SOLICITAR TRANSFERENCIA DE ARTICULO</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar"><span aria-hidden="true">&times;</span></button>
            </div>
            {!! Form::open([
                'method'=>'POST',
                'action' => ['Frontend\Articulos\StockController@transferir'],
                'style' => 'display:inline'
            ]) !!}
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="input-group">
                            <div class="input-group-addon"><strong>Sucursal Origen: </strong></div>
                            {!! Form::select('sucursal', $sucursalesArray, '', array('class' => 'form-control', 'required' => 'required')) !!}
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="input-group-addon"><strong>Sucursal Destino: {{(Auth::user()->sucursal) ? $sucursalesArray[Auth::user()->sucursal->id] : ''}}</strong></div>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-md-12">
                        <div class="input-group">
                            <div class="input-group-addon"><strong>Cantidad solicitada: </strong></div>
                            {!! Form::number('cantidad', 1, ['class' => 'form-control', 'required' => 'required']) !!}
                        </div>
                    </div>
                </div>
                {{ Form::hidden('idStock',0,['id' => 'idStock']) }}
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-themecolor">Solicitar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

@section('before-scripts')

    <script>
        function pasarId(idStock){
            $('#idStock').val(idStock);
        }
    </script>

@endsection