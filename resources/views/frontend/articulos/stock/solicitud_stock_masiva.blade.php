@extends('frontend.layouts.app')

@section ('title', trans('labels.frontend.stock_titulo')." - Solicitud Masiva")

@section('after-styles')
    {{ Html::style("css/frontend/plugins/jquery/jquery-ui.min.css") }}
@endsection

@section('content')
    <div class="row page-titles">
        <div class="col-md-8 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">Solicitud Masiva de Articulos</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item"><a href="{{url('/articulos/stock')}}">{{ trans('labels.frontend.stock_titulo') }}</a></li>
                <li class="breadcrumb-item active">Solicitud Masiva</li>
            </ol>
        </div>
        <div class="col-md-4">
            <div class="pull-right">
                <a href="{{ url('/articulos/stock') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning btn-sm">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i> {{ trans('buttons.general.crud.back') }}
                </a>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
            <br>
            @include('includes.partials.messages')
            {!! Form::open(['url' => '/articulos/stock/guardarSolicitudMasiva', 'class' => 'form-material', 'files' => true, 'method' => 'POST']) !!}
                <div class="form-group {{ $errors->has('sucursal_id') ? 'has-error' : ''}}">
                    {!! Form::label('sucursal_id', 'Sucursal') !!}
                    {!! Form::select('sucursal_id', $sucursalesArray, '', ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('sucursal_id', '<p class="text-danger">:message</p>') !!}
                </div>
                <table id="tabla-detalles-articulos" class="table color-table info-table">
                    <thead>
                    <tr>
                        <th>Articulo</th>
                        <th>Cantidad a solicitar</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody></tbody>
                    <tfoot>
                    <tr>
                        <th colspan="10">
                            <button class="btn btn-info btn-sm" id="agregar-detalle">
                                <i class="fa fa-plus" aria-hidden="true"></i> Agregar Articulo
                            </button>
                        </th>
                    </tr>
                    </tfoot>
                </table>
            {!! Form::close() !!}
            <hr>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-12">
                        {{ Form::submit(isset($submitButtonText) ? $submitButtonText : trans('buttons.general.save'), ['class' => 'btn btn-lg btn-themecolor pull-right']) }}
                    </div>
                </div>
            </div>
            <br>
        </div>
    </div>

@endsection

@section('after-scripts')
    {{ Html::script("js/frontend/plugins/jquery/jquery-ui.min.js") }}
    {{ Html::script("js/frontend/plugins/maskedinput/jquery.maskedinput.min.js") }}

<script>
    $( document ).ready(function() {

        $(function() {
            $( document ).tooltip({
                position: {
                    my: "right top",
                    at: "left-5 top-5",
                    collision: "none"
                }
            });
        });

        $(document).on("keydown.autocomplete", ".buscar-articulo", function (e) {
            if (e.keyCode == 13) {
                e.preventDefault();
            }else{
                var sucursal = $('select#sucursal_id option:selected').val();
                $(this).autocomplete({
                    source: '{{ url('/articulos/articulo/obtenerStockArticulo') }}/' + sucursal,
                    minLength: 2,
                    autoFocus: true,
                    select: function (e, ui) {
                        var fila = $(this).parent().parent();

                        fila.find('input.articulo-id').val(ui.item.id);
                        ui.item.existencia = parseInt(ui.item.existencia);
                        var cantidad = parseInt(fila.find('td input.cantidad').val());
                        if(ui.item.existencia < cantidad && $.isNumeric(cantidad)) {
                            fila.find('td input.cantidad').val(ui.item.existencia);
                        }
                        fila.find('td input.cantidad').attr('max', ui.item.existencia);
                        fila.find('td input.cantidad').attr('title', 'En stock: '+ui.item.existencia);

                    }
                });
            }
        });

        $(document).on('click','.borrar-detalle', function() {
            inputNombre = this.attributes['name'].value;
            $('.borrar-detalle[name="' + inputNombre + '"]').parent().parent().hide("slow");
            $('.borrar-detalle[name="' + inputNombre + '"]').parent().find('input.estado').val(0);
        });

        $("#agregar-detalle").click(function(){
            agregarDetalle();
        });

        agregarDetalle();

    });

    function agregarDetalle(inputName) {
        var numero_orden = $('table.table tbody tr').length+ 1;

        $("#tabla-detalles-articulos").append('<tr>' +
            '<td>' +
                '<input class="form-control buscar-articulo" name="detalles['+(numero_orden*-1)+'][articulo]"  placeholder="Buscar Articulo..." type="text" required="required">' +
                '<input class="articulo-id" name="detalles['+(numero_orden*-1)+'][articulo_id]" type="hidden" value="">' +
            '</td>' +
            '<td>' +
                '<input class="form-control cantidad" name="detalles['+(numero_orden*-1)+'][cantidad]" type="number" value="1" max="" min="0" title="" required="required">' +
            '</td>' +
            '<td align="center">' +
                '<a class="btn btn-danger btn-sm btn-circle borrar-detalle" name="detalles['+(numero_orden*-1)+'][eliminar]" tabindex="99999">' +
                    '<i class="fa fa-trash-o" aria-hidden="true"></i>' +
                '</a>' +
                '<input type="hidden" class="estado" name="detalles['+(numero_orden*-1)+'][estado]" value="1">' +
            '</td>' +
            '</tr>');
        if(inputName != null){
            $("input[name='detalles["+(numero_orden*-1)+"]["+inputName+"]']").focus();
        }else{
            $("input[name='detalles["+(numero_orden*-1)+"][articulo]']").focus();
        }
    }


</script>
@endsection