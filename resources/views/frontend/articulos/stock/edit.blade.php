@extends('frontend.layouts.app')

@section ('title', trans('labels.frontend.stock_titulo')." - Configurar")

@section('content')
    <div class="row page-titles">
        <div class="col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('navs.frontend.user.administration') }} {{ trans('labels.frontend.stock_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item"><a href="{{url('/articulos/stock')}}">{{ trans('labels.frontend.stock_titulo') }}</a></li>
                <li class="breadcrumb-item">{{ trans('buttons.general.crud.edit') }} {{ trans('labels.frontend.stock') }}</li>
                <li class="breadcrumb-item active">{{ $stock->articulo->codigo }}# {{ $stock->articulo }}</li>
            </ol>
        </div>
        <div class="col-4">
            <div class="pull-right">
                <a href="{{ url('/articulos/stock') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans('buttons.general.crud.back') }}</span>
                </a>
            </div>
        </div>
    </div>

    @include('includes.partials.messages')

    {!! Form::model($stock, [
        'method' => 'PATCH',
        'url' => ['/articulos/stock', $stock->id],
        'class' => 'form-horizontal',
        'files' => true
    ]) !!}

        @include ('frontend.articulos.stock.form', ['submitButtonText' => trans("buttons.general.crud.update")])

    {!! Form::close() !!}

@endsection
