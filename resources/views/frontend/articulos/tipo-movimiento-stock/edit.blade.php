@extends('frontend.layouts.app')

@section ('title', trans('labels.frontend.tipomovimientostock_titulo')." - ".trans('buttons.general.crud.edit'))

@section('content')
    <div class="main-content">
        <div class="main-content-inner">
            @include ('frontend.includes.breadcrumbs')
            <div class="page-header">
                <h1>
                    {{ trans('labels.frontend.tipomovimientostock_titulo') }}
                    <small>
                        <i class="ace-icon fa fa-angle-double-right"></i>
                        <small>{{ trans('buttons.general.crud.edit') }} TipoMovimientoStock </small> #
                        <small><i class="ace-icon fa fa-angle-double-right"></i></small>
                        <small>{{ $tipomovimientostock->id }}# {{ $tipomovimientostock }}</small>
                    </small>

                    <div class="box-tools pull-right">
                        <a href="{{ url('/articulos/tipo-movimiento-stock') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning btn-sm">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> {{ trans('buttons.general.crud.back') }}
                        </a>
                    </div>
                </h1>
            </div>

            <div class="row">
                <div class="col-xs-12">

                    @include('includes.partials.messages')

                    {!! Form::model($tipomovimientostock, [
                        'method' => 'PATCH',
                        'url' => ['/articulos/tipo-movimiento-stock', $tipomovimientostock->id],
                        'class' => 'form-horizontal',
                        'files' => true
                    ]) !!}

                        @include ('frontend.articulos.tipo-movimiento-stock.form', ['submitButtonText' => trans("buttons.general.crud.update")])

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
