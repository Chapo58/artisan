@extends('frontend.layouts.app')

@section ('title', 'Carga rapida de articulos')

@section('after-styles')
    {{ Html::style("css/frontend/plugins/datatables/responsive.dataTables.min.css") }}
@endsection

@section('content')
    <div class="row page-titles">
        <div class="col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('navs.frontend.user.administration') }} {{ trans('labels.frontend.articulo_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item"><a href="{{url('/articulos/articulo')}}">{{ trans('labels.frontend.articulo_titulo') }}</a></li>
                <li class="breadcrumb-item active">Carga rapida de articulos</li>
            </ol>
        </div>
        <div class="col-4">
            <div class="pull-right">
                <a href="{{ url('/articulos/articulo') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans('buttons.general.crud.back') }}</span>
                </a>
            </div>
        </div>
    </div>

    @include('includes.partials.messages')

    {!! Form::open(['url' => '/articulos/articulo/guardarArticuloRapido', 'class' => 'form-horizontal', 'files' => true]) !!}
        @include ('frontend.articulos.articulo.form_rapido')
    {!! Form::close() !!}

@endsection

@section('after-scripts')
    {{ Html::script("js/frontend/plugins/datatables/jquery.dataTables.min.js") }}
    {{ Html::script("js/frontend/plugins/datatables/dataTables.responsive.min.js") }}
    {{ Html::script("js/frontend/plugins/datatables/dataTables.select.min.js") }}
    <script>
        $( document ).ready(function() {
            actualizarPreciosListas();

            $('#tabla-precios').DataTable({
                "paging":   false,
                "info":     false,
                "searching":   false,
                responsive: {
                    details: {
                        type: 'column',
                        target: 'tr'
                    }
                },
                "columnDefs": [{
                    targets: 0,
                    className: 'control',
                    orderable: false,
                    searchable: false,
                    defaultContent: '&nbsp;&nbsp;'
                }]
            });
        });

        $('select#porcentaje_iva').on('change', function () {
            actualizarPreciosListas();
        });

        $(document).on('change', 'input#ultimo_costo', function () {
            $('input#ultimo_costo').val(this.value);
            actualizarPreciosListas();
        });

        $(document).on('change', 'input#lleva_stock', function () {
            if($('input#lleva_stock').is(':checked')){
                $('#parametros_stock').show();
            } else {
                $('#parametros_stock').hide();
            }
        });

        $(document).on('change', 'input.porcentaje-lista', function () {
            var iva = parseFloat($('select#porcentaje_iva option:selected').text());
            var costo = parseFloat($('input#ultimo_costo').val());

            if(isNaN(costo) || costo == 0 || isNaN(iva)){
                swal("Debe definir un costo y un porcentaje de iva al articulo");
            } else {
                actualizarPreciosListas();
            }
        });

        function actualizarPreciosListas(){
            var $filasTablaPrecios = $("#tabla-precios tbody tr");
            $filasTablaPrecios.each(function (index) {
                var $filaPrecio = $(this);

                var iva = parseFloat($('select#porcentaje_iva option:selected').text());
                var porLista = parseFloat($filaPrecio.find('.porcentaje-lista').val());
                var costo = parseFloat($('input#ultimo_costo').val());

                if(!isNaN(costo) && costo != 0 && !isNaN(iva)){
                    var precioNeto = (costo * porLista)/100 + costo;
                    $filaPrecio.find('.precio-neto').val(precioNeto.toFixed(2));

                    var precioVenta = (precioNeto * iva)/100 + precioNeto;
                    $filaPrecio.find('.precio-venta').val(precioVenta.toFixed(2));
                }
            });
        }
    </script>
@endsection