@extends('frontend.layouts.app')

@section ('title', trans('labels.frontend.articulo_titulo')." - ".trans('buttons.general.crud.show'))

@section('after-styles')
    {{ Html::style("css/frontend/plugins/datatables/responsive.dataTables.min.css") }}
@endsection

@section('content')
    <div class="row page-titles">
        <div class="col-6 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('navs.frontend.user.administration') }} {{ trans('labels.frontend.articulo_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item"><a href="{{url('/articulos/articulo')}}">{{ trans('labels.frontend.articulo_titulo') }}</a></li>
                <li class="breadcrumb-item">{{ trans('buttons.general.crud.show') }} {{ trans('labels.frontend.articulo_titulo') }}</li>
                <li class="breadcrumb-item active">{{ $articulo->codigo }}# {{ $articulo }}</li>
            </ol>
        </div>
        <div class="col-6">
            <div class="pull-right">
                <a href="{{ url('/articulos/articulo') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans('buttons.general.crud.back') }}</span>
                </a>
                <a href="{{ url('/articulos/articulo/' . $articulo->id . '/edit') }}" title="{{ trans('buttons.general.crud.edit') }}" class="btn btn-info">
                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans('buttons.general.crud.edit') }}</span>
                </a>
                <a href="javascript:void(0)" title="{{trans("buttons.general.crud.delete")." ".trans("labels.frontend.articulo")}}" class="btn btn-danger delete">
                    <i class="fa fa-trash" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans("buttons.general.crud.delete") }}</span>
                </a>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
            <ul class="nav nav-tabs profile-tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#info" role="tab">
                        <span class="hidden-sm-up"><i class="fa fa-info-circle"></i></span>
                        <span class="hidden-xs-down"><i class="fa fa-info-circle"></i> Información</span>
                    </a>
                </li>
                @if(isset($stock))
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#stock" role="tab">
                            <span class="hidden-sm-up"><i class="fa fa-cubes"></i></span>
                            <span class="hidden-xs-down"><i class="fa fa-cubes"></i> Stock</span>
                        </a>
                    </li>
                @endif
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#ventas" role="tab">
                        <span class="hidden-sm-up"><i class="fa fa-ticket"></i></span>
                        <span class="hidden-xs-down"><i class="fa fa-ticket"></i> Ventas</span>
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="info" role="tabpanel">
                    <h4 class="font-medium m-t-30">{{ $articulo }}</h4>
                    <hr>
                    <div class="row">
                        <div class="col-md-3">
                            <img src="{{ isset($articulo->imagen_url) ? $articulo->imagen_url : '/articulos/no-image.jpg' }}" alt="{{ isset($articulo->nombre) ? $articulo->nombre : '' }}" width="300" style="border: 2px solid lightslategrey;">
                        </div>
                        <div class="col-md-9">
                            <div class="row">
                                <div class="col-md-3 font-weight-bold">Codigo Interno</div>
                                <div class="col-md-3">{{ $articulo->codigo }}</div>

                                <div class="col-md-3 font-weight-bold">Código de barras</div>
                                <div class="col-md-3">{{ $articulo->codigo_barra }}</div>
                            </div><hr>
                            <div class="row">
                                <div class="col-md-3 font-weight-bold">{{ trans('validation.attributes.nombre') }}</div>
                                <div class="col-md-3">{{ $articulo->nombre }}</div>

                                <div class="col-md-3 font-weight-bold">{{ trans('validation.attributes.marca_id') }}</div>
                                <div class="col-md-3">
                                    @if(isset($articulo->marca))
                                        <a href="{{url('articulos/marca/'.$articulo->marca->id)}}">{{$articulo->marca}}</a>
                                    @else
                                        Ninguna
                                    @endif
                                </div>
                            </div><hr>
                            <div class="row">
                                <div class="col-md-3 font-weight-bold">Modelo</div>
                                <div class="col-md-3">{{ $articulo->modelo }}</div>

                                <div class="col-md-3 font-weight-bold">Modelo Secundario</div>
                                <div class="col-md-3">{{ $articulo->modelo_secundario }}</div>
                            </div><hr>
                            <div class="row">
                                <div class="col-md-3 font-weight-bold">{{ trans('validation.attributes.unidad_id') }}</div>
                                <div class="col-md-3">{{ $articulo->unidad }}</div>

                                <div class="col-md-3 font-weight-bold">Unidades por bulto</div>
                                <div class="col-md-3">
                                    @if($articulo->unidad)
                                        {{ number_format($articulo->cantidad,0) }} {{ $articulo->unidad->presentacion }}
                                    @endif
                                </div>
                            </div><hr><div class="row">
                                <div class="col-md-3 font-weight-bold">{{ trans('validation.attributes.rubro_id') }}</div>
                                <div class="col-md-3">
                                    @if(isset($articulo->rubro))
                                        <a href="{{url('articulos/rubro/'.$articulo->rubro->id)}}">{{$articulo->rubro}}</a>
                                    @else
                                        Ninguno
                                    @endif
                                </div>

                                <div class="col-md-3 font-weight-bold">{{ trans('validation.attributes.subrubro_id') }}</div>
                                <div class="col-md-3">
                                    @if(isset($articulo->subrubro))
                                        <a href="{{url('articulos/subrubro/'.$articulo->subrubro->id)}}">{{$articulo->subrubro}}</a>
                                    @else
                                        Ninguno
                                    @endif
                                </div>
                            </div><hr>
                            <div class="row">
                                <div class="col-md-3 font-weight-bold">Porcentaje IVA</div>
                                <div class="col-md-3">{{ $articulo->porcentajeIva() }}</div>

                                <div class="col-md-3 font-weight-bold">Actividad AFIP</div>
                                <div class="col-md-3">
                                    @if(isset($articulo->actividadAfip))
                                        <a href="{{url('configuraciones/actividad-afip/'.$articulo->actividadAfip->id)}}">{{$articulo->actividadAfip}}</a>
                                    @endif
                                </div>
                            </div><hr>
                            <div class="row">
                                <div class="col-md-3 font-weight-bold">Moneda</div>
                                <div class="col-md-3"><a href="{{url('configuraciones/moneda/'.$articulo->moneda->id)}}">{{ $articulo->moneda }}</a></div>

                                @roles(['Usuario Admin', 'Usuario Gerente'])
                                    <div class="col-md-3 font-weight-bold">Último Costo</div>
                                    <div class="col-md-3">{{ $articulo->moneda->signo }} {{ number_format($articulo->ultimo_costo, 2, ',', '') }}</div>
                                @endauth
                            </div><hr>
                            <div class="row">
                                <div class="col-md-3 font-weight-bold">Descripción</div>
                                <div class="col-md-9">{{ $articulo->descripcion }}</div>
                            </div><hr>
                            <div class="row">
                                <div class="col-md-3 font-weight-bold">Creado el</div>
                                <div class="col-md-3">{{ $articulo->created_at->format('d/m/Y H:m') }}</div>

                                <div class="col-md-3 font-weight-bold">Ultima Modificación</div>
                                <div class="col-md-3">{{ $articulo->updated_at->format('d/m/Y H:m') }}</div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="table-responsive">
            <table id="verarticulos-table" class="table color-bordered-table selectable table-hover table-striped form-material">
                <thead class="thin-border-bottom">
                <tr style="background:#99abb4;color:#fff;">
                    <th></th>
                    <th>
                        Lista de Precios
                    </th>
                    <th>
                        Precio Neto
                    </th>
                    <th>
                        IVA
                    </th>
                    <th>
                        Impuesto Interno
                    </th>
                    <th>
                        Precio Bruto
                    </th>
                    <th>
                        Actualizado
                    </th>
                </tr>
                </thead>
                <tbody>
                @foreach($articulo->precios as $precio)
                    @if(isset($precio->listaPrecio))
                        <tr>
                            <td>&nbsp;&nbsp;</td>
                            <td>
                                <a href="{{url('/ventas/lista-precios/'.$precio->listaPrecio->id)}}">
                                    {{ $precio->listaPrecio }}
                                </a>
                            </td>
                            <td>
                                <b class="text-success">{{ $articulo->moneda->signo }} {{ number_format($precio->precio, 2, ',', '')  }}</b>
                            </td>
                            <td>
                                <b class="text-success">{{ $articulo->moneda->signo }} {{ number_format( $precio->precio*($articulo->porcentajeIva()/100), 2, ',', '') }}</b>
                            </td>
                            <td>
                                <b class="text-success">{{ $articulo->moneda->signo }} {{ $articulo->impuesto }}</b>
                            </td>
                            <td>
                                <b class="text-success">{{ $articulo->moneda->signo }} {{ number_format( $precio->precio*(1 + $articulo->porcentajeIva()/100) + $articulo->impuesto, 2, ',', '')  }}</b>
                            </td>
                            <td>{{ $precio->updated_at->format('d/m/Y H:i') }}</td>
                        </tr>
                    @endif
                @endforeach
                </tbody>
            </table>
            <table id="verarticulos2-table" class="table color-bordered-table selectable table-hover table-striped form-material">
                <thead class="thin-border-bottom">
                <tr style="background:#99abb4;color:#fff;">
                    <th></th>
                    <th>
                        Proveedores
                    </th>
                    <th>
                        Fecha
                    </th>
                    <th>
                        Moneda
                    </th>
                    <th>
                        Costo Uni.
                    </th>
                    <th>
                        Alícuota IVA
                    </th>
                    <th>
                        Costo Total Uni.
                    </th>
                </tr>
                </thead>
                <tbody>
                @foreach($articulo->detalleArticulos as $detalleArticulo)
                    <tr>
                        <td>&nbsp;&nbsp;</td>
                        <td>
                            @if($detalleArticulo->proveedor)
                                <a href="{{url('/personas/proveedor/'.$detalleArticulo->proveedor->id)}}">
                                    {{ $detalleArticulo->proveedor->razon_social }}
                                </a>
                            @else
                                Proveedor Eliminado
                            @endif
                        </td>
                        <td>
                            <b>{{ $detalleArticulo->created_at->format('d/m/Y H:i') }}</b>
                        </td>
                        <td>
                            <b class="text-success">{{ $detalleArticulo->moneda }}</b>
                        </td>
                        <td>
                            <b class="text-success">{{ $detalleArticulo->moneda->signo }} {{ number_format($detalleArticulo->costo/$detalleArticulo->cantidad, 2) }}</b>
                        </td>
                        <td>
                            <b class="text-success">
                                @if($detalleArticulo->costo > 0)
                                    <?php
                                        $iva = $detalleArticulo->importe_iva*100/$detalleArticulo->costo;
                                        switch (true) {
                                            case ($iva > 10 && $iva < 12):
                                                $iva = 10.5;
                                                break;
                                            case ($iva > 20 && $iva < 22):
                                                $iva = 21;
                                                break;
                                        }
                                    ?>
                                    {{ $iva }} %
                                @else
                                    {{ $articulo->porcentajeIva() }} %
                                @endif
                            </b>
                        </td>
                        <td>
                            <b class="text-success">{{ $detalleArticulo->moneda->signo }} {{ number_format($detalleArticulo->costo/$detalleArticulo->cantidad + $detalleArticulo->importe_iva/$detalleArticulo->cantidad, 2) }}</b>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            </div>
                </div>
                <div class="tab-pane" id="stock" role="tabpanel">
                    <br>
                    @if($stock)
                        <div class="row">
                            <div class="col-md-2 font-weight-bold">Existencia Actual</div>
                            <div class="col-md-4">
                                @if($stock->cantidad > $articulo->minimo)
                                    <span class="text-success font-weight-bold">{{ number_format($stock->cantidad, 0) }}</span>
                                @else
                                    <span class="text-danger font-weight-bold">{{ number_format($stock->cantidad, 0) }}</span>
                                @endif
                            </div>

                            <div class="col-md-2 font-weight-bold">Alarma</div>
                            <div class="col-md-4">
                                @if($stock->alarma == 1)
                                    <span class="label label-success">Activada</span>
                                @else
                                    <span class="label label-warning">Desactivada</span>
                                @endif
                            </div>
                        </div><hr>
                        <div class="row">
                            <div class="col-md-2 font-weight-bold">Límite Stock Minimo</div>
                            <div class="col-md-4">{{ number_format($stock->minimo, 0) }}</div>

                            <div class="col-md-2 font-weight-bold">Límite Stock Máximo</div>
                            <div class="col-md-4">{{ number_format($stock->maximo, 0) }}</div>
                        </div><hr>
                         <table id="verarticulos3-table" class="table  selectable table-bordered table-striped color-table form-material table-hover" style="width: 100%">
                            <thead class="thin-border-bottom">
                            <tr style="background:#99abb4;color:#fff;">
                                <th></th>
                                <th>
                                    Fecha
                                </th>
                                <th>
                                    Tipo de Movimiento
                                </th>
                                <th>
                                    Cantidad
                                </th>
                                <th>
                                    Usuario
                                </th>
                                <th>
                                    Descripción
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($stock->movimientosStock as $movimiento)
                                <tr>
                                    <td>&nbsp;&nbsp;</td>
                                    <td>{{ $movimiento->created_at->format('d/m/Y H:i') }}</td>
                                    <td>{{ $movimiento->tipoMovimiento->nombre }}</td>
                                    <td><strong>{{ number_format($movimiento->cantidad) }}</strong></td>
                                    <td> {{($movimiento->usuario) ? $movimiento->usuario : '' }} </td>
                                    <td>{{ $movimiento->descripcion }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        <h1>El articulo no lleva stock</h1>
                    @endif
                </div>
                <div class="tab-pane" id="ventas" role="tabpanel">
                     <table id="verarticulos4-table" class="table color-bordered-table selectable table-hover table-striped form-material" style="width: 100%">
                        <thead class="thin-border-bottom">
                            <tr style="background:#99abb4;color:#fff;">
                            <th></th>
                            <th>Número</th>
                            <th>Fecha</th>
                            <th>Cliente</th>
                            <th>Lista Usada</th>
                            <th>Vendedor</th>
                            <th>Total</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($ventas as $item)
                            <tr {{ ($item->estado_facturacion == 2) ? 'class=danger' : '' }}>
                                <td>&nbsp;&nbsp;</td>
                                <td>
                                    <a href="{{url('/ventas/venta/'.$item->id)}}">
                                        {{ $tipos_comprobantes[$item->tipo_comprobante_id] }} {{ $item->punto_venta }}-{{ $item->numero }}
                                    </a>
                                </td>
                                <td>{{ $item->fecha->format('d/m/Y H:i') }}</td>
                                <td>{{ $item->razon_social }}</td>
                                <td>{{ $item->lista}}</td>
                                <td>{{ isset($item->usuario) ? $item->usuario : '' }}</td>
                                <td>{{ $item->signo }} {{ $item->total_cobro }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('after-scripts')


    {{ Html::script("js/frontend/plugins/datatables/jquery.dataTables.min.js") }}
    {{ Html::script("js/frontend/plugins/datatables/dataTables.responsive.min.js") }}
    {{ Html::script("js/frontend/plugins/datatables/dataTables.select.min.js") }}

<script>
        $(document).ready(function() {
            $('#verarticulos-table').DataTable({
                "aaSorting": [1,'asc'],
                "paging":   false,
                "info":     false,
                "searching": false,
                "ordering":false,
                responsive: {
                    details: {
                        type: 'column',
                        target: 'tr'
                    }
                },
                "columnDefs": [{
                    targets: 0,
                    className: 'control',
                    orderable: false,
                    searchable: false,
                    defaultContent: '&nbsp;&nbsp;'
                },
                { responsivePriority: 1, targets: -1 },
                { responsivePriority: 1, targets: -2 },
                { responsivePriority: 1, targets: 1 }],
            });
        } );

        $(document).ready(function() {
            $('#verarticulos2-table').DataTable({
                "aaSorting": [1,'asc'],
                "paging":   false,
                "info":     false,
                "searching":   false,
                "ordering":false,
                responsive: {
                    details: {
                        type: 'column',
                        target: 'tr'
                    }
                },
                "columnDefs": [{
                    targets: 0,
                    className: 'control',
                    orderable: false,
                    searchable: false,
                    defaultContent: '&nbsp;&nbsp;'
                },
                { responsivePriority: 1, targets: -1 },
                { responsivePriority: 1, targets: -2 },
                { responsivePriority: 1, targets: 1 }],
            });
        } );

        $(document).ready(function() {
            $('#verarticulos3-table').DataTable({
                "aaSorting": [1,'asc'],
                "paging":   false,
                "info":     false,
                "searching":   false,
                "ordering":false,
                responsive: {
                    details: {
                        type: 'column',
                        target: 'tr'
                    }
                },
                "columnDefs": [{
                    targets: 0,
                    className: 'control',
                    orderable: false,
                    searchable: false,
                    defaultContent: '&nbsp;&nbsp;'
                },
                { responsivePriority: 1, targets: -1 },
                { responsivePriority: 1, targets: -2 },
                { responsivePriority: 1, targets: 1 }],
            });
        } );

        $(document).ready(function() {
            $('#verarticulos4-table').DataTable({
                "aaSorting": [1,'asc'],
                "paging":   false,
                "info":     false,
                "searching":   false,
                "ordering":false,
                responsive: {
                    details: {
                        type: 'column',
                        target: 'tr'
                    }
                },
                "columnDefs": [{
                    targets: 0,
                    className: 'control',
                    orderable: false,
                    searchable: false,
                    defaultContent: '&nbsp;&nbsp;'
                },
                { responsivePriority: 1, targets: -1 },
                { responsivePriority: 1, targets: -2 },
                { responsivePriority: 1, targets: 1 }],
            });
        } );

        $("body").on("click", ".delete", function () {
            swal({
                title: "Eliminar Articulo",
                text: "¿Realmente desea eliminar este registro?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#ff8726',
                confirmButtonText: "Eliminar"
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        type: 'POST',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: '{{ url('/articulos/articulo/'.$articulo->id) }}',
                        data: {"_method" : 'DELETE'},
                        success: function (msg) {
                            swal("Eliminado!", "El registro ha sido correctamente eliminado.", "success").then((result) => { window.location.href = '{{url('/articulos/articulo')}}'; });
                        }
                    });
                }
            });
        });

    </script>

@endsection