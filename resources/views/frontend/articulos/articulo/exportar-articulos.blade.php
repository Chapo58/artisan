<table>
    <thead>
        <tr>
            <th>Codigo Interno</th>
            <th>Nombre</th>
            <th>Modelo</th>
            <th>Codigo de Barras</th>
            <th>Costo</th>
            <th>IVA</th>
            <th>Stock</th>
            <th>Marca</th>
            <th>Rubro</th>
            <th>Subrubro</th>
        </tr>
    </thead>
    <tbody>
        @foreach($articulos as $articulo)
            <tr>
                <td>{{ $articulo->codigo }}</td>
                <td>{{ $articulo->nombre }}</td>
                <td>{{ $articulo->modelo }}</td>
                <td>{{ $articulo->codigo_barra }}</td>
                <td>{{ $articulo->ultimo_costo }}</td>
                <td>{{ $porcentajesIva[$articulo->porcentaje_iva] }}</td>
                <td>{{ ($articulo->getStock($sucursal)) ? $articulo->getStock($sucursal) : '' }}</td>
                <td>{{ $articulo->marca }}</td>
                <td>{{ $articulo->rubro }}</td>
                <td>{{ $articulo->subrubro }}</td>
            </tr>
        @endforeach
    </tbody>
</table>

