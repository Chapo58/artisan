<div class="card">
    <div class="card-body">
        <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
        <br>
        <div class="row form-group">
            <div class="col-md-6">
                {!! Form::label('nombre', 'Nombre') !!}
                {!! Form::text('nombre', null, ['class' => 'form-control', 'required' => 'required']) !!}
                {!! $errors->first('nombre', '<p class="text-danger">:message</p>') !!}
            </div>
            <div class="form-group col-md-2">
                {!! Form::label('lleva_stock', 'Lleva Stock') !!}
                <div class="switch">
                    <label>
                        {!! Form::checkbox('lleva_stock', true, isset($articulo->lleva_stock) ? $articulo->lleva_stock : true) !!}<span class="lever"></span>
                    </label>
                </div>
            </div>
            <div class="col-md-4" id="parametros_stock">
                {!! Form::label('stock', 'Stock') !!}
                <i class="fa fa-question-circle text-info mytooltip">
                    <span class="tooltip-content3">Stock actual del articulo disponible para venta</span>
                </i>
                {!! Form::number('stock', 1, ['class' => 'form-control', 'required' => 'required']) !!}
                {!! $errors->first('stock', '<p class="text-danger">:message</p>') !!}
            </div>
        </div>
        <div class="row form-group">
            <div class="col-md-6">
                {!! Form::label('ultimo_costo', 'Precio Base Neto') !!}
                <i class="fa fa-question-circle text-info mytooltip">
                    <span class="tooltip-content3">Valor neto de compra o fabricación del articulo</span>
                </i>
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-usd"></i>
                    </div>
                    {!! Form::number('ultimo_costo', '', ['class' => 'form-control', 'step' => '0.01', 'required' => 'required']) !!}
                </div>
                {!! $errors->first('ultimo_costo', '<p class="text-danger">:message</p>') !!}
            </div>
            <div class="col-md-6">
                {!! Form::label('porcentaje_iva', 'Porcentaje IVA') !!}
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-percent"></i>
                    </div>
                    {!! Form::select('porcentaje_iva', $porcentajesIva, '', ['id' => 'porcentaje_iva', 'class' => 'form-control', 'required' => 'required']) !!}
                </div>
                {!! $errors->first('porcentaje_iva', '<p class="text-danger">:message</p>') !!}
            </div>
        </div>
        <table id="tabla-precios" class="table color-bordered-table selectable table-hover table-striped form-material">
            <thead>
                <tr style="background:#99abb4;color:#fff;">
                    <th></th>
                    <th>Lista Precio</th>
                    <th>Porcentaje</th>
                    <th>Precio Neto</th>
                    <th>Precio Bruto</th>
                </tr>
            </thead>
            <tbody>
            @if(isset($listas_sin_usar))
                @foreach($listas_sin_usar as $lista)
                    <tr>
                        <td></td>
                        <td class="align-middle">
                            <span class="font-weight-bold">{{ $lista->nombre }}</span>
                            <input type="hidden" name="precios[{{ ($loop->index+1)*-1 }}][id]" value="{{ ($loop->index+1)*-1 }}">
                            <input type="hidden" name="precios[{{ ($loop->index+1)*-1 }}][lista_precio_id]" value="{{ $lista->id }}">
                        </td>
                        <td>
                            <div class="input-group">
                                <div class="left-icon">%</div>
                                {!! Form::number('precios['.(($loop->index+1)*-1).'][porcentaje_lista]', isset($lista->porcentaje) ? $lista->porcentaje : 0, ['class' => 'form-control porcentaje-lista', 'step' => '0.01']) !!}
                            </div>
                        </td>
                        <td>
                            <div class="input-group">
                                <div class="left-icon">$</div>
                                {!! Form::number('precios['.(($loop->index+1)*-1).'][precio_neto]', '', ['class' => 'precio-neto form-control', 'step' => '0.01', 'readonly']) !!}
                            </div>
                        </td>
                        <td>
                            <div class="input-group">
                                <div class="left-icon">$</div>
                                {!! Form::number('precios['.(($loop->index+1)*-1).'][precio_venta]', '', ['class' => 'precio-venta form-control', 'step' => '0.01']) !!}
                            </div>
                        </td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
        <hr>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-12">
                    {{ Form::submit(isset($submitButtonText) ? $submitButtonText : trans('buttons.general.save'), ['class' => 'btn btn-lg btn-themecolor pull-right']) }}
                </div>
            </div>
        </div>
    </div>
</div>