@extends ('frontend.layouts.app')

@section ('title', trans('labels.frontend.articulo_titulo')." - ".trans('navs.frontend.user.administration'))

@section('after-styles')
    {{ Html::style("css/frontend/plugins/datatables/responsive.dataTables.min.css") }}
@endsection

@section('content')
    <div class="row page-titles">
        <div class="col-6 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('navs.frontend.user.administration') }} {{ trans('labels.frontend.articulo_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item active">{{ trans('labels.frontend.articulo_titulo') }}</li>
            </ol>
        </div>
        <div class="col-6">
            <div class="pull-right">
                <a href="{{ url('/articulos/articulo/create') }}" class="btn btn-success" title="{{ trans('buttons.general.crud.create') }} {{ trans('labels.frontend.articulo') }}">
                    <i class="fa fa-plus" aria-hidden="true"></i>
                    <span class="hidden-xs-down">Nuevo {{ trans('labels.frontend.articulo') }}</span>
                </a>
                <a href="{{ url('/articulos/articulo/create_rapido') }}" class="btn btn-success" title="Carga Rapida">
                    <i class="fa fa-rocket" aria-hidden="true"></i>
                    <span class="hidden-xs-down">Carga Rapida</span>
                </a>
                <a href="{{ url('/articulos/articulo/plantilla') }}" target="_blank" class="btn btn-info" data-toggle="tooltip" title="Descarga una plantilla vacia de Excel en la cual podras cargar tus nuevos articulos al sistema.">
                    <i class="fa fa-file-excel-o" aria-hidden="true"></i>
                    <span class="hidden-xs-down">Plantilla</span>
                </a>
                <a href="{{ url('/articulos/articulo/exportar') }}" target="_blank" class="btn btn-primary" data-toggle="tooltip" title="Descarga en un archivo de Excel todos tus articulos con sus detalles. Puedes realizar cualquier cambio en el archivo y luego volver a importarlo al sistema para actualizar los articulos.">
                    <i class="fa fa-cloud-download" aria-hidden="true"></i>
                    <span class="hidden-xs-down">Exportar</span>
                </a>
                @if($user->email != 'demo@ciatt.com.ar')
                <button type="button" data-backdrop="static" data-toggle="modal" data-target="#modalImportarArticulos" class="btn btn-warning">
                    <div data-toggle="tooltip" title="Importa al sistema tu plantilla con nuevos articulos o tu archivo de excel exportado y modificado.">
                        <span class="fa fa-cloud-upload"></span>
                        <span class="hidden-xs-down">Importar</span>
                    </div>
                </button>
                @endif
                @if($sucursales->count() > 1)
                    <a href="{{ url('/articulos/stock/solicitud-masiva') }}" class="btn btn-primary" data-toggle="tooltip" title="Generar una solicitud de transferencia de stock de varios articulos a la vez">
                        <i class="fa fa-exchange" aria-hidden="true"></i>
                        <span class="hidden-xs-down">Solicitar de Stock</span>
                    </a>
                @endif
            </div>
        </div>
    </div>

    @tips($_SERVER['REQUEST_URI'])

    <div class="card">
        <div class="card-body">
            <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
            @include('includes.partials.messages')
            <div class="table-responsive">
                <table id="articulo-table" class="table selectable table-striped table-hover table-sm">
                    <thead>
                        <tr>
                            <th></th>
                            <th class="min-desktop">Código</th>
                            <th>{{ trans('validation.attributes.nombre') }}</th>
                            <th class="min-desktop">Modelo</th>
                            <th>Stock</th>
                            <th>Precio</th>
                            <th>{{ trans('validation.attributes.rubro_id') }}</th>
                            <th>{{ trans('validation.attributes.subrubro_id') }}</th>
                            <th>{{ trans('validation.attributes.marca_id') }}</th>
                            <th>{{ trans('labels.general.actions') }}</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    @include('frontend.articulos.articulo.importar_articulos_dialog')
    @include('frontend.articulos.stock.solicitar_stock_dialog')

@endsection

@section('after-scripts')
{{ Html::script("js/frontend/plugins/datatables/jquery.dataTables.min.js") }}
{{ Html::script("js/frontend/plugins/datatables/dataTables.responsive.min.js") }}
{{ Html::script("js/frontend/plugins/datatables/dataTables.select.min.js") }}

<script>
    $(document).ready(function() {
        $('#articulo-table').DataTable({
            "aaSorting": [1,'asc'],
            processing: true,
            serverSide: true,
            responsive: {
                details: {
                    type: 'column',
                    target: 'tr'
                }
            },
            "columnDefs": [{
                targets: 0,
                className: 'control',
                orderable: false,
                searchable: false,
                defaultContent: '&nbsp;&nbsp;'
            },
                { responsivePriority: 1, targets: -1 }],
            ajax: '{!! route('frontend.articulos.ArticulosData') !!}',
            columns: [
                {data: null, name: ''},
                {data: 'codigo', name: 'codigo'},
                {data: 'nombre', name: 'nombre'},
                {data: 'modelo', name: 'modelo'},
                {data: 'stock', name: 'stock'},
                {data: 'precio', name: 'precio'},
                {data: 'rubro', name: 'rubro'},
                {data: 'subrubro', name: 'subrubro'},
                {data: 'marca', name: 'marca'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        });
    });



    $("#form_importar").on('submit',function(event){
        $('#modalImportarArticulos').modal('toggle');

        swal({
            title: 'Importando Articulos',
            text: 'Importando Articulos... Por favor aguarde...',
            timer: 150000,
            allowOutsideClick: false,
            onOpen: () => {
                swal.showLoading()
            }
        })
    });

    $("body").on("click", ".delete", function () {
        var id = $(this).attr("data-id");
        swal({
            title: "Eliminar Articulo",
            text: "¿Realmente desea eliminar este registro?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#ff8726',
            confirmButtonText: "Eliminar"
        }).then((result) => {
            if (result.value) {
            $.ajax({
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/articulos/articulo/'+id,
                data: {"_method" : 'DELETE'},
                success: function (msg) {
                    $("#" + id).hide(1);
                    swal("Eliminado!", "El registro ha sido correctamente eliminado.", "success");
                }
            });
            }
        });
    });
</script>

@endsection
