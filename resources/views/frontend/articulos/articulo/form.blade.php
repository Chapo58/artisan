<div class="card">
    <div class="card-body p-b-0">
        <div class="card-actions">
            <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
        </div>
        <h4 class="card-title">{{ trans('buttons.general.crud.create') }} {{ trans('labels.frontend.articulo_titulo') }}</h4>
        <!-- Nav tabs -->
        <ul class="nav nav-tabs customtab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#info" role="tab">
                    <span class="hidden-sm-up"><i class="ti-info"></i></span>
                    <span class="hidden-xs-down">Información</span>
                </a>
            </li>
            @if(Auth::user()->sucursal)
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#stock" role="tab">
                        <span class="hidden-sm-up"><i class="ti-package"></i></span>
                        <span class="hidden-xs-down">Stock</span>
                    </a>
                </li>
            @endif
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#precios" role="tab">
                    <span class="hidden-sm-up"><i class="ti-money"></i></span>
                    <span class="hidden-xs-down">Precios</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#imagenes" role="tab">
                    <span class="hidden-sm-up"><i class="ti-image"></i></span>
                    <span class="hidden-xs-down">Imagenes</span>
                </a>
            </li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            <div class="tab-pane active" id="info" role="tabpanel">
                <div class="p-20">
                    <div class="row">
                        <div class="form-group col-md-6">
                            {!! Form::label('codigo', 'Código Interno *') !!}
                            <i class="fa fa-question-circle text-info mytooltip">
                                <span class="tooltip-content3">Codigo autonumerico unico para este articulo dentro del sistema</span>
                            </i>
                            {!! Form::number('codigo', $codigo, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Codigo Interno']) !!}
                            {!! $errors->first('codigo', '<p class="text-danger">:message</p>') !!}
                        </div>
                        <div class="form-group col-md-6">
                            {!! Form::label('codigo_barra', 'Código de Barras') !!}
                            {!! Form::number('codigo_barra', null, ['class' => 'form-control', 'placeholder' => 'Codigo de Barras']) !!}
                            {!! $errors->first('codigo_barra', '<p class="text-danger">:message</p>') !!}
                        </div>
                        <div class="form-group col-md-12">
                            {!! Form::label('nombre', 'Nombre *') !!}
                            {!! Form::text('nombre', null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Nombre del articulo']) !!}
                            {!! $errors->first('nombre', '<p class="text-danger">:message</p>') !!}
                        </div>
                        <div class="form-group col-md-6">
                            {!! Form::label('modelo', 'Modelo') !!}
                            {!! Form::text('modelo', null, ['class' => 'form-control', 'placeholder' => 'Modelo']) !!}
                            {!! $errors->first('modelo', '<p class="text-danger">:message</p>') !!}
                        </div>
                        <div class="form-group col-md-6">
                            {!! Form::label('modelo_secundario', 'Modelo Secundario') !!}
                            {!! Form::text('modelo_secundario', null, ['class' => 'form-control', 'placeholder' => 'Modelo Secundario']) !!}
                            {!! $errors->first('modelo_secundario', '<p class="text-danger">:message</p>') !!}
                        </div>
                        <div class="form-group col-md-6">
                            {!! Form::label('unidad_id', 'Unidad de medida') !!}
                            {!! Form::select('unidad_id', $unidades, isset($articulo->unidad) ? $articulo->unidad->id : 0, ['class' => 'form-control']) !!}
                            {!! $errors->first('unidad_id', '<p class="text-danger">:message</p>') !!}
                        </div>
                        <div class="form-group col-md-6">
                            {!! Form::label('cantidad', 'Unidades por bulto') !!}
                            {!! Form::number('cantidad', isset($articulo->cantidad) ? number_format($articulo->cantidad, 2, '.', '') : 1, ['class' => 'form-control', 'step' => '0.01']) !!}
                            {!! $errors->first('cantidad', '<p class="text-danger">:message</p>') !!}
                        </div>
                        <div class="form-group col-md-4">
                            {!! Form::label('rubro_id', 'Rubro') !!}
                            <div class="row">
                                <div class="col-md-10">
                                    {!! Form::select('rubro_id', $rubros, isset($articulo->rubro) ? $articulo->rubro->id : '', ['class' => 'form-control']) !!}
                                </div>
                                <div class="col-md-1">
                                    <button type="button" class="btn btn-secondary" id="agregar-rubro"><i class="fa fa-plus"></i> </button>
                                </div>
                            </div>
                            {!! $errors->first('rubro_id', '<p class="text-danger">:message</p>') !!}
                        </div>
                        <div class="form-group col-md-4">
                            {!! Form::label('subrubro_id', 'Subrubro') !!}
                            <div class="row">
                                <div class="col-md-10">
                                    {!! Form::select('subrubro_id', $subrubros, isset($articulo->subrubro) ? $articulo->subrubro->id : '', ['class' => 'form-control']) !!}
                                </div>
                                <div class="col-md-1">
                                    <button type="button" class="btn btn-secondary" id="agregar-subrubro"><i class="fa fa-plus"></i> </button>
                                </div>
                            </div>
                            {!! $errors->first('subrubro_id', '<p class="text-danger">:message</p>') !!}
                        </div>
                        <div class="form-group col-md-4">
                            {!! Form::label('marca_id', 'Marca') !!}
                            <div class="row">
                                <div class="col-md-10">
                                    {!! Form::select('marca_id', $marcas, isset($articulo->marca) ? $articulo->marca->id : '', ['class' => 'form-control']) !!}
                                </div>
                                <div class="col-md-1">
                                    <button type="button" class="btn btn-secondary" id="agregar-marca"><i class="fa fa-plus"></i> </button>
                                </div>
                            </div>
                            {!! $errors->first('marca_id', '<p class="text-danger">:message</p>') !!}
                        </div>
                        <div class="form-group col-md-6">
                            {!! Form::label('actividad_afip_id', 'Actividad AFIP') !!}
                            <i class="fa fa-question-circle text-info mytooltip">
                                <span class="tooltip-content3">En caso de poseer varias actividades económicas diferentes frente a afip especifique aquí a que actividad pertenece este articulo</span>
                            </i>
                            {!! Form::select('actividad_afip_id', $actividades_afip, isset($articulo->actividadAfip) ? $articulo->actividadAfip->id : '', ['class' => 'form-control']) !!}
                            {!! $errors->first('actividad_afip_id', '<p class="text-danger">:message</p>') !!}
                        </div>
                        @permiso('ecommerce',true)
                        <div class="form-group col-md-6">
                            {!! Form::label('destacado', 'Articulo Destacado') !!}
                            <i class="fa fa-question-circle text-info mytooltip">
                                <span class="tooltip-content3">En el E-Commerce, el articulo estara en la lista de destacados.</span>
                            </i>
                            <div class="switch">
                                <label>
                                    {!! Form::checkbox('destacado', true, isset($articulo->destacado) ? $articulo->destacado : false) !!}<span class="lever"></span>
                                </label>
                            </div>
                        </div>
                        @endif
                        <div class="form-group col-md-12">
                            {!! Form::label('descripcion', 'Descripción') !!}
                            {!! Form::textarea('descripcion', null, ['class' => 'form-control']) !!}
                            {!! $errors->first('descripcion', '<p class="text-danger">:message</p>') !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane p-20" id="stock" role="tabpanel">
                <div class="row">
                    <div class="form-group col-md-3">
                        {!! Form::label('lleva_stock', 'Lleva Stock') !!}
                        <div class="switch">
                            <label>
                                {!! Form::checkbox('lleva_stock', true, isset($articulo->lleva_stock) ? $articulo->lleva_stock : true) !!}<span class="lever"></span>
                            </label>
                        </div>
                    </div>
                    <div class="col-md-9" id="parametros_stock" {{(isset($articulo) && !$articulo->lleva_stock) ? 'style=display:none;' : ''}}>
                        <div class="row">
                        <div class="form-group col-md-4">
                            {!! Form::label('stock[cantidad]', 'Existencias *') !!}
                            <i class="fa fa-question-circle text-info mytooltip">
                                <span class="tooltip-content3">Stock actual del articulo disponible para venta</span>
                            </i>
                            {!! Form::number('stock[cantidad]', isset($stock) ? $stock->cantidad : 1, ['class' => 'form-control', 'required' => 'required']) !!}
                            {!! $errors->first('stock[cantidad]', '<p class="text-danger">:message</p>') !!}
                        </div>
                        <div class="form-group col-md-4">
                            {!! Form::label('stock[maximo]', 'Limite Maximo') !!}
                            <i class="fa fa-question-circle text-info mytooltip">
                                <span class="tooltip-content3">Limite maximo de stock por sucursal</span>
                            </i>
                            {!! Form::number('stock[maximo]', isset($stock) ? $stock->maximo : 100, ['class' => 'form-control']) !!}
                            {!! $errors->first('stock[maximo]', '<p class="text-danger">:message</p>') !!}
                        </div>
                        <div class="form-group col-md-4">
                            {!! Form::label('stock[minimo]', 'Limite Minimo') !!}
                            <i class="fa fa-question-circle text-info mytooltip">
                                <span class="tooltip-content3">Limite minimo de stock por sucursal</span>
                            </i>
                            {!! Form::number('stock[minimo]', isset($stock) ? $stock->minimo : 10, ['class' => 'form-control']) !!}
                            {!! $errors->first('stock[minimo]', '<p class="text-danger">:message</p>') !!}
                        </div>
                        <div class="form-group col-md-12">
                            {!! Form::label('stock[alarma]', 'Alerta de stock') !!}
                            <i class="fa fa-question-circle text-info mytooltip">
                                <span class="tooltip-content3">Alertar en caso de que las existencias del articulo sean menores a su limite mínimo o superen su limite máximo</span>
                            </i>
                            <div class="switch">
                                <label>DESACTIVADO
                                    {!! Form::checkbox('stock[alarma]', true, isset($stock) ? $stock->alarma : true) !!}<span class="lever"></span>
                                    ACTIVADO</label>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane p-20" id="precios" role="tabpanel">
                <div class="row">
                    <div class="form-group col-md-4">
                        {!! Form::label('moneda_id', 'Moneda *') !!}
                        {!! Form::select('moneda_id', $monedas, isset($articulo->moneda) ? $articulo->moneda->id : '', ['class' => 'form-control', 'required' => 'required']) !!}
                        {!! $errors->first('moneda_id', '<p class="text-danger">:message</p>') !!}
                    </div>
                    <div class="form-group col-md-4">
                        {!! Form::label('ultimo_costo', 'Precio Base Neto *') !!}
                        <i class="fa fa-question-circle text-info mytooltip">
                            <span class="tooltip-content3">Valor neto de compra o fabricación del articulo</span>
                        </i>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-usd"></i>
                            </div>
                            {!! Form::number('ultimo_costo', isset($articulo->ultimo_costo) ? $articulo->ultimo_costo : '', ['class' => 'form-control', 'step' => '0.01', 'required' => 'required']) !!}
                        </div>
                        {!! $errors->first('ultimo_costo', '<p class="text-danger">:message</p>') !!}
                    </div>
                    <div class="form-group col-md-4">
                        {!! Form::label('porcentaje_iva', 'Porcentaje IVA *') !!}
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-percent"></i>
                            </div>
                            {!! Form::select('porcentaje_iva', $porcentajesIva, isset($articulo->porcentaje_iva) ? $articulo->porcentaje_iva : '', ['id' => 'porcentaje_iva', 'class' => 'form-control', 'required' => 'required']) !!}
                        </div>
                        {!! $errors->first('porcentaje_iva', '<p class="text-danger">:message</p>') !!}
                    </div>
                    <div class="form-group col-md-6">
                        {!! Form::label('impuesto', '$ Impuestos Internos') !!}
                        <i class="fa fa-question-circle text-info mytooltip">
                            <span class="tooltip-content3">Importe de impuestos internos. Se calcula sobre la base real</span>
                        </i>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-usd"></i>
                            </div>
                            {!! Form::number('impuesto', isset($articulo->impuesto) ? $articulo->impuesto : 0, ['id' => 'impuesto', 'class' => 'form-control', 'step' => '0.01']) !!}
                        </div>
                        {!! $errors->first('impuesto', '<p class="text-danger">:message</p>') !!}
                    </div>
                    <div class="form-group col-md-6">
                        {!! Form::label('porcentaje_impuesto', '% Impuestos Internos') !!}
                        <i class="fa fa-question-circle text-info mytooltip">
                            <span class="tooltip-content3">Porcentaje de impuestos internos. Se calcula sobre la base real</span>
                        </i>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-percent"></i>
                            </div>
                            {!! Form::number('porcentaje_impuesto', (isset($articulo->ultimo_costo) && $articulo->ultimo_costo != 0) ? ($articulo->impuesto*100)/$articulo->ultimo_costo : 0, ['id' => 'porcentaje_impuesto','class' => 'form-control', 'step' => '0.01']) !!}
                        </div>
                        {!! $errors->first('porcentaje_impuesto', '<p class="text-danger">:message</p>') !!}
                    </div>
                    <table id="tabla-precios" class="table selectable color-bordered-table muted-bordered-table table-hover table-striped  col-md-12 form-material">
                        <thead>
                         <tr style="background:#99abb4;color:#fff;">
                            <th>Lista Precio</th>
                            <th>Porcentaje</th>
                            <th>Precio Neto</th>
                            <th>Precio Bruto</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(isset($articulo->precios))
                            @foreach($articulo->precios as $precio)
                                @if($precio->listaPrecio)
                                <tr>
                                    <td>&nbsp;&nbsp;</td>
                                    <td class="align-middle">
                                        <span class="font-weight-bold">{{ $precio->listaPrecio }}</span>
                                        <input type="hidden" name="precios[{{ $precio->id }}][id]" value="{{ $precio->id }}">
                                        <input type="hidden" name="precios[{{ $precio->id }}][lista_precio_id]" value="{{ $precio->listaPrecio->id }}">
                                    </td>
                                    <td>
                                        <div class="input-group">
                                            <div class="left-icon">%</div>
                                            {!! Form::number('precios['.$precio->id.'][porcentaje_lista]', number_format((($precio->precio - $articulo->ultimo_costo)/$articulo->ultimo_costo)*100, 2, '.', ''), ['class' => 'form-control porcentaje-lista', 'step' => '0.01']) !!}
                                        </div>
                                    </td>
                                    <td>
                                        <div class="input-group">
                                            <div class="left-icon">$</div>
                                            {!! Form::number('precios['.$precio->id.'][precio_neto]', number_format( $precio->precio, 2, '.', ''), ['class' => 'precio-neto form-control', 'step' => '0.01', 'readonly']) !!}
                                        </div>
                                    </td>
                                    <td>
                                        <div class="input-group">
                                            <div class="left-icon">$</div>
                                            {!! Form::number('precios['.$precio->id.'][precio_venta]', number_format($precio->precio*( ($porcentajesIva[$articulo->porcentaje_iva] / 100) + 1) + $articulo->impuesto , 2, '.', ''), ['class' => 'precio-venta form-control', 'step' => '0.01']) !!}
                                        </div>
                                    </td>
                                </tr>
                                @endif
                            @endforeach
                        @endif
                        @if(isset($listas_sin_usar))
                            @foreach($listas_sin_usar as $lista)
                                <tr>
                                    <td class="align-middle">
                                        <span class="font-weight-bold">{{ $lista->nombre }}</span>
                                        <input type="hidden" name="precios[{{ ($loop->index+1)*-1 }}][id]" value="{{ ($loop->index+1)*-1 }}">
                                        <input type="hidden" name="precios[{{ ($loop->index+1)*-1 }}][lista_precio_id]" value="{{ $lista->id }}">
                                    </td>
                                    <td>
                                        <div class="input-group">
                                            <div class="left-icon">%</div>
                                            {!! Form::number('precios['.(($loop->index+1)*-1).'][porcentaje_lista]', isset($lista->porcentaje) ? $lista->porcentaje : 0, ['class' => 'form-control porcentaje-lista', 'step' => '0.01']) !!}
                                        </div>
                                    </td>
                                    <td>
                                        <div class="input-group">
                                            <div class="left-icon">$</div>
                                            {!! Form::number('precios['.(($loop->index+1)*-1).'][precio_neto]', '', ['class' => 'precio-neto form-control', 'step' => '0.01', 'readonly']) !!}
                                        </div>
                                    </td>
                                    <td>
                                        <div class="input-group">
                                            <div class="left-icon">$</div>
                                            {!! Form::number('precios['.(($loop->index+1)*-1).'][precio_venta]', '', ['class' => 'precio-venta form-control', 'step' => '0.01']) !!}
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="tab-pane p-20" id="imagenes" role="tabpanel">
                <div class="row">
                    <div class="form-group col-md-7">
                        <div class="form-group">
                            <label>Seleccionar Imagen</label>
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <div class="input-group image-preview">
                                        <input type="text" class="form-control image-preview-filename" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
                                        <span class="input-group-btn">
                                    <!-- image-preview-clear button -->
                                    <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                                      <span class="glyphicon glyphicon-remove"></span> Limpiar
                                    </button>
                                            <!-- image-preview-input -->
                                    <div class="btn btn-default image-preview-input">
                                      <span class="glyphicon glyphicon-folder-open"></span>
                                      <span class="image-preview-input-title">Buscar</span>
                                      <input type="file" name="image" accept=".png, .jpg, .jpeg">
                                    </div>
                                </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{--<div class="col-md-12">
                        <h1 class="text-center">Cargar Imagenes</h1><br>
                        {!! csrf_field() !!}
                        <div class="form-group">
                            <div class="file-loading">
                                <input id="imagefile" type="file" name="image" accept="image/*" data-min-file-count="1" multiple>
                            </div>
                        </div>
                    </div>--}}
                </div>
            </div>
        </div>
        <hr>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-12">
                    {{ Form::submit(isset($submitButtonText) ? $submitButtonText : trans('buttons.general.save'), ['class' => 'btn btn-lg btn-themecolor pull-right']) }}
                </div>
                <div class="col-md-6"> </div>
            </div>
        </div>
    </div>
</div>

@include('frontend.articulos.rubro.rubro_dialog')
@include('frontend.articulos.subrubro.subrubro_dialog')
@include('frontend.articulos.marca.marca_dialog')

