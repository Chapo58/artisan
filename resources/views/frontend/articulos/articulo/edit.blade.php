@extends('frontend.layouts.app')

@section ('title', trans('labels.frontend.articulo_titulo')." - ".trans('buttons.general.crud.edit'))

@section('after-styles')
    {{ Html::style("css/frontend/plugins/datatables/responsive.dataTables.min.css") }}
@endsection

@section('content')
    <div class="row page-titles">
        <div class="col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('navs.frontend.user.administration') }} {{ trans('labels.frontend.articulo_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item"><a href="{{url('/articulos/articulo')}}">{{ trans('labels.frontend.articulo_titulo') }}</a></li>
                <li class="breadcrumb-item">{{ trans('buttons.general.crud.edit') }} {{ trans('labels.frontend.articulo_titulo') }}</li>
                <li class="breadcrumb-item active">{{ $articulo->codigo }}# {{ $articulo }}</li>
            </ol>
        </div>
        <div class="col-4">
            <div class="pull-right">
                <a href="{{ url('/articulos/articulo') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans('buttons.general.crud.back') }}</span>
                </a>
            </div>
        </div>
    </div>

    @include('includes.partials.messages')



    {!! Form::model($articulo, [
        'method' => 'PATCH',
        'url' => ['/articulos/articulo', $articulo->id],
        'class' => 'form-horizontal',
        'files' => true
    ]) !!}

        @include ('frontend.articulos.articulo.form', ['submitButtonText' => trans("buttons.general.crud.update")])

    {!! Form::close() !!}

@endsection

    {{ Html::script("js/frontend/plugins/datatables/jquery.dataTables.min.js") }}
    {{ Html::script("js/frontend/plugins/datatables/dataTables.responsive.min.js") }}
    {{ Html::script("js/frontend/plugins/datatables/dataTables.select.min.js") }}


@section('after-scripts')
    <script>

        $(document).ready(function() {
            $('#tabla-precios tbody tr').DataTable({
                "aaSorting": [1,'asc'],
                
                "info":     false,
                "searching":   false,
                
                responsive: {
                    details: {
                        type: 'column',
                        target: 'tr'
                    }
                },
                "columnDefs": [{
                    targets: 0,
                    className: 'control',
                    orderable: false,
                    searchable: false,
                    defaultContent: '&nbsp;&nbsp;'
                },
                { responsivePriority: 1, targets: -1 },
                { responsivePriority: 1, targets: -2 },
                { responsivePriority: 1, targets: 1 }],
            });
        } );

        $( document ).ready(function() {
            actualizarPreciosListas();
        });

        $('input#codigo_barra').keypress(function(event){
            if (event.keyCode === 10 || event.keyCode === 13){
                event.preventDefault();
            }
        });

        $(document).on('change', 'input#lleva_stock', function () {
            if($('input#lleva_stock').is(':checked')){
                $('#parametros_stock').show();
            } else {
                $('#parametros_stock').hide();
            }
        });

        $('select#porcentaje_iva').on('change', function () {
            actualizarPreciosListas();
        });

        $(document).on('change', 'input#ultimo_costo', function () {
            $('input#ultimo_costo').val(this.value);
            cambiarImpuesto();
            actualizarPreciosListas();
        });

        $(document).on('change', 'input#impuesto', function () {
            $('input#impuesto').val(this.value);
            cambiarPorcentajeImpuesto();
            actualizarPreciosListas();
        });

        $(document).on('change', 'input#porcentaje_impuesto', function () {
            $('input#porcentaje_impuesto').val(this.value);
            cambiarImpuesto();
            actualizarPreciosListas();
        });

        $(document).on('change', 'input.porcentaje-lista', function () {
            var iva = parseFloat($('select#porcentaje_iva option:selected').text());
            var costo = parseFloat($('input#ultimo_costo').val());

            if(isNaN(costo) || costo == 0 || isNaN(iva)){
                swal("Debe definir un costo y un porcentaje de iva al articulo");
            } else {
                actualizarPreciosListas();
            }
        });

        function cambiarImpuesto() {
            var costo = parseFloat($('input#ultimo_costo').val());
            var porcentaje_impuesto = parseFloat($('input#porcentaje_impuesto').val());

            var impuesto = (costo * porcentaje_impuesto / 100).toFixed(2);

            $('input#impuesto').val(impuesto);
        }

        function cambiarPorcentajeImpuesto() {
            var costo = parseFloat($('input#ultimo_costo').val());
            var impuesto = parseFloat($('input#impuesto').val());
            if(costo != 0){
                var porcentaje_impuesto = (impuesto * 100 / costo).toFixed(2);
                $('input#porcentaje_impuesto').val(porcentaje_impuesto);
            }
        }

        function actualizarPreciosListas(){
            var $filasTablaPrecios = $("#tabla-precios tbody tr");
            $filasTablaPrecios.each(function (index) {
                var $filaPrecio = $(this);

                var iva = parseFloat($('select#porcentaje_iva option:selected').text());
                var porLista = parseFloat($filaPrecio.find('.porcentaje-lista').val());
                var costo = parseFloat($('input#ultimo_costo').val());
                var impuesto = parseFloat($('input#impuesto').val());

                if(!isNaN(costo) && costo != 0 && !isNaN(iva)){
                    var precioNeto = (costo * porLista)/100 + costo;
                    $filaPrecio.find('.precio-neto').val(precioNeto.toFixed(2));

                    var precioVenta = (precioNeto * iva)/100 + precioNeto + impuesto;
                    $filaPrecio.find('.precio-venta').val(precioVenta.toFixed(2));
                }

            });
        }
    </script>

    @yield('scripts-dialog-rubro')
    @yield('scripts-dialog-subrubro')
    @yield('scripts-dialog-marca')
@endsection