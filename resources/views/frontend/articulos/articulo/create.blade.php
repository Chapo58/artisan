@extends('frontend.layouts.app')

@section ('title', trans('labels.frontend.articulo_titulo')." - ".trans('buttons.general.crud.create'))

@section('after-styles')
    {{ Html::style("css/frontend/plugins/dropzone/dropzone.css") }}
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.9/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    <div class="row page-titles">
        <div class="col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('navs.frontend.user.administration') }} {{ trans('labels.frontend.articulo_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item"><a href="{{url('/articulos/articulo')}}">{{ trans('labels.frontend.articulo_titulo') }}</a></li>
                <li class="breadcrumb-item active">{{ trans('buttons.general.crud.create') }} {{ trans('labels.frontend.articulo_titulo') }}</li>
            </ol>
        </div>
        <div class="col-4">
            <div class="pull-right">
                <a href="{{ url('/articulos/articulo') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans('buttons.general.crud.back') }}</span>
                </a>
            </div>
        </div>
    </div>

    @tips($_SERVER['REQUEST_URI'])

    @include('includes.partials.messages')

    {!! Form::open(['url' => '/articulos/articulo', 'class' => 'form-horizontal', 'files' => true]) !!}
        @include ('frontend.articulos.articulo.form')
    {!! Form::close() !!}

    {{--<div class="card">
        <div class="card-body">
            <form action="#" class="dropzone">
                <div class="fallback">
                    <input name="file" type="file" multiple />
                </div>
            </form>
        </div>
    </div>--}}

@endsection

@section('after-scripts')
    {{ Html::script("js/frontend/plugins/dropzone/dropzone.js") }}

    <!-- piexif.min.js is needed for auto orienting image files OR when restoring exif data in resized images and when you
        wish to resize images before upload. This must be loaded before fileinput.min.js -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.9/js/plugins/piexif.min.js" type="text/javascript"></script>
    <!-- sortable.min.js is only needed if you wish to sort / rearrange files in initial preview.
        This must be loaded before fileinput.min.js -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.9/js/plugins/sortable.min.js" type="text/javascript"></script>
    <!-- purify.min.js is only needed if you wish to purify HTML content in your preview for
        HTML files. This must be loaded before fileinput.min.js -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.9/js/plugins/purify.min.js" type="text/javascript"></script>
    <!-- popper.min.js below is needed if you use bootstrap 4.x. You can also use the bootstrap js
       3.3.x versions without popper.min.js. -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
    <!-- the main fileinput plugin file -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.9/js/fileinput.min.js"></script>
    <!-- optionally if you need a theme like font awesome theme you can include it as mentioned below -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.9/themes/fa/theme.js"></script>
    <!-- optionally if you need translation for your language then include  locale file as mentioned below -->
¿

    <script>

        $('#imagefile').fileinput({
            theme: 'fa',
            uploadUrl: "{!! route('frontend.articulos.UploadImages') !!}",
            uploadExtraData: function() {
                return {
                    _token: "{{ csrf_token() }}",
                };
            },
            allowedFileExtensions: ['jpg', 'png', 'gif','jpeg'],
            overwriteInitial: false,
            maxFileSize:2048,
            maxFilesNum: 10
        });

        $( document ).ready(function() {
            actualizarPreciosListas();
        });

        $('input#codigo_barra').keypress(function(event){
            if (event.keyCode === 10 || event.keyCode === 13){
                event.preventDefault();
            }
        });

        $(document).on('change', 'input#lleva_stock', function () {
            if($('input#lleva_stock').is(':checked')){
                $('#parametros_stock').show();
            } else {
                $('#parametros_stock').hide();
            }
        });

        $('select#porcentaje_iva').on('change', function () {
            actualizarPreciosListas();
        });

        $(document).on('change', 'input#ultimo_costo', function () {
            $('input#ultimo_costo').val(this.value);
            cambiarImpuesto();
            actualizarPreciosListas();
        });

        $(document).on('change', 'input#impuesto', function () {
            $('input#impuesto').val(this.value);
            cambiarPorcentajeImpuesto();
            actualizarPreciosListas();
        });

        $(document).on('change', 'input#porcentaje_impuesto', function () {
            $('input#porcentaje_impuesto').val(this.value);
            cambiarImpuesto();
            actualizarPreciosListas();
        });

        $(document).on('change', 'input.porcentaje-lista', function () {
            var iva = parseFloat($('select#porcentaje_iva option:selected').text());
            var costo = parseFloat($('input#ultimo_costo').val());

            if(isNaN(costo) || costo == 0 || isNaN(iva)){
                swal("Debe definir un costo y un porcentaje de iva al articulo");
            } else {
                actualizarPreciosListas();
            }
        });

        $(document).on('change', 'input.precio-venta', function () {
            var iva = parseFloat($('select#porcentaje_iva option:selected').text());
            var costo = parseFloat($('input#ultimo_costo').val());

            if(isNaN(costo) || costo == 0 || isNaN(iva)){
                swal("Debe definir un costo y un porcentaje de iva al articulo");
            } else {
                var iva = parseFloat($('select#porcentaje_iva option:selected').text());
                var costo = parseFloat($('input#ultimo_costo').val());

                var precioNeto = $(this).val() / ((iva / 100) + 1);
                var porLista = ((precioNeto - costo)/costo)*100;

                $(this).closest('tr').find('.precio-neto').val(precioNeto.toFixed(2));
                $(this).closest('tr').find('.porcentaje-lista').val(porLista.toFixed(2));
            }
        });

        function cambiarImpuesto() {
            var costo = parseFloat($('input#ultimo_costo').val());
            var porcentaje_impuesto = parseFloat($('input#porcentaje_impuesto').val());

            var impuesto = (costo * porcentaje_impuesto / 100).toFixed(2);

            $('input#impuesto').val(impuesto);
        }

        function cambiarPorcentajeImpuesto() {
            var costo = parseFloat($('input#ultimo_costo').val());
            var impuesto = parseFloat($('input#impuesto').val());
            if(costo != 0){
                var porcentaje_impuesto = (impuesto * 100 / costo).toFixed(2);
                $('input#porcentaje_impuesto').val(porcentaje_impuesto);
            }
        }

        function actualizarPreciosListas(){
            var $filasTablaPrecios = $("#tabla-precios tbody tr");
            $filasTablaPrecios.each(function (index) {
                var $filaPrecio = $(this);

                var iva = parseFloat($('select#porcentaje_iva option:selected').text());
                var porLista = parseFloat($filaPrecio.find('.porcentaje-lista').val());
                var costo = parseFloat($('input#ultimo_costo').val());
                var impuesto = parseFloat($('input#impuesto').val());

                if(!isNaN(costo) && costo != 0 && !isNaN(iva)){
                    var precioNeto = (costo * porLista)/100 + costo;
                    $filaPrecio.find('.precio-neto').val(precioNeto.toFixed(2));

                    var precioVenta = (precioNeto * iva)/100 + precioNeto + impuesto;
                    $filaPrecio.find('.precio-venta').val(precioVenta.toFixed(2));
                }
            });
        }
    </script>

    @yield('scripts-dialog-rubro')
    @yield('scripts-dialog-subrubro')
    @yield('scripts-dialog-marca')
@endsection