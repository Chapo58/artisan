<div class="modal fade" id="modal-marca-nueva" tabindex="-1" role="dialog" aria-labelledby="marcaModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="marcaModalLabel">Nueva Marca</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    {!! Form::label('nombre_marca', 'Nombre') !!}
                    {!! Form::text('nombre_marca', '',['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="modal-footer">
                <button id="guardar-marca" type="button" class="btn btn-themecolor">Guardar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>

@section('scripts-dialog-marca')
    <script>
        $("#agregar-marca").click(function(){
            $('input#nombre_marca').parent().removeClass('has-error');
            $('input#nombre_marca').val('');
            $('#modal-marca-nueva').modal();
        });

        $("#guardar-marca").click(function(){
            $('input#nombre_marca').parent().removeClass('has-error');
            if($('input#nombre_marca').val() == ''){
                $('input#nombre_marca').parent().addClass('has-error');
                $('input#nombre_marca').focus();
            }else{
                $('#modal-marca-nueva').modal('hide');
                guardarNuevaMarca();
            }
        });

        function guardarNuevaMarca() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $( "input[name*='_token']" ).val()
                }
            });
            var nombre = $('input#nombre_marca').val();
            $.ajax({
                type: "POST",
                url: '{{ url("/articulos/marca/guardarNuevaMarca") }}',
                data: {nombre: nombre},
                success: function( msg ) {
                    $('select#marca_id').append($('<option>', {
                        value: msg.id,
                        text: msg.nombre
                    }));
                    $('select#marca_id').val(msg.id);
                }
            });
        }
    </script>
@endsection