<div class="card">
    <div class="card-body">
        <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
        <br>
        <div class="row form-group">
            <div class="col-md-12">
                {!! Form::label('nombre', trans('validation.attributes.nombre')) !!}
                {!! Form::text('nombre', null, ['class' => 'form-control', 'required' => 'required']) !!}
                {!! $errors->first('nombre', '<p class="text-danger">:message</p>') !!}
            </div>
        </div>
        <div class="row form-group">
            <div class="col-md-12">
                {!! Form::label('descripcion', trans('validation.attributes.descripcion')) !!}
                {!! Form::textarea('descripcion', null, ['class' => 'form-control']) !!}
                {!! $errors->first('descripcion', '<p class="text-danger">:message</p>') !!}
            </div>
        </div>
        <div class="row form-group">
            <div class="form-group col-md-7">
                <div class="form-group">
                    <label>Seleccionar Imagen (Tamaño recomendado: <i class="font-weight-bold">170 x 98 px</i>)</label>
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="input-group image-preview">
                                <input type="text" class="form-control image-preview-filename" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
                                <span class="input-group-btn">
                                    <!-- image-preview-clear button -->
                                    <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                                      <span class="glyphicon glyphicon-remove"></span> Limpiar
                                    </button>
                                    <!-- image-preview-input -->
                                    <div class="btn btn-default image-preview-input">
                                      <span class="glyphicon glyphicon-folder-open"></span>
                                      <span class="image-preview-input-title">Buscar</span>
                                      <input type="file" name="image" accept=".png, .jpg, .jpeg">
                                    </div>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @if(isset($marca->imagen_url))
                <div class="col-md-3">
                    {!! Form::label('imagen_url', 'Imagen Actual') !!}
                    <img src="{{ url($marca->imagen_url) }}" alt="{{ $marca }}" width="300" style="border: 2px solid lightslategrey;">
                </div>
            @endif
        </div>
        <hr>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-12">
                    {{ Form::submit(isset($submitButtonText) ? $submitButtonText : trans('buttons.general.save'), ['class' => 'btn btn-lg btn-themecolor pull-right']) }}
                </div>
            </div>
        </div>
    </div>
</div>
