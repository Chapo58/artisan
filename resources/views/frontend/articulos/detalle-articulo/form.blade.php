<div class="form-group {{ $errors->has('costo') ? 'has-error' : ''}}">
    {!! Form::label('costo', 'Costo', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('costo', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('costo', '<p class="text-danger">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('descuento') ? 'has-error' : ''}}">
    {!! Form::label('descuento', 'Descuento', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('descuento', null, ['class' => 'form-control']) !!}
        {!! $errors->first('descuento', '<p class="text-danger">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('redondeo') ? 'has-error' : ''}}">
    {!! Form::label('redondeo', 'Redondeo', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('redondeo', null, ['class' => 'form-control']) !!}
        {!! $errors->first('redondeo', '<p class="text-danger">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('iva') ? 'has-error' : ''}}">
    {!! Form::label('iva', 'Iva', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('iva', null, ['class' => 'form-control']) !!}
        {!! $errors->first('iva', '<p class="text-danger">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('cantidad') ? 'has-error' : ''}}">
    {!! Form::label('cantidad', 'Cantidad', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('cantidad', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('cantidad', '<p class="text-danger">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('proveedor_id') ? 'has-error' : ''}}">
    {!! Form::label('proveedor_id', 'Proveedor Id', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('proveedor_id', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('proveedor_id', '<p class="text-danger">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('articulo_id') ? 'has-error' : ''}}">
    {!! Form::label('articulo_id', 'Articulo Id', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('articulo_id', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('articulo_id', '<p class="text-danger">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('unidad_id') ? 'has-error' : ''}}">
    {!! Form::label('unidad_id', 'Unidad Id', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('unidad_id', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('unidad_id', '<p class="text-danger">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {{ Form::submit(isset($submitButtonText) ? $submitButtonText : trans('buttons.general.save'), array('class' => 'btn btn-primary')) }}
    </div>
</div>
