@extends('frontend.layouts.app')

@section ('title', trans('labels.frontend.detallearticulo')." - ".trans('buttons.general.crud.show'))

@section('content')
    <div class="main-content">
        <div class="main-content-inner">
            @include ('frontend.includes.breadcrumbs')

            <div class="page-header">
                <h1>
                    {{ trans('labels.frontend.detallearticulo') }}
                    <small>
                        <i class="ace-icon fa fa-angle-double-right"></i>
                        <small>{{ trans('buttons.general.crud.show') }} DetalleArticulo</small>
                        <small><i class="ace-icon fa fa-angle-double-right"></i></small>
                        <small>{{ $detallearticulo->id }}</small>
                    </small>

                    <div class="box-tools pull-right">
                        <a href="{{ url('/articulos/detalle-articulo') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning btn-sm">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> {{ trans('buttons.general.crud.back') }}
                        </a>
                        <a href="{{ url('/articulos/detalle-articulo/' . $detallearticulo->id . '/edit') }}" title="{{ trans('buttons.general.crud.edit') }}" class="btn btn-primary btn-sm">
                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i> {{ trans('buttons.general.crud.edit') }}
                        </a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['articulos/detallearticulo', $detallearticulo->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> '.trans("buttons.general.crud.delete"), array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-sm',
                                    'title' => trans("buttons.general.crud.delete")." ".trans("labels.frontend.detallearticulo"),
                                    'onclick'=>'return confirm("Confirm delete?")'
                            ))!!}
                        {!! Form::close() !!}
                    </div>
                </h1>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-borderless">
                        <tbody>
                            <tr>
                                <th>ID</th><td>{{ $detallearticulo->id }}</td>
                            </tr>
                            <tr><th> Costo </th><td> {{ $detallearticulo->costo }} </td></tr><tr><th> Descuento </th><td> {{ $detallearticulo->descuento }} </td></tr><tr><th> Redondeo </th><td> {{ $detallearticulo->redondeo }} </td></tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
