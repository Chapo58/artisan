<div class="modal fade" id="modal-subrubro-nuevo" tabindex="-1" role="dialog" aria-labelledby="subrubroModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="subrubroModalLabel">Nuevo Subrubro</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    {!! Form::label('nombre_subrubro', 'Nombre') !!}
                    {!! Form::text('nombre_subrubro', '',['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="modal-footer">
                <button id="guardar-subrubro" type="button" class="btn btn-themecolor">Guardar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>

@section('scripts-dialog-subrubro')
    <script>
        $("#agregar-subrubro").click(function(){
            $('input#nombre_subrubro').parent().removeClass('has-error');
            $('input#nombre_subrubro').val('');
            $('#modal-subrubro-nuevo').modal();
        });

        $("#guardar-subrubro").click(function(){
            $('input#nombre_subrubro').parent().removeClass('has-error');
            if($('input#nombre_subrubro').val() == ''){
                $('input#nombre_subrubro').parent().addClass('has-error');
                $('input#nombre_subrubro').focus();
            }else{
                $('#modal-subrubro-nuevo').modal('hide');
                guardarNuevoSubrubro();
            }
        });

        function guardarNuevoSubrubro() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $( "input[name*='_token']" ).val()
                }
            });
            var nombre = $('input#nombre_subrubro').val();
            var rubro_id = $('select#rubro_id option:selected').val()
            $.ajax({
                type: "POST",
                url: '{{ url("/articulos/subrubro/guardarNuevoSubrubro") }}',
                data: {nombre: nombre, rubro_id: rubro_id},
                success: function( msg ) {
                    $('select#subrubro_id').append($('<option>', {
                        value: msg.id,
                        text: msg.nombre
                    }));
                    $('select#subrubro_id').val(msg.id);
                }
            });
        }
    </script>
@endsection