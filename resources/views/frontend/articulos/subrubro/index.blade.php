@extends ('frontend.layouts.app')

@section ('title', trans('labels.frontend.subrubro_titulo')." - ".trans('navs.frontend.user.administration'))

@section('after-styles')
    {{ Html::style("css/frontend/plugins/jquery/jquery-ui.min.css") }}
@endsection

@section('content')
    <div class="row page-titles">
        <div class="col-6 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('navs.frontend.user.administration') }} {{ trans('labels.frontend.subrubro_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item active">{{ trans('labels.frontend.subrubro_titulo') }}</li>
            </ol>
        </div>
        <div class="col-6">
            <div class="pull-right">
                <a href="{{ url('/articulos/subrubro/create') }}" class="btn btn-themecolor btn-lg" title="{{ trans('buttons.general.crud.create').' '.trans('labels.frontend.subrubro')  }}">
                    <i class="fa fa-plus" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans('buttons.general.crud.create').' '.trans('labels.frontend.subrubro') }}</span>
                </a>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
            @include('includes.partials.messages')
            <div class="table-responsive">
                <table id="subrubro-table" class="table table-striped table-hover table-sm">
                    <thead>
                    <tr>
                        <th>{{ trans('validation.attributes.nombre') }}</th>
                        <th>{{ trans('validation.attributes.descripcion') }}</th>
                        <th>{{ trans('validation.attributes.rubro_id') }}</th>
                        <th>{{ trans('labels.general.actions') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($subrubro as $item)
                        <tr id="{{$item->id}}">
                            <td>{{ $item->nombre }}</td>
                            <td>{{ $item->descripcion }}</td>
                            <td>{{ isset($item->rubro) ? $item->rubro->nombre: 'Ninguno' }}</td>
                            <td>
                                <a href="{{ url('/articulos/subrubro/' . $item->id) }}" data-toggle="tooltip" data-original-title="{{ trans('buttons.general.crud.show') }} {{ trans('labels.frontend.subrubro') }}">
                                    <i class="fa fa-eye text-success m-r-10"></i>
                                </a>
                                <a href="{{ url('/articulos/subrubro/' . $item->id . '/edit') }}" data-toggle="tooltip" data-original-title="{{ trans('buttons.general.crud.edit') }} {{ trans('labels.frontend.subrubro') }}">
                                    <i class="fa fa-pencil text-info m-r-10"></i>
                                </a>
                                <a href="javascript:void(0)" class="delete" data-id="{{$item->id}}" data-toggle="tooltip" data-original-title="{{trans("buttons.general.crud.delete")." ".trans("labels.frontend.subrubro")}}">
                                    <i class="fa fa-close text-danger"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection

@section('after-scripts')
    {{ Html::script("js/frontend/plugins/datatables/jquery.dataTables.min.js") }}
    {{ Html::script("js/frontend/plugins/jquery/jquery-ui.min.js") }}

<script>
    $(document).ready(function() {
        $('#subrubro-table').DataTable();
    } );

    $('#searchname').autocomplete({
        source:'{{ url('/articulos/subrubro/autocomplete') }}',
        minlength:1,
        autoFocus:true,
        select:function(e,ui) {
            $('#searchname').val(ui.item.value);
        }
    });

    $("body").on("click", ".delete", function () {
        var id = $(this).attr("data-id");
        swal({
            title: "Eliminar Subrubro",
            text: "¿Realmente desea eliminar este registro?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#ff8726',
            confirmButtonText: "Eliminar"
        }).then((result) => {
            if (result.value) {
            $.ajax({
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/articulos/subrubro/'+id,
                data: {"_method" : 'DELETE'},
                success: function (msg) {
                    $("#" + id).hide(1);
                    swal("Eliminado!", "El registro ha sido correctamente eliminado.", "success");
                }
            });
        }
    });
    });
</script>

@endsection