<div class="card">
    <div class="card-body">
        <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
        <br>
        <div class="row form-group">
            <div class="col-md-6">
                {!! Form::label('nombre', trans('validation.attributes.nombre')) !!}
                {!! Form::text('nombre', null, ['class' => 'form-control', 'required' => 'required']) !!}
                {!! $errors->first('nombre', '<p class="text-danger">:message</p>') !!}
            </div>
            <div class="col-md-6">
                {!! Form::label('rubro_id', trans('validation.attributes.rubro_id')) !!}
                {!! Form::select('rubro_id', $rubros, isset($subrubro->rubro_id) ? $subrubro->rubro_id : 0, array('class' => 'form-control')) !!}
                {!! $errors->first('rubro_id', '<p class="text-danger">:message</p>') !!}
            </div>
        </div>
        <div class="row form-group">
            <div class="col-md-12">
                {!! Form::label('descripcion', trans('validation.attributes.descripcion')) !!}
                {!! Form::textarea('descripcion', null, ['class' => 'form-control']) !!}
                {!! $errors->first('descripcion', '<p class="text-danger">:message</p>') !!}
            </div>
        </div>
        <hr>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-12">
                    {{ Form::submit(isset($submitButtonText) ? $submitButtonText : trans('buttons.general.save'), ['class' => 'btn btn-lg btn-themecolor pull-right']) }}
                </div>
            </div>
        </div>
    </div>
</div>