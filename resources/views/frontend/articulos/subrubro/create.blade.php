@extends('frontend.layouts.app')

@section ('title', trans('labels.frontend.subrubro_titulo')." - ".trans('buttons.general.crud.create'))

@section('content')
    <div class="row page-titles">
        <div class="col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('navs.frontend.user.administration') }} {{ trans('labels.frontend.subrubro_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item"><a href="{{url('/articulos/subrubro')}}">{{ trans('labels.frontend.subrubro_titulo') }}</a></li>
                <li class="breadcrumb-item active">{{ trans('buttons.general.crud.create') }} {{ trans('labels.frontend.subrubro') }}</li>
            </ol>
        </div>
        <div class="col-4">
            <div class="pull-right">
                <a href="{{ url('/articulos/subrubro') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans('buttons.general.crud.back') }}</span>
                </a>
            </div>
        </div>
    </div>

    @include('includes.partials.messages')

    {!! Form::open(['url' => '/articulos/subrubro', 'class' => 'form-horizontal', 'files' => true]) !!}
        @include ('frontend.articulos.subrubro.form')
    {!! Form::close() !!}

@endsection
