@extends('frontend.layouts.app')

@section ('title', trans('labels.frontend.rubro_titulo')." - ".trans('buttons.general.crud.show'))

@section('after-styles')
    {{ Html::style("css/frontend/plugins/datatables/responsive.dataTables.min.css") }}
@endsection

@section('content')
    <div class="row page-titles">
        <div class="col-6 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('navs.frontend.user.administration') }} {{ trans('labels.frontend.rubro_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item"><a href="{{url('/articulos/rubro')}}">{{ trans('labels.frontend.rubro_titulo') }}</a></li>
                <li class="breadcrumb-item">{{ trans('buttons.general.crud.show') }} {{ trans('labels.frontend.rubro') }}</li>
                <li class="breadcrumb-item active">{{ $rubro }}</li>
            </ol>
        </div>
        <div class="col-6">
            <div class="pull-right">
                <a href="{{ url('/articulos/rubro') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans('buttons.general.crud.back') }}</span>
                </a>
                <a href="{{ url('/articulos/rubro/' . $rubro->id . '/edit') }}" title="{{ trans('buttons.general.crud.edit') }}" class="btn btn-info">
                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans('buttons.general.crud.edit') }}</span>
                </a>
                <a href="javascript:void(0)" title="{{trans("buttons.general.crud.delete")." ".trans("labels.frontend.rubro")}}" class="btn btn-danger delete">
                    <i class="fa fa-trash" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans("buttons.general.crud.delete") }}</span>
                </a>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
            <ul class="nav nav-tabs profile-tab" role="tablist">
                <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#info" role="tab">Información</a> </li>
                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#subrubros" role="tab">Subrubros</a> </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="info" role="tabpanel">
            <h4 class="font-medium m-t-30">{{ $rubro }}</h4>
            <hr>
            <div class="row">
                <div class="col-md-3 font-weight-bold">Nombre</div>
                <div class="col-md-9">{{ $rubro->nombre }}</div>
            </div><hr>
            <div class="row">
                <div class="col-md-3 font-weight-bold">Descripción</div>
                <div class="col-md-9">{{ $rubro->descripcion }}</div>
            </div><hr>
            <div class="row">
                <div class="col-md-3 font-weight-bold">Imagen</div>
                @if(isset($rubro->imagen_url))
                    <div class="col-md-9"><img src="{{ url($rubro->imagen_url) }}" alt="{{ isset($rubro->titulo) }}" width="500" height="100" style="border: 2px solid lightslategrey;"></div>
                @endif
            </div><hr>
            <div class="row">
                <div class="col-md-3 font-weight-bold">Creado el</div>
                <div class="col-md-9">{{ $rubro->created_at->format('d/m/Y H:m') }}</div>
            </div><hr>
            <div class="row">
                <div class="col-md-3 font-weight-bold">Ultima Modificación</div>
                <div class="col-md-9">{{ $rubro->updated_at->format('d/m/Y H:m') }}</div>
            </div>

                </div>
                <div class="tab-pane" id="subrubros" role="tabpanel">

            <div class="m-t-30">
                <a href="{{ url('/articulos/subrubro/create/'.$rubro->id) }}" class="btn btn-themecolor btn-lg" title="{{ trans('buttons.general.crud.create').' '.trans('labels.frontend.subrubro')  }}">
                    <i class="fa fa-plus" aria-hidden="true"></i> Agregar {{ trans('labels.frontend.subrubro') }}
                </a>
            </div>
            <hr>
            <table class="table color-bordered-table muted-bordered-table table-hover table-striped">
                <thead class="thin-border-bottom">
                    <tr>
                        <th>{{ trans('validation.attributes.nombre') }}</th>
                        <th>{{ trans('validation.attributes.descripcion') }}</th>
                        <th>{{ trans('labels.general.actions') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($subRubros as $item)
                        <tr id="subrubro-{{$item->id}}">
                            <td>{{ $item->nombre }}</td>
                            <td>{{ $item->descripcion }}</td>
                            <td>
                                <a href="{{ url('/articulos/subrubro/' . $item->id) }}" class="mytooltip">
                                    <i class="fa fa-eye text-success m-r-10"></i><span class="tooltip-content3">Ver Subrubro</span>
                                </a>
                                <a href="{{ url('/articulos/subrubro/' . $item->id . '/edit') }}" class="mytooltip">
                                    <i class="fa fa-pencil text-info m-r-10"></i><span class="tooltip-content3">Modificar Subrubro</span>
                                </a>
                                <a class="mytooltip delete-subrubro pointer" data-id="{{$item->id}}">
                                    <i class="fa fa-close text-danger"></i><span class="tooltip-content3">Eliminar Subrubro</span>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('after-scripts')

<script>
    $("body").on("click", ".delete", function () {
        swal({
            title: "Eliminar Rubro",
            text: "¿Realmente desea eliminar este registro?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#ff8726',
            confirmButtonText: "Eliminar"
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '{{ url('/articulos/rubro/'.$rubro->id) }}',
                    data: {"_method" : 'DELETE'},
                    success: function (msg) {
                        swal("Eliminado!", "El registro ha sido correctamente eliminado.", "success").then((result) => { window.location.href = '{{url('/articulos/rubro')}}'; });
                    }
                });
            }
        });
    });

    $("body").on("click", ".delete-subrubro", function () {
        var id = $(this).attr("data-id");
        swal({
            title: "Eliminar Subrubro",
            text: "¿Realmente desea eliminar este registro?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#ff8726',
            confirmButtonText: "Eliminar"
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/articulos/subrubro/'+id,
                    data: {"_method" : 'DELETE'},
                    success: function (msg) {
                        $("#subrubro-" + id).hide(1);
                        swal("Eliminado!", "El registro ha sido correctamente eliminado.", "success");
                    }
                });
            }
        });
    });
</script>

@endsection
