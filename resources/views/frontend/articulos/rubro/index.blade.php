@extends ('frontend.layouts.app')

@section ('title', trans('labels.frontend.rubro_titulo')." - ".trans('navs.frontend.user.administration'))

@section('content')
    <div class="row page-titles">
        <div class="col-6 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('navs.frontend.user.administration') }} {{ trans('labels.frontend.rubro_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item active">{{ trans('labels.frontend.rubro_titulo') }}</li>
            </ol>
        </div>
        <div class="col-6">
            <div class="pull-right">
                <a href="{{ url('/articulos/rubro/create') }}" class="btn btn-themecolor btn-lg" title="{{ trans('buttons.general.crud.create') }} Rubro">
                    <i class="fa fa-plus" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans('buttons.general.crud.create2') }} Rubro</span>
                </a>
            </div>
        </div>
    </div>

    @tips($_SERVER['REQUEST_URI'])

    <div class="card">
        <div class="card-body">
            <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
            @include('includes.partials.messages')
            <div class="table-responsive">
                <table id="rubro-table" class="table table-striped table-hover table-sm">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>{{ trans('validation.attributes.nombre') }}</th>
                            <th>{{ trans('validation.attributes.descripcion') }}</th>
                            <th>Subrubros</th>
                            <th>{{ trans('labels.general.actions') }}</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    <script id="details-template" type="text/x-handlebars-template">
        <table class="table details-table full-color-table full-muted-table hover-table" id="subrubros-@{{id}}">
            <thead>
            <tr>
                <th>Nombre</th>
                <th>Descripción</th>
                <th>{{ trans('labels.general.actions') }}</th>
            </tr>
            </thead>
        </table>
    </script>
@endsection

@section('after-scripts')
    {{ Html::script("js/frontend/plugins/datatables/jquery.dataTables.min.js") }}
    {{ Html::script("js/frontend/plugins/handlebars/handlebars-v4.0.11.js") }}

<script>
    $(document).ready(function() {
        var template = Handlebars.compile($("#details-template").html());

        var table = $('#rubro-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('frontend.articulos.RubrosData') !!}',
            columns: [
                {data: 'id', name: 'id', orderable: false, searchable: false, visible: false},
                {data: 'nombre', name: 'nombre'},
                {data: 'descripcion', name: 'descripcion'},
                {
                    "className":      'details-control',
                    "orderable":      false,
                    "searchable":      false,
                    "data":           null,
                    "defaultContent": '<a class="text-primary pointer"><i class="fa fa-chevron-circle-down"></i> Desplegar</a>'
                },
                {data: 'action', name: 'action', orderable: false, searchable: false}
            ]
        });

        // Add event listener for opening and closing details
        $('#rubro-table tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = table.row(tr);
            var id = tr.attr("id");
            var tableId = 'subrubros-' + id;

            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            } else {
                // Open this row
                row.child(template(row.data())).show();
                initTable(tableId, row.data());
                tr.addClass('shown');
                tr.next().find('td').addClass('no-padding bg-gray');
            }
        });

        function initTable(tableId, data) {
            $('#' + tableId).DataTable({
                processing: true,
                serverSide: true,
                "paging":   false,
                "info":     false,
                "searching":   false,
                "aaSorting": [],
                ajax: data.details_url,
                columns: [
                    { data: 'nombre', name: 'nombre', orderable: false },
                    { data: 'descripcion', name: 'descripcion', orderable: false },
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ]
            })
        }

    });

    $("body").on("click", ".delete-rubro", function () {
        var id = $(this).attr("data-id");
        swal({
            title: "Eliminar Rubro",
            text: "¿Realmente desea eliminar este registro?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#ff8726',
            confirmButtonText: "Eliminar"
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/articulos/rubro/'+id,
                    data: {"_method" : 'DELETE'},
                    success: function (msg) {
                        $("#" + id).hide(1);
                        swal("Eliminado!", "El registro ha sido correctamente eliminado.", "success");
                    }
                });
            }
        });
    });

    $("body").on("click", ".delete-subrubro", function () {
        var id = $(this).attr("data-id");
        swal({
            title: "Eliminar Subrubro",
            text: "¿Realmente desea eliminar este registro?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#ff8726',
            confirmButtonText: "Eliminar"
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/articulos/subrubro/'+id,
                    data: {"_method" : 'DELETE'},
                    success: function (msg) {
                        $("#subrubro-" + id).hide(1);
                        swal("Eliminado!", "El registro ha sido correctamente eliminado.", "success");
                    }
                });
            }
        });
    });
</script>

@endsection