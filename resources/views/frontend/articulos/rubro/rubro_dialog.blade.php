<div class="modal fade" id="modal-rubro-nuevo" tabindex="-1" role="dialog" aria-labelledby="rubroModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="rubroModalLabel">Nuevo Rubro</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    {!! Form::label('nombre_rubro', 'Nombre') !!}
                    {!! Form::text('nombre_rubro', '',['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="modal-footer">
                <button id="guardar-rubro" type="button" class="btn btn-themecolor">Guardar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>

@section('scripts-dialog-rubro')
    <script>
        $("#agregar-rubro").click(function(){
            $('input#nombre_rubro').parent().removeClass('has-error');
            $('input#nombre_rubro').val('');
            $('#modal-rubro-nuevo').modal();
        });

        $("#guardar-rubro").click(function(){
            $('input#nombre_rubro').parent().removeClass('has-error');
            if($('input#nombre_rubro').val() == ''){
                $('input#nombre_rubro').parent().addClass('has-error');
                $('input#nombre_rubro').focus();
            }else{
                $('#modal-rubro-nuevo').modal('hide');
                guardarNuevoRubro();
            }
        });

        function guardarNuevoRubro() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $( "input[name*='_token']" ).val()
                }
            });
            var nombre = $('input#nombre_rubro').val();
            $.ajax({
                type: "POST",
                url: '{{ url("/articulos/rubro/guardarNuevoRubro") }}',
                data: {nombre: nombre},
                success: function( msg ) {
                    $('select#rubro_id').append($('<option>', {
                        value: msg.id,
                        text: msg.nombre
                    }));
                    $('select#rubro_id').val(msg.id);
                }
            });
        }
    </script>
@endsection