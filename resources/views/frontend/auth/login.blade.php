<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title', app_name())</title>

    <!-- Meta -->
    <meta name="description" content="@yield('meta_description', 'Ciatt Gestion')">
    <meta name="author" content="@yield('meta_author', 'Luciano Ciattaglia')">

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{{ asset('favicon.png') }}}">
    <!-- For-Mobile-Apps-and-Meta-Tags -->
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link rel="icon" sizes="192x192" href="{{{ asset('favicon.png') }}}">
    <meta name="apple-mobile-web-app-title" content="Ciatt Gestion">
    <link href="{{{ asset('apple-touch-icon.png') }}}" rel="apple-touch-icon" />
    <!-- //For-Mobile-Apps-and-Meta-Tags -->

    <!-- Bootstrap Core CSS -->
    {{ Html::style("css/frontend/plugins/bootstrap/css/bootstrap.min.css") }}
<!-- Custom CSS -->
    {{ Html::style("css/frontend/horizontal_style.css") }}
<!-- You can change the theme colors from here -->
    {{ Html::style("css/frontend/colors/blue.css") }}

    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>

<body>
<!-- ============================================================== -->
<!-- Preloader - style you can find in spinners.css -->
<!-- ============================================================== -->
<div class="preloader">
    <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
</div>
<!-- ============================================================== -->
<!-- Main wrapper - style you can find in pages.scss -->
<!-- ============================================================== -->
<section id="wrapper" class="login-register login-sidebar" style="background-image:url(images/frontend/background/login-register.jpg);">
    <div class="login-box card">
        <div class="card-body">
            {{ Form::open(['route' => 'frontend.auth.login', 'class' => 'form-horizontal form-material', 'id' => 'loginform']) }}
            <a href="javascript:void(0)" class="text-center db">
                <img src="{{url('images/frontend/logos/logo_mini.png')}}" style="max-width: 250px; max-height: 70px;" alt="Inicio" />
            </a>
            <div class="form-group m-t-40">
                 @include('includes.partials.messages')
                <div class="col-xs-12">
                    {{ Form::input('email', 'email', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.email'), 'required' => 'required']) }}
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-12">
                    {{ Form::input('password', 'password', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.password'), 'required' => 'required']) }}
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    <div class="checkbox checkbox-primary pull-left p-t-0">
                        <input name="remember" id="checkbox-signup" type="checkbox">
                        <label for="checkbox-signup"> {{ trans('labels.frontend.auth.remember_me') }} </label>
                    </div>
                    <a href="javascript:void(0)" class="pull-right" onclick="demoLogin()"><i class="fa fa-sign-in"></i> Demo</a>
                </div>
            </div>
            <div class="form-group m-t-20">
                <div class="col-md-12">
                    <a href="javascript:void(0)" id="to-recover" class="text-dark"><i class="fa fa-lock m-r-5"></i> {{trans('labels.frontend.passwords.forgot_password')}}</a>
                </div>
            </div>
            <div class="form-group text-center m-t-20">
                <div class="col-xs-12">
                    <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">{{trans('labels.frontend.auth.login_button')}}</button>
                </div>
            </div>
            <div class="form-group m-b-0">
                <div class="col-sm-12 text-center">
                    <p>¿Aun no tienes una cuenta? <a href="/register" class="text-primary m-l-5"><b>Registrate</b></a></p>
                </div>
            </div>
            {{ Form::close() }}
            {{ Form::open(['route' => 'frontend.auth.password.email', 'class' => 'form-horizontal', 'id' => 'recoverform']) }}
            <div class="form-group ">
                <div class="col-xs-12">
                    <h3>Recuperar Contraseña</h3>
                    <p class="text-muted">Ingrese su email para verificar su identidad </p>
                </div>
            </div>
            <div class="form-group ">
                <div class="col-xs-12">
                    {{ Form::input('email', 'email', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.email'), 'required' => 'required']) }}
                </div>
            </div>
            <div class="form-group text-center m-t-20">
                <div class="col-xs-12">
                    <button class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">{{trans('labels.frontend.passwords.send_password_reset_link_button')}}</button>
                </div>
            </div>
            {{ Form::close() }}
        </div>
    </div>
</section>
<!-- ============================================================== -->
<!-- End Wrapper -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- All Jquery -->
<!-- ============================================================== -->
{{ Html::script("js/frontend/plugins/jquery/jquery.min.js") }}
<!-- Bootstrap tether Core JavaScript -->
{{ Html::script("js/frontend/plugins/bootstrap/popper.min.js") }}
{{ Html::script("js/frontend/plugins/bootstrap/bootstrap.min.js") }}
<!-- slimscrollbar scrollbar JavaScript -->
{{ Html::script("js/frontend/jquery.slimscroll.js") }}
<!--Wave Effects -->
{{ Html::script("js/frontend/waves.js") }}
<!--Menu sidebar -->
{{ Html::script("js/frontend/sidebarmenu.js") }}
<!--stickey kit -->
{{ Html::script("js/frontend/plugins/sticky-kit-master/sticky-kit.min.js") }}
{{ Html::script("js/frontend/plugins/sparkline/jquery.sparkline.min.js") }}
<!--Custom JavaScript -->
{{ Html::script("js/frontend/horizontal_custom.min.js") }}
<!-- ============================================================== -->
<!-- Style switcher -->
<!-- ============================================================== -->
{{ Html::script("js/frontend/plugins/styleswitcher/jQuery.style.switcher.js") }}

<script>
    function demoLogin() {
        $("[name='email']").val('demo@ciatt.com.ar');
        $("[name='password']").val('ciatt');
        $('.form-material').submit();
    }
</script>

</body>

</html>
