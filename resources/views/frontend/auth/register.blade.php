<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title', app_name())</title>

    <!-- Meta -->
    <meta name="description" content="@yield('meta_description', 'Ciatt Gestion')">
    <meta name="author" content="@yield('meta_author', 'Luciano Ciattaglia')">

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{{ asset('favicon.png') }}}">
    <!-- For-Mobile-Apps-and-Meta-Tags -->
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link rel="icon" sizes="192x192" href="{{{ asset('favicon.png') }}}">
    <meta name="apple-mobile-web-app-title" content="Ciatt Gestion">
    <link href="{{{ asset('apple-touch-icon.png') }}}" rel="apple-touch-icon" />
    <!-- //For-Mobile-Apps-and-Meta-Tags -->

    <!-- Bootstrap Core CSS -->
{{ Html::style("css/frontend/plugins/bootstrap/css/bootstrap.min.css") }}
<!-- Custom CSS -->
{{ Html::style("css/frontend/horizontal_style.css") }}
<!-- You can change the theme colors from here -->
    {{ Html::style("css/frontend/colors/blue.css") }}

    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>

<body>
<!-- ============================================================== -->
<!-- Preloader - style you can find in spinners.css -->
<!-- ============================================================== -->
<div class="preloader">
    <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
</div>
<!-- ============================================================== -->
<!-- Main wrapper - style you can find in pages.scss -->
<!-- ============================================================== -->
<section id="wrapper" class="login-register login-sidebar" style="background-image:url(images/frontend/background/login-register.jpg);">
    <div class="login-box card">
        <div class="card-body">
            {{ Form::open(['route' => 'frontend.auth.register', 'class' => 'form-horizontal form-material']) }}
            <a href="javascript:void(0)" class="text-center db">
                <img src="{{url('images/frontend/logos/logo_mini.png')}}" style="max-height: 50px;" alt="Inicio" />
            </a>
            <h3 class="box-title m-t-20 m-b-0">Registrarse</h3><small>Crea tu empresa <strong style="color:green;">GRATIS</strong> y disfruta de tu nuevo sistema de gestión!</small>
            <div class="form-group m-t-20">
                @include('includes.partials.messages')
                <div class="col-xs-12">
                    {{ Form::input('razon_social', 'razon_social', null, ['class' => 'form-control', 'placeholder' => 'Razon Social', 'required' => 'required']) }}
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-12">
                    {!! Form::select('condicion_iva_id', $condiciones_iva, '', array('class' => 'form-control', 'required' => 'required')) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-12">
                    {{ Form::input('email', 'email', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.email'), 'required' => 'required']) }}
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-12">
                    {{ Form::input('password', 'password', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.password'), 'required' => 'required']) }}
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-12">
                    {{ Form::input('password', 'password_confirmation', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.password_confirmation'), 'required' => 'required']) }}
                </div>
            </div>
            <div class="form-group text-center m-t-20">
                <div class="col-xs-12">
                    <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">{{trans('labels.frontend.auth.register_button')}}</button>
                </div>
            </div>
            {{ Form::close() }}
            <div class="form-group m-b-0">
                <div class="col-sm-12 text-center">
                    <p>¿Ya tienes una cuenta? <a href="/login" class="text-info m-l-5"><b>Iniciar Sesión</b></a></p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ============================================================== -->
<!-- End Wrapper -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- All Jquery -->
<!-- ============================================================== -->
{{ Html::script("js/frontend/plugins/jquery/jquery.min.js") }}
<!-- Bootstrap tether Core JavaScript -->
{{ Html::script("js/frontend/plugins/bootstrap/popper.min.js") }}
{{ Html::script("js/frontend/plugins/bootstrap/bootstrap.min.js") }}
<!-- slimscrollbar scrollbar JavaScript -->
{{ Html::script("js/frontend/jquery.slimscroll.js") }}
<!--Wave Effects -->
{{ Html::script("js/frontend/waves.js") }}
<!--Menu sidebar -->
{{ Html::script("js/frontend/sidebarmenu.js") }}
<!--stickey kit -->
{{ Html::script("js/frontend/plugins/sticky-kit-master/sticky-kit.min.js") }}
{{ Html::script("js/frontend/plugins/sparkline/jquery.sparkline.min.js") }}
<!--Custom JavaScript -->
{{ Html::script("js/frontend/horizontal_custom.min.js") }}
<!-- ============================================================== -->
<!-- Style switcher -->
<!-- ============================================================== -->
{{ Html::script("js/frontend/plugins/styleswitcher/jQuery.style.switcher.js") }}
</body>

</html>