@extends ('frontend.layouts.app')

@section ('title', trans('labels.frontend.movimientobancario_titulo')." - ".trans('navs.frontend.user.administration'))

@section('after-styles')
    {{ Html::style("css/frontend/plugins/datatables/responsive.dataTables.min.css") }}
@endsection

@section('content')
    <div class="row page-titles">
        <div class="col-6 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('labels.frontend.movimientobancario_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item active">{{ trans('labels.frontend.movimientobancario_titulo') }}</li>
            </ol>
        </div>
        <div class="col-6">
            <div class="pull-right">
                <a href="{{ url('/fondos/movimiento-bancario/create/0') }}" class="btn btn-themecolor" title="Crear Nuevo Deposito">
                    <i class="fa fa-plus" aria-hidden="true"></i>
                    <span class="hidden-xs-down">Deposito</span>
                </a>
                <a href="{{ url('/fondos/movimiento-bancario/create/1') }}" class="btn btn-warning" title="Crear Nueva Extraccion">
                    <i class="fa fa-plus" aria-hidden="true"></i>
                    <span class="hidden-xs-down">Extracción</span>
                </a>
                <a href="{{ url('/fondos/movimiento-bancario/create/2') }}" class="btn btn-primary" title="Crear Nueva Transferencia">
                    <i class="fa fa-plus" aria-hidden="true"></i>
                    <span class="hidden-xs-down">Transferencia</span>
                </a>
            </div>
        </div>
    </div>

    @tips($_SERVER['REQUEST_URI'])

    <div class="card">
        <div class="card-body">
            <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
            @include('includes.partials.messages')
            <div class="table-responsive">
                <table id="movimientobancario-table" class="table selectable table-striped table-hover table-sm">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Fecha</th>
                            <th>Cuen. Ban. Destino</th>
                            <th>Cuen. Ban. Origen</th>
                            <th>Operacion</th>
                            <th>Importe</th>
                            <th>{{ trans('labels.general.actions') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($movimientosBancarios as $item)
                        <tr id="{{$item->id}}">
                            <td>&nbsp;&nbsp;</td>
                            <td>{{ $item->fecha->format('d/m/Y') }}</td>
                            <td>{{ isset($item->cuentaBancariaDestino) ? 'Nº '.$item->cuentaBancariaDestino->numero.' - '.$item->cuentaBancariaDestino->banco->nombre : '' }}</td>
                            <td>{{ isset($item->cuentaBancariaOrigen) ? 'Nº '.$item->cuentaBancariaOrigen->numero.' - '.$item->cuentaBancariaOrigen->banco->nombre : '' }}</td>
                            <td>
                                @if($item->tipo == 0)
                                    <span class="label label-success">
                                      @elseif($item->tipo == 1)
                                            <span class="label label-warning">
                                      @else
                                                    <span class="label label-purple">
                                      @endif
                                                        {{ $tipos[$item->tipo] }}
                                        </span>
                            </td>
                            <td><b>$ {{ $item->importe }}</b></td>
                            <td>
                                <a href="{{ url('/fondos/movimiento-bancario/' . $item->id) }}" class="mytooltip">
                                    <i class="fa fa-eye text-success m-r-10"></i>
                                    <span class="tooltip-content3">{{ trans('buttons.general.crud.show') }} {{ trans('labels.frontend.movimientobancario') }}</span>
                                </a>
                                <a href="{{ url('/fondos/movimiento-bancario/' . $item->id . '/edit') }}" class="mytooltip">
                                    <i class="fa fa-pencil text-info m-r-10"></i>
                                    <span class="tooltip-content3">{{ trans('buttons.general.crud.edit') }} {{ trans('labels.frontend.movimientobancario') }}</span>
                                </a>
                                <a href="javascript:void(0)" class="delete mytooltip" data-id="{{$item->id}}">
                                    <i class="fa fa-close text-danger"></i>
                                    <span class="tooltip-content3">{{ trans('buttons.general.crud.delete') }} {{ trans('labels.frontend.movimientobancario') }}</span>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection

@section('after-scripts')
    {{ Html::script("js/frontend/plugins/datatables/jquery.dataTables.min.js") }}
    {{ Html::script("js/frontend/plugins/datatables/dataTables.responsive.min.js") }}
    {{ Html::script("js/frontend/plugins/datatables/dataTables.select.min.js") }}

<script>
    $(document).ready(function() {
        $('#movimientobancario-table').DataTable({
            "aaSorting": [],
            responsive: {
                details: {
                    type: 'column',
                    target: 'tr'
                }
            },
            "columnDefs": [{
                targets: 0,
                className: 'control',
                orderable: false,
                searchable: false,
                defaultContent: '&nbsp;&nbsp;'
            },
                { responsivePriority: 1, targets: -1 },
                { responsivePriority: 2, targets: -2 },
                { responsivePriority: 1, targets: -3 }]
        });
    } );

    $("body").on("click", ".delete", function () {
        var id = $(this).attr("data-id");
        swal({
            title: "{{trans('buttons.general.crud.delete').' '.trans('labels.frontend.movimientobancario')}}",
            text: "¿Realmente desea eliminar este registro?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#ff8726',
            confirmButtonText: "Eliminar"
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/fondos/movimiento-bancario/'+id,
                    data: {"_method" : 'DELETE'},
                    success: function (msg) {
                        $("#" + id).hide(1);
                        swal("Eliminado!", "El registro ha sido correctamente eliminado.", "success");
                    }
                });
            }
        });
    });
</script>

@endsection
