@extends('frontend.layouts.app')

@section ('title', trans('labels.frontend.movimientobancario_titulo')." - ".trans('buttons.general.crud.show'))

@section('content')
    <div class="row page-titles">
        <div class="col-6 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('labels.frontend.movimientobancario_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item"><a href="{{url('/fondos/movimiento-bancario')}}">{{ trans('labels.frontend.movimientobancario_titulo') }}</a></li>
                <li class="breadcrumb-item">{{ trans('buttons.general.crud.show') }} {{ trans('labels.frontend.movimientobancario') }}</li>
                <li class="breadcrumb-item active">{{ $movimientobancario }}</li>
            </ol>
        </div>
        <div class="col-6">
            <div class="pull-right">
                <a href="{{ url('/fondos/movimiento-bancario') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning ">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans('buttons.general.crud.back') }}</span>
                </a>
                <a href="{{ url('/fondos/movimiento-bancario/' . $movimientobancario->id . '/edit') }}" title="{{ trans('buttons.general.crud.edit') }}" class="btn btn-info ">
                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans('buttons.general.crud.edit') }}</span>
                </a>
                <a href="javascript:void(0)" title="{{trans("buttons.general.crud.delete")." ".trans("labels.frontend.movimientobancario")}}" class="btn btn-danger  delete">
                    <i class="fa fa-trash" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans("buttons.general.crud.delete") }}</span>
                </a>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
            <h4 class="font-medium m-t-30">{{ $movimientobancario }}</h4>
            <hr>
            <div class="row">
                <div class="col-md-3 font-weight-bold">Fecha</div>
                <div class="col-md-9">{{ $movimientobancario->fecha->format('d/m/Y') }}</div>
            </div><hr>
            <div class="row">
                <div class="col-md-3 font-weight-bold">Importe</div>
                <div class="col-md-9">$ {{ $movimientobancario->importe }}</div>
            </div><hr>
            <div class="row">
                <div class="col-md-3 font-weight-bold">Numero</div>
                <div class="col-md-9">{{ $movimientobancario->numero }}</div>
            </div><hr>
            @if(isset($movimientobancario->cuentaBancariaOrigen))
            <div class="row">
                <div class="col-md-3 font-weight-bold">Cuenta Bancaria Origen</div>
                <div class="col-md-9">{{ $movimientobancario->cuentaBancariaOrigen }}</div>
            </div><hr>
            @endif
            @if(isset($movimientobancario->cuentaBancariaDestino))
                <div class="row">
                    <div class="col-md-3 font-weight-bold">Cuenta Bancaria Destino</div>
                    <div class="col-md-9">{{ $movimientobancario->cuentaBancariaDestino }}</div>
                </div><hr>
            @endif
            <div class="row">
                <div class="col-md-3 font-weight-bold">Tipo</div>
                <div class="col-md-9">{{ $tipos[$movimientobancario->tipo] }}</div>
            </div><hr>
            <div class="row">
                <div class="col-md-3 font-weight-bold">Comentario</div>
                <div class="col-md-9">{{ $movimientobancario->comentario }}</div>
            </div><hr>
            <div class="row">
                <div class="col-md-3 font-weight-bold">Creado el</div>
                <div class="col-md-9">{{ $movimientobancario->created_at->format('d/m/Y H:m') }}</div>
            </div><hr>
            <div class="row">
                <div class="col-md-3 font-weight-bold">Ultima Modificación</div>
                <div class="col-md-9">{{ $movimientobancario->updated_at->format('d/m/Y H:m') }}</div>
            </div>
        </div>
    </div>
@endsection

@section('after-scripts')

<script>
    $("body").on("click", ".delete", function () {
        swal({
            title: "{{trans('buttons.general.crud.delete').' '.trans('labels.frontend.movimientobancario')}}",
            text: "¿Realmente desea eliminar este registro?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#ff8726',
            confirmButtonText: "Eliminar"
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '{{ url('/fondos/movimiento-bancario/'.$movimientobancario->id) }}',
                    data: {"_method" : 'DELETE'},
                    success: function (msg) {
                        swal("Eliminado!", "El registro ha sido correctamente eliminado.", "success").then((result) => { window.location.href = '{{url('/fondos/movimiento-bancario')}}'; });
                    }
                });
            }
        });
    });
</script>

@endsection
