<div class="card">
    <div class="card-body">
        <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
        <br>
        <div class="row form-group">
            <div class="col-md-6">
                {!! Form::label('importe', 'Importe') !!}
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-usd"></i>
                    </div>
                    {!! Form::number('importe', null, ['class' => 'form-control', 'required' => 'required', 'step' => '0.01']) !!}
                </div>
                {!! $errors->first('importe', '<p class="text-danger">:message</p>') !!}
            </div>
            <div class="col-md-6">
                {!! Form::label('fecha', 'Fecha') !!}
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    {!! Form::date('fecha', isset($movimientoBancario->fecha) ? $movimientoBancario->fecha : $fecha , ['class' => 'form-control', 'required' => 'required']) !!}
                </div>
                {!! $errors->first('fecha', '<p class="text-danger">:message</p>') !!}
            </div>
        </div>
        <div class="row form-group">
            @if(isset($tipoMovimiento) && $tipoMovimiento == 2)
                <div class="col-md-6">
                    {!! Form::label('cuenta_bancaria_origen_id', 'Cuen. Ban. Origen') !!}
                    {!! Form::select('cuenta_bancaria_origen_id', $cuentaBancaria, isset($movimientoBancario->cuentaBancariaOrigen) ? $movimientoBancario->cuentaBancariaOrigen->id : '', array('class' => 'form-control', 'required' => 'required')) !!}
                    {!! $errors->first('cuenta_bancaria_origen_id', '<p class="text-danger">:message</p>') !!}
                </div>
                <div class="col-md-6">
                    {!! Form::label('cuenta_bancaria_destino_id', 'Cuen. Ban. Destino') !!}
                    {!! Form::select('cuenta_bancaria_destino_id', $cuentaBancaria, isset($movimientoBancario->cuentaBancariaDestino) ? $movimientoBancario->cuentaBancariaDestino->id : '', array('class' => 'form-control', 'required' => 'required')) !!}
                    {!! $errors->first('cuenta_bancaria_destino_id', '<p class="text-danger">:message</p>') !!}
                </div>
            @else
                <div class="col-md-12">
                    {!! Form::label('cuenta_bancaria_destino_id', 'Cuenta Bancaria') !!}
                    {!! Form::select('cuenta_bancaria_destino_id', $cuentaBancaria, isset($movimientoBancario->cuentaBancariaDestino) ? $movimientoBancario->cuentaBancariaDestino->id : '', array('class' => 'form-control', 'required' => 'required')) !!}
                    {!! $errors->first('cuenta_bancaria_destino_id', '<p class="text-danger">:message</p>') !!}
                </div>
            @endif
        </div>
        <div class="row form-group">
            <div class="col-md-6">
                {!! Form::label('numero', 'Numero') !!}
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-hashtag"></i>
                    </div>
                    {!! Form::number('numero', null, ['class' => 'form-control']) !!}
                </div>
                {!! $errors->first('numero', '<p class="text-danger">:message</p>') !!}
            </div>
            <div class="col-md-6">
                {!! Form::label('moneda_id', 'Moneda') !!}
                {!! Form::select('moneda_id', $monedas, isset($movimientobancario->moneda) ? $movimientobancario->moneda->id : '', array('class' => 'form-control', 'required' => 'required')) !!}
                {!! $errors->first('moneda_id', '<p class="text-danger">:message</p>') !!}
            </div>
        </div>
        <div class="row form-group">
            <div class="col-md-12">
                {!! Form::label('comentario', 'Comentario') !!}
                {!! Form::textarea('comentario', null, ['class' => 'form-control','size' => '10x3']) !!}
                {!! $errors->first('comentario', '<p class="text-danger">:message</p>') !!}
            </div>
        </div>
        <div class="row form-group">
            <div class="col-md-12 demo-checkbox">
                <input type="checkbox" id="generarMovimiento" name="generarMovimiento" class="filled-in chk-col-cyan" checked />
                <label for="generarMovimiento"><strong>Generar Movimiento en la caja actual</strong></label>
                <i class="fa fa-question-circle text-info mytooltip">
                    <span class="tooltip-content3">Si esta seleccionado, se creara un movimiento automáticamente en la caja actual de entrada o salida con forma de pago "Cuenta Bancaria" y el importe cargado</span>
                </i>
            </div>
        </div>
        {{ Form::hidden('tipo', isset($tipoMovimiento) ? $tipoMovimiento : $movimientoBancario->tipo) }}
        <hr>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-12">
                    {{ Form::submit(isset($submitButtonText) ? $submitButtonText : trans('buttons.general.save'), ['class' => 'btn btn-lg btn-themecolor pull-right']) }}
                </div>
            </div>
        </div>
    </div>
</div>
