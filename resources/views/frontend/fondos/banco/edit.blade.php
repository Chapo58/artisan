@extends('frontend.layouts.app')

@section ('title', trans('labels.frontend.banco_titulo')." - ".trans('buttons.general.crud.edit'))

@section('content')
    <div class="row page-titles">
        <div class="col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('labels.frontend.banco_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item"><a href="{{url('/fondos/banco')}}">{{ trans('labels.frontend.banco_titulo') }}</a></li>
                <li class="breadcrumb-item">{{ trans('buttons.general.crud.edit') }} {{ trans('labels.frontend.banco') }}</li>
                <li class="breadcrumb-item active">{{ $banco }}</li>
            </ol>
        </div>
        <div class="col-4">
            <div class="pull-right">
                <a href="{{ url('/fondos/banco') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans('buttons.general.crud.back') }}</span>
                </a>
            </div>
        </div>
    </div>

    @include('includes.partials.messages')

    {!! Form::model($banco, [
        'method' => 'PATCH',
        'url' => ['/fondos/banco', $banco->id],
        'class' => 'form-horizontal',
        'files' => true
    ]) !!}

        @include ('frontend.fondos.banco.form', ['submitButtonText' => trans("buttons.general.crud.update")])

    {!! Form::close() !!}

@endsection
