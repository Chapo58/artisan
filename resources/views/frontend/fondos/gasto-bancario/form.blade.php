<div class="card">
    <div class="card-body">
        <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
        <br>
        <div class="row form-group">
            <div class="col-md-12">
                {!! Form::label('nombre', 'Nombre') !!}
                {!! Form::text('nombre', null, ['class' => 'form-control', 'required' => 'required']) !!}
                {!! $errors->first('nombre', '<p class="text-danger">:message</p>') !!}
            </div>
        </div>
        <div class="row form-group">
            <div class="col-md-6">
                {!! Form::label('porcentaje', 'Porcentaje') !!}
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-percent"></i>
                    </div>
                    {!! Form::number('porcentaje', null, ['class' => 'form-control', 'step' => '0.1']) !!}
                </div>
                {!! $errors->first('porcentaje', '<p class="text-danger">:message</p>') !!}
            </div>
            <div class="col-md-6">
                {!! Form::label('importe', 'Importe') !!}
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-dollar"></i>
                    </div>
                    {!! Form::number('importe', null, ['class' => 'form-control', 'step' => '0.1']) !!}
                </div>
                {!! $errors->first('importe', '<p class="text-danger">:message</p>') !!}
            </div>
        </div>
        <hr>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-12">
                    {{ Form::submit(isset($submitButtonText) ? $submitButtonText : trans('buttons.general.save'), ['class' => 'btn btn-lg btn-themecolor pull-right']) }}
                </div>
            </div>
        </div>
    </div>
</div>
