<div class="card">
    <div class="card-body">
        <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
        <br>
        <div class="row form-group">
            <div class="col-md-4">
                {!! Form::label('cuenta_bancaria_id', 'Cuenta Bancaria') !!}
                {!! Form::select('cuenta_bancaria_id', $cuentas_bancarias, isset($chequePropio->cuentaBancaria) ? $chequePropio->cuentaBancaria->id : '', array('class' => 'form-control', 'required' => 'required')) !!}
                {!! $errors->first('cuenta_bancaria_id', '<p class="text-danger">:message</p>') !!}
            </div>
            <div class="col-md-4">
                {!! Form::label('numero', 'Numero') !!}
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-hashtag"></i>
                    </div>
                    {!! Form::number('numero', null, ['class' => 'form-control', 'required' => 'required', 'step' => '0.1']) !!}
                </div>
                {!! $errors->first('numero', '<p class="text-danger">:message</p>') !!}
            </div>
            <div class="col-md-4">
                {!! Form::label('importe', 'Importe') !!}
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-usd"></i>
                    </div>
                    {!! Form::number('importe', null, ['class' => 'form-control', 'required' => 'required', 'step' => '0.01']) !!}
                </div>
                {!! $errors->first('importe', '<p class="text-danger">:message</p>') !!}
            </div>
        </div>
        <div class="row form-group">
            <div class="col-md-6">
                {!! Form::label('emision', 'Emision') !!}
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    {!! Form::text('emision', isset($chequePropio->emision) ? $chequePropio->emision->format('d/m/Y') : $fecha_hoy, ['class' => 'form-control fecha']) !!}
                </div>
                {!! $errors->first('emision', '<p class="text-danger">:message</p>') !!}
            </div>
            <div class="col-md-6">
                {!! Form::label('vencimiento', 'Vencimiento') !!}
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    {!! Form::text('vencimiento', isset($chequePropio->vencimiento) ? $chequePropio->vencimiento->format('d/m/Y') : '', ['class' => 'form-control fecha']) !!}
                </div>
                {!! $errors->first('vencimiento', '<p class="text-danger">:message</p>') !!}
            </div>
        </div>
        <hr>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-12">
                    {{ Form::submit(isset($submitButtonText) ? $submitButtonText : trans('buttons.general.save'), ['class' => 'btn btn-lg btn-themecolor pull-right']) }}
                </div>
            </div>
        </div>
    </div>
</div>
