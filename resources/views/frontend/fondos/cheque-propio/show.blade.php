@extends('frontend.layouts.app')

@section ('title', trans('labels.frontend.chequepropio_titulo')." - ".trans('buttons.general.crud.show'))

@section('content')
    <div class="row page-titles">
        <div class="col-6 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('labels.frontend.chequepropio_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item"><a href="{{url('/fondos/cheque-propio')}}">{{ trans('labels.frontend.chequepropio_titulo') }}</a></li>
                <li class="breadcrumb-item">{{ trans('buttons.general.crud.show') }} {{ trans('labels.frontend.chequepropio') }}</li>
                <li class="breadcrumb-item active">{{ $chequePropio }}</li>
            </ol>
        </div>
        <div class="col-6">
            <div class="pull-right">
                <a href="{{ url('/fondos/cheque-propio') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning ">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans('buttons.general.crud.back') }}</span>
                </a>
                <a href="{{ url('/fondos/cheque-propio/' . $chequePropio->id . '/edit') }}" title="{{ trans('buttons.general.crud.edit') }}" class="btn btn-info ">
                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans('buttons.general.crud.edit') }}</span>
                </a>
                <a href="javascript:void(0)" title="{{trans("buttons.general.crud.delete")." ".trans("labels.frontend.chequepropio")}}" class="btn btn-danger  delete">
                    <i class="fa fa-trash" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans("buttons.general.crud.delete") }}</span>
                </a>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
            <h4 class="font-medium m-t-30">
                Cheque {{ $chequePropio }}
                @if($chequePropio->estado == 0)
                    <span class="label label-warning">{{$estados[$chequePropio->estado]}}</span>
                @elseif($chequePropio->estado == 1)
                    <span class="label label-success">{{$estados[$chequePropio->estado]}}</span>
                @elseif($chequePropio->estado == 2)
                    <span class="label label-primary">{{$estados[$chequePropio->estado]}}</span>
                @else
                    <span class="label label-info">{{$estados[$chequePropio->estado]}}</span>
                @endif
            </h4>
            <hr>
            <div class="row">
                <div class="col-md-3 font-weight-bold">Numero</div>
                <div class="col-md-9">{{ $chequePropio->numero }}</div>
            </div><hr>
            <div class="row">
                <div class="col-md-3 font-weight-bold">Importe</div>
                <div class="col-md-9">$ {{ $chequePropio->importe }}</div>
            </div><hr>
            <div class="row">
                <div class="col-md-3 font-weight-bold">Chequera</div>
                <div class="col-md-9">{{ $chequePropio->chequera }}</div>
            </div><hr>
            <div class="row">
                <div class="col-md-3 font-weight-bold">Emision</div>
                <div class="col-md-9">{{ ($chequePropio->emision) ? $chequePropio->emision->format('d/m/Y') : '' }}</div>
            </div><hr>
            <div class="row">
                <div class="col-md-3 font-weight-bold">Vencimiento</div>
                <div class="col-md-9">{{ ($chequePropio->vencimiento) ? $chequePropio->vencimiento->format('d/m/Y') : '' }}</div>
            </div><hr>
            <div class="row">
                <div class="col-md-3 font-weight-bold">Creado el</div>
                <div class="col-md-9">{{ $chequePropio->created_at->format('d/m/Y H:m') }}</div>
            </div><hr>
            <div class="row">
                <div class="col-md-3 font-weight-bold">Ultima Modificación</div>
                <div class="col-md-9">{{ $chequePropio->updated_at->format('d/m/Y H:m') }}</div>
            </div>
        </div>
    </div>

@endsection

@section('after-scripts')
<script>
    $("body").on("click", ".delete", function () {
        swal({
            title: "{{trans('buttons.general.crud.delete').' '.trans('labels.frontend.chequepropio')}}",
            text: "¿Realmente desea eliminar este registro?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#ff8726',
            confirmButtonText: "Eliminar"
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '{{ url('/fondos/cheque-propio/'.$chequePropio->id) }}',
                    data: {"_method" : 'DELETE'},
                    success: function (msg) {
                        swal("Eliminado!", "El registro ha sido correctamente eliminado.", "success").then((result) => { window.location.href = '{{url('/fondos/cheque-propio')}}'; });
                    }
                });
            }
        });
    });
</script>
@endsection
