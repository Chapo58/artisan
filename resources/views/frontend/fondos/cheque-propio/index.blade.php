@extends ('frontend.layouts.app')

@section ('title', trans('labels.frontend.chequepropio_titulo')." - ".trans('navs.frontend.user.administration'))

@section('after-styles')
    {{ Html::style("css/frontend/plugins/datatables/responsive.dataTables.min.css") }}
@endsection

@section('content')
    <div class="row page-titles">
        <div class="col-6 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('labels.frontend.chequepropio_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item active">{{ trans('labels.frontend.chequepropio_titulo') }}</li>
            </ol>
        </div>
        <div class="col-6">
            <div class="pull-right">
                <a href="{{ url('/fondos/cheque-propio/create') }}" class="btn btn-themecolor btn-lg" title="{{ trans('buttons.general.crud.create').' '.trans('labels.frontend.chequepropio')  }}">
                    <i class="fa fa-plus" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans('buttons.general.crud.create2').' '.trans('labels.frontend.chequepropio') }}</span>
                </a>
            </div>
        </div>
    </div>

    @tips($_SERVER['REQUEST_URI'])

    <div class="card">
        <div class="card-body">
            <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
            @include('includes.partials.messages')
            <div class="table-responsive">
                <table id="chequepropio-table" class="table selectable table-striped table-hover table-sm">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Numero</th>
                            <th>Importe</th>
                            <th>Cuenta Bancaria</th>
                            <th>Emision</th>
                            <th>Vencimiento</th>
                            <th>Estado</th>
                            <th>{{ trans('labels.general.actions') }}</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('after-scripts')
    {{ Html::script("js/frontend/plugins/datatables/jquery.dataTables.min.js") }}
    {{ Html::script("js/frontend/plugins/datatables/dataTables.responsive.min.js") }}
    {{ Html::script("js/frontend/plugins/datatables/dataTables.select.min.js") }}

<script>
    $(document).ready(function() {
        $('#chequepropio-table').DataTable({
            "aaSorting": [],
            processing: true,
            serverSide: true,
            responsive: {
                details: {
                    type: 'column',
                    target: 'tr'
                }
            },
            "columnDefs": [{
                targets: 0,
                className: 'control',
                orderable: false,
                searchable: false,
                defaultContent: '&nbsp;&nbsp;'
            },
                { responsivePriority: 1, targets: -1 }],
            ajax: '{!! route('frontend.fondos.ChequesPropiosData') !!}',
            columns: [
                {data: null, name: ''},
                {data: 'numero', name: 'numero'},
                {data: 'importe', name: 'importe'},
                {data: 'cuentaBancaria', name: 'cuentaBancaria'},
                {data: 'emision', name: 'emision'},
                {data: 'vencimiento', name: 'vencimiento'},
                {data: 'estado', name: 'estado'},
                {data: 'action', name: 'action', orderable: false, searchable: false}
            ]
        });
    } );

    $("body").on("click", ".delete", function () {
        var id = $(this).attr("data-id");
        swal({
            title: "{{trans('buttons.general.crud.delete').' '.trans('labels.frontend.chequepropio')}}",
            text: "¿Realmente desea eliminar este registro?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#ff8726',
            confirmButtonText: "Eliminar"
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/fondos/cheque-propio/'+id,
                    data: {"_method" : 'DELETE'},
                    success: function (msg) {
                        $("#" + id).hide(1);
                        swal("Eliminado!", "El registro ha sido correctamente eliminado.", "success");
                    }
                });
            }
        });
    });
</script>

@endsection
