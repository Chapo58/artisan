<div class="card">
    <div class="card-body">
        <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
        <br>
        <div class="row form-group">
            <div class="col-md-4">
                {!! Form::label('importe', 'Importe') !!}
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-usd"></i>
                    </div>
                    {!! Form::number('importe', null, ['class' => 'form-control', 'step' => '.01', 'required' => 'required']) !!}
                </div>
                {!! $errors->first('importe', '<p class="text-danger">:message</p>') !!}
            </div>
            <div class="col-md-4">
                {!! Form::label('numero', 'Numero') !!}
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-hashtag"></i>
                    </div>
                    {!! Form::number('numero', null, ['class' => 'form-control', 'required' => 'required']) !!}
                </div>
                {!! $errors->first('numero', '<p class="text-danger">:message</p>') !!}
            </div>
            <div class="col-md-4">
                {!! Form::label('banco_id', 'Banco') !!}
                {!! Form::select('banco_id', $bancos, isset($cheque->banco) ? $cheque->banco->id : '', array('class' => 'form-control')) !!}
                {!! $errors->first('banco_id', '<p class="text-danger">:message</p>') !!}
            </div>
        </div>
        <div class="row form-group">
            <div class="col-md-6">
                {!! Form::label('emision', 'Emision') !!}
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    {!! Form::text('emision', isset($cheque->emision) ? $cheque->emision->format('d/m/Y') : $fecha_hoy, ['class' => 'form-control fecha']) !!}
                </div>
                {!! $errors->first('emision', '<p class="text-danger">:message</p>') !!}
            </div>
            <div class="col-md-6">
                {!! Form::label('vencimiento', 'Vencimiento') !!}
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    {!! Form::text('vencimiento', isset($cheque->vencimiento) ? $cheque->vencimiento->format('d/m/Y') : $fecha_hoy, ['class' => 'form-control fecha']) !!}
                </div>
                {!! $errors->first('vencimiento', '<p class="text-danger">:message</p>') !!}
            </div>
        </div>
        <div class="row form-group">
            <div class="col-md-6">
                {!! Form::label('firmante', 'Firmante') !!}
                {!! Form::text('firmante', null, ['class' => 'form-control', 'required' => 'required']) !!}
                {!! $errors->first('firmante', '<p class="text-danger">:message</p>') !!}
            </div>
            <div class="col-md-6">
                {!! Form::label('sucursal', 'Sucursal') !!}
                {!! Form::text('sucursal', null, ['class' => 'form-control']) !!}
                {!! $errors->first('sucursal', '<p class="text-danger">:message</p>') !!}
            </div>
        </div>
        <hr>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-12">
                    {{ Form::submit(isset($submitButtonText) ? $submitButtonText : trans('buttons.general.save'), ['class' => 'btn btn-lg btn-themecolor pull-right']) }}
                </div>
            </div>
        </div>
    </div>
</div>
