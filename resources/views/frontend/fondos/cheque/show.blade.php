@extends('frontend.layouts.app')

@section ('title', "Cartera de Cheques - ".trans('buttons.general.crud.show'))

@section('content')
    <div class="row page-titles">
        <div class="col-6 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">Cartera de Cheques</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item"><a href="{{url('/fondos/cheque')}}">Cartera de Cheques</a></li>
                <li class="breadcrumb-item">{{ trans('buttons.general.crud.show') }} Cheque</li>
                <li class="breadcrumb-item active">{{ $cheque }}</li>
            </ol>
        </div>
        <div class="col-6">
            <div class="pull-right">
                <a href="{{ url('/fondos/cheque') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans('buttons.general.crud.back') }}</span>
                </a>
                @if($cheque->estado == $estadoPendiente)
                    <a href="{{ url('/fondos/cheque/' . $cheque->id . '/edit') }}" title="{{ trans('buttons.general.crud.edit') }}" class="btn btn-info">
                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                        <span class="hidden-xs-down">{{ trans('buttons.general.crud.edit') }}</span>
                    </a>
                    <a href="javascript:void(0)" title="Cobrar Cheque" onclick="cobrar()" class="btn btn-primary ">
                        <i class="fa fa-usd" aria-hidden="true"></i>
                        <span class="hidden-xs-down">Cobrar</span>
                    </a>
                @endif
                <a href="javascript:void(0)" title="{{trans("buttons.general.crud.delete")}} Cheque" class="btn btn-danger delete">
                    <i class="fa fa-trash" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans("buttons.general.crud.delete") }}</span>
                </a>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
            <h4 class="font-medium m-t-30">
                Cheque {{ $cheque }}
                @if($cheque->estado == 0)
                    <span class="label label-warning">{{$estados[$cheque->estado]}}</span>
                @elseif($cheque->estado == 1)
                    <span class="label label-success">{{$estados[$cheque->estado]}}</span>
                @elseif($cheque->estado == 2)
                    <span class="label label-primary">{{$estados[$cheque->estado]}}</span>
                @else
                    <span class="label label-info">{{$estados[$cheque->estado]}}</span>
                @endif
            </h4>
            <hr>
            <div class="row">
                <div class="col-md-2 font-weight-bold">Numero</div>
                <div class="col-md-4">{{ $cheque->numero }}</div>
                <div class="col-md-2 font-weight-bold">Firmante</div>
                <div class="col-md-3">{{ $cheque->firmante }}</div>
            </div><hr>
            <div class="row">
                <div class="col-md-2 font-weight-bold">Fecha de Emision</div>
                <div class="col-md-4">{{ ($cheque->emision) ? $cheque->emision->format('d/m/Y') : "" }}</div>
                <div class="col-md-2 font-weight-bold">Fecha de Vencimiento</div>
                <div class="col-md-3">{{ ($cheque->vencimiento) ? $cheque->vencimiento->format('d/m/Y') : "" }}</div>
            </div><hr>
            <div class="row">
                <div class="col-md-2 font-weight-bold">Banco</div>
                <div class="col-md-4">{{ isset($cheque->banco) ? $cheque->banco->nombre: 'Ninguno' }}</div>
                <div class="col-md-2 font-weight-bold">Sucursal</div>
                <div class="col-md-3">{{ $cheque->sucursal }}</div>
            </div><hr>
            <div class="row">
                <div class="col-md-2 font-weight-bold">Importe</div>
                <div class="col-md-4">{{ $cheque->importe }}</div>
                <div class="col-md-2 font-weight-bold">Estado</div>
                <div class="col-md-3">{{ $estados[$cheque->estado] }}</div>
            </div><hr>
            <div class="row">
                <div class="col-md-3 font-weight-bold">Creado el</div>
                <div class="col-md-9">{{ $cheque->created_at->format('d/m/Y H:m') }}</div>
            </div><hr>
            <div class="row">
                <div class="col-md-3 font-weight-bold">Ultima Modificación</div>
                <div class="col-md-9">{{ $cheque->updated_at->format('d/m/Y H:m') }}</div>
            </div>
        </div>
    </div>

    {!! Form::open([
        'method'=>'GET',
        'action' => ['Frontend\Fondos\ChequeController@cobrar',$cheque->id],
        'style' => 'display:inline',
        'id' => 'form-cobrar-cheque'
    ]) !!}
    {!! Form::close() !!}

@endsection

@section('after-scripts')

<script>
    $("body").on("click", ".delete", function () {
        swal({
            title: "{{trans('buttons.general.crud.delete')}} Cheque",
            text: "¿Realmente desea eliminar este registro?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#ff8726',
            confirmButtonText: "Eliminar"
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '{{ url('/fondos/cheque/'.$cheque->id) }}',
                    data: {"_method" : 'DELETE'},
                    success: function (msg) {
                        swal("Eliminado!", "El registro ha sido correctamente eliminado.", "success").then((result) => { window.location.href = '{{url('/fondos/cheque')}}'; });
                    }
                });
            }
        });
    });

function cobrar(){
    swal({
        title: "Cobrar Cheque",
        text: "Se generara el movimiento de caja en su caja actual. ¿Desea Continuar?",
        type: "info",
        showCancelButton: true,
        confirmButtonColor: '#2EB800',
        confirmButtonText: "Cobrar"
    }).then((result) => {
        if (result.value) {
            $('form#form-cobrar-cheque').submit();
        }
    });
}
</script>

@endsection
