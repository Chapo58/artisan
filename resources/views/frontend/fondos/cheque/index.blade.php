@extends ('frontend.layouts.app')

@section ('title',"Cartera de Cheques - ".trans('navs.frontend.user.administration'))

@section('after-styles')
    {{ Html::style("css/frontend/plugins/datatables/responsive.dataTables.min.css") }}
@endsection

@section('content')
    <div class="row page-titles">
        <div class="col-6 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">Cartera de Cheques</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item active">Cartera de Cheques</li>
            </ol>
        </div>
        <div class="col-6">
            <div class="pull-right">
                <a href="{{ url('/fondos/cheque/create') }}" class="btn btn-themecolor btn-lg" title="Cargar nuevo Cheque">
                    <i class="fa fa-plus" aria-hidden="true"></i>
                    <span class="hidden-xs-down">Cargar nuevo Cheque</span>
                </a>
            </div>
        </div>
    </div>

    @tips($_SERVER['REQUEST_URI'])

    <div class="card">
        <div class="card-body">
            <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
            @include('includes.partials.messages')
            <div class="table-responsive">
                <table id="cheques-table" class="table selectable table-striped table-hover table-sm">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Nº</th>
                            <th>Banco</th>
                            <th>Importe</th>
                            <th>Vencimiento</th>
                            <th>Firmante</th>
                            <th>Estado</th>
                            <th>{{ trans('labels.general.actions') }}</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    @include('frontend.fondos.cheque.seleccionar_cuenta_bancaria_dialog')

@endsection

@section('after-scripts')
    {{ Html::script("js/frontend/plugins/datatables/jquery.dataTables.min.js") }}
    {{ Html::script("js/frontend/plugins/datatables/dataTables.responsive.min.js") }}
    {{ Html::script("js/frontend/plugins/datatables/dataTables.select.min.js") }}

<script>
    $(document).ready(function() {
        $('#cheques-table').DataTable({
            "aaSorting": [],
            processing: true,
            serverSide: true,
            responsive: {
                details: {
                    type: 'column',
                    target: 'tr'
                }
            },
            "columnDefs": [{
                targets: 0,
                className: 'control',
                orderable: false,
                searchable: false,
                defaultContent: '&nbsp;&nbsp;'
            },
                { responsivePriority: 1, targets: -1 }],
            ajax: '{!! route('frontend.fondos.ChequesData') !!}',
            columns: [
                {data: null, name: ''},
                {data: 'numero', name: 'numero'},
                {data: 'banco', name: 'banco'},
                {data: 'importe', name: 'importe'},
                {data: 'vencimiento', name: 'vencimiento'},
                {data: 'firmante', name: 'firmante'},
                {data: 'estado', name: 'estado'},
                {data: 'action', name: 'action', orderable: false, searchable: false}
            ]
        });
    } );

    $("body").on("click", ".delete", function () {
        var id = $(this).attr("data-id");
        swal({
            title: "{{trans('buttons.general.crud.delete').' Cheque'}}",
            text: "¿Realmente desea eliminar este registro?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#ff8726',
            confirmButtonText: "Eliminar"
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/fondos/cheque/'+id,
                    data: {"_method" : 'DELETE'},
                    success: function (msg) {
                        $("#" + id).hide(1);
                        swal("Eliminado!", "El registro ha sido correctamente eliminado.", "success");
                    }
                });
            }
        });
    });

    $("body").on("click", ".cobrar", function () {
        var id = $(this).attr("data-id");
        swal({
            title: "Cobrar Cheque",
            text: "Se generara el movimiento de caja en su caja actual. ¿Desea Continuar?",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: '#2EB800',
            confirmButtonText: "Cobrar"
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/fondos/cheque/cobrar/'+id,
                    data: {"_method" : 'GET'},
                    success: function (msg) {
                        swal("Hecho!", "Movimiento de caja generado.", "success").then((result) => { window.location.reload(); });
                    }
                });
            }
        });
    });

    function pasarId(idCheque){
      $('#idCheque').val(idCheque);
    }
</script>

@endsection
