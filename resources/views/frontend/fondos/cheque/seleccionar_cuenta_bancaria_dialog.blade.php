<div class="modal fade" id="modalDeposito" tabindex="-1" role="dialog" aria-labelledby="depositarChequeModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="depositarChequeModalLabel">Depositar Cheque</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar"><span aria-hidden="true">&times;</span></button>
            </div>
            {!! Form::open([
                'method'=>'POST',
                'action' => ['Frontend\Fondos\ChequeController@depositar'],
                'style' => 'display:inline'
            ]) !!}
            <div class="modal-body">
                <div class="form-group">
                    {!! Form::label('cuenta_bancaria_id', 'Cuenta Bancaria') !!}
                    {!! Form::select('cuenta_bancaria_id', $cuentas_bancarias, '', array('class' => 'form-control', 'required' => 'required')) !!}
                </div>
                {{ Form::hidden('idCheque',0,['id' => 'idCheque']) }}
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Depositar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>