@extends ('frontend.layouts.app')

@section ('title', trans('labels.frontend.cuenta_bancaria_titulo')." - ".trans('navs.frontend.user.administration'))

@section('after-styles')
    {{ Html::style("css/frontend/plugins/datatables/responsive.dataTables.min.css") }}
@endsection

@section('content')
    <div class="row page-titles">
        <div class="col-6 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('labels.frontend.cuenta_bancaria_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item active">{{ trans('labels.frontend.cuenta_bancaria_titulo') }}</li>
            </ol>
        </div>
        <div class="col-6">
            <div class="pull-right">
                <a href="{{ url('/fondos/cuenta-bancaria/create') }}" class="btn btn-themecolor btn-lg" title="{{ trans('buttons.general.crud.create').' '.trans('labels.frontend.cuenta_bancaria')  }}">
                    <i class="fa fa-plus" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans('buttons.general.crud.create').' '.trans('labels.frontend.cuenta_bancaria') }}</span>
                </a>
            </div>
        </div>
    </div>

    @tips($_SERVER['REQUEST_URI'])

    <div class="card">
        <div class="card-body">
            <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
            @include('includes.partials.messages')
            <div class="table-responsive">
                <table id="cuentasbancarias-table" class="table selectable table-striped table-hover table-sm">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Numero</th>
                            <th>Banco</th>
                            <th>Tipo</th>
                            <th>Moneda</th>
                            <th>Cbu</th>
                            <th>Cuit</th>
                            <th>Saldo</th>
                            <th>{{ trans('labels.general.actions') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($cuentasbancarias as $item)
                        <tr id="{{$item->id}}">
                            <td>&nbsp;&nbsp;</td>
                            <td>{{ $item->numero }}</td>
                            <td>{{ isset($item->banco) ? $item->banco : 'Ninguno' }}</td>
                            <td>
                                {{ $tipos[$item->tipo] }}
                            </td>
                            <td>
                                {{ isset($item->moneda) ? $item->moneda->nombre: 'Ninguno' }}
                            </td>
                            <td>{{$item->cbu}}</td>
                            <td>{{$item->cuit}}</td>
                            <td>$ {{$item->saldo}}</td>
                            <td>
                                <a href="{{ url('/fondos/cuenta-bancaria/' . $item->id) }}" class="mytooltip">
                                    <i class="fa fa-eye text-success m-r-10"></i>
                                    <span class="tooltip-content3">{{ trans('buttons.general.crud.show') }} {{ trans('labels.frontend.cuenta_bancaria') }}</span>
                                </a>
                                <a href="{{ url('/fondos/cuenta-bancaria/' . $item->id . '/edit') }}" class="mytooltip">
                                    <i class="fa fa-pencil text-info m-r-10"></i>
                                    <span class="tooltip-content3">{{ trans('buttons.general.crud.edit') }} {{ trans('labels.frontend.cuenta_bancaria') }}</span>
                                </a>
                                <a href="javascript:void(0)" class="delete mytooltip" data-id="{{$item->id}}">
                                    <i class="fa fa-close text-danger"></i>
                                    <span class="tooltip-content3">{{ trans('buttons.general.crud.delete') }} {{ trans('labels.frontend.cuenta_bancaria') }}</span>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection

@section('after-scripts')
    {{ Html::script("js/frontend/plugins/datatables/jquery.dataTables.min.js") }}
    {{ Html::script("js/frontend/plugins/datatables/dataTables.responsive.min.js") }}
    {{ Html::script("js/frontend/plugins/datatables/dataTables.select.min.js") }}

<script>
    $(document).ready(function() {
        $('#cuentasbancarias-table').DataTable({
            "aaSorting": [1,'asc'],
            responsive: {
                details: {
                    type: 'column',
                    target: 'tr'
                }
            },
            "columnDefs": [{
                targets: 0,
                className: 'control',
                orderable: false,
                searchable: false,
                defaultContent: '&nbsp;&nbsp;'
            },
                { responsivePriority: 1, targets: -1 }]
        });
    } );

    $("body").on("click", ".delete", function () {
        var id = $(this).attr("data-id");
        swal({
            title: "{{trans('buttons.general.crud.delete').' '.trans('labels.frontend.cuenta_bancaria')}}",
            text: "¿Realmente desea eliminar este registro?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#ff8726',
            confirmButtonText: "Eliminar"
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/fondos/cuenta-bancaria/'+id,
                    data: {"_method" : 'DELETE'},
                    success: function (msg) {
                        $("#" + id).hide(1);
                        swal("Eliminado!", "El registro ha sido correctamente eliminado.", "success");
                    }
                });
            }
        });
    });
</script>

@endsection
