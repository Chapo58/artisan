<div class="card">
    <div class="card-body">
        <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
        <br>
        <div class="row form-group">
            <div class="col-md-6">
                {!! Form::label('numero', 'Numero') !!}
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-hashtag"></i>
                    </div>
                    {!! Form::number('numero', null, ['class' => 'form-control']) !!}
                </div>
                {!! $errors->first('numero', '<p class="text-danger">:message</p>') !!}
            </div>
            <div class="col-md-6">
                {!! Form::label('banco_id', 'Banco') !!}
                {!! Form::select('banco_id', $bancos, isset($cuentasbancarias->banco) ? $cuentasbancarias->banco->id : '', array('class' => 'form-control')) !!}
                {!! $errors->first('banco_id', '<p class="text-danger">:message</p>') !!}
            </div>
        </div>
        <div class="row form-group">
            <div class="col-md-6">
                {!! Form::label('tipo', 'Tipo') !!}
                {!! Form::select('tipo', $tipos, isset($cuentasbancarias->tipo) ? $cuentasbancarias->tipo : 1, array('class' => 'form-control')) !!}
                {!! $errors->first('tipo', '<p class="text-danger">:message</p>') !!}
            </div>
            <div class="col-md-6">
                {!! Form::label('moneda_id', trans('Moneda')) !!}
                {!! Form::select('moneda_id', $monedas, isset($cuentasbancarias->moneda) ? $cuentasbancarias->moneda->id : '', array('class' => 'form-control', 'required' => 'required')) !!}
                {!! $errors->first('moneda_id', '<p class="text-danger">:message</p>') !!}
            </div>
        </div>
        <div class="row form-group">
            <div class="col-md-6">
                {!! Form::label('cbu', 'Cbu') !!}
                {!! Form::text('cbu', null, ['class' => 'form-control']) !!}
                {!! $errors->first('cbu', '<p class="text-danger">:message</p>') !!}
            </div>
            <div class="col-md-6">
                {!! Form::label('cuit', 'Cuit') !!}
                {!! Form::text('cuit', null, ['class' => 'form-control']) !!}
                {!! $errors->first('cuit', '<p class="text-danger">:message</p>') !!}
            </div>
        </div>
        <hr>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-12">
                    {{ Form::submit(isset($submitButtonText) ? $submitButtonText : trans('buttons.general.save'), ['class' => 'btn btn-lg btn-themecolor pull-right']) }}
                </div>
            </div>
        </div>
    </div>
</div>
