@extends('frontend.layouts.app')

@section ('title', trans('labels.frontend.cuenta_bancaria_titulo')." - ".trans('buttons.general.crud.edit'))

@section('content')
    <div class="row page-titles">
        <div class="col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('labels.frontend.cuenta_bancaria_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item"><a href="{{url('/fondos/cuenta-bancaria')}}">{{ trans('labels.frontend.cuenta_bancaria_titulo') }}</a></li>
                <li class="breadcrumb-item">{{ trans('buttons.general.crud.edit') }} {{ trans('labels.frontend.cuenta_bancaria') }}</li>
                <li class="breadcrumb-item active">{{ $cuentasbancarias }}</li>
            </ol>
        </div>
        <div class="col-4">
            <div class="pull-right">
                <a href="{{ url('/fondos/cuenta-bancaria') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning ">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans('buttons.general.crud.back') }}</span>
                </a>
            </div>
        </div>
    </div>

    @include('includes.partials.messages')

    {!! Form::model($cuentasbancarias, [
        'method' => 'PATCH',
        'url' => ['/fondos/cuenta-bancaria', $cuentasbancarias->id],
        'class' => 'form-horizontal',
        'files' => true
    ]) !!}

        @include ('frontend.fondos.cuenta-bancaria.form', ['submitButtonText' => trans("buttons.general.crud.update")])

    {!! Form::close() !!}

@endsection
