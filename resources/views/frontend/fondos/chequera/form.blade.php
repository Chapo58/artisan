<div class="card">
    <div class="card-body">
        <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
        <br>
        <div class="row form-group">
            <div class="col-md-12">
                {!! Form::label('cuenta_bancaria_id', trans('Cuenta Bancaria')) !!}
                {!! Form::select('cuenta_bancaria_id', $cuentaBancaria, isset($chequera->cuentaBancaria) ? $chequera->cuentaBancaria->id : '', array('class' => 'form-control', 'required' => 'required')) !!}
                {!! $errors->first('cuenta_bancaria_id', '<p class="text-danger">:message</p>') !!}
            </div>
        </div>
        <div class="row form-group">
            <div class="col-md-6">
                {!! Form::label('desde', 'Desde') !!}
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-hashtag"></i>
                    </div>
                    {!! Form::number('desde', null, ['class' => 'form-control', 'required' => 'required']) !!}
                </div>
                {!! $errors->first('desde', '<p class="text-danger">:message</p>') !!}
            </div>
            <div class="col-md-6">
                {!! Form::label('hasta', 'Hasta') !!}
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-hashtag"></i>
                    </div>
                    {!! Form::number('hasta', null, ['class' => 'form-control', 'required' => 'required']) !!}
                </div>
                {!! $errors->first('hasta', '<p class="text-danger">:message</p>') !!}
            </div>
        </div>
        <hr>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-12">
                    {{ Form::submit(isset($submitButtonText) ? $submitButtonText : trans('buttons.general.save'), ['class' => 'btn btn-lg btn-themecolor pull-right']) }}
                </div>
            </div>
        </div>
    </div>
</div>