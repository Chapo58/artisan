@extends('frontend.layouts.app')

@section ('title', trans('labels.frontend.transporte_titulo')." - ".trans('buttons.general.crud.create'))

@section('content')
    <div class="main-content">
        <div class="main-content-inner">
            @include ('frontend.includes.breadcrumbs')
            <div class="page-header">
                <h1>
                    {{ trans('labels.frontend.transporte_titulo') }}
                    <small>
                        <i class="ace-icon fa fa-angle-double-right"></i>
                        <small>{{ trans('buttons.general.crud.create') }} Transporte</small>
                    </small>

                    <div class="box-tools pull-right">
                        <a href="{{ url('/pedidos/transporte') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning btn-sm">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> {{ trans('buttons.general.crud.back') }}
                        </a>
                    </div>
                </h1>
            </div>

            <div class="row">
                <div class="col-xs-12">

                    @include('includes.partials.messages')

                    {!! Form::open(['url' => '/pedidos/transporte', 'class' => 'form-horizontal', 'files' => true]) !!}
                        @include ('frontend\pedidos.transporte.form')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
