<div class="form-group {{ $errors->has('codigo_interno') ? 'has-error' : ''}}">
    {!! Form::label('codigo_interno', 'Codigo Interno', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('codigo_interno', null, ['class' => 'form-control']) !!}
        {!! $errors->first('codigo_interno', '<p class="text-danger">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('nombre') ? 'has-error' : ''}}">
    {!! Form::label('nombre', 'Nombre', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('nombre', null, ['class' => 'form-control']) !!}
        {!! $errors->first('nombre', '<p class="text-danger">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('direccion') ? 'has-error' : ''}}">
    {!! Form::label('direccion', 'Direccion', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('direccion', null, ['class' => 'form-control']) !!}
        {!! $errors->first('direccion', '<p class="text-danger">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('codigo_postal') ? 'has-error' : ''}}">
    {!! Form::label('codigo_postal', 'Codigo Postal', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('codigo_postal', null, ['class' => 'form-control']) !!}
        {!! $errors->first('codigo_postal', '<p class="text-danger">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('telefono') ? 'has-error' : ''}}">
    {!! Form::label('telefono', 'Telefono', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('telefono', null, ['class' => 'form-control']) !!}
        {!! $errors->first('telefono', '<p class="text-danger">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
    {!! Form::label('email', 'Email', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('email', null, ['class' => 'form-control']) !!}
        {!! $errors->first('email', '<p class="text-danger">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {{ Form::submit(isset($submitButtonText) ? $submitButtonText : trans('buttons.general.save'), array('class' => 'btn btn-primary')) }}
    </div>
</div>
