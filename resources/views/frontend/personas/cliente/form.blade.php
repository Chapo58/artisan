<div class="card">
    <div class="card-body">
        <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
        <!-- Nav tabs -->
        <ul class="nav nav-tabs customtab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#info" role="tab">
                    <span class="hidden-sm-up"><i class="ti-info"></i></span>
                    <span class="hidden-xs-down">Información</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#adicionales" role="tab">
                    <span class="hidden-sm-up"><i class="ti-mobile"></i></span>
                    <span class="hidden-xs-down">Datos Adicionales</span>
                </a>
            </li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            <div class="tab-pane active" id="info" role="tabpanel">
                <div class="p-20">
                    <div class="row form-group">
                        <div class="col-md-4">
                            {!! Form::label('razon_social', 'Razon Social') !!}
                            {!! Form::text('razon_social', null, ['class' => 'form-control', 'required' => 'required']) !!}
                            {!! $errors->first('razon_social', '<p class="text-danger">:message</p>') !!}
                        </div>
                        <div class="col-md-4">
                            {!! Form::label('dni', 'DNI/CUIT') !!}
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-hashtag"></i>
                                </div>
                                {!! Form::text('dni', null, ['class' => 'form-control', 'required' => 'required']) !!}
                            </div>
                            {!! $errors->first('dni', '<p class="text-danger">:message</p>') !!}
                        </div>
                        <div class="col-md-4">
                            {!! Form::label('condicion_iva_id', 'Condición Iva') !!}
                            {!! Form::select('condicion_iva_id', $condiciones_iva, isset($cliente->condicionIva) ? $cliente->condicionIva->id : '', array('class' => 'form-control')) !!}
                            {!! $errors->first('condicion_iva_id', '<p class="text-danger">:message</p>') !!}
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-6">
                            {!! Form::label('direccion', 'Dirección') !!}
                            {!! Form::text('domicilio[direccion]', null, ['class' => 'form-control', 'required' => 'required','id' => 'direccion']) !!}
                            {!! $errors->first('direccion', '<p class="text-danger">:message</p>') !!}
                        </div>
                        <div class="col-md-6">
                            {!! Form::label('localidad', 'Localidad') !!}
                            {!! Form::text('domicilio[localidad]', null, ['class' => 'form-control', 'required' => 'required','id' => 'localidad']) !!}
                            {!! $errors->first('localidad', '<p class="text-danger">:message</p>') !!}
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-4">
                            {!! Form::label('codigo_postal', 'Código Postal') !!}
                            {!! Form::text('domicilio[codigo_postal]', null, ['class' => 'form-control','id' => 'codigo_postal']) !!}
                            {!! $errors->first('codigo_postal', '<p class="text-danger">:message</p>') !!}
                        </div>
                        <div class="col-md-4">
                            {!! Form::label('provincia_id', 'Provincia') !!}
                            {!! Form::select('domicilio[provincia_id]', $provincias, isset($cliente->domicilio->provincia) ? $cliente->domicilio->provincia->id : '', ['id' => 'provincia_id', 'class' => 'form-control', 'required' => 'required']) !!}
                            {!! $errors->first('provincia_id', '<p class="text-danger">:message</p>') !!}
                        </div>
                        <div class="col-md-4">
                            {!! Form::label('pais_id', 'País') !!}
                            {!! Form::select('domicilio[pais_id]', $paises, isset($cliente->domicilio->pais) ? $cliente->domicilio->pais->id : 1, ['id' => 'pais_id', 'class' => 'form-control', 'required' => 'required']) !!}
                            {!! $errors->first('pais_id', '<p class="text-danger">:message</p>') !!}
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-6">
                            {!! Form::label('saldo_maximo', 'C.C. Saldo Máximo') !!}
                            <i class="fa fa-question-circle text-info mytooltip">
                                <span class="tooltip-content3">Importe limite en pesos que el cliente puede deber en cuentas corrientes</span>
                            </i>
                            {!! Form::text('saldo_maximo', null, ['class' => 'form-control']) !!}
                            {!! $errors->first('saldo_maximo', '<p class="text-danger">:message</p>') !!}
                        </div>
                        <div class="col-md-6">
                            {!! Form::label('lista_precios_id', 'Lista Precio') !!}
                            <i class="fa fa-question-circle text-info mytooltip">
                                <span class="tooltip-content3">Lista de precios por defecto que se utilizaran en las ventas a este cliente</span>
                            </i>
                            {!! Form::select('lista_precios_id', $lista_precios, isset($cliente->listaPrecio) ? $cliente->listaPrecio->id : '', array('class' => 'form-control')) !!}
                            {!! $errors->first('lista_precios_id', '<p class="text-danger">:message</p>') !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane p-20" id="adicionales" role="tabpanel">
                <div class="row form-group">
                    <div class="col-md-6">
                        {!! Form::label('email', 'Email') !!}
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-at"></i>
                            </div>
                            {!! Form::email('email', null, ['class' => 'form-control']) !!}
                        </div>
                        {!! $errors->first('email', '<p class="text-danger">:message</p>') !!}
                    </div>
                    <div class="col-md-6">
                        {!! Form::label('contacto', 'Contacto') !!}
                        {!! Form::text('contacto', null, ['class' => 'form-control']) !!}
                        {!! $errors->first('contacto', '<p class="text-danger">:message</p>') !!}
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-6">
                        {!! Form::label('telefono', 'Teléfono') !!}
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa  fa-phone"></i>
                            </div>
                            {!! Form::text('telefono', null, ['class' => 'form-control telefono']) !!}
                        </div>
                        {!! $errors->first('telefono', '<p class="text-danger">:message</p>') !!}
                    </div>
                    <div class="col-md-6">
                        {!! Form::label('telefono2', 'Telefono alternativo') !!}
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa  fa-phone"></i>
                            </div>
                            <input class="form-control telefono" name="domicilio[telefono2]" id="telefono2" type="text" value="{{ isset($proveedor->domicilio) ? $proveedor->domicilio->telefono2 : '' }}">
                        </div>
                        {!! $errors->first('telefono2', '<p class="text-danger">:message</p>') !!}
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-12">
                    {{ Form::submit(isset($submitButtonText) ? $submitButtonText : trans('buttons.general.save'), ['class' => 'btn btn-lg btn-themecolor pull-right']) }}
                </div>
                <div class="col-md-6"> </div>
            </div>
        </div>
    </div>
</div>

@include('backend.configuraciones.provincia.provincia_dialog')
@include('backend.configuraciones.localidad.localidad_dialog')