@extends('frontend.layouts.app')

@section ('title', trans('labels.frontend.cliente_titulo')." - ".trans('buttons.general.crud.edit'))

@section('content')
    <div class="row page-titles">
        <div class="col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('labels.frontend.cliente_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item"><a href="{{url('/personas/cliente')}}">{{ trans('labels.frontend.cliente_titulo') }}</a></li>
                <li class="breadcrumb-item">{{ trans('buttons.general.crud.edit') }} {{ trans('labels.frontend.cliente') }}</li>
                <li class="breadcrumb-item active">{{ $cliente }}</li>
            </ol>
        </div>
        <div class="col-4">
            <div class="pull-right">
                <a href="{{ url('/personas/cliente') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning ">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans('buttons.general.crud.back') }}</span>
                </a>
            </div>
        </div>
    </div>

    {!! Form::model($cliente, [
        'method' => 'PATCH',
        'url' => ['/personas/cliente', $cliente->id],
        'class' => 'form-horizontal',
        'files' => true
    ]) !!}

        @include ('frontend.personas.cliente.form', ['submitButtonText' => trans("buttons.general.crud.update")])

    {!! Form::close() !!}

@endsection

@section('after-scripts')
    {{ Html::script("js/frontend/plugin/maskedinput/jquery.maskedinput.min.js") }}
    <script>
        $( document ).ready(function() {
            $('input#dni').mask("99999999?999");
        });
    </script>

    @yield('scripts-dialog-provincia')
    @yield('scripts-dialog-localidad')
@endsection