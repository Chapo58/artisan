<div class="modal fade" id="modalImportarClientes" tabindex="-1" role="dialog" aria-labelledby="importarClientesModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="importarClientesModalLabel">Importar Clientes</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar"><span aria-hidden="true">&times;</span></button>
            </div>
            {!! Form::open([
                'method'=>'POST',
                'action' => ['Frontend\Personas\ClienteController@importarClientes'],
                'style' => 'display:inline',
                'id' => 'form_importar',
                'files' => true
            ]) !!}
            <div class="modal-body">
                <div class="form-group">
                    {!! Form::label('archivo', 'Seleccionar Archivo') !!}
                    {!! Form::file('archivo', ['required' => 'required']) !!}
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-themecolor">Importar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
