@extends ('frontend.layouts.app')

@section ('title', trans('labels.frontend.cliente_titulo')." - ".trans('navs.frontend.user.administration'))

@section('after-styles')
    {{ Html::style("css/frontend/plugins/datatables/responsive.dataTables.min.css") }}
@endsection

@section('content')
    <div class="row page-titles">
        <div class="col-6 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('labels.frontend.cliente_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item active">{{ trans('labels.frontend.cliente_titulo') }}</li>
            </ol>
        </div>
        <div class="col-6">
            <div class="pull-right">
                <a href="{{ url('/personas/cliente/create') }}" class="btn btn-themecolor" title="{{ trans('buttons.general.crud.create').' '.trans('labels.frontend.cliente')  }}">
                    <i class="fa fa-plus" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans('buttons.general.crud.create2').' '.trans('labels.frontend.cliente') }}</span>
                </a>
                <a href="{{ url('/personas/cliente/plantilla') }}" target="_blank" class="btn btn-info" title="Plantilla">
                    <i class="fa fa-file-excel-o" aria-hidden="true"></i>
                    <span class="hidden-xs-down">Plantilla</span>
                </a>
                <a href="{{ url('/personas/cliente/exportar') }}" target="_blank" class="btn btn-primary" title="Exportar">
                    <i class="fa fa-cloud-download" aria-hidden="true"></i>
                    <span class="hidden-xs-down">Exportar</span>
                </a>
                @if($user->email != 'demo@ciatt.com.ar')
                <button type="button" data-backdrop="static" data-toggle="modal" data-target="#modalImportarClientes" class="btn btn-warning" title="Importar Clientes">
                    <span class="fa fa-cloud-upload"></span>
                    <span class="hidden-xs-down">Importar</span>
                </button>
                @endif
            </div>
        </div>
    </div>

    @tips($_SERVER['REQUEST_URI'])

    <div class="card">
        <div class="card-body">
            <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
            @include('includes.partials.messages')
            <table id="cliente-table" class="table selectable table-striped table-hover table-sm">
                <thead>
                    <tr>
                        <th></th>
                        <th>Razón Social</th>
                        <th>Condición Iva</th>
                        <th>DNI/CUIT</th>
                        <th>E-mail</th>
                        <th>Teléfono</th>
                        <th>{{ trans('labels.general.actions') }}</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>

    @include('frontend.personas.cliente.importar_clientes_dialog')

@endsection

@section('after-scripts')
    {{ Html::script("js/frontend/plugins/datatables/jquery.dataTables.min.js") }}
    {{ Html::script("js/frontend/plugins/datatables/dataTables.responsive.min.js") }}
    {{ Html::script("js/frontend/plugins/datatables/dataTables.select.min.js") }}

    <script>
        $(document).ready(function() {
            $('#cliente-table').DataTable({
                processing: true,
                serverSide: true,
                responsive: {
                    details: {
                        type: 'column',
                        target: 'tr'
                    }
                },
                "columnDefs": [{
                    targets: 0,
                    className: 'control',
                    orderable: false,
                    searchable: false,
                    defaultContent: '&nbsp;&nbsp;'
                },
                    { responsivePriority: 1, targets: -1 }],
                ajax: '{!! route('frontend.personas.ClientesData') !!}',
                columns: [
                    {data: null, name: ''},
                    {data: 'razon_social', name: 'razon_social'},
                    {data: 'condicioniva', name: 'condicioniva'},
                    {data: 'dni', name: 'dni'},
                    {data: 'email', name: 'email'},
                    {data: 'telefono', name: 'telefono'},
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ]
            });
        });

        $("#form_importar").on('submit',function(event){
            $('#modalImportarClientes').modal('toggle');

            swal({
                title: 'Importando Clientes',
                text: 'Importando Clientes... Por favor aguarde...',
                allowOutsideClick: false,
                timer: 150000,
                onOpen: () => {
                    swal.showLoading()
                }
            })
        });

        $("body").on("click", ".delete", function () {
            var id = $(this).attr("data-id");
            swal({
                title: "{{trans('buttons.general.crud.delete').' '.trans('labels.frontend.cliente')}}",
                text: "¿Realmente desea eliminar este registro?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#ff8726',
                confirmButtonText: "Eliminar"
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        type: 'POST',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: '/personas/cliente/'+id,
                        data: {"_method" : 'DELETE'},
                        success: function (msg) {
                            $("#" + id).hide(1);
                            swal("Eliminado!", "El registro ha sido correctamente eliminado.", "success");
                        }
                    });
                }
            });
        });
    </script>

@endsection
