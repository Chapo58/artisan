<table>
    <thead>
        <tr>
            <th>Razon Social</th>
            <th>DNI/CUIT</th>
            <th>Condición IVA</th>
            <th>Dirección</th>
            <th>Localidad</th>
            <th>Provincia</th>
            <th>Telefono</th>
            <th>Email</th>
        </tr>
    </thead>
    <tbody>
        @foreach($clientes as $cliente)
            <tr>
                <td>{{ $cliente->razon_social }}</td>
                <td>{{ $cliente->dni }}</td>
                <td>{{ $cliente->condicionIva }}</td>
                <td>{{ ($cliente->domicilio) ? $cliente->domicilio->direccion : '' }}</td>
                <td>{{ ($cliente->domicilio) ? $cliente->domicilio->localidad : '' }}</td>
                <td>{{ ($cliente->domicilio) ? $cliente->domicilio->provincia : '' }}</td>
                <td>{{ $cliente->telefono }}</td>
                <td>{{ $cliente->email }}</td>
            </tr>
        @endforeach
    </tbody>
</table>

