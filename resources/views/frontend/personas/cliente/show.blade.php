@extends('frontend.layouts.app')

@section ('title', trans('labels.frontend.cliente_titulo')." - ".trans('buttons.general.crud.show'))

@section('after-styles')
    {{ Html::style("css/frontend/plugins/datatables/responsive.dataTables.min.css") }}
@endsection

@section('content')
    <div class="row page-titles">
        <div class="col-6 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('labels.frontend.cliente_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item"><a href="{{url('/personas/cliente')}}">{{ trans('labels.frontend.cliente_titulo') }}</a></li>
                <li class="breadcrumb-item">{{ trans('buttons.general.crud.show') }} {{ trans('labels.frontend.cliente') }}</li>
                <li class="breadcrumb-item active">{{ $cliente }}</li>
            </ol>
        </div>
        <div class="col-6">
            <div class="pull-right">
                <a href="{{ url('/personas/cliente') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning ">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans('buttons.general.crud.back') }}</span>
                </a>
                <a href="{{ url('/personas/cliente/' . $cliente->id . '/edit') }}" title="{{ trans('buttons.general.crud.edit') }}" class="btn btn-info ">
                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans('buttons.general.crud.edit') }}</span>
                </a>
                <a href="{{ url('/ventas/recibo/crearReciboCliente/' . $cliente->id) }}" class="btn btn-primary ">
                    <i class="fa fa-plus" aria-hidden="true"></i>
                    <span class="hidden-xs-down">Recibo</span>
                </a>
                <a href="javascript:void(0)" title="{{trans("buttons.general.crud.delete")." ".trans("labels.frontend.cliente")}}" class="btn btn-danger  delete">
                    <i class="fa fa-trash" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans("buttons.general.crud.delete") }}</span>
                </a>
            </div>
        </div>
    </div>

    @tips($_SERVER['REQUEST_URI'])

    <div class="card">
        <div class="card-body">
            <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
            <ul class="nav nav-tabs profile-tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#info" role="tab">
                        <span class="hidden-sm-up"><i class="fa fa-info-circle"></i></span>
                        <span class="hidden-xs-down"><i class="fa fa-info-circle"></i> Información</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#ventas" role="tab">
                        <span class="hidden-sm-up"><i class="fa fa-ticket"></i></span>
                        <span class="hidden-xs-down"><i class="fa fa-ticket"></i> Ventas</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#cc" role="tab">
                        <span class="hidden-sm-up"><i class="fa fa-drivers-license-o"></i></span>
                        <span class="hidden-xs-down"><i class="fa fa-drivers-license-o"></i> Cuenta Corriente</span>
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="info" role="tabpanel">
                    <h4 class="font-medium m-t-30">{{ $cliente->razon_social }}</h4>
                    <hr>
                    <div class="row">
                        <div class="col-md-3 col-xs-6"> <span class="font-bold">DNI/CUIT</span>
                            <br>
                            <p class="text-muted">
                                @if(strlen($cliente->dni) == 8)
                                    {{  substr($cliente->dni, 0, 2) }}.{{ substr($cliente->dni, 2, 3) }}.{{ substr($cliente->dni, 5, 3) }}
                                @else
                                    {{  substr($cliente->dni, 0, 2) }}-{{ substr($cliente->dni, 2, 8) }}-{{ substr($cliente->dni, 10, 1) }}
                                @endif
                            </p>
                        </div>
                        <div class="col-md-3 col-xs-6 b-r"> <span class="font-bold">Teléfono</span>
                            <br>
                            <p class="text-muted">{{ $cliente->telefono }}</p>
                        </div>
                        <div class="col-md-3 col-xs-6 b-r"> <span class="font-bold">Email</span>
                            <br>
                            <p class="text-muted">{{ $cliente->email }}</p>
                        </div>
                        <div class="col-md-3 col-xs-6 b-r"> <span class="font-bold">Domicilio</span>
                            <br>
                            <p class="text-muted">{{ $cliente->domicilio }}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2 col-xs-12 font-weight-bold">Codición Iva</div>
                        <div class="col-md-4 col-xs-12">{{ isset($cliente->condicionIva) ? $cliente->condicionIva->nombre : '' }}</div>

                        <div class="col-md-2 col-xs-12 font-weight-bold">Lista de Precios</div>
                        <div class="col-md-4 col-xs-12">{{ isset($cliente->listaPrecio) ? $cliente->listaPrecio->nombre : '' }}</div>
                    </div><br>
                    <div class="row">
                        <div class="col-md-2 col-xs-12 font-weight-bold">Saldo Máximo C.C.</div>
                        <div class="col-md-4 col-xs-12">{{ $cliente->saldo_maximo }}</div>

                        <div class="col-md-2 col-xs-12 font-weight-bold">Saldo Actual C.C.</div>
                        <div class="col-md-4 col-xs-12">
                            @if($cliente->saldo_actual > $cliente->saldo_maximo)
                                <b class="text-danger"> $ {{ $cliente->saldo_actual }} </b>
                            @elseif($cliente->saldo_actual > 0)
                                <b style="color:#F7D05B"> $ {{ $cliente->saldo_actual }} </b>
                            @else
                                <b class="text-success"> $ {{ $cliente->saldo_actual }} </b>
                            @endif
                        </div>
                    </div><br>
                    <div class="row">
                        <div class="col-md-3 col-xs-12 font-weight-bold">Contacto</div>
                        <div class="col-md-3 col-xs-12">{{ $cliente->contacto }}</div>
                    </div><br>
                    <div class="row">
                        <div class="col-md-3 col-xs-12 font-weight-bold">Creado el</div>
                        <div class="col-md-3 col-xs-12">{{ $cliente->created_at->format('d/m/Y H:m:s') }}</div>
                    </div><br><div class="row">
                        <div class="col-md-3 col-xs-12 font-weight-bold">Última modificación</div>
                        <div class="col-md-3 col-xs-12">{{ $cliente->updated_at->format('d/m/Y H:m:s') }}</div>
                    </div><br>
                </div>
                <div class="tab-pane" id="ventas" role="tabpanel">
                    <table id="venta" class="table selectable table-bordered table-striped color-table muted-table form-material" style="width: 100%">
                        <thead>
                       <tr style="background:#99abb4;color:#fff;">
                            <th></th>
                            <th>Número</th>
                            <th>Fecha</th>
                            <th>Lista Usada</th>
                            <th>Vendedor</th>
                            <th>Total</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($ventas as $item)
                            <tr {{ ($item->estado_facturacion == 2) ? 'class=danger' : '' }}>
                                <td>&nbsp;&nbsp;</td>
                                <td>
                                    <a href="{{url('/ventas/venta/'.$item->id)}}">
                                        {{ $tipos_comprobantes[$item->tipo_comprobante_id] }} {{ $item->puntoVenta }}-{{ $item->numero }}
                                    </a>
                                </td>
                                <td>{{ ($item->fecha) ? $item->fecha->format('d/m/Y H:i') : $item->created_at->format('d/m/Y H:i') }}</td>
                                <td>{{ $item->listaPrecios}}</td>
                                <td>{{ isset($item->usuario) ? $item->usuario : '' }}</td>
                                <td>{{ $item->moneda->signo }} {{ $item->total_cobro }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="row">
                        <div class="col-md-3 col-lg-4"></div>
                        <div class="col-md-6 col-lg-4 col-xlg-2">
                            <div class="card card-inverse card-dark">
                                <div class="box text-center">
                                    <h1 class="font-light text-white">$ {{$ventas->sum('total_cobro')}}</h1>
                                    <h6 class="text-white">Importe Total Vendido</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="cc" role="tabpanel">
                     <table id="cuentacorriente" class="table selectable table-bordered table-striped color-table form-material" style="width: 100%">
                        <thead>
                        <tr style="background:#99abb4;color:#fff;">
                            <th></th>
                            <th>Fecha</th>
                            <th>Comprabante</th>
                            <th>Tipo de Pago</th>
                            <th>Vencimiento</th>
                            <th>Debe</th>
                            <th>Haber</th>
                            <th>Saldo</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($cuentasCorrientes as $item)
                            <tr id="{{$item->id}}">
                                <td>&nbsp;&nbsp;</td>
                                <td>{{ $item->created_at->format('d/m/Y H:i') }}</td>
                                <td>
                                    <a href="{{ url($item->url).'/'.$item->entidad_id }}">
                                        {{ $tipos_comprobantes[$item->tipo_comprobante] }} {{ $item->detalle }}
                                    </a>
                                </td>
                                <td>{{ ($item->tipo_cuenta_corriente_id) ? $item->tipoCuentaCorriente : '' }}</td>
                                <td>{{ ($item->proximo_vencimiento) ? $item->proximo_vencimiento->format('d/m/Y') : '' }}</td>
                                <td><b class="text-danger">$ {{ $item->debe }}</b></td>
                                <td><b class="text-success">$ {{ $item->haber }}</b></td>
                                <td>
                                    <b class="@if($item->debe > $item->haber) text-danger @else text-success @endif">
                                        @if($item->debe >= $item->haber)
                                            $ {{ ($item->debe - $item->haber) }}
                                        @else
                                            $ {{ ($item->haber - $item->debe) }}
                                        @endif
                                    </b>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    @if($cuentasCorrientes->sum('debe') - $cuentasCorrientes->sum('haber') > 0)
                            <div class="card card-danger card-inverse">
                                <div class="box text-center">
                                    <h1 class="font-light text-white">$ {{ number_format(($cuentasCorrientes->sum('debe') - $cuentasCorrientes->sum('haber')), 2,',','.') }}</h1>
                                    <h6 class="text-white">Saldo</h6>
                                </div>
                            </div>
                    @elseif($cuentasCorrientes->sum('debe') - $cuentasCorrientes->sum('haber') < 0)
                            <div class="card card-success card-inverse">
                                <div class="box text-center">
                                    <h1 class="font-light text-white">$ {{ number_format(($cuentasCorrientes->sum('haber') - $cuentasCorrientes->sum('debe')), 2,',','.') }}</h1>
                                    <h6 class="text-white">Saldo A FAVOR</h6>
                                </div>
                            </div>
                    @else
                            <div class="card card-inverse card-dark">
                                <div class="box text-center">
                                    <h1 class="font-light text-white">$ 0,00</h1>
                                    <h6 class="text-white">Sin Saldo</h6>
                                </div>
                            </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

@endsection

@section('after-scripts')

    {{ Html::script("js/frontend/plugins/datatables/jquery.dataTables.min.js") }}
    {{ Html::script("js/frontend/plugins/datatables/dataTables.responsive.min.js") }}
    {{ Html::script("js/frontend/plugins/datatables/dataTables.select.min.js") }}

<script>

      $(document).ready(function() {
            $('#venta').DataTable({
                "aaSorting": [1,'asc'],
                "info":     false,
                "searching":   false,
                responsive: {
                    details: {
                        type: 'column',
                        target: 'tr'
                    }
                },
                "columnDefs": [{
                    targets: 0,
                    className: 'control',
                    orderable: false,
                    searchable: false,
                    defaultContent: '&nbsp;&nbsp;'
                },
                { responsivePriority: 1, targets: -1 },
                { responsivePriority: 1, targets: -2 },
                { responsivePriority: 1, targets: 1 }],
            });
        } );

        $(document).ready(function() {
            $('#cuentacorriente').DataTable({
                "aaSorting": [1,'asc'],
                "paging":   false,
                "info":     false,
                "searching":   false,
                "ordering":false,
                responsive: {
                    details: {
                        type: 'column',
                        target: 'tr'
                    }
                },
                "columnDefs": [{
                    targets: 0,
                    className: 'control',
                    orderable: false,
                    searchable: false,
                    defaultContent: '&nbsp;&nbsp;'
                },
                { responsivePriority: 1, targets: -1 },
                { responsivePriority: 1, targets: -2 },
                { responsivePriority: 1, targets: 1 }],
            });
        } );



    $("body").on("click", ".delete", function () {
        swal({
            title: "{{trans('buttons.general.crud.delete').' '.trans('labels.frontend.cliente')}}",
            text: "¿Realmente desea eliminar este registro?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#ff8726',
            confirmButtonText: "Eliminar"
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '{{ url('/personas/cliente/'.$cliente->id) }}',
                    data: {"_method" : 'DELETE'},
                    success: function (msg) {
                        swal("Eliminado!", "El registro ha sido correctamente eliminado.", "success").then((result) => { window.location.href = '{{url('/personas/cliente')}}'; });
                    }
                });
            }
        });
    });
</script>

@endsection