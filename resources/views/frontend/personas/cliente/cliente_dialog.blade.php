<div class="modal fade" id="modal-cliente-nuevo" tabindex="-1" role="dialog" aria-labelledby="clienteModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="clienteModalLabel">Nuevo Cliente</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    {!! Form::label('cliente_razon_social', 'Razón Social') !!}
                    {!! Form::text('cliente_razon_social', '',['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('cliente_dni', 'DNI/CUIT') !!}
                    <div class="input-group">
                        <div class="input-group-addon"><i class="fa fa-hashtag"></i></div>
                        {!! Form::text('cliente_dni', '',['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('condicion_iva_id', 'Condicion IVA') !!}
                    {!! Form::select('cliente_condicion_iva_id', $condiciones_iva, '', array('class' => 'form-control', 'id' => 'cliente_condicion_iva_id')) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('cliente_telefono', 'Telefono') !!}
                    <div class="input-group">
                        <div class="input-group-addon"><i class="fa fa-phone"></i></div>
                        {!! Form::text('cliente_telefono', '',['class' => 'form-control']) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button id="guardar-cliente" type="button" class="btn btn-themecolor">Guardar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>

@section('scripts-dialog-cliente')
    {{ Html::script("js/frontend/plugins/maskedinput/jquery.maskedinput.min.js") }}
    <script>
        $('input#cliente_dni').mask("99999999?999");
        $("#agregar-cliente").click(function(){
            $('input#cliente_razon_social').parent().removeClass('has-danger');
            $('input#cliente_dni').parent().removeClass('has-danger');
            $('select#cliente_condicion_iva_id').parent().removeClass('has-danger');
            $('input#cliente_razon_social').val('');
            $('input#cliente_dni').val('');
            $('select#cliente_condicion_iva_id').val('');
            $('#modal-cliente-nuevo').modal();
        });

        $("#guardar-cliente").click(function(){
            $('input#cliente_razon_social').parent().removeClass('has-danger');
            $('input#cliente_dni').parent().removeClass('has-danger');
            $('select#cliente_condicion_iva_id').parent().removeClass('has-danger');

            if($('input#cliente_razon_social').val() == ''){
                $('input#cliente_razon_social').parent().addClass('has-danger');
                $('input#cliente_razon_social').focus();
            }else if($('input#cliente_dni').val() == ''){
                $('input#cliente_dni').parent().addClass('has-danger');
                $('input#cliente_dni').focus();
            }else if($('select#cliente_condicion_iva_id').val() == ''){
                $('select#cliente_condicion_iva_id').parent().addClass('has-danger');
                $('select#cliente_condicion_iva_id').focus();
            }else{
                $('#modal-cliente-nuevo').modal('hide');
                guardarNuevoCliente();
            }
        });

        function guardarNuevoCliente() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $( "input[name*='_token']" ).val()
                }
            });
            var razon_social = $('#cliente_razon_social').val();
            var dni = $('#cliente_dni').val();
            var telefono = $('#cliente_telefono').val();
            var condicion_iva_id = $('#cliente_condicion_iva_id').val();
            $.ajax({
                type: "POST",
                url: '{{ url("/personas/cliente/guardarNuevoCliente") }}',
                data: {razon_social: razon_social, dni: dni, telefono: telefono, condicion_iva_id: condicion_iva_id},
                success: function( msg ) {
                    $("#buscar_cliente").val($('#cliente_razon_social').val());
                    $('input#cliente_id').val(msg);
                }
            });
        }
    </script>
@endsection