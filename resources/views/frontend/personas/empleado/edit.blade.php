@extends('frontend.layouts.app')

@section ('title', trans('labels.frontend.empleado_titulo')." - ".trans('buttons.general.crud.edit'))

@section('content')
    <div class="main-content">
        <div class="main-content-inner">
            @include ('frontend.includes.breadcrumbs')
            <div class="page-header">
                <h1>
                    {{ trans('labels.frontend.empleado_titulo') }}
                    <small>
                        <i class="ace-icon fa fa-angle-double-right"></i>
                        <small>{{ trans('buttons.general.crud.edit') }} Empleado </small>
                        <small><i class="ace-icon fa fa-angle-double-right"></i></small>
                        <small>{{ $empleado->id }}# {{ $empleado }}</small>
                    </small>

                    <div class="box-tools pull-right">
                        <a href="{{ url('/personas/empleado') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning ">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> {{ trans('buttons.general.crud.back') }}
                        </a>
                    </div>
                </h1>
            </div>

            <div class="row">
                <div class="col-xs-12">

                    @include('includes.partials.messages')

                    {!! Form::model($empleado, [
                        'method' => 'PATCH',
                        'url' => ['/personas/empleado', $empleado->id],
                        'class' => 'form-horizontal',
                        'files' => true
                    ]) !!}

                        @include ('frontend.personas.empleado.form', ['submitButtonText' => trans("buttons.general.crud.update")])

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('after-scripts')
    {{ Html::script("js/frontend/plugin/maskedinput/jquery.maskedinput.min.js") }}
    <script>
        $( document ).ready(function() {
            $('input#cuil').mask("99-99999999-9",{placeholder:"##-########-#"});
            $('input.telefono').mask("999 999-9999",{placeholder:"### ###-####"});
        });
    </script>
@endsection