<div class="form-group {{ $errors->has('nombre') ? 'has-error' : ''}}">
    {!! Form::label('nombre', 'Nombre', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('nombre', null, ['class' => 'form-control']) !!}
        {!! $errors->first('nombre', '<p class="text-danger">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('cuil') ? 'has-error' : ''}}">
    {!! Form::label('cuil', 'Cuil', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <div class="input-group">
            <div class="input-group-addon">
                <i class="fa fa-hashtag"></i>
            </div>
            <input class="form-control" required="required" name="cuil" id="cuil" type="text" value="{{ isset($empleado) ? $empleado->cuil : ''}}">
        </div>
        {!! $errors->first('cuil', '<p class="text-danger">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('sucursal_id') ? 'has-error' : ''}}">
    {!! Form::label('sucursal_id', 'Sucursal', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::select('sucursal_id', $sucursales, isset($empleado->sucursal) ? $empleado->sucursal_id : '', array('class' => 'form-control')) !!}
        {!! $errors->first('sucursal_id', '<p class="text-danger">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('caja_id') ? 'has-error' : ''}}">
    {!! Form::label('caja_id', 'Caja por defecto', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::select('caja_id', $cajas, isset($empleado->caja) ? $empleado->caja_id : '', array('class' => 'form-control')) !!}
        {!! $errors->first('caja_id', '<p class="text-danger">:message</p>') !!}
    </div>
</div>
<div class="accordion well" id="accordion2">
    <div class="accordion-group">
        <div class="accordion-heading">
            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
                Datos adicionales
            </a>
        </div>
        <div id="collapseOne" class="accordion-body collapse in">
            <div class="form-group {{ $errors->has('direccion') ? 'has-error' : ''}}">
                {!! Form::label('direccion', 'Direccion', ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-6">
                    {!! Form::text('domicilio[direccion]', null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('direccion', '<p class="text-danger">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('pais_id') ? 'has-error' : ''}}">
                {!! Form::label('pais_id', 'Pais', ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-6">
                    {!! Form::select('domicilio[pais_id]', $paises, isset($empleado->domicilio->pais) ? $empleado->domicilio->pais->id : 1, array('class' => 'form-control', 'required' => 'required')) !!}
                    {!! $errors->first('pais_id', '<p class="text-danger">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('provincia_id') ? 'has-error' : ''}}">
                {!! Form::label('provincia_id', 'Provincia', ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-6">
                    {!! Form::select('domicilio[provincia_id]', $provincias, isset($empleado->domicilio->provincia) ? $empleado->domicilio->provincia->id : '', array('class' => 'form-control', 'required' => 'required')) !!}
                    {!! $errors->first('provincia_id', '<p class="text-danger">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('localidad') ? 'has-error' : ''}}">
                {!! Form::label('localidad', 'Localidad', ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-6">
                    {!! Form::text('domicilio[localidad]', null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('localidad', '<p class="text-danger">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('codigo_postal') ? 'has-error' : ''}}">
                {!! Form::label('codigo_postal', 'Codigo Postal', ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-6">
                    {!! Form::text('domicilio[codigo_postal]', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('codigo_postal', '<p class="text-danger">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('telefono') ? 'has-error' : ''}}">
                {!! Form::label('telefono', 'Telefono', ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-6">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa  fa-phone"></i>
                        </div>
                        <input class="form-control telefono" name="domicilio[telefono]" id="domicilio[telefono]" type="text" value="{{ isset($empleado->domicilio) ? $empleado->domicilio->telefono : '' }}">
                    </div>
                    {!! $errors->first('telefono', '<p class="text-danger">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('telefono2') ? 'has-error' : ''}}">
                {!! Form::label('telefono2', 'Telefono alternativo', ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-6">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa  fa-phone"></i>
                        </div>
                        <input class="form-control telefono" name="domicilio[telefono2]" id="domicilio[telefono2]" type="text" value="{{ isset($empleado->domicilio) ? $empleado->domicilio->telefono2 : '' }}">
                    </div>
                    {!! $errors->first('telefono2', '<p class="text-danger">:message</p>') !!}
                </div>
            </div>
        </div>
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {{ Form::submit(isset($submitButtonText) ? $submitButtonText : trans('buttons.general.save'), array('class' => 'btn btn-primary')) }}
    </div>
</div>
