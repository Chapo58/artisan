@extends ('frontend.layouts.app')

@section ('title', trans('labels.frontend.empleado_titulo')." - ".trans('navs.frontend.user.administration'))

@section('after-styles')
    {{ Html::style("css/backend/plugin/datatables/dataTables.bootstrap.min.css") }}
@endsection

@section('content')
    <div class="main-content">
        <div class="main-content-inner">
            @include ('frontend.includes.breadcrumbs')
            <div class="page-header">
                <h1>
                    {{ trans('labels.frontend.empleado_titulo') }}
                    <small>
                        <i class="ace-icon fa fa-angle-double-right"></i>
                        <small>{{ trans('navs.frontend.user.administration') }}</small>
                    </small>

                    <div class="box-tools pull-right">
                        <a href="{{ url('/personas/empleado/create') }}" class="btn btn-themecolor btn-lg" title="{{ trans('buttons.general.crud.create') }} Empleado">
                            <i class="fa fa-plus" aria-hidden="true"></i> {{ trans('buttons.general.crud.create2') }} {{ trans('labels.frontend.empleado') }}
                        </a>
                    </div><!--box-tools pull-right-->
                </h1>
            </div>

            @include('includes.partials.messages')

            {!! Form::open(['method' => 'GET', 'url' => '/personas/empleado', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
            <div class="input-group">
                <input type="text" class="form-control" name="search" placeholder="{{ trans('strings.backend.general.search_placeholder') }}">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="submit">
                        <i class="fa fa-search"></i>
                    </button>
                </span>
            </div>
            {!! Form::close() !!}

            <div class="row">
                <div class="col-xs-12">
                    <div class="table-responsive">
                        <table id="empleado-table" class="table table-condensed table-hover">
                            <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Cuil</th>
                                    <th>Caja por defecto</th>
                                    <th>{{ trans('labels.general.actions') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($empleado as $item)
                                <tr>
                                    <td>{{ $item->nombre }}</td>
                                    <td>{{ $item->cuil }}</td>
                                    <td>{{ $item->caja }}</td>
                                    <td>
                                        <a href="{{ url('/personas/empleado/' . $item->id) }}" title="{{ trans('buttons.general.crud.show') }} {{ trans('labels.frontend.empleado') }}">
                                            <button class="btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i> {{ trans('buttons.general.crud.show') }}</button>
                                        </a>
                                        <a href="{{ url('/personas/empleado/' . $item->id . '/edit') }}" title="{{ trans('buttons.general.crud.edit') }} {{ trans('labels.frontend.empleado') }}">
                                            <button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> {{ trans('buttons.general.crud.edit') }}</button>
                                        </a>
                                        {!! Form::open([
                                            'method'=>'DELETE',
                                            'url' => ['/personas/empleado', $item->id],
                                            'style' => 'display:inline',
                                            'id' => 'form-delete-'.$item->id
                                        ]) !!}
                                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> '.trans("buttons.general.crud.delete"), array(
                                                    'type' => 'button',
                                                    'class' => 'btn btn-danger btn-xs',
                                                    'title' => trans("buttons.general.crud.delete")." ".trans("labels.frontend.empleado"),
                                                    'onclick'=> 'eliminar("form-delete-'.$item->id.'")'
                                            )) !!}
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="pagination-wrapper"> {!! $empleado->appends(['search' => Request::get('search')])->render() !!} </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('after-scripts')
{{ Html::script("js/backend/plugin/datatables/jquery.dataTables.min.js") }}
{{ Html::script("js/backend/plugin/datatables/dataTables.bootstrap.min.js") }}
{{ Html::script("js/frontend/plugin/dialog/bootstrap-dialog.min.js") }}

<script>
    $(document).ready(function() {
        $('#empleado-table').DataTable( {
            "paging":   false,
            "info":     false,
            "searching":   false
        });
    } );

    function eliminar(idFormDelete){
        BootstrapDialog.show({
            title: 'Eliminar {{ trans("labels.frontend.empleado") }}',
            type: BootstrapDialog.TYPE_DANGER,
            message: '¿Esta seguro que desea eliminar este registro?',
            buttons: [{
                icon: 'glyphicon glyphicon-trash',
                label: ' Eliminar',
                cssClass: 'btn-danger',
                action: function(dialogItself){
                    $('form#'+idFormDelete).submit();
                }
            },{
                icon: 'glyphicon glyphicon-remove',
                label: ' Cancelar',
                cssClass: 'btn-default',
                action: function(dialogItself){
                    dialogItself.close();
                }
            }]
        });
    }
</script>

@endsection