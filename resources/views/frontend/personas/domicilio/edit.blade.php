@extends('frontend.layouts.app')

@section ('title', trans('labels.frontend.domicilio')." - ".trans('buttons.general.crud.edit'))

@section('content')
    <div class="main-content">
        <div class="main-content-inner">
            @include ('frontend.includes.breadcrumbs')
            <div class="page-header">
                <h1>
                    {{ trans('labels.frontend.domicilio') }}
                    <small>
                        <i class="ace-icon fa fa-angle-double-right"></i>
                        <small>{{ trans('buttons.general.crud.edit') }} Domicilio #{{ $domicilio->id }}</small>
                    </small>

                    <div class="box-tools pull-right">
                        <a href="{{ url('/personas/domicilio') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning ">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> {{ trans('buttons.general.crud.back') }}
                        </a>
                    </div>
                </h1>
            </div>

            <div class="row">
                <div class="col-xs-12">

                    @include('includes.partials.messages')

                    {!! Form::model($domicilio, [
                        'method' => 'PATCH',
                        'url' => ['/personas/domicilio', $domicilio->id],
                        'class' => 'form-horizontal',
                        'files' => true
                    ]) !!}

                        @include ('frontend.personas.domicilio.form', ['submitButtonText' => trans("buttons.general.crud.update")])

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
