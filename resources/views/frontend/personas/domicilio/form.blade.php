<div class="form-group {{ $errors->has('direccion') ? 'has-error' : ''}}">
    {!! Form::label('direccion', 'Direccion', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('direccion', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('direccion', '<p class="text-danger">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('codigo_postal') ? 'has-error' : ''}}">
    {!! Form::label('codigo_postal', 'Codigo Postal', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('codigo_postal', null, ['class' => 'form-control']) !!}
        {!! $errors->first('codigo_postal', '<p class="text-danger">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('pais_id') ? 'has-error' : ''}}">
    {!! Form::label('pais_id', 'Pais Id', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('pais_id', null, ['class' => 'form-control']) !!}
        {!! $errors->first('pais_id', '<p class="text-danger">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('provincia_id') ? 'has-error' : ''}}">
    {!! Form::label('provincia_id', 'Provincia Id', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('provincia_id', null, ['class' => 'form-control']) !!}
        {!! $errors->first('provincia_id', '<p class="text-danger">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('localidad_id') ? 'has-error' : ''}}">
    {!! Form::label('localidad_id', 'Localidad Id', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('localidad_id', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('localidad_id', '<p class="text-danger">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('telefono') ? 'has-error' : ''}}">
    {!! Form::label('telefono', 'Telefono', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('telefono', null, ['class' => 'form-control']) !!}
        {!! $errors->first('telefono', '<p class="text-danger">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('telefono2') ? 'has-error' : ''}}">
    {!! Form::label('telefono2', 'Telefono2', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('telefono2', null, ['class' => 'form-control']) !!}
        {!! $errors->first('telefono2', '<p class="text-danger">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {{ Form::submit(isset($submitButtonText) ? $submitButtonText : trans('buttons.general.save'), array('class' => 'btn btn-primary')) }}
    </div>
</div>
