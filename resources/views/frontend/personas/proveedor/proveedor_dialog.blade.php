<div class="modal fade" id="modal-proveedor-nuevo" tabindex="-1" role="dialog" aria-labelledby="proveedorModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="proveedorModalLabel">Nuevo Proveedor</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    {!! Form::label('proveedor_razon_social', 'Proveedor') !!}
                    {!! Form::text('proveedor_razon_social', '',['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('proveedor_cuit', 'DNI/CUIT') !!}
                    <div class="input-group">
                        <div class="input-group-addon"><i class="fa fa-hashtag"></i></div>
                        {!! Form::text('proveedor_cuit', '',['class' => 'form-control']) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button id="guardar-proveedor" type="button" class="btn btn-primary">Guardar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>

@section('scripts-dialog-proveedor')
    {{ Html::script("js/frontend/plugins/maskedinput/jquery.maskedinput.min.js") }}
    <script>
        $('input#proveedor_cuit').mask("99999999?999");
        $("#agregar-proveedor").click(function(){
            $('input#proveedor_razon_social').parent().removeClass('has-danger');
            $('input#proveedor_cuit').parent().removeClass('has-danger');
            $('input#proveedor_razon_social').val('');
            $('input#proveedor_cuit').val('');
            $('#modal-proveedor-nuevo').modal();
        });

        $("#guardar-proveedor").click(function(){
            $('input#proveedor_razon_social').parent().removeClass('has-danger');
            $('input#proveedor_cuit').parent().removeClass('has-danger');

            if($('input#proveedor_razon_social').val() == ''){
                $('input#proveedor_razon_social').parent().addClass('has-danger');
                $('input#proveedor_razon_social').focus();
            }else if($('input#proveedor_cuit').val() == ''){
                $('input#proveedor_cuit').parent().addClass('has-danger');
                $('input#proveedor_cuit').focus();
            }else{
                $('#modal-proveedor-nuevo').modal('hide');
                guardarNuevoProveedor();
            }
        });

        function guardarNuevoProveedor() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $( "input[name*='_token']" ).val()
                }
            });
            var razon_social = $('#proveedor_razon_social').val();
            var cuit = $('#proveedor_cuit').val();
            console.log('por aca llega');
            $.ajax({
                type: "POST",
                url: '{{ url("/personas/proveedor/guardarNuevoProveedor") }}',
                data: {razon_social: razon_social, cuit: cuit},
                success: function( msg ) {
                    $("#buscar_proveedor").val($('#proveedor_razon_social').val());
                    $("#proveedor_id").val(msg);
                }
            });
        }
    </script>
@endsection