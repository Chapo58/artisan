@extends('frontend.layouts.app')

@section ('title', trans('labels.frontend.proveedor_titulo')." - ".trans('buttons.general.crud.show'))

@section('after-styles')
    {{ Html::style("css/frontend/plugins/datatables/responsive.dataTables.min.css") }}
@endsection

@section('content')
    <div class="row page-titles">
        <div class="col-6 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('labels.frontend.proveedor_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item"><a href="{{url('/personas/proveedor')}}">{{ trans('labels.frontend.proveedor_titulo') }}</a></li>
                <li class="breadcrumb-item">{{ trans('buttons.general.crud.show') }} {{ trans('labels.frontend.proveedor') }}</li>
                <li class="breadcrumb-item active">{{ $proveedor }}</li>
            </ol>
        </div>
        <div class="col-6">
            <div class="pull-right">
                <a href="{{ url('/personas/proveedor') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning ">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans('buttons.general.crud.back') }}</span>
                </a>
                <a href="{{ url('/personas/proveedor/' . $proveedor->id . '/edit') }}" title="{{ trans('buttons.general.crud.edit') }}" class="btn btn-info ">
                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans('buttons.general.crud.edit') }}</span>
                </a>
                <a href="{{ url('/compras/orden-pago/crearOrdenPagoProveedor/' . $proveedor->id) }}" class="btn btn-primary ">
                    <i class="fa fa-plus" aria-hidden="true"></i>
                    <span class="hidden-xs-down">Orden de Pago</span>
                </a>
                <a href="javascript:void(0)" title="{{trans("buttons.general.crud.delete")." ".trans("labels.frontend.proveedor")}}" class="btn btn-danger  delete">
                    <i class="fa fa-trash" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans("buttons.general.crud.delete") }}</span>
                </a>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
            <ul class="nav nav-tabs profile-tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#info" role="tab">
                        <span class="hidden-sm-up"><i class="fa fa-info-circle"></i></span>
                        <span class="hidden-xs-down"><i class="fa fa-info-circle"></i> Información</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#compras" role="tab">
                        <span class="hidden-sm-up"><i class="fa fa-shopping-cart"></i></span>
                        <span class="hidden-xs-down"><i class="fa fa-shopping-cart"></i> Compras</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#cc" role="tab">
                        <span class="hidden-sm-up"><i class="fa fa-drivers-license-o"></i></span>
                        <span class="hidden-xs-down"><i class="fa fa-drivers-license-o"></i> Cuenta Corriente</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#articulos" role="tab">
                        <span class="hidden-sm-up"><i class="fa fa-tags"></i></span>
                        <span class="hidden-xs-down"><i class="fa fa-tags"></i> Articulos</span>
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="info" role="tabpanel">
                    <h4 class="font-medium m-t-30">{{ $proveedor->razon_social }}</h4>
                    <hr>
                    <div class="row">
                        <div class="col-md-3 col-xs-6 font-weight-bold"> <strong>CUIT</strong>
                            <br>
                            <p class="text-muted">{{ $proveedor->cuit }}</p>
                        </div>
                        <div class="col-md-3 col-xs-6 b-r font-weight-bold"> <strong>Teléfono</strong>
                            <br>
                            <p class="text-muted">{{ $proveedor->telefono }}</p>
                        </div>
                        <div class="col-md-3 col-xs-6 b-r font-weight-bold"> <strong>Email</strong>
                            <br>
                            <p class="text-muted">{{ $proveedor->email }}</p>
                        </div>
                        <div class="col-md-3 col-xs-6 b-r font-weight-bold"> <strong>Domicilio</strong>
                            <br>
                            <p class="text-muted">{{ $proveedor->domicilio }}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-xs-12 font-weight-bold">Codicion Iva</div>
                        <div class="col-md-3 col-xs-12">{{(isset($proveedor->condicionIva)) ? $proveedor->condicionIva->nombre : ''}}</div>
                    </div><br>
                    <div class="row">
                        <div class="col-md-3 col-xs-12 font-weight-bold">Contacto</div>
                        <div class="col-md-3 col-xs-12">{{ $proveedor->contacto }}</div>
                    </div><br>
                    <div class="row">
                        <div class="col-md-3 col-xs-12 font-weight-bold">Creado el</div>
                        <div class="col-md-3 col-xs-12">{{ $proveedor->created_at->format('d/m/Y H:m:s') }}</div>
                    </div><br>
                    <div class="row">
                        <div class="col-md-3 col-xs-12 font-weight-bold">Última modificación</div>
                        <div class="col-md-3 col-xs-12">{{ $proveedor->created_at->format('d/m/Y H:m:s') }}</div>
                    </div>
                </div>
                <div class="tab-pane" id="compras" role="tabpanel">
                    <table id="compras-table" class="table selectable table-bordered table-striped color-table form-material" style="width: 100%">
                        <thead>
                        <tr style="background:#99abb4;color:#fff;">
                            <th></th>
                            <th>Número</th>
                            <th>Fecha</th>
                            <th>Total</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($compras as $item)
                            <tr>
                                <td>&nbsp;&nbsp;</td>
                                <td>
                                    <a href="{{url('/compras/compra/'.$item->id)}}">
                                        {{ $tipos_comprobantes[$item->tipo_comprobante_id] }} {{ $item->punto_venta }}-{{ $item->numero }}
                                    </a>
                                </td>
                                <td>{{ $item->fecha->format('d/m/Y H:i') }}</td>
                                <td>{{ $item->moneda->signo }} {{ $item->total_pago }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="row">
                        <div class="col-md-3 col-lg-4"></div>
                        <div class="col-md-6 col-lg-4 col-xlg-2">
                            <div class="card card-inverse card-dark">
                                <div class="box text-center">
                                    <h1 class="font-light text-white">$ {{$compras->sum('total_pago')}}</h1>
                                    <h6 class="text-white">Importe Total Comprado</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="cc" role="tabpanel">
                    <div class="table-responsive">
                        <table id="cuentacorriente-table" class="table selectable table-bordered table-striped color-table  form-material" style="width: 100%">
                            <thead>
                            <tr style="background:#99abb4;color:#fff;">
                                <th></th>
                                <th>Comprabante</th>
                                <th>Tipo de Pago</th>
                                <th>Vencimiento</th>
                                <th>Debe</th>
                                <th>Haber</th>
                                <th>Saldo</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($cuentasCorrientes as $item)
                                <tr id="{{$item->id}}">
                                    <td>&nbsp;&nbsp;</td>
                                    <td>
                                        <a href="{{ url($item->url).'/'.$item->entidad_id }}">
                                            {{ $tipos_comprobantes[$item->tipo_comprobante] }} {{ $item->detalle }}
                                        </a>
                                    </td>
                                    <td>{{ ($item->tipo_cuenta_corriente_id) ? $item->tipoCuentaCorriente : '' }}</td>
                                    <td>{{ ($item->proximo_vencimiento) ? $item->proximo_vencimiento->format('d/m/Y') : '' }}</td>
                                    <td><b class="text-success">$ {{ $item->debe }}</b></td>
                                    <td><b class="text-danger">$ {{ $item->haber }}</b></td>
                                    <td>
                                        <b class="@if($item->debe > $item->haber) text-success @else text-danger @endif">
                                            $ {{ number_format(($item->haber - $item->debe), 2) }}
                                        </b>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="card card-danger card-inverse">
                        <div class="box text-center">
                            <h1 class="font-light text-white">$ {{ number_format(($cuentasCorrientes->sum('haber') - $cuentasCorrientes->sum('debe')), 2,',','.') }}</h1>
                            <h6 class="text-white">Saldo</h6>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="articulos" role="tabpanel">
                    <div class="table-responsive">
                        <table id="articulos-table" class="table selectable table-bordered table-striped color-table  form-material" style="width: 100%">
                            <thead>
                            <tr style="background:#99abb4;color:#fff;">
                                <th></th>
                                <th>Código</th>
                                <th>Nombre</th>
                                <th>Costo</th>
                                <th>Rubro</th>
                                <th>Subrubro</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($articulosProveedor as $item)
                                <tr>
                                    <td>&nbsp;&nbsp;</td>
                                    <td>{{ $item->articulo->codigo }}</td>
                                    <td><a href="{{url('articulos/articulo/'.$item->id)}}">{{ $item->articulo }}</a></td>
                                    <td>{{ $item->moneda->signo }} {{ $item->costo }}</td>
                                    <td>{{ $item->articulo->rubro }}</td>
                                    <td>{{ $item->articulo->subrubro }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('after-scripts')

    {{ Html::script("js/frontend/plugins/datatables/jquery.dataTables.min.js") }}
    {{ Html::script("js/frontend/plugins/datatables/dataTables.responsive.min.js") }}
    {{ Html::script("js/frontend/plugins/datatables/dataTables.select.min.js") }}

<script>

      $(document).ready(function() {
            $('#compras-table').DataTable({
                "aaSorting": [1,'asc'],
                "info":     false,
                "searching":   false,
                responsive: {
                    details: {
                        type: 'column',
                        target: 'tr'
                    }
                },
                "columnDefs": [{
                    targets: 0,
                    className: 'control',
                    orderable: false,
                    searchable: false,
                    defaultContent: '&nbsp;&nbsp;'
                },
                { responsivePriority: 1, targets: -1 },
                { responsivePriority: 1, targets: -2 },
                { responsivePriority: 1, targets: 1 }],
            });
        } );

        $(document).ready(function() {
            $('#cuentacorriente-table').DataTable({
                "aaSorting": [1,'asc'],
                "info":     false,
                "searching":   false,
                "info":     false,
                "searching":   false,
                responsive: {
                    details: {
                        type: 'column',
                        target: 'tr'
                    }
                },
                "columnDefs": [{
                    targets: 0,
                    className: 'control',
                    orderable: false,
                    searchable: false,
                    defaultContent: '&nbsp;&nbsp;'
                },
                { responsivePriority: 1, targets: -1 },
                { responsivePriority: 1, targets: -2 },
                { responsivePriority: 1, targets: 1 }],
            });
        } );

        $(document).ready(function() {
            $('#articulos-table').DataTable({
                "aaSorting": [1,'asc'],
                "info":     false,
                "searching":   false,
                responsive: {
                    details: {
                        type: 'column',
                        target: 'tr'
                    }
                },
                "columnDefs": [{
                    targets: 0,
                    className: 'control',
                    orderable: false,
                    searchable: false,
                    defaultContent: '&nbsp;&nbsp;'
                },
                { responsivePriority: 1, targets: -1 },
                { responsivePriority: 1, targets: -2 },
                { responsivePriority: 1, targets: 1 }],
            });
        } );

    $("body").on("click", ".delete", function () {
        swal({
            title: "{{trans('buttons.general.crud.delete').' '.trans('labels.frontend.proveedor')}}",
            text: "¿Realmente desea eliminar este registro?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#ff8726',
            confirmButtonText: "Eliminar"
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '{{ url('/personas/proveedor/'.$proveedor->id) }}',
                    data: {"_method" : 'DELETE'},
                    success: function (msg) {
                        swal("Eliminado!", "El registro ha sido correctamente eliminado.", "success").then((result) => { window.location.href = '{{url('/personas/proveedor')}}'; });
                    }
                });
            }
        });
    });
</script>

@endsection