@extends ('frontend.layouts.app')

@section ('title', trans('labels.frontend.proveedor_titulo')." - Artículos por Proveedor")

@section('content')
    <div class="row page-titles">
        <div class="col-md-8 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('labels.frontend.proveedor_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item"><a href="{{url('/personas/proveedor')}}">{{ trans('labels.frontend.proveedor_titulo') }}</a></li>
                <li class="breadcrumb-item">{{ trans('buttons.general.crud.show') }} Artículos por Proveedor</li>
                <li class="breadcrumb-item active">{{ $proveedor }}</li>
            </ol>
        </div>
        <div class="col-md-4">
            <div class="pull-right">
                <a href="{{ url('/personas/proveedor') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning ">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i> {{ trans('buttons.general.crud.back') }}
                </a>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
            @include('includes.partials.messages')
            <div class="table-responsive">
                <table id="detallearticulo-table" class="table table-striped table-hover table-sm">
                    <thead>
                    <tr>
                        <th>Código</th>
                        <th>Nombre</th>
                        <th>Costo</th>
                        <th>Cantidad</th>
                        <th>Rubro</th>
                        <th>Subrubro</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($articulosProveedor as $item)
                        <tr>
                            <td>{{ $item->articulo->codigo }}</td>
                            <td>{{ $item->articulo }}</td>
                            <td>{{ $item->moneda->signo }} {{ $item->costo }}</td>
                            <td>{{ number_format($item->cantidad,0) }} {{ $item->articulo->unidad->presentacion }}</td>
                            <td>{{ $item->articulo->rubro }}</td>
                            <td>{{ $item->articulo->subrubro }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection

@section('after-scripts')
    {{ Html::script("js/frontend/plugins/datatables/jquery.dataTables.min.js") }}

<script>
    $(document).ready(function() {
        $('#detallearticulo-table').DataTable();
    } );
</script>

@endsection