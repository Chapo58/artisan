@extends('frontend.layouts.app')

@section ('title', trans('labels.frontend.proveedor_titulo')." - ".trans('buttons.general.crud.edit'))

@section('content')
    <div class="row page-titles">
        <div class="col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('labels.frontend.proveedor_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item"><a href="{{url('/personas/proveedor')}}">{{ trans('labels.frontend.proveedor_titulo') }}</a></li>
                <li class="breadcrumb-item">{{ trans('buttons.general.crud.edit') }} {{ trans('labels.frontend.proveedor') }}</li>
                <li class="breadcrumb-item active">{{ $proveedor }}</li>
            </ol>
        </div>
        <div class="col-4">
            <div class="pull-right">
                <a href="{{ url('/personas/proveedor') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning ">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans('buttons.general.crud.back') }}</span>
                </a>
            </div>
        </div>
    </div>
    
    @include('includes.partials.messages')

    {!! Form::model($proveedor, [
        'method' => 'PATCH',
        'url' => ['/personas/proveedor', $proveedor->id],
        'class' => 'form-horizontal',
        'files' => true
    ]) !!}

        @include ('frontend.personas.proveedor.form', ['submitButtonText' => trans("buttons.general.crud.update")])

    {!! Form::close() !!}

@endsection

@section('after-scripts')
    {{ Html::script("js/frontend/plugin/maskedinput/jquery.maskedinput.min.js") }}
    <script>
        $( document ).ready(function() {
            $('input#cuit').mask("99-99999999-9",{placeholder:"##-########-#"});
        });
    </script>

    @yield('scripts-dialog-provincia')
    @yield('scripts-dialog-localidad')
@endsection