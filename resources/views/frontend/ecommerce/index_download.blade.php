<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
	<head>
		<meta charset="utf-8">
		<title>{{$empresa}}</title>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="keywords" content="tienda virtual, ecommerce, articulos, compras, productos, servicios, carrito">
		<meta name="description" content="Tienda Virtual de {{$empresa}}">

		<!-- Favicon -->
		<link rel="shortcut icon" href="{{{ asset('favicon.png') }}}">
		<!-- For-Mobile-Apps-and-Meta-Tags -->
		<meta name="mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<link rel="icon" sizes="192x192" href="{{{ asset('favicon.png') }}}">
		<meta name="apple-mobile-web-app-title" content="Artisan Gestión">
		<link href="{{{ asset('apple-touch-icon.png') }}}" rel="apple-touch-icon" />
		<!-- //For-Mobile-Apps-and-Meta-Tags -->
	</head>

	<body>
		<iframe src="https://artisan.com.ar/ecommerce/frame/{{$empresa->id}}" style="position:fixed; top:0px; left:0px; bottom:0px; right:0px; width:100%; height:100%; border:none; margin:0; padding:0; overflow:hidden; z-index:999999;">
			Su navegador necesita soportar iframes para visualizar correctamente este sitio
		</iframe>
	</body>
</html>