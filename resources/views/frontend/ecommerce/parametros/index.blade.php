@extends('frontend.layouts.app')

@section ('title', 'Parametros Ecommerce')

@section('after-styles')
    {{ Html::style("css/frontend/plugins/editor/bootstrap-wysihtml5.css") }}
@endsection

@section('content')
    <div class="row page-titles">
        <div class="col-12 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">Parametros</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item">Ecommerce</li>
                <li class="breadcrumb-item active">Parametros</li>
            </ol>
        </div>
    </div>

    @tips($_SERVER['REQUEST_URI'])

    @include('includes.partials.messages')

    {!! Form::model($empresa->ecommerce, [
        'method' => 'POST',
        'url' => ['/ecommerce/parametros/guardar'],
        'class' => 'form-horizontal',
        'files' => true
    ]) !!}

    <div class="card">
        <div class="card-body">
            <ul class="nav nav-tabs customtab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#principal" role="tab">
                        <span class="hidden-sm-up"><i class="ti-info"></i></span>
                        <span class="hidden-xs-down">Principal</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#contacto" role="tab">
                        <span class="hidden-sm-up"><i class="ti-mobile"></i></span>
                        <span class="hidden-xs-down">Información de Contacto</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#politica" role="tab">
                        <span class="hidden-sm-up"><i class="ti-shield"></i></span>
                        <span class="hidden-xs-down">Politicas de Privacidad</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#condiciones" role="tab">
                        <span class="hidden-sm-up"><i class="ti-clipboard"></i></span>
                        <span class="hidden-xs-down">Terminos y Condiciones</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#tawkto" role="tab">
                        <span class="hidden-sm-up"><i class="ti-comments-smiley"></i></span>
                        <span class="hidden-xs-down">TawkTo</span>
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="principal" role="tabpanel">
                    <div class="p-20">
                        <div class="row form-group">
                            <div class="col-md-6">
                                {!! Form::label('nombre', 'Nombre de la empresa') !!}
                                {!! Form::text('nombre', ($empresa->ecommerce) ? $empresa->ecommerce : $empresa, ['class' => 'form-control', 'required' => 'required']) !!}
                                {!! $errors->first('nombre', '<p class="text-danger">:message</p>') !!}
                            </div>
                            <div class="form-group col-md-3">
                                {!! Form::label('lista_precio_id', 'Lista de Precios') !!}
                                {!! Form::select('lista_precio_id', $lista_precios, isset($empresa->ecommerce->listaPrecios) ? $empresa->ecommerce->listaPrecios->id : $lista_precios_defecto->valor, ['class' => 'form-control', 'required' => 'required']) !!}
                                {!! $errors->first('lista_precio_id', '<p class="text-danger">:message</p>') !!}
                            </div>
                            <div class="form-group col-md-3">
                                {!! Form::label('moneda_id', 'Moneda') !!}
                                {!! Form::select('moneda_id', $monedas, isset($empresa->ecommerce->moneda) ? $empresa->ecommerce->moneda->id : (isset($monedaPorDefecto) ? $monedaPorDefecto->id : ''), ['class' => 'form-control', 'id' => 'moneda_id', 'required' => 'required']) !!}
                                {!! $errors->first('moneda_id', '<p class="text-danger">:message</p>') !!}
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="form-group col-md-7">
                                <div class="form-group">
                                    <label>Seleccionar Logo</label>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12">
                                            <div class="input-group image-preview">
                                                <input type="text" class="form-control image-preview-filename" disabled="disabled">
                                                <span class="input-group-btn">
                                                <!-- image-preview-clear button -->
                                                <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                                                  <span class="glyphicon glyphicon-remove"></span> Limpiar
                                                </button>
                                                    <!-- image-preview-input -->
                                                <div class="btn btn-default image-preview-input">
                                                  <span class="glyphicon glyphicon-folder-open"></span>
                                                  <span class="image-preview-input-title">Buscar</span>
                                                  <input type="file" name="image" accept=".png, .jpg, .jpeg">
                                                </div>
                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @if(isset($empresa->ecommerce))
                                <div class="col-md-3">
                                    {!! Form::label('imagen_url', 'Logo Actual') !!}
                                    <img src="{{ $empresa->ecommerce->logo() }}" alt="{{ $empresa }}" width="300" style="border: 2px solid lightslategrey;">
                                </div>
                            @endif
                        </div>
                        <div class="form-group col-md-12">
                            {!! Form::label('descripcion', 'Descripción breve de la empresa') !!}
                            {!! Form::textarea('descripcion', null, ['class' => 'form-control']) !!}
                            {!! $errors->first('descripcion', '<p class="text-danger">:message</p>') !!}
                        </div>
                        <div class="row form-group">
                            <div class="form-group col-md-3">
                                {!! Form::label('habilitado', 'Habilitado') !!}
                                <i class="fa fa-question-circle text-info mytooltip">
                                    <span class="tooltip-content3">En caso de estar el ecommerce deshabilitado, este se mostrara "En Construccion" cuando alguien intente acceder a el</span>
                                </i>
                                <div class="switch">
                                    <label>
                                        {!! Form::checkbox('habilitado', true, isset($empresa->ecommerce) ? $empresa->ecommerce->habilitado : true) !!}<span class="lever"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                {!! Form::label('mostrar_precios', 'Mostrar Precios') !!}
                                <i class="fa fa-question-circle text-info mytooltip">
                                    <span class="tooltip-content3">Indica si los clientes deben estar logeados para poder visualizar los precios de los artículos</span>
                                </i>
                                <div class="switch">
                                    <label>
                                        {!! Form::checkbox('mostrar_precios', true, isset($empresa->ecommerce) ? $empresa->ecommerce->mostrar_precios : true) !!}<span class="lever"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                {!! Form::label('precio_final', 'Precio Final') !!}
                                <i class="fa fa-question-circle text-info mytooltip">
                                    <span class="tooltip-content3">Indica si los artículos en el ecommerce deben mostrarse con los impuestos desglosados o no</span>
                                </i>
                                <div class="switch">
                                    <label>
                                        {!! Form::checkbox('precio_final', true, isset($empresa->ecommerce) ? $empresa->ecommerce->precio_final : true) !!}<span class="lever"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                {!! Form::label('articulos_sin_stock', 'Articulos Sin Stock') !!}
                                <i class="fa fa-question-circle text-info mytooltip">
                                    <span class="tooltip-content3">Indica si deben visualizarse en el ecommerce los artículos que lleven stock pero que no posean existencias</span>
                                </i>
                                <div class="switch">
                                    <label>
                                        {!! Form::checkbox('articulos_sin_stock', true, isset($empresa->ecommerce) ? $empresa->ecommerce->articulos_sin_stock : true) !!}<span class="lever"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane p-20" id="contacto" role="tabpanel">
                    <div class="row form-group">
                        <div class="col-md-4">
                            {!! Form::label('telefono', 'Telefono') !!}
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-phone"></i>
                                </div>
                                {!! Form::text('telefono', ($empresa->ecommerce) ? $empresa->ecommerce->telefono : $empresa->telefono, ['class' => 'form-control numero']) !!}
                            </div>
                            {!! $errors->first('telefono', '<p class="text-danger">:message</p>') !!}
                        </div>
                        <div class="col-md-4">
                            {!! Form::label('telefono2', 'Telefono 2') !!}
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-phone"></i>
                                </div>
                                {!! Form::text('telefono2', null, ['class' => 'form-control numero']) !!}
                            </div>
                            {!! $errors->first('telefono2', '<p class="text-danger">:message</p>') !!}
                        </div>
                        <div class="col-md-4">
                            {!! Form::label('telefono3', 'Telefono 3') !!}
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-phone"></i>
                                </div>
                                {!! Form::text('telefono3', null, ['class' => 'form-control numero']) !!}
                            </div>
                            {!! $errors->first('telefono3', '<p class="text-danger">:message</p>') !!}
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-6">
                            {!! Form::label('email', 'Email') !!}
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                {!! Form::text('email', ($empresa->ecommerce) ? $empresa->ecommerce->email : $empresa->email, ['class' => 'form-control']) !!}
                            </div>
                            {!! $errors->first('email', '<p class="text-danger">:message</p>') !!}
                        </div>
                        <div class="col-md-6">
                            {!! Form::label('email2', 'Email 2') !!}
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                {!! Form::text('email2', null, ['class' => 'form-control']) !!}
                            </div>
                            {!! $errors->first('email2', '<p class="text-danger">:message</p>') !!}
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-6">
                            {!! Form::label('facebook', 'Facebook') !!}
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-facebook"></i></span>
                                {!! Form::text('facebook', null, ['class' => 'form-control']) !!}
                            </div>
                            {!! $errors->first('facebook', '<p class="text-danger">:message</p>') !!}
                        </div>
                        <div class="col-md-6">
                            {!! Form::label('twitter', 'Twitter') !!}
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-twitter"></i></span>
                                {!! Form::text('twitter', null, ['class' => 'form-control']) !!}
                            </div>
                            {!! $errors->first('twitter', '<p class="text-danger">:message</p>') !!}
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-6">
                            {!! Form::label('instagram', 'Instagram') !!}
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-instagram"></i></span>
                                {!! Form::text('instagram', null, ['class' => 'form-control']) !!}
                            </div>
                            {!! $errors->first('instagram', '<p class="text-danger">:message</p>') !!}
                        </div>
                        <div class="col-md-6">
                            {!! Form::label('google_plus', 'Google Plus') !!}
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-google-plus"></i></span>
                                {!! Form::text('google_plus', null, ['class' => 'form-control']) !!}
                            </div>
                            {!! $errors->first('google_plus', '<p class="text-danger">:message</p>') !!}
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-6">
                            {!! Form::label('linkedin', 'Linkedin') !!}
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-linkedin"></i></span>
                                {!! Form::text('linkedin', null, ['class' => 'form-control']) !!}
                            </div>
                            {!! $errors->first('linkedin', '<p class="text-danger">:message</p>') !!}
                        </div>
                        <div class="col-md-6">
                            {!! Form::label('skype', 'Skype') !!}
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-skype"></i></span>
                                {!! Form::text('skype', null, ['class' => 'form-control']) !!}
                            </div>
                            {!! $errors->first('skype', '<p class="text-danger">:message</p>') !!}
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-6">
                            {!! Form::label('direccion', 'Dirección') !!}
                            {!! Form::text('domicilio[direccion]', isset($empresa->ecommerce->domicilio->direccion) ? $empresa->ecommerce->domicilio->direccion : '', ['class' => 'form-control']) !!}
                            {!! $errors->first('direccion', '<p class="text-danger">:message</p>') !!}
                        </div>
                        <div class="col-md-6">
                            {!! Form::label('pais_id', 'País', ['class' => 'col-md-2 control-label']) !!}
                            {!! Form::select('domicilio[pais_id]', $paises, isset($empresa->ecommerce->domicilio->pais) ? $empresa->ecommerce->domicilio->pais->id : 1, ['id' => 'pais_id', 'class' => 'form-control']) !!}
                            {!! $errors->first('pais_id', '<p class="text-danger">:message</p>') !!}
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-6">
                            {!! Form::label('provincia_id', 'Provincia') !!}
                            {!! Form::select('domicilio[provincia_id]', $provincias, isset($empresa->ecommerce->domicilio->provincia) ? $empresa->ecommerce->domicilio->provincia->id : '', ['id' => 'provincia_id', 'class' => 'form-control']) !!}
                            {!! $errors->first('provincia_id', '<p class="text-danger">:message</p>') !!}
                        </div>
                        <div class="col-md-6">
                            {!! Form::label('codigo_postal', 'Código Postal') !!}
                            {!! Form::text('domicilio[codigo_postal]', isset($empresa->ecommerce->domicilio->codigo_postal) ? $empresa->ecommerce->domicilio->codigo_postal : '', ['class' => 'form-control']) !!}
                            {!! $errors->first('codigo_postal', '<p class="text-danger">:message</p>') !!}
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-6">
                            {!! Form::label('localidad', 'Localidad') !!}
                            {!! Form::text('domicilio[localidad]', isset($empresa->ecommerce->domicilio->localidad) ? $empresa->ecommerce->domicilio->localidad : '', ['class' => 'form-control','id' => 'localidad']) !!}
                            {!! $errors->first('localidad', '<p class="text-danger">:message</p>') !!}
                        </div>
                    </div>
                </div>
                <div class="tab-pane p-20" id="politica" role="tabpanel">
                    <div class="row form-group">
                        <div class="col-md-12">
                            {!! Form::label('politica_privacidad', 'Politica de Privacidad') !!}
                            {!! Form::textarea('politica_privacidad', null, ['class' => 'form-control textarea_editor']) !!}
                            {!! $errors->first('politica_privacidad', '<p class="text-danger">:message</p>') !!}
                        </div>
                    </div>
                </div>
                <div class="tab-pane p-20" id="condiciones" role="tabpanel">
                    <div class="row form-group">
                        <div class="col-md-12">
                            {!! Form::label('terminos_condiciones', 'Terminos y Condiciones') !!}
                            {!! Form::textarea('terminos_condiciones', null, ['class' => 'form-control textarea_editor2']) !!}
                            {!! $errors->first('terminos_condiciones', '<p class="text-danger">:message</p>') !!}
                        </div>
                    </div>
                </div>
                <div class="tab-pane p-20" id="tawkto" role="tabpanel">
                    <div class="row form-group">
                        <div class="form-group col-md-2">
                            {!! Form::label('tawkto_habilitado', 'Habilitado') !!}
                            <div class="switch">
                                <label>
                                    {!! Form::checkbox('tawkto_habilitado', true, isset($empresa->ecommerce) ? $empresa->ecommerce->tawkto_habilitado : true) !!}<span class="lever"></span>
                                </label>
                            </div>
                        </div>
                        <div class="form-group col-md-10">
                            {!! Form::label('tawkto_codigo', 'Codigo del Chat') !!}
                            {!! Form::textarea('tawkto_codigo', null, ['class' => 'form-control']) !!}
                            {!! $errors->first('tawkto_codigo', '<p class="text-danger">:message</p>') !!}
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-12">
                        {{ Form::submit(isset($submitButtonText) ? $submitButtonText : trans('buttons.general.save'), ['class' => 'btn btn-lg btn-themecolor pull-right']) }}
                    </div>
                </div>
            </div>
        </div>
    </div>

    {!! Form::close() !!}

    <div class="card">
        <div class="card-body">
            <h4 class="font-medium m-t-30">Incorporar E-Commerce a tu web</h4>
            <hr>
            <div class="font-weight-bold">Para incorporar la tienda virtual a tu sitio web, solo copia y pega el siguiente código en tu archivo index:</div>
            <br>
            <blockquote>
            {{'<iframe src="https://artisan.com.ar/ecommerce/frame/'.$empresa->id.'" style="position:fixed; top:0px; left:0px; bottom:0px; right:0px; width:100%; height:100%; border:none; margin:0; padding:0; overflow:hidden; z-index:999999;"></iframe>'}}
            </blockquote>
            <hr>
            <div class="font-weight-bold">O descarga nuestro archivo index ya configurado para tu empresa:</div>
            <a class="btn btn-bg btn-primary waves-effect waves-light" href="{{url('/ecommerce/parametros/downloadIndex')}}" target="_blank">
                <span class="btn-label"><i class="fa fa-download"></i></span>
                Descargar Index
            </a>
            <hr>
            <i class="text-info">¿Todavía no tenes un sitio web? Contáctate con nosotros a través del chat para que te ayudemos.</i>
        </div>
    </div>

@endsection

@section('after-scripts')
    {{ Html::script("js/frontend/plugins/editor/wysihtml5-0.3.0.js") }}
    {{ Html::script("js/frontend/plugins/editor/bootstrap-wysihtml5.js") }}

    <script>
        $(document).ready(function() {
            $('.textarea_editor').wysihtml5();
            $('.textarea_editor2').wysihtml5();
        });
    </script>
@endsection