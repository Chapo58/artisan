<div class="card">
    <div class="card-body">
        <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
        <br>
        <div class="row form-group">
            <div class="col-md-5">
                {!! Form::label('titulo', 'Titulo') !!}
                {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
                {!! $errors->first('titulo', '<p class="text-danger">:message</p>') !!}
            </div>
            <div class="col-md-4">
                {!! Form::label('link', 'Link') !!}
                {!! Form::text('link', null, ['class' => 'form-control']) !!}
                {!! $errors->first('link', '<p class="text-danger">:message</p>') !!}
            </div>
            <div class="col-md-3">
                {!! Form::label('orden', 'Orden') !!}
                {!! Form::number('orden', null, ['class' => 'form-control']) !!}
                {!! $errors->first('orden', '<p class="text-danger">:message</p>') !!}
            </div>
        </div>
        <div class="row form-group">
            <div class="form-group col-md-7">
                <div class="form-group">
                    <label>Seleccionar Imagen (Tamaño recomendado: <i class="font-weight-bold">1920 x 585 px</i>)</label>
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="input-group image-preview">
                                <input type="text" class="form-control image-preview-filename" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
                                <span class="input-group-btn">
                                    <!-- image-preview-clear button -->
                                    <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                                      <span class="glyphicon glyphicon-remove"></span> Limpiar
                                    </button>
                                    <!-- image-preview-input -->
                                    <div class="btn btn-default image-preview-input">
                                      <span class="glyphicon glyphicon-folder-open"></span>
                                      <span class="image-preview-input-title">Buscar</span>
                                      <input type="file" name="image" accept=".png, .jpg, .jpeg">
                                    </div>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @if(isset($slider->imagen_url))
            <div class="col-md-3">
                {!! Form::label('imagen_url', 'Imagen Actual') !!}
                <img src="{{ url($slider->imagen_url) }}" alt="{{ isset($slider->titulo) }}" width="300" style="border: 2px solid lightslategrey;">
            </div>
            @endif
        </div>
        <div class="row form-group">
            <div class="col-md-6">
                {!! Form::label('texto', 'Texto') !!}
                {!! Form::textarea('texto', null, ['class' => 'form-control']) !!}
                {!! $errors->first('texto', '<p class="text-danger">:message</p>') !!}
            </div>
            <div class="col-md-3">
                {!! Form::label('texto', 'Color del texto y titulo') !!}
                {!! Form::text('color', null, ['class' => 'colorpicker form-control']) !!}
            </div>
        </div>
        <hr>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-12">
                    {{ Form::submit(isset($submitButtonText) ? $submitButtonText : trans('buttons.general.save'), ['class' => 'btn btn-lg btn-themecolor pull-right']) }}
                </div>
            </div>
        </div>
    </div>
</div>
