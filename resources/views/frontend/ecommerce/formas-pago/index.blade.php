@extends ('frontend.layouts.app')

@section ('title', 'Formas de Pago')

@section('after-styles')
    {{ Html::style("css/frontend/plugins/datatables/responsive.dataTables.min.css") }}
@endsection

@section('content')
    <div class="row page-titles">
        <div class="col-md-12 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">Formas de Pago</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item active">Formas de Pago</li>
            </ol>
        </div>
    </div>

    @tips($_SERVER['REQUEST_URI'])

    <div class="card">
            <div class="card-body">
                <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
                @include('includes.partials.messages')
                <div class="table-responsive">
                    <table id="formas-pago-table" class="table selectable table-striped table-hover table-sm">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Forma de Pago</th>
                                <th>Habilitado</th>
                                <th>{{ trans('labels.general.actions') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($formasPago as $item)
                            <tr id="{{$item->id}}">
                                <td>&nbsp;&nbsp;</td>
                                <td>{{ $item }}</td>
                                <td>
                                    @if($item->habilitado)
                                        <span class="text-bold text-green">Habilitado</span>
                                    @else
                                        <span class="text-bold text-danger">Deshabilitado</span>
                                    @endif
                                </td>
                                <td>
                                    <a href="{{ url('/ecommerce/formas-pago/' . $item->id) }}" class="mytooltip">
                                        <i class="fa fa-eye text-success m-r-10"></i>
                                        <span class="tooltip-content3">{{ trans('buttons.general.crud.show') }} Forma de Pago</span>
                                    </a>
                                    <a href="{{ url('/ecommerce/formas-pago/' . $item->id . '/edit') }}" class="mytooltip">
                                        <i class="fa fa-pencil text-info m-r-10"></i>
                                        <span class="tooltip-content3">{{ trans('buttons.general.crud.edit') }} Forma de Pago</span>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
@endsection

@section('after-scripts')
    {{ Html::script("js/frontend/plugins/datatables/jquery.dataTables.min.js") }}
    {{ Html::script("js/frontend/plugins/datatables/dataTables.responsive.min.js") }}
    {{ Html::script("js/frontend/plugins/datatables/dataTables.select.min.js") }}

<script>
    $(document).ready(function() {
        $('#formas-pago-table').DataTable({
            "aaSorting": [1,'asc'],
            responsive: {
                details: {
                    type: 'column',
                    target: 'tr'
                }
            },
            "columnDefs": [{
                targets: 0,
                className: 'control',
                orderable: false,
                searchable: false,
                defaultContent: '&nbsp;&nbsp;'
            },
                { responsivePriority: 1, targets: -1 }]
        });
    } );
</script>

@endsection