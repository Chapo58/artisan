<div class="card">
    <div class="card-body">
        <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
        <br>
        <div class="row form-group">
            <div class="form-group col-md-2">
                {!! Form::label('habilitado', 'Habilitado') !!}
                <div class="switch">
                    <label>
                        {!! Form::checkbox('habilitado', true, isset($formaPago->habilitado) ? $formaPago->habilitado : true) !!}<span class="lever"></span>
                    </label>
                </div>
            </div>
            <div class="col-md-10">
                {!! Form::label('instrucciones', 'Instrucciones de Pago') !!}
                {!! Form::textarea('instrucciones', null, ['class' => 'form-control textarea_editor']) !!}
                {!! $errors->first('instrucciones', '<p class="text-danger">:message</p>') !!}
            </div>
        </div>
        <hr>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-12">
                    {{ Form::submit(isset($submitButtonText) ? $submitButtonText : trans('buttons.general.save'), ['class' => 'btn btn-lg btn-themecolor pull-right']) }}
                </div>
            </div>
        </div>
    </div>
</div>
