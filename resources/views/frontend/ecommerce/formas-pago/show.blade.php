@extends('frontend.layouts.app')

@section ('title', 'Formas de Pago')

@section('content')
    <div class="row page-titles">
        <div class="col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">Formas de Pago</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item"><a href="{{url('/ecommerce/formas-pago')}}">Formas de Pago</a></li>
                <li class="breadcrumb-item">Ver Froma de Pago</li>
                <li class="breadcrumb-item active">{{ $formaPago }}</li>
            </ol>
        </div>
        <div class="col-4">
            <div class="pull-right">
                <a href="{{ url('/ecommerce/formas-pago') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans('buttons.general.crud.back') }}</span>
                </a>
                <a href="{{ url('/ecommerce/formas-pago/' . $formaPago->id . '/edit') }}" title="{{ trans('buttons.general.crud.edit') }}" class="btn btn-info">
                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans('buttons.general.crud.edit') }}</span>
                </a>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
            <h4 class="font-medium m-t-30">{{ $formaPago }}</h4>
            <hr>
            <div class="row">
                <div class="col-md-3 font-weight-bold">Instrucciones</div>
                <div class="col-md-9">{!! $formaPago->instrucciones !!}</div>
            </div><hr>
            <div class="row">
                <div class="col-md-3 font-weight-bold">Habilitado</div>
                <div class="col-md-9">{{ ($formaPago->habilitado) ? 'Habilitado' : 'Deshabilitado' }}</div>
            </div><hr>
            <div class="row">
                <div class="col-md-3 font-weight-bold">Creado el</div>
                <div class="col-md-9">{{ $formaPago->created_at->format('d/m/Y H:m') }}</div>
            </div><hr>
            <div class="row">
                <div class="col-md-3 font-weight-bold">Ultima Modificación</div>
                <div class="col-md-9">{{ $formaPago->updated_at->format('d/m/Y H:m') }}</div>
            </div>
        </div>
    </div>

@endsection