<div class="card">
    <div class="card-body">
        <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
        <br>
        <div class="row form-group">
            <div class="col-md-6">
                {!! Form::label('titulo', 'Titulo') !!}
                {!! Form::text('titulo', null, ['class' => 'form-control', 'required' => 'required']) !!}
                {!! $errors->first('titulo', '<p class="text-danger">:message</p>') !!}
            </div>
            <div class="col-md-6">
                {!! Form::label('subtitulo', 'Subtitulo') !!}
                {!! Form::text('subtitulo', null, ['class' => 'form-control']) !!}
                {!! $errors->first('subtitulo', '<p class="text-danger">:message</p>') !!}
            </div>
        </div>
        <div class="row form-group">
            <div class="form-group col-md-7">
                <div class="form-group">
                    <label>Seleccionar Imagen (Tamaño recomendado: <i class="font-weight-bold">432 x 520 px</i>)</label>
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="input-group image-preview">
                                <input type="text" class="form-control image-preview-filename" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
                                <span class="input-group-btn">
                                    <!-- image-preview-clear button -->
                                    <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                                      <span class="glyphicon glyphicon-remove"></span> Limpiar
                                    </button>
                                    <!-- image-preview-input -->
                                    <div class="btn btn-default image-preview-input">
                                      <span class="glyphicon glyphicon-folder-open"></span>
                                      <span class="image-preview-input-title">Buscar</span>
                                      <input type="file" name="image" accept=".png, .jpg, .jpeg">
                                    </div>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @if(isset($pagina->imagen_url))
                <div class="col-md-3">
                    {!! Form::label('imagen_url', 'Imagen Actual') !!}
                    <img src="{{ url($pagina->imagen_url) }}" alt="{{ isset($pagina->titulo) }}" width="300" style="border: 2px solid lightslategrey;">
                </div>
            @endif
        </div>
        <div class="row form-group">
            <div class="col-md-12">
                {!! Form::label('contenido', 'Contenido') !!}
                {!! Form::textarea('contenido', null, ['class' => 'form-control textarea_editor', 'required' => 'required']) !!}
                {!! $errors->first('contenido', '<p class="text-danger">:message</p>') !!}
            </div>
        </div>
        <hr>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-12">
                    {{ Form::submit(isset($submitButtonText) ? $submitButtonText : trans('buttons.general.save'), ['class' => 'btn btn-lg btn-themecolor pull-right']) }}
                </div>
            </div>
        </div>
    </div>
</div>
