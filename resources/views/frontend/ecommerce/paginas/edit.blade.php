@extends('frontend.layouts.app')

@section ('title', trans('labels.frontend.paginas_titulo')." - ".trans('buttons.general.crud.edit'))

@section('after-styles')
    {{ Html::style("css/frontend/plugins/editor/bootstrap-wysihtml5.css") }}
@endsection

@section('content')
    <div class="row page-titles">
        <div class="col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('labels.frontend.paginas_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item"><a href="{{url('/ecommerce/paginas')}}">{{ trans('labels.frontend.paginas_titulo') }}</a></li>
                <li class="breadcrumb-item">{{ trans('buttons.general.crud.edit') }} {{ trans('labels.frontend.paginas') }}</li>
                <li class="breadcrumb-item active">{{ $pagina }}</li>
            </ol>
        </div>
        <div class="col-4">
            <div class="pull-right">
                <a href="{{ url('/ecommerce/paginas') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans('buttons.general.crud.back') }}</span>
                </a>
            </div>
        </div>
    </div>

    @include('includes.partials.messages')

    {!! Form::model($pagina, [
        'method' => 'PATCH',
        'url' => ['/ecommerce/paginas', $pagina->id],
        'class' => 'form-horizontal',
        'files' => true
    ]) !!}

        @include ('frontend.ecommerce.paginas.form', ['submitButtonText' => trans("buttons.general.crud.update")])

    {!! Form::close() !!}

@endsection

@section('after-scripts')
    {{ Html::script("js/frontend/plugins/editor/wysihtml5-0.3.0.js") }}
    {{ Html::script("js/frontend/plugins/editor/bootstrap-wysihtml5.js") }}

    <script>
        $(document).ready(function() {
            $('.textarea_editor').wysihtml5();
        });
    </script>
@endsection