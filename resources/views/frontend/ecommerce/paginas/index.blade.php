@extends ('frontend.layouts.app')

@section ('title', trans('labels.frontend.paginas_titulo')." - ".trans('navs.frontend.user.administration'))

@section('after-styles')
    {{ Html::style("css/frontend/plugins/datatables/responsive.dataTables.min.css") }}
@endsection

@section('content')
    <div class="row page-titles">
        <div class="col-6 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('labels.frontend.paginas_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item active">{{ trans('labels.frontend.paginas_titulo') }}</li>
            </ol>
        </div>
        <div class="col-6">
            <div class="pull-right">
                <a href="{{ url('/ecommerce/paginas/create') }}" class="btn btn-themecolor btn-lg" title="{{ trans('buttons.general.crud.create').' '.trans('labels.frontend.paginas')  }}">
                    <i class="fa fa-plus" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans('buttons.general.crud.create').' '.trans('labels.frontend.paginas') }}</span>
                </a>
            </div>
        </div>
    </div>

    @tips($_SERVER['REQUEST_URI'])

    <div class="card">
            <div class="card-body">
                <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
                @include('includes.partials.messages')
                <div class="table-responsive">
                    <table id="paginas-table" class="table selectable table-striped table-hover table-sm">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Titulo</th>
                                <th>Subtitulo</th>
                                <th>{{ trans('labels.general.actions') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($paginas as $item)
                            <tr id="{{$item->id}}">
                                <td>&nbsp;&nbsp;</td>
                                <td>{{ $item->titulo }}</td>
                                <td>{{ $item->subtitulo }}</td>
                                <td>
                                    <a href="{{ url('/ecommerce/paginas/' . $item->id) }}" class="mytooltip">
                                        <i class="fa fa-eye text-success m-r-10"></i>
                                        <span class="tooltip-content3">{{ trans('buttons.general.crud.show') }} {{ trans('labels.frontend.paginas') }}</span>
                                    </a>
                                    <a href="{{ url('/ecommerce/paginas/' . $item->id . '/edit') }}" class="mytooltip">
                                        <i class="fa fa-pencil text-info m-r-10"></i>
                                        <span class="tooltip-content3">{{ trans('buttons.general.crud.edit') }} {{ trans('labels.frontend.paginas') }}</span>
                                    </a>
                                    <a href="javascript:void(0)" class="delete mytooltip" data-id="{{$item->id}}">
                                        <i class="fa fa-close text-danger"></i>
                                        <span class="tooltip-content3">{{ trans('buttons.general.crud.delete') }} {{ trans('labels.frontend.paginas') }}</span>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
@endsection

@section('after-scripts')
    {{ Html::script("js/frontend/plugins/datatables/jquery.dataTables.min.js") }}
    {{ Html::script("js/frontend/plugins/datatables/dataTables.responsive.min.js") }}
    {{ Html::script("js/frontend/plugins/datatables/dataTables.select.min.js") }}

<script>
    $(document).ready(function() {
        $('#paginas-table').DataTable({
            "aaSorting": [1,'asc'],
            responsive: {
                details: {
                    type: 'column',
                    target: 'tr'
                }
            },
            "columnDefs": [{
                targets: 0,
                className: 'control',
                orderable: false,
                searchable: false,
                defaultContent: '&nbsp;&nbsp;'
            },
                { responsivePriority: 1, targets: -1 }]
        });
    } );

    $("body").on("click", ".delete", function () {
        var id = $(this).attr("data-id");
        swal({
            title: "{{trans('buttons.general.crud.delete').' '.trans('labels.frontend.paginas')}}",
            text: "¿Realmente desea eliminar este registro?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#ff8726',
            confirmButtonText: "Eliminar"
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/ecommerce/paginas/'+id,
                    data: {"_method" : 'DELETE'},
                    success: function (msg) {
                        $("#" + id).hide(1);
                        swal("Eliminado!", "El registro ha sido correctamente eliminado.", "success");
                    }
                });
            }
        });
    });
</script>

@endsection