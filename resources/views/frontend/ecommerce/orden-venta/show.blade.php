@extends('frontend.layouts.app')

@section ('title', trans('labels.frontend.orden_venta_titulo')." - ".trans('buttons.general.crud.show'))

@section('content')
    <div class="row page-titles">
        <div class="col-6 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('labels.frontend.orden_venta_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item"><a href="{{url('/ecommerce/orden-venta')}}">{{ trans('labels.frontend.orden_venta_titulo') }}</a></li>
                <li class="breadcrumb-item">{{ trans('buttons.general.crud.show') }} {{ trans('labels.frontend.orden_venta') }}</li>
                <li class="breadcrumb-item active">{{ $ordenVenta }}</li>
            </ol>
        </div>
        <div class="col-6">
            <div class="pull-right">
                <a href="{{ url('/ecommerce/orden-venta') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans('buttons.general.crud.back') }}</span>
                </a>
                <a href="{{ url('/ecommerce/orden-venta/imprimir/' . $ordenVenta->id) }}" target="_blank" title="Imprimir" class="btn btn-primary">
                    <i class="fa fa-print" aria-hidden="true"></i>
                    <span class="hidden-xs-down">Imprimir</span>
                </a>
                <a href="javascript:void(0)" title="{{trans("buttons.general.crud.delete")." ".trans("labels.frontend.orden_venta")}}" class="btn btn-danger delete">
                    <i class="fa fa-trash" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans("buttons.general.crud.delete") }}</span>
                </a>
            </div>
        </div>
    </div>

    <div class="card card-body printableArea">
        <h3>
            <b>Orden de Venta</b>
            <span class="pull-right">N° {{ $ordenVenta->numero }}</span>
        </h3>
        <hr>
        <div class="row">
            <div class="col-md-12">
                <div class="pull-left">
                    <address>
                        <h3> &nbsp;<b class="text-danger">{{$empresa->razon_social}}</b></h3>
                        <p class="text-muted m-l-5">{{ (isset($empresa->domicilio->direccion)) ? $empresa->domicilio->direccion : '' }}
                            <br/> I.V.A.: {{ $empresa->condicionIva }}
                            <br/> CUIT: {{ $empresa->cuit }}
                            <br/> II.BB.: {{ $empresa->ingresos_brutos }}
                            <br/> Inicio de Actividades: {{($empresa->inicio_actividad) ? $empresa->inicio_actividad->format('d/m/Y') : ''}}</p>
                    </address>
                </div>
                <div class="pull-right text-right">
                    <address>
                        <h3 class="font-bold">
                            @if($ordenVenta->cliente)
                                <a href="{{url('/personas/cliente/'.$ordenVenta->cliente->id)}}">{{ $ordenVenta->cliente }}</a>
                        </h3>
                        <p class="text-muted m-l-30">{{ (isset($ordenVenta->cliente->domicilio->direccion)) ? $ordenVenta->cliente->domicilio->direccion : '' }}
                            <br/> I.V.A.: {{ $ordenVenta->cliente->condicionIva }}
                            <br/> CUIT: {{ $ordenVenta->cliente->dni }}
                            <br/> Cuenta: {{ $ordenVenta->cliente_id }}</p>
                        @else
                            Cliente Eliminado
                            </h3>
                        @endif
                    </address>
                </div>
            </div>
            <div class="col-md-2 col-xs-6 b-r text-center text-muted"> <span class="font-bold">Lista de Precios</span>
                <br>
                <p>
                    <a href="{{url('/ventas/lista-precios/'.$ordenVenta->listaPrecio->id)}}">{{ $ordenVenta->listaPrecio }}</a>
                </p>
            </div>
            <div class="col-md-3 col-xs-6 b-r text-center text-muted"> <span class="font-bold">Moneda/Cotización</span>
                <br>
                <p>
                    {{ $ordenVenta->moneda }} - <b class="text-success">$ {{ $ordenVenta->cotizacion }}</b>
                </p>
            </div>
            <div class="col-md-2 col-xs-6 b-r text-center text-muted"> <span class="font-bold">Fecha Orden</span>
                <br>
                <p>
                    <i class="fa fa-calendar"></i> {{ $ordenVenta->created_at->format('d/m/Y H:i') }}
                </p>
            </div>
            <div class="col-md-3 col-xs-6 text-center text-muted"> <span class="font-bold">Venta Asociada</span>
                <br>
                <p>
                    @if(isset($ordenVenta->venta))
                        <a href="{{url('ventas/venta/'.$ordenVenta->venta->id)}}">
                            {{ $ordenVenta->venta.' - Total: $'.$ordenVenta->venta->total_cobro }}
                        </a>
                    @endif
                </p>
            </div>
            <div class="col-md-2 col-xs-6 b-r text-center text-muted"> <span class="font-bold">Estado</span>
                <br>
                <p>
                    {{ $ordenVenta->estado }}
                </p>
            </div>
            <div class="col-md-12">
                <div class="table-responsive m-t-40" style="clear: both;">
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th class="text-center">#</th>
                            <th>Descripción</th>
                            <th class="text-right">Cantidad</th>
                            <th class="text-right">Pre/Uni. Neto</th>
                            <th class="text-right">Neto</th>
                            <th class="text-right">IVA</th>
                            <th class="text-right">Imp. Int.</th>
                            <th class="text-right">Total</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(!$ordenVenta->detalles->isEmpty())
                            @foreach($ordenVenta->detalles as $detalle)
                                <tr>
                                    <td class="text-center">
                                        {{ $loop->index + 1 }}
                                    </td>
                                    <td>
                                        @if(isset($detalle->articulo))
                                            <a href="{{url('/articulos/articulo/'.$detalle->articulo->id)}}">
                                                ({{ $detalle->articulo->codigo }}) {{ $detalle->articulo->nombre }} {{ isset($detalle->marca) ? ' - '.$detalle->marca : '' }} {{ isset($detalle->modelo) ? ' - '.$detalle->modelo : '' }}{{ isset($detalle->modelo_secundario) ? ' - '.$detalle->modelo_secundario : '' }}
                                            </a>
                                        @else
                                            Artículo Eliminado
                                        @endif
                                    </td>
                                    <td class="text-right">
                                        {{ number_format($detalle->cantidad , 0) }}
                                    </td>
                                    <td class="text-right">
                                        {{ $ordenVenta->moneda->signo }} {{ number_format($detalle->precio_neto, 2) }}
                                    </td>
                                    <td class="text-right">
                                        {{ $ordenVenta->moneda->signo }} {{ number_format($detalle->cantidad*$detalle->precio_neto , 2) }}
                                    </td>
                                    <td class="text-right">
                                        {{ number_format($porcentajes_iva[$detalle->porcentaje_iva], 2) }} %
                                    </td>
                                    <td class="text-right">
                                        {{ $ordenVenta->moneda->signo }} {{ number_format($detalle->cantidad*$detalle->importe_impuesto_interno, 2) }}
                                    </td>
                                    <td class="text-right">
                                        {{ $ordenVenta->moneda->signo }} {{ number_format(($detalle->cantidad * $detalle->precio_neto) * (($porcentajes_iva[$detalle->porcentaje_iva]/100) + 1) + ($detalle->cantidad*$detalle->importe_impuesto_interno), 2) }}
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-md-12">
                <div class="pull-right m-t-30 text-right">
                    <p>Importe Neto : {{ $ordenVenta->moneda->signo }} {{ number_format($ordenVenta->importe_neto, 2) }} </p>
                    <p>Iva : {{ $ordenVenta->moneda->signo }} {{ number_format($ordenVenta->importe_iva, 2) }} </p>
                    <p>Interes/Descuento : {{ $ordenVenta->moneda->signo }} {{ number_format($ordenVenta->importe_descuento, 2) }} </p>
                    <p>Impuestos Internos : {{ $ordenVenta->moneda->signo }} {{ number_format($ordenVenta->importe_impuesto_interno, 2) }} </p>
                    <hr>
                    <h3><b>Total :</b> {{ $ordenVenta->moneda->signo }} {{ number_format($ordenVenta->total, 2) }}</h3>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('after-scripts')
<script>
    $("body").on("click", ".delete", function () {
        swal({
            title: "{{trans('buttons.general.crud.delete').' '.trans('labels.frontend.orden_venta')}}",
            text: "¿Realmente desea eliminar este registro?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#ff8726',
            confirmButtonText: "Eliminar"
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '{{ url('/ecommerce/orden-venta/'.$ordenVenta->id) }}',
                    data: {"_method" : 'DELETE'},
                    success: function (msg) {
                        swal("Eliminado!", "El registro ha sido correctamente eliminado.", "success").then((result) => { window.location.href = '{{url('/ecommerce/orden-venta')}}'; });
                    }
                });
            }
        });
    });
</script>
@endsection