@extends('frontend.layouts.app')

@section ('title', trans('labels.frontend.orden_venta_titulo')." - ".trans('buttons.general.crud.create'))

@section('after-styles')
    {{ Html::style("css/frontend/plugins/jquery/jquery-ui.min.css") }}
@endsection

@section('content')
    <div class="row page-titles">
        <div class="col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('labels.frontend.orden_venta_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item"><a href="{{url('/ecommerce/orden-venta')}}">{{ trans('labels.frontend.orden_venta_titulo') }}</a></li>
                <li class="breadcrumb-item active">{{ trans('buttons.general.crud.create') }} {{ trans('labels.frontend.orden_venta') }}</li>
            </ol>
        </div>
        <div class="col-4">
            <div class="pull-right">
                <a href="{{ url('/ecommerce/orden-venta') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans('buttons.general.crud.back') }}</span>
                </a>
            </div>
        </div>
    </div>

    @tips($_SERVER['REQUEST_URI'])

    @include('includes.partials.messages')

    {!! Form::open(['url' => '/ecommerce/orden-venta', 'class' => 'form-horizontal', 'files' => true]) !!}
        @include ('frontend.ecommerce.orden-venta.form')
    {!! Form::close() !!}

@endsection

@section('after-scripts')
    {{ Html::script("js/frontend/plugins/jquery/jquery-ui.min.js") }}
    {{ Html::script("js/frontend/plugins/jquery/datepicker-es.js") }}
    {{ Html::script("js/frontend/plugins/maskedinput/jquery.maskedinput.min.js") }}

<script>
    $( document ).ready(function() {
        $('input.fecha').datepicker({
            firstDay: 0,
            dateFormat: 'dd/mm/yy'
        }).mask("99/99/9999", {placeholder: "dd/mm/aaaa"});
        $('input#numero').mask("99999999",{placeholder:"########"});
        $(function() {
            $( document ).tooltip({
                position: {
                    my: "right top",
                    at: "left-5 top-5",
                    collision: "none"
                }
            });
        });

        $(document).on("keydown.autocomplete", "#buscar_cliente", function (e) {
            $(this).autocomplete({
                source: '{{ url('/personas/cliente/obtenerClienteParaVenta') }}',
                minlength: 1,
                autoFocus: true,
                select: function (e, ui) {
                    $('input#buscar_cliente').val(ui.item.value);
                    $('input#cliente_id').val(ui.item.id);
                    if(ui.item.lista_precios_id != null)
                        $('select#lista_precio_id').val(ui.item.lista_precios_id);
                },
                response: function(e, ui) {
                    if (ui.content.length == 1){ // Si solo me devuelve un cliente, lo autocargo
                        ui.item = ui.content[0];
                        $('input#buscar_cliente').val(ui.item.value).blur();
                        $('input#cliente_id').val(ui.item.id);
                        if(ui.item.lista_precios_id != null)
                            $('select#lista_precio_id').val(ui.item.lista_precios_id);
                    }
                }
            });
        });

        $('select#moneda_id').on('change', function () {
            buscarCotizacion();
        });

        function buscarCotizacion() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $( "input[name*='_token']" ).val()
                }
            });
            var moneda_id = $('#moneda_id option:selected').val();
            $.ajax({
                type: "POST",
                url: '{{ url("/configuraciones/moneda/obtenerCotizacion") }}',
                data: {moneda_id: moneda_id},
                success: function( msg ) {
                    $("#cotizacion").val(msg);
                }
            });
        }

        $(document).on("keydown.autocomplete", ".buscar-articulo", function (e) {
            if (e.keyCode == 13) {
                e.preventDefault();
            }else{
                $(this).autocomplete({
                    source: '{{ url('/articulos/articulo/obtenerPrecioArticuloNoStock') }}/'+$('select#lista_precio_id option:selected').val(),
                    minlength: 1,
                    autoFocus: true,
                    select: function (e, ui) {
                        inputNombre = this.attributes['name'].value;
                        $('.buscar-articulo[name="' + inputNombre + '"]').val(ui.item.value);
                        $('.buscar-articulo[name="' + inputNombre + '"]').parent().find('input.articulo-id').val(ui.item.id);
                        if($('#moneda_id option:selected').val() != ui.item.moneda_id){
                            if(ui.item.cotizacion == 1){
                                ui.item.precio = (ui.item.precio / parseFloat($("#cotizacion").val())).toFixed(2);
                            }else{
                                ui.item.precio = (ui.item.precio * ui.item.cotizacion).toFixed(2);
                            }
                        }
                        $('.buscar-articulo[name="' + inputNombre + '"]').parent().parent().find('td input.precio').val(ui.item.precio);
                        $('.buscar-articulo[name="' + inputNombre + '"]').parent().parent().find('td input.iva').val(ui.item.iva);
                        $('.buscar-articulo[name="' + inputNombre + '"]').parent().parent().find('td input.porcentaje-iva').val(ui.item.porcentaje_iva);
                        agregarDetalle();
                    },
                    response: function(e, ui) {
                        if (ui.content.length == 1){ // Si solo me devuelve un articulo, lo autocargo
                            ui.item = ui.content[0];

                            inputNombre = this.attributes['name'].value;
                            $('.buscar-articulo[name="' + inputNombre + '"]').val(ui.item.value);
                            $('.buscar-articulo[name="' + inputNombre + '"]').parent().find('input.articulo-id').val(ui.item.id);
                            if($('#moneda_id option:selected').val() != ui.item.moneda_id){
                                if(ui.item.cotizacion == 1){
                                    ui.item.precio = (ui.item.precio / parseFloat($("#cotizacion").val())).toFixed(2);
                                }else{
                                    ui.item.precio = (ui.item.precio * ui.item.cotizacion).toFixed(2);
                                }
                            }
                            $('.buscar-articulo[name="' + inputNombre + '"]').parent().parent().find('td input.precio').val(ui.item.precio);
                            $('.buscar-articulo[name="' + inputNombre + '"]').parent().parent().find('td input.iva').val(ui.item.iva);
                            $('.buscar-articulo[name="' + inputNombre + '"]').parent().parent().find('td input.porcentaje-iva').val(ui.item.porcentaje_iva);
                            agregarDetalle();
                        }
                    }
                });
            }
        });

        $(document).on('click', '.borrar-detalle', function () {
            inputNombre = this.attributes['name'].value;
            idDetalle = parseInt($('.borrar-detalle[name="' + inputNombre + '"]').parent().parent().find('td input.id').val());

            $('.borrar-detalle[name="' + inputNombre + '"]').parent().parent().hide("slow");
            $('.borrar-detalle[name="' + inputNombre + '"]').parent().find('input.estado').val(0);
            $('.borrar-detalle[name="' + inputNombre + '"]').parent().parent().find('td input.subtotal-neto').val(0);
            $('.borrar-detalle[name="' + inputNombre + '"]').parent().parent().find('td input.subtotal-bruto').val(0);

            refrescarTotales();
        });

        calcularSubtotales();
        calcularPorcentajeImpuesto();
        refrescarTotales();
    });

    $("#agregar-detalle").click(function(){
        agregarDetalle();
    });

    $("#agregar-detalle-de-cantidad").on('focusin', function(){
        agregarDetalle('cantidad');
    });

    $("#agregar-detalle-de-articulo").on('focusin', function(){
        agregarDetalle('articulo');
    });

    function agregarDetalle(inputName) {
        var numero_orden = $('table#tabla-detalles-orden-venta tbody tr').length+ 1;
        $("#tabla-detalles-orden-venta").append('<tr>' +
            '<td>' +
            numero_orden +
            '<input type="hidden" class="id" name="detalles['+(numero_orden*-1)+'][id]" value="'+(numero_orden*-1)+'">' +
            '</td>' +
            '<td>' +
            '<input class="form-control cantidad text-center" name="detalles['+(numero_orden*-1)+'][cantidad]" type="number" value="1" max="" min="0" title="">' +
            '</td>' +
            '<td>' +
            '<input class="form-control buscar-articulo" name="detalles['+(numero_orden*-1)+'][articulo]"  placeholder="Buscar..." type="text">' +
            '<input class="articulo-id" name="detalles['+(numero_orden*-1)+'][articulo_id]" type="hidden" value="">' +
            '</td>' +
            '<td>' +
            '<div class="input-group">' +
            '<div class="left-icon">$</div>' +
            '<input class="form-control precio text-right" name="detalles['+(numero_orden*-1)+'][precio_neto]" type="number" step="0.01" tabindex="99999">' +
            '</div>' +
            '</td>' +
            '<td>' +
            '<div class="input-group">' +
            '<div class="left-icon">$</div>' +
            '<input class="form-control subtotal-neto text-right" name="detalles['+(numero_orden*-1)+'][total_neto]" type="number" step="0.01" readonly tabindex="99999">' +
            '</div>' +
            '</td>' +
            '<td>' +
            '<div class="input-group">' +
            '<div class="left-icon">%</div>' +
            '<input class="form-control iva text-right" name="detalles['+(numero_orden*-1)+'][iva]" type="number" step="0.01" readonly tabindex="99999">' +
            '</div>' +
            '<input type="hidden" class="porcentaje-iva" name="detalles['+(numero_orden*-1)+'][porcentaje_iva]" value="">' +
            '</td>' +
            '<td>' +
            '<div class="input-group">' +
            '<div class="left-icon">$</div>' +
            '<input class="form-control subtotal-bruto text-right" name="detalles['+(numero_orden*-1)+'][total_bruto]" type="number" step="0.01" readonly tabindex="99999">' +
            '</div>' +
            '<input type="hidden" class="impuesto-interno" name="detalles['+(numero_orden*-1)+'][importe_impuesto_interno]" value="">' +
            '<input type="hidden" class="subtotal-impuesto-interno" name="detalles['+(numero_orden*-1)+'][subtotal_impuesto_interno]" value="0">' +
            '</td>' +
            '<td>' +
            '<a class="btn btn-danger btn-circle borrar-detalle" name="detalles['+(numero_orden*-1)+'][eliminar]" tabindex="99999">' +
            '<i class="fa fa-trash-o" aria-hidden="true"></i>' +
            '</a>' +
            '<input type="hidden" class="estado" name="detalles['+(numero_orden*-1)+'][estado]" value="1">' +
            '</td>' +
            '</tr>');
        if(inputName != null){
            $("input[name='detalles["+(numero_orden*-1)+"]["+inputName+"]']").focus();
        }else{
            $("input[name='detalles["+(numero_orden*-1)+"][articulo]']").focus();
        }
        calcularSubtotales();
    }

    $("#porcentaje-impuesto").on('change', function (){
        calcularImporteImpuesto();
    });

    $("#importe-impuesto").on('change', function (){
        calcularPorcentajeImpuesto();
    });

    function refrescarTotales() {
        var primerProducto = $("#tabla-detalles-orden-venta tbody tr:visible").first().find('.precio').val();
        if(primerProducto){
            $.ajax({
                type: "POST",
                url: '{{ url("/ventas/calcularTotales") }}',
                data: $('form').serialize(),
                success: function( resultados ) {
                    $('#importe-neto').val(resultados['netos']['total']);
                    $('#importe-iva').val(resultados['ivas']['total']);
                    $('#importe-bruto').val(resultados['bruto']);
                    $('#total-impuesto-interno').val(resultados['impuestosInternos']);
                    $('#total').val(resultados['total']);
                    $('#totalGrande').html('$ ' + resultados['total']);
                }
            });
        }
    }

    function calcularSubtotales(){
        var $tablaTodasFilas = $("#tabla-detalles-orden-venta tbody tr");
        $tablaTodasFilas.each(function (index) {
            var $tablaFila = $(this);

            $tablaFila.find('.cantidad ,.precio, .buscar-articulo').on('change', function () {
                var cantidad = $tablaFila.find('.cantidad').val();
                var precio = $tablaFila.find('.precio').val();
                var iva = $tablaFila.find('.iva').val();

                var subtotalNeto = parseInt(cantidad, 10) * parseFloat(precio);
                var importeIva = subtotalNeto * parseFloat(iva) / 100;
                var subtotalBruto = subtotalNeto + importeIva;

                if (!isNaN(subtotalNeto)) {
                    $tablaFila.find('.subtotal-neto').val(subtotalNeto.toFixed(2));
                }
                if (!isNaN(subtotalBruto)) {
                    $tablaFila.find('.subtotal-bruto').val(subtotalBruto.toFixed(2));
                }else{
                    $tablaFila.find('.subtotal-bruto').val(subtotalNeto.toFixed(2));
                }
                refrescarTotales();
            });
        });
    }

    function calcularImporteImpuesto(){
        var importeBruto = $('#importe-bruto').val();
        var procentajeImpuesto = $('#porcentaje-impuesto').val();
        var importeImpuesto = parseFloat(importeBruto) * parseFloat(procentajeImpuesto) / 100;
        if (!isNaN(importeImpuesto)) {
            $('#importe-impuesto').val(importeImpuesto.toFixed(2));
        }else{
            $('#importe-impuesto').val('0.0');
        }
    }

    function calcularPorcentajeImpuesto(){
        var importeBruto = $('#importe-bruto').val();
        var importeImpuesto = $('#importe-impuesto').val();
        var procentajeImpuesto = parseFloat(importeImpuesto) * 100 / parseFloat(importeBruto);
        if (!isNaN(procentajeImpuesto)) {
            $('#porcentaje-impuesto').val(procentajeImpuesto.toFixed(2));
        }else{
            $('#porcentaje-impuesto').val('0.0');
        }
    }

</script>

@yield('scripts-dialog-cliente')
@endsection