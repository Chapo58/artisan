<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <body class="no-skin">
        <style>
            hr {
                margin-top: 0.5em;
                margin-bottom: 0.1em;
                border-width: 1px;
                border-top: 1px solid #000;
            }
        </style>
        <div class="main-container ace-save-state" id="main-container">
            <div class="container">
                <table style="width:100%">
                    <tr>
                        <td style="width:50%">
                            <img class="img-responsive" style="max-width: 200px; max-height: 80px;" src="{{$empresa->logo(true)}}"/>
                        </td>
                        <td>
                        </td>
                        <td>
                            <p>ORDEN DE VENTA Nº {{ $ordenVenta->numero }}</p>
                        </td>
                    </tr>
                </table>
                <table style="width:100%;font-size: 80%;">
                    <tr>
                        <td style="width:70%">{{ $empresa->razon_social }}</td>
                        <td>Emision: {{date('d/m/Y')}}</td>
                    </tr>
                    <tr>
                        <td>{{ (isset($empresa->domicilio->direccion)) ? $empresa->domicilio->direccion : '' }}</td>
                        <td>CUIT: {{ $empresa->cuit }}</td>
                    </tr>
                    <tr>
                        <td>
                            @if(isset($empresa->domicilio->codigo_postal))
                                {{$empresa->domicilio->codigo_postal}}
                            @endif
                            @if(isset($empresa->domicilio->localidad))
                                , {{$empresa->domicilio->localidad}}
                            @endif
                        </td>
                        <td>II.BB.: {{ $empresa->ingresos_brutos }}</td>
                    </tr>
                    <tr>
                        <td>
                            @if(isset($empresa->domicilio->provincia))
                                {{$empresa->domicilio->provincia}}
                            @endif
                            @if(isset($empresa->domicilio->pais))
                                , {{$empresa->domicilio->pais}}
                            @endif
                        </td>
                        <td>Inicio de Actividades: {{($empresa->inicio_actividad) ? $empresa->inicio_actividad->format('d/m/Y') : ''}}</td>
                    </tr>
                    <tr>
                        <td>{{ $empresa->telefono }}</td>
                        <td>I.V.A.: {{ $empresa->condicionIva }}</td>
                    </tr>
                </table>
                <hr>
                @if($ordenVenta->cliente)
                <table style="width:100%;font-size: 80%;">
                    <tr>
                        <td style="width:70%">{{ $ordenVenta->cliente->razon_social }}</td>
                        <td>Cuenta: {{ $ordenVenta->cliente_id }}</td>
                    </tr>
                    <tr>
                        <td>@if(isset($ordenVenta->cliente->domicilio)) {{$ordenVenta->cliente->domicilio->direccion}} @endif</td>
                        <td>CUIT: {{ $ordenVenta->cliente->cuit }}</td>
                    </tr>
                    <tr>
                        <td>@if(isset($ordenVenta->cliente->domicilio)) {{$ordenVenta->cliente->domicilio->localidad}} @endif</td>
                        <td>II.BB.: {{ $ordenVenta->cliente->ingresos_brutos }}</td>
                    </tr>
                    <tr>
                        <td>@if(isset($ordenVenta->cliente->domicilio)) {{$ordenVenta->cliente->domicilio->provincia}} @endif</td>
                        <td>I.V.A.: </td>
                    </tr>
                    <tr>
                        <td>{{ $ordenVenta->cliente->telefono }}</td>
                        <td></td>
                    </tr>
                </table>
                @endif
                <br>
                <table class="table table-condensed table-striped table-bordered" style="width:100%;font-size: 80%;">
                    <thead>
                        <tr class="info">
                            <th>Cantidad</th>
                            <th>Articulo</th>
                            <th>Precio Unitario</th>
                            <th>Total Neto</th>
                            <th>Alicuota</th>
                            <th>Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(!$ordenVenta->detalles->isEmpty())
                            @foreach($ordenVenta->detalles as $detalle)
                                <tr>
                                    <td>
                                        {{ number_format($detalle->cantidad , 0) }}
                                    </td>
                                    <td>
                                        {{ $detalle->articulo->nombre }}
                                    </td>
                                    <td>
                                        <b class="green">$ {{ number_format($detalle->precio_neto, 2) }}</b>
                                    </td>
                                    <td>
                                        <b class="green">$ {{ number_format($detalle->cantidad*$detalle->precio_neto , 2) }}</b>
                                    </td>
                                    <td>
                                        <b class="green">{{ number_format($porcentajes_iva[$detalle->porcentaje_iva], 2) }} %</b>
                                    </td>
                                    <td>
                                        <b class="green">$ {{ number_format( ($detalle->cantidad * $detalle->precio_neto) * ( ($porcentajes_iva[$detalle->porcentaje_iva]/100) + 1), 2) }}</b>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
                <div style="position: absolute;bottom: 100px;">
                    <hr />
                    <table style="font-size: 80%;width:50%;float:left;">
                        <tr>
                            <td>2. Comentario: {{ $ordenVenta->mensaje }}</td>
                        </tr>
                    </table>
                    <table style="font-size: 80%;width:50%;float:right;">
                        <tr>
                            <td style="width: 50%;">Neto:</td>
                            <td style="width: 50%;" align="right">$ {{ number_format($ordenVenta->importe_neto, 2) }}</td>
                        </tr>
                        <tr>
                            <td>IVA:</td>
                            <td align="right">$ {{ number_format($ordenVenta->importe_iva, 2) }}</td>
                        </tr>
                        <tr>
                            <td>Impuesto/Descuento:</td>
                            <td align="right">$ {{ number_format($ordenVenta->importe_descuento, 2) }}</td>
                        </tr>
                        <tr>
                            <td colspan=2><hr /></td>
                        </tr>
                        <tr style="font-size: 130%;font-weight: bold;">
                            <td>Total</td>
                            <td align="right">$ {{ number_format($ordenVenta->total, 2) }}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </body>
</html>
