@extends('frontend.layouts.app')

@section ('title','Calendario')

@section('after-styles')

{!! Html::style('css/frontend/plugins/bootstrap/css/bootstrap.min.css') !!}
{!! Html::style('js/frontend/plugins/fullcalendar/fullcalendar.min.css') !!}
{!! Html::style('css/frontend/plugins/datatimepicker/css/bootstrap-material-datetimepicker.css') !!}
{!! Html::style('css/frontend/plugins/colorpicker/css/bootstrap-colorpicker.min.css') !!}

@endsection

@section('content')

<div class="row page-titles">
    <div class="col-md-12 col-12 align-self-center">
        <h3 class="text-themecolor m-b-0 m-t-0">Calendario</h3>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
            <li class="breadcrumb-item active">Calendario</li>
        </ol>
    </div>
</div>

@tips($_SERVER['REQUEST_URI'])

<div class="card">
    <div class="card-body">
        <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
        @include('includes.partials.messages')
        <div id='calendar'></div>
    </div>
</div>

{{ Form::open(['url' => 'calendario/tareas', 'method' => 'POST', 'role' => 'form', 'id'=>'form-event']) }}
<div id="modal-event" class="modal fade" tabindex="-1" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4>Registro de nuevo evento</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            {{ Form::label('titulo', 'Titulo del evento') }}
                            {{ Form::text('titulo', old('titulo'), ['class' => 'form-control','required' => 'required']) }}
                            {!! $errors->first('titulo', '<p class="text-danger">:message</p>') !!}
                        </div>
                    </div><br>
                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            {{ Form::label('fecha_inicio', 'Fecha de inicio') }}
                            {{ Form::text('fecha_inicio', old('fecha_inicio'), ['class' => 'form-control', 'readonly' => 'true']) }}
                            {!! $errors->first('fecha_inicio', '<p class="text-danger">:message</p>') !!}
                        </div>
                        <div class="col-md-6 col-xs-12">
                            {{ Form::label('hora_inicio', 'Hora inicio') }}
                            {{ Form::text('hora_inicio', old('hora_inicio'), ['class' => 'form-control time_start']) }}
                            {!! $errors->first('hora_inicio', '<p class="text-danger">:message</p>') !!}
                        </div>
                    </div><br>
                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            {{ Form::label('fecha_fin', 'Fecha fin') }}
                            {{ Form::text('fecha_fin', old('fecha_fin'), ['class' => 'form-control date_end']) }}
                            {!! $errors->first('fecha_fin', '<p class="text-danger">:message</p>') !!}
                        </div>
                        <div class="col-md-6 col-xs-12">
                            {{ Form::label('hora_fin', 'Hora fin') }}
                            {{ Form::text('hora_fin', old('hora_fin'), ['class' => 'form-control time_end']) }}
                            {!! $errors->first('hora_fin', '<p class="text-danger">:message</p>') !!}
                        </div>
                    </div><br>
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            {{ Form::label('color', 'Color') }}
                            <div class="input-group colorpicker">
                                {{ Form::text('color', old('color'), ['class' => 'form-control']) }}
                                {!! $errors->first('color', '<p class="text-danger">:message</p>') !!}
                                <span class="input-group-addon"><i class="fa fa-tint"></i> Seleccionar</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <meta name="csrf-token" content="{{ csrf_token() }}">
                <input name="_method" id="metodo" type="hidden" value="">
                <button type="button" class="btn btn-dafault" data-dismiss="modal">Cancelar</button>
                <button type="button" id="delete" data-href="{{ url('events') }}" data-id="" class="btn btn-danger">Eliminar</button>
                {!! Form::submit('Guardar', ['class' => 'btn btn-themecolor']) !!}
            </div>
        </div>
    </div>
</div>
{{ Form::close() }}

@endsection
@section('after-scripts')

{!! Html::script('js/frontend/plugins/fullcalendar/lib/moment.min.js') !!}
{!! Html::script('js/frontend/plugins/fullcalendar/fullcalendar.min.js') !!}
{!! Html::script('js/frontend/plugins/fullcalendar/locale/es.js') !!}
{!! Html::script('js/frontend/plugins/bootstrap-datetimepicker/js/bootstrap-material-datetimepicker.js') !!}
{!! Html::script("js/frontend/plugins/dialog/bootstrap-dialog.min.js") !!}
{!! Html::script('js/frontend/plugins/colorpicker/js/bootstrap-colorpicker.min.js') !!}

<script>

    $(document).ready(function() {

        $('#calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,basicWeek,basicDay'
            },

            navLinks: true,
            editable: true,
            selectable: true,
            selectHelper: true,
            select: function(start){
                start = moment(start.format());
                $('#fecha_inicio').val(start.format('YYYY-MM-DD'));
                $('#titulo').val('');
                $('#hora_inicio').val('');
                $('#fecha_fin').val('');
                $('#hora_fin').val('');
                $('#color').val('');
                $('#form-event').attr('action', 'tareas');
                $('#metodo').val('POST');
                $('#delete').hide();
                $('#modal-event').modal('show');
            },
            events: [
                    @foreach($eventos as $item)
                {
                    id: "{{ $item->id }}",
                    title: "{{ $item->titulo }}",
                    start: "{{ $item->fecha_inicio->format('Y-m-d') }}{{($item->hora_inicio) ? ' '.$item->hora_inicio : ''}}",
                    end: "{{ ($item->fecha_fin) ? $item->fecha_fin->format('Y-m-d') : '' }}{{($item->hora_fin) ? ' '.$item->hora_fin : ''}}",
                    color: "{{ $item->color }}"

                },
                    @endforeach
            ],
            eventClick: function(event, jsEvent, view){
                var fecha_inicio = $.fullCalendar.moment(event.start).format('YYYY-MM-DD');
                var hora_inicio = $.fullCalendar.moment(event.start).format('HH:mm');
                var fecha_fin = $.fullCalendar.moment(event.end).format('YYYY-MM-DD');
                var hora_fin = $.fullCalendar.moment(event.end).format('HH:mm');
                $('#delete').attr('data-id', event.id);
                $('#titulo').val(event.title);
                $('#fecha_inicio').val(fecha_inicio);
                if(hora_inicio != "00:00"){
                    $('#hora_inicio').val(hora_inicio);
                }
                if(fecha_fin != "Invalid date"){
                    $('#fecha_fin').val(fecha_fin);
                }
                if(hora_fin != "Invalid date"){
                    $('#hora_fin').val(hora_fin);
                }
                $('#color').val(event.color);
                $('#modal-event').modal('show');
                $('#form-event').attr('action', '/calendario/tareas/'+event.id);
                $('#metodo').val('PATCH');
                $('#delete').show();
            },
        });

    });

    $('.colorpicker').colorpicker();

    $('.time_start').bootstrapMaterialDatePicker({
        date: false,
        format: 'HH:mm'
    });

    $('.date_end').bootstrapMaterialDatePicker({
        date: true,
        time: false,
        format: 'YYYY-MM-DD'
    });

    $('.time_end').bootstrapMaterialDatePicker({
        date: false,
        format: 'HH:mm'
    });

    $('#delete').on('click', function(){
        var x = $(this);
        var url = window.location.href;
        var delete_url = url+'/'+x.attr('data-id');

        $.ajax({
            url: delete_url,
            type: 'POST',
            data: { "_method" : 'DELETE', "_token": "{{ csrf_token() }}" },
            success: function(result){
                $('#modal-event').modal('hide');
                swal("Eliminado!", "Evento Eliminado correctamente", "success");
                location.href =url;
            },
            error: function(result){
                $('#modal-event').modal('hide');
                alert('ERROR');
            }
        });
    });

</script>

@endsection
