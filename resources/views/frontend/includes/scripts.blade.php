<!-- ============================================================== -->
<!-- All Jquery -->
<!-- ============================================================== -->
{{ Html::script("js/frontend/plugins/jquery/jquery.min.js") }}
<!-- Bootstrap tether Core JavaScript -->
{{ Html::script("js/frontend/plugins/bootstrap/popper.min.js") }}
{{ Html::script("js/frontend/plugins/bootstrap/bootstrap.min.js") }}
<!-- slimscrollbar scrollbar JavaScript -->
{{ Html::script("js/frontend/jquery.slimscroll.js") }}
<!--Wave Effects -->
{{ Html::script("js/frontend/waves.js") }}
<!--SweetAlert 2 -->
{{ Html::script("js/frontend/sweetalert2.min.js") }}
<!--Menu sidebar -->
{{ Html::script("js/frontend/sidebarmenu.js") }}
<!--stickey kit -->
{{ Html::script("js/frontend/plugins/sticky-kit-master/sticky-kit.min.js") }}
{{ Html::script("js/frontend/plugins/sparkline/jquery.sparkline.min.js") }}

<!--Custom JavaScript -->
@if($user->panel_orientacion)
    {{ Html::script("js/frontend/".$user->panel_orientacion."_custom.min.js") }}
@else
    {{ Html::script("js/frontend/horizontal_custom.min.js") }}
@endif
{{ Html::script("js/frontend/custom.js") }}

<script>
    function logout(){
        swal({
            title: "Cerrar Sesion",
            text: "¿Realmente desea salir del sistema?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#ff8726',
            confirmButtonText: "Salir"
        }).then((result) => {
            if (result.value) {
                window.location.href = "{{url('/logout')}}";
            }
        });
    }

    function marcarTipLeido(){
        $.ajax({
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '/configuraciones/usuario/marcarTipLeido',
            data: {'ruta' : '{{$_SERVER['REQUEST_URI']}}'},
            success: function() {}
        });
    }

    @if(isset($tawkto) && $tawkto->habilitado == 1)
    <!--Start of Tawk.to Script-->
    {!! $tawkto->codigo !!}
        Tawk_API.visitor = {
        name  : "{{$user->name}}",
        email : "{{$user->email}}"
    };
    Tawk_API.onLoad = function(){
        Tawk_API.setAttributes({
            'Empresa'    : "{{$user->empresa->razon_social}}"
        }, function(error){});
    };
    <!--End of Tawk.to Script-->
    @endif

</script>

<!-- Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-53444193-6"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-53444193-6');
</script>