<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">

    @if($user->panel_orientacion == 'vertical' || $user->panel_orientacion == 'dark')
        <!-- User profile -->
        <div class="user-profile" style="background: url('{{url('images/frontend/background/user-info.jpg')}}') no-repeat;">
            <!-- User profile image -->
            <div class="profile-img"> <img src="{{ ($user->avatar) ? $user->avatar : url('usuarios/no-image.png') }}" alt="Usuario" /> </div>
            <!-- User profile text-->
            <div class="profile-text">
                <a href="javascript:void(0)" class="dropdown-toggle link u-dropdown" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">
                    {{$user->name}} <span class="caret"></span>
                </a>
                <div class="dropdown-menu animated flipInY">
                    <a href="{{ url('/configuraciones/usuario/' . $user->id) }}" class="dropdown-item"><i class="ti-user"></i> {{trans('navs.frontend.user.account')}}</a>
                    @permission('view-backend')
                    <div class="dropdown-divider"></div> <a href="admin/dashboard" class="dropdown-item"><i class="ti-settings"></i> {{trans('navs.frontend.user.administration')}}</a>
                    @endauth
                    <div class="dropdown-divider"></div> <a href="javascript:void(0)" onclick="logout()" class="dropdown-item"><i class="fa fa-power-off"></i> {{trans('navs.general.logout')}}</a>
                </div>
            </div>
        </div>
        <!-- End User profile text-->
    @endif

        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class="{{ Request::is('dashboard*') ? 'active' : '' }}">
                    <a href="{{ url('/dashboard') }}" aria-expanded="false">
                        <i class="fa fa-home"></i>
                        <span class="hide-menu">Inicio </span>
                    </a>
                </li>
                @permiso('ventas',true)
                <li class="{{ Request::is('ventas/*') ? 'active' : '' }}">
                    <a class="has-arrow " href="javascript:void(0)" aria-expanded="false">
                        <i class="fa fa-ticket"></i>
                        <span>Ventas</span>
                    </a>
                    <ul aria-expanded="false" class="collapse">
                        @permiso('ventas')
                            <li><a href="{{ url('/ventas/venta') }}"><i class="menu-icon fa fa-list"></i> Ventas</a></li>
                        @endif
                        @permiso('facturacion-automatica')
                            <li><a href="{{ url('/ventas/venta/listado-ventas-automaticas') }}"><i class="menu-icon fa fa-list"></i> Ventas Automaticas</a></li>
                        @endif
                        @permiso('notas-credito')
                            <li><a href="{{ url('/ventas/nota-credito') }}"><i class="menu-icon fa fa-list"></i> Notas de Crédito</a></li>
                        @endif
                        @permiso('notas-debito')
                            <li><a href="{{ url('/ventas/nota-debito') }}"><i class="menu-icon fa fa-list"></i> Notas de Débito</a></li>
                        @endif
                        @permiso('recibos')
                            <li><a href="{{ url('/ventas/recibo') }}"><i class="menu-icon fa fa-list"></i> Recibos</a></li>
                        @endif
                        @permiso('presupuestos')
                            <li><a href="{{ url('/ventas/presupuesto') }}"><i class="menu-icon fa fa-list"></i> Presupuestos</a></li>
                        @endif
                        @permiso('facturacion-automatica')
                            <li><a href="{{ url('/ventas/factura-venta-automatica') }}"><i class="menu-icon fa fa-list"></i> Facturación Automatica</a></li>
                        @endif
                    </ul>
                </li>
                @endif
                @permiso('compras',true)
                <li class="{{ Request::is('compras/*') ? 'active' : '' }}">
                    <a class="has-arrow " href="javascript:void(0)" aria-expanded="false"><i class="fa fa-shopping-cart"></i><span class="hide-menu">Compras</span></a>
                    <ul aria-expanded="false" class="collapse">
                        @permiso('compras')
                            <li><a href="{{ url('/compras/compra') }}"><i class="menu-icon fa fa-list"></i> Compras</a></li>
                        @endif
                        @permiso('orden-compra')
                            <li><a href="{{ url('/compras/orden-compra') }}"><i class="menu-icon fa fa-list"></i> Ordenes de Compras</a></li>
                        @endif
                        @permiso('orden-pago')
                            <li><a href="{{ url('/compras/orden-pago') }}"><i class="menu-icon fa fa-list"></i> Órdenes de Pagos</a></li>
                        @endif
                    </ul>
                </li>
                @endif
                @permiso('caja',true)
                <li class="{{ Request::is('caja/*') ? 'active' : '' }}">
                    <a class="has-arrow " href="javascript:void(0)" aria-expanded="false"><i class="fa fa-inbox"></i><span class="hide-menu">Caja</span></a>
                    <ul aria-expanded="false" class="collapse">
                        @permiso('movimientos-caja')
                            <li><a href="{{ url('/caja/movimiento-caja') }}"><i class="menu-icon fa fa-caret-right"></i> Movimientos de Caja</a></li>
                        @endif
                        @permiso('historico-cajas')
                            <li><a href="{{ url('/caja/cierre-caja') }}"><i class="menu-icon fa fa-caret-right"></i> Historico de Cajas</a></li>
                        @endif
                        @permiso('administrar-cajas')
                            <li><a href="{{ url('/caja/cajas') }}"><i class="menu-icon fa fa-caret-right"></i> Administrar Cajas</a></li>
                        @endif
                        @permiso('conceptos-caja')
                            <li><a href="{{ url('/caja/concepto-caja') }}"><i class="menu-icon fa fa-caret-right"></i> Conceptos Cajas</a></li>
                        @endif
                    </ul>
                </li>
                @endif
                @permiso('articulos',true)
                <li class="{{ Request::is('articulos/*') ? 'active' : '' }}">
                    <a class="has-arrow " href="javascript:void(0)" aria-expanded="false"><i class="fa fa-tags"></i><span class="hide-menu">Artículos</span></a>
                    <ul aria-expanded="false" class="collapse">
                        @permiso('articulos')
                            <li><a href="{{ url('/articulos/articulo') }}"><i class="menu-icon fa fa-caret-right"></i> Artículos</a></li>
                        @endif
                        @permiso('movimientos-stock')
                            <li><a href="{{ url('/articulos/movimiento-stock') }}"><i class="menu-icon fa fa-caret-right"></i> Movimientos de Stock</a></li>
                        @endif
                        @permiso('grupos')
                            <li><a href="{{ url('/articulos/grupo-articulo') }}"><i class="menu-icon fa fa-caret-right"></i> Grupos</a></li>
                        @endif
                        @permiso('rubros')
                            <li><a href="{{ url('/articulos/rubro') }}"><i class="menu-icon fa fa-caret-right"></i> Rubros</a></li>
                        @endif
                        @permiso('marcas')
                            <li><a href="{{ url('/articulos/marca') }}"><i class="menu-icon fa fa-caret-right"></i> Marcas</a></li>
                        @endif
                        @permiso('listas-precios')
                            <li><a href="{{ url('/ventas/lista-precios') }}"><i class="menu-icon fa fa-caret-right"></i> Listas de Precios</a></li>
                        @endif
                    </ul>
                </li>
                @endif
                @permiso('personas',true)
                <li class="{{ Request::is('personas/*') ? 'active' : '' }}">
                    <a class="has-arrow " href="javascript:void(0)" aria-expanded="false"><i class="fa fa-users"></i><span class="hide-menu">Personas</span></a>
                    <ul aria-expanded="false" class="collapse">
                        @permiso('proveedores')
                        <li><a href="{{ url('/personas/proveedor') }}"><i class="menu-icon fa fa-caret-right"></i> Proveedores</a></li>
                        @endif
                        @permiso('clientes')
                            <li><a href="{{ url('/personas/cliente') }}"><i class="menu-icon fa fa-caret-right"></i> Clientes</a></li>
                        @endif
                        @permiso('cuentas-corrientes')
                            <li><a href="{{ url('/ventas/cuenta-corriente') }}"><i class="menu-icon fa fa-caret-right"></i> Cuentas Corrientes</a></li>
                        @endif
                    </ul>
                </li>
                @endif
                @permiso('fondos',true)
                <li class="{{ Request::is('fondos/*') ? 'active' : '' }}">
                    <a class="has-arrow " href="javascript:void(0)" aria-expanded="false"><i class="fa fa-money"></i><span class="hide-menu">Fondos</span></a>
                    <ul aria-expanded="false" class="collapse">
                        @permiso('movimientos-bancarios')
                            <li><a href="{{ url('/fondos/movimiento-bancario') }}"><i class="menu-icon fa fa-caret-right"></i> Movimientos Bancarios</a></li>
                        @endif
                        @permiso('gastos-bancarios')
                            <li><a href="{{ url('/fondos/gasto-bancario') }}"><i class="menu-icon fa fa-caret-right"></i> Gastos Bancarios</a></li>
                        @endif
                        @permiso('cupones-tarjetas')
                            <li><a href="{{ url('/ventas/cupon-tarjeta') }}"><i class="menu-icon fa fa-caret-right"></i> Cupones de Tarjetas</a></li>
                        @endif
                        @permiso('bancos')
                            <li><a href="{{ url('/fondos/banco') }}"><i class="menu-icon fa fa-caret-right"></i> Bancos</a></li>
                        @endif
                        @permiso('cuentas-bancarias')
                            <li><a href="{{ url('/fondos/cuenta-bancaria') }}"><i class="menu-icon fa fa-caret-right"></i> Cuentas Bancarias</a></li>
                        @endif
                        @permiso('chequeras')
                            <li><a href="{{ url('/fondos/chequera') }}"><i class="menu-icon fa fa-caret-right"></i> Chequeras</a></li>
                        @endif
                        @permiso('cheques-propios')
                            <li><a href="{{ url('/fondos/cheque-propio') }}"><i class="menu-icon fa fa-caret-right"></i> Cheques Propios</a></li>
                        @endif
                        @permiso('cartera-cheques')
                            <li><a href="{{ url('/fondos/cheque') }}"><i class="menu-icon fa fa-caret-right"></i> Cartera de Cheques</a></li>
                        @endif
                    </ul>
                </li>
                @endif
                @if(isset($user->empresa->nombre) && $user->empresa->nombre == "LQH" || isset($user->empresa->nombre) && $user->empresa->nombre == "Die Kupple")
                    <li class="{{ Request::is('produccion/*') ? 'active' : '' }}">
                    <a class="has-arrow " href="javascript:void(0)" aria-expanded="false"><i class="fa fa-cogs"></i><span class="hide-menu">Producción</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{ url('/produccion/planilla-corte') }}"><i class="menu-icon fa fa-caret-right"></i> Planillas de Corte</a></li>
                        <li><a href="{{ url('/produccion/rollo') }}"><i class="menu-icon fa fa-caret-right"></i> Rollos</a></li>
                        <li><a href="{{ url('/produccion/tipo-tela') }}"><i class="menu-icon fa fa-caret-right"></i> Tipos de Telas</a></li>
                        <li><a href="{{ url('/produccion/color') }}"><i class="menu-icon fa fa-caret-right"></i> Colores</a></li>
                        <li><a href="{{ url('/produccion/talleres') }}"><i class="menu-icon fa fa-caret-right"></i> Talleres</a></li>
                        <li><a href="{{ url('/produccion/tipo-corte') }}"><i class="menu-icon fa fa-caret-right"></i> Tipos de Cortes</a></li>
                        <li><a href="{{ url('/produccion/tipo-producto') }}"><i class="menu-icon fa fa-caret-right"></i> Tipos de Productos</a></li>
                        <li><a href="{{ url('/produccion/orden-pedido') }}"><i class="menu-icon fa fa-caret-right"></i> Ordenes de Pedidos</a></li>
                    </ul>
                </li>
                @endif
                @permiso('informes',true)
                <li class="{{ Request::is('informes/*') ? 'active' : '' }}">
                    <a class="has-arrow " href="javascript:void(0)" aria-expanded="false"><i class="fa fa-bar-chart-o"></i><span class="hide-menu">Informes</span></a>
                    <ul aria-expanded="false" class="collapse">
                        @permiso('libro-iva-venta')
                            <li><a href="{{ url('/informes/libro-iva-venta') }}"><i class="menu-icon fa fa-caret-right"></i> Libro de IVA Venta</a></li>
                        @endif
                        @permiso('libro-iva-compra')
                            <li><a href="{{ url('/informes/libro-iva-compra') }}"><i class="menu-icon fa fa-caret-right"></i> Libro de IVA Compra</a></li>
                        @endif
                        @permiso('regimen-compras-ventas')
                            <li><a href="{{ url('/informes/regimen') }}"><i class="menu-icon fa fa-caret-right"></i> Regimen de Compras y Ventas</a></li>
                        @endif
                        @permiso('conceptos-caja-informes')
                            <li><a href="{{ url('/informes/conceptos-caja') }}"><i class="menu-icon fa fa-caret-right"></i> Conceptos de Caja</a></li>
                        @endif
                        @permiso('vendedores')
                            <li><a href="{{ url('/informes/vendedor') }}"><i class="menu-icon fa fa-caret-right"></i> Vendedores</a></li>
                        @endif
                        @permiso('informe-actividades')
                            <li><a href="{{ url('/informes/actividad') }}"><i class="menu-icon fa fa-caret-right"></i> Resumen Actividades</a></li>
                        @endif
                    </ul>
                </li>
                @endif
                @permiso('calendario',true)
                <li class="{{ Request::is('calendario/*') ? 'active' : '' }}">
                    <a href="{{ url('/calendario/tareas') }}"><i class="fa fa-calendar"></i><span class="hide-menu">Calendario</span></a>
                </li>
                @endif
                @permiso('configuraciones',true)
                <li class="{{ Request::is('configuraciones/*') ? 'active' : '' }}">
                    <a class="has-arrow " href="javascript:void(0)" aria-expanded="false"><i class="fa fa-wrench"></i><span class="hide-menu">Configuraciones</span></a>
                    <ul aria-expanded="false" class="collapse">
                        @permiso('actividades-fiscales')
                        <li><a href="{{ url('/configuraciones/actividad-afip') }}"><i class="menu-icon fa fa-caret-right"></i> Actividades Fiscales</a></li>
                        @endif
                        @permiso('apariencia-sistema')
                            <li><a href="{{ url('/configuraciones/apariencia') }}"><i class="menu-icon fa fa-caret-right"></i> Apariencia del Sistema</a></li>
                        @endif
                        @permiso('configuracion-cuentas-corrientes')
                            <li><a href="{{ url('/configuraciones/tipo-cuenta-corriente') }}"><i class="menu-icon fa fa-caret-right"></i> Cuentas Corrientes</a></li>
                        @endif
                        @permiso('datos-empresa')
                            <li><a href="{{ url('/configuraciones/datos-empresa/'.$user->empresa->id.'/edit') }}"><i class="menu-icon fa fa-caret-right"></i> Datos Empresa</a></li>
                        @endif
                        @permiso('formas-pago')
                        <li>
                            <a class="has-arrow" href="javascript:void(0)" aria-expanded="false"><i class="menu-icon fa fa-caret-right"></i> Formas de Pago</a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="{{url('/configuraciones/tarjeta')}}">Tarjetas Aceptadas</a></li>
                                <li><a href="{{url('/configuraciones/plan-tarjeta')}}">Planes de Tarjetas</a></li>
                                <li><a href="{{url('/configuraciones/tarjeta-propia')}}">Tarjetas Propias</a></li>
                            </ul>
                        </li>
                        @endif
                        @permiso('monedas')
                        <li><a href="{{ url('/configuraciones/moneda') }}"><i class="menu-icon fa fa-caret-right"></i> Monedas</a></li>
                        @endif
                        @permiso('opciones')
                        <li><a href="{{ url('/configuraciones/opcion') }}"><i class="menu-icon fa fa-caret-right"></i> Opciones</a></li>
                        @endif
                        @permiso('puntos-de-venta')
                        <li><a href="{{ url('/configuraciones/punto-de-venta') }}"><i class="menu-icon fa fa-caret-right"></i> Puntos de Venta</a></li>
                        @endif
                        @permiso('usuarios')
                        <li><a href="{{ url('/configuraciones/usuario') }}"><i class="menu-icon fa fa-caret-right"></i> Usuarios</a></li>
                        @endif
                    </ul>
                </li>
                @endif
                @permiso('ecommerce',true)
                <li class="{{ Request::is('ecommerce/*') ? 'active' : '' }}">
                    <a class="has-arrow " href="javascript:void(0)" aria-expanded="false"><i class="fa fa-globe"></i><span class="hide-menu">E-Commerce</span></a>
                    <ul aria-expanded="false" class="collapse">
                        @permiso('parametros')
                        <li><a href="{{ url('/ecommerce/parametros') }}"><i class="menu-icon fa fa-caret-right"></i> Parametros</a></li>
                        @endif
                        @permiso('sliders')
                            <li><a href="{{ url('/ecommerce/slider') }}"><i class="menu-icon fa fa-caret-right"></i> Sliders</a></li>
                        @endif
                        @permiso('paginas')
                            <li><a href="{{ url('/ecommerce/paginas') }}"><i class="menu-icon fa fa-caret-right"></i> Paginas</a></li>
                        @endif
                        @permiso('caracteristicas')
                            <li><a href="{{ url('/ecommerce/caracteristicas') }}"><i class="menu-icon fa fa-caret-right"></i> Caracteristicas</a></li>
                        @endif
                        @permiso('suscripciones')
                        <li><a href="{{ url('/ecommerce/suscripciones') }}"><i class="menu-icon fa fa-caret-right"></i> Suscripciones</a></li>
                        @endif
                        @permiso('formas-pagos-ecommerce')
                        <li><a href="{{ url('/ecommerce/formas-pago') }}"><i class="menu-icon fa fa-caret-right"></i> Formas de Pago</a></li>
                        @endif
                        @permiso('ordenes-ventas')
                        <li><a href="{{ url('/ecommerce/orden-venta') }}"><i class="menu-icon fa fa-caret-right"></i> Ordenes Ventas</a></li>
                        @endif
                    </ul>
                </li>
                @endif
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>
