<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="csrf-token" content="{{ csrf_token() }}">

<title>@yield('title', app_name())</title>

<!-- Meta -->
<meta name="description" content="@yield('meta_description', 'Artisan')">
<meta name="author" content="@yield('meta_author', 'Luciano Ciattaglia')">
@yield('meta')

<!-- Favicon -->
<link rel="shortcut icon" href="{{{ asset('favicon.png') }}}">
<!-- For-Mobile-Apps-and-Meta-Tags -->
<meta name="mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-capable" content="yes">
<link rel="icon" sizes="192x192" href="{{{ asset('favicon.png') }}}">
<meta name="apple-mobile-web-app-title" content="Artisan">
<link href="{{{ asset('apple-touch-icon.png') }}}" rel="apple-touch-icon" />
<!-- //For-Mobile-Apps-and-Meta-Tags -->

@if($user->empresa->fecha_vencimiento && $user->empresa->fecha_vencimiento <= \Carbon\Carbon::now()->subDays(7) && Request::url() != url('/configuraciones/pago/create'))
    <meta http-equiv="refresh"
          content="0; url={{url('/configuraciones/pago/create')}}">
@endif

<!-- Styles -->
@yield('before-styles')

<!-- Bootstrap Core CSS -->
{{ Html::style("css/frontend/plugins/bootstrap/css/bootstrap.min.css") }}
<!-- SweetAlert 2 -->
{{ Html::style("css/frontend/sweetalert2.min.css") }}

<!-- Custom CSS -->
@if($user->panel_orientacion)
    {{ Html::style("css/frontend/".$user->panel_orientacion."_style.css") }}
@else
    {{ Html::style("css/frontend/horizontal_style.css") }}
@endif
{{ Html::style("css/frontend/custom.css") }}

<!-- Theme -->
@if($user->panel_color)
    <link rel="stylesheet" type="text/css" href="{{ asset('css/frontend/colors/'.$user->panel_color.'.css') }}" id="theme">
@else
    <link rel="stylesheet" type="text/css" href="{{ asset('css/frontend/colors/blue.css') }}" id="theme">
@endif

@yield('after-styles')

<!-- Scripts -->
<script>
    window.Laravel = <?php echo json_encode([
        'csrfToken' => csrf_token(),
    ]); ?>
</script>