<footer class="footer">
    <div class="row">
        <div class="col-6 text-left">
            © {{date('Y')}} Artisan Gestión
        </div>
        <div class="col-5 text-right">
            Desarrollado por <a href="https://ciatt.com.ar" target="_blank">Ciatt Software</a>
        </div>
    </div>
</footer>