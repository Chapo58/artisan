<!-- ============================================================== -->
<!-- Preloader - style you can find in spinners.css -->
<!-- ============================================================== -->
<div class="preloader">
    <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
</div>
    <!-- ============================================================== -->
    <!-- Topbar header - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <header class="topbar">
        <nav class="navbar top-navbar navbar-expand-md navbar-light">
            <!-- ============================================================== -->
            <!-- Logo -->
            <!-- ============================================================== -->
            <div class="navbar-header">
                <a class="navbar-brand" href="/dashboard">
                    <!-- Logo icon -->
                    <b id="mobile-icon">
                        <img src="{{url('images/frontend/logos/favicon.png')}}" alt="Logo" />
                    </b>
                    <span>
                        <img src="{{$user->empresa->logo()}}" alt="Logo" style="max-width: 200px; max-height: 55px;" />
                    </span>
                    <!--End Logo icon -->
                </a>
            </div>
            <!-- ============================================================== -->
            <!-- End Logo -->
            <!-- ============================================================== -->
            <div class="navbar-collapse">
                <!-- ============================================================== -->
                <!-- toggle and nav items -->
                <!-- ============================================================== -->
                <ul class="navbar-nav mr-auto mt-md-0">
                    <!-- This is  -->
                    <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="mdi mdi-menu"></i></a> </li>
                    <li class="nav-item"> <a class="nav-link sidebartoggler hidden-sm-down text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                    <!-- ============================================================== -->
                    <!-- Search -->
                    <!-- ============================================================== -->
                    <li class="nav-item hidden-sm-down search-box">
                        <a class="nav-link hidden-sm-down text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="ti-search"></i></a>
                        <form class="app-search">
                            <input type="text" class="form-control" placeholder="Buscar..."> <a class="srh-btn"><i class="ti-close"></i></a> </form>
                    </li>
                    {{--<li class="nav-item dropdown mega-dropdown"> <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="mdi mdi-view-grid"></i></a>
                        <div class="dropdown-menu scale-up-left">
                            <ul class="mega-dropdown-menu row">
                                <li class="col-lg-3 m-b-30">
                                    <h4 class="m-b-20">Titulo</h4>
                                </li>
                            </ul>
                        </div>
                    </li>--}}
                </ul>
                <!-- ============================================================== -->
                <!-- User profile and search -->
                <!-- ============================================================== -->
                <ul class="navbar-nav my-lg-0">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-plus"></i></a>
                        <div class="dropdown-menu dropdown-menu-right scale-up">
                            @if($user->permiso('venta-rapida') && $user->permiso('ventas'))
                                <a class="dropdown-item" href="{{ url('/ventas/venta/create_rapido') }}"><i class="mdi mdi-plus"></i> Nueva Venta Rapida</a>
                            @endif
                            @if($user->permiso('articulos'))
                                <a class="dropdown-item" href="{{ url('/articulos/articulo/create_rapido') }}"><i class="mdi mdi-plus"></i> Nuevo Articulo Rapido</a>
                            @endif
                            @if($user->permiso('presupuestos'))
                                <a class="dropdown-item" href="{{ url('/ventas/presupuesto/create') }}"><i class="mdi mdi-plus"></i> Nuevo Presupuesto</a>
                            @endif
                            @if($user->permiso('clientes'))
                                <a class="dropdown-item" href="{{ url('/personas/cliente/create') }}"><i class="mdi mdi-plus"></i> Nuevo Cliente</a>
                            @endif
                        </div>
                    </li>
                    <!-- ============================================================== -->
                    <!-- Comment -->
                    <!-- ============================================================== -->
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="mdi mdi-message"></i>
                            <div class="notify">
                                @if(alertas())
                                    <span class="heartbit"></span> <span class="point"></span>
                                @endif
                            </div>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right mailbox scale-up">
                            <ul>
                                <li>
                                    <div class="drop-title">Notificaciones</div>
                                </li>
                                <li>
                                    <div class="message-center">
                                        @if(alertas())
                                            @foreach(alertas() as $item)
                                                <a href="{{$item['url']}}">
                                                    <div class="btn btn-{{$item['color']}} btn-circle"><i class="fa {{$item['icon']}}"></i></div>
                                                    <div class="mail-contnet">
                                                        <h5>{{$item['titulo']}}</h5> <span class="mail-desc">{{$item['valor']}}</span> </div>
                                                </a>
                                            @endforeach
                                        @else
                                            <p class="text-center font-weight-bold">No hay nuevas notificaciones...</p>
                                        @endif
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <!-- ============================================================== -->
                    <!-- End Comment -->
                    <!-- ============================================================== -->

                    <!-- ============================================================== -->
                    <!-- Profile -->
                    <!-- ============================================================== -->
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="{{ ($user->avatar) ? $user->avatar : url('usuarios/no-image.png') }}" alt="Usuario" class="profile-pic" /></a>
                        <div class="dropdown-menu dropdown-menu-right scale-up">
                            <ul class="dropdown-user">
                                <li>
                                    <div class="dw-user-box">
                                        <div class="u-img"><img src="{{ ($user->avatar) ? $user->avatar : url('usuarios/no-image.png') }}" alt="Usuario"></div>
                                        <div class="u-text">
                                            <h4>{{$user->name}}</h4>
                                            <p class="text-muted">{{substr($user->email,0,15)}}...</p><a href="{{ url('/configuraciones/usuario/' . $user->id) }}" class="btn btn-rounded btn-danger btn-sm">Ver Perfil</a></div>
                                    </div>
                                </li>
                                <li><a href="{{url('/configuraciones/usuario/'.$user->id.'/edit')}}"><i class="ti-settings"></i> Configuraciones de Usuario</a></li>
                                @role('Usuario Admin')
                                    @if($user->empresa->fecha_vencimiento)
                                        <li><a href="{{ url('/configuraciones/pago') }}"><i class="ti-wallet"></i> Pagos del Sistema</a></li>
                                    @endif
                                @endauth
                                @permission('view-backend')
                                    <li role="separator" class="divider"></li>
                                    <li><a href="admin/dashboard"><i class="ti-settings"></i> {{trans('navs.frontend.user.administration')}}</a></li>
                                @endauth
                                <li><a href="{{url('/configuraciones/soporte')}}"><i class="ti-flag"></i> Actualizaciones</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="javascript:void(0)" onclick="logout()"><i class="fa fa-power-off"></i> {{trans('navs.general.logout')}}</a></li>
                                @if ($user && session()->has("admin_user_id") && session()->has("temp_user_id"))
                                    <li role="separator" class="divider"></li>
                                    <li><a href="{{ route("frontend.auth.logout-as") }}" class="text-danger"><i class="fa fa-arrow-circle-left"></i> VOLVER AL PANEL DE ADMINISTRACION</a></li>
                                @endif
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
