@extends('frontend.layouts.app')

@section ('title', trans('labels.frontend.apariencia_titulo')." - ".trans('buttons.general.crud.edit'))

@section('content')
    <div class="row page-titles">
        <div class="col-md-12 col-12 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('labels.frontend.apariencia_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item active">{{ trans('buttons.general.crud.edit') }} {{ trans('labels.frontend.apariencia') }}</li>
            </ol>
        </div>
    </div>

    @tips($_SERVER['REQUEST_URI'])

    @include('includes.partials.messages')

    {!! Form::open([
        'method'=>'POST',
        'class' => 'form-horizontal',
        'action' => ['Frontend\Configuraciones\AparienciaController@actualizar']
    ]) !!}

    <div class="card">
        <div class="card-body">
            <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
            <br>
            <div class="row form-group">
                <div class="col-md-5">
                    <ul id="themecolors" class="colores m-t-20">
                        <li><h4 class="font-medium m-t-30">Menu Luminoso</h4><hr></li>
                        <li><a href="javascript:void(0)" data-theme="default" class="default-theme">1</a></li>
                        <li><a href="javascript:void(0)" data-theme="green" class="green-theme">2</a></li>
                        <li><a href="javascript:void(0)" data-theme="red" class="red-theme">3</a></li>
                        <li><a href="javascript:void(0)" data-theme="blue" class="blue-theme">4</a></li>
                        <li><a href="javascript:void(0)" data-theme="purple" class="purple-theme">5</a></li>
                        <li><a href="javascript:void(0)" data-theme="megna" class="megna-theme">6</a></li>
                        <li class="d-block m-t-30"><h4 class="font-medium m-t-30">Menu Oscuro</h4><hr></li>
                        <li><a href="javascript:void(0)" data-theme="default-dark" class="default-dark-theme">7</a></li>
                        <li><a href="javascript:void(0)" data-theme="green-dark" class="green-dark-theme">8</a></li>
                        <li><a href="javascript:void(0)" data-theme="red-dark" class="red-dark-theme">9</a></li>
                        <li><a href="javascript:void(0)" data-theme="blue-dark" class="blue-dark-theme">10</a></li>
                        <li><a href="javascript:void(0)" data-theme="purple-dark" class="purple-dark-theme">11</a></li>
                        <li><a href="javascript:void(0)" data-theme="megna-dark" class="megna-dark-theme ">12</a></li>
                    </ul>
                </div>
                <div class="col-md-7">
                    <h4 class="font-medium m-t-30">Tipo de Menu</h4><hr>
                    <div class="demo-radio-button">
                        <table>
                            <tr>
                                <td>
                                    <input name="menu" value="horizontal" type="radio" id="horizontal" class="radio-col-cyan" />
                                    <label for="horizontal">Horizontal</label>
                                </td>
                                <td>
                                    <input name="menu" value="vertical" type="radio" id="vertical" class="radio-col-cyan" />
                                    <label for="vertical">Vertical</label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <img width="250" src="{{ url('images/frontend/horizontal.jpg') }}" alt="Horizontal">
                                </td>
                                <td>
                                    <img width="250" src="{{ url('images/frontend/vertical.jpg') }}" alt="Vertical">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input style="margin-top:15px;" name="menu" value="dark" type="radio" id="dark" class="radio-col-cyan" />
                                    <label style="margin-top:15px;" for="dark">Vertical Oscuro</label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <img width="250" src="{{ url('images/frontend/dark.jpg') }}" alt="Horizontal">
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            {{ Form::hidden('color') }}
            <hr>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-12">
                        {{ Form::submit(isset($submitButtonText) ? $submitButtonText : trans('buttons.general.save').' Cambios', ['class' => 'btn btn-lg btn-themecolor pull-right']) }}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <h4 class="font-medium">Descargar Acceso Directo a Artisan</h4>
            <hr>
            <div class="row">
                <div class="col-md-4">
                    <a href="{{url('downloads/Chrome/Artisan.exe')}}" target="_blank">
                        <h1 class="text-center"><i class="fa fa-chrome"></i> Chrome</h1>
                    </a>
                </div>
                <div class="col-md-4">
                    <a href="{{url('downloads/Kiosko/Artisan.exe')}}" target="_blank">
                        <h1 class="text-center"><i class="fa fa-window-maximize"></i> <i class="fa fa-chrome"></i> Chrome</h1>
                        <h2 class="text-center">(Pantalla Completa)</h2>
                    </a>
                </div>
                <div class="col-md-4">
                    <a href="{{url('downloads/Firefox/Artisan.exe')}}" target="_blank">
                        <h1 class="text-center"><i class="fa fa-firefox"></i> FireFox</h1>
                    </a>
                </div>
            </div>
        </div>
    </div>

    {!! Form::close() !!}

@endsection

@section('after-scripts')
    <!-- ============================================================== -->
    <!-- Style switcher -->
    <!-- ============================================================== -->
    {{ Html::script("js/frontend/plugins/styleswitcher/jQuery.style.switcher.js") }}

    <script>
        $(document).ready(function() {
            @if($user->panel_color)
                $('ul.colores li').find('a.{{$user->panel_color}}-theme').addClass('working');
                $('[name=color]').val('{{$user->panel_color}}');
            @else
                $('ul.colores li').find('a.blue-theme').addClass('working');
                $('[name=color]').val('blue');
            @endif

            @if($user->panel_orientacion)
                $('#{{$user->panel_orientacion}}').prop('checked', true);
            @else
                $('#horizontal').prop('checked', true);
            @endif

            $('ul.colores li').click(function(e) {
                var color = $(this).find("a").attr("data-theme");
                $('[name=color]').val(color);
            });
        });
    </script>
@endsection

