@extends('frontend.layouts.app')

@section ('title', trans('labels.frontend.opcion_titulo')." - ".trans('buttons.general.crud.show'))

@section('content')
    <div class="row page-titles">
        <div class="col-6 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('labels.frontend.opcion_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item"><a href="{{ url('/configuraciones/opcion') }}">{{ trans('labels.frontend.opcion_titulo') }}</a></li>
                <li class="breadcrumb-item">{{ trans('buttons.general.crud.show') }} {{ trans('labels.frontend.opcion') }}</li>
                <li class="breadcrumb-item active">{{ $opcion }}</li>
            </ol>
        </div>
        <div class="col-6">
            <div class="pull-right">
                <a href="{{ url('/configuraciones/opcion')  }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans('buttons.general.crud.back') }}</span>
                </a>
                @role('Usuario Admin')
                    <a href="{{ url('/configuraciones/opcion/' . $opcion->id . '/edit') }}" title="{{ trans('buttons.general.crud.edit') }}" class="btn btn-info">
                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                        <span class="hidden-xs-down">{{ trans('buttons.general.crud.edit') }}</span>
                    </a>
                @endauth
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
            <h4 class="font-medium m-t-30">{{ $opcion }}</h4>
            <hr>
            <div class="row">
                <div class="col-md-3 font-weight-bold">Sucursal</div>
                <div class="col-md-9">{{ isset($opcion->sucursal) ? $opcion->sucursal : 'Todas' }}</div>
            </div><hr>
            <div class="row">
                <div class="col-md-3 font-weight-bold">Opción</div>
                <div class="col-md-9">{{ $opcion->presentacion }}</div>
            </div><hr>
            <div class="row">
                <div class="col-md-3 font-weight-bold">Nombre</div>
                <div class="col-md-9">{{ $opcion->nombre }}</div>
            </div><hr>
            <div class="row">
                <div class="col-md-3 font-weight-bold">Valor</div>
                <div class="col-md-9">{{ $opcion->valor }}</div>
            </div><hr>
            <div class="row">
                <div class="col-md-3 font-weight-bold">Creado el</div>
                <div class="col-md-9">{{ $opcion->created_at->format('d/m/Y H:m') }}</div>
            </div><hr>
            <div class="row">
                <div class="col-md-3 font-weight-bold">Ultima Modificación</div>
                <div class="col-md-9">{{ $opcion->updated_at->format('d/m/Y H:m') }}</div>
            </div>
        </div>
    </div>

@endsection