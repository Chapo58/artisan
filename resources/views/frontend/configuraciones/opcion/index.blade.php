@extends ('frontend.layouts.app')

@section ('title', trans('labels.frontend.opcion_titulo')." - ".trans('navs.frontend.user.administration'))

@section('after-styles')
    {{ Html::style("css/frontend/plugins/datatables/responsive.dataTables.min.css") }}
@endsection

@section('content')
    <div class="row page-titles">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('labels.frontend.opcion_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item active">{{ trans('labels.frontend.opcion_titulo') }}</li>
            </ol>
        </div>
    </div>

    @tips($_SERVER['REQUEST_URI'])

    <div class="card">
        <div class="card-body">
            <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
            @include('includes.partials.messages')
            <div class="table-responsive">
                <table id="opcion-table" class="table selectable table-striped table-hover table-sm">
                    <thead>
                        <tr>
                            <td>&nbsp;&nbsp;</td>
                            <th class="min-desktop">Sucursal</th>
                            <th>Opción</th>
                            <th>Valor</th>
                            <th>{{ trans('labels.general.actions') }}</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

@endsection

@section('after-scripts')
    {{ Html::script("js/frontend/plugins/datatables/jquery.dataTables.min.js") }}
    {{ Html::script("js/frontend/plugins/datatables/dataTables.responsive.min.js") }}
    {{ Html::script("js/frontend/plugins/datatables/dataTables.select.min.js") }}

<script>
    $(document).ready(function() {
        $('#opcion-table').DataTable({
                "aaSorting": [],
                processing: true,
                serverSide: true,
                responsive: {
                    details: {
                        type: 'column',
                        target: 'tr'
                    }
                },
                "columnDefs": [{
                    targets: 0,
                    className: 'control',
                    orderable: false,
                    searchable: false,
                    defaultContent: '&nbsp;&nbsp;'
                },
                { responsivePriority: 1, targets: -1 }],
                ajax: '{!! route('frontend.configuraciones.OpcionData') !!}',
                columns: [
                    {data: null, name: ''},
                    {data: 'sucursal', name: 'sucursal'},
                    {data: 'presentacion', name: 'presentacion'},
                    {data: 'valor', name: 'valor'},
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ]
            });
    } );
</script>


@endsection