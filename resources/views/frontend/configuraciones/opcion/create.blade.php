@extends('frontend.layouts.app')

@section ('title', trans('labels.frontend.opcion_titulo')." - ".trans('buttons.general.crud.create'))

@section('content')
<div class="row page-titles">
        <div class="col-md-8 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('labels.frontend.opcion_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item"><a href="{{url('/configuraciones/opcion')}}">{{ trans('labels.frontend.opcion_titulo') }}</a></li>
                <li class="breadcrumb-item active">{{ trans('buttons.general.crud.create') }} {{ trans('labels.frontend.opcion') }}</li>
            </ol>
        </div>
        <div class="col-md-4">
            <div class="pull-right">
                <a href="{{ url('/configuraciones/opcion') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i> {{ trans('buttons.general.crud.back') }}
                </a>
            </div>
        </div>
    </div>

    @include('includes.partials.messages')

    {!! Form::open(['url' => '/configuraciones/opcion', 'class' => 'form-horizontal', 'files' => true]) !!}
        @include ('frontend.configuraciones.opcion.form')
    {!! Form::close() !!}

@endsection

@section('after-scripts')
{{ Html::script("js/frontend/plugins/maskedinput/jquery.maskedinput.min.js") }}

<script>
    $( document ).ready(function() {
        $('input[name="empleado[cuil]"]').mask("99-99999999-9",{placeholder:"##-########-#"});
    });
</script>
@endsection






    <div class="main-content">
        <div class="main-content-inner">
            @include ('frontend.includes.breadcrumbs')
            <div class="page-header">
                <h1>
                    {{ trans('labels.frontend.opcion_titulo') }}
                    <small>
                        <i class="ace-icon fa fa-angle-double-right"></i>
                        <small>{{ trans('buttons.general.crud.create') }} Opcion</small>
                    </small>

                    <div class="box-tools pull-right">
                        <a href="{{ url('/configuraciones/opcion') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning btn-sm">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> {{ trans('buttons.general.crud.back') }}
                        </a>
                    </div>
                </h1>
            </div>

            <div class="row">
                <div class="col-xs-12">

                    @include('includes.partials.messages')

                    {!! Form::open(['url' => '/configuraciones/opcion', 'class' => 'form-horizontal', 'files' => true]) !!}
                        @include ('frontend.configuraciones.opcion.form')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
