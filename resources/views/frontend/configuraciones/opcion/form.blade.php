

<div class="card">
    <div class="card-header">
        <div class="card-actions">
            <a class="" data-action="collapse"><i class="ti-minus"></i></a>
            <a class="btn-minimize" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
            <a class="btn-close" data-action="close"><i class="ti-close"></i></a>
        </div>
        <h4 class="card-title m-b-0">Información de la Opcion</h4>
    </div>
    <div class="card-body collapse show b-t">
        <div class="row form-group">
            <div class="col-md-6">
                {!! Form::text('presentacion', null, ['class' => 'form-control', 'required' => 'required', 'readonly']) !!}
                {!! $errors->first('presentacion', '<p class="text-danger">:message</p>') !!}
            </div>
            <div class="col-md-6">
                @if($array)
                    {!! Form::select('valor', $array, isset($opcion->valor) ? $opcion->valor : '', array('class' => 'form-control', 'required' => 'required')) !!}
                @else
                    {!! Form::text('valor', null, ['id' => 'valor', 'class' => 'form-control '.$esNumeracion, 'required' => 'required']) !!}
                @endif
                {!! $errors->first('valor', '<p class="text-danger">:message</p>') !!}
            </div>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-body">
        <div class="form-group">
            {{ Form::submit(isset($submitButtonText) ? $submitButtonText : trans('buttons.general.save'), ['class' => 'btn btn-themecolor btn-lg pull-right']) }}
        </div>
    </div>
</div>
