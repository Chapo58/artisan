@extends('frontend.layouts.app')

@section ('title', trans('labels.frontend.actividadafip_titulo')." - ".trans('buttons.general.crud.show'))

@section('content')
    <div class="row page-titles">
        <div class="col-6 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('labels.frontend.actividadafip_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item"><a href="{{url('/configuraciones/actividad-afip')}}">{{ trans('labels.frontend.actividadafip_titulo') }}</a></li>
                <li class="breadcrumb-item">{{ trans('buttons.general.crud.show') }} {{ trans('labels.frontend.actividadafip') }}</li>
                <li class="breadcrumb-item active">{{ $actividadafip }}</li>
            </ol>
        </div>
        <div class="col-6">
            <div class="pull-right">
                <a href="{{ url('/configuraciones/actividad-afip') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans('buttons.general.crud.back') }}</span>
                </a>
                @roles(['Usuario Admin', 'Usuario Gerente'])
                    <a href="{{ url('/configuraciones/actividad-afip/' . $actividadafip->id . '/edit') }}" title="{{ trans('buttons.general.crud.edit') }}" class="btn btn-info">
                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                        <span class="hidden-xs-down">{{ trans('buttons.general.crud.edit') }}</span>
                    </a>
                    <a href="javascript:void(0)" title="{{trans("buttons.general.crud.delete")." ".trans("labels.frontend.actividadafip")}}" class="btn btn-danger delete">
                        <i class="fa fa-trash" aria-hidden="true"></i>
                        <span class="hidden-xs-down">{{ trans("buttons.general.crud.delete") }}</span>
                    </a>
                @endauth
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
            <h4 class="font-medium m-t-30">{{ $actividadafip }}</h4>
            <hr>
            <div class="row">
                <div class="col-md-3 font-weight-bold">Codigo</div>
                <div class="col-md-9">{{ $actividadafip->codigo }}</div>
            </div><hr>
            <div class="row">
                <div class="col-md-3 font-weight-bold">Descripcion</div>
                <div class="col-md-9">{{ $actividadafip->descripcion }}</div>
            </div><hr>
            <div class="row">
                <div class="col-md-3 font-weight-bold">Creado el</div>
                <div class="col-md-9">{{ $actividadafip->created_at->format('d/m/Y H:m') }}</div>
            </div><hr>
            <div class="row">
                <div class="col-md-3 font-weight-bold">Ultima Modificación</div>
                <div class="col-md-9">{{ $actividadafip->updated_at->format('d/m/Y H:m') }}</div>
            </div>
        </div>
    </div>

@endsection

@section('after-scripts')
    <script>
        $("body").on("click", ".delete", function () {
            swal({
                title: "{{trans('buttons.general.crud.delete').' '.trans('labels.frontend.actividadafip')}}",
                text: "¿Realmente desea eliminar este registro?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#ff8726',
                confirmButtonText: "Eliminar"
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        type: 'POST',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: '{{ url('/configuraciones/actividad-afip/'.$actividadafip->id) }}',
                        data: {"_method" : 'DELETE'},
                        success: function (msg) {
                            swal("Eliminado!", "El registro ha sido correctamente eliminado.", "success").then((result) => { window.location.href = '{{url('/configuraciones/actividad-afip')}}'; });
                        }
                    });
                }
            });
        });
    </script>
@endsection