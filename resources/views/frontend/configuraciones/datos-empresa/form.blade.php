<div class="card">
    <div class="card-body">
        <ul class="nav nav-tabs customtab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#principal" role="tab">
                    <span class="hidden-sm-up"><i class="ti-info"></i></span>
                    <span class="hidden-xs-down">Principal</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#contacto" role="tab">
                    <span class="hidden-sm-up"><i class="ti-mobile"></i></span>
                    <span class="hidden-xs-down">Información de Contacto</span>
                </a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="principal" role="tabpanel">
                <div class="p-20">
                    <div class="row form-group">
                        <div class="col-md-6">
                            {!! Form::label('razon_social', 'Razon Social') !!}
                            {!! Form::text('razon_social', null, ['class' => 'form-control', 'required' => 'required']) !!}
                            {!! $errors->first('razon_social', '<p class="text-danger">:message</p>') !!}
                        </div>
                        <div class="col-md-6">
                            {!! Form::label('cuit', 'Cuit') !!}
                            {!! Form::text('cuit', null, ['class' => 'form-control cuit', 'required' => 'required']) !!}
                            {!! $errors->first('cuit', '<p class="text-danger">:message</p>') !!}
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-6">
                            {!! Form::label('nombre', 'Nombre') !!}
                            {!! Form::text('nombre', null, ['class' => 'form-control']) !!}
                            {!! $errors->first('nombre', '<p class="text-danger">:message</p>') !!}
                        </div>
                        <div class="col-md-6">
                            {!! Form::label('web', 'Sitio Web') !!}
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-globe"></i>
                                </div>
                                {!! Form::text('web', null, ['class' => 'form-control']) !!}
                            </div>
                            {!! $errors->first('web', '<p class="text-danger">:message</p>') !!}
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-4">
                            {!! Form::label('condicion_iva_id', 'Condicion Iva') !!}
                            {!! Form::select('condicion_iva_id', $condiciones_iva, isset($empresa) ? $empresa->condicion_iva_id : null, ['class' => 'form-control', 'required' => 'required']) !!}
                            {!! $errors->first('condicion_iva_id', '<p class="text-danger">:message</p>') !!}
                        </div>
                        <div class="col-md-4 {{ $errors->has('ingresos_brutos') ? 'has-danger' : ''}}">
                            {!! Form::label('ingresos_brutos', 'Ingresos Brutos') !!}
                            {!! Form::text('ingresos_brutos', null, ['class' => 'form-control']) !!}
                            {!! $errors->first('ingresos_brutos', '<p class="text-danger">:message</p>') !!}
                        </div>
                        <div class="col-md-4 {{ $errors->has('inicio_actividad') ? 'has-danger' : ''}}">
                            {!! Form::label('inicio_actividad', 'Inicio de Actividades') !!}
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                {!! Form::date('inicio_actividad', isset($empresa->inicio_actividad) ? $empresa->inicio_actividad->format('Y-m-d') : '', ['class' => 'form-control']) !!}
                            </div>
                            {!! $errors->first('inicio_actividad', '<p class="text-danger">:message</p>') !!}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            {!! Form::label('imagen_url', 'Logo Actual') !!}
                            <img src="{{ isset($empresa->imagen_url) ? $empresa->imagen_url : '/empresas/no-image.png' }}" alt="{{ isset($empresa->razon_social) ? $empresa->razon_social : '' }}" width="200" style="border: 2px solid lightslategrey;">
                        </div>
                        <div class="form-group col-md-9">
                            <div class="form-group">
                                <label>Seleccionar Logo</label>
                                <div class="row">
                                    <div class="col-md-8 col-sm-12">
                                        <div class="input-group image-preview">
                                            <input type="text" class="form-control image-preview-filename" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
                                            <span class="input-group-btn">
                                                <!-- image-preview-clear button -->
                                                <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                                                  <span class="glyphicon glyphicon-remove"></span> Limpiar
                                                </button>
                                                <!-- image-preview-input -->
                                                <div class="btn btn-default image-preview-input">
                                                  <span class="glyphicon glyphicon-folder-open"></span>
                                                  <span class="image-preview-input-title">Buscar</span>
                                                  <input type="file" name="image" accept=".png, .jpg, .jpeg">
                                                </div>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane p-20" id="contacto" role="tabpanel">
                <div class="row form-group">
                    <div class="col-md-6">
                        {!! Form::label('telefono', 'Telefono') !!}
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-phone"></i>
                            </div>
                            {!! Form::text('telefono', null, ['class' => 'form-control numero']) !!}
                        </div>
                        {!! $errors->first('telefono', '<p class="text-danger">:message</p>') !!}
                    </div>
                    <div class="col-md-6">
                        {!! Form::label('email', 'Email') !!}
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                            {!! Form::text('email', null, ['class' => 'form-control']) !!}
                        </div>
                        {!! $errors->first('email', '<p class="text-danger">:message</p>') !!}
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-6">
                        {!! Form::label('direccion', 'Dirección') !!}
                        {!! Form::text('domicilio[direccion]', isset($empresa->domicilio) ? $empresa->domicilio->direccion : '', ['class' => 'form-control']) !!}
                        {!! $errors->first('direccion', '<p class="text-danger">:message</p>') !!}
                    </div>
                    <div class="col-md-6">
                        {!! Form::label('localidad', 'Localidad') !!}
                        {!! Form::text('domicilio[localidad]', isset($empresa->domicilio) ? $empresa->domicilio->localidad : '', ['class' => 'form-control']) !!}
                        {!! $errors->first('localidad', '<p class="text-danger">:message</p>') !!}
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-4">
                        {!! Form::label('codigo_postal', 'Código Postal') !!}
                        {!! Form::text('domicilio[codigo_postal]', isset($empresa->domicilio) ? $empresa->domicilio->codigo_postal : '', ['class' => 'form-control']) !!}
                        {!! $errors->first('codigo_postal', '<p class="text-danger">:message</p>') !!}
                    </div>
                    <div class="col-md-4">
                        {!! Form::label('provincia_id', 'Provincia') !!}
                        {!! Form::select('domicilio[provincia_id]', $provincias, isset($empresa->domicilio->provincia) ? $empresa->domicilio->provincia->id : '', ['id' => 'provincia_id', 'class' => 'form-control']) !!}
                        {!! $errors->first('provincia_id', '<p class="text-danger">:message</p>') !!}
                    </div>
                    <div class="col-md-4">
                        {!! Form::label('pais_id', 'País') !!}
                        {!! Form::select('domicilio[pais_id]', $paises, isset($empresa->domicilio->pais) ? $empresa->domicilio->pais->id : 1, ['id' => 'pais_id', 'class' => 'form-control']) !!}
                        {!! $errors->first('pais_id', '<p class="text-danger">:message</p>') !!}
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-12">
                    {{ Form::submit(isset($submitButtonText) ? $submitButtonText : trans('buttons.general.save'), ['class' => 'btn btn-themecolor btn-lg pull-right']) }}
                </div>
            </div>
        </div>
    </div>
</div>

@include('backend.configuraciones.provincia.provincia_dialog')
@include('backend.configuraciones.localidad.localidad_dialog')
