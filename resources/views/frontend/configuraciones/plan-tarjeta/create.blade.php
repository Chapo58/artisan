@extends('frontend.layouts.app')

@section ('title', trans('labels.frontend.plantarjeta_titulo')." - ".trans('buttons.general.crud.create'))

@section('content')
    <div class="row page-titles">
        <div class="col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('labels.frontend.plantarjeta_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item"><a href="{{url('/configuraciones/plan-tarjeta')}}">{{ trans('labels.frontend.plantarjeta_titulo') }}</a></li>
                <li class="breadcrumb-item active">{{ trans('buttons.general.crud.create') }} {{ trans('labels.frontend.plantarjeta') }}</li>
            </ol>
        </div>
        <div class="col-4">
            <div class="pull-right">
                <a href="{{ url('/configuraciones/plan-tarjeta') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans('buttons.general.crud.back') }}</span>
                </a>
            </div>
        </div>
    </div>

    @tips($_SERVER['REQUEST_URI'])

    @include('includes.partials.messages')

    {!! Form::open(['url' => '/configuraciones/plan-tarjeta', 'class' => 'form-horizontal', 'files' => true]) !!}
        @include ('frontend.configuraciones.plan-tarjeta.form')
    {!! Form::close() !!}
                
@endsection

@section('after-scripts')
    {{ Html::script("js/frontend/plugins/maskedinput/jquery.maskedinput.min.js") }}
@endsection