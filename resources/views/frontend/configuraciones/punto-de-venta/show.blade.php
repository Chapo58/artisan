@extends('frontend.layouts.app')

@section ('title', trans('labels.frontend.puntodeventa_titulo')." - ".trans('buttons.general.crud.show'))

@section('content')

    <div class="row page-titles">
        <div class="col-6 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('labels.frontend.puntodeventa_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item"><a href="{{url('/configuraciones/punto-de-venta')}}">{{ trans('labels.frontend.puntodeventa_titulo') }}</a></li>
                <li class="breadcrumb-item">{{ trans('buttons.general.crud.show') }} {{ trans('labels.frontend.puntodeventa') }}</li>
                <li class="breadcrumb-item active">{{ $puntodeventa }}</li>
            </ol>
        </div>
        <div class="col-6">
            <div class="pull-right">
                <a href="{{ url('/configuraciones/punto-de-venta') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans('buttons.general.crud.back') }}</span>
                </a>
                @role('Usuario Admin')
                <a href="{{ url('/configuraciones/punto-de-venta/' . $puntodeventa->id . '/edit') }}" title="{{ trans('buttons.general.crud.edit') }}" class="btn btn-info">
                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans('buttons.general.crud.edit') }}</span>
                </a>
                <a href="javascript:void(0)" title="{{trans("buttons.general.crud.delete")." ".trans("labels.frontend.puntodeventa")}}" class="btn btn-danger delete">
                    <i class="fa fa-trash" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans("buttons.general.crud.delete") }}</span>
                </a>
                @endauth
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
            <h4 class="font-medium m-t-30">{{ $puntodeventa }}</h4>
            <hr>
            <div class="row">
                <div class="col-md-3 font-weight-bold">Numero</div>
                <div class="col-md-9">{{ $puntodeventa->numero }}</div>
            </div><hr>
            <div class="row">
                <div class="col-md-3 font-weight-bold">Tipo Punto de Venta</div>
                <div class="col-md-9">{{ $puntodeventa->tipo }}</div>
            </div><hr>
            <div class="row">
                <div class="col-md-3 font-weight-bold">Sucursal</div>
                <div class="col-md-9">{{ $puntodeventa->sucursal }}</div>
            </div><hr>
            @if($puntodeventa->tipo == "Impresora Fiscal")
                <div class="row">
                    <div class="col-md-2 font-weight-bold">Host</div>
                    <div class="col-md-4">{{ $puntodeventa->host_fiscal }}</div>

                    <div class="col-md-2 font-weight-bold">Puerto</div>
                    <div class="col-md-4">{{ $puntodeventa->puerto_fiscal }}</div>
                </div><hr>
                <div class="row">
                    <div class="col-md-2 font-weight-bold">Modelo</div>
                    <div class="col-md-4">{{ $tiposControladoras[$puntodeventa->modelo_impresora_fiscal] }}</div>

                    <div class="col-md-2 font-weight-bold">Baudios</div>
                    <div class="col-md-4">{{ $puntodeventa->baudios_fiscal }}</div>
                </div><hr>
                <div class="row">
                    <div class="col-md-3 font-weight-bold">Usuario</div>
                    <div class="col-md-9">{{ $puntodeventa->usuario }}</div>
                </div><hr>
            @endif
            @if($puntodeventa->tipo == "Comandera")
                <div class="row">
                    <div class="col-md-3 font-weight-bold">Anchura Papel</div>
                    <div class="col-md-9">{{ $puntodeventa->anchura_papel_comandera }} cm</div>
                </div><hr>
            @endif
            <div class="row">
                <div class="col-md-3 font-weight-bold">Creado el</div>
                <div class="col-md-9">{{ $puntodeventa->created_at->format('d/m/Y H:m') }}</div>
            </div><hr>
            <div class="row">
                <div class="col-md-3 font-weight-bold">Ultima Modificación</div>
                <div class="col-md-9">{{ $puntodeventa->updated_at->format('d/m/Y H:m') }}</div>
            </div>
        </div>
    </div>

@endsection

@section('after-scripts')

    <script>
        $("body").on("click", ".delete", function () {
            swal({
                title: "{{trans('buttons.general.crud.delete').' '.trans('labels.frontend.puntodeventa')}}",
                text: "¿Realmente desea eliminar este registro?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#ff8726',
                confirmButtonText: "Eliminar"
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        type: 'POST',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: '{{ url('configuraciones/puntodeventa'.$puntodeventa->id) }}',
                        data: {"_method" : 'DELETE'},
                        success: function (msg) {
                            swal("Eliminado!", "El registro ha sido correctamente eliminado.", "success").then((result) => { window.location.href = '{{url('/fondos/banco')}}'; });
                        }
                    });
                }
            });
        });
    </script>

@endsection

