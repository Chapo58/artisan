<div class="card">
    <div class="card-body">
        <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
        <br>
        <div class="row form-group">
                <div class="col-md-2">
                    {!! Form::label('numero', 'Numero') !!}
                    {!! Form::text('numero', null, ['id' => 'codigo', 'class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('numero', '<p class="text-danger">:message</p>') !!}
                </div>
                <div class="col-md-4">
                    {!! Form::label('tipo_punto_id', 'Tipo') !!}
                    {!! Form::select('tipo_punto_id',$tipos_punto_venta, isset($puntodeventa->tipo) ? $puntodeventa->tipo->id : '', ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('tipo_punto_id', '<p class="text-danger">:message</p>') !!}
                </div>
                <div class="col-md-6">
                    {!! Form::label('sucursal_id', 'Sucursal') !!}
                    {!! Form::select('sucursal_id',$sucursales, isset($puntodeventa->sucursal) ? $puntodeventa->sucursal_id : '', ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('sucursal_id', '<p class="text-danger">:message</p>') !!}
                </div>

        </div>
                <div class="row form-group" id="datosFiscal" style="display:none">
                    <div class="col-md-3">
                        {!! Form::label('host_fiscal', 'Host') !!}
                        <i class="fa fa-question-circle text-info mytooltip">
                            <span class="tooltip-content3">Dirección IP de la computadora en la cual se va a facturar. Se recomienda dejarlo por defecto en localhost</span>
                        </i>
                        {!! Form::text('host_fiscal', 'localhost', ['id' => 'host_fiscal', 'class' => 'form-control'])  !!}
                        {!! $errors->first('host_fiscal', '<p class="text-danger">:message</p>') !!}
                    </div>
                    <div class="col-md-3">
                        {!! Form::label('puerto_fiscal', 'Puerto') !!}
                        <i class="fa fa-question-circle text-info mytooltip">
                            <span class="tooltip-content3">Puerto COM de la controladora fiscal</span>
                        </i>
                        {!! Form::text('puerto_fiscal', null, ['id' => 'puerto_fiscal', 'class' => 'form-control'])  !!}
                        {!! $errors->first('puerto_fiscal', '<p class="text-danger">:message</p>') !!}
                    </div>
                    <div class="col-md-3">
                        {!! Form::label('modelo_impresora_fiscal', 'Modelo') !!}
                        {!! Form::select('modelo_impresora_fiscal', $tiposControladoras, isset($puntodeventa->modelo_impresora_fiscal) ? $puntodeventa->modelo_impresora_fiscal : '', ['class' => 'form-control'])  !!}
                        {!! $errors->first('modelo_impresora_fiscal', '<p class="text-danger">:message</p>') !!}
                    </div>
                    <div class="col-md-3">
                        {!! Form::label('baudios_fiscal', 'Baudios') !!}
                        <i class="fa fa-question-circle text-info mytooltip">
                            <span class="tooltip-content3">Baudios con los que trabaja la controladora. Se recomienda dejarlo por defecto en 9600</span>
                        </i>
                        {!! Form::text('baudios_fiscal', 9600, ['id' => 'baudios_fiscal', 'class' => 'form-control'])  !!}
                        {!! $errors->first('baudios_fiscal', '<p class="text-danger">:message</p>') !!}
                    </div>
                    <div class="col-md-3 m-t-20">
                        {!! Form::label('usuario_id_fiscal', 'Usuario') !!}
                        <i class="fa fa-question-circle text-info mytooltip">
                            <span class="tooltip-content3">El usuario que se utilizara en la PC que este conectada a la controladora</span>
                        </i>
                        {!! Form::select('usuario_id_fiscal',$usuarios, isset($puntodeventa->usuario) ? $puntodeventa->usuario->id : '', ['class' => 'form-control']) !!}
                        {!! $errors->first('usuario_id_fiscal', '<p class="text-danger">:message</p>') !!}
                    </div>
                    <div class="col-md-3 m-t-20">
                        {!! Form::label('descarga', 'Descargas') !!}
                        <i class="fa fa-question-circle text-info mytooltip">
                            <span class="tooltip-content3">Instalador de la controladora fiscal. Solo debe instalarse en la computadora que este conectada a la controladora</span>
                        </i><br>
                        <a href="{{url('downloads/Drivers.rar')}}" target="_blank" class="btn btn-themecolor"><i class="fa fa-download"></i>&nbsp;&nbsp;Descargar Driver</a>
                    </div>
                </div>

                <div class="row form-group {{ $errors->has('anchura_papel_comandera') ? 'has-error' : ''}}" id="datosComandera" style="display:none">
                    <div class="col-md-6">
                    {!! Form::label('anchura_papel_comandera', 'Anchura Papel', ['class' => 'col-md-4 control-label']) !!}
                    {!! Form::number('anchura_papel_comandera', null, ['id' => 'anchura_papel_comandera', 'class' => 'form-control', 'placeholder' => 'Anchura Papel en cm', 'step' => '0.1']) !!}
                    {!! $errors->first('anchura_papel_comandera', '<p class="text-danger">:message</p>') !!}
                    </div>
                </div>

                <div class="row form-group" id="datosElectronica" style="display:none">
                    <div class="col-md-6">
                        {!! Form::label('cert', 'Certificado') !!}
                        {!! Form::file('cert') !!}
                    </div>
                    <div class="col-md-6">
                        {!! Form::label('key', 'Clave Privada') !!}
                        {!! Form::file('key') !!}
                    </div>
                </div>
        <hr>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-12">
                    {{ Form::submit(isset($submitButtonText) ? $submitButtonText : trans('buttons.general.save'), ['class' => 'btn btn-lg btn-themecolor pull-right']) }}
                </div>
            </div>
        </div>
    </div>
</div>




@section('after-scripts')

    <script>
        $(document).ready(function() {
            @if(isset($puntodeventa->tipo) && $puntodeventa->tipo->id == 1)
                $('#datosFiscal').show();
            @endif
            @if(isset($puntodeventa->tipo) && $puntodeventa->tipo->id == 3)
                $('#datosElectronica').show();
            @endif
            @if(isset($puntodeventa->tipo) && $puntodeventa->tipo->id == 4)
                $('#datosComandera').show();
            @endif
        } );

        $('#tipo_punto_id').on('change',function(e){
            if($(this).val() == 1) {
                $('#datosFiscal').show();
                $('#datosComandera').hide();
                $('#datosElectronica').hide();
            } else if($(this).val() == 3) {
                $('#datosElectronica').show();
                $('#datosFiscal').hide();
                $('#datosComandera').hide();
            } else if($(this).val() == 4) {
                $('#datosComandera').show();
                $('#datosFiscal').hide();
                $('#datosElectronica').hide();
            } else {
                $('#datosFiscal').hide();
                $('#datosComandera').hide();
                $('#datosElectronica').hide();
            }
        });
    </script>

@endsection
