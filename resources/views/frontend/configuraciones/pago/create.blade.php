@extends('frontend.layouts.app')

@section ('title', trans('labels.frontend.pago_titulo')." - ".trans('buttons.general.crud.create'))

@section('after-styles')
    {{ Html::style("css/frontend/plugins/wizard/steps.css") }}
@endsection

@section('content')
    <div class="row page-titles">
        <div class="col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('labels.frontend.pago_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item"><a href="{{url('/configuraciones/pago')}}">{{ trans('labels.frontend.pago_titulo') }}</a></li>
                <li class="breadcrumb-item active">{{ trans('buttons.general.crud.create') }} {{ trans('labels.frontend.pago') }}</li>
            </ol>
        </div>
        <div class="col-4">
            <div class="pull-right">
                <a href="{{ url('/configuraciones/pago') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans('buttons.general.crud.back') }}</span>
                </a>
            </div>
        </div>
    </div>

    @tips($_SERVER['REQUEST_URI'])

    @if($user->empresa->fecha_vencimiento && $user->empresa->fecha_vencimiento <= \Carbon\Carbon::now()->subDays(7))
    <div class="col-md-12">
        <div class="card">
            <div class="d-flex flex-row">
                <div class="p-10 bg-danger">
                    <h3 class="text-white box m-b-0"><i class="fa fa-exclamation-triangle"></i></h3>
                </div>
                <div class="align-self-center m-l-20">
                    <h3 class="m-b-0 text-info">Su cuenta ha vencido, por favor realice un pago para continuar utilizando su sistema.</h3>
                    <h5 class="text-muted m-b-0">Fecha de Vencimiento: <strong>{{$user->empresa->fecha_vencimiento->format('d/m/Y')}}</strong></h5>
                </div>
            </div>
        </div>
    </div>
    @endif

    @include('includes.partials.messages')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body wizard-content">
                    {!! Form::open(['url' => '/configuraciones/pago', 'class' => 'tab-wizard wizard-circle', 'files' => true, 'id' => 'formPago']) !!}
                        @include ('frontend.configuraciones.pago.form')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@endsection

@section('after-scripts')
    {{ Html::script("js/frontend/plugins/wizard/jquery.steps.min.js") }}
    {{ Html::script("js/frontend/plugins/wizard/jquery.validate.min.js") }}

    <script>
        var plan = 0;
        $(document).ready(function() {
            disableNext();
        });

        stepsWizard = $(".tab-wizard").steps({
            headerTag: "h6"
            , bodyTag: "section"
            , transitionEffect: "fade"
            , titleTemplate: '<span class="step">#index#</span> #title#'
            , labels: {
                finish: "Confirmar"
            },
            onFinished: function (event, currentIndex) {
             //   $('form#formPago').submit();
                data = $(this).serialize();
                $.ajax({
                    type: "POST",
                    url: "/configuraciones/pago",
                    data: data,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    beforeSend: function() {
                        swal({
                            title: 'Procesando Pago',
                            text: 'Procesando Pago... Por favor aguarde...',
                            onOpen: () => {
                                swal.showLoading()
                            }
                        })
                    },
                    success: function( response ) {
                        swal({
                            title: response['titulo'],
                            text: response['mensaje'],
                            timer: 2000,
                            type: "success",
                            showConfirmButton: false
                        }).then(
                            window.location.href = response['url']
                        )
                    },
                });
            },
            onStepChanging: function (event, currentIndex, newIndex){
                if(currentIndex==0 && plan==0){
                    swal("Debes seleccionar un plan primero.");
                    return false;
                } else if(currentIndex==1){
                    calcularPago();
                    return true;
                } else {
                    return true;
                }
            }
        });

        function seleccionarPlan(id){
            plan = id;
            $('[name=plan_id]').val(plan);
            stepsWizard.steps("next");
        }

        function calcularPago(){
            var periodo = $('input[name=periodo]:checked', '#formPago').val();
            var forma_pago_id = $('input[name=forma_pago_id]:checked', '#formPago').val();
            $.ajax({
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ url("/configuraciones/pago/obtenerInformacion") }}',
                data: {periodo: periodo, forma_pago_id: forma_pago_id, plan_id: plan},
                success: function( response ) {
                    if(response['instrucciones']){
                        $('#info-contenido').html(response['instrucciones']);
                        $('#info').show();
                    }
                    $('#importe').html('$ '+response['importe']);
                }
            });
        }

        function disableNext(){
            var nextButton = $(".actions ul li:nth-child(2) a");
            buttonEnabled = $(".actions ul li:nth-child(2)").addClass("disabled").attr("aria-disabled", "true");
        }
    </script>
@endsection
