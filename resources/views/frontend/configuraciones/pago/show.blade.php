@extends('frontend.layouts.app')

@section ('title', trans('labels.frontend.pago_titulo')." - ".trans('buttons.general.crud.show'))

@section('content')
    <div class="row page-titles">
        <div class="col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('labels.frontend.pago_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item"><a href="{{url('/configuraciones/pago')}}">{{ trans('labels.frontend.pago_titulo') }}</a></li>
                <li class="breadcrumb-item">{{ trans('buttons.general.crud.show') }} {{ trans('labels.frontend.pago') }}</li>
                <li class="breadcrumb-item active">{{ $pago }}</li>
            </ol>
        </div>
        <div class="col-md-4">
            <div class="pull-right">
                <a href="{{ url('/configuraciones/pago') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans('buttons.general.crud.back') }}</span>
                </a>
                @if($pago->estado == 0)
                <a href="javascript:void(0)" title="{{trans("buttons.general.crud.delete")." ".trans("labels.frontend.pago")}}" class="btn btn-danger delete">
                    <i class="fa fa-trash" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans("buttons.general.crud.delete") }}</span>
                </a>
                @endif
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
            <h4 class="font-medium m-t-30">{{ $pago }}</h4>
            <hr>
            <div class="row">
                <div class="col-md-3">Usuario</div>
                <div class="col-md-3">{{ $pago->usuario }}</div>
            </div><hr>
            <div class="row">
                <div class="col-md-3">Empresa</div>
                <div class="col-md-3">{{ $pago->empresa }}</div>
            </div><hr>
            <div class="row">
                <div class="col-md-3">Forma de Pago</div>
                <div class="col-md-3">{{ $pago->formaPago }}</div>
            </div><hr>
            <div class="row">
                <div class="col-md-3">Plan</div>
                <div class="col-md-3">{{ $pago->plan }}</div>
            </div><hr>
            <div class="row">
                <div class="col-md-3">Periodo</div>
                <div class="col-md-3">{{ $pago->periodo }}</div>
            </div><hr>
            <div class="row">
                <div class="col-md-3">Importe</div>
                <div class="col-md-3">$ {{ number_format($pago->importe,2,'.',',') }}</div>
            </div><hr>
            <div class="row">
                <div class="col-md-3">Estado</div>
                <div class="col-md-3">
                    <span class="label @if($pago->estado == 0)label-warning @elseif($pago->estado == 1)label-success @elseif($pago->estado == 2)label-danger @else label-primary @endif">
                        {{ $estadosPagos[$pago->estado] }}
                    </span>
                </div>
            </div><hr>
            <div class="row">
                <div class="col-md-3">Fecha Pago</div>
                <div class="col-md-3">{{ $pago->created_at->format('d/m/Y H:m') }}</div>
            </div><hr>
            <div class="row">
                <div class="col-md-3">Fecha Procesamiento</div>
                <div class="col-md-3">{{ $pago->updated_at->format('d/m/Y H:m') }}</div>
            </div><hr>
            <div class="row">
                <div class="col-md-3">Nota</div>
                <div class="col-md-3">{{ $pago->nota }}</div>
            </div><hr>
            <div class="row">
                <div class="col-md-3">Observaciones</div>
                <div class="col-md-3">{{ $pago->observaciones }}</div>
            </div>
        </div>
    </div>

@endsection

@section('after-scripts')
<script>
    $("body").on("click", ".delete", function () {
        swal({
            title: "{{trans('buttons.general.crud.delete').' '.trans('labels.frontend.pago')}}",
            text: "¿Realmente desea eliminar este registro?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#ff8726',
            confirmButtonText: "Eliminar"
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '{{ url('/configuraciones/pago/'.$pago->id) }}',
                    data: {"_method" : 'DELETE'},
                    success: function (msg) {
                        swal("Eliminado!", "El registro ha sido correctamente eliminado.", "success").then((result) => { window.location.href = '{{url('/configuraciones/pago')}}'; });
                    }
                });
            }
        });
    });
</script>

@endsection
