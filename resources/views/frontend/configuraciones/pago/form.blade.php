<h6>Seleccionar Plan</h6>
  <section>
    <div class="row pricing-plan">
      @foreach($planes as $plan)
        <div class="col-md-3 col-xs-12 col-sm-6 no-padding">
          <div class="pricing-box {{(isset($user->empresa->plan) && $user->empresa->plan == $plan) ? 'featured-plan' : ''}}">
            <div class="pricing-body b-l b-r">
              <div class="pricing-header">
                @if(isset($user->empresa->plan) && $user->empresa->plan == $plan)
                  <h4 class="price-lable text-white bg-warning"> Actual</h4>
                @endif
                @if($plan->precio > 0)
                <h4 class="text-center">{{ $plan->nombre }}</h4>
                <h2 class="text-center"><span class="price-sign">$</span>{{ number_format($plan->precio,0,'','') }}</h2>
                <p class="uppercase text-center">mensuales</p>
                @else
                    <h2 class="text-center text-green">Gratis</h2>
                @endif
              </div>
              <div class="price-table-content">
                @if($plan->cantidad_usuarios > 1)
                  <div class="price-row"><i class="icon-people"></i> {{ $plan->cantidad_usuarios }} Usuarios</div>
                @else
                  <div class="price-row"><i class="icon-user"></i> {{ $plan->cantidad_usuarios }} Usuario</div>
                @endif
                <div class="price-row"><i class="icon-printer"></i> {{ $plan->cantidad_puntos_venta }} Puntos de Venta</div>
                <div class="price-row"><i class="icon-location-pin"></i> {{ $plan->cantidad_sucursales }} Sucursales</div>
                <div class="price-row"><i class="icon-speech"></i> {{ ($plan->chat) ? 'Soporte por Chat' : 'Sin Soporte por Chat' }}</div>
                @if($plan->precio > 0)
                <div class="price-row">
                  @if(isset($user->empresa->plan) && $user->empresa->plan == $plan)
                    <button type="button" onclick="seleccionarPlan({{$plan->id}})" class="btn btn-lg btn-info waves-effect waves-light m-t-20">Renovar</button>
                  @else
                    <button type="button" onclick="seleccionarPlan({{$plan->id}})" class="btn btn-success waves-effect waves-light m-t-20">Seleccionar</button>
                  @endif
                </div>
                @endif
              </div>
            </div>
          </div>
        </div>
      @endforeach
    </div>
  </section>
  <h6>Forma de Pago</h6>
  <section>
    <div class="form-group">
      <label class="control-label">Periodo</label>
      <div class="form-check">
        <label class="custom-control custom-radio">
          <input id="1mes" name="periodo" type="radio" value="Mensual" class="custom-control-input">
          <span class="custom-control-indicator"></span>
          <span class="custom-control-description">1 Mes</span>
        </label>
        <label class="custom-control custom-radio">
          <input id="1año" name="periodo" type="radio" value="Anual" checked class="custom-control-input">
          <span class="custom-control-indicator"></span>
          <span class="custom-control-description">1 Año <i style="color:green;">(1 Mes de Regalo!)</i></span>
        </label>
      </div>
    </div>
    <div class="form-group">
      <label class="control-label">Forma de Pago</label>
      <div class="form-check">
        @foreach($formasPago as $pago)
          <label class="custom-control custom-radio">
            <input id="radio{{$pago->id}}" name="forma_pago_id" checked="" value="{{$pago->id}}" type="radio" class="custom-control-input">
            <span class="custom-control-indicator"></span>
            <span class="custom-control-description">{{$pago}}</span>
          </label>
        @endforeach
      </div>
    </div>
  </section>
  <h6>Confirmar Pago</h6>
  <section>
    <div class="alert alert-info" id="info" style="display:none">
      <h3 class="text-info">
        <i class="fa fa-exclamation-circle"></i> Información
      </h3>
      <p id="info-contenido"></p>
    </div>
    <div class="form-group">
      <div class="card card-inverse card-megna">
        <div class="box bg-megna text-white">
          <h1></h1>
          <h1 class="font-light text-white">IMPORTE: <div class="pull-right" id="importe">$ 0.00</div></h1>
          <h1></h1>
        </div>
      </div>
    </div>
    <div class="form-group">
      {!! Form::label('nota', 'Nota Adjunta') !!}
      {!! Form::textarea('nota', null, ['class' => 'form-control', 'placeholder' => 'Aclaraciones correspondientes al pago']) !!}
      {!! $errors->first('nota', '<p class="text-danger">:message</p>') !!}
    </div>
  </section>
  {{ Form::hidden('plan_id') }}
