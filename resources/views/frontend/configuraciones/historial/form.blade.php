<div class="form-group {{ $errors->has('user_id') ? 'has-error' : ''}}">
    {!! Form::label('user_id', 'User Id', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('user_id', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('user_id', '<p class="text-danger">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('entidad_id') ? 'has-error' : ''}}">
    {!! Form::label('entidad_id', 'Entidad Id', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('entidad_id', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('entidad_id', '<p class="text-danger">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('tipo_historial') ? 'has-error' : ''}}">
    {!! Form::label('tipo_historial', 'Tipo Historial', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('tipo_historial', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('tipo_historial', '<p class="text-danger">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('texto') ? 'has-error' : ''}}">
    {!! Form::label('texto', 'Texto', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('texto', null, ['class' => 'form-control']) !!}
        {!! $errors->first('texto', '<p class="text-danger">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('url') ? 'has-error' : ''}}">
    {!! Form::label('url', 'Url', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('url', null, ['class' => 'form-control']) !!}
        {!! $errors->first('url', '<p class="text-danger">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {{ Form::submit(isset($submitButtonText) ? $submitButtonText : trans('buttons.general.save'), array('class' => 'btn btn-primary')) }}
    </div>
</div>
