@extends('frontend.layouts.app')

@section ('title', trans('labels.frontend.condicioniva')." - ".trans('buttons.general.crud.show'))

@section('content')
    <div class="main-content">
        <div class="main-content-inner">
            @include ('frontend.includes.breadcrumbs')

            <div class="page-header">
                <h1>
                    {{ trans('labels.frontend.condicioniva') }}
                    <small>
                        <i class="ace-icon fa fa-angle-double-right"></i>
                        <small>{{ trans('buttons.general.crud.show') }} CondicionIva</small>
                        <small><i class="ace-icon fa fa-angle-double-right"></i></small>
                        <small>{{ $condicioniva->id }}</small>
                    </small>

                    <div class="box-tools pull-right">
                        <a href="{{ url('/configuraciones/condicion-iva') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> {{ trans('buttons.general.crud.back') }}
                        </a>
                        <a href="{{ url('/configuraciones/condicion-iva/' . $condicioniva->id . '/edit') }}" title="{{ trans('buttons.general.crud.edit') }}" class="btn btn-primary">
                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i> {{ trans('buttons.general.crud.edit') }}
                        </a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['configuraciones/condicioniva', $condicioniva->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> '.trans("buttons.general.crud.delete"), array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger',
                                    'title' => trans("buttons.general.crud.delete")." ".trans("labels.frontend.condicioniva"),
                                    'onclick'=>'return confirm("Confirm delete?")'
                            ))!!}
                        {!! Form::close() !!}
                    </div>
                </h1>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-borderless">
                        <tbody>
                            <tr>
                                <th>ID</th><td>{{ $condicioniva->id }}</td>
                            </tr>
                            <tr><th> Nombre </th><td> {{ $condicioniva->nombre }} </td></tr><tr><th> Alicuota </th><td> {{ $condicioniva->alicuota }} </td></tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
