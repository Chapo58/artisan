<div class="form-group {{ $errors->has('nombre') ? 'has-error' : ''}}">
    {!! Form::label('nombre', 'Nombre', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('nombre', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('nombre', '<p class="text-danger">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('alicuota') ? 'has-error' : ''}}">
    {!! Form::label('alicuota', 'Alicuota', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('alicuota', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('alicuota', '<p class="text-danger">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {{ Form::submit(isset($submitButtonText) ? $submitButtonText : trans('buttons.general.save'), array('class' => 'btn btn-primary')) }}
    </div>
</div>
