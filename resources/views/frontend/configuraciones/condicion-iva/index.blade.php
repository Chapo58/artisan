@extends ('frontend.layouts.app')

@section ('title', trans('labels.frontend.condicioniva')." - ".trans('navs.frontend.user.administration'))

@section('after-styles')
    {{ Html::style("css/backend/plugin/datatables/dataTables.bootstrap.min.css") }}
@endsection

@section('content')
    <div class="main-content">
        <div class="main-content-inner">
            @include ('frontend.includes.breadcrumbs')
            <div class="page-header">
                <h1>
                    {{ trans('labels.frontend.condicioniva') }}
                    <small>
                        <i class="ace-icon fa fa-angle-double-right"></i>
                        <small>{{ trans('navs.frontend.user.administration') }}</small>
                    </small>

                    <div class="box-tools pull-right">
                        <a href="{{ url('/configuraciones/condicion-iva/create') }}" class="btn btn-themecolor btn-lg" title="{{ trans('buttons.general.crud.create') }} CondicionIva">
                            <i class="fa fa-plus" aria-hidden="true"></i> {{ trans('buttons.general.crud.create') }} {{ trans('labels.frontend.condicioniva') }}
                        </a>
                    </div><!--box-tools pull-right-->
                </h1>
            </div>

            @tips($_SERVER['REQUEST_URI'])

            @include('includes.partials.messages')

            {!! Form::open(['method' => 'GET', 'url' => '/configuraciones/condicion-iva', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
            <div class="input-group">
                <input type="text" class="form-control" name="search" placeholder="{{ trans('strings.backend.general.search_placeholder') }}">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="submit">
                        <i class="fa fa-search"></i>
                    </button>
                </span>
            </div>
            {!! Form::close() !!}

            <div class="row">
                <div class="col-xs-12">
                    <div class="table-responsive">
                        <table id="condicioniva-table" class="table table-condensed table-hover">
                            <thead>
                                <tr>
                                    <th>ID</th><th>Nombre</th><th>Alicuota</th><th>{{ trans('labels.general.actions') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($condicioniva as $item)
                                <tr>
                                    <td>{{ $item->id }}</td>
                                    <td>{{ $item->nombre }}</td><td>{{ $item->alicuota }}</td>
                                    <td>
                                        <a href="{{ url('/configuraciones/condicion-iva/' . $item->id) }}" title="{{ trans('buttons.general.crud.show') }} {{ trans('labels.frontend.condicioniva') }}">
                                            <button class="btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i> {{ trans('buttons.general.crud.show') }}</button>
                                        </a>
                                        <a href="{{ url('/configuraciones/condicion-iva/' . $item->id . '/edit') }}" title="{{ trans('buttons.general.crud.edit') }} {{ trans('labels.frontend.condicioniva') }}">
                                            <button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> {{ trans('buttons.general.crud.edit') }}</button>
                                        </a>
                                        {!! Form::open([
                                            'method'=>'DELETE',
                                            'url' => ['/configuraciones/condicion-iva', $item->id],
                                            'style' => 'display:inline'
                                        ]) !!}
                                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> '.trans("buttons.general.crud.delete"), array(
                                                    'type' => 'submit',
                                                    'class' => 'btn btn-danger btn-xs',
                                                    'title' => trans("buttons.general.crud.delete")." ".trans("labels.frontend.condicioniva"),
                                                    'onclick'=>'return confirm("Confirm delete?")'
                                            )) !!}
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="pagination-wrapper"> {!! $condicioniva->appends(['search' => Request::get('search')])->render() !!} </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('after-scripts')
{{ Html::script("js/backend/plugin/datatables/jquery.dataTables.min.js") }}
{{ Html::script("js/backend/plugin/datatables/dataTables.bootstrap.min.js") }}

<script>
    $(document).ready(function() {
        $('#condicioniva-table').DataTable( {
            "paging":   false,
            "info":     false,
            "searching":   false
        });
    } );
</script>

@endsection