@extends('frontend.layouts.app')

@section ('title', trans('labels.frontend.tarjetapropia_titulo')." - ".trans('buttons.general.crud.show'))

@section('content')
    <div class="row page-titles">
        <div class="col-6 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('labels.frontend.tarjetapropia_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item"><a href="{{ url('/configuraciones/tarjeta-propia') }}">{{ trans('labels.frontend.tarjetapropia_titulo') }}</a></li>
                <li class="breadcrumb-item">{{ trans('buttons.general.crud.show') }} {{ trans('labels.frontend.tarjetapropia') }}</li>
                <li class="breadcrumb-item active">{{ $tarjetaPropia->nombre }}</li>
            </ol>
        </div>
        <div class="col-6">
            <div class="pull-right">
                <a href="{{ url('/configuraciones/tarjeta-propia')  }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans('buttons.general.crud.back') }}</span>
                </a>
                <a href="{{ url('/configuraciones/tarjeta-propia/' . $tarjetaPropia->id . '/edit') }}" title="{{ trans('buttons.general.crud.edit') }}" class="btn btn-info">
                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans('buttons.general.crud.edit') }}</span>
                </a>
                <a href="javascript:void(0)" title="{{trans("buttons.general.crud.delete")." ".trans("labels.frontend.tarjetapropia")}}" class="btn btn-danger delete">
                    <i class="fa fa-trash" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans("buttons.general.crud.delete") }}</span>
                </a>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-body">
            <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
            <h4 class="font-medium m-t-30">{{ $tarjetaPropia }}</h4>
            <hr>
            <div class="row">
                <div class="col-md-2 font-weight-bold">Nombre</div>
                <div class="col-md-4">{{ $tarjetaPropia->nombre }}</div>

                <div class="col-md-2 font-weight-bold">Banco</div>
                <div class="col-md-4">{{ $tarjetaPropia->banco }}</div>
            </div><hr>
            <div class="row">
                <div class="col-md-2 font-weight-bold">Tipo</div>
                <div class="col-md-4">{{ $tiposTarjetas[$tarjetaPropia->tipo] }}</div>

                <div class="col-md-2 font-weight-bold">Titular</div>
                <div class="col-md-4">{{ $tarjetaPropia->titular }}</div>
            </div><hr>
            <div class="row">
                <div class="col-md-2 font-weight-bold">Vencimiento</div>
                <div class="col-md-4">{{ ($tarjetaPropia->fecha_vencimiento) ? $tarjetaPropia->fecha_vencimiento->format('m/Y') : '' }}</div>

                <div class="col-md-2 font-weight-bold">Numero</div>
                <div class="col-md-4">{{ $tarjetaPropia->numero }}</div>
            </div><hr>
            <div class="row">
                <div class="col-md-3 font-weight-bold">Limite</div>
                <div class="col-md-9">{{ $tarjetaPropia->limite }}</div>
            </div><hr>
            <div class="row">
                <div class="col-md-3 font-weight-bold">Creado el</div>
                <div class="col-md-9">{{ $tarjetaPropia->created_at->format('d/m/Y H:m') }}</div>
            </div><hr>
            <div class="row">
                <div class="col-md-3 font-weight-bold">Ultima Modificación</div>
                <div class="col-md-9">{{ $tarjetaPropia->updated_at->format('d/m/Y H:m') }}</div>
            </div>
        </div>
    </div>

@endsection
@section('after-scripts')
    <script>
        $("body").on("click", ".delete", function () {
            swal({
                title: "{{trans('buttons.general.crud.delete').' '.trans('labels.frontend.tarjetapropia')}}",
                text: "¿Realmente desea eliminar este registro?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#ff8726',
                confirmButtonText: "Eliminar"
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        type: 'POST',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: '{{ url('configuraciones/tarjeta-propia/'.$tarjetaPropia->id) }}',
                        data: {"_method" : 'DELETE'},
                        success: function (msg) {
                            swal("Eliminado!", "El registro ha sido correctamente eliminado.", "success").then((result) => { window.location.href = '{{url('/configuraciones/tarjeta-propia')}}'; });
                        }
                    });
                }
            });
        });
    </script>
@endsection