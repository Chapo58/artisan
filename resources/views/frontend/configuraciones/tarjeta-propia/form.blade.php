<div class="card" >
    <div class="card-header">
        <div class="card-actions">
            <a class="mytooltip" data-action="collapse"><i class="ti-minus"></i><span class="tooltip-content3">Minimizar Ventana</span></a>
            <a class="btn-minimize mytooltip" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
        </div>
        <h4 class="card-title m-b-0">
            Principal
        </h4>
    </div>
    <div class="card-body collapse show b-t">
        <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('nombre', 'Nombre') !!}
                    {!! Form::text('nombre', null, ['id' => 'codigo', 'class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('nombre', '<p class="text-danger">:message</p>') !!}
                </div>
                <div class="col-md-4">
                    {!! Form::label('banco_id', 'Banco') !!}
                    {!! Form::select('banco_id', $bancos, isset($tarjetaPropia->banco) ? $tarjetaPropia->banco->id : '', array('class' => 'form-control')) !!}
                    {!! $errors->first('banco_id', '<p class="text-danger">:message</p>') !!}
                </div>
                <div class="col-md-4">
                    {!! Form::label('tipo', 'Tipo') !!}
                    {!! Form::select('tipo', $tiposTarjetas, isset($tarjetaPropia->tipo) ? $tarjetaPropia->tipo : 0, array('class' => 'form-control')) !!}
                    {!! $errors->first('tipo', '<p class="text-danger">:message</p>') !!}
                </div>

        </div>
    </div>
</div>

<div class="card" >
    <div class="card-header">
        <div class="card-actions">
            <a class="mytooltip" data-action="collapse"><i class="ti-minus"></i><span class="tooltip-content3">Minimizar Ventana</span></a>
            <a class="btn-minimize mytooltip" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
        </div>
        <h4 class="card-title m-b-0">
            Detalles
        </h4>
    </div>
    <div class="card-body collapse show b-t">
        <div class="row form-group">
                <div class="col-md-3">
                    {!! Form::label('titular', 'Titular') !!}
                    {!! Form::text('titular', null, ['id' => 'codigo', 'class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('titular', '<p class="text-danger">:message</p>') !!}
                </div>
                <div class="col-md-3">
                    {!! Form::label('fecha_vencimiento', 'Vencimiento') !!}
                    <input class="form-control" value="{{isset($tarjetaPropia) ? $tarjetaPropia->fecha_vencimiento->format('Y-m') : ''}}" name="fecha_vencimiento" type="month" id="fecha_vencimiento">
                    {!! $errors->first('fecha_vencimiento', '<p class="text-danger">:message</p>') !!}
                </div>
                <div class="col-md-3">
                    {!! Form::label('numero', 'Numero') !!}
                    {!! Form::text('numero', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('numero', '<p class="text-danger">:message</p>') !!}
                </div>
                <div class="col-md-3">
                    {!! Form::label('limite', 'Limite') !!}
                    {!! Form::number('limite', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('limite', '<p class="text-danger">:message</p>') !!}
                </div>
        </div>
    </div>
</div>

<div class="card">
    <div class="card-body">
        <div class="form-group">
            {{ Form::submit(isset($submitButtonText) ? $submitButtonText : trans('buttons.general.save'), ['class' => 'btn btn-themecolor btn-lg pull-right']) }}
        </div>
    </div>
</div>

