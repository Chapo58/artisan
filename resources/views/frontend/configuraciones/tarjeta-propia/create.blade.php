@extends('frontend.layouts.app')

@section ('title', trans('labels.frontend.tarjetapropia_titulo')." - ".trans('buttons.general.crud.create'))

@section('content')
    <div class="row page-titles">
        <div class="col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('labels.frontend.tarjetapropia_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item"><a href="{{url('/configuraciones/tarjeta-propia')}}">{{ trans('labels.frontend.tarjetapropia_titulo') }}</a></li>
                <li class="breadcrumb-item active">{{ trans('buttons.general.crud.create') }} {{ trans('labels.frontend.tarjetapropia') }}</li>
            </ol>
        </div>
        <div class="col-4">
            <div class="pull-right">
                <a href="{{ url('/configuraciones/tarjeta-propia') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans('buttons.general.crud.back') }}</span>
                </a>
            </div>
        </div>
    </div>

    @tips($_SERVER['REQUEST_URI'])

    @include('includes.partials.messages')

    {!! Form::open(['url' => '/configuraciones/tarjeta-propia', 'class' => 'form-horizontal', 'files' => true]) !!}
        @include ('frontend.configuraciones.tarjeta-propia.form')
    {!! Form::close() !!}

@endsection
