@include('includes.partials.messages')
<div class="table-responsive">
    <table id="logs-table" class="table table-striped table-hover table-sm">
        <thead>
        <tr>
            <th>Fecha</th>
            <th>Tiempo</th>
            <th>Tipo</th>
            <th>Registro</th>
        </tr>
        </thead>
        <tbody>
        @foreach($logs as $item)
            <tr>
                <td>{{ $item->created_at->format('d/m/Y H:i') }}</td>
                <td>{{ $item->created_at->diffForHumans() }}</td>
                <td>
                    @if($item->tipo_historial == 0)
                        <b class="text-success">{{ $tiposLog[$item->tipo_historial] }}</b>
                    @elseif($item->tipo_historial == 1)
                        <b class="text-info">{{ $tiposLog[$item->tipo_historial] }}</b>
                    @else
                        <b class="text-danger">{{ $tiposLog[$item->tipo_historial] }}</b>
                    @endif
                </td>
                <td>
                    @if($item->url != null)
                        Registro: <a href="{{ url($item->url) }}" title="Ver">{{ $item->texto }}</a>
                    @else
                        Registro: {{ $item->texto }}
                    @endif
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>