@extends('frontend.layouts.app')

@section ('title', trans('labels.frontend.usuario_titulo')." - ".trans('buttons.general.crud.show'))

@section('content')
    <div class="row page-titles">
        <div class="col-6 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('labels.frontend.usuario_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item"><a href="{{url('/configuraciones/usuario')}}">{{ trans('labels.frontend.usuario_titulo') }}</a></li>
                <li class="breadcrumb-item">{{ trans('buttons.general.crud.show') }} {{ trans('labels.frontend.usuario') }}</li>
                <li class="breadcrumb-item active">{{ $usuario }}</li>
            </ol>
        </div>
        <div class="col-6">
            <div class="pull-right">
                <a href="{{ url('/configuraciones/usuario') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans('buttons.general.crud.back') }}</span>
                </a>
                @role('Usuario Admin')
                    <a href="javascript:void(0)" title="{{trans("buttons.general.crud.delete")." ".trans("labels.frontend.usuario")}}" class="btn btn-danger delete">
                        <i class="fa fa-trash" aria-hidden="true"></i>
                        <span class="hidden-xs-down">{{ trans("buttons.general.crud.delete") }}</span>
                    </a>
                @endauth
            </div>
        </div>
    </div>

     <!-- Row -->
<div class="row">
    <!-- Column -->
    <div class="col-lg-3 col-xlg-2 col-md-4">
        <div class="card"> <img class="card-img" src="{{ url('images/frontend/background/socialbg.jpg') }}" alt="Fondo">
            <div class="card-img-overlay card-inverse social-profile d-flex ">
                <div class="align-self-center"> <img src="{{ ($usuario->avatar) ? $usuario->avatar : url('usuarios/no-image.png') }}" class="img-circle" width="100">
                    <h4 class="card-title">{{ $usuario->name }}</h4>
                    <h6 class="card-subtitle">{{ $usuario->rol }}</h6>
                    <p class="text-white">Usuario desde el {{ $usuario->created_at->format('d/m/Y') }} ({{ $usuario->created_at->diffForHumans() }}) </p>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body"> 
                <small class="text-muted">Email </small>
                <h6>{{ $usuario->email }}</h6> 
                <small class="text-muted p-t-30 db">Teléfono</small>
                <h6>{{isset($usuario->empleado->telefono) ? $usuario->empleado->telefono : ''}}</h6>
                <small class="text-muted p-t-30 db">Dirección</small>
                <h6>{{isset($usuario->empleado->domicilio) ? $usuario->empleado->domicilio : ''}}</h6>
                {{--<div class="map-box">--}}
                    {{--<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d470029.1604841957!2d72.29955005258641!3d23.019996818380896!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x395e848aba5bd449%3A0x4fcedd11614f6516!2sAhmedabad%2C+Gujarat!5e0!3m2!1sen!2sin!4v1493204785508" width="100%" height="150" frameborder="0" style="border:0" allowfullscreen></iframe>--}}
                {{--</div> <small class="text-muted p-t-30 db">Redes Sociales</small>--}}
                {{--<br/>--}}
                {{--<button class="btn btn-circle btn-secondary"><i class="fa fa-facebook"></i></button>--}}
                {{--<button class="btn btn-circle btn-secondary"><i class="fa fa-twitter"></i></button>--}}
                {{--<button class="btn btn-circle btn-secondary"><i class="fa fa-instagram"></i></button>--}}
            </div>
        </div>
    </div>
    <!-- Column -->
    <!-- Column -->
    <div class="col-lg-9 col-xlg-10 col-md-8">
        <div class="card">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs profile-tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#perfil" role="tab">
                        <span class="hidden-sm-up"><i class="fa fa-user"></i></span>
                        <span class="hidden-xs-down">{{ trans('navs.frontend.user.profile') }}</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#actualizar" role="tab">
                        <span class="hidden-sm-up"><i class="fa fa-info-circle"></i></span>
                        <span class="hidden-xs-down">{{ trans('labels.frontend.user.profile.update_information') }}</span>
                    </a>
                </li>
                @role('Usuario Admin')
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#logs" role="tab">
                            <span class="hidden-sm-up"><i class="fa fa-history"></i></span>
                            <span class="hidden-xs-down">Logs</span>
                        </a>
                    </li>
                @endauth
                @if ($usuario->canChangePassword())
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#passw" role="tab">
                            <span class="hidden-sm-up"><i class="fa fa-lock"></i></span>
                            <span class="hidden-xs-down">{{ trans('navs.frontend.user.change_password') }}</span>
                        </a>
                    </li>
                @endif
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
                <div class="tab-pane active" id="perfil" role="tabpanel">
                    <div class="card-body">
                        @include('frontend.user.account.tabs.profile')
                    </div>
                </div>
                <div class="tab-pane" id="actualizar" role="tabpanel">
                    <div class="card-body">
                        @include('frontend.user.account.tabs.edit')
                    </div>
                </div>
                @role('Usuario Admin')
                    <div class="tab-pane" id="logs" role="tabpanel">
                        <div class="card-body">
                            @include('frontend.configuraciones.usuario.show_logs')
                        </div>
                    </div>
                @endauth
                @if ($usuario->canChangePassword())
                    <div class="tab-pane" id="passw" role="tabpanel">
                        <div class="card-body">
                            @include('frontend.user.account.tabs.change-password')
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
    <!-- Column -->
</div>
<!-- Row -->

@endsection

@section('after-scripts')
    {{ Html::script("js/frontend/plugins/datatables/jquery.dataTables.min.js") }}
    {{ Html::script("js/frontend/plugins/maskedinput/jquery.maskedinput.min.js") }}

    <script>
        $(document).ready(function() {
            $('input[name="empleado[cuil]"]').mask("99-99999999-9",{placeholder:"##-########-#"});

            $('#logs-table').DataTable({"aaSorting": []});
        } );

        $("body").on("click", ".delete", function () {
            swal({
                title: "{{trans('buttons.general.crud.delete').' '.trans('labels.frontend.usuario')}}",
                text: "¿Realmente desea eliminar este registro?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#ff8726',
                confirmButtonText: "Eliminar"
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        type: 'POST',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: '{{ url('/configuraciones/usuario/'.$usuario->id) }}',
                        data: {"_method" : 'DELETE'},
                        success: function (msg) {
                            swal("Eliminado!", "El registro ha sido correctamente eliminado.", "success").then((result) => { window.location.href = '{{url('/configuraciones/usuario')}}'; });
                        }
                    });
                }
            });
        });
    </script>
@endsection