<div class="card">
    <div class="card-header">
        <div class="card-actions">
            <a class="" data-action="collapse"><i class="ti-minus"></i></a>
            <a class="btn-minimize" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
        </div>
        <h4 class="card-title m-b-0">Información del Usuario</h4>
    </div>
    <div class="card-body collapse show b-t">
        <div class="row form-group">
            <div class="col-md-6">
                {!! Form::label('email', trans('validation.attributes.backend.access.users.email')) !!}
                {{ Form::email('email', null, ['class' => 'form-control', 'required' => 'required']) }}
                {!! $errors->first('email', '<p class="text-danger">:message</p>') !!}
            </div>
            <div class="col-md-6">
                {!! Form::label('name', trans('validation.attributes.backend.access.users.name').' usuario') !!}
                {{ Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) }}
                {!! $errors->first('name', '<p class="text-danger">:message</p>') !!}
            </div>
        </div>
        @if(isset($usuario))
            <div class="row form-group">
                <div class="col-md-12">
                    {!! Form::label('status', 'Activo') !!}
                    <div class="switch">
                        <label>DESACTIVADO
                            {!! Form::checkbox('status', true, ($usuario->status == 1) ? true : false) !!}<span class="lever"></span>
                            ACTIVADO</label>
                    </div>
                    {!! $errors->first('status', '<p class="text-danger">:message</p>') !!}
                </div>
            </div>
        @else
            <div class="row form-group">
                <div class="col-md-12">
                    {!! Form::label('rol_id', 'Rol') !!}
                    {{ Form::select('rol_id', $roles, 0, ['class' => 'form-control']) }}
                    {!! $errors->first('rol_id', '<p class="text-danger">:message</p>') !!}
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-6">
                    {!! Form::label('password', trans('validation.attributes.backend.access.users.password')) !!}
                    {{ Form::input('password', 'password', null, ['class' => 'form-control', 'required' => 'required']) }}
                    {!! $errors->first('password', '<p class="text-danger">:message</p>') !!}
                </div>
                <div class="col-md-6">
                    {!! Form::label('password_confirmation', trans('validation.attributes.backend.access.users.password_confirmation')) !!}
                    {{ Form::input('password', 'password_confirmation', null, ['class' => 'form-control', 'required' => 'required']) }}
                    {!! $errors->first('password_confirmation', '<p class="text-danger">:message</p>') !!}
                </div>
            </div>
        @endif
        <div class="row form-group">
            <div class="col-md-12">
                {!! Form::label('sucursal_id', 'Sucursal') !!}
                {{ Form::select('sucursal_id', $sucursales, isset($usuario->sucursal) ? $usuario->sucursal_id : 0, ['class' => 'form-control', 'required' => 'required']) }}
                {!! $errors->first('sucursal_id', '<p class="text-danger">:message</p>') !!}
            </div>
        </div>
    </div>
</div>

<div class="card">
    <div class="card-header">
        <div class="card-actions">
            <a class="" data-action="collapse"><i class="ti-minus"></i></a>
            <a class="btn-minimize" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
        </div>
        <h4 class="card-title m-b-0">Información del Empleado</h4>
    </div>
    <div class="card-body collapse show b-t">
        <div class="row form-group">
            <div class="col-md-6">
                {!! Form::label('empleado[nombre]', 'Nombre Empleado') !!}
                {!! Form::text('empleado[nombre]', null, ['class' => 'form-control', 'required' => 'required']) !!}
                {!! $errors->first('empleado[nombre]', '<p class="text-danger">:message</p>') !!}
            </div>
            <div class="col-md-6">
                {!! Form::label('empleado[cuil]', 'Cuil') !!}
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-hashtag"></i>
                    </div>
                    {!! Form::text('empleado[cuil]', null, ['class' => 'form-control', 'required' => 'required']) !!}
                </div>
                {!! $errors->first('empleado[cuil]', '<p class="text-danger">:message</p>') !!}
            </div>
        </div>
        <div class="row form-group">
            <div class="col-md-6">
                {!! Form::label('empleado[caja_id]', 'Caja por defecto') !!}
                {!! Form::select('empleado[caja_id]', $cajas, isset($usuario->empleado) ? $usuario->empleado->caja_id : '', ['class' => 'form-control']) !!}
                {!! $errors->first('empleado[caja_id]', '<p class="text-danger">:message</p>') !!}
            </div>
            <div class="col-md-6">
                {!! Form::label('empleado[punto_venta_id]', 'Punto de Venta por defecto') !!}
                {!! Form::select('empleado[punto_venta_id]', $puntos_venta, isset($usuario->empleado) ? $usuario->empleado->punto_venta_id : '', ['class' => 'form-control']) !!}
                {!! $errors->first('empleado[punto_venta_id]', '<p class="text-danger">:message</p>') !!}
            </div>
        </div>
        <div class="row form-group">
            <div class="col-md-6">
                {!! Form::label('direccion', 'Dirección') !!}
                {!! Form::text('domicilio[direccion]', isset($usuario->empleado->domicilio->direccion) ? $usuario->empleado->domicilio->direccion : '', ['class' => 'form-control', 'required' => 'required','id' => 'direccion']) !!}
                {!! $errors->first('direccion', '<p class="text-danger">:message</p>') !!}
            </div>
            <div class="col-md-6">
                {!! Form::label('localidad', 'Localidad') !!}
                {!! Form::text('domicilio[localidad]', isset($usuario->empleado->domicilio->localidad) ? $usuario->empleado->domicilio->localidad : '', ['class' => 'form-control', 'required' => 'required','id' => 'localidad']) !!}
                {!! $errors->first('localidad', '<p class="text-danger">:message</p>') !!}
            </div>
        </div>
        <div class="row form-group">
            <div class="col-md-6">
                {!! Form::label('codigo_postal', 'Código Postal') !!}
                {!! Form::text('domicilio[codigo_postal]', isset($usuario->empleado->domicilio->codigo_postal) ? $usuario->empleado->domicilio->codigo_postal : '', ['class' => 'form-control','id' => 'codigo_postal']) !!}
                {!! $errors->first('codigo_postal', '<p class="text-danger">:message</p>') !!}
            </div>
            <div class="col-md-6">
                {!! Form::label('provincia_id', 'Provincia') !!}
                {!! Form::select('domicilio[provincia_id]', $provincias, isset($usuario->empleado->domicilio->provincia) ? $usuario->empleado->domicilio->provincia->id : '', ['id' => 'provincia_id', 'class' => 'form-control', 'required' => 'required']) !!}
                {!! $errors->first('provincia_id', '<p class="text-danger">:message</p>') !!}
            </div>
        </div>
        <div class="row form-group">
            <div class="col-md-6">
                {!! Form::label('telefono', 'Teléfono') !!}
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa  fa-phone"></i>
                    </div>
                    {!! Form::text('empleado[telefono]', isset($usuario->empleado->telefono) ? $usuario->empleado->telefono : '', ['class' => 'form-control','id' => 'telefono']) !!}
                </div>
                {!! $errors->first('telefono', '<p class="text-danger">:message</p>') !!}
            </div>
        </div>
    </div>
</div>

<div class="card">
    <div class="card-body">
        <div class="form-group">
            {{ Form::submit(isset($submitButtonText) ? $submitButtonText : trans('buttons.general.save'), ['class' => 'btn btn-themecolor btn-lg pull-right']) }}
        </div>
    </div>
</div>

@include('backend.configuraciones.provincia.provincia_dialog')
@include('backend.configuraciones.localidad.localidad_dialog')
