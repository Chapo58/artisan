@extends('frontend.layouts.app')

@section ('title', trans('labels.frontend.tarjeta_titulo')." - ".trans('buttons.general.crud.show'))

@section('content')
    <div class="row page-titles">
        <div class="col-6 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('labels.frontend.tarjeta_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item"><a href="{{url('/configuraciones/tarjeta')}}">{{ trans('labels.frontend.tarjeta_titulo') }}</a></li>
                <li class="breadcrumb-item">{{ trans('buttons.general.crud.show') }} {{ trans('labels.frontend.tarjeta') }}</li>
                <li class="breadcrumb-item active">{{ $tarjeta }}</li>
            </ol>
        </div>
        <div class="col-6">
            <div class="pull-right">
                <a href="{{ url('/configuraciones/tarjeta') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans('buttons.general.crud.back') }}</span>
                </a>
                <a href="{{ url('/configuraciones/tarjeta/' . $tarjeta->id . '/edit') }}" title="{{ trans('buttons.general.crud.edit') }}" class="btn btn-info">
                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans('buttons.general.crud.edit') }}</span>
                </a>
                <a href="javascript:void(0)" title="{{trans("buttons.general.crud.delete")." ".trans("labels.frontend.tarjeta")}}" class="btn btn-danger delete">
                    <i class="fa fa-trash" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans("buttons.general.crud.delete") }}</span>
                </a>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
            <ul class="nav nav-tabs profile-tab" role="tablist">
                <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#info" role="tab">Información</a> </li>
                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#planes" role="tab">Planes de Tarjeta</a> </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="info" role="tabpanel">
                    <h4 class="font-medium m-t-30">{{ $tarjeta }}</h4>
                    <hr>
                    <div class="row">
                        <div class="col-md-3 font-weight-bold">Numero</div>
                        <div class="col-md-9">{{ $tarjeta->numero }}</div>
                    </div><hr>
                    <div class="row">
                        <div class="col-md-3 font-weight-bold">Nombre</div>
                        <div class="col-md-9">{{ $tarjeta->nombre }}</div>
                    </div><hr>
                    <div class="row">
                        <div class="col-md-3 font-weight-bold">Comercio</div>
                        <div class="col-md-9">{{ $tarjeta->comercio }}</div>
                    </div><hr>
                    <div class="row">
                        <div class="col-md-3 font-weight-bold">Creada el</div>
                        <div class="col-md-9">{{ $tarjeta->created_at->format('d/m/Y H:m') }}</div>
                    </div><hr>
                    <div class="row">
                        <div class="col-md-3 font-weight-bold">Ultima Modificación</div>
                        <div class="col-md-9">{{ $tarjeta->updated_at->format('d/m/Y H:m') }}</div>
                    </div>
                </div>
                <div class="tab-pane" id="planes" role="tabpanel">
                    <table class="table color-bordered-table muted-bordered-table table-hover table-striped">
                        <thead class="thin-border-bottom">
                        <tr>
                            <th>Nombre</th>
                            <th>Interes</th>
                            <th>Cuotas</th>
                            <th>{{ trans('labels.general.actions') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($planesTarjeta as $item)
                            <tr id="plan-{{$item->id}}">
                                <td>{{ $item->nombre }}</td>
                                <td>{{ $item->interes }} %</td>
                                <td>{{ $item->cuotas }}</td>
                                <td>
                                    <a href="{{ url('/configuraciones/plan-tarjeta/' . $item->id) }}" class="mytooltip">
                                        <i class="fa fa-eye text-success m-r-10"></i><span class="tooltip-content3">Ver Plan Tarjeta</span>
                                    </a>
                                    <a href="{{ url('/configuraciones/plan-tarjeta/' . $item->id . '/edit') }}" class="mytooltip">
                                        <i class="fa fa-pencil text-info m-r-10"></i><span class="tooltip-content3">Modificar Plan Tarjeta</span>
                                    </a>
                                    <a class="mytooltip delete-plan pointer" data-id="{{$item->id}}">
                                        <i class="fa fa-close text-danger"></i><span class="tooltip-content3">Eliminar Plan Tarjeta</span>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('after-scripts')
<script>
    $("body").on("click", ".delete", function () {
        swal({
            title: "{{trans('buttons.general.crud.delete').' '.trans('labels.frontend.tarjeta')}}",
            text: "¿Realmente desea eliminar este registro?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#ff8726',
            confirmButtonText: "Eliminar"
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '{{ url('/configuraciones/tarjeta/'.$tarjeta->id) }}',
                    data: {"_method" : 'DELETE'},
                    success: function (msg) {
                        swal("Eliminado!", "El registro ha sido correctamente eliminado.", "success").then((result) => { window.location.href = '{{url('/configuraciones/tarjeta')}}'; });
                    }
                });
            }
        });
    });

    $("body").on("click", ".delete-plan", function () {
        var id = $(this).attr("data-id");
        swal({
            title: "Eliminar Plan Tarjeta",
            text: "¿Realmente desea eliminar este registro?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#ff8726',
            confirmButtonText: "Eliminar"
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/configuraciones/plan-tarjeta/'+id,
                    data: {"_method" : 'DELETE'},
                    success: function (msg) {
                        $("#plan-" + id).hide(1);
                        swal("Eliminado!", "El registro ha sido correctamente eliminado.", "success");
                    }
                });
            }
        });
    });
</script>

@endsection
