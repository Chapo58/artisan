<div class="card">
  <div class="card-body">
    <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
    <br>
    <div class="row form-group">
      <div class="col-md-2">
        {!! Form::label('numero', 'Numero') !!}
        <div class="input-group">
            <div class="input-group-addon">
                <i class="fa fa-hashtag"></i>
            </div>
            {!! Form::number('numero', null, ['class' => 'form-control', 'step' => '.01', 'required' => 'required']) !!}
        </div>
        {!! $errors->first('numero', '<p class="text-danger">:message</p>') !!}
      </div>
      <div class="col-md-5">
          {!! Form::label('nombre', 'Nombre') !!}
          {!! Form::text('nombre', null, ['class' => 'form-control', 'required' => 'required']) !!}
          {!! $errors->first('nombre', '<p class="text-danger">:message</p>') !!}
      </div>
      <div class="col-md-5">
          {!! Form::label('comercio', 'Comercio') !!}
          {!! Form::text('comercio', null, ['class' => 'form-control']) !!}
          {!! $errors->first('comercio', '<p class="text-danger">:message</p>') !!}
      </div>
    </div>
    <hr>
    <div class="form-actions">
      <div class="row">
        <div class="col-md-12">
          {{ Form::submit(isset($submitButtonText) ? $submitButtonText : trans('buttons.general.save'), ['class' => 'btn btn-lg btn-themecolor pull-right']) }}
        </div>
      </div>
    </div>
  </div>
</div>
