@extends ('frontend.layouts.app')

@section ('title', "Soporte - Actualizaciones")

@section('content')
    <div class="row page-titles">
        <div class="col-md-12 col-12 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">Actualizaciones</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item active">Actualizaciones</li>
            </ol>
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
            <div class="row">
                <div class="col-12">
                    <h4> 15/05/2019 - Versión: 2.0.3</h4>
                    <table>
                        <tr>
                            <td><span class="label label-success"> Nuevo </span></td>
                            <td>Descripción de detalles de ventas y presupuestos modificables.</td>
                        </tr>
                        <tr>
                            <td><span class="label label-success"> Nuevo </span></td>
                            <td>Al momento de crear o modificar artículos, ahora puede ingresarse manualmente el precio final de cada lista de precios.</td>
                        </tr>
                        <tr>
                            <td><span class="label label-success"> Nuevo </span></td>
                            <td>Validación de codigos internos de articulos unicos.</td>
                        </tr>
                        <tr>
                            <td><span class="label label-danger"> Fix </span></td>
                            <td>Listados de recibos de algunos clientes.</td>
                        </tr>
                    </table>
                    <hr>
                </div>
                <div class="col-12">
                    <h4> 12/03/2019 - Versión: 2.0.2</h4>
                    <table>
                        <tr>
                            <td><span class="label label-success"> Nuevo </span></td>
                            <td>Posibilidad de modificar los precios finales de articulos en comprobantes.</td>
                        </tr>
                        <tr>
                            <td><span class="label label-success"> Nuevo </span></td>
                            <td>Selección de formato de precios en impresion de listas de precios.</td>
                        </tr>
                        <tr>
                            <td><span class="label label-success"> Nuevo </span></td>
                            <td>Porcentajes incrementales en listas de precios.</td>
                        </tr>
                        <tr>
                            <td><span class="label label-success"> Nuevo </span></td>
                            <td>Color en los textos de los sliders del E-Commerce.</td>
                        </tr>
                        <tr>
                            <td><span class="label label-success"> Nuevo </span></td>
                            <td>Cobro con Mercado Pago en comprobantes.</td>
                        </tr>
                        <tr>
                            <td><span class="label label-warning"> Actualización </span></td>
                            <td>Los titulos ya no son obligatorios en los sliders del E-Commerce.</td>
                        </tr>
                        <tr>
                            <td><span class="label label-danger"> Fix </span></td>
                            <td>Alicuotas Ventas (Regimen de Compras y Ventas).</td>
                        </tr>
                        <tr>
                            <td><span class="label label-danger"> Fix </span></td>
                            <td>Altas de nuevos clientes en el E-Commerce.</td>
                        </tr>
                        <tr>
                            <td><span class="label label-danger"> Fix </span></td>
                            <td>Total Presupuestos.</td>
                        </tr>
                    </table>
                    <hr>
                </div>
                <div class="col-12">
                    <h4> 06/02/2019 - Versión: 2.0.1</h4>
                    <table>
                        <tr>
                            <td><span class="label label-success"> Nuevo </span></td>
                            <td>Accesos directos al sistema.</td>
                        </tr>
                        <tr>
                            <td><span class="label label-success"> Nuevo </span></td>
                            <td>Condición de iva de clientes al ser creados desde comprobantes.</td>
                        </tr>
                        <tr>
                            <td><span class="label label-warning"> Actualización </span></td>
                            <td>Terminos de Uso y Politicas de Privacidad del sistema.</td>
                        </tr>
                        <tr>
                            <td><span class="label label-danger"> Fix </span></td>
                            <td>Parametros del E-Commerce.</td>
                        </tr>
                        <tr>
                            <td><span class="label label-danger"> Fix </span></td>
                            <td>Ver de Articulos.</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection