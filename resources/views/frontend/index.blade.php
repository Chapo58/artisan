<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
	<head>
		<meta charset="utf-8">
		<title>Artisan - Sistema de Gestión Integral para Empresas</title>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="keywords" content="sistema, gestion, artisan, administracion, empresas, comercios, ventas, caja, stock">
		<meta name="description" content="Gestione los Recursos de su empresa y tome decisiones apoyadas en Información de tiempo real con el sistema Gestión Integral de Empresa.">

		<!-- Favicon -->
		<link rel="shortcut icon" href="{{{ asset('favicon.png') }}}">
		<!-- For-Mobile-Apps-and-Meta-Tags -->
		<meta name="mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<link rel="icon" sizes="192x192" href="{{{ asset('favicon.png') }}}">
		<meta name="apple-mobile-web-app-title" content="Artisan Gestión">
		<link href="{{{ asset('apple-touch-icon.png') }}}" rel="apple-touch-icon" />
		<!-- //For-Mobile-Apps-and-Meta-Tags -->

		<!-- animate css -->
		{{ Html::style("css/landing/animate.min.css") }}
		<!-- bootstrap css -->
		{{ Html::style("css/landing/bootstrap.min.css") }}
		<!-- font-awesome -->
		{{ Html::style("css/landing/font-awesome.min.css") }}
		<!-- google font -->
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,700,800' rel='stylesheet' type='text/css'>

		<!-- custom css -->
		{{ Html::style("css/landing/ciatt-style.css") }}

	</head>
	<body>
		<!-- start preloader -->
		<div class="preloader">
			<div class="sk-spinner sk-spinner-rotating-plane"></div>
    	 </div>
		<!-- end preloader -->
		<!-- start navigation -->
		<nav class="navbar navbar-default navbar-fixed-top ciatt-nav" role="navigation">
			<div class="container">
				<div class="navbar-header">
					<button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="icon icon-bar"></span>
						<span class="icon icon-bar"></span>
						<span class="icon icon-bar"></span>
					</button>
					<a href="#" class="navbar-brand">
						<div>
							<img src="{{url('images/frontend/logos/favicon.png')}}" style="max-height: 40px;" alt="Favicon">
							<img src="{{url('images/frontend/logos/logo_mini.png')}}" style="max-height: 40px;" alt="Logo">
						</div>
					</a>
				</div>
				<div class="collapse navbar-collapse">
					<ul class="nav navbar-nav navbar-right text-uppercase">
						<li><a href="#home">Inicio</a></li>
						<li><a href="#feature">Características</a></li>
						<li><a href="#pricing">Precio</a></li>
						<li><a href="#download">Demo Online</a></li>
						<li><a href="#contact">Contacto</a></li>
					</ul>
				</div>
			</div>
		</nav>
		<!-- end navigation -->
		<!-- start home -->
		<section id="home">
			<div class="overlay">
				<div class="container">
					<div class="row">
						<div class="col-md-1"></div>
						<div class="col-md-10 wow fadeIn" data-wow-delay="0.3s">
							<h1 class="text-upper">Artisan Sistema de Gestión</h1>
							<p class="tm-white">Te ayudamos a automatizar la gestión de tu empresa, minimizar el riesgo de errores y reducir el tiempo empleado en el levamiento y registro de datos en tareas administrativas.</p>
							<p>
                                <a href="{{url('/login')}}" class="btn btn-primary text-uppercase">Iniciar Sesion</a>
                                <a href="{{url('/register')}}" class="btn btn-primary text-uppercase">Registra tu empresa <strong>GRATIS</strong></a></p>
							<img src="{{url('images/landing/software-img.png')}}" class="img-responsive" alt="Artisan Gestión">
						</div>
						<div class="col-md-1"></div>
					</div>
				</div>
			</div>
		</section>
		<!-- end home -->
		<!-- start divider -->
		<section id="divider">
			<div class="container">
				<div class="row">
					<div class="col-md-4 wow fadeInUp ciatt-box" data-wow-delay="0.3s">
						<i class="fa fa-mobile"></i>
						<h3 class="text-uppercase">MULTIPLATAFORMA</h3>
						<p>El sistema de gestión está adaptado para el óptimo funcionamiento en todos los dispositivos: Computadoras, notebooks, tablets y celulares. </p>
					</div>
					<div class="col-md-4 wow fadeInUp ciatt-box" data-wow-delay="0.3s">
						<i class="fa fa-lightbulb-o"></i>
						<h3 class="text-uppercase">FÁCIL E INTUITIVO</h3>
						<p>Los módulos y funciones del sistema están distribuidos de manera fácil y clara para que el usuario con sólo navegar por el menú principal sepa qué contiene. </p>
					</div>
					<div class="col-md-4 wow fadeInUp ciatt-box" data-wow-delay="0.3s">
						<i class="fa fa-shield"></i>
						<h3 class="text-uppercase">SEGURO Y PRIVADO</h3>
						<p>Artisan Gestión es desarrollado por especialistas en seguridad informática garantizando que su información siempre será privada y segura. Accesible sólo para aquellos que posean los permisos necesarios. </p>
					</div>				
				</div>
				<div class="col-md-12 wow bounceIn">
				<p><h3 class="text-uppercase">«Gestioná los Recursos de tu empresa y tomá decisiones apoyadas en Información de tiempo real con el sistema Gestión Integral».</h3></p></br>
				
				<p><center><img src="{{url('images/landing/ciatt-demo.gif')}}" class="img-responsive" alt="Artisan Demo" /></center></p></div>
			</div>
			
		</section>
		<!-- end divider -->

		<!-- start feature -->
		<section id="feature">
			<div class="container">
				<div class="row">
					<div class="col-md-6 wow fadeInLeft" data-wow-delay="0.6s">
						<h2 class="text-uppercase">Características</h2>
						<p><li><strong>Administración de precios en multi-moneda.</strong></li>
                    <li><strong>Múltiples listas de precios.</strong></li>
                    <li><strong>Informes en PDF.</strong></li>
                    <li><strong>Importación y exportación de Excel.</strong></li>
					<li><strong>Creación automática de Artículos.</strong></li>
                    <li><strong>Centro de Ayuda.</strong></li>
                    <li><strong>Soporte 24/7.</strong></li></p>
								</div>
					<div class="col-md-6 wow fadeInRight" data-wow-delay="0.6s">
						<img src="{{url('images/landing/ciattventa.png')}}" class="img-responsive" alt="feature img">
					</div>
				</div>
			</div>
		</section>
		<!-- end feature -->

		<!-- start feature1 -->
		<section id="feature1">
			<div class="container">
				<div class="row">
					<div class="col-md-6 wow fadeInUp" data-wow-delay="0.6s">
						<img src="{{url('images/landing/ciattcaja.png')}}" class="img-responsive" alt="feature img">
					</div>
					<div class="col-md-6 wow fadeInUp" data-wow-delay="0.6s">
						<h2 class="text-uppercase">Ventajas del Software</h2>
						<p><li><strong>Control total sobre sus ingresos y deudas.</strong></li>
                    <li><strong>Gestión completa de pedidos.</strong></li>
                    <li><strong>Módulo de facturación electronica ya incluído.</strong></li>
                    <li><strong>Facturas, tickets o cualquier tipo de comprobantes en segundos.</strong></li>
					<li><strong>Informes detallados.</strong></li>
                    <li><strong>Control total sobre los usuarios y sus actividades.</strong></li>
                    <li><strong>Estadísticas gráficas indispensables para monitorear su avance.</strong></li>
                    <li><strong>Control de tareas programadas.</strong></li></p>
					</div>
				</div>
			</div>
					</section>
		<!-- end feature1 -->

		<!-- start pricing -->
		<section id="pricing">
			<div class="container">
				<div class="row">
				  <div class="col-md-12 wow bounceIn">
				    <h2 class="text-uppercase">Tabla de Precios</h2>
					</div>
					@foreach($planes as $plan)
					<div class="col-md-4 wow fadeIn" data-wow-delay="0.6s">
						<div class="pricing {{($plan->id == 2) ? 'active' : ''}} text-uppercase">
							<div class="pricing-title">
								<h4>{{ $plan->nombre }}</h4>
								<p>${{ number_format($plan->precio,0,'','') }}</p>
								<small class="text-lowercase">mensual</small>
							</div>
							<ul>
								<li>
									@if($plan->cantidad_usuarios > 1)
										<i class="fa fa-users"></i> {{ $plan->cantidad_usuarios }} Usuarios
									@else
										<i class="fa fa-user"></i> {{ $plan->cantidad_usuarios }} Usuario
									@endif
								</li>
								<li><i class="fa fa-print"></i> {{ $plan->cantidad_puntos_venta }}
									{{($plan->cantidad_puntos_venta > 1) ? 'Puntos de Ventas' : 'Punto de Venta'}}
								</li>
								<li><i class="fa fa-building"></i> {{ $plan->cantidad_sucursales }}
									{{($plan->cantidad_sucursales > 1) ? 'Sucursales' : 'Sucursal'}}
								</li>
								<li><i class="fa fa-comments"></i> {{ ($plan->chat) ? 'Soporte por Chat' : 'Sin Soporte por Chat' }}</li>
							</ul>
							{{--<button class="btn btn-primary text-uppercase">Contratar</button>--}}
						</div>
					</div>
					@endforeach
				</div>
				<hr><br>
				<div class="row">
					<div class="col-md-12 wow bounceIn">
						<h4>Tu primer mes es <span style="color:green;">GRATIS</span> y si te suscribis un año completo te damos <i>otro mes mas de regalo</i>!</h4>
					</div>
				</div>
			</div>
		</section>
		<!-- end pricing -->

		<!-- start download -->
		<section id="download">
			<div class="container">
				<div class="row">
					<div class="col-md-6 wow fadeInLeft" data-wow-delay="0.6s">
						<h2 class="text-uppercase">Demo Online del Sistema</h2>
						<p>Podes utilizar el sistema con nuestro usuario demo o registrar tu empresa de forma totalmente gratuita! <br>
							<strong>Usuario demo:</strong> demo@ciatt.com.ar | <strong>Contraseña:</strong> ciatt</p>
						<a href="{{url('/login')}}" class="btn btn-primary text-uppercase"><i class="fa fa-pie-chart"></i> VER DEMO ONLINE</a>
					</div>
					<div class="col-md-6 wow fadeInRight" data-wow-delay="0.6s">
						<img src="{{url('images/landing/software-img-3.png')}}" class="img-responsive" alt="feature img">
					</div>
				</div>
			</div>
		</section>
		<!-- end download -->

		<section id="nube">
			<div class="container">
				<div class="row">
					<div class="col-md-12 wow fadeInLeft" data-wow-delay="0.6s">
						<h3 class="text-uppercase">Diferencias entre sistemas en la nube <i class="fa fa-cloud" style="color:teal;"></i> y sistemas instalables o enlatados <i class="fa fa-cube" style="color:orange;"></i></h3>
						<table class="table">
							<tr>
								<th>Artisan Gestión</th>
								<th>Sistemas Instalables o Enlatados</th>
							</tr>
							<tr>
								<td><i class="fa fa-check" style="color:green;"></i> No necesita instalación y puede utilizarse en cualquier dispositivo en cualquier momento.</td>
								<td>Necesita ser instalado y solo funciona en las computadoras registradas.</td>
							</tr>
							<tr>
								<td><i class="fa fa-check" style="color:green;"></i> No consume espacio en disco ni recursos.</td>
								<td>Consumen mucho espacio y recursos. Mientras más lento sea su equipo, más lento será el sistema.</td>
							</tr>
							<tr>
								<td><i class="fa fa-check" style="color:green;"></i> Backups automáticos, permanentes y garantizados. Su información siempre estará a salvo.</td>
								<td>No realizan copias de seguridad o deben hacerse de forma manual.</td>
							</tr>
							<tr>
								<td><i class="fa fa-check" style="color:green;"></i> Actualización y mantenimiento permanente invisibles para el usuario.</td>
								<td>Actualizaciones esporádicas, muchas veces con costos adicionales, que requieren intervención del usuario.</td>
							</tr>
							<tr>
								<td><i class="fa fa-check" style="color:green;"></i> Posibilidad de facturar desde cualquier sitio.</td>
								<td>Facturación solo desde los equipos con los puntos de venta habilitados e instalados.</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</section>

		<!-- start contact -->
		<section id="contact">
			<div class="overlay">
				<div class="container">
					<div class="row">
						<div class="col-md-6 wow fadeInUp" data-wow-delay="0.6s">
							<h2 class="text-uppercase">¿Te ayudamos en algo?</h2>
							<p>Soporte continuo y especializado en todos nuestros desarrollos. Puede realizar todas las consultas que desee por cualquiera de los canales de comunicación disponibles.</p>
							<address>
								<p><i class="fa fa-phone"></i> +54 03471 15 600189</p>
								<p><i class="fa fa-envelope-o"></i> info@ciatt.com.ar</p>
								<p><i class="fa fa-skype"></i> Ciatt Software</p>
							</address>
						</div>
						<div class="col-md-6 wow fadeInUp" data-wow-delay="0.6s">
							<div class="contact-form">
								<form action="#" method="post">
									<div class="col-md-6">
										<input type="text" class="form-control" placeholder="Nombre">
									</div>
									<div class="col-md-6">
										<input type="email" class="form-control" placeholder="Email">
									</div>
									<div class="col-md-12">
										<input type="text" class="form-control" placeholder="Asunto">
									</div>
									<div class="col-md-12">
										<textarea class="form-control" placeholder="Mensaje" rows="4"></textarea>
									</div>
									<div class="col-md-8">
										<input type="submit" class="form-control text-uppercase" value="Enviar">
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- end contact -->

		<!-- start footer -->
		<footer>
			<div class="container">
				<div class="row">
					<p>Copyright © 2018 Artisan Gestion</p>
				</div>
			</div>
		</footer>
		<!-- end footer -->

		{{ Html::script("js/landing/jquery.js") }}
		{{ Html::script("js/landing/bootstrap.min.js") }}
		{{ Html::script("js/landing/wow.min.js") }}
		{{ Html::script("js/landing/jquery.singlePageNav.min.js") }}
		{{ Html::script("js/landing/custom.js") }}

		<!--Start of Tawk.to Script-->
		<script type="text/javascript">
            var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
            (function(){
                var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
                s1.async=true;
                s1.src='https://embed.tawk.to/5aebad585f7cdf4f0533d962/default';
                s1.charset='UTF-8';
                s1.setAttribute('crossorigin','*');
                s0.parentNode.insertBefore(s1,s0);
            })();
		</script>
		<!--End of Tawk.to Script-->

		<!-- Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-53444193-6"></script>
		<script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-53444193-6');
		</script>
	</body>
</html>