<div class="modal fade" id="modal-pago-tarjeta" tabindex="-1" role="dialog" aria-labelledby="tarjetaModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="tarjetaModalLabel">Pago Tarjeta</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    {!! Form::label('tarjeta_id', 'Tarjeta') !!}
                    {!! Form::select('tarjeta_id', $tarjetas, '',['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('importe_pago_tarjeta', 'Importe') !!}
                    <div class="input-group">
                        <div class="input-group-addon"><i class="fa fa-usd"></i></div>
                        {!! Form::number('importe_pago_tarjeta', '',['class' => 'form-control', 'step' => '0.01']) !!}
                        <input type="hidden" id="importe_pago_auxiliar" value="0">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button id="guardar-pago-tarjata" type="button" class="btn btn-themecolor"><i class="glyphicon glyphicon-ok"></i> Guardar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>

@section('scripts-dialog-tarjeta')
    <script>

    </script>
@endsection