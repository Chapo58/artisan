<div class="modal fade" id="modal-cheque-propio" tabindex="-1" role="dialog" aria-labelledby="chequeModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="chequeModalLabel">Cheque Propio</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    {!! Form::label('numero_cheque_propio', 'Número') !!}
                    <div class="input-group">
                        <div class="input-group-addon"><i class="fa fa-hashtag"></i></div>
                        {!! Form::number('numero_cheque_propio', '',['class' => 'form-control', 'step' => '0.01']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('chequera_cheque_propio_id', 'Chequera') !!}
                    {!! Form::select('chequera_cheque_propio_id', $chequeras, 0,['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('importe_cheque_propio', 'Importe') !!}
                    <div class="input-group">
                        <div class="input-group-addon"><i class="fa fa-usd"></i></div>
                        {!! Form::number('importe_cheque_propio', '',['class' => 'form-control', 'step' => '0.01']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('vencimiento_cheque_propio', 'Vencimiento') !!}
                    <div class="input-group">
                        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                        {!! Form::text('vencimiento_cheque_propio', '',['class' => 'form-control fecha', 'step' => '0.01', 'style' => 'z-index: 100000;']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('emision_cheque_propio', 'Emisión') !!}
                    <div class="input-group">
                        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                        {!! Form::text('emision_cheque_propio', '',['class' => 'form-control fecha', 'step' => '0.01', 'style' => 'z-index: 100000;']) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button id="guardar-cheque-propio" type="button" class="btn btn-themecolor"><i class="glyphicon glyphicon-ok"></i> Guardar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>

@section('scripts-dialog-cheque-propio')
    <script>

    </script>
@endsection