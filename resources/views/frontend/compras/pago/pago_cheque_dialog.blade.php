<div class="modal fade" id="modal-cheque-tercero" tabindex="-1" role="dialog" aria-labelledby="chequeModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="chequeModalLabel">Cheque de Tercero</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    {!! Form::label('cheque_id', 'Cheque') !!}
                    {!! Form::select('cheque_id', $cheques_terceros, 0,['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('importe_cheque', 'Importe') !!}
                    <div class="input-group">
                        <div class="input-group-addon"><i class="fa fa-usd"></i></div>
                        {!! Form::number('importe_cheque', '',['class' => 'form-control', 'step' => '0.01']) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button id="seleccionar-cheque" type="button" class="btn btn-themecolor"><i class="glyphicon glyphicon-ok"></i> Seleccionar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>

@section('scripts-dialog-cheque')
    <script>
        $('select#cheque_id').on('change', function () {
            buscarDatosCheque();
        });

        function buscarDatosCheque() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $( "input[name*='_token']" ).val()
                }
            });
            var cheque_id = $('select#cheque_id option:selected').val();
            $.ajax({
                type: "POST",
                url: '{{ url("/fondos/cheque/datos-cheque") }}',
                data: {cheque_id: cheque_id},
                success: function( msg ) {
                    $('#importe_cheque').val(msg);
                }
            });
        }
    </script>
@endsection