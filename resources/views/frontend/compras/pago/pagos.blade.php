<div class="card">
    <div class="card-header">
        <div class="card-actions">
            <a class="mytooltip" data-action="collapse"><i class="ti-minus"></i><span class="tooltip-content3">Minimizar Ventana</span></a>
            <a class="btn-minimize mytooltip" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
        </div>
        <h4 class="card-title m-b-0">
            Forma de Pago
        </h4>
    </div>
    <div class="card-body collapse show b-t">
        <div class="row form-group">
            <div class="col-md-12">
                {!! Form::label('forma_pago', 'Formas de Pagos') !!}
                <div class="row">
                    <div class="col-md-6">
                        {!! Form::select('forma_pago', $formas_pagos, 0, ['class' => 'form-control']) !!}
                        {!! $errors->first('forma_pago', '<p class="text-danger">:message</p>') !!}
                    </div>
                    <div class="col-md-2">
                        <button type="button" class="btn btn-success" id="agregar-detalle-pago">
                            <i class="fa fa-plus" aria-hidden="true"></i> Agregar
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <table id="tabla-detalles-pagos" class="table color-table info-table form-material">
                <thead>
                <tr>
                    <th style="width:50%;">Descripción</th>
                    <th style="width:25%;">Monto</th>
                    <th style="width:25%;">Interés</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @if (isset($compra))
                    @foreach($compra->detallesPagos as $detallePago)
                        <tr>
                            <td>
                                {{ $detallePago->descripcion }}
                            </td>
                            <td>
                                <div class="input-group">
                                    <div class="input-group-addon"><i class="fa fa-usd"></i></div>
                                    <input class="form-control green monto" name="detalles_pago[{{ $detallePago->id }}][monto]" type="number" step="0.01" value="{{ $detallePago->monto }}" readonly>
                                </div>
                            </td>
                            <td>
                                <div class="input-group">
                                    <div class="input-group-addon"><i class="fa fa-usd"></i></div>
                                    <input class="form-control green interes" name="detalles_pago[{{ $detallePago->id }}][interes]" type="number" value="{{ $detallePago->interes }}" readonly>
                                </div>
                            </td>
                            <td>
                                <a class="btn btn-danger btn-sm borrar-detalle-cobro" name="detalles_pago[{{ $detallePago->id }}][eliminar]" title="Borrar Detalle Pago">
                                    <i class="fa fa-trash-o" aria-hidden="true"></i>
                                </a>
                                <input type="hidden" name="detalles_pago[{{ $detallePago->id }}][estado]" class="estado" value="1">
                                <input type="hidden" name="detalles_pago[{{ $detallePago->id }}][id]" value="{{ $detallePago->id }}">
                                <input type="hidden" name="detalles_pago[{{ $detallePago->id }}][forma_cobro]" value="{{ $detallePago->forma_cobro }}">
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="card card-inverse card-danger">
                    <div class="box bg-danger text-center">
                        <h2 class="font-light text-white">
                            <div class="pull-left">DIFERENCIA:</div>
                            <div class="pull-right" id="diferencia">$ 0.00</div>
                        </h2>
                    </div>
                </div>
            </div>
            <div class="col-md-3"></div>
            <div class="col-md-5">
                <div class="card card-inverse card-info">
                    <div class="box bg-info text-center">
                        <h2 class="font-light text-white">
                            <div class="pull-left">TOTAL PAGO:</div>
                            <div class="pull-right" id="total-final">$ 0.00</div>
                        </h2>
                    </div>
                </div>
            </div>
        </div>
        {!! Form::number('total_pago', null, ['id' => 'total-pago','class' => 'd-none', 'readonly']) !!}
    </div>
</div>

    @include('frontend.compras.pago.pago_cheque_dialog')
    @include('frontend.compras.pago.pago_cheque_propio_dialog')
    @include('frontend.compras.pago.pago_cuenta_bancaria_dialog')
    @include('frontend.compras.pago.pago_cupon_tarjeta_dialog')

@section('scripts-pagos')
    <script>
        $('.form-horizontal').submit(function(e) {
            detalles = $('input[name^="detalles_pago"]');
            if(detalles.length == 0){
                e.preventDefault();
                swal("Pagos", "Por favor ingrese al menos un medio de pago para finalizar.", "warning");
            }
        });

        $("#agregar-detalle-pago").click(function(){
            formaPagoDescripcion = $('#forma_pago option:selected').text();
            formaPagoValor = $('#forma_pago option:selected').val();

            monto = calcularMontoPago();
            if(formaPagoValor == '0'){//Efectivo
                agregarDetallePago(formaPagoValor, 'Pago en '+formaPagoDescripcion, monto, 0, null, false);
            }else if(formaPagoValor == '4'){//Cuenta Corriente
                agregarDetallePago(formaPagoValor, formaPagoDescripcion, monto, 0,null, false);
            }else if(formaPagoValor == '6'){//Mercado Pago
                agregarDetallePago(formaPagoValor, formaPagoDescripcion, monto, 0,null, false);
            }else if(formaPagoValor == '1'){//Cheque
                $('select#cheque_id').val('');
                $('input#importe_cheque').val('');
                $('#modal-cheque-tercero').modal();
            }else if(formaPagoValor == '2'){//Tranferencia Bancario
                $('input#importe_transferencia_bancaria').val(monto);
                $('#modal-transferencia-nueva').modal();
            }else if(formaPagoValor == '3'){//Pago Tarjeta
                $('select#tarjeta_id').val('');
                $('input#importe_pago_tarjeta').val(monto);
                $('input#importe_pago_auxiliar').val(monto);
                $('#modal-pago-tarjeta').modal();
            }else if(formaPagoValor == '5'){//Cheque Propio
                $('input#numero_cheque_propio').val('');
                $('select#chequera_cheque_propio_id').val('');
                $('input#importe_cheque_propio').val(monto);
                $('input#vencimiento_cheque_propio').val('');
                $('input#emision_cheque_propio').val('');
                $('#modal-cheque-propio').modal();
            }
        });

        $("#seleccionar-cheque").click(function(){
            $('select#cheque_id').parent().removeClass('has-error');
            $('input#importe_cheque').parent().removeClass('has-error');

            if($('select#cheque_id option:selected').val() == ''){
                $('select#cheque_id').parent().addClass('has-error');
                $('select#cheque_id').focus();
            }else if($('input#importe_cheque').val() == ''){
                $('input#importe_cheque').parent().addClass('has-error');
                $('input#importe_cheque').focus();
            }else{
                cheque = [
                    ['cheque_id', $('select#cheque_id option:selected').val()],
                ];

                formaPagoValor = $('#forma_pago option:selected').val();
                formaPagoDescripcion = $('#forma_pago option:selected').text()+': '+$('select#cheque_id option:selected').text();
                monto = $('input#importe_cheque').val();

                $('#modal-cheque-tercero').modal('hide');
                $('select#cheque_id option:selected').remove()
                $('input#importe_cheque').val('');

                agregarDetallePago(formaPagoValor, formaPagoDescripcion, monto, 0, cheque, true);
            }
        });

        $("#guardar-transferencia-bancaria").click(function(){
            $('input#importe_transferencia_bancaria').parent().removeClass('has-error');

            if($('input#importe_transferencia_bancaria').val() == ''){
                $('input#importe_transferencia_bancaria').parent().addClass('has-error');
                $('input#importe_transferencia_bancaria').focus();
            }else{
                transferenciaBancaria = [
                    ['cuenta_bancaria_id', $('select#cuenta_bancaria_id option:selected').val()],
                    ['importe_transferencia_bancaria', $('input#importe_transferencia_bancaria').val()]
                ];

                formaPagoValor = $('#forma_pago option:selected').val();
                formaPagoDescripcion = 'Pago por Transferencia Bancaria de cuenta Nº:'+$('select#cuenta_bancaria_id option:selected').text();
                monto = $('input#importe_transferencia_bancaria').val();

                $('#modal-transferencia-nueva').modal('hide');
                $('input#importe_transferencia_bancaria').val('');

                agregarDetallePago(formaPagoValor, formaPagoDescripcion, monto, 0, transferenciaBancaria, true);
            }
        });

        $("#guardar-pago-tarjata").click(function(){
            $('select#tarjeta_id').parent().removeClass('has-error');
            $('input#importe_pago_tarjeta').parent().removeClass('has-error');

            if($('select#tarjeta_id option:selected').val() == ''){
                $('select#tarjeta_id').parent().addClass('has-error');
                $('select#tarjeta_id').focus();
            }else if($('input#importe_pago_tarjeta').val() == ''){
                $('input#importe_pago_tarjeta').parent().addClass('has-error');
                $('input#importe_pago_tarjeta').focus();
            }else{
                pagoTarjeta = [
                    ['tarjeta_id', $('select#tarjeta_id option:selected').val()],
                    ['importe_pago_tarjeta', $('input#importe_pago_tarjeta').val()]
                ];

                formaPagoValor = $('#forma_pago option:selected').val();
                formaPagoDescripcion = 'Pago con tarjeta '+$('select#tarjeta_id option:selected').text();
                if($('input#importe_pago_tarjeta').val() > $('input#importe_pago_auxiliar').val()){
                    interes = $('input#importe_pago_tarjeta').val() - $('input#importe_pago_auxiliar').val();
                }else{
                    interes = 0;
                }
                monto = parseFloat($('input#importe_pago_tarjeta').val()) + (interes * -1);

                $('#modal-pago-tarjeta').modal('hide');
                $('input#importe_pago_tarjeta').val('');
                $('input#importe_pago_auxiliar').val('');

                agregarDetallePago(formaPagoValor, formaPagoDescripcion, monto, interes, pagoTarjeta, true);
            }
        });

        $("#guardar-cheque-propio").click(function(){
            $('input#numero_cheque_propio').parent().removeClass('has-error');
            $('select#chequera_cheque_propio_id').parent().removeClass('has-error');
            $('input#importe_cheque_propio').parent().removeClass('has-error');

            if($('input#numero_cheque_propio').val() == ''){
                $('input#numero_cheque_propio').parent().addClass('has-error');
                $('input#numero_cheque_propio').focus();
            }else if($('select#chequera_cheque_propio_id option:selected').val() == ''){
                $('select#chequera_cheque_propio_id').parent().addClass('has-error');
                $('select#chequera_cheque_propio_id').focus();
            }else if($('input#importe_cheque_propio').val() == ''){
                $('input#importe_cheque_propio').parent().addClass('has-error');
                $('input#importe_cheque_propio').focus();
            }else{
                pagoChequePropio = [
                    ['numero', $('input#numero_cheque_propio').val()],
                    ['chequera_id', $('select#chequera_cheque_propio_id option:selected').val()],
                    ['importe', $('input#importe_cheque_propio').val()],
                    ['vencimiento', $('input#vencimiento_cheque_propio').val()],
                    ['emision', $('input#emision_cheque_propio').val()],
                ];

                formaPagoValor = $('#forma_pago option:selected').val();
                formaPagoDescripcion = 'Pago con Cheque Propio Nº '+$('input#numero_cheque_propio').val();
                monto = parseFloat($('input#importe_cheque_propio').val());

                $('#modal-cheque-propio').modal('hide');

                agregarDetallePago(formaPagoValor, formaPagoDescripcion, monto, 0, pagoChequePropio, true);
            }
        });

        $(document).on('click', '.borrar-detalle-pago', function (e) {
            e.preventDefault();
            inputNombre = this.attributes['name'].value;

            $('.borrar-detalle-pago[name="' + inputNombre + '"]').parent().find('input.estado').val(0);
            $('.borrar-detalle-pago[name="' + inputNombre + '"]').parent().parent().hide("slow");
            $('.borrar-detalle-pago[name="' + inputNombre + '"]').parent().parent().find('td input.monto').val(0);
            $('.borrar-detalle-pago[name="' + inputNombre + '"]').parent().parent().find('td input.interes').val(0);

            refrescarTotales();
        });

        $(document).on('change','.monto',function(e){
            refrescarTotales();
        });

        $('select#forma_pago').on('change', function () {
            refrescarTotales();
        });

        function calcularMontoPago() {
            total = parseFloat($("input#total").val());
            totalPagos = 0;
            pagosAgregados = $('input[name^="detalles_pago"].monto');
            for (var i = 0; i < pagosAgregados.length; i++) {
                totalPagos += parseFloat(pagosAgregados[i].value);
            }
            return (total - totalPagos).toFixed(2);
        }

        function agregarDetallePago(valor, descripcion, monto, interes, objeto, readonly) {
            var numero_orden = $('table#tabla-detalles-pagos tbody tr').length+ 1;

            objetoString = '';
            if(objeto != null){
                for (var i = 0; i < objeto.length; i++) {
                    objetoString += '<input type="hidden" name="detalles_pago['+(numero_orden*-1)+']['+objeto[i][0]+']" value="'+objeto[i][1]+'">'
                }
            }

            stringReadonly = '';
            if(readonly){
                stringReadonly = 'readonly';
            }

            $("#tabla-detalles-pagos").append('<tr>' +
                '<td>' +
                    descripcion +
                    '<input type="hidden" class="id" name="detalles_pago['+(numero_orden*-1)+'][id]" value="'+(numero_orden*-1)+'">' +
                '</td>' +
                '<td>' +
                    '<div class="input-group">' +
                        '<div class="left-icon">$</div>' +
                        '<input class="form-control green monto" name="detalles_pago['+(numero_orden*-1)+'][monto]" type="number" step="0.01" value="'+ monto +'" '+ stringReadonly +'>' +
                    '</div>' +
                '</td>' +
                '<td>' +
                    '<div class="input-group">' +
                        '<div class="left-icon">$</div>' +
                        '<input class="form-control green interes" name="detalles_pago['+(numero_orden*-1)+'][interes]" type="number" value="'+ interes +'" readonly>' +
                    '</div>' +
                '</td>' +
                '<td>' +
                    '<button type="button" class="btn btn-danger btn-circle borrar-detalle-pago" name="detalles_pago['+(numero_orden*-1)+'][eliminar]" title="Borrar Pago">' +
                        '<i class="fa fa-trash-o" aria-hidden="true"></i>' +
                    '</button>' +
                    '<input type="hidden" name="detalles_pago['+(numero_orden*-1)+'][estado]" class="estado" value="1">' +
                    '<input type="hidden" name="detalles_pago['+(numero_orden*-1)+'][id]" value="'+(numero_orden*-1)+'">' +
                    '<input type="hidden" name="detalles_pago['+(numero_orden*-1)+'][forma_pago]" value="'+valor+'">' +
                    objetoString +
                '</td>' +
            '</tr>');

            refrescarTotales();
        }
    </script>
    @yield('scripts-dialog-tarjeta')
    @yield('scripts-dialog-cheque')
    @yield('scripts-dialog-cheque-propio')
@endsection