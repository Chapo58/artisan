<div class="modal fade" id="modal-transferencia-nueva" tabindex="-1" role="dialog" aria-labelledby="transferenciaModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="transferenciaModalLabel">Tranferencia Bancaria</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    {!! Form::label('cuenta_bancaria_id', 'Cuenta Bancaria') !!}
                    {!! Form::select('cuenta_bancaria_id', $cuentas_bancarias, 0,['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('importe_transferencia_bancaria', 'Importe') !!}
                    <div class="input-group">
                        <div class="input-group-addon"><i class="fa fa-usd"></i></div>
                        {!! Form::number('importe_transferencia_bancaria', '',['class' => 'form-control', 'step' => '0.01']) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button id="guardar-transferencia-bancaria" type="button" class="btn btn-themecolor"><i class="glyphicon glyphicon-ok"></i> Guardar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>