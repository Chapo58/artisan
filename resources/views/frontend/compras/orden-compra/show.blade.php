@extends('frontend.layouts.app')

@section ('title', trans('labels.frontend.orden_compra_titulo')." - ".trans('buttons.general.crud.show'))

@section('content')
    <div class="row page-titles">
        <div class="col-6 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('labels.frontend.orden_compra_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item"><a href="{{url('/compras/orden-compra')}}">{{ trans('labels.frontend.orden_compra_titulo') }}</a></li>
                <li class="breadcrumb-item">{{ trans('buttons.general.crud.show') }} {{ trans('labels.frontend.orden_compra') }}</li>
                <li class="breadcrumb-item active">{{ $ordenCompra }}</li>
            </ol>
        </div>
        <div class="col-6">
            <div class="pull-right">
                <a href="{{ url('/compras/orden-compra') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans('buttons.general.crud.back') }}</span>
                </a>
                <a href="{{ url('/compras/orden-compra/' . $ordenCompra->id . '/edit') }}" title="{{ trans('buttons.general.crud.edit') }}" class="btn btn-info">
                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans('buttons.general.crud.edit') }}</span>
                </a>
                <a href="{{ url('/compras/orden-compra/generar-compra/'.$ordenCompra->id) }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning">
                    <i class="fa fa-copy" aria-hidden="true"></i>
                    <span class="hidden-xs-down">Generar Compra</span>
                </a>
                <a id="print" href="javascript:void(0)" target="_blank" title="Imprimir" class="btn btn-primary">
                    <i class="fa fa-print" aria-hidden="true"></i>
                    <span class="hidden-xs-down">Imprimir</span>
                </a>
                <a href="javascript:void(0)" title="{{trans("buttons.general.crud.delete")." ".trans("labels.frontend.orden_compra")}}" class="btn btn-danger delete">
                    <i class="fa fa-trash" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans("buttons.general.crud.delete") }}</span>
                </a>
            </div>
        </div>
    </div>

    <div class="card card-body printableArea">
        <h3>
            <b>Orden de Compra</b>
            <span class="pull-right">N° {{ $ordenCompra->numero }}</span>
        </h3>
        <hr>
        <div class="row">
            <div class="col-md-12">
                <div class="pull-left">
                    <address>
                        <h3> &nbsp;<b class="text-danger">
                                @if($ordenCompra->proveedor)
                                    <a href="{{url('/personas/proveedor/'.$ordenCompra->proveedor->id)}}">{{ $ordenCompra->proveedor }}</a>
                            </b></h3>
                        <p class="text-muted m-l-5">{{ (isset($ordenCompra->proveedor->domicilio->direccion)) ? $ordenCompra->proveedor->domicilio->direccion : '' }}
                            <br/> I.V.A.: {{ $ordenCompra->proveedor->condicionIva }}
                            <br/> CUIT: {{ $ordenCompra->proveedor->cuit }}
                            <br/> Cuenta: {{ $ordenCompra->proveedor->id }}</p>
                        @else
                            Proveedor Eliminado
                            </b></h3>
                        @endif
                    </address>
                </div>
                <div class="pull-right text-right">
                    <address>
                        <h3 class="font-bold">{{$empresa->razon_social}}</h3>
                        <p class="text-muted m-l-30">{{ (isset($empresa->domicilio->direccion)) ? $empresa->domicilio->direccion : '' }}
                            <br/> I.V.A.: {{ $empresa->condicionIva }}
                            <br/> CUIT: {{ $empresa->cuit }}
                            <br/> II.BB.: {{ $empresa->ingresos_brutos }}
                            <br/> Inicio de Actividades: {{($empresa->inicio_actividad) ? $empresa->inicio_actividad->format('d/m/Y') : ''}}</p>
                        <p class="text-muted"><b>Fecha :</b> <i class="fa fa-calendar"></i> {{ $ordenCompra->fecha->format('d/m/Y H:i') }}</p>
                    </address>
                </div>
            </div>

            <div class="col-md-12">
                <div class="table-responsive m-t-40" style="clear: both;">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th class="text-center" style="width:10%;">#</th>
                                <th class="text-right" style="width:20%;">Cantidad</th>
                                <th style="width:70%;">Descripción</th>
                            </tr>
                        </thead>
                        <tbody>
                        @if(!$ordenCompra->detalles->isEmpty())
                            @foreach($ordenCompra->detalles as $detalle)
                                <tr>
                                    <td class="text-center">
                                        {{ $loop->index + 1 }}
                                    </td>
                                    <td class="text-right">
                                        {{ number_format($detalle->cantidad , 0) }}
                                    </td>
                                    <td>
                                        @if(isset($detalle->articulo))
                                            <a href="{{url('/articulos/articulo/'.$detalle->articulo->id)}}">
                                                {{ $detalle->articulo->nombre }}
                                            </a>
                                        @else
                                            {{ $detalle->articulo_nombre }}
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
            @if($ordenCompra->descripcion)
                <p>Comentario: <i>{{ $ordenCompra->descripcion }}</i></p>
            @endif
        </div>
    </div>
@endsection

@section('after-scripts')
    {{ Html::script("js/frontend/jquery.PrintArea.js") }}

<script>
    $(document).ready(function() {
        $("#print").click(function() {
            var mode = 'iframe'; //popup
            var close = mode == "popup";
            var options = {
                mode: mode,
                popClose: close
            };
            $("div.printableArea").printArea(options);
        });
    });

    $("body").on("click", ".delete", function () {
        swal({
            title: "{{trans('buttons.general.crud.delete').' '.trans('labels.frontend.orden_compra')}}",
            text: "¿Realmente desea eliminar este registro?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#ff8726',
            confirmButtonText: "Eliminar"
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '{{ url('/compras/orden-compra/'.$ordenCompra->id) }}',
                    data: {"_method" : 'DELETE'},
                    success: function (msg) {
                        swal("Eliminado!", "El registro ha sido correctamente eliminado.", "success").then((result) => { window.location.href = '{{url('/compras/orden-compra')}}'; });
                    }
                });
            }
        });
    });
</script>

@endsection