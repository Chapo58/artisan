@extends('frontend.layouts.app')

@section ('title', trans('labels.frontend.orden_compra_titulo')." - ".trans('buttons.general.crud.create'))

@section('after-styles')
    {{ Html::style("css/frontend/plugins/jquery/jquery-ui.min.css") }}
@endsection

@section('content')
    <div class="row page-titles">
        <div class="col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('labels.frontend.orden_compra_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item"><a href="{{url('/compras/orden-compra')}}">{{ trans('labels.frontend.orden_compra_titulo') }}</a></li>
                <li class="breadcrumb-item active">{{ trans('buttons.general.crud.create') }} {{ trans('labels.frontend.orden_compra') }}</li>
            </ol>
        </div>
        <div class="col-4">
            <div class="pull-right">
                <a href="{{ url('/compras/orden-compra') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans('buttons.general.crud.back') }}</span>
                </a>
            </div>
        </div>
    </div>

    @tips($_SERVER['REQUEST_URI'])

    @include('includes.partials.messages')

    {!! Form::open(['url' => '/compras/orden-compra', 'class' => 'form-horizontal', 'files' => true]) !!}
        @include ('frontend.compras.orden-compra.form')
    {!! Form::close() !!}

@endsection

@section('after-scripts')
    {{ Html::script("js/frontend/plugins/jquery/jquery-ui.min.js") }}
    {{ Html::script("js/frontend/plugins/jquery/datepicker-es.js") }}
    {{ Html::script("js/frontend/plugins/maskedinput/jquery.maskedinput.min.js") }}

    <script>
        $( document ).ready(function() {
            $('input.fecha').datepicker({
                firstDay: 0,
                dateFormat: 'dd/mm/yy'
            }).mask("99/99/9999", {placeholder: "dd/mm/aaaa"});
            $('input#numero').mask("99999999",{placeholder:"########"});

            $(document).on("keydown.autocomplete", "#buscar_proveedor", function (e) {
                if (e.keyCode == 13) {
                    e.preventDefault();
                }else {
                    $(this).autocomplete({
                        source: '{{ url('/personas/proveedor/obtenerProveedorParaCompra') }}',
                        minlength: 1,
                        autoFocus: true,
                        select: function (e, ui) {
                            $('input#buscar_proveedor').val(ui.item.value);
                            $('input#proveedor_id').val(ui.item.id);
                        },
                        response: function(e, ui) {
                            if (ui.content.length == 1){ // Si solo me devuelve un proveedor, lo autocargo
                                ui.item = ui.content[0];
                                $('input#buscar_proveedor').val(ui.item.value).blur();
                                $('input#proveedor_id').val(ui.item.id);
                            }
                        }
                    });
                }
            });

            $(document).on("keydown.autocomplete",".buscar-articulo",function(e){
                $(this).autocomplete({
                    source:'{{ url('/articulos/articulo/obtenerArticuloConPorcentajeIva') }}',
                    minlength:1,
                    autoFocus:true,
                    select:function(e,ui) {
                        var fila = $(this).parent().parent();

                        $(this).val(ui.item.value);
                        fila.find('input.articulo-id').val(ui.item.id);
                    }
                });
            });

            $(document).on('click','.borrar-detalle', function() {
                var fila = $(this).parent().parent();

                fila.hide("slow");
                fila.find('input.estado').val(0);
                fila.find('input').removeAttr('required');
            });

        });

        $("#agregar-detalle").click(function(){
            agregarDetalle();
        });

        $("#agregar-detalle-de-cantidad").on('focusin', function(){
            agregarDetalle('cantidad');
        });

        $("#agregar-detalle-de-articulo").on('focusin', function(){
            agregarDetalle('articulo');
        });

        function agregarDetalle(inputName) {
            var numero_orden = $('table.table tbody tr').length+ 1;
            $("#tabla-detalles-compra").append('<tr>' +
                '<td>' +
                numero_orden +
                '<input type="hidden" class="id" name="detalles['+(numero_orden*-1)+'][id]" value="'+(numero_orden*-1)+'">' +
                '</td>' +
                '<td>' +
                '<input class="form-control cantidad" name="detalles['+(numero_orden*-1)+'][cantidad]" type="number" value="1" min="0" required="required">' +
                '</td>' +
                '<td>' +
                '<input class="form-control buscar-articulo" name="detalles['+(numero_orden*-1)+'][articulo]"  placeholder="Buscar..." type="text" required="required">' +
                '<input class="articulo-id" name="detalles['+(numero_orden*-1)+'][articulo_id]" type="hidden" value="">' +
                '</td>' +
                '<td>' +
                '<a class="btn btn-danger btn-circle borrar-detalle" name="detalles['+(numero_orden*-1)+'][eliminar]" tabindex="99999">' +
                '<i class="fa fa-trash-o" aria-hidden="true"></i>' +
                '</a>' +
                '<input type="hidden" class="estado" name="detalles['+(numero_orden*-1)+'][estado]" value="1">' +
                '</td>' +
                '</tr>');
            if(inputName != null){
                $("input[name='detalles["+(numero_orden*-1)+"]["+inputName+"]']").focus();
            }else{
                $("input[name='detalles["+(numero_orden*-1)+"][articulo]']").focus();
            }
        }

    </script>

    @yield('scripts-dialog-proveedor')
@endsection
