<div class="card">
    <div class="card-header">
        <div class="card-actions">
            <a class="mytooltip" data-action="collapse"><i class="ti-minus"></i><span class="tooltip-content3">Minimizar Ventana</span></a>
            <a class="btn-minimize mytooltip" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
        </div>
        <h4 class="card-title m-b-0">
            Información de la Orden de Compra
        </h4>
    </div>
    <div class="card-body collapse show b-t">
        <div class="row form-group">
            <div class="col-md-4">
                {!! Form::label('proveedor_id', 'Proveedor') !!}
                <div class="row">
                    <div class="col-md-10">
                        <span class="input-icon input-icon-right">
                            <input id="buscar_proveedor" name="buscar_proveedor" class="form-control" placeholder="Buscar..." type="text" required="required" value="{{ isset($ordenCompra->proveedor) ? $ordenCompra->proveedor : '' }}" {{ isset($editar) ? 'disabled' : '' }}>
                        </span>
                        <input id="proveedor_id" name="proveedor_id" type="hidden" value="{{ isset($ordenCompra->proveedor) ? $ordenCompra->proveedor->id : '' }}">
                        {!! $errors->first('proveedor_id', '<p class="text-danger">:message</p>') !!}
                    </div>
                    <div class="col-md-2">
                        <button type="button" class="btn btn-secondary" id="agregar-proveedor"><i class="fa fa-plus"></i> </button>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                {!! Form::label('fecha', 'Fecha') !!}
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    {!! Form::text('fecha', isset($ordenCompra) ? $ordenCompra->fecha->format('d/m/Y') : date('d/m/Y'), ['class' => 'form-control fecha', 'required' => 'required', isset($editar) ? 'readonly' : '']) !!}
                </div>
                {!! $errors->first('fecha', '<p class="text-danger">:message</p>') !!}
            </div>
            <div class="col-md-4">
                {!! Form::label('numero', 'Número') !!}
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-hashtag"></i>
                    </div>
                    {!! Form::text('numero', isset($ordenCompra) ? $ordenCompra->numero : $numero, ['id' =>'numero' ,'class' => 'form-control', 'required' => 'required', 'readonly']) !!}
                </div>
                {!! $errors->first('numero', '<p class="text-danger">:message</p>') !!}
            </div>
        </div>
    </div>
</div>

<div class="card">
    <div class="card-header">
        <div class="card-actions">
            <a class="mytooltip" data-action="collapse"><i class="ti-minus"></i><span class="tooltip-content3">Minimizar Ventana</span></a>
            <a class="btn-minimize mytooltip" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
        </div>
        <h4 class="card-title m-b-0">
            Detalle de la Orden de Compra
        </h4>
    </div>
    <div class="card-body collapse show b-t">
        <div class="table-responsive">
            <table id="tabla-detalles-compra" class="table color-table info-table form-material">
                <thead>
                    <tr>
                        <th class="text-center">#</th>
                        <th class="text-center" style="width:20%;">Cantidad</th>
                        <th style="width:70%;">Artículo</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                @if (isset($ordenCompra))
                    @foreach($ordenCompra->detalles as $detalle)
                        <tr>
                            <td>
                                {{ $loop->index + 1 }}
                                <input type="hidden" class="id" name="detalles[{{ $detalle->id }}][id]" value="{{ $detalle->id }}">
                            </td>
                            <td>{!! Form::number('detalles['.$detalle->id.'][cantidad]', number_format($detalle->cantidad,0), ['class' => 'form-control cantidad', 'step' => '1', 'min' => '0']) !!}</td>
                            <td>
                                <input type="text" class="form-control buscar-articulo" name="detalles[{{ $detalle->id }}][articulo]" placeholder="{{ trans('strings.backend.general.search_placeholder') }}" value="{{ $detalle }}" readonly>
                                <input type="hidden" class="articulo-id" name="detalles[{{ $detalle->id }}][articulo_id]" value="{{ $detalle->articulo_id }}">
                            </td>
                            <td>
                                <a href="javascript:void(0)" class="btn btn-danger btn-circle borrar-detalle" name="detalles[{{ $detalle->id }}][eliminar]">
                                    <i class="fa fa-trash-o" aria-hidden="true"></i>
                                </a>
                                <input type="hidden" class="estado" name="detalles[{{ $detalle->id }}][estado]" value="1">
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
                <tfoot>
                    <tr>
                        <td>
                            ...
                        </td>
                        <td>{!! Form::number('', '', ['class' => 'form-control', 'id' => 'agregar-detalle-de-cantidad']) !!}</td>
                        <td>
                            <input type="text" class="form-control" id="agregar-detalle-de-articulo" placeholder="Nuevo Detalle...">
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <th colspan="4">
                            <a href="javascript:void(0)" class="btn btn-info btn-sm" id="agregar-detalle">
                                <i class="fa fa-plus" aria-hidden="true"></i> Agregar Detalle
                            </a>
                        </th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>

<div class="card">
    <div class="card-body">
        <div class="form-group">
            <input type="checkbox" id="enviar_mail" name="enviar_mail" class="filled-in chk-col-cyan" checked />
            <label for="actualizar_stocks"><strong>Enviar Orden de Compra por Email al Proveedor</strong></label>

            {{ Form::submit(isset($submitButtonText) ? $submitButtonText : trans('buttons.general.save'), ['class' => 'btn btn-themecolor btn-lg pull-right']) }}
        </div>
    </div>
</div>

<div class="card">
    <div class="card-body">
        <div class="row form-group">
            <div class="col-md-12">
                {!! Form::label('descripcion', 'Comentario') !!}
                {!! Form::textarea('descripcion', null, ['class' => 'form-control']) !!}
                {!! $errors->first('descripcion', '<p class="text-danger">:message</p>') !!}
            </div>
        </div>
    </div>
</div>

@include('frontend.personas.proveedor.proveedor_dialog')