@extends('frontend.layouts.app')

@section ('title', trans('labels.frontend.ordenpago_titulo')." - ".trans('buttons.general.crud.show'))

@section('content')
    <div class="row page-titles">
        <div class="col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('labels.frontend.ordenpago_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item"><a href="{{url('/compras/orden-pago')}}">{{ trans('labels.frontend.ordenpago_titulo') }}</a></li>
                <li class="breadcrumb-item">{{ trans('buttons.general.crud.show') }} {{ trans('labels.frontend.ordenpago') }}</li>
                <li class="breadcrumb-item active">Nº {{ $orden->numero }} - {{ $orden->proveedor }}</li>
            </ol>
        </div>
        <div class="col-md-4">
            <div class="pull-right">
                <a href="{{ url('/compras/orden-pago') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans('buttons.general.crud.back') }}</span>
                </a>
                <a href="javascript:void(0)" title="{{trans("buttons.general.crud.delete")." ".trans("labels.frontend.ordenpago")}}" class="btn btn-danger delete">
                    <i class="fa fa-trash" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans("buttons.general.crud.delete") }}</span>
                </a>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            <div class="card-actions">
                <a class="" data-action="collapse"><i class="ti-minus"></i></a>
                <a class="btn-minimize" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
                <a class="btn-close" data-action="close"><i class="ti-close"></i></a>
            </div>
            <h4 class="card-title m-b-0">
                Información de la Orden
            </h4>
        </div>
        <div class="card-body collapse show b-t">
            <div class="table-responsive">
                <table class="table table-borderless">
                    <tbody>
                    <tr>
                        <th>Proveedor</th><td>{{ $orden->proveedor }}</td>
                        <th>Número</th><td>{{ $orden->numero }}</td>
                    </tr>
                    <tr>
                        <th>Fecha</th><td>{{ $orden->fecha->format('d/m/Y H:i') }}</td>
                        <th>Moneda</th><td>{{ $orden->moneda }}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            <div class="card-actions">
                <a class="" data-action="collapse"><i class="ti-minus"></i></a>
                <a class="btn-minimize" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
                <a class="btn-close" data-action="close"><i class="ti-close"></i></a>
            </div>
            <h4 class="card-title m-b-0">
                Detalles de la Orden
            </h4>
        </div>
        <div class="card-body collapse show b-t">
            <div class="table-responsive">
                <table class="table color-table muted-table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>
                            Nº
                        </th>
                        <th>
                            Descripción
                        </th>
                        <th>
                            Monto
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!$orden->detalles->isEmpty())
                        @foreach($orden->detalles as $detalle)
                            <tr>
                                <td>
                                    {{ $loop->index + 1 }}
                                </td>
                                <td>
                                    @if($detalle->cuentaCorriente)
                                        {{ $detalle->descripcion }} - Comprobante Nº {{ $detalle->cuentaCorriente->detalle }}
                                    @else
                                        Comprobante Eliminado
                                    @endif
                                </td>
                                <td>
                                    <b>$ {{ number_format($detalle->monto, 2) }}</b>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                    <tfoot>
                    <tr>
                        <td></td>
                        <td>Total</td>
                        <td><b class="green">{{ $orden->moneda->signo }} {{ $orden->importe }}</b></td>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            <div class="card-actions">
                <a class="" data-action="collapse"><i class="ti-minus"></i></a>
                <a class="btn-minimize" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
                <a class="btn-close" data-action="close"><i class="ti-close"></i></a>
            </div>
            <h4 class="card-title m-b-0">
                Pago
            </h4>
        </div>
        <div class="card-body collapse show b-t">
            <div class="table-responsive">
                <table class="table color-table muted-table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>
                                Nº
                            </th>
                            <th>
                                Descripción
                            </th>
                            <th>
                                Monto
                            </th>
                            <th>
                                Interes
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(!$orden->detallesPagos->isEmpty())
                            @foreach($orden->detallesPagos as $detallePago)
                                <tr>
                                    <td>
                                        {{ $loop->index + 1 }}
                                    </td>
                                    <td>
                                        {{ $detallePago->descripcion }}
                                    </td>
                                    <td>
                                        <b class="text-success">{{ $orden->moneda->signo }} {{ number_format($detallePago->monto , 2) }}</b>
                                    </td>
                                    <td>
                                        <b class="text-success">{{ $orden->moneda->signo }} {{ number_format($detallePago->interes, 2) }}</b>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                    <tfoot>
                        <tr>
                            <td></td>
                            <td></td>
                            <td>
                                Total Cobro
                            </td>
                            <td>
                                <b class="text-success">{{ $orden->moneda->signo }} {{ number_format($orden->total_pago, 2) }}</b>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>

@endsection

@section('after-scripts')
    <script>
        $("body").on("click", ".delete", function () {
            swal({
                title: "{{trans('buttons.general.crud.delete').' '.trans('labels.frontend.ordenpago')}}",
                text: "¿Realmente desea eliminar este registro?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#ff8726',
                confirmButtonText: "Eliminar"
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        type: 'POST',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: '{{ url('/compras/orden-pago/'.$orden->id) }}',
                        data: {"_method" : 'DELETE'},
                        success: function (msg) {
                            swal("Eliminado!", "El registro ha sido correctamente eliminado.", "success").then((result) => { window.location.href = '{{url('/compras/orden-pago')}}'; });
                        }
                    });
                }
            });
        });
    </script>
@endsection