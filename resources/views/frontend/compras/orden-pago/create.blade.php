@extends('frontend.layouts.app')

@section ('title', trans('labels.frontend.ordenpago_titulo')." - ".trans('buttons.general.crud.create'))

@section('after-styles')
    {{ Html::style("css/frontend/plugins/jquery/jquery-ui.min.css") }}
@endsection

@section('content')
    <div class="row page-titles">
        <div class="col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('labels.frontend.ordenpago_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item"><a href="{{url('/compras/orden-pago')}}">{{ trans('labels.frontend.ordenpago_titulo') }}</a></li>
                <li class="breadcrumb-item active">{{ trans('buttons.general.crud.create') }} {{ trans('labels.frontend.ordenpago') }}</li>
            </ol>
        </div>
        <div class="col-4">
            <div class="pull-right">
                <a href="{{ url('/compras/orden-pago') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans('buttons.general.crud.back') }}</span>
                </a>
            </div>
        </div>
    </div>

    @tips($_SERVER['REQUEST_URI'])

    @include('includes.partials.messages')

    {!! Form::open(['url' => '/compras/orden-pago', 'class' => 'form-horizontal', 'files' => true]) !!}
        @include ('frontend.compras.orden-pago.form')
    {!! Form::close() !!}

@endsection

@section('after-scripts')
    {{ Html::script("js/frontend/plugins/jquery/jquery-ui.min.js") }}
    {{ Html::script("js/frontend/plugins/jquery/datepicker-es.js") }}
    {{ Html::script("js/frontend/plugins/maskedinput/jquery.maskedinput.min.js") }}

    <script>
        $( document ).ready(function() {
            $('input.fecha').datepicker({
                firstDay: 0,
                dateFormat: 'dd/mm/yy'
            }).mask("99/99/9999", {placeholder: "dd/mm/aaaa"});
            $('input#numero').mask("99999999", {placeholder: "########"});

            calcularTotalOrdenPago();
        });

        $(document).on('click', '.borrar-detalle', function () {
            inputNombre = this.attributes['name'].value;

            $('.borrar-detalle[name="' + inputNombre + '"]').parent().parent().hide("slow");
            $('.borrar-detalle[name="' + inputNombre + '"]').parent().find('input.estado').val(0);
            $('.borrar-detalle[name="' + inputNombre + '"]').parent().parent().find('td input.detalle-orden-pago-monto').val(0);

            refrescarTotales();
        });

        $("#total").on('change', function (){
            refrescarTotales();
        });

        function calcularTotalOrdenPago(){
            totalOrdenPago = 0;
            detallesOrdenesPagos = $('input.detalle-orden-pago-monto');
            for (var i = 0; i < detallesOrdenesPagos.length; i++) {
                totalOrdenPago += parseFloat(detallesOrdenesPagos[i].value);
            }
            $('input#total').val(totalOrdenPago.toFixed(2));
            refrescarTotales();
        }

        function refrescarTotales() {
            actualizarMontoTotalPago();
            $('#diferencia').html('$ ' + calcularMontoPago() * -1);
        }

        function actualizarMontoTotalPago() {
            totalPagos = 0;
            totalInteres = 0;
            montosPagosAgregados = $('input[name^="detalles_pago"].monto');
            for (var i = 0; i < montosPagosAgregados.length; i++) {
                totalPagos += parseFloat(montosPagosAgregados[i].value);
            }
            interesesPagosAgregados = $('input[name^="detalles_pago"].interes');
            for (var i = 0; i < interesesPagosAgregados.length; i++) {
                totalInteres += parseFloat(interesesPagosAgregados[i].value);
            }
            $('input#total-pago').val((totalPagos + totalInteres).toFixed(2));
            $('#total-final').html('$ ' + (totalPagos + totalInteres).toFixed(2));
        }

    </script>

    @yield('scripts-pagos')
@endsection