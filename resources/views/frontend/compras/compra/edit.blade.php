@extends('frontend.layouts.app')

@section ('title', trans('labels.frontend.compra_titulo')." - ".trans('buttons.general.crud.edit'))

@section('content')
    <div class="main-content">
        <div class="main-content-inner">
            @include ('frontend.includes.breadcrumbs')
            <div class="page-header">
                <h1>
                    {{ trans('labels.frontend.compra_titulo') }}
                    <small>
                        <i class="ace-icon fa fa-angle-double-right"></i>
                        <small>{{ trans('buttons.general.crud.edit') }} Compra </small>
                        <small><i class="ace-icon fa fa-angle-double-right"></i></small>
                        <small>{{ $compra }}</small>
                    </small>

                    <div class="box-tools pull-right">
                        <a href="{{ url('/compras/compra') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> {{ trans('buttons.general.crud.back') }}
                        </a>
                    </div>
                </h1>
            </div>

            <div class="row">
                <div class="col-xs-12">

                    @include('includes.partials.messages')

                    {!! Form::model($compra, [
                        'method' => 'PATCH',
                        'url' => ['/compras/compra', $compra->id],
                        'class' => 'form-horizontal',
                        'files' => true
                    ]) !!}

                        @include ('frontend.compras.compra.form', ['submitButtonText' => trans("buttons.general.crud.update")])

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('after-scripts')
    {{ Html::script("js/frontend/plugin/jquery/jquery-ui.min.js") }}
    {{ Html::script("js/frontend/plugin/jquery/datepicker-es.js") }}
    {{ Html::script("js/frontend/plugin/maskedinput/jquery.maskedinput.min.js") }}

    <script>
        $( document ).ready(function() {
            $('input.fecha').datepicker({
                firstDay: 0,
                dateFormat: 'dd/mm/yy'
            }).mask("99/99/9999", {placeholder: "dd/mm/aaaa"});
            $('input#numero').mask("99999999",{placeholder:"########"});

            $(document).on("keydown.autocomplete",".buscar-articulo",function(e){
                $(this).autocomplete({
                    source:'{{ url('/articulos/articulo/obtenerArticuloConPorcentajeIva') }}',
                    minlength:1,
                    autoFocus:true,
                    select:function(e,ui) {
                        inputNombre = this.attributes['name'].value;
                        $('.buscar-articulo[name="' + inputNombre + '"]').val(ui.item.value);
                        $('.buscar-articulo[name="' + inputNombre + '"]').parent().find('input.articulo-id').val(ui.item.id);
                        $('.buscar-articulo[name="' + inputNombre + '"]').parent().parent().find('td select.iva').val(ui.item.porcentaje_iva).change();
                    }
                });
            });

            $(document).on('click','.borrar-detalle', function() {
                inputNombre = this.attributes['name'].value;
                idDetalle = parseInt($('.borrar-detalle[name="' + inputNombre + '"]').parent().parent().find('td input.id').val());

                $('.borrar-detalle[name="' + inputNombre + '"]').parent().parent().hide("slow");
                $('.borrar-detalle[name="' + inputNombre + '"]').parent().find('input.estado').val(0);
                $('.borrar-detalle[name="' + inputNombre + '"]').parent().parent().find('td input.subtotal-neto').val(0);
                $('.borrar-detalle[name="' + inputNombre + '"]').parent().parent().find('td input.subtotal-bruto').val(0);

                refrescarTotales();
            });

            calcularSubtotales();
            calcularImporteBruto();
            calcularPorcentajeImpuesto();
        });

        $("#agregar-detalle").click(function(){
            agregarDetalle();
        });

        $("#agregar-detalle-de-cantidad").on('focusin', function(){
            agregarDetalle('cantidad');
        });

        $("#agregar-detalle-de-articulo").on('focusin', function(){
            agregarDetalle('articulo');
        });

        function agregarDetalle(inputName) {
            var numero_orden = $('table.table tbody tr').length+ 1;
            $("#tabla-detalles-compra").append('<tr>' +
                '<td>' +
                numero_orden +
                '<input type="hidden" class="id" name="detalles['+(numero_orden*-1)+'][id]" value="'+(numero_orden*-1)+'">' +
                '</td>' +
                '<td>' +
                '<input class="form-control cantidad" name="detalles['+(numero_orden*-1)+'][cantidad]" type="number" value="1" min="0">' +
                '</td>' +
                '<td>' +
                '<input class="form-control buscar-articulo" name="detalles['+(numero_orden*-1)+'][articulo]"  placeholder="Buscar..." type="text">' +
                '<input class="articulo-id" name="detalles['+(numero_orden*-1)+'][articulo_id]" type="hidden" value="">' +
                '</td>' +
                '<td>' +
                '<div class="input-group">' +
                '<div class="input-group-addon"><i class="fa fa-usd"></i></div>' +
                '<input class="form-control precio" name="detalles['+(numero_orden*-1)+'][precio]" type="number" step="0.01">' +
                '</div>' +
                '</td>' +
                '<td>' +
                '<div class="input-group">' +
                '<div class="input-group-addon"><i class="fa fa-usd"></i></div>' +
                '<input class="form-control subtotal-neto" name="detalles['+(numero_orden*-1)+'][total_neto]" type="number" step="0.01" readonly tabindex="99999">' +
                '</div>' +
                '</td>' +
                '<td>' +
                '<div class="input-group">' +
                '<div class="input-group-addon"><i class="fa fa-percent"></i></div>' +
                '<select class="form-control iva" name="detalles['+(numero_orden*-1)+'][porcentaje_iva]">' +
                '<option value="" selected="selected">Seleccione una opción...</option>' +
                '<option value="0">0</option>' +
                '<option value="3">2.5</option>' +
                '<option value="4">5</option>' +
                '<option value="1">10.5</option>' +
                '<option value="2">21</option>' +
                '<option value="5">27</option>' +
                '</select>' +
                '</div>' +
                '</td>' +
                '<td>' +
                '<div class="input-group">' +
                '<div class="input-group-addon"><i class="fa fa-usd"></i></div>' +
                '<input class="form-control subtotal-bruto" name="detalles['+(numero_orden*-1)+'][total_bruto]" type="number" step="0.01" readonly tabindex="99999">' +
                '</div>' +
                '</td>' +
                '<td>' +
                '<a class="btn btn-danger btn-sm borrar-detalle" name="detalles['+(numero_orden*-1)+'][eliminar]" tabindex="99999">' +
                '<i class="fa fa-trash-o" aria-hidden="true"></i>' +
                '</a>' +
                '<input type="hidden" class="estado" name="detalles['+(numero_orden*-1)+'][estado]" value="1">' +
                '</td>' +
                '</tr>');
            if(inputName != null){
                $("input[name='detalles["+(numero_orden*-1)+"]["+inputName+"]']").focus();
            }else{
                $("input[name='detalles["+(numero_orden*-1)+"][articulo]']").focus();
            }
            calcularSubtotales();
        }

        $("#porcentaje-impuesto").on('change', function (){
            calcularImporteImpuesto();
            calcularTotal();
        });

        $("#importe-impuesto").on('change', function (){
            calcularPorcentajeImpuesto();
            calcularTotal();
        });

        function refrescarTotales() {
            calcularImporteNeto();
            calcularImporteBruto();
            calcularImporteIva();
            calcularImporteImpuesto();
            calcularTotal();
        }

        function calcularSubtotales(){
            var $tablaTodasFilas = $("#tabla-detalles-compra tbody tr");
            $tablaTodasFilas.each(function (index) {
                var $tablaFila = $(this);

                $tablaFila.find('.cantidad ,.precio, .iva, .buscar-articulo').on('change', function () {
                    var cantidad = $tablaFila.find('.cantidad').val();
                    var precio = $tablaFila.find('.precio').val();
                    var iva = $tablaFila.find('.iva').find(":selected").text();

                    var subtotalNeto = parseInt(cantidad, 10) * parseFloat(precio);
                    var subtotalBruto = subtotalNeto + subtotalNeto * parseFloat(iva) / 100;

                    if (!isNaN(subtotalNeto)) {
                        $tablaFila.find('.subtotal-neto').val(subtotalNeto.toFixed(2));
                    }
                    if (!isNaN(subtotalBruto)) {
                        $tablaFila.find('.subtotal-bruto').val(subtotalBruto.toFixed(2));
                    }else{
                        $tablaFila.find('.subtotal-bruto').val(subtotalNeto.toFixed(2));
                    }
                    refrescarTotales();
                });
            });
        }

        function calcularImporteNeto() {
            var importeNeto = 0;
            $('.subtotal-neto').each(function () {
                var auxImporteNeto = parseFloat($(this).val());
                importeNeto += isNaN(auxImporteNeto) ? 0 : auxImporteNeto;
            });
            $('#importe-neto').val(importeNeto.toFixed(2));
        }

        function calcularImporteIva(){
            var importeNeto = $('#importe-neto').val();
            var importeBruto = $('#importe-bruto').val();
            var importeIva = parseFloat(importeBruto) - parseFloat(importeNeto);
            if (!isNaN(importeIva)) {
                $('#importe-iva').val(importeIva.toFixed(2));
            }else{
                $('#importe-iva').val('0.0');
            }
        }

        function calcularImporteBruto() {
            var importeBruto = 0;
            $('.subtotal-bruto').each(function () {
                var auxImporteBruto = parseFloat($(this).val());
                importeBruto += isNaN(auxImporteBruto) ? 0 : auxImporteBruto;
            });
            $('#importe-bruto').val(importeBruto.toFixed(2));
        }

        function calcularImporteImpuesto(){
            var importeBruto = $('#importe-bruto').val();
            var procentajeImpuesto = $('#porcentaje-impuesto').val();
            var importeImpuesto = parseFloat(importeBruto) * parseFloat(procentajeImpuesto) / 100;
            if (!isNaN(importeImpuesto)) {
                $('#importe-impuesto').val(importeImpuesto.toFixed(2));
            }else{
                $('#importe-impuesto').val('0.0');
            }
        }

        function calcularPorcentajeImpuesto(){
            var importeBruto = $('#importe-bruto').val();
            var importeImpuesto = $('#importe-impuesto').val();
            var procentajeImpuesto = parseFloat(importeImpuesto) * 100 / parseFloat(importeBruto);
            if (!isNaN(procentajeImpuesto)) {
                $('#porcentaje-impuesto').val(procentajeImpuesto.toFixed(2));
            }else{
                $('#porcentaje-impuesto').val('0.0');
            }
        }

        function calcularTotal(){
            var importeBruto = $('#importe-bruto').val();
            var importeImpuesto = $('#importe-impuesto').val();
            var total = parseFloat(importeBruto) + parseFloat(importeImpuesto);
            if (!isNaN(total)) {
                $('#total').val(total.toFixed(2));
            }else{
                $('#total').val('0.0');
            }
            actualizarResto();
        }
    </script>
    @yield('scripts-pagos')
@endsection
