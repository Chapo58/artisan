@extends('frontend.layouts.app')

@section ('title', trans('labels.frontend.compra_titulo')." - ".trans('buttons.general.crud.create'))

@section('after-styles')
    {{ Html::style("css/frontend/plugins/jquery/jquery-ui.min.css") }}
@endsection

@section('content')
    <div class="row page-titles">
        <div class="col-6 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('labels.frontend.compra_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item"><a href="{{url('/compras/compra')}}">{{ trans('labels.frontend.compra_titulo') }}</a></li>
                <li class="breadcrumb-item active">{{ trans('buttons.general.crud.create') }} {{ trans('labels.frontend.compra') }}</li>
            </ol>
        </div>
        <div class="col-6">
            <div class="pull-right">
                <a href="{{ url('/compras/compra') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans('buttons.general.crud.back') }}</span>
                </a>
            </div>
        </div>
    </div>

    @tips($_SERVER['REQUEST_URI'])

    @include('includes.partials.messages')

    {!! Form::open(['url' => '/compras/compra', 'class' => 'form-horizontal', 'files' => true]) !!}
        @include ('frontend.compras.compra.form')
    {!! Form::close() !!}

@endsection

@section('after-scripts')
    {{ Html::script("js/frontend/plugins/jquery/jquery-ui.min.js") }}
    {{ Html::script("js/frontend/plugins/jquery/datepicker-es.js") }}
    {{ Html::script("js/frontend/plugins/maskedinput/jquery.maskedinput.min.js") }}

    <script>
        $( document ).ready(function() {
            $('input.fecha').datepicker({
                firstDay: 0,
                dateFormat: 'dd/mm/yy'
            }).mask("99/99/9999", {placeholder: "dd/mm/aaaa"});
            $('input#numero').mask("99999999",{placeholder:"########"});

            $(document).on("keydown.autocomplete", "#buscar_proveedor", function (e) {
                if (e.keyCode == 13) {
                    e.preventDefault();
                }else {
                    $(this).autocomplete({
                        source: '{{ url('/personas/proveedor/obtenerProveedorParaCompra') }}',
                        minlength: 1,
                        autoFocus: true,
                        select: function (e, ui) {
                            $('input#buscar_proveedor').val(ui.item.value);
                            $('input#proveedor_id').val(ui.item.id);
                            if (ui.item.tipo_comprobante_id != null) {
                                $('select#tipo_comprobante_id').val(ui.item.tipo_comprobante_id);
                            }
                        },
                        response: function(e, ui) {
                            if (ui.content.length == 1){ // Si solo me devuelve un proveedor, lo autocargo
                                ui.item = ui.content[0];
                                $('input#buscar_proveedor').val(ui.item.value).blur();
                                $('input#proveedor_id').val(ui.item.id);
                                if (ui.item.tipo_comprobante_id != null) {
                                    $('select#tipo_comprobante_id').val(ui.item.tipo_comprobante_id);
                                }
                            }
                        }
                    });
                }
            });

            $('select#moneda_id').on('change', function () {
                buscarCotizacion();
            });

            function buscarCotizacion() {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $( "input[name*='_token']" ).val()
                    }
                });
                var moneda_id = $('#moneda_id option:selected').val();
                $.ajax({
                    type: "POST",
                    url: '{{ url("/configuraciones/moneda/obtenerCotizacion") }}',
                    data: {moneda_id: moneda_id},
                    success: function( msg ) {
                        $("#cotizacion").val(msg);
                    }
                });
            }

            $(document).on("keydown.autocomplete",".buscar-articulo",function(e){
                $(this).autocomplete({
                    source:'{{ url('/articulos/articulo/obtenerArticuloConPorcentajeIva') }}',
                    minlength:1,
                    autoFocus:true,
                    select:function(e,ui) {
                        var fila = $(this).parent().parent();

                        $(this).val(ui.item.value);
                        fila.find('input.articulo-id').val(ui.item.id);
                        fila.find('td select.iva').val(ui.item.porcentaje_iva).change();
                    }
                });
            });

            $(document).on('click','.borrar-detalle', function() {
                var fila = $(this).parent().parent();

                fila.hide("slow");
                fila.find('input.estado').val(0);
                fila.find('td input.subtotal-neto').val(0);
                fila.find('td input.subtotal-bruto').val(0);
                fila.find('input').removeAttr('required');
                fila.find('select').removeAttr('required');

                refrescarTotales();
            });

            calcularSubtotales();
            calcularPorcentajeImpuesto();
            refrescarTotales();
        });

        $("#agregar-detalle").click(function(){
            agregarDetalle();
        });

        $("#agregar-detalle-de-cantidad").on('focusin', function(){
            agregarDetalle('cantidad');
        });

        $("#agregar-detalle-de-articulo").on('focusin', function(){
            agregarDetalle('articulo');
        });

        function agregarDetalle(inputName) {
            var numero_orden = $('table.table tbody tr').length+ 1;
            $("#tabla-detalles-compra").append('<tr>' +
                '<td>' +
                numero_orden +
                '<input type="hidden" class="id" name="detalles['+(numero_orden*-1)+'][id]" value="'+(numero_orden*-1)+'">' +
                '</td>' +
                '<td>' +
                '<input class="form-control cantidad text-center" name="detalles['+(numero_orden*-1)+'][cantidad]" type="number" value="1" min="0" required="required">' +
                '</td>' +
                '<td>' +
                '<input class="form-control buscar-articulo" name="detalles['+(numero_orden*-1)+'][articulo]"  placeholder="Buscar..." type="text" required="required">' +
                '<input class="articulo-id" name="detalles['+(numero_orden*-1)+'][articulo_id]" type="hidden" value="">' +
                '</td>' +
                '<td>' +
                '<div class="input-group">' +
                '<div class="left-icon">$</div>' +
                '<input class="form-control precio text-right" name="detalles['+(numero_orden*-1)+'][precio_neto]" type="number" step="0.01" required="required">' +
                '</div>' +
                '</td>' +
                '<td>' +
                '<div class="input-group">' +
                '<div class="left-icon">$</div>' +
                '<input class="form-control subtotal-neto text-right" name="detalles['+(numero_orden*-1)+'][total_neto]" type="number" step="0.01" readonly tabindex="99999">' +
                '</div>' +
                '</td>' +
                '<td>' +
                '<div class="input-group">' +
                '<div class="left-icon">%</div>' +
                '<select class="form-control iva text-right" name="detalles['+(numero_orden*-1)+'][porcentaje_iva]" required="required">' +
                '<option value="" selected="selected">Seleccionar...</option>' +
                '<option value="0">0</option>' +
                '<option value="3">2.5</option>' +
                '<option value="4">5</option>' +
                '<option value="1">10.5</option>' +
                '<option value="2">21</option>' +
                '<option value="5">27</option>' +
                '</select>' +
                '</div>' +
                '</td>' +
                '<td>' +
                '<div class="input-group">' +
                '<div class="left-icon">$</div>' +
                '<input class="form-control subtotal-bruto text-right" name="detalles['+(numero_orden*-1)+'][total_bruto]" type="number" step="0.01" readonly tabindex="99999">' +
                '</div>' +
                '</td>' +
                '<td>' +
                '<a class="btn btn-danger btn-circle borrar-detalle" name="detalles['+(numero_orden*-1)+'][eliminar]" tabindex="99999">' +
                '<i class="fa fa-trash-o" aria-hidden="true"></i>' +
                '</a>' +
                '<input type="hidden" class="estado" name="detalles['+(numero_orden*-1)+'][estado]" value="1">' +
                '</td>' +
                '</tr>');
            if(inputName != null){
                $("input[name='detalles["+(numero_orden*-1)+"]["+inputName+"]']").focus();
            }else{
                $("input[name='detalles["+(numero_orden*-1)+"][articulo]']").focus();
            }
            calcularSubtotales();
        }

        $("#porcentaje-impuesto").on('change', function (){
            calcularImporteImpuesto();
            refrescarTotales();
        });

        $("#importe-impuesto").on('change', function (){
            calcularPorcentajeImpuesto();
            refrescarTotales();
        });

        $("#importe-impuesto-interno").on('change', function (){
            refrescarTotales();
        });

        $("#importe-rentecion").on('change', function (){
            refrescarTotales();
        });

        $("#importe-concepto").on('change', function (){
            refrescarTotales();
        });

        function refrescarTotales() {
            var primerProducto = $("#tabla-detalles-compra tbody tr:visible").first().find('.precio').val();
            if(primerProducto){
                $.ajax({
                    type: "POST",
                    url: '{{ url("/compras/calcularTotales") }}',
                    data: $('form').serialize(),
                    success: function( resultados ) {
                        $('#importe-neto').val(resultados['netos']['total']);
                        $('#importe-iva').val(resultados['ivas']['total']);
                        $('#importe-bruto').val(resultados['bruto']);
                        $('#total').val(resultados['total']);

                        $('#totalGrande').html('$ ' + resultados['total']);
                        actualizarMontoTotalPago();
                        $('#diferencia').html('$ ' + calcularMontoPago() * -1);
                    }
                });
            }
        }

        function calcularSubtotales(){
            var $tablaTodasFilas = $("#tabla-detalles-compra tbody tr");
            $tablaTodasFilas.each(function (index) {
                var $tablaFila = $(this);

                $tablaFila.find('.cantidad ,.precio, .iva, .buscar-articulo').on('change', function () {
                    var cantidad = $tablaFila.find('.cantidad').val();
                    var precio = $tablaFila.find('.precio').val();
                    var iva = $tablaFila.find('.iva').find(":selected").text();

                    var subtotalNeto = parseInt(cantidad, 10) * parseFloat(precio);
                    var subtotalBruto = subtotalNeto + subtotalNeto * parseFloat(iva) / 100;

                    if (!isNaN(subtotalNeto)) {
                        $tablaFila.find('.subtotal-neto').val(subtotalNeto.toFixed(2));
                    }
                    if (!isNaN(subtotalBruto)) {
                        $tablaFila.find('.subtotal-bruto').val(subtotalBruto.toFixed(2));
                    }
                    refrescarTotales();
                });
            });
        }

        function calcularImporteImpuesto(){
            var importeBruto = $('#importe-bruto').val();
            var procentajeImpuesto = $('#porcentaje-impuesto').val();
            var importeImpuesto = parseFloat(importeBruto) * parseFloat(procentajeImpuesto) / 100;
            if (!isNaN(importeImpuesto)) {
                $('#importe-impuesto').val(importeImpuesto.toFixed(2));
            }else{
                $('#importe-impuesto').val('0.0');
            }
        }

        function calcularPorcentajeImpuesto(){
            var importeBruto = $('#importe-bruto').val();
            var importeImpuesto = $('#importe-impuesto').val();
            var procentajeImpuesto = parseFloat(importeImpuesto) * 100 / parseFloat(importeBruto);
            if (!isNaN(procentajeImpuesto)) {
                $('#porcentaje-impuesto').val(procentajeImpuesto.toFixed(2));
            }else{
                $('#porcentaje-impuesto').val('0.0');
            }
        }

        function actualizarMontoTotalPago() {
            totalPagos = 0;
            totalInteres = 0;
            montosPagosAgregados = $('input[name^="detalles_pago"].monto');
            for (var i = 0; i < montosPagosAgregados.length; i++) {
                totalPagos += parseFloat(montosPagosAgregados[i].value);
            }
            interesesPagosAgregados = $('input[name^="detalles_pago"].interes');
            for (var i = 0; i < interesesPagosAgregados.length; i++) {
                totalInteres += parseFloat(interesesPagosAgregados[i].value);
            }
            $('input#total-pago').val((totalPagos + totalInteres).toFixed(2));
            $('#total-final').html('$ ' + (totalPagos + totalInteres).toFixed(2));
        }

    </script>

    @yield('scripts-dialog-proveedor')
    @yield('scripts-pagos')
@endsection
