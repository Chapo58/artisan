@extends('frontend.layouts.app')

@section ('title', trans('labels.frontend.compra_titulo')." - ".trans('buttons.general.crud.show'))

@section('content')
    <div class="row page-titles">
        <div class="col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('labels.frontend.compra_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item"><a href="{{url('/compras/compra')}}">{{ trans('labels.frontend.compra_titulo') }}</a></li>
                <li class="breadcrumb-item">{{ trans('buttons.general.crud.show') }} {{ trans('labels.frontend.compra') }}</li>
                <li class="breadcrumb-item active">{{ $compra }}</li>
            </ol>
        </div>
        <div class="col-4">
            <div class="pull-right">
                <a href="{{ url('/compras/compra') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans('buttons.general.crud.back') }}</span>
                </a>
                <a href="javascript:void(0)" title="{{trans("buttons.general.crud.delete")." ".trans("labels.frontend.compra")}}" class="btn btn-danger delete">
                    <i class="fa fa-trash" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans("buttons.general.crud.delete") }}</span>
                </a>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            <div class="card-actions">
                <a class="mytooltip" data-action="collapse"><i class="ti-minus"></i><span class="tooltip-content3">Minimizar Ventana</span></a>
                <a class="btn-minimize mytooltip" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
                <a class="btn-close mytooltip" data-action="close"><i class="ti-close"></i><span class="tooltip-content3">Cerrar Ventana</span></a>
            </div>
            <h4 class="card-title m-b-0">
                Información de la Compra
            </h4>
        </div>
        <div class="card-body collapse show b-t">
            <div class="table-responsive">
                <table class="table table-borderless">
                    <tbody>
                        <tr>
                            <th>Proveedor</th><td><a href="{{url('/personas/proveedor/'.$compra->proveedor->id)}}">{{ $compra->proveedor }}</a></td>
                            <th>Tipo Comprobante</th><td>{{ $tipos_comprobantes[$compra->tipo_comprobante_id] }}</td>
                            <th>Punto Venta</th><td>{{ $compra->punto_venta }}</td>
                            <th>Número</th><td>{{ $compra->numero }}</td>
                        </tr>
                        <tr>
                            <th>Fecha</th><td>{{ $compra->fecha->format('d/m/Y H:i') }}</td>
                            <th>Moneda</th><td>{{ $compra->moneda }}</td>
                            <th>Cotizacíon</th><td><b class="text-success">$ {{ $compra->cotizacion }}</b></td>
                        </tr>
                    </tbody>
                </table>
                @if($compra->comentario)
                    <strong>Comentario: <i>{{ $compra->comentario }}</i></strong>
                @endif
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            <div class="card-actions">
                <a class="mytooltip" data-action="collapse"><i class="ti-minus"></i><span class="tooltip-content3">Minimizar Ventana</span></a>
                <a class="btn-minimize mytooltip" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
                <a class="btn-close mytooltip" data-action="close"><i class="ti-close"></i><span class="tooltip-content3">Cerrar Ventana</span></a>
            </div>
            <h4 class="card-title m-b-0">
                Detalles de la Compra
            </h4>
        </div>
        <div class="card-body collapse show b-t">
            <div class="table-responsive">
                <table class="table color-table muted-table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>
                            Nº
                        </th>
                        <th>
                            Cantidad
                        </th>
                        <th>
                            Artículo
                        </th>
                        <th>
                            Pre/Uni. Neto
                        </th>
                        <th>
                            Neto
                        </th>
                        <th>
                            IVA
                        </th>
                        <th>
                            Bruto
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!$compra->detalles->isEmpty())
                        @foreach($compra->detalles as $detalle)
                            <tr>
                                <td>
                                    {{ $loop->index + 1 }}
                                </td>
                                <td>
                                    {{ number_format($detalle->cantidad , 0) }}
                                </td>
                                <td>
                                    @if(isset($detalle->articulo))
                                        <a href="{{url('/articulos/articulo/'.$detalle->articulo->id)}}">
                                            {{ $detalle->articulo->nombre }}
                                        </a>
                                    @else
                                        Artículo Eliminado
                                    @endif
                                </td>
                                <td>
                                    <b class="text-success">{{ $compra->moneda->signo }} {{ number_format($detalle->precio_neto, 2) }}</b>
                                </td>
                                <td>
                                    <b class="text-success">{{ $compra->moneda->signo }} {{ number_format($detalle->cantidad*$detalle->precio_neto , 2) }}</b>
                                </td>
                                <td>
                                    <b class="text-success"> {{ number_format($porcentajes_iva[$detalle->porcentaje_iva], 2) }} %</b>
                                </td>
                                <td>
                                    <b class="text-success">{{ $compra->moneda->signo }} {{ number_format(($detalle->cantidad * $detalle->precio_neto) * (($porcentajes_iva[$detalle->porcentaje_iva]/100) + 1), 2) }}</b>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
                <table class="table table-borderless">
                    <tbody>
                    <tr>
                        <th class="text-right" colspan="3">Importe Neto</th>
                        <td><b class="text-success">{{ $compra->moneda->signo }} {{ number_format($compra->importe_neto, 2) }}</b></td>
                    </tr>
                    <tr>
                        <th class="text-right" colspan="3">Iva ($)</th><td><b class="text-success">{{ $compra->moneda->signo }} {{ number_format($compra->importe_iva, 2) }}</b></td>
                    </tr>
                    <tr>
                        <th class="text-right">Impuesto/Descuento (%)</th><td><b class="text-success">{{ number_format($compra->impuesto*100/($compra->total - $compra->impuesto), 2) }} %</b></td>
                        <th class="text-right">Impuesto/Descuento ($)</th><td><b class="text-success">{{ $compra->moneda->signo }} {{ number_format($compra->impuesto, 2) }}</b></td>
                    </tr>
                    @if($compra->importe_impuesto_interno != 0)
                        <tr>
                            <th class="text-right" colspan="3">Impuestos Internos ($)</th><td><b class="text-success">{{ $compra->moneda->signo }} {{ number_format($compra->importe_impuesto_interno, 2) }}</b></td>
                        </tr>
                    @endif
                    @if($compra->retencion != 0)
                        <tr>
                            <th class="text-right" colspan="3">Retenciones ($)</th><td><b class="text-success">{{ $compra->moneda->signo }} {{ number_format($compra->retencion, 2) }}</b></td>
                        </tr>
                    @endif
                    @if($compra->concepto_no_gravado != 0)
                        <tr>
                            <th class="text-right" colspan="3">Conceptos No Grabados ($)</th><td><b class="text-success">{{ $compra->moneda->signo }} {{ number_format($compra->concepto_no_gravado, 2) }}</b></td>
                        </tr>
                    @endif
                    <tr>
                        <th class="text-right" colspan="3">Total</th><td><b class="text-success">{{ $compra->moneda->signo }} {{ number_format($compra->total, 2) }}</b></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            <div class="card-actions">
                <a class="mytooltip" data-action="collapse"><i class="ti-minus"></i><span class="tooltip-content3">Minimizar Ventana</span></a>
                <a class="btn-minimize mytooltip" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
                <a class="btn-close mytooltip" data-action="close"><i class="ti-close"></i><span class="tooltip-content3">Cerrar Ventana</span></a>
            </div>
            <h4 class="card-title m-b-0">
                Pago
            </h4>
        </div>
        <div class="card-body collapse show b-t">
            <div class="table-responsive">
                <table class="table color-table muted-table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>
                            Nº
                        </th>
                        <th>
                            Descripción
                        </th>
                        <th>
                            Monto
                        </th>
                        <th>
                            Interes
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!$compra->detallesPagos->isEmpty())
                        @foreach($compra->detallesPagos as $detallePago)
                            <tr>
                                <td>
                                    {{ $loop->index + 1 }}
                                </td>
                                <td>
                                    {{ $detallePago->descripcion }}
                                </td>
                                <td>
                                    <b class="text-success">{{ $compra->moneda->signo }} {{ number_format($detallePago->monto , 2) }}</b>
                                </td>
                                <td>
                                    <b class="text-success">{{ $compra->moneda->signo }} {{ number_format($detallePago->interes, 2) }}</b>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                    <tfoot>
                    <tr>
                        <td></td>
                        <td></td>
                        <td>
                            Total Pago
                        </td>
                        <td>
                            <b class="text-success">{{ $compra->moneda->signo }} {{ number_format($compra->total_pago, 2) }}</b>
                        </td>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('after-scripts')

<script>
    $("body").on("click", ".delete", function () {
        swal({
            title: "{{trans('buttons.general.crud.delete').' '.trans('labels.frontend.compra')}}",
            text: "¿Realmente desea eliminar este registro?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#ff8726',
            confirmButtonText: "Eliminar"
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '{{ url('/compras/compra/'.$compra->id) }}',
                    data: {"_method" : 'DELETE'},
                    success: function (msg) {
                        swal("Eliminado!", "El registro ha sido correctamente eliminado.", "success").then((result) => { window.location.href = '{{url('/compras/compra')}}'; });
                    }
                });
            }
        });
    });
</script>

@endsection