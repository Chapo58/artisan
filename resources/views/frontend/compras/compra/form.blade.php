<div class="card">
    <div class="card-body">
        <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
        <div class="row form-group">
            <div class="col-md-4">
                {!! Form::label('proveedor_id', 'Proveedor') !!}
                <div class="row">
                    <div class="col-md-10">
                        <span class="input-icon input-icon-right">
                            <input id="buscar_proveedor" name="buscar_proveedor" class="form-control" placeholder="Buscar..." type="text" required="required" value="{{ isset($compra->proveedor) ? $compra->proveedor : '' }}" {{ isset($editar) ? 'disabled' : '' }}>
                        </span>
                        <input id="proveedor_id" name="proveedor_id" type="hidden" value="{{ isset($compra->proveedor) ? $compra->proveedor->id : '' }}">
                        {!! $errors->first('proveedor_id', '<p class="text-danger">:message</p>') !!}
                    </div>
                    <div class="col-md-2">
                        <button type="button" class="btn btn-secondary" id="agregar-proveedor"><i class="fa fa-plus"></i> </button>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                {!! Form::label('tipo_comprobante_id', 'Tipo de Comprobante') !!}
                {!! Form::select('tipo_comprobante_id', $tipos_comprobantes, isset($compra->tipo_comprobante_id) ? $compra->tipo_comprobante_id : 0, ['class' => 'form-control', 'required' => 'required', isset($editar) ? 'disabled' : '']) !!}
                {!! $errors->first('tipo_comprobante_id', '<p class="text-danger">:message</p>') !!}
            </div>
            <div class="col-md-4">
                {!! Form::label('numero', 'Punto de Venta # Número') !!}
                <div class="input-group">
                    {!! Form::text('punto_venta', isset($compra->punto_venta) ? $compra->punto_venta : '', ['class' => 'form-control', 'required' => 'required']) !!}
                    <div class="input-group-addon">
                        <i class="fa fa-hashtag"></i>
                    </div>
                    {!! Form::text('numero', isset($compra->numero) ? $compra->numero : '', ['id' =>'numero' ,'class' => 'form-control', 'required' => 'required', isset($editar) ? 'disabled' : '']) !!}
                </div>
                {!! $errors->first('numero', '<p class="text-danger">:message</p>') !!}
            </div>
        </div>
        <div class="row form-group" style="margin-top:-15px;">
            <div class="col-md-6">
                {!! Form::label('fecha', 'Fecha') !!}
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    {!! Form::text('fecha', isset($compra->fecha) ? $compra->fecha->format('d/m/Y H:i') : date('d/m/Y H:i'), ['class' => 'form-control fecha', 'required' => 'required', isset($editar) ? 'disabled' : '']) !!}
                </div>
                {!! $errors->first('fecha', '<p class="text-danger">:message</p>') !!}
            </div>
            <div class="col-md-6">
                {!! Form::label('moneda_id', 'Moneda') !!}
                <div class="row">
                    <div class="col-md-7">
                        {!! Form::select('moneda_id', $monedas, isset($compra->moneda) ? $compra->moneda->id : (isset($monedaPorDefecto) ? $monedaPorDefecto->id : '') , ['class' => 'form-control', 'id' => 'moneda_id', 'required' => 'required', isset($editar) ? 'disabled' : '']) !!}
                        {!! $errors->first('moneda_id', '<p class="text-danger">:message</p>') !!}
                    </div>
                    <div class="col-md-5">
                        {!! Form::number('cotizacion', isset($compra->cotizacion) ? $compra->cotizacion : (isset($monedaPorDefecto) ? $monedaPorDefecto->cotizacion : 1), ['class' => 'form-control', 'step' => '0.01', 'id' => 'cotizacion', isset($editar) ? 'disabled' : '']) !!}
                        {!! $errors->first('cotizacion', '<p class="text-danger">:message</p>') !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <table id="tabla-detalles-compra" class="table color-table info-table form-material table-sm">
                <thead>
                <tr>
                    <th class="text-center">#</th>
                    <th class="text-center" style="width:10%;">Cantidad</th>
                    <th style="width:30%;">Artículo</th>
                    <th class="text-center" style="width:15%;">Pre/Uni. Neto</th>
                    <th class="text-center" style="width:15%;">Neto</th>
                    <th class="text-center" style="width:15%;">IVA</th>
                    <th class="text-center" style="width:15%;">Bruto</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @if (isset($compra))
                    @foreach($compra->detalles as $detalle)
                        <tr>
                            <td>
                                {{ $loop->index + 1 }}
                                <input type="hidden" class="id" name="detalles[{{ $detalle->id }}][id]" value="{{ $detalle->id }}">
                            </td>
                            <td>{!! Form::number('detalles['.$detalle->id.'][cantidad]', number_format($detalle->cantidad,0), ['class' => 'form-control cantidad text-center', 'step' => '1', 'min' => '0']) !!}</td>
                            <td>
                                <input type="text" class="form-control buscar-articulo" name="detalles[{{ $detalle->id }}][articulo]" placeholder="{{ trans('strings.backend.general.search_placeholder') }}" value="{{ (isset($detalle->articulo->codigo)) ? '('.$detalle->articulo->codigo.') '.$detalle->articulo->nombre : $detalle->articulo }}">
                                <input type="hidden" class="articulo-id" name="detalles[{{ $detalle->id }}][articulo_id]" value="{{ $detalle->articulo_id }}">
                            </td>
                            <td>
                                <div class="input-group">
                                    <div class="left-icon">$</div>
                                    {!! Form::number('detalles['.$detalle->id.'][precio_neto]', $detalle->precio_neto, ['class' => 'form-control precio text-right', 'step' => '0.01', 'tabindex' => '9999']) !!}
                                </div>
                            </td>
                            <td>
                                <div class="input-group">
                                    <div class="left-icon">$</div>
                                    {!! Form::number('detalles['.$detalle->id.'][total_neto]', $detalle->cantidad*$detalle->precio_neto, ['class' => 'form-control subtotal-neto text-right', 'step' => '0.01', 'tabindex' => '9999', 'readonly']) !!}
                                </div>
                            </td>
                            <td>
                                <div class="input-group">
                                    <div class="left-icon">$</div>
                                    {!! Form::select('detalles['.$detalle->id.'][porcentaje_iva]', $porcentajes_iva, (isset($detalle->articulo->porcentaje_iva)) ? $detalle->articulo->porcentaje_iva : $detalle->porcentaje_iva, ['class' => 'form-control iva text-right', 'tabindex' => '9999', 'required' => 'required']) !!}
                                </div>
                            </td>
                            <td>
                                <div class="input-group">
                                    <div class="left-icon">%</div>
                                    {!! Form::number('detalles['.$detalle->id.'][total_bruto]', ($detalle->cantidad*$detalle->precio_neto)+($detalle->cantidad*$detalle->precio_neto*$porcentajes_iva[(int)$detalle->porcentaje_iva]/100), ['class' => 'form-control subtotal-bruto text-right', 'readonly', 'step' => '0.01']) !!}
                                </div>
                            </td>
                            <td>
                                <a href="javascript:void(0)" class="btn btn-danger btn-circle borrar-detalle" name="detalles[{{ $detalle->id }}][eliminar]">
                                    <i class="fa fa-trash-o" aria-hidden="true"></i>
                                </a>
                                <input type="hidden" class="estado" name="detalles[{{ $detalle->id }}][estado]" value="1">
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
                <tfoot>
                <tr>
                    <td>
                        ...
                    </td>
                    <td>{!! Form::number('', '', ['class' => 'form-control', 'id' => 'agregar-detalle-de-cantidad']) !!}</td>
                    <td>
                        <input type="text" class="form-control" id="agregar-detalle-de-articulo" placeholder="Nuevo Detalle...">
                    </td>
                    <td>
                        <div class="input-group">
                            <div class="left-icon">$</div>
                            {!! Form::number('', null, ['class' => 'form-control', 'readonly', 'tabindex' => '9999']) !!}
                        </div>
                    </td>
                    <td>
                        <div class="input-group">
                            <div class="left-icon">$</div>
                            {!! Form::number('', null, ['class' => 'form-control', 'readonly', 'tabindex' => '9999']) !!}
                        </div>
                    </td>
                    <td>
                        <div class="input-group">
                            <div class="left-icon">%</div>
                            {!! Form::select('', ['...'], null, ['class' => 'form-control', 'readonly', 'tabindex' => '9999', 'disabled' => 'disabled']) !!}
                        </div>
                    </td>
                    <td>
                        <div class="input-group">
                            <div class="left-icon">$</div>
                            {!! Form::number('', null, ['class' => 'form-control', 'readonly', 'tabindex' => '9999']) !!}
                        </div>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <th colspan="3">
                        <a href="javascript:void(0)" class="btn btn-info btn-sm" id="agregar-detalle">
                            <i class="fa fa-plus" aria-hidden="true"></i> Agregar Detalle
                        </a>
                    </th>
                    <th><span class="pull-right">Subtotales</span></th>
                    <th>
                        <div class="input-group">
                            <div class="left-icon">$</div>
                            {!! Form::number('importe_neto', null, ['id' => 'importe-neto', 'class' => 'form-control text-right', 'readonly']) !!}
                        </div>
                    </th>
                    <th>
                        <div class="input-group">
                            <div class="left-icon">$</div>
                            {!! Form::number('importe_iva', null, ['id' => 'importe-iva', 'class' => 'form-control text-right', 'step' => '0.01', 'readonly']) !!}
                        </div>
                    </th>
                    <th>
                        <div class="input-group">
                            <div class="left-icon">$</div>
                            {!! Form::number('importe_bruto', null, ['id' => 'importe-bruto', 'class' => 'form-control text-right', 'readonly']) !!}
                        </div>
                    </th>
                </tr>
                <tr>
                    <th colspan="4"></th>
                    <th><span class="pull-right">Int<span class="hidden-sm hidden-xs">eres</span>.(+)/</br>Desc<span class="hidden-sm hidden-xs">uento</span>.(-)</span></th>
                    <th>
                        <div class="input-group">
                            <div class="left-icon">%</div>
                            {!! Form::number('porcentaje_descuento', null, ['id' => 'porcentaje-impuesto', 'class' => 'form-control text-right', 'step' => '0.01']) !!}
                        </div>
                    </th>
                    <th>
                        <div class="input-group">
                            <div class="left-icon">$</div>
                            {!! Form::number('importe_descuento', 0, ['id' => 'importe-impuesto', 'class' => 'form-control text-right', 'step' => '0.01']) !!}
                        </div>
                    </th>
                    <th></th>
                </tr>
                <tr>
                    <th colspan="5">
                    </th>
                    <th><span class="pull-right">Imp. Interno</span></th>
                    <th>
                        <div class="input-group">
                            <div class="left-icon">$</div>
                            {!! Form::number('importe_impuesto_interno', 0, ['id' => 'importe-impuesto-interno', 'class' => 'form-control text-right', 'step' => '0.01']) !!}
                        </div>
                    </th>
                    <th></th>
                </tr>
                <tr>
                    <th colspan="5">
                    </th>
                    <th><span class="pull-right">Retenciones</span></th>
                    <th>
                        <div class="input-group">
                            <div class="left-icon">$</div>
                            {!! Form::number('retencion', 0, ['id' => 'importe-rentecion', 'class' => 'form-control text-right', 'step' => '0.01']) !!}
                        </div>
                    </th>
                    <th></th>
                </tr>
                <tr>
                    <th colspan="3" rowspan="2">
                        <div class="form-group">
                            {!! Form::label('descripcion', 'Comentario') !!}
                            {!! Form::textarea('descripcion', null, ['class' => 'form-control', 'rows' => '4']) !!}
                            {!! $errors->first('descripcion', '<p class="text-danger">:message</p>') !!}
                        </div>
                    </th>
                    <th colspan="2"></th>
                    <th><span class="pull-right">Imp. No Grabados</span></th>
                    <th>
                        <div class="input-group">
                            <div class="left-icon">$</div>
                            {!! Form::number('concepto_no_gravado', 0, ['id' => 'importe-concepto', 'class' => 'form-control text-right', 'step' => '0.01']) !!}
                        </div>
                    </th>
                    <th></th>
                </tr>
                <tr>
                    <th colspan="2"></th>
                    <th colspan="3">
                        <div class="card card-inverse card-info">
                            <div class="box bg-info text-center">
                                <h1 class="font-light text-white">
                                    <div class="pull-left">TOTAL:</div>
                                    <div class="pull-right" id="totalGrande">$ 0.00</div>
                                </h1>
                            </div>
                        </div>
                    </th>
                    {!! Form::number('total', null, ['id' => 'total', 'class' => 'd-none', 'step' => '0.01', 'required' => 'required']) !!}
                </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>

@include('frontend.compras.pago.pagos')

<div class="card">
    <div class="card-body">
        <div class="form-group">
            <input type="checkbox" id="actualizar_stocks" name="actualizar_stocks" class="filled-in chk-col-cyan" checked />
            <label for="actualizar_stocks"><strong>Actualizar Stock</strong></label>
            <i class="fa fa-question-circle text-info mytooltip">
                <span class="tooltip-content3">Suma los articulos comprados al stock actual de los mismos</span>
            </i>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <input type="checkbox" id="actualizar_precios" name="actualizar_precios" class="filled-in chk-col-cyan" checked />
            <label for="actualizar_precios"><strong>Actualizar Precios</strong></label>
            <i class="fa fa-question-circle text-info mytooltip">
                <span class="tooltip-content3">Reemplaza los precios de compra e ivas de los articulos ya existentes en esta compra con los detallados en el comprobante</span>
            </i>

            {{ Form::submit(isset($submitButtonText) ? $submitButtonText : trans('buttons.general.save'), ['class' => 'btn btn-themecolor btn-lg pull-right']) }}
        </div>
    </div>
</div>

@include('frontend.personas.proveedor.proveedor_dialog')