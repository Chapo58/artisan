<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        @include('frontend.includes.head')
    </head>

    @if($user->panel_orientacion == 'vertical' || $user->panel_orientacion == 'dark')
        <body class="fix-header fix-sidebar card-no-border">
    @else
        <body class="fix-header fix-sidebar card-no-border logo-center">
    @endif

        <div id="main-wrapper">
            @include('includes.partials.logged-in-as')
            @if ($user)
                @include('frontend.includes.nav')
                @include('frontend.includes.sidebar')
            @endif

            <div class="page-wrapper">
                <div class="container-fluid">
                @yield('content')

                @include('frontend.includes.footer')
                </div>
            </div>
        </div>

        <!-- Scripts -->
        @yield('before-scripts')

        @include('frontend.includes.scripts')

        @yield('after-scripts')

    </body>
</html>
