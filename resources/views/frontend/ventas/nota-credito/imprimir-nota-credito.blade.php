<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <body class="no-skin">
        <style>
            hr {
                margin-top: 0.5em;
                margin-bottom: 0.1em;
                border-width: 1px;
                border-top: 1px solid #000;
            }
        </style>
        @include('includes.partials.logged-in-as')

        <div class="main-container ace-save-state" id="main-container">
            <div class="container">
                <table style="width:100%">
                    <tr>
                        <td style="width:50%">
                            <img class="img-responsive" style="max-width: 200px; max-height: 80px;" src="{{$empresa->logo(true)}}"/>
                        </td>
                        <td>
                            <h1>{{ substr($tipos_comprobantes[$notaCredito->tipo_comprobante_id], -1) }}</h1>
                        </td>
                        <td style="width:30%">
                            <p>NOTA DE CREDITO <br>Nº {{ $notaCredito->puntoVenta.'-'.$notaCredito->numero }}</p>
                        </td>
                    </tr>
                </table>
                <table style="width:100%;font-size: 80%;">
                    <tr>
                        <td style="width:70%">{{ $empresa->razon_social }}</td>
                        <td>Emision: {{date('d/m/Y')}}</td>
                    </tr>
                    <tr>
                        <td style="width:70%">{{ (isset($empresa->domicilio->direccion)) ? $empresa->domicilio->direccion : '' }}</td>
                        <td>CUIT: {{ $empresa->cuit }}</td>
                    </tr>
                    <tr>
                        <td style="width:70%">
                            @if(isset($empresa->domicilio->codigo_postal))
                                {{$empresa->domicilio->codigo_postal}}
                            @endif
                            @if(isset($empresa->domicilio->localidad))
                                , {{$empresa->domicilio->localidad}}
                            @endif
                        </td>
                        <td>II.BB.: {{ $empresa->ingresos_brutos }}</td>
                    </tr>
                    <tr>
                        <td style="width:70%">
                            @if(isset($empresa->domicilio->provincia))
                                {{$empresa->domicilio->provincia}}
                            @endif
                            @if(isset($empresa->domicilio->pais))
                                , {{$empresa->domicilio->pais}}
                            @endif
                        </td>
                        <td>Inicio de Actividades: {{($empresa->inicio_actividad) ? $empresa->inicio_actividad->format('d/m/Y') : ''}}</td>
                    </tr>
                    <tr>
                        <td style="width:70%">{{ $empresa->telefono }}</td>
                        <td>I.V.A.: {{ $empresa->condicionIva }}</td>
                    </tr>
                </table>
                <hr>
                <table style="width:100%;font-size: 80%;">
                @if($notaCredito->cliente == "Consumidor Final")
                        <tr>
                            <td style="width:70%">{{ $notaCredito->cliente->razon_social }}</td>
                        </tr>
                @else
                        <tr>
                            <td style="width:70%">{{ $notaCredito->cliente->razon_social }}</td>
                            <td>Cuenta: {{ $notaCredito->cliente_id }}</td>
                        </tr>
                        <tr>
                            <td style="width:70%">{{ (isset($notaCredito->cliente->domicilio->direccion)) ? $notaCredito->cliente->domicilio->direccion : '' }}</td>
                            <td>CUIT: {{ $notaCredito->cliente->dni }}</td>
                        </tr>
                        <tr>
                            <td style="width:70%">{{ (isset($notaCredito->cliente->domicilio->localidad)) ? $notaCredito->cliente->domicilio->localidad : '' }}</td>
                            <td>II.BB.: {{ (isset($notaCredito->cliente->ingresos_brutos)) ? $notaCredito->cliente->ingresos_brutos : '' }}</td>
                        </tr>
                        <tr>
                            <td style="width:70%">{{ (isset($notaCredito->cliente->domicilio->provincia)) ? $notaCredito->cliente->domicilio->provincia : '' }}</td>
                            <td>I.V.A.: {{ $notaCredito->cliente->condicionIva }}</td>
                        </tr>
                        <tr>
                            <td style="width:70%">{{ (isset($notaCredito->cliente->telefono)) ? $notaCredito->cliente->telefono : '' }}</td>
                            <td></td>
                        </tr>
                @endif
                </table>
                <br>
                <table class="table table-condensed table-striped table-bordered" style="width:100%;font-size: 80%;">
                    <thead>
                        <tr class="info">
                            <th>Cantidad</th>
                            <th>Articulo</th>
                            <th>Precio Unitario</th>
                            <th>Total Neto</th>
                            <th>Alicuota</th>
                            <th>Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(!$notaCredito->detalles->isEmpty())
                            @foreach($notaCredito->detalles as $detalle)
                                <tr>
                                    <td>
                                        {{ number_format($detalle->cantidad , 0) }}
                                    </td>
                                    <td>
                                        {{ $detalle->articulo->nombre }}
                                    </td>
                                    <td>
                                        <b class="green">$ {{ number_format($detalle->precio_neto, 2) }}</b>
                                    </td>
                                    <td>
                                        <b class="green">$ {{ number_format($detalle->cantidad*$detalle->precio_neto , 2) }}</b>
                                    </td>
                                    <td>
                                        <b class="green">{{ number_format($porcentajes_iva[$detalle->porcentaje_iva], 2) }} %</b>
                                    </td>
                                    <td>
                                        <b class="green">$ {{ number_format( ($detalle->cantidad * $detalle->precio_neto) * ( ($porcentajes_iva[$detalle->porcentaje_iva]/100) + 1), 2) }}</b>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
                <div style="position: absolute;bottom: 80px;">
                    <hr />
                    <table style="font-size: 80%;width:50%;float:left;">
                        <tr>
                            <td style="width: 50%;">
                                1. Forma de Pago:
                                @foreach($notaCredito->detallesPagos as $detallePagos)
                                    {{ $detallePagos->descripcion }}
                                @endforeach
                            </td>
                        </tr>
                        @if($notaCredito->descripcion)
                        <tr>
                            <td style="width: 50%;">2. Comentario: {{ $notaCredito->descripcion }}</td>
                        </tr>
                        @endif
                        @if($codigoBarras)
                            <tr>
                                <td rowspan="3">
                                    <div class="col-md-12">
                                        {!! $codigoBarras !!}
                                    </div>
                                    <div class="col-md-12" style="font-size:80%;">
                                        {{ $numeroCodigoBarras }}
                                    </div>
                                    <div class="col-md-12">
                                        <i>C A E</i>: {{ $cae }}
                                    </div>
                                </td>
                            </tr>
                        @endif
                    </table>
                    <table style="font-size: 80%;width:50%;float:right;">
                        <tr>
                            <td style="width: 50%;">Neto:</td>
                            <td style="width: 50%;" align="right">$ {{ number_format($notaCredito->importe_neto, 2) }}</td>
                        </tr>
                        <tr>
                            <td style="width: 50%;">IVA:</td>
                            <td style="width: 50%;" align="right">$ {{ number_format($notaCredito->importe_iva, 2) }}</td>
                        </tr>
                        <tr>
                            <td style="width: 50%;">Impuesto/Descuento:</td>
                            <td style="width: 50%;" align="right">$ {{ number_format($notaCredito->importe_descuento, 2) }}</td>
                        </tr>
                        <tr>
                            <td colspan=2><hr /></td>
                        </tr>
                        <tr>
                            <td style="width: 50%;font-size: 130%;font-weight: bold;">Total</td>
                            <td style="width: 50%;font-size: 130%;font-weight: bold;" align="right">$ {{ number_format($notaCredito->total, 2) }}</td>
                        </tr>
                    </table>
                </div>
            </div><!-- container -->
        </div>
    </body>
</html>
