@extends('frontend.layouts.app')

@section ('title', trans('labels.frontend.notacredito_titulo'))

@section('content')
    <div class="main-content">
        <div class="main-content-inner">

            <div class="page-header">
                <h1>

                </h1>
            </div>

        </div>
    </div>
@endsection

@section('after-scripts')
    {{ Html::script("js/frontend/plugins/fiscal/ifu.js") }}
    {{ Html::script("js/frontend/custom.js") }}

<script>

    host = "{{ $notaCredito->puntoVenta->host_fiscal }}";
    modelo = {{ $notaCredito->puntoVenta->modelo_impresora_fiscal }};
    puerto = {{ $notaCredito->puntoVenta->puerto_fiscal }};
    baudios = {{ $notaCredito->puntoVenta->baudios_fiscal }};

    @if($notaCredito->cliente->condicionIva == "Responsable Inscripto")
        condicionIva = riResponsableInscripto;
        tipoNota = tcNota_Credito_A;
        tipoFactura = tcFactura_A;
    @elseif($notaCredito->cliente->condicionIva == "Monotributista")
        condicionIva = riMonotributo;
        tipoNota = tcNota_Credito_B;
        tipoFactura = tcFactura_B;
    @elseif($notaCredito->cliente->condicionIva == "Exento")
        condicionIva = riExento;
        tipoNota = tcNota_Credito_B;
        tipoFactura = tcFactura_B;
    @else
        condicionIva = riConsumidorFinal;
        tipoNota = tcNota_Credito_B;
        tipoFactura = tcFactura_B;
    @endif

    @if($empresa->condicionIva != "Responsable Inscripto")
        tipoNota = tcNota_Credito_C;
        tipoFactura = tcFactura_C;
    @endif

    function ultimoNumeroComprobante(){
        var driver = new Driver();
        driver.modelo = modelo;
        driver.puerto = puerto;
        driver.baudios = baudios;
        try {
            driver.iniciarTrabajo();
            driver.ultimoComprobante(tipoNota);
            driver.finalizarTrabajo();
            numeroComprobante = parseInt(driver.response.ultimoNumero) + 1;
        } catch (e){
            numeroComprobante = 0;
            cambiarEstadoFacturacion("ERROR");
            swal("Error", e, "error");
        }
    }

    function imprimir(){
        var driver = new Driver();
        driver.host = host //Cambiar por el ip de la maquina de la fiscal si es remota
        driver.modelo = modelo;
        driver.puerto = puerto;
        driver.baudios = baudios;

        @if(strlen($notaCredito->cliente->dni) > 8)
            tipoDocumento = tdCUIT;
        @else
            tipoDocumento = tdDNI;
        @endif

        try {
            driver.iniciarTrabajo();
            driver.cancelarComprobante();

            @if(!($notaCredito->cliente->condicionIva == "Consumidor Final" && $notaCredito->total <= 1000))
                driver.datosCliente("{{ $notaCredito->cliente->razon_social }}", tipoDocumento, "{{ $notaCredito->cliente->dni }}", condicionIva, "{{ (isset($notaCredito->cliente->domicilio->direccion)) ? $notaCredito->cliente->domicilio->direccion : ' ' }}");
            @endif

            @if($notaCredito->venta)
                //   Si es nota de credito enviar la factura de referencia
                driver.documentoDeReferencia2g(tipoFactura, "{{ $notaCredito->venta->puntoVenta.'-'.$notaCredito->venta->numero }}");
            @endif

            driver.abrirComprobante(tipoNota);

            @foreach($notaCredito->detalles as $detalle)
            <?php
            if($notaCredito->tipo_comprobante_id != 3){ // No es Nota de Credito A
                $detalle->precio_neto = $detalle->precio_neto + $detalle->precio_iva;
                if($detalle->importe_impuesto_interno){
                    $detalle->precio_neto = $detalle->precio_neto + $detalle->importe_impuesto_interno;
                }
            }
            ?>
             driver.imprimirItem2g("{{ $detalle->articulo->nombre }}", {{ number_format($detalle->cantidad , 0) }}, {{ number_format($detalle->precio_neto, 2, '.', '') }}, {{$porcentajes_iva[$detalle->porcentaje_iva]}}, {{($detalle->importe_impuesto_interno) ? $detalle->importe_impuesto_interno : 0}}, Gravado, tiFijo, 1, "{{ $detalle->articulo->codigo_barra }}", "{{ $detalle->articulo->codigo }}", Unidad);
            @endforeach

            @if($notaCredito->importe_descuento || $notaCredito->interesesPago())
                driver.imprimirDescuentoGeneral("Impuesto/Descuento (%)", {{ number_format(($notaCredito->importe_descuento + $notaCredito->interesesPago()) * -1, 2) }});
            @endif

            @foreach($notaCredito->detallesPagos as $detallePago)
             cuotas = 0;
             @if($detallePago->forma_pago == 0)
                 formaDePago = Efectivo;
             @elseif($detallePago->forma_pago == 1)
                 formaDePago = Cheque;
             @elseif($detallePago->forma_pago == 2)
                 formaDePago = Deposito;
             @elseif($detallePago->forma_pago == 3)
                 formaDePago = TarjetaDeCredito;
                 cuotas = 1;
             @else
                 formaDePago = CuentaCorriente;
             @endif
             driver.imprimirPago2g("{{ $detallePago->descripcion }}", {{ number_format($detallePago->monto + $detallePago->interes, 2, '.', '') }}, "", formaDePago, cuotas, "", "");
             // TODO Hacer el calculo de cuotas de tarjetas de credito
            @endforeach
            driver.cerrarComprobante();
            driver.finalizarTrabajo();

            cambiarEstadoFacturacion("FACTURADO");
            swal({
                title: "Operación realizada con éxito!",
                timer: 2000,
                type: "success",
                showConfirmButton: false
            }).then(
                window.location.href = "{{ url('/ventas/nota-credito') }}"
            )
        } catch (e){
            cambiarEstadoFacturacion("ERROR");
            swal("Error", e, "error");
        }
    }

    function cambiarEstadoFacturacion(estado) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            }
        });
        $.ajax({
            type: "POST",
            url: '{{ url("/ventas/nota-credito/setEstadoFacturacion") }}',
            data: { nota_id: {{ $notaCredito->id }}, estado: estado, numeroComprobante: numeroComprobante }
        });
    }

    $(document).ready(function() {
        swal({
            title: 'Realizando Nota de Credito',
            text: 'Imprimiendo Nota de Credito... Por favor aguarde...',
            timer: 10000,
            onOpen: () => {
                swal.showLoading()
            }
        });
        ultimoNumeroComprobante();
        imprimir();
    } );

</script>

@endsection
