@extends('frontend.layouts.app')

@section ('title', trans('labels.frontend.notacredito_titulo')." - ".trans('buttons.general.crud.show'))

@section('content')
    <div class="row page-titles">
        <div class="col-6 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('labels.frontend.notacredito_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item"><a href="{{url('/ventas/nota-credito')}}">{{ trans('labels.frontend.notacredito_titulo') }}</a></li>
                <li class="breadcrumb-item">{{ trans('buttons.general.crud.show') }} {{ trans('labels.frontend.notacredito') }}</li>
                <li class="breadcrumb-item active">{{ $notacredito }}</li>
            </ol>
        </div>
        <div class="col-6">
            <div class="pull-right">
                <a href="{{ url('/ventas/nota-credito') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans('buttons.general.crud.back') }}</span>
                </a>
                @if($notacredito->puntoVenta->tipo->nombre == "Comandera" || $notacredito->puntoVenta->tipo->nombre == "Impresora Manual")
                    <a href="{{ url('/ventas/nota-credito/imprimir/' . $notacredito->id) }}" target="_blank" title="Imprimir" class="btn btn-primary">
                        <i class="fa fa-print" aria-hidden="true"></i>
                        <span class="hidden-xs-down">Imprimir</span>
                    </a>
                    <a href="javascript:void(0)" title="{{trans("buttons.general.crud.delete")." ".trans("labels.frontend.notacredito")}}" class="btn btn-danger delete">
                        <i class="fa fa-trash" aria-hidden="true"></i>
                        <span class="hidden-xs-down">{{ trans("buttons.general.crud.delete") }}</span>
                    </a>
                @endif
                @if($notacredito->estado_facturacion == 2)
                    <a href="{{ url('/ventas/facturacion-fiscal/facturarNotaDeCredito/' . $notacredito->id) }}" target="_blank" title="Facturar">
                        <button class="btn btn-info">
                            <i class="fa fa-print" aria-hidden="true"></i>
                            <span class="hidden-xs-down">Volver a Facturar</span>
                        </button>
                    </a>
                @endif
            </div>
        </div>
    </div>

    @if($notacredito->estado_facturacion == 2)
        <div class="alert alert-danger alert-rounded font-bold">
            <i class="fa fa-exclamation-triangle"></i> ERROR DE FACTURACION
        </div>
    @endif
    @if($notacredito->estado_facturacion == 4)
        <div class="alert alert-warning alert-rounded font-bold">
            <i class="fa fa-exclamation-circle"></i> FACTURACION PENDIENTE
        </div>
    @endif
    <div class="card card-body printableArea">
        <h3>
            <b>{{ $tipos_comprobantes[$notacredito->tipo_comprobante_id] }}</b>
            <span class="pull-right">N° {{ $notacredito->puntoVenta.'-'.$notacredito->numero }}</span>
        </h3>
        <hr>
        <div class="row">
            <div class="col-md-12">
                <div class="pull-left">
                    <address>
                        <h3> &nbsp;<b class="text-danger">{{$empresa->razon_social}}</b></h3>
                        <p class="text-muted m-l-5">{{ (isset($empresa->domicilio->direccion)) ? $empresa->domicilio->direccion : '' }}
                            <br/> I.V.A.: {{ $empresa->condicionIva }}
                            <br/> CUIT: {{ $empresa->cuit }}
                            <br/> II.BB.: {{ $empresa->ingresos_brutos }}
                            <br/> Inicio de Actividades: {{($empresa->inicio_actividad) ? $empresa->inicio_actividad->format('d/m/Y') : ''}}</p>
                    </address>
                </div>
                <div class="pull-right text-right">
                    <address>
                        <h3 class="font-bold">
                            @if($notacredito->cliente)
                                <a href="{{url('/personas/cliente/'.$notacredito->cliente->id)}}">{{ $notacredito->cliente }}</a>
                        </h3>
                        <p class="text-muted m-l-30">{{ (isset($notacredito->cliente->domicilio->direccion)) ? $notacredito->cliente->domicilio->direccion : '' }}
                            <br/> I.V.A.: {{ $notacredito->cliente->condicionIva }}
                            <br/> CUIT: {{ $notacredito->cliente->dni }}
                            <br/> Cuenta: {{ $notacredito->cliente_id }}</p>
                        @else
                            Cliente Eliminado
                            </h3>
                        @endif
                    </address>
                </div>
            </div>
            <div class="col-md-2 col-xs-6 b-r text-center text-muted"> <span class="font-bold">Lista de Precios</span>
                <br>
                <p>
                    <a href="{{url('/ventas/lista-precios/'.$notacredito->listaPrecios->id)}}">{{ $notacredito->listaPrecios }}</a>
                </p>
            </div>
            <div class="col-md-3 col-xs-6 b-r text-center text-muted"> <span class="font-bold">Moneda/Cotización</span>
                <br>
                <p>
                    {{ $notacredito->moneda }} - <b class="text-success">$ {{ $notacredito->cotizacion }}</b>
                </p>
            </div>
            <div class="col-md-2 col-xs-6 b-r text-center text-muted"> <span class="font-bold">Fecha Factura</span>
                <br>
                <p>
                    <i class="fa fa-calendar"></i> {{ $notacredito->fecha->format('d/m/Y H:i') }}
                </p>
            </div>
            <div class="col-md-3 col-xs-6 text-center text-muted"> <span class="font-bold">Venta Asociada</span>
                <br>
                <p>
                    @if(isset($notacredito->venta))
                        <a href="{{url('ventas/venta/'.$notacredito->venta->id)}}">
                            {{ $notacredito->venta.' - Total: $'.$notacredito->venta->total_cobro }}
                        </a>
                    @endif
                </p>
            </div>
            <div class="col-md-2 col-xs-6 b-r text-center text-muted"> <span class="font-bold">Concepto</span>
                <br>
                <p>
                    {{ $conceptos[$notacredito->concepto_nota_credito] }}
                </p>
            </div>
            <div class="col-md-12">
                <div class="table-responsive m-t-40" style="clear: both;">
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th class="text-center">#</th>
                            <th>Descripción</th>
                            <th class="text-right">Cantidad</th>
                            <th class="text-right">Pre/Uni. Neto</th>
                            <th class="text-right">Neto</th>
                            <th class="text-right">IVA</th>
                            <th class="text-right">Imp. Int.</th>
                            <th class="text-right">Total</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(!$notacredito->detalles->isEmpty())
                            @foreach($notacredito->detalles as $detalle)
                                <tr>
                                    <td class="text-center">
                                        {{ $loop->index + 1 }}
                                    </td>
                                    <td>
                                        @if(isset($detalle->articulo))
                                            <a href="{{url('/articulos/articulo/'.$detalle->articulo->id)}}">
                                                ({{ $detalle->articulo->codigo }}) {{ $detalle->articulo->nombre }} {{ isset($detalle->marca) ? ' - '.$detalle->marca : '' }} {{ isset($detalle->modelo) ? ' - '.$detalle->modelo : '' }}{{ isset($detalle->modelo_secundario) ? ' - '.$detalle->modelo_secundario : '' }}
                                            </a>
                                        @else
                                            Artículo Eliminado
                                        @endif
                                    </td>
                                    <td class="text-right">
                                        {{ number_format($detalle->cantidad , 0) }}
                                    </td>
                                    <td class="text-right">
                                        {{ $notacredito->moneda->signo }} {{ number_format($detalle->precio_neto, 2) }}
                                    </td>
                                    <td class="text-right">
                                        {{ $notacredito->moneda->signo }} {{ number_format($detalle->cantidad*$detalle->precio_neto , 2) }}
                                    </td>
                                    <td class="text-right">
                                        {{ number_format($porcentajes_iva[$detalle->porcentaje_iva], 2) }} %
                                    </td>
                                    <td class="text-right">
                                        {{ $notacredito->moneda->signo }} {{ number_format($detalle->cantidad*$detalle->importe_impuesto_interno, 2) }}
                                    </td>
                                    <td class="text-right">
                                        {{ $notacredito->moneda->signo }} {{ number_format(($detalle->cantidad * $detalle->precio_neto) * (($porcentajes_iva[$detalle->porcentaje_iva]/100) + 1) + ($detalle->cantidad*$detalle->importe_impuesto_interno), 2) }}
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-md-6">
                <hr class="m-t-30">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th class="text-center">#</th>
                        <th>Forma de Pago</th>
                        <th class="text-right">Monto</th>
                        <th class="text-right">Interes</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!$notacredito->detallesPagos->isEmpty())
                        @foreach($notacredito->detallesPagos as $detallePago)
                            <tr>
                                <td class="text-center">
                                    {{ $loop->index + 1 }}
                                </td>
                                <td>
                                    {{ $detallePago->descripcion }} -
                                    @if(isset($detallePago->entidad))
                                        <a href="{{ url($detallePago->entidad->url) }}">
                                            {{ $detallePago->entidad }}
                                        </a>
                                    @endif
                                </td>
                                <td class="text-right">
                                    {{ $notacredito->moneda->signo }} {{ number_format($detallePago->monto , 2) }}
                                </td>
                                <td class="text-right">
                                    {{ $notacredito->moneda->signo }} {{ number_format($detallePago->interes, 2) }}
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                    <tfoot>
                    <tr>
                        <td></td>
                        <td></td>
                        <td class="text-right font-bold">
                            Total Pago
                        </td>
                        <td class="text-right">
                            {{ $notacredito->moneda->signo }} {{ number_format($notacredito->total_pago, 2) }}
                        </td>
                    </tr>
                    </tfoot>
                </table>
                @if($notacredito->descripcion)
                    <p>Comentario: <i>{{ $notacredito->descripcion }}</i></p>
                @endif
            </div>
            <div class="col-md-6">
                <div class="pull-right m-t-30 text-right">
                    <p>Importe Neto : {{ $notacredito->moneda->signo }} {{ number_format($notacredito->importe_neto, 2) }} </p>
                    <p>Iva : {{ $notacredito->moneda->signo }} {{ number_format($notacredito->importe_iva, 2) }} </p>
                    <p>Interes/Descuento : {{ $notacredito->moneda->signo }} {{ number_format($notacredito->importe_descuento, 2) }} </p>
                    <p>Impuestos Internos : {{ $notacredito->moneda->signo }} {{ number_format($notacredito->importe_impuesto_interno, 2) }} </p>
                    <hr>
                    <h3><b>Total :</b> {{ $notacredito->moneda->signo }} {{ number_format($notacredito->total, 2) }}</h3>
                </div>
            </div>
            @if($codigoBarras)
                <div class="col-md-12">
                    {!! $codigoBarras !!}
                </div>
                <div class="col-md-12" style="font-size:80%;">
                    {{ $numeroCodigoBarras }}
                </div>
                <div class="col-md-12">
                    <i>C A E</i>: {{ $cae }}
                </div>
            @endif
        </div>
    </div>
@endsection

@section('after-scripts')

<script>
    $("body").on("click", ".delete", function () {
        swal({
            title: "{{trans('buttons.general.crud.delete').' '.trans('labels.frontend.notacredito')}}",
            text: "¿Realmente desea eliminar este registro?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#ff8726',
            confirmButtonText: "Eliminar"
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '{{ url('/ventas/nota-credito/'.$notacredito->id) }}',
                    data: {"_method" : 'DELETE'},
                    success: function (msg) {
                        swal("Eliminado!", "El registro ha sido correctamente eliminado.", "success").then((result) => { window.location.href = '{{url('/ventas/nota-credito')}}'; });
                    }
                });
            }
        });
    });
</script>

@endsection