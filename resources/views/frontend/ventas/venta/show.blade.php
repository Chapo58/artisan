@extends('frontend.layouts.app')

@section ('title', trans('labels.frontend.venta_titulo')." - ".trans('buttons.general.crud.show'))

@section('content')
    <div class="row page-titles">
        <div class="col-6 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('labels.frontend.venta_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item"><a href="{{url('/ventas/venta')}}">{{ trans('labels.frontend.venta_titulo') }}</a></li>
                <li class="breadcrumb-item">{{ trans('buttons.general.crud.show') }} {{ trans('labels.frontend.venta') }}</li>
                <li class="breadcrumb-item active">
                    {{ $venta }}
                    @if(isset($venta->factura_automatica_id))
                        (Factura Generada Automáticamente)
                    @endif
                </li>
            </ol>
        </div>
        <div class="col-6">
            <div class="pull-right">
                <a href="{{ url('/ventas/venta') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans('buttons.general.crud.back') }}</span>
                </a>
                @if($venta->factura_automatica_id == null)
                    @if($venta->puntoVenta->tipo != 'Impresora Fiscal' && $venta->estado != 12)
                        <a href="{{ url('/ventas/venta/imprimir/' . $venta->id) }}" target="_blank" title="Imprimir" class="btn btn-primary">
                            <i class="fa fa-print" aria-hidden="true"></i>
                            <span class="hidden-xs-down">Imprimir</span>
                        </a>
                    @endif
                    @if($venta->estado == 12)
                        <a href="{{url('/ventas/venta/'.$venta->id. '/edit')}}" title="Continuar Venta Guardada" class="btn btn-info">
                            <i class="fa fa-mail-forward" aria-hidden="true"></i>
                            <span class="hidden-xs-down">Continuar</span>
                        </a>
                    @endif
                    @if($venta->estado_facturacion != 1)
                        <a href="javascript:void(0)" title="{{trans("buttons.general.crud.delete")." ".trans("labels.frontend.venta")}}" class="btn btn-danger delete">
                            <i class="fa fa-trash" aria-hidden="true"></i>
                            <span class="hidden-xs-down">{{ trans("buttons.general.crud.delete") }}</span>
                        </a>
                    @endif
                    @if($venta->estado_facturacion == 2 || $venta->estado_facturacion == 4)
                        @if($venta->tipo_punto_id == 1 && $user->controladoraFiscal())
                            <a href="{{ url('/ventas/facturacion-fiscal/facturarVenta/' . $venta->id) }}" target="_blank" title="Facturar">
                                <button class="btn btn-info">
                                    <i class="fa fa-print" aria-hidden="true"></i>
                                    <span class="hidden-xs-down">Facturar</span>
                                </button>
                            </a>
                        @elseif($venta->tipo_punto_id == 3)
                            <a href="{{ url('/ventas/facturacion-electronica/facturarVenta/' . $venta->id) }}" target="_blank" title="Facturar">
                                <button class="btn btn-info">
                                    <i class="fa fa-print" aria-hidden="true"></i>
                                    <span class="hidden-xs-down">Facturar</span>
                                </button>
                            </a>
                        @endif
                    @endif
                @endif
            </div>
        </div>
    </div>

    @tips($_SERVER['REQUEST_URI'])

    @if($venta->estado_facturacion == 2)
        <div class="alert alert-danger alert-rounded font-bold">
            <i class="fa fa-exclamation-triangle"></i> ERROR DE FACTURACION
        </div>
    @endif
    @if($venta->estado_facturacion == 4)
        <div class="alert alert-warning alert-rounded font-bold">
            <i class="fa fa-exclamation-circle"></i> FACTURACION PENDIENTE
        </div>
    @endif
    @if($venta->estado == 12)
        <div class="alert alert-info alert-rounded font-bold">
            <i class="fa fa-save"></i> VENTA GUARDADA
        </div>
    @endif
    <div class="card card-body printableArea">
        <h3>
            <b>{{ $venta->tipoComprobante() }}</b>
            <span class="pull-right">N° {{ $venta->puntoVenta.'-'.$venta->numero }}</span>
        </h3>
        <hr>
        <div class="row">
            <div class="col-md-12">
                <div class="pull-left">
                    <address>
                        <h3> &nbsp;<b class="text-danger">{{$empresa->razon_social}}</b></h3>
                        <p class="text-muted m-l-5">{{ (isset($empresa->domicilio->direccion)) ? $empresa->domicilio->direccion : '' }}
                            <br/> I.V.A.: {{ $empresa->condicionIva }}
                            <br/> CUIT: {{ $empresa->cuit }}
                            <br/> II.BB.: {{ $empresa->ingresos_brutos }}
                            <br/> Inicio de Actividades: {{($empresa->inicio_actividad) ? $empresa->inicio_actividad->format('d/m/Y') : ''}}</p>
                    </address>
                </div>
                <div class="pull-right text-right">
                    <address>
                        <h3 class="font-bold">
                            @if($venta->cliente)
                                <a href="{{url('/personas/cliente/'.$venta->cliente->id)}}">{{ $venta->cliente }}</a>
                        </h3>
                        <p class="text-muted m-l-30">{{ (isset($venta->cliente->domicilio->direccion)) ? $venta->cliente->domicilio->direccion : '' }}
                            <br/> I.V.A.: {{ $venta->cliente->condicionIva }}
                            <br/> CUIT: {{ $venta->cliente->dni }}
                            <br/> Cuenta: {{ $venta->cliente_id }}</p>
                        @else
                            Cliente Eliminado
                            </h3>
                        @endif
                    </address>
                </div>
            </div>
            <div class="col-md-3 col-xs-6 text-center text-muted"> <span class="font-bold">Vendedor</span>
                <br>
                <p>
                    @if($venta->usuario)
                        <a href="{{url('/configuraciones/usuario/'.$venta->usuario->id)}}">{{ $venta->usuario }}</a>
                    @else
                        Usuario Eliminado
                    @endif
                </p>
            </div>
            <div class="col-md-3 col-xs-6 b-r text-center text-muted"> <span class="font-bold">Lista de Precios</span>
                <br>
                <p>
                    <a href="{{url('/ventas/lista-precios/'.$venta->listaPrecios->id)}}">{{ $venta->listaPrecios }}</a>
                </p>
            </div>
            <div class="col-md-3 col-xs-6 b-r text-center text-muted"> <span class="font-bold">Moneda/Cotización</span>
                <br>
                <p>
                    {{ $venta->moneda }} - <b class="text-success">$ {{ $venta->cotizacion }}</b>
                </p>
            </div>
            <div class="col-md-3 col-xs-6 b-r text-center text-muted"> <span class="font-bold">Fecha Factura</span>
                <br>
                <p>
                    <i class="fa fa-calendar"></i> {{ $venta->fecha->format('d/m/Y H:i') }}
                </p>
            </div>

            <div class="col-md-12">
                <div class="table-responsive m-t-40" style="clear: both;">
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th class="text-center">#</th>
                            <th>Descripción</th>
                            <th class="text-right">Cantidad</th>
                            @if($venta->tipoComprobante() == 'Factura A' || $venta->tipoComprobante() == 'Tique Factura A')
                                <th class="text-right">Pre/Uni. Neto</th>
                                <th class="text-right">Neto</th>
                                <th class="text-right">IVA</th>
                                <th class="text-right">Imp. Int.</th>
                            @else
                                <th class="text-right">Precio Unitario</th>
                            @endif
                            <th class="text-right">Total</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(!$venta->detalles->isEmpty())
                            @foreach($venta->detalles as $detalle)
                                <tr>
                                    <td class="text-center">
                                        {{ $loop->index + 1 }}
                                    </td>
                                    <td>
                                        @if(isset($detalle->articulo))
                                            <a href="{{url('/articulos/articulo/'.$detalle->articulo->id)}}">
                                                @if($detalle->descripcion)
                                                    {{$detalle->descripcion}}
                                                @else
                                                   ({{ $detalle->articulo->codigo }}) {{ $detalle->articulo->nombre }} {{ isset($detalle->marca) ? ' - '.$detalle->marca : '' }} {{ isset($detalle->modelo) ? ' - '.$detalle->modelo : '' }}{{ isset($detalle->modelo_secundario) ? ' - '.$detalle->modelo_secundario : '' }}
                                                @endif
                                            </a>
                                        @else
                                            Artículo Eliminado
                                        @endif
                                    </td>
                                    <td class="text-right">
                                        {{ number_format($detalle->cantidad , 0) }}
                                    </td>
                                    <td class="text-right">
                                        {{ $venta->moneda->signo }} {{ number_format($detalle->precioUnitario(), 2) }}
                                    </td>
                                    @if($venta->tipoComprobante() == 'Factura A' || $venta->tipoComprobante() == 'Tique Factura A')
                                        <td class="text-right">
                                            {{ $venta->moneda->signo }} {{ number_format($detalle->cantidad*$detalle->precio_neto , 2) }}
                                        </td>
                                        <td class="text-right">
                                            {{ number_format($porcentajes_iva[$detalle->porcentaje_iva], 2) }} %
                                        </td>
                                        <td class="text-right">
                                            {{ $venta->moneda->signo }} {{ number_format($detalle->cantidad*$detalle->importe_impuesto_interno, 2) }}
                                        </td>
                                    @endif
                                    <td class="text-right">
                                        {{ $venta->moneda->signo }} {{ number_format(($detalle->cantidad * $detalle->precio_neto) * (($porcentajes_iva[$detalle->porcentaje_iva]/100) + 1) + ($detalle->cantidad*$detalle->importe_impuesto_interno), 2) }}
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-md-6">
                <hr class="m-t-30">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th class="text-center">#</th>
                            <th>Forma de Pago</th>
                            <th class="text-right">Monto</th>
                            <th class="text-right">Interes</th>
                        </tr>
                    </thead>
                    <tbody>
                    @if(!$venta->detallesCobros->isEmpty())
                        @foreach($venta->detallesCobros as $detalleCobro)
                            <tr>
                                <td class="text-center">
                                    {{ $loop->index + 1 }}
                                </td>
                                <td>
                                    {{ $detalleCobro->descripcion }} -
                                    @if(isset($detalleCobro->entidad))
                                        <a href="{{ url($detalleCobro->entidad->url) }}">
                                            {{ $detalleCobro->entidad }}
                                        </a>
                                    @endif
                                </td>
                                <td class="text-right">
                                    {{ $venta->moneda->signo }} {{ number_format($detalleCobro->monto , 2) }}
                                </td>
                                <td class="text-right">
                                    {{ $venta->moneda->signo }} {{ number_format($detalleCobro->interes, 2) }}
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                    <tfoot>
                        <tr>
                            <td></td>
                            <td></td>
                            <td class="text-right font-bold">
                                Total Cobro
                            </td>
                            <td class="text-right">
                                {{ $venta->moneda->signo }} {{ number_format($venta->total_cobro, 2) }}
                            </td>
                        </tr>
                    </tfoot>
                </table>
                @if($venta->descripcion)
                    <p>Comentario: <i>{{ $venta->descripcion }}</i></p>
                @endif
            </div>
            <div class="col-md-6">
                <div class="pull-right m-t-30 text-right">
                    @if($venta->tipoComprobante() == 'Factura A' || $venta->tipoComprobante() == 'Tique Factura A')
                        <p>Importe Neto : {{ $venta->moneda->signo }} {{ number_format($venta->importe_neto, 2) }} </p>
                        <p>Iva : {{ $venta->moneda->signo }} {{ number_format($venta->importe_iva, 2) }} </p>

                        @if(isset($venta->factura_automatica_id) && $venta->estado == 10)
                        <!-- Estado FACTURA_AUTOMATICA_PENDIENTE -->
                            <p>Recargo por vencimiento : {{ $venta->moneda->signo }} {{ $venta->importe_descuento }} </p>
                        @endif

                        <p>Impuestos Internos : {{ $venta->moneda->signo }} {{ number_format($venta->importe_impuesto_interno, 2) }} </p>
                    @endif
                    <p>Interes/Descuento : {{ $venta->moneda->signo }} {{ number_format($venta->porcentaje_descuento, 2) }} </p>
                    <hr>
                    <h3><b>Total :</b> {{ $venta->moneda->signo }} {{ number_format($venta->total, 2) }}</h3>
                </div>
            </div>
            @if($codigoBarras)
                <div class="col-md-12">
                    {!! $codigoBarras !!}
                </div>
                <div class="col-md-12" style="font-size:80%;">
                    {{ $numeroCodigoBarras }}
                </div>
                <div class="col-md-12">
                    <i>C A E</i>: {{ $cae }}
                </div>
            @endif
            {{--<div class="col-md-12">
                <div class="clearfix"></div>
                <hr>
                <div class="text-right">
                    <button id="print" class="btn btn-default btn-outline" type="button"> <span><i class="fa fa-print"></i> Imprimir</span> </button>
                </div>
            </div>--}}
        </div>
    </div>

@endsection

@section('after-scripts')
    {{ Html::script("js/frontend/jquery.PrintArea.js") }}

<script>
    $(document).ready(function() {
        $("#print").click(function() {
            var mode = 'iframe'; //popup
            var close = mode == "popup";
            var options = {
                mode: mode,
                popClose: close
            };
            $("div.printableArea").printArea(options);
        });
    });

    $("body").on("click", ".delete", function () {
        swal({
            title: "{{trans('buttons.general.crud.delete').' '.trans('labels.frontend.venta')}}",
            text: "¿Realmente desea eliminar este registro?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#ff8726',
            confirmButtonText: "Eliminar"
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '{{ url('/ventas/venta/'.$venta->id) }}',
                    data: {"_method" : 'DELETE'},
                    success: function (msg) {
                        swal("Eliminado!", "El registro ha sido correctamente eliminado.", "success").then((result) => { window.location.href = '{{url('/ventas/venta')}}'; });
                    }
                });
            }
        });
    });
</script>

@endsection
