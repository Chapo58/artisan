<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <body class="no-skin">
        <style>
            hr {
                margin-top: 0.5em;
                margin-bottom: 0.1em;
                border-width: 1px;
                border-top: 1px solid #000;
            }
        </style>
        @include('includes.partials.logged-in-as')

        <div class="main-container ace-save-state" id="main-container">
            <div class="container">
                <table style="width:100%">
                    <tr>
                        <td style="width:50%">
                            <img class="img-responsive" style="max-width: 200px; max-height: 80px;" src="{{$empresa->logo(true)}}"/>
                        </td>
                        <td>
                            <h1>{{ substr($venta->tipoComprobante(), -1) }}</h1>
                        </td>
                        <td>
                            <p>FACTURA Nº {{ $venta->puntoVenta.'-'.$venta->numero }}</p>
                        </td>
                    </tr>
                </table>
                <table style="width:100%;font-size: 80%;">
                    <tr>
                        <td style="width:70%">{{ $empresa->razon_social }}</td>
                        <td>Emision: {{ $venta->fecha->format('d/m/Y') }}</td>
                    </tr>
                    <tr>
                        <td style="width:70%">{{ (isset($empresa->domicilio->direccion)) ? $empresa->domicilio->direccion : '' }}</td>
                        <td>CUIT: {{ $empresa->cuit }}</td>
                    </tr>
                    <tr>
                        <td style="width:70%">
                            @if(isset($empresa->domicilio->codigo_postal))
                                {{$empresa->domicilio->codigo_postal}}
                            @endif
                            @if(isset($empresa->domicilio->localidad))
                                , {{$empresa->domicilio->localidad}}
                            @endif
                        </td>
                        <td>II.BB.: {{ $empresa->ingresos_brutos }}</td>
                    </tr>
                    <tr>
                        <td style="width:70%">
                            @if(isset($empresa->domicilio->provincia))
                                {{$empresa->domicilio->provincia}}
                            @endif
                            @if(isset($empresa->domicilio->pais))
                                , {{$empresa->domicilio->pais}}
                            @endif
                        </td>
                        <td>Inicio de Actividades: {{($empresa->inicio_actividad) ? $empresa->inicio_actividad->format('d/m/Y') : ''}}</td>
                    </tr>
                    <tr>
                        <td style="width:70%">{{ $empresa->telefono }}</td>
                        <td>I.V.A.: {{ $empresa->condicionIva }}</td>
                    </tr>
                </table>
                <hr>
                <table style="width:100%;font-size: 80%;">
                @if($venta->cliente == "Consumidor Final")
                        <tr>
                            <td style="width:70%">{{ $venta->cliente->razon_social }}</td>
                        </tr>
                @else
                        <tr>
                            <td style="width:70%">{{ $venta->cliente->razon_social }}</td>
                            <td>Cuenta: {{ $venta->cliente_id }}</td>
                        </tr>
                        <tr>
                            <td style="width:70%">{{ (isset($venta->cliente->domicilio->direccion)) ? $venta->cliente->domicilio->direccion : '' }}</td>
                            <td>CUIT: {{ $venta->cliente->dni }}</td>
                        </tr>
                        <tr>
                            <td style="width:70%">{{ (isset($venta->cliente->domicilio->localidad)) ? $venta->cliente->domicilio->localidad : '' }}</td>
                            <td>II.BB.: {{ (isset($venta->cliente->ingresos_brutos)) ? $venta->cliente->ingresos_brutos : '' }}</td>
                        </tr>
                        <tr>
                            <td style="width:70%">{{ (isset($venta->cliente->domicilio->provincia)) ? $venta->cliente->domicilio->provincia : '' }}</td>
                            <td>I.V.A.: {{ $venta->cliente->condicionIva }}</td>
                        </tr>
                        <tr>
                            <td style="width:70%">{{ (isset($venta->cliente->telefono)) ? $venta->cliente->telefono : '' }}</td>
                            <td></td>
                        </tr>
                @endif
                </table>
                <br>
                <table class="table table-condensed table-striped table-bordered" style="width:100%;font-size: 80%;">
                    <thead>
                        <tr class="info">
                            <th>Cantidad</th>
                            <th>Articulo</th>
                            <th>Precio Unitario</th>
                            @if($venta->tipoComprobante() == 'Factura A' || $venta->tipoComprobante() == 'Tique Factura A')
                                <th>Total Neto</th>
                                <th>Alicuota</th>
                            @endif
                            <th>Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(!$venta->detalles->isEmpty())
                            @foreach($venta->detalles as $detalle)
                                <tr>
                                    <td>
                                        {{ number_format($detalle->cantidad , 0) }}
                                    </td>
                                    <td>
                                        @if($detalle->descripcion)
                                            {{ $detalle->descripcion }}
                                        @else
                                            {{ $detalle->articulo->nombre }}
                                        @endif
                                    </td>
                                    <td>
                                        <b class="green">$ {{ number_format($detalle->precioUnitario(), 2) }}</b>
                                    </td>
                                    @if($venta->tipoComprobante() == 'Factura A' || $venta->tipoComprobante() == 'Tique Factura A')
                                    <td>
                                        <b class="green">$ {{ number_format($detalle->cantidad*$detalle->precio_neto , 2) }}</b>
                                    </td>
                                    <td>
                                        <b class="green">{{ number_format($porcentajes_iva[$detalle->porcentaje_iva], 2) }} %</b>
                                    </td>
                                    @endif
                                    <td>
                                        <b class="green">$ {{ number_format( ($detalle->cantidad * $detalle->precio_neto) * ( ($porcentajes_iva[$detalle->porcentaje_iva]/100) + 1), 2) }}</b>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
                <div style="position: absolute;bottom: 80px;">
                    <hr />
                    <table style="font-size: 80%;width:50%;float:left;">
                        <tr>
                            <td style="width: 50%;">
                                1. Forma de Pago:
                                @foreach($venta->detallesCobros as $detalleCobro)
                                    {{ $detalleCobro->descripcion }}
                                @endforeach
                            </td>
                        </tr>
                        @if($venta->descripcion)
                        <tr>
                            <td style="width: 50%;">2. Comentario: {{ $venta->descripcion }}</td>
                        </tr>
                        @endif
                        @if($codigoBarras)
                            <tr>
                                <td rowspan="3">
                                    <div class="col-md-12">
                                        {!! $codigoBarras !!}
                                    </div>
                                    <div class="col-md-12" style="font-size:80%;">
                                        {{ $numeroCodigoBarras }}
                                    </div>
                                    <div class="col-md-12">
                                        <i>C A E</i>: {{ $cae }}
                                    </div>
                                </td>
                            </tr>
                        @endif
                    </table>
                    <table style="font-size: 80%;width:50%;float:right;">
                        @if($venta->tipoComprobante() == 'Factura A' || $venta->tipoComprobante() == 'Tique Factura A')
                        <tr>
                            <td style="width: 50%;">Neto:</td>
                            <td style="width: 50%;" align="right">$ {{ number_format($venta->importe_neto, 2) }}</td>
                        </tr>
                        <tr>
                            <td style="width: 50%;">IVA:</td>
                            <td style="width: 50%;" align="right">$ {{ number_format($venta->importe_iva, 2) }}</td>
                        </tr>
                        @endif
                        <tr>
                            <td style="width: 50%;">Impuesto/Descuento:</td>
                            <td style="width: 50%;" align="right">$ {{ number_format($venta->porcentaje_descuento, 2) }}</td>
                        </tr>
                        <tr>
                            <td colspan=2><hr /></td>
                        </tr>
                        <tr>
                            <td style="width: 50%;font-size: 130%;font-weight: bold;">Total</td>
                            <td style="width: 50%;font-size: 130%;font-weight: bold;" align="right">$ {{ number_format($venta->total, 2) }}</td>
                        </tr>
                    </table>
                </div>
            </div><!-- container -->
        </div>
    </body>
    {{-- <script type="text/php">
        if (isset($pdf)) {
            $x = 510;
            $y = 750;
            $text = "Pagina {PAGE_NUM} de {PAGE_COUNT}";
            $font = null;
            $size = 12;
            $word_space = 0.0;  //  default
            $char_space = 0.0;  //  default
            $angle = 0.0;   //  default
            $pdf->page_text($x, $y, $text, $font, $size, $word_space, $char_space, $angle);
        }
    </script> --}}
</html>
