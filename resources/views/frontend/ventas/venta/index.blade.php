@extends ('frontend.layouts.app')

@section ('title', trans('labels.frontend.venta_titulo')." - ".trans('navs.frontend.user.administration'))

@section('after-styles')
    {{ Html::style("css/frontend/plugins/datatables/responsive.dataTables.min.css") }}
@endsection

@section('content')
    <div class="row page-titles">
        <div class="col-6 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('labels.frontend.venta_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item active">{{ trans('labels.frontend.venta_titulo') }}</li>
            </ol>
        </div>
        <div class="col-6">
            @if($estadoCaja == 1)
                <div class="pull-right">
                    <a href="{{ url('/ventas/venta/create') }}" class="btn btn-themecolor btn-lg" title="{{ trans('buttons.general.crud.create').' '.trans('labels.frontend.venta')  }}">
                        <i class="fa fa-plus" aria-hidden="true"></i>
                        <span class="hidden-xs-down">Nueva {{ trans('labels.frontend.venta') }}</span>
                    </a>
                    <a href="{{ url('/ventas/venta/create_rapido') }}" class="btn btn-success btn-lg" title="Venta Rapida">
                        <i class="fa fa-rocket" aria-hidden="true"></i>
                        <span class="hidden-xs-down">Venta Rapida</span>
                    </a>
                </div>
            @else
                <div class="row">
                    <div class="col-md-8">
                        <h2 class="text-danger pull-right"><i class="fa fa-exclamation-circle" aria-hidden="true"></i> Caja cerrada</h2>
                    </div>
                    <div class="col-md-4">
                        <a href="{{url('caja/movimiento-caja')}}" class="btn btn-themecolor btn-lg pull-right">
                            <i class="fa fa-mail-reply" aria-hidden="true"></i> Abrir Caja
                        </a>
                    </div>
                </div>
            @endif
        </div>
    </div>

    @tips($_SERVER['REQUEST_URI'])

    <div class="card">
        <div class="card-body">
            <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
            @include('includes.partials.messages')
            <div class="table-responsive">
                <table id="venta-table" class="table selectable table-striped table-hover table-sm">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Número</th>
                            <th>Fecha</th>
                            <th>Cliente</th>
                            <th>Lista Usada</th>
                            <th>Vendedor</th>
                            <th>Total</th>
                            <th>{{ trans('labels.general.actions') }}</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

@endsection

@section('after-scripts')
    {{ Html::script("js/frontend/plugins/datatables/jquery.dataTables.min.js") }}
    {{ Html::script("js/frontend/plugins/datatables/dataTables.responsive.min.js") }}
    {{ Html::script("js/frontend/plugins/datatables/dataTables.select.min.js") }}

<script>
    $(document).ready(function() {
        $('#venta-table').DataTable({
            "aaSorting": [],
            processing: true,
            serverSide: true,
            responsive: {
                details: {
                    type: 'column',
                    target: 'tr'
                }
            },
            "columnDefs": [{
                targets: 0,
                className: 'control',
                orderable: false,
                searchable: false,
                defaultContent: '&nbsp;&nbsp;'
            },
                { responsivePriority: 1, targets: -1 },
                { responsivePriority: 1, targets: -2 },
                { responsivePriority: 1, targets: 1 }],
            ajax: '{!! route('frontend.ventas.VentasData') !!}',
            columns: [
                {data: null, name: ''},
                {data: 'numero', name: 'numero'},
                {data: 'fecha', name: 'fecha'},
                {data: 'razon_social', name: 'razon_social'},
                {data: 'lista', name: 'lista'},
                {data: 'usuario', name: 'usuario'},
                {data: 'total', name: 'total'},
                {data: 'action', name: 'action', orderable: false, searchable: false}
            ]
        });
    } );

    $("body").on("click", ".delete", function () {
        var id = $(this).attr("data-id");
        swal({
            title: "{{trans('buttons.general.crud.delete').' '.trans('labels.frontend.venta')}}",
            text: "¿Realmente desea eliminar este registro?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#ff8726',
            confirmButtonText: "Eliminar"
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/ventas/venta/'+id,
                    data: {"_method" : 'DELETE'},
                    success: function (msg) {
                        $("#" + id).hide(1);
                        swal("Eliminado!", "El registro ha sido correctamente eliminado.", "success");
                    }
                });
            }
        });
    });
</script>

@endsection
