<div class="card">
    <div class="card-body">
        <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
        <div class="row">
            <div class="col-9">
                <div class="table-responsive">
                    <table id="tabla-detalles-venta" class="table color-table info-table form-material table-sm">
                        <thead>
                        <tr>
                            <th style="display:none;">#</th>
                            <th class="text-center" style="width:5%;">Cant.</th>
                            <th style="width:70%;">Artículo</th>
                            <th style="display:none;">P. Unitario</th>
                            <th style="display:none;">Desc.</th>
                            <th style="display:none;">Neto</th>
                            <th style="display:none;">IVA</th>
                            <th class="text-center" style="width:25%;">Precio</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                        <tfoot>
                        <tr>
                            <td style="display:none;">
                                ...
                            </td>
                            <td>{!! Form::number('', '', ['class' => 'form-control', 'id' => 'agregar-detalle-de-cantidad']) !!}</td>
                            <td>
                                <input type="text" class="form-control" id="agregar-detalle-de-articulo" placeholder="Nuevo Detalle...">
                            </td>
                            <td style="display:none;">
                                <div class="input-group">
                                    <div class="left-icon">$</div>
                                    {!! Form::number('', null, ['class' => 'form-control', 'readonly', 'tabindex' => '9999']) !!}
                                </div>
                            </td>
                            <td style="display:none;">
                                {!! Form::number('', null, ['class' => 'form-control', 'readonly', 'tabindex' => '9999', 'placeholder' => '$']) !!}
                            </td>
                            <td style="display:none;">
                                {!! Form::number('', null, ['class' => 'form-control', 'readonly', 'tabindex' => '9999', 'placeholder' => '%']) !!}
                            </td>
                            <td>
                                <div class="input-group">
                                    <div class="left-icon">$</div>
                                    {!! Form::number('', null, ['class' => 'form-control', 'readonly', 'tabindex' => '9999']) !!}
                                </div>
                            </td>
                            <td style="display:none;">
                                <div class="input-group">
                                    <div class="left-icon">%</div>
                                    {!! Form::number('', null, ['class' => 'form-control', 'readonly', 'tabindex' => '9999']) !!}
                                </div>
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <th colspan="3"></th>
                            <th style="display:none;"><span class="pull-right">Subtot<span class="hidden-sm hidden-xs">ales</span>.</span></th>
                            <th style="display:none;">
                                {!! Form::number('importe_neto', null, ['id' => 'importe-neto', 'class' => 'form-control', 'readonly']) !!}
                            </th>
                            <th style="display:none;">
                                {!! Form::number('importe_iva', null, ['id' => 'importe-iva', 'class' => 'form-control', 'step' => '0.01', 'readonly']) !!}
                            </th>
                            <th style="display:none;">
                                {!! Form::number('importe_bruto', null, ['id' => 'importe-bruto', 'class' => 'form-control', 'readonly']) !!}
                            </th>
                        </tr>
                        <tr style="display:none;">
                            <th colspan="4">
                            </th>
                            <th><span class="pull-right">Int<span class="hidden-sm hidden-xs">eres</span>.(+)/</br>Desc<span class="hidden-sm hidden-xs">uento</span>.(-)</span></th>
                            <th>
                                {!! Form::number('porcentaje_descuento', null, ['id' => 'porcentaje-impuesto', 'class' => 'form-control', 'step' => '0.01']) !!}
                            </th>
                            <th>
                                {!! Form::number('importe_descuento', null, ['id' => 'importe-impuesto', 'class' => 'form-control', 'step' => '0.01']) !!}
                            </th>
                            <th></th>
                        </tr>
                        <tr style="display:none;">
                            <th colspan="5">
                            </th>
                            <th><span class="pull-right">Imp<span class="hidden-sm hidden-xs">uestos</span>. Int<span class="hidden-sm hidden-xs">ernos</span>.</span></th>
                            <th>
                                {!! Form::text('importe_impuesto_interno', 0, ['id' => 'total-impuesto-interno', 'class' => 'form-control', 'readonly']) !!}
                            </th>
                            <th></th>
                        </tr>
                        <tr style="display:none;">
                            <th colspan="5">
                            </th>
                            <th><span class="pull-right">Total</span></th>
                            <th>
                                {!! Form::number('total', null, ['id' => 'total', 'class' => 'form-control', 'step' => '0.01', 'required' => 'required']) !!}
                            </th>
                            <th></th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
            {{ Form::hidden('guardar') }}
            <div class="col-3">
                <div class="card card-inverse card-info">
                    <div class="box bg-info text-center">
                        <h1 class="font-light text-white"><div id="totalGrande">$ 0.00</div></h1>
                        <h6 class="text-white">TOTAL A COBRAR</h6>
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::submit('Finalizar', ['class' => 'btn btn-primary btn-lg','style' => 'width:100%;']) }}
                </div>
                <div class="form-group">
                    {{ Form::button('<i class="fa fa-save"></i> Guardar', ['class' => 'btn btn-info btn-lg guardar','style' => 'width:100%;']) }}
                </div>
            </div>
        </div>

    </div>
</div>
