<div class="card">
    <div class="card-body">
        <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
        <div class="row form-group">
            <div class="col-md-4">
                {!! Form::label('cliente_id', 'Cliente') !!}
                <div class="row">
                    <div class="col-md-10">
                        <span class="input-icon input-icon-right">
                            <input id="buscar_cliente" name="buscar_cliente" class="form-control" placeholder="Buscar..." type="text" required="required" value="{{ isset($venta->cliente) ? $venta->cliente : $clientePorDefecto->razon_social }}">
                        </span>
                        <input id="cliente_id" name="cliente_id" type="hidden" value="{{ isset($venta->cliente) ? $venta->cliente->id : $clientePorDefecto->id }}">
                        {!! $errors->first('cliente_id', '<p class="text-danger">:message</p>') !!}
                    </div>
                    <div class="col-md-2">
                        <button type="button" class="btn btn-secondary" id="agregar-cliente"><i class="fa fa-plus"></i> </button>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                {!! Form::label('tipo_comprobante_id', 'Comprobante') !!}
                {!! Form::select('tipo_comprobante_id', $tipos_comprobantes, isset($venta->tipo_comprobante_id) ? $venta->tipo_comprobante_id : $tipo_comprobante_por_defecto, array('class' => 'form-control', 'required' => 'required')) !!}
                {!! $errors->first('tipo_comprobante_id', '<p class="text-danger">:message</p>') !!}
            </div>
            <div class="col-md-4">
                {!! Form::label('numero', 'Punto de Venta # Número') !!}
                <div class="input-group">
                    {!! Form::select('punto_venta_id', $puntos_de_ventas, isset($venta->punto_venta_id) ? $venta->punto_venta_id : (isset($punto_de_venta_por_defecto) ? $punto_de_venta_por_defecto->id : 0), ['id' =>'punto_venta_id' ,'class' => 'form-control', 'required' => 'required']) !!}
                    <div class="input-group-addon">
                        <i class="fa fa-hashtag"></i>
                    </div>
                    {!! Form::text('numero', isset($venta->numero) ? $venta->numero : $numero_factura, ['id' =>'numero' ,'class' => 'form-control', 'required' => 'required']) !!}
                </div>
                {!! $errors->first('numero', '<p class="text-danger">:message</p>') !!}
            </div>
            <div class="col-md-2">
                {!! Form::label('lista_precios_id', 'Lista de Precios') !!}
                {!! Form::select('lista_precios_id', $lista_precios, isset($venta->listaPrecios) ? $venta->listaPrecios->id : $lista_precios_defecto->valor, ['class' => 'form-control', 'required' => 'required']) !!}
                {!! $errors->first('lista_precios_id', '<p class="text-danger">:message</p>') !!}
            </div>
            <div class="col-md-3">
                {!! Form::label('fecha', 'Fecha') !!}
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    {!! Form::date('fecha', isset($venta->fecha) ? $venta->fecha : date('Y-m-d'), ['class' => 'form-control', 'required' => 'required', 'readonly']) !!}
                </div>
                {!! $errors->first('fecha', '<p class="text-danger">:message</p>') !!}
            </div>
            <div class="col-md-5">
                <div class="row">
                    <div class="col-md-7">
                        {!! Form::label('moneda_id', 'Moneda') !!}
                        {!! Form::select('moneda_id', $monedas, isset($venta->moneda) ? $venta->moneda->id : (isset($monedaPorDefecto) ? $monedaPorDefecto->id : ''), ['class' => 'form-control', 'id' => 'moneda_id', 'required' => 'required', isset($editar) ? 'disabled' : '']) !!}
                        {!! $errors->first('moneda_id', '<p class="text-danger">:message</p>') !!}
                    </div>
                    <div class="col-md-5">
                        {!! Form::label('cotizacion', 'Cotización') !!}
                        {!! Form::number('cotizacion', isset($venta) ? $venta->cotizacion : (isset($monedaPorDefecto) ? $monedaPorDefecto->cotizacion : 1), ['class' => 'form-control', 'step' => '0.01', 'id' => 'cotizacion', isset($editar) ? 'readonly' : '']) !!}
                        {!! $errors->first('cotizacion', '<p class="text-danger">:message</p>') !!}
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                {!! Form::label('usuario_id', 'Vendedor') !!}
                {!! Form::select('usuario_id', $vendedores, isset($venta->usuario) ? $venta->usuario->id : Auth::user()->id, ['class' => 'form-control', 'id' => 'usuario_id']) !!}
                {!! $errors->first('usuario_id', '<p class="text-danger">:message</p>') !!}
            </div>
        </div>
        <div class="table-responsive">
            <table id="tabla-detalles-venta" class="table color-table info-table form-material table-sm">
                <thead>
                <tr>
                    <th class="text-center">#</th>
                    <th class="text-center" style="width:8%;">Cantidad</th>
                    <th style="width:27%;">Artículo</th>
                    <th class="text-center" style="width:13%;">Pre/Uni. Neto</th>
                    <th class="text-center" style="width:14%;">Desc.</th>
                    <th class="text-center" style="width:13%;">Neto</th>
                    <th class="text-center" style="width:13%;">IVA</th>
                    <th class="text-center" style="width:13%;">Bruto</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @if (isset($venta))
                    @foreach($venta->detalles as $detalle)
                        @if($detalle->articulo)
                            <tr>
                                <td>
                                    {{ $loop->index + 1 }}
                                    <input type="hidden" class="id" name="detalles[{{ $detalle->id }}][id]" value="{{ $detalle->id }}">
                                </td>
                                <td>{!! Form::number('detalles['.$detalle->id.'][cantidad]', number_format($detalle->cantidad,0), ['class' => 'form-control cantidad text-center', 'step' => '1', 'min' => '0', 'max' => '', 'title' => '']) !!}</td>
                                <td>
                                    <input type="text" class="form-control buscar-articulo" name="detalles[{{ $detalle->id }}][descripcion]" placeholder="{{ trans('strings.backend.general.search_placeholder') }}" value="{{ ($detalle->descripcion) ? $detalle->descripcion : '('.$detalle->articulo->codigo.') '.$detalle->articulo->nombre }}">
                                    <input type="hidden" class="articulo-id" name="detalles[{{ $detalle->id }}][articulo_id]" value="{{ $detalle->articulo_id }}">
                                </td>
                                <td>
                                    <div class="input-group">
                                        <div class="left-icon">$</div>
                                        {!! Form::number('detalles['.$detalle->id.'][precio_neto]', $detalle->precio_neto, ['class' => 'form-control precio text-right', 'step' => '0.01', 'tabindex' => '9999']) !!}
                                        {!! Form::hidden('detalles['.$detalle->id.'][precio-real]', $detalle->precio_neto, ['class' => 'form-control precio-real', 'step' => '0.01', 'tabindex' => '9999']) !!}
                                    </div>
                                </td>
                                <td>
                                    <div class="input-group">
                                        <div class="left-icon">%</div>
                                        {!! Form::number('detalles['.$detalle->id.'][descuento]', null, ['class' => 'form-control descuento text-right', 'step' => '0.01', 'tabindex' => '9999']) !!}
                                    </div>
                                </td>
                                <td>
                                    <div class="input-group">
                                        <div class="left-icon">$</div>
                                        {!! Form::number('detalles['.$detalle->id.'][total_neto]', $detalle->cantidad*$detalle->precio_neto, ['class' => 'form-control subtotal-neto text-right', 'readonly', 'step' => '0.01', 'tabindex' => '9999']) !!}
                                    </div>
                                </td>
                                <td><div class="input-group">
                                        <div class="left-icon">%</div>
                                        {!! Form::text('detalles['.$detalle->id.'][iva]', number_format($porcentajes_iva[$detalle->porcentaje_iva], 2), array('class' => 'form-control iva text-right', 'readonly', 'tabindex' => '9999')) !!}
                                    </div>
                                    <input type="hidden" class="porcentaje-iva" name="detalles[{{ $detalle->id }}][porcentaje_iva]" value="{{ $detalle->porcentaje_iva }}">
                                </td>
                                <td>
                                    <div class="input-group">
                                        <div class="left-icon">$</div>
                                        {!! Form::number('detalles['.$detalle->id.'][total_bruto]', number_format(($detalle->cantidad*$detalle->precio_neto) * ($porcentajes_iva[$detalle->porcentaje_iva]/100 + 1), 2, '.', ''), ['class' => 'form-control text-right subtotal-bruto', 'readonly', 'step' => '0.01', 'tabindex' => '9999']) !!}
                                        <input type="hidden" class="impuesto-interno" name="detalles[{{ $detalle->id }}][importe_impuesto_interno]" value="{{ $detalle->importe_impuesto_interno }}">
                                    </div>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-danger btn-sm btn-circle borrar-detalle text-right" name="detalles[{{ $detalle->id }}][eliminar]">
                                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                                    </button>
                                    <input type="hidden" class="estado" name="detalles[{{ $detalle->id }}][estado]" value="1">
                                </td>
                            </tr>
                        @endif
                    @endforeach
                @endif
                </tbody>
                <tfoot>
                <tr>
                    <td>
                        ...
                    </td>
                    <td>{!! Form::number('', '', ['class' => 'form-control', 'id' => 'agregar-detalle-de-cantidad']) !!}</td>
                    <td>
                        <input type="text" class="form-control" id="agregar-detalle-de-articulo" placeholder="Nuevo Detalle...">
                    </td>
                    <td>
                        <div class="input-group">
                            <div class="left-icon">$</div>
                            {!! Form::number('', null, ['class' => 'form-control', 'readonly', 'tabindex' => '9999']) !!}
                        </div>
                    </td>
                    <td>
                        <div class="input-group">
                            <div class="left-icon">%</div>
                            {!! Form::number('', null, ['class' => 'form-control', 'readonly', 'tabindex' => '9999']) !!}
                        </div>
                    </td>
                    <td>
                        <div class="input-group">
                            <div class="left-icon">$</div>
                            {!! Form::number('', null, ['class' => 'form-control', 'readonly', 'tabindex' => '9999']) !!}
                        </div>
                    </td>
                    <td>
                        <div class="input-group">
                            <div class="left-icon">%</div>
                            {!! Form::number('', null, ['class' => 'form-control', 'readonly', 'tabindex' => '9999']) !!}
                        </div>
                    </td>
                    <td>
                        <div class="input-group">
                            <div class="left-icon">$</div>
                            {!! Form::number('', null, ['class' => 'form-control', 'readonly', 'tabindex' => '9999']) !!}
                        </div>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <th colspan="4">
                        <a href="javascript:void(0)" class="btn btn-info btn-sm" id="agregar-detalle">
                            <i class="fa fa-plus" aria-hidden="true"></i> Agregar Detalle
                        </a>
                    </th>
                    <th><span class="pull-right">Subtot<span class="hidden-sm hidden-xs">ales</span>.</span></th>
                    <th>
                        <div class="input-group">
                            <div class="left-icon">$</div>
                            {!! Form::number('importe_neto', null, ['id' => 'importe-neto', 'class' => 'form-control text-right', 'readonly']) !!}
                        </div>
                    </th>
                    <th>
                        <div class="input-group">
                            <div class="left-icon">$</div>
                            {!! Form::number('importe_iva', null, ['id' => 'importe-iva', 'class' => 'form-control text-right', 'step' => '0.01', 'readonly']) !!}
                        </div>
                    </th>
                    <th>
                        <div class="input-group">
                            <div class="left-icon">$</div>
                            {!! Form::number('importe_bruto', (isset($venta) ? $venta->importe_neto + $venta->importe_iva + + $venta->importe_impuesto_interno : null), ['id' => 'importe-bruto', 'class' => 'form-control text-right', 'readonly']) !!}
                        </div>
                    </th>
                </tr>
                <tr>
                    <th colspan="5">
                    </th>
                    <th><span class="pull-right">Int<span class="hidden-sm hidden-xs">eres</span>.(+)/</br>Desc<span class="hidden-sm hidden-xs">uento</span>.(-)</span></th>
                    <th>
                        <div class="input-group">
                            <div class="left-icon">%</div>
                            {!! Form::number('porcentaje_descuento', null, ['id' => 'porcentaje-impuesto', 'class' => 'form-control text-right', 'step' => '0.01']) !!}
                        </div>
                    </th>
                    <th>
                        <div class="input-group">
                            <div class="left-icon">$</div>
                            {!! Form::number('importe_descuento', null, ['id' => 'importe-impuesto', 'class' => 'form-control text-right', 'step' => '0.01']) !!}
                        </div>
                    </th>
                    <th></th>
                </tr>
                <tr>
                    <th colspan="5" rowspan="2">
                        <div class="form-group">
                            {!! Form::label('descripcion', 'Comentario') !!}
                            {!! Form::textarea('descripcion', null, ['class' => 'form-control', 'rows' => '4']) !!}
                            {!! $errors->first('descripcion', '<p class="text-danger">:message</p>') !!}
                        </div>
                    </th>
                    <th rowspan="2">
                    </th>
                    <th><span class="pull-right">Imp<span class="hidden-sm hidden-xs">uestos</span>. Int<span class="hidden-sm hidden-xs">ernos</span>.</span></th>
                    <th>
                        <div class="input-group">
                            <div class="left-icon">$</div>
                            {!! Form::number('importe_impuesto_interno', 0, ['id' => 'total-impuesto-interno', 'class' => 'form-control text-right', 'step' => '0.01', 'readonly']) !!}
                        </div>
                    </th>
                    <th></th>
                </tr>
                <tr>
                    <th colspan="3">
                        <div class="card card-inverse card-info">
                            <div class="box bg-info text-center">
                                <h1 class="font-light text-white">
                                    <div class="pull-left">TOTAL:</div>
                                    <div class="pull-right" id="totalGrande">$ 0.00</div>
                                </h1>
                            </div>
                        </div>
                    </th>
                    {!! Form::number('total', null, ['id' => 'total', 'class' => 'd-none', 'step' => '0.01', 'required' => 'required']) !!}
                </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>

<div id="alerta_cuenta_corriente" class="alert alert-warning" style="display:none;">
    <button type="button" class="close">
        <i class="ace-icon fa fa-times"></i>
    </button>
    <i id="icono_cuenta_corriente" class="ace-icon fa fa-exclamation-circle red" aria-hidden="true" style="display: none;"></i>
    <strong>Cuenta Corriente : </strong> <i class="fa fa-circle" aria-hidden="true"></i> Saldo Actual <strong id="saldo_actual"></strong> <i class="fa fa-circle" aria-hidden="true"></i> Saldo Máximo  <strong id="saldo_maximo"></strong>
</div>

@include('frontend.ventas.cobro.cobros')

{{ Form::hidden('guardar') }}
<div class="card">
    <div class="card-body">
        {{ Form::button('<i class="fa fa-save"></i> Guardar', ['class' => 'btn btn-info btn-lg guardar']) }}
        {{ Form::submit('Finalizar', ['class' => 'btn btn-themecolor btn-lg pull-right']) }}
    </div>
</div>

@include('frontend.personas.cliente.cliente_dialog')
