@extends('frontend.layouts.app')

@section ('title', trans('labels.frontend.venta_titulo')." - ".trans('buttons.general.crud.create'))

@section('after-styles')
    {{ Html::style("css/frontend/plugins/jquery/jquery-ui.min.css") }}
@endsection

@section('content')
    <div class="row page-titles">
        <div class="col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('labels.frontend.venta_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item"><a href="{{url('/ventas/venta')}}">{{ trans('labels.frontend.venta_titulo') }}</a></li>
                <li class="breadcrumb-item active">{{ trans('buttons.general.crud.create') }} {{ trans('labels.frontend.venta') }} Rapida</li>
            </ol>
        </div>
        <div class="col-4">
            <div class="pull-right">
                <a href="{{ url('/ventas/venta') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans('buttons.general.crud.back') }}</span>
                </a>
            </div>
        </div>
    </div>

    @tips($_SERVER['REQUEST_URI'])

    @include('includes.partials.messages')

    {!! Form::open(['url' => '/ventas/venta/guardarVentaRapida', 'method' => 'post', 'class' => 'form-horizontal', 'files' => true]) !!}
        @include ('frontend.ventas.venta.form_rapido')
    {!! Form::close() !!}

@endsection

@section('after-scripts')
    {{ Html::script("js/frontend/plugins/jquery/jquery-ui.min.js") }}

    <script>
        $( document ).ready(function() {

            $(document).on("keydown.autocomplete", ".buscar-articulo", function (e) {
                if (e.keyCode == 13) {
                    e.preventDefault();
                }else{
                    $(this).autocomplete({
                        source: '{{ url('/articulos/articulo/obtenerPrecioArticulo') }}/{{$listaPrecio->valor}}',
                        minLength: 1,
                        autoFocus: true,
                        select: function (e, ui) {
                            var fila = $(this).parent().parent();
                            var cotizacion = 1;

                            $(this).val(ui.item.value);
                            $(this).parent().find('input.articulo-id').val(ui.item.id);
                            if({{$moneda->id}} != ui.item.moneda_id){
                                if(ui.item.cotizacion == 1){
                                    ui.item.precio = (ui.item.precio / cotizacion).toFixed(2);
                                }else{
                                    ui.item.precio = (ui.item.precio * ui.item.cotizacion).toFixed(2);
                                }
                            }
                            fila.find('td input.precio').val(ui.item.precio);
                            fila.find('td input.precio-real').val(ui.item.precio);
                            fila.find('td input.iva').val(ui.item.iva);
                            fila.find('td input.porcentaje-iva').val(ui.item.porcentaje_iva);
                            fila.find('td input.impuesto-interno').val(ui.item.impuesto_interno);
                            if(ui.item.existencia != null){
                                ui.item.existencia = (ui.item.existencia).toFixed(0);
                                var cantidad = parseInt(fila.find('td input.cantidad').val());
                                if(ui.item.existencia < cantidad && $.isNumeric(cantidad)) {
                                    fila.find('td input.cantidad').val(ui.item.existencia);
                                }
                                fila.find('td input.cantidad').attr('max', ui.item.existencia);
                                fila.find('td input.cantidad').attr('title', 'En stock: '+ui.item.existencia);
                            }else{
                                fila.find('td input.cantidad').attr('max', '');
                                fila.find('td input.cantidad').attr('title', '');
                            }
                            agregarDetalle();
                        },
                        response: function(e, ui) {
                          if (ui.content.length == 1 && !$(this).parent().find('input.articulo-id').val()){ // Si solo me devuelve un articulo, lo autocargo
                                var fila = $(this).parent().parent();
                                var cotizacion = 1;

                                ui.item = ui.content[0];

                                $(this).val(ui.item.value);
                                $(this).parent().find('input.articulo-id').val(ui.item.id);
                                if({{$moneda->id}} != ui.item.moneda_id){
                                    if(ui.item.cotizacion == 1){
                                        ui.item.precio = (ui.item.precio / cotizacion).toFixed(2);
                                    }else{
                                        ui.item.precio = (ui.item.precio * ui.item.cotizacion).toFixed(2);
                                    }
                                }
                                fila.find('td input.precio').val(ui.item.precio);
                                fila.find('td input.precio-real').val(ui.item.precio);
                                fila.find('td input.iva').val(ui.item.iva);
                                fila.find('td input.porcentaje-iva').val(ui.item.porcentaje_iva);
                                fila.find('td input.impuesto-interno').val(ui.item.impuesto_interno);
                                if(ui.item.existencia != null){
                                    ui.item.existencia = (ui.item.existencia).toFixed(0);
                                    var cantidad = parseInt(fila.find('td input.cantidad').val());
                                    if(ui.item.existencia < cantidad && $.isNumeric(cantidad)) {
                                        fila.find('td input.cantidad').val(ui.item.existencia);
                                    }
                                    fila.find('td input.cantidad').attr('max', ui.item.existencia);
                                    fila.find('td input.cantidad').attr('title', 'En stock: '+ui.item.existencia);
                                }else{
                                    fila.find('td input.cantidad').attr('max', '');
                                    fila.find('td input.cantidad').attr('title', '');
                                }
                                agregarDetalle();
                           }
                        }
                    });
                }
            });

            $(document).on('click', '.borrar-detalle', function () {
                var fila = $(this).parent().parent();

                fila.hide(1000, function(){
                    fila.find('td input.estado').val(0);
                    fila.find('td input.subtotal-neto').val(0);
                    fila.find('td input.subtotal-impuesto-interno').val(0);
                    fila.find('td input.subtotal-bruto').val(0);
                    fila.find('input').removeAttr('required');

                    refrescarTotales();
                });
            });

            calcularSubtotales();
            calcularPorcentajeImpuesto();
            refrescarTotales();
        });

        $("#agregar-detalle").click(function(){
            agregarDetalle();
        });

        $("#agregar-detalle-de-cantidad").on('focusin', function(){
            agregarDetalle('cantidad');
        });

        $("#agregar-detalle-de-articulo").on('focusin', function(){
            agregarDetalle('descripcion');
        });

        function agregarDetalle(inputName) {
            var numero_orden = $('table#tabla-detalles-venta tbody tr').length+ 1;
            $("#tabla-detalles-venta").append('<tr>' +
                '<td style="display:none;">' +
                numero_orden +
                '<input type="hidden" class="id" name="detalles['+(numero_orden*-1)+'][id]" value="'+(numero_orden*-1)+'">' +
                '</td>' +
                '<td>' +
                '<input class="form-control cantidad text-center" name="detalles['+(numero_orden*-1)+'][cantidad]" type="number" value="1" max="" min="0" title="" required="required">' +
                '</td>' +
                '<td>' +
                '<input class="form-control buscar-articulo" name="detalles['+(numero_orden*-1)+'][descripcion]"  placeholder="Buscar..." type="text">' +
                '<input class="articulo-id" name="detalles['+(numero_orden*-1)+'][articulo_id]" type="hidden" value="">' +
                '</td>' +
                '<td style="display:none;">' +
                '<div class="input-group">' +
                '<div class="left-icon">$</div>' +
                '<input class="form-control precio text-right" name="detalles['+(numero_orden*-1)+'][precio_neto]" type="number" step="0.01" tabindex="99999">' +
                '<input class="form-control precio-real" name="detalles['+(numero_orden*-1)+'][precio_real]" type="hidden" step="0.01" tabindex="99999">' +
                '</div>' +
                '</td>' +
                '<td style="display:none;">' +
                '<div class="input-group">' +
                '<div class="left-icon">%</div>' +
                '<input class="form-control descuento text-right" name="detalles['+(numero_orden*-1)+'][descuento]" type="number" step="0.01" tabindex="99">' +
                '</div>' +
                '</td>' +
                '<td style="display:none;">' +
                '<input class="form-control subtotal-neto" name="detalles['+(numero_orden*-1)+'][total_neto]" placeholder="$" type="number" step="0.01" readonly tabindex="99999">' +
                '</td>' +
                '<td style="display:none;">' +
                '<input class="form-control iva" name="detalles['+(numero_orden*-1)+'][iva]" type="number" placeholder="%" step="0.01" readonly tabindex="99999">' +
                '<input type="hidden" class="porcentaje-iva" name="detalles['+(numero_orden*-1)+'][porcentaje_iva]" value="">' +
                '</td>' +
                '<td>' +
                '<div class="input-group">' +
                '<div class="left-icon">$</div>' +
                '<input class="form-control subtotal-bruto text-right" name="detalles['+(numero_orden*-1)+'][total_bruto]" type="number" step="0.01" tabindex="99999">' +
                '</div>' +
                '<input type="hidden" class="impuesto-interno" name="detalles['+(numero_orden*-1)+'][importe_impuesto_interno]" value="">' +
                '<input type="hidden" class="subtotal-impuesto-interno" name="detalles['+(numero_orden*-1)+'][subtotal_impuesto_interno]" value="0">' +
                '</td>' +
                '<td class="text-center">' +
                '<a class="btn btn-danger btn-circle btn-sm borrar-detalle" name="detalles['+(numero_orden*-1)+'][eliminar]" tabindex="99999">' +
                '<i class="fa fa-trash-o" aria-hidden="true"></i>' +
                '</a>' +
                '<input type="hidden" class="estado" name="detalles['+(numero_orden*-1)+'][estado]" value="1">' +
                '</td>' +
                '</tr>');
            if(inputName != null){
                $("input[name='detalles["+(numero_orden*-1)+"]["+inputName+"]']").focus();
            }else{
                $("input[name='detalles["+(numero_orden*-1)+"][descripcion]']").focus();
            }
            calcularSubtotales();
        }

        $("#porcentaje-impuesto").on('change', function (){
            calcularImporteImpuesto();
            refrescarTotales();
        });

        $("#importe-impuesto").on('change', function (){
            calcularPorcentajeImpuesto();
            refrescarTotales();
        });

        $(".guardar").on("click", function(e){
            e.preventDefault();
            $("[name='guardar']").val(1);
            $('.form-horizontal').submit();
        });

        function refrescarTotales() {
            var primerProducto = $("#tabla-detalles-venta tbody tr:visible").first().find('.precio').val();
            if(primerProducto){
                $.ajax({
                    type: "POST",
                    url: '{{ url("/ventas/calcularTotales") }}',
                    data: $('form').serialize(),
                    success: function( resultados ) {
                        $('#importe-neto').val(resultados['netos']['total']);
                        $('#importe-iva').val(resultados['ivas']['total']);
                        $('#importe-bruto').val(resultados['bruto']);
                        $('#total-impuesto-interno').val(resultados['impuestosInternos']);
                        $('#total').val(resultados['total']);
                        $('#totalGrande').html('$ '+resultados['total']);
                    }
                });
            }
        }

        function calcularSubtotales(){
            var $tablaTodasFilas = $("#tabla-detalles-venta tbody tr");
            $tablaTodasFilas.each(function (index) {
                var $tablaFila = $(this);

                $tablaFila.find('.cantidad ,.precio, .buscar-articulo').on('change', function () {
                    var cantidad = parseInt($tablaFila.find('.cantidad').val(), 10);
                    var precioReal = parseFloat($tablaFila.find('.precio-real').val());
                    var precio = parseFloat($tablaFila.find('.precio').val());
                    var descuento = parseFloat($tablaFila.find('.descuento').val());
                    var iva = parseFloat($tablaFila.find('.iva').val());
                    var impuesto = parseFloat($tablaFila.find('.impuesto-interno').val());

                    var subtotalNeto = precio * cantidad;
                    var importeIva = subtotalNeto * iva / 100;
                    var subtotalImpuestoInterno = impuesto * cantidad;
                    var subtotalBruto = subtotalNeto + importeIva + subtotalImpuestoInterno;

                    subtotalNeto = subtotalNeto + importeIva;

                    if(precio != precioReal){ // Si se modifico el precio tengo que actualizar el porcentaje de descuento
                        descuento = ((precio - precioReal)/precioReal)*100;
                    }
                    if (!isNaN(descuento)) {
                        $tablaFila.find('.descuento').val(descuento.toFixed(2));
                    }
                    if (!isNaN(subtotalNeto)) {
                        $tablaFila.find('.subtotal-neto').val(subtotalNeto.toFixed(2));
                    }
                    if (!isNaN(subtotalImpuestoInterno)) {
                        $tablaFila.find('.subtotal-impuesto-interno').val(subtotalImpuestoInterno.toFixed(2));
                    }
                    if (!isNaN(subtotalBruto)) {
                        $tablaFila.find('.subtotal-bruto').val(subtotalBruto.toFixed(2));
                    }else{
                        $tablaFila.find('.subtotal-bruto').val(subtotalNeto.toFixed(2));
                    }
                    refrescarTotales();
                });

                $tablaFila.find('.descuento').on('change', function () {
                    var cantidad = parseInt($tablaFila.find('.cantidad').val(), 10);
                    var precioReal = parseFloat($tablaFila.find('.precio-real').val());
                    var precio = parseFloat($tablaFila.find('.precio').val());
                    var descuento = parseFloat($tablaFila.find('.descuento').val());
                    var iva = parseFloat($tablaFila.find('.iva').val());
                    var impuesto = parseFloat($tablaFila.find('.impuesto-interno').val());

                    if (!isNaN(descuento)) {
                        precio = (precioReal * descuento)/100 + precioReal;
                    } else {
                        precio = precioReal;
                    }
                    var subtotalNeto = precio * cantidad;
                    var importeIva = subtotalNeto * iva / 100;
                    var subtotalImpuestoInterno = impuesto * cantidad;
                    var subtotalBruto = subtotalNeto + importeIva + subtotalImpuestoInterno;

                    if (!isNaN(precio)) {
                        $tablaFila.find('.precio').val(precio.toFixed(2));
                    }
                    if (!isNaN(subtotalNeto)) {
                        $tablaFila.find('.subtotal-neto').val(subtotalNeto.toFixed(2));
                    }
                    if (!isNaN(subtotalImpuestoInterno)) {
                        $tablaFila.find('.subtotal-impuesto-interno').val(subtotalImpuestoInterno.toFixed(2));
                    }
                    if (!isNaN(subtotalBruto)) {
                        $tablaFila.find('.subtotal-bruto').val(subtotalBruto.toFixed(2));
                    }else{
                        $tablaFila.find('.subtotal-bruto').val(subtotalNeto.toFixed(2));
                    }
                    refrescarTotales();
                });

                $tablaFila.find('.subtotal-bruto').on('change', function () {
                    var cantidad = parseInt($tablaFila.find('.cantidad').val(), 10);
                    var precioReal = parseFloat($tablaFila.find('.precio-real').val());
                    var precio = parseFloat($tablaFila.find('.precio').val());
                    var precioFinal = parseFloat($tablaFila.find('.subtotal-bruto').val());
                    var descuento = parseFloat($tablaFila.find('.descuento').val());
                    var iva = parseFloat($tablaFila.find('.iva').val());

                    var subtotalNeto = precioFinal / ((iva / 100) + 1);
                    precio = subtotalNeto / cantidad;

                    descuento = ((precio - precioReal)/precioReal)*100;
                    if (!isNaN(descuento)) {
                        $tablaFila.find('.descuento').val(descuento.toFixed(2));
                    }
                    if (!isNaN(subtotalNeto)) {
                        $tablaFila.find('.subtotal-neto').val(subtotalNeto.toFixed(2));
                    }
                    if (!isNaN(precio)) {
                        $tablaFila.find('.precio').val(precio.toFixed(2));
                    }
                    refrescarTotales();
                });

            });
        }

        function calcularImporteImpuesto(){
            var importeBruto = $('#importe-bruto').val();
            var procentajeImpuesto = $('#porcentaje-impuesto').val();
            var importeImpuesto = parseFloat(importeBruto) * parseFloat(procentajeImpuesto) / 100;
            if (!isNaN(importeImpuesto)) {
                $('#importe-impuesto').val(importeImpuesto.toFixed(2));
            }else{
                $('#importe-impuesto').val('0.0');
            }
        }

        function calcularPorcentajeImpuesto(){
            var importeBruto = $('#importe-bruto').val();
            var importeImpuesto = $('#importe-impuesto').val();
            var procentajeImpuesto = parseFloat(importeImpuesto) * 100 / parseFloat(importeBruto);
            if (!isNaN(procentajeImpuesto)) {
                $('#porcentaje-impuesto').val(procentajeImpuesto.toFixed(2));
            }else{
                $('#porcentaje-impuesto').val('0.0');
            }
        }

    </script>

@endsection