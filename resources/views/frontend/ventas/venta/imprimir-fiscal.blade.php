@extends('frontend.layouts.app')

@section ('title', trans('labels.frontend.venta_titulo'))

@section('content')
    <div class="main-content">
        <div class="main-content-inner">
            <div class="page-header">
                <h1>

                </h1>
            </div>

        </div>
    </div>
@endsection

@section('after-scripts')
    {{ Html::script("js/frontend/plugins/fiscal/ifu.js") }}
    {{ Html::script("js/frontend/custom.js") }}

<script>

    host = "{{ $venta->puntoVenta->host_fiscal }}";
    modelo = {{ $venta->puntoVenta->modelo_impresora_fiscal }};
    puerto = {{ $venta->puntoVenta->puerto_fiscal }};
    baudios = {{ $venta->puntoVenta->baudios_fiscal }};

    @if($venta->cliente->condicionIva == "Responsable Inscripto")
        condicionIva = riResponsableInscripto;
        tipoFactura = tcFactura_A;
    @elseif($venta->cliente->condicionIva == "Monotributista")
        condicionIva = riMonotributo;
        tipoFactura = tcFactura_B;
    @elseif($venta->cliente->condicionIva == "Exento")
        condicionIva = riExento;
        tipoFactura = tcFactura_B;
    @else
        condicionIva = riConsumidorFinal;
        tipoFactura = tcFactura_B;
    @endif

    @if($empresa->condicionIva != "Responsable Inscripto")
        tipoFactura = tcFactura_C;
    @endif

    function ultimoNumeroComprobante(){
        var driver = new Driver();
        driver.modelo = modelo;
        driver.puerto = puerto;
        driver.baudios = baudios;
        try {
            driver.iniciarTrabajo();
            driver.ultimoComprobante(tipoFactura);
            driver.finalizarTrabajo();
            numeroComprobante = parseInt(driver.response.ultimoNumero) + 1;
        } catch (e){
            numeroComprobante = 0;
            cambiarEstadoFacturacion("ERROR");
            swal("Error", e, "error");
        }
    }

    function imprimir(){
        var driver = new Driver();
        driver.host = host; //Cambiar por el ip de la maquina de la fiscal si es remota
        driver.modelo = modelo;
        driver.puerto = puerto;
        driver.baudios = baudios;

        @if(strlen($venta->cliente->dni) > 8)
            tipoDocumento = tdCUIT;
        @else
            tipoDocumento = tdDNI;
        @endif

        try {
            driver.iniciarTrabajo();
            driver.cancelarComprobante();
            driver.abrirComprobante(tipoFactura);
        } catch (e){}

        try {
            driver.iniciarTrabajo();
            driver.cancelarComprobante();

            @if(!($venta->cliente->condicionIva == "Consumidor Final" && $venta->total <= 1000))
                driver.datosCliente("{{ $venta->cliente->razon_social }}", tipoDocumento, "{{ $venta->cliente->dni }}", condicionIva, "{{ (isset($venta->cliente->domicilio->direccion)) ? $venta->cliente->domicilio->direccion : ' ' }}");
            @endif

            if(driver.modelo == 24){
                @if($venta->cliente->condicionIva == "Consumidor Final" && $venta->total <= 1000)
                    driver.datosCliente("Consumidor Final", tdDNI, "00000000", riConsumidorFinal, " ");
                @endif
                driver.documentoDeReferencia2g(tcRemito, "0001-00000023");
            }

            driver.abrirComprobante(tipoFactura);

            @foreach($venta->detalles as $detalle)
            <?php
            if($venta->tipo_comprobante_id != 1){ // No es Factura A
                $detalle->precio_neto = $detalle->precio_neto + $detalle->precio_iva;
                if($detalle->importe_impuesto_interno){
                    $detalle->precio_neto = $detalle->precio_neto + $detalle->importe_impuesto_interno;
                }
            }
            ?>
                driver.imprimirItem2g("{{ $detalle->descripcion }}", {{ number_format($detalle->cantidad , 0) }}, {{ number_format($detalle->precio_neto, 2, '.', '') }}, {{$porcentajes_iva[$detalle->porcentaje_iva]}}, {{($detalle->importe_impuesto_interno) ? $detalle->importe_impuesto_interno : 0}}, Gravado, tiFijo, 1, "{{ $detalle->articulo->codigo_barra }}", "{{ $detalle->articulo->codigo }}", Unidad);
            @endforeach

            @if($venta->importe_descuento || $venta->interesesCobro())
                driver.imprimirDescuentoGeneral("Impuesto/Descuento (%)", {{ number_format(($venta->importe_descuento + $venta->interesesCobro()) * -1, 2, '.', '') }});
            @endif

            @foreach($venta->detallesCobros as $detalleCobro)
             cuotas = 0;
             @if($detalleCobro->forma_cobro == 0)
                 formaDePago = Efectivo;
             @elseif($detalleCobro->forma_cobro == 1)
                 formaDePago = Cheque;
             @elseif($detalleCobro->forma_cobro == 2)
                 formaDePago = Deposito;
             @elseif($detalleCobro->forma_cobro == 3)
                 formaDePago = TarjetaDeCredito;
                 cuotas = 1;
             @else
                 formaDePago = CuentaCorriente;
             @endif
             driver.imprimirPago2g("{{ $detalleCobro->descripcion }}", {{ number_format($detalleCobro->monto + $detalleCobro->interes, 2, '.', '') }}, "", formaDePago, cuotas, "", "");
             // TODO Hacer el calculo de cuotas de tarjetas de credito
            @endforeach
            driver.cerrarComprobante();
            driver.finalizarTrabajo();

            cambiarEstadoFacturacion("FACTURADO");
            swal({
                title: "Operación realizada con éxito!",
                timer: 2000,
                type: "success",
                showConfirmButton: false
            }).then(
            @if($ventaRapida)
                window.location.href="{{ url('/ventas/venta/create_rapido') }}"
            @else
                window.location.href="{{ url('/ventas/venta/create') }}"
            @endif
            )
        } catch (e){
            cambiarEstadoFacturacion("ERROR");
            swal("Error", e, "error");
        }
    }

    function validarEstadoFacturacion(){
        $.ajax({
            type: "POST",
            url: '{{ url("/ventas/venta/obtenerEstadoVenta") }}',
            data: {venta_id: {{$venta->id}} },
            success: function( msg ) {
                if(msg == 1){ // Si la venta ya fue correctamente facturada
                    window.location.href = "{{ url('/ventas/venta') }}";
                } else {
                    imprimir();
                }
            }
        });
    }

    function cambiarEstadoFacturacion(estado) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            }
        });
        $.ajax({
            type: "POST",
            url: '{{ url("/ventas/venta/setEstadoFacturacion") }}',
            data: { venta_id: {{ $venta->id }}, estado: estado, numeroComprobante: numeroComprobante }
        });
    }

    $(document).ready(function() {
        swal({
            title: 'Realizando Factura',
            text: 'Imprimiendo Factura... Por favor aguarde...',
            timer: 10000,
            onOpen: () => {
                swal.showLoading()
            }
        });
        ultimoNumeroComprobante();
        validarEstadoFacturacion();
    } );

</script>

@endsection
