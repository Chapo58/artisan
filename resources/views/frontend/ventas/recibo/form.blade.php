<div class="card">
    <div class="card-body">
        <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
        <div class="row form-group">
            <div class="col-md-3">
                {!! Form::label('cliente_id', 'Cliente') !!}
                @if($cliente == null)
                    <input id="buscar_cliente" name="buscar_cliente" class="form-control" placeholder="Buscar..." type="text" required="required" value="{{ isset($venta->cliente) ? $venta->cliente : '' }}" {{ isset($editar) ? 'disabled' : '' }}>
                    <input id="cliente_id" name="cliente_id" type="hidden" value="{{ isset($venta->cliente) ? $venta->cliente->id : '' }}">
                @else
                    <input class="form-control" type="text" required="required" value="{{ $cliente->razon_social }}" disabled>
                    <input id="cliente_id" name="cliente_id" type="hidden" value="{{ $cliente->id }}">
                @endif
                {!! $errors->first('cliente_id', '<p class="text-danger">:message</p>') !!}
            </div>
            <div class="col-md-3">
                {!! Form::label('numero', 'Número') !!}
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-hashtag"></i>
                    </div>
                    {!! Form::text('numero', isset($recibo) ? $recibo->numero : $numero_recibo, ['id' =>'numero' ,'class' => 'form-control', 'required' => 'required', 'readonly']) !!}
                </div>
                {!! $errors->first('numero', '<p class="text-danger">:message</p>') !!}
            </div>
            <div class="col-md-3">
                {!! Form::label('fecha', 'Fecha') !!}
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    {!! Form::text('fecha', isset($recibo) ? $recibo->fecha->format('d/m/Y H:i') : $fecha_hoy, ['class' => 'form-control fecha', 'required' => 'required', isset($editar) ? 'disabled' : '']) !!}
                </div>
                {!! $errors->first('fecha', '<p class="text-danger">:message</p>') !!}
            </div>
            <div class="col-md-3">
                <div class="row">
                    <div class="col-md-7">
                        {!! Form::label('moneda_id', 'Moneda') !!}
                        {!! Form::select('moneda_id', $monedas, isset($monedaPorDefecto) ? $monedaPorDefecto->id : '', ['class' => 'form-control', 'id' => 'moneda_id', 'required' => 'required', isset($editar) ? 'disabled' : '']) !!}
                        {!! $errors->first('moneda_id', '<p class="text-danger">:message</p>') !!}
                    </div>
                    <div class="col-md-5">
                        {!! Form::label('cotizacion', 'Cotización') !!}
                        {!! Form::number('cotizacion', isset($monedaPorDefecto) ? $monedaPorDefecto->cotizacion : 1, ['class' => 'form-control', 'step' => '0.01', 'id' => 'cotizacion', isset($editar) ? 'disabled' : '']) !!}
                        {!! $errors->first('cotizacion', '<p class="text-danger">:message</p>') !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <table id="tabla-detalles-recibo" class="table color-table info-table">
                <thead>
                <tr>
                    <th style="width:70%;">Descripción</th>
                    <th style="width:30%;">Monto</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @if (isset($detalles))
                    @foreach($detalles as $detalle)
                        <tr>
                            <td>
                                {!! Form::text('detalles['.$detalle->id.'][descripcion]', Carbon\Carbon::parse($detalle->fecha)->format('d/m/Y H:i').' - '.$detalle->descripcion.' - Comprobante Nº '.$detalle->detalle, ['class' => 'form-control', 'tabindex' => '9999', 'disabled']) !!}
                                <input type="hidden" class="id" name="detalles[{{ $detalle->id }}][id]" value="{{ $detalle->id }}">
                                <input type="hidden" name="detalles[{{ $detalle->id }}][tipo]" value="{{ $detalle->tipo }}">
                            </td>
                            <td>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-usd"></i>
                                    </div>
                                    {!! Form::number('detalles['.$detalle->id.'][monto]', $detalle->monto, ['class' => 'form-control detalle-recibo-monto', 'readonly', 'placeholder' => '$']) !!}
                                </div>
                            </td>
                            <td>
                                <button type="button" class="btn btn-danger borrar-detalle" name="detalles[{{ $detalle->id }}][eliminar]">
                                    <i class="fa fa-trash-o" aria-hidden="true"></i>
                                </button>
                                <input type="hidden" class="estado" name="detalles[{{ $detalle->id }}][estado]" value="1">
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
                <tfoot>
                <tr>
                    <th><span class="pull-right">Total Cobro</span></th>
                    <th>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-usd"></i>
                            </div>
                            {!! Form::number('total', '', ['id' => 'total', 'class' => 'form-control', 'step' => '0.01', 'required' => 'required', 'placeholder' => '$']) !!}
                        </div>
                    </th>
                    <th></th>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>

@include('frontend.ventas.cobro.cobros')

<div class="card">
    <div class="card-body">
        <div class="form-group">
            {{ Form::submit(isset($submitButtonText) ? $submitButtonText : trans('buttons.general.save'), ['class' => 'btn btn-themecolor btn-lg pull-right']) }}
        </div>
    </div>
</div>

<div class="card">
    <div class="card-body">
        <div class="row form-group">
            <div class="col-md-12">
                {!! Form::label('descripcion', 'Comentario') !!}
                {!! Form::textarea('descripcion', null, ['class' => 'form-control']) !!}
                {!! $errors->first('descripcion', '<p class="text-danger">:message</p>') !!}
            </div>
        </div>
    </div>
</div>
