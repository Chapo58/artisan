<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <body class="no-skin">
        <style>
            hr {
                margin-top: 0.5em;
                margin-bottom: 0.1em;
                border-width: 1px;
                border-top: 1px solid #000;
            }
        </style>
        @include('includes.partials.logged-in-as')

        <div class="main-container ace-save-state" id="main-container">
            <div class="container">
                <table style="width:100%">
                    <tr>
                        <td style="width:50%">
                            <img class="img-responsive" style="max-width: 200px; max-height: 80px;" src="{{$empresa->logo(true)}}"/>
                        </td>
                        <td>
                            <h1>R</h1>
                        </td>
                        <td>
                            <p>RECIBO Nº {{ $recibo->numero }}</p>
                        </td>
                    </tr>
                </table>
                <table style="width:100%;font-size: 80%;">
                    <tr>
                        <td style="width:70%">{{ $empresa->razon_social }}</td>
                        <td>Emision: {{date('d/m/Y')}}</td>
                    </tr>
                    <tr>
                        <td style="width:70%">{{ (isset($empresa->domicilio->direccion)) ? $empresa->domicilio->direccion : '' }}</td>
                        <td>CUIT: {{ $empresa->cuit }}</td>
                    </tr>
                    <tr>
                        <td style="width:70%">
                            @if(isset($empresa->domicilio->codigo_postal))
                                {{$empresa->domicilio->codigo_postal}}
                            @endif
                            @if(isset($empresa->domicilio->localidad))
                                , {{$empresa->domicilio->localidad}}
                            @endif
                        </td>
                        <td>II.BB.: {{ $empresa->ingresos_brutos }}</td>
                    </tr>
                    <tr>
                        <td style="width:70%">
                            @if(isset($empresa->domicilio->provincia))
                                {{$empresa->domicilio->provincia}}
                            @endif
                            @if(isset($empresa->domicilio->pais))
                                , {{$empresa->domicilio->pais}}
                            @endif
                        </td>
                        <td>Inicio de Actividades: {{($empresa->inicio_actividad) ? $empresa->inicio_actividad->format('d/m/Y') : ''}}</td>
                    </tr>
                    <tr>
                        <td style="width:70%">{{ $empresa->telefono }}</td>
                        <td>I.V.A.: {{ $empresa->condicionIva }}</td>
                    </tr>
                </table>
                <hr>
                <table style="width:100%;font-size: 80%;">
                @if($recibo->cliente->condicionIva == "Consumidor Final")
                        <tr>
                            <td style="width:70%">{{ $recibo->cliente->razon_social }}</td>
                        </tr>
                @else
                        <tr>
                            <td style="width:70%">{{ $recibo->cliente->razon_social }}</td>
                            <td>Cuenta: {{ $recibo->cliente_id }}</td>
                        </tr>
                        <tr>
                            <td style="width:70%">{{ (isset($recibo->cliente->domicilio->direccion)) ? $recibo->cliente->domicilio->direccion : '' }}</td>
                            <td>CUIT: {{ $recibo->cliente->dni }}</td>
                        </tr>
                        <tr>
                            <td style="width:70%">{{ (isset($recibo->cliente->domicilio->localidad)) ? $recibo->cliente->domicilio->localidad : '' }}</td>
                            <td>II.BB.: {{ (isset($recibo->cliente->ingresos_brutos)) ? $recibo->cliente->ingresos_brutos : '' }}</td>
                        </tr>
                        <tr>
                            <td style="width:70%">{{ (isset($recibo->cliente->domicilio->provincia)) ? $recibo->cliente->domicilio->provincia : '' }}</td>
                            <td>I.V.A.: </td>
                        </tr>
                        <tr>
                            <td style="width:70%">{{ (isset($recibo->cliente->telefono)) ? $recibo->cliente->telefono : '' }}</td>
                            <td></td>
                        </tr>
                @endif
                </table>
                <br>
                <table class="table table-condensed table-striped table-bordered" style="width:100%;font-size: 80%;">
                    <thead>
                        <tr class="info">
                            <th>Descripcion</th>
                            <th>Importe</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(!$recibo->detalles->isEmpty())
                            @foreach($recibo->detalles as $detalle)
                                <tr>
                                    <td>
                                        {{ $detalle->descripcion }}
                                    </td>
                                    <td>
                                        <b class="green">$ {{ number_format($detalle->monto, 2) }}</b>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
                <div style="position: absolute;bottom: 80px;">
                    <hr />
                    <table style="font-size: 80%;width:50%;float:left;">
                        <tr>
                            <td style="width: 50%;">
                                1. Forma de Pago:
                                @foreach($recibo->detallesCobros as $detalleCobro)
                                    {{ $detalleCobro->descripcion }}
                                @endforeach
                            </td>
                        </tr>
                        @if($recibo->descripcion)
                        <tr>
                            <td style="width: 50%;">2. Comentario: {{ $recibo->descripcion }}</td>
                        </tr>
                        @endif
                    </table>
                    <table style="font-size: 80%;width:50%;float:right;">
                        <tr>
                            <td style="width: 50%;font-size: 130%;font-weight: bold;">Total</td>
                            <td style="width: 50%;font-size: 130%;font-weight: bold;" align="right">$ {{ number_format($recibo->importe, 2) }}</td>
                        </tr>
                    </table>
                </div>
            </div><!-- container -->
        </div>
    </body>
</html>
