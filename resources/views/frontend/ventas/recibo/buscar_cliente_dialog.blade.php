@section('after-styles')
    {{ Html::style("css/frontend/plugins/jquery/jquery-ui.min.css") }}
@endsection

<div class="modal fade" id="modalBuscarCliente" tabindex="-1" role="dialog" aria-labelledby="buscarClienteModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="buscarClienteModalLabel">Buscar Cliente</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <input id="buscar_cliente" name="buscar_cliente" class="form-control" placeholder="Buscar..." type="text" value="">
                    <input id="cliente_id" name="cliente_id" type="hidden" value="">
                </div>
            </div>
            <div class="modal-footer">
                <a id="crear-recibo" href="" title="Crear Recibo" disabled="disabled">
                    <button class="btn btn-themecolor"><i class="glyphicon glyphicon-ok" ></i> Crear Recibo</button>
                </a>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>

@section('scripts-buscar-cliente-dialog')
    {{ Html::script("js/frontend/plugins/jquery/jquery-ui.min.js") }}

    <script>
        $( document ).ready(function() {
            $('#crear-recibo').click(function () {return false;});
            $(document).on("keydown.autocomplete", "#buscar_cliente", function (e) {
                $(this).autocomplete({
                    source: '{{ url('/personas/cliente/obtenerClienteParaVenta') }}',
                    minlength: 1,
                    autoFocus: true,
                    appendTo: '#modalBuscarCliente',
                    select: function (e, ui) {
                        $('input#buscar_cliente').val(ui.item.value);
                        $('input#cliente_id').val(ui.item.id);

                        $link = "{{ url('/ventas/recibo/crearReciboCliente/') }}";
                        $('a#crear-recibo').attr('href', $link + "/" + ui.item.id);
                        $('#crear-recibo').unbind('click');
                    },
                    response: function(e, ui) {
                        if (ui.content.length == 1){ // Si solo me devuelve un cliente, lo autocargo
                            ui.item = ui.content[0];

                            $('input#buscar_cliente').val(ui.item.value);
                            $('input#cliente_id').val(ui.item.id);

                            $link = "{{ url('/ventas/recibo/crearReciboCliente/') }}";
                            $('a#crear-recibo').attr('href', $link + "/" + ui.item.id);
                            $('#crear-recibo').unbind('click');
                        }
                    }
                });
            });
            $(document).on('hide.bs.modal','#modalBuscarCliente', function () {
                $('#crear-recibo').click(function () {return false;});
                $('input#buscar_cliente').val('');
            });
        });
    </script>
@endsection