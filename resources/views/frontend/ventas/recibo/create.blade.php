@extends('frontend.layouts.app')

@section ('title', trans('labels.frontend.recibo_titulo')." - ".trans('buttons.general.crud.create'))

@section('after-styles')
    {{ Html::style("css/frontend/plugins/jquery/jquery-ui.min.css") }}
@endsection

@section('content')
    <div class="row page-titles">
        <div class="col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('labels.frontend.recibo_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item"><a href="{{url('/ventas/recibo')}}">{{ trans('labels.frontend.recibo_titulo') }}</a></li>
                <li class="breadcrumb-item active">{{ trans('buttons.general.crud.create') }} {{ trans('labels.frontend.recibo') }}</li>
            </ol>
        </div>
        <div class="col-4">
            <div class="pull-right">
                <a href="{{ url('/ventas/cuenta-corriente/show-cliente/'.$cliente->id) }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans('buttons.general.crud.back') }}</span>
                </a>
            </div>
        </div>
    </div>

    @tips($_SERVER['REQUEST_URI'])

    @include('includes.partials.messages')

    {!! Form::open(['url' => '/ventas/recibo', 'class' => 'form-horizontal', 'files' => true]) !!}
        @include ('frontend.ventas.recibo.form')
    {!! Form::close() !!}

@endsection

@section('after-scripts')
    {{ Html::script("js/frontend/plugins/jquery/jquery-ui.min.js") }}
    {{ Html::script("js/frontend/plugins/jquery/datepicker-es.js") }}
    {{ Html::script("js/frontend/plugins/maskedinput/jquery.maskedinput.min.js") }}

    <script>
        $( document ).ready(function() {
            $('input.fecha').datepicker({
                firstDay: 0,
                dateFormat: 'dd/mm/yy'
            }).mask("99/99/9999", {placeholder: "dd/mm/aaaa"});
            $('input#numero').mask("99999999", {placeholder: "########"});

            calcularTotalRecibo();
        });

        $(document).on('click', '.borrar-detalle', function () {
            inputNombre = this.attributes['name'].value;

            $('.borrar-detalle[name="' + inputNombre + '"]').parent().parent().hide("slow");
            $('.borrar-detalle[name="' + inputNombre + '"]').parent().find('input.estado').val(0);
            $('.borrar-detalle[name="' + inputNombre + '"]').parent().parent().find('td input.detalle-recibo-monto').val(0);

            refrescarTotales();
        });

        $("#total").on('change', function (){
            refrescarTotales();
        });

        function calcularTotalRecibo(){
            totalRecibo = 0;
            detallesRecibos = $('input.detalle-recibo-monto');
            for (var i = 0; i < detallesRecibos.length; i++) {
                totalRecibo += parseFloat(detallesRecibos[i].value);
            }
            $('input#total').val(totalRecibo.toFixed(2));
            refrescarTotales();
        }

        function refrescarTotales() {
            actualizarMontoTotalCobro();
            $('#diferencia').html('$ ' + calcularMontoCobro() * -1);
        }

        function actualizarMontoTotalCobro() {
            totalPagos = 0;
            totalInteres = 0;
            montosCobrosAgregados = $('input[name^="detalles_cobro"].monto');
            for (var i = 0; i < montosCobrosAgregados.length; i++) {
                totalPagos += parseFloat(montosCobrosAgregados[i].value);
            }
            interesesCobrosAgregados = $('input[name^="detalles_cobro"].interes');
            for (var i = 0; i < interesesCobrosAgregados.length; i++) {
                totalInteres += parseFloat(interesesCobrosAgregados[i].value);
            }
            $('input#total-cobro').val((totalPagos + totalInteres).toFixed(2));
            $('#total-final').html('$ ' + (totalPagos + totalInteres).toFixed(2));
        }

    </script>

    @yield('scripts-cobros')
@endsection
