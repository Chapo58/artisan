@extends('frontend.layouts.app')

@section ('title', trans('labels.frontend.recibo_titulo')." - ".trans('buttons.general.crud.show'))

@section('content')
    <div class="row page-titles">
        <div class="col-6 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('labels.frontend.recibo_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item"><a href="{{url('/ventas/recibo')}}">{{ trans('labels.frontend.recibo_titulo') }}</a></li>
                <li class="breadcrumb-item">{{ trans('buttons.general.crud.show') }} {{ trans('labels.frontend.recibo') }}</li>
                <li class="breadcrumb-item active">{{ $recibo }}</li>
            </ol>
        </div>
        <div class="col-6">
            <div class="pull-right">
                <a href="{{ url('/ventas/recibo') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans('buttons.general.crud.back') }}</span>
                </a>
                <a id="print" href="javascript:void(0)" target="_blank" title="Imprimir" class="btn btn-primary">
                    <i class="fa fa-print" aria-hidden="true"></i>
                    <span class="hidden-xs-down">Imprimir</span>
                </a>
                <a href="javascript:void(0)" title="{{trans("buttons.general.crud.delete")." ".trans("labels.frontend.recibo")}}" class="btn btn-danger delete">
                    <i class="fa fa-trash" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans("buttons.general.crud.delete") }}</span>
                </a>
            </div>
        </div>
    </div>

    <div class="card card-body printableArea">
        <h3>
            <b>Recibo</b>
            <span class="pull-right">N° {{ $recibo->numero }}</span>
        </h3>
        <hr>
        <div class="row">
            <div class="col-md-12">
                <div class="pull-left">
                    <address>
                        <h3> &nbsp;<b class="text-danger">{{$empresa->razon_social}}</b></h3>
                        <p class="text-muted m-l-5">{{ (isset($empresa->domicilio->direccion)) ? $empresa->domicilio->direccion : '' }}
                            <br/> I.V.A.: {{ $empresa->condicionIva }}
                            <br/> CUIT: {{ $empresa->cuit }}
                            <br/> II.BB.: {{ $empresa->ingresos_brutos }}
                            <br/> Inicio de Actividades: {{($empresa->inicio_actividad) ? $empresa->inicio_actividad->format('d/m/Y') : ''}}</p>
                    </address>
                </div>
                <div class="pull-right text-right">
                    <address>
                        <h3 class="font-bold">
                            @if($recibo->cliente)
                                <a href="{{url('/personas/cliente/'.$recibo->cliente->id)}}">{{ $recibo->cliente }}</a>
                        </h3>
                        <p class="text-muted m-l-30">{{ (isset($recibo->cliente->domicilio->direccion)) ? $recibo->cliente->domicilio->direccion : '' }}
                            <br/> I.V.A.: {{ $recibo->cliente->condicionIva }}
                            <br/> CUIT: {{ $recibo->cliente->dni }}
                            <br/> Cuenta: {{ $recibo->cliente_id }}</p>
                        @else
                            Cliente Eliminado
                            </h3>
                        @endif
                        <p class="m-t-30 text-muted"><b>Moneda/Cotización :</b> {{ $recibo->moneda }} - <b class="text-success">$ {{ $recibo->cotizacion }}</b></p>
                        <p class="text-muted"><b>Fecha :</b> <i class="fa fa-calendar"></i> {{ $recibo->fecha->format('d/m/Y H:i') }}</p>
                    </address>
                </div>
            </div>

            <div class="col-md-12">
                <div class="table-responsive m-t-40" style="clear: both;">
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th class="text-center">#</th>
                            <th>Descripción</th>
                            <th class="text-right">Monto</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(!$recibo->detalles->isEmpty())
                            @foreach($recibo->detalles as $detalle)
                                <tr>
                                    <td class="text-center">
                                        {{ $loop->index + 1 }}
                                    </td>
                                    <td>
                                        {{ $detalle->descripcion }} - Comprobante Nº {{ $detalle->cuentaCorriente->detalle }}
                                    </td>
                                    <td class="text-right">
                                        {{ $recibo->moneda->signo }} {{ number_format($detalle->monto, 2) }}
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
            <div style="width:55%;">
                <hr class="m-t-30">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th class="text-center">#</th>
                        <th>Forma de Pago</th>
                        <th class="text-right">Monto</th>
                        <th class="text-right">Interes</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!$recibo->detallesCobros->isEmpty())
                        @foreach($recibo->detallesCobros as $detalleCobro)
                            <tr>
                                <td class="text-center">
                                    {{ $loop->index + 1 }}
                                </td>
                                <td>
                                    {{ $detalleCobro->descripcion }} -
                                    @if(isset($detalleCobro->entidad))
                                        <a href="{{ url($detalleCobro->entidad->url) }}">
                                            {{ $detalleCobro->entidad }}
                                        </a>
                                    @endif
                                </td>
                                <td class="text-right">
                                    {{ $recibo->moneda->signo }} {{ number_format($detalleCobro->monto , 2) }}
                                </td>
                                <td class="text-right">
                                    {{ $recibo->moneda->signo }} {{ number_format($detalleCobro->interes, 2) }}
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                    <tfoot>
                    <tr>
                        <td></td>
                        <td></td>
                        <td class="text-right font-bold">
                            Total Cobro
                        </td>
                        <td class="text-right">
                            {{ $recibo->moneda->signo }} {{ number_format($recibo->total_cobro, 2) }}
                        </td>
                    </tr>
                    </tfoot>
                </table>
                @if($recibo->descripcion)
                    <p>Comentario: <i>{{ $recibo->descripcion }}</i></p>
                @endif
            </div>
            <div style="width:45%;">
                <div class="pull-right text-right">
                    <hr class="m-t-30">
                    <h3><b>Total :</b> {{ $recibo->moneda->signo }} {{ number_format($recibo->importe, 2) }}</h3>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('after-scripts')
    {{ Html::script("js/frontend/jquery.PrintArea.js") }}
<script>
    $(document).ready(function() {
        $("#print").click(function() {
            var mode = 'iframe'; //popup
            var close = mode == "popup";
            var options = {
                mode: mode,
                popClose: close
            };
            $("div.printableArea").printArea(options);
        });
    });

    $("body").on("click", ".delete", function () {
        swal({
            title: "{{trans('buttons.general.crud.delete').' '.trans('labels.frontend.recibo')}}",
            text: "¿Realmente desea eliminar este registro?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#ff8726',
            confirmButtonText: "Eliminar"
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '{{ url('/ventas/recibo/'.$recibo->id) }}',
                    data: {"_method" : 'DELETE'},
                    success: function (msg) {
                        swal("Eliminado!", "El registro ha sido correctamente eliminado.", "success").then((result) => { window.location.href = '{{url('/ventas/recibo')}}'; });
                    }
                });
            }
        });
    });
</script>
@endsection