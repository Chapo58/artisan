<div class="card">
    <div class="card-body">
        <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
        <br>
        <div class="row form-group">
            <div class="col-md-5">
                {!! Form::label('nombre', trans('validation.attributes.nombre')) !!}
                {!! Form::text('nombre', null, ['class' => 'form-control', 'required' => 'required']) !!}
                {!! $errors->first('nombre', '<p class="text-danger">:message</p>') !!}
            </div>
            <div class="col-md-4">
                {!! Form::label('porcentaje', 'Porcentaje Base (+/-)') !!}
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-percent"></i>
                    </div>
                    {!! Form::number('porcentaje', null, ['class' => 'form-control', 'step' => '0.01']) !!}
                </div>
                {!! $errors->first('porcentaje', '<p class="text-danger">:message</p>') !!}
            </div>
            <div class="form-group col-md-3" {{(!isset($listaprecio)) ? 'style=display:none;' : ''}}>
                {!! Form::label('actualizar', 'Recalcular y Pisar Precios') !!}
                <i class="fa fa-question-circle text-info mytooltip">
                    <span class="tooltip-content3">Actualiza los precios de todos los artículos de esta lista de precios al precio nuevo reemplazando sus porcentajes actuales por el nuevo "Porcentaje Base"</span>
                </i>
                <div class="switch">
                    <label>
                        {!! Form::checkbox('actualizar', true, true) !!}<span class="lever"></span>
                    </label>
                </div>
            </div>
            @if(isset($listaprecio))
                <div class="col-md-5"></div>
                <div class="col-md-4">
                    {!! Form::label('porcentaje_acumulativo', 'Porcentaje Acumulativo (+/-)') !!}
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-percent"></i>
                        </div>
                        {!! Form::number('porcentaje_acumulativo', null, ['class' => 'form-control', 'step' => '0.01']) !!}
                    </div>
                    {!! $errors->first('porcentaje_acumulativo', '<p class="text-danger">:message</p>') !!}
                </div>
                @endif
        </div>
        <div class="row form-group">
            <div class="col-md-12">
                {!! Form::label('descripcion', trans('validation.attributes.descripcion')) !!}
                {!! Form::textarea('descripcion', null, ['class' => 'form-control']) !!}
                {!! $errors->first('descripcion', '<p class="text-danger">:message</p>') !!}
            </div>
        </div>
        <hr>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-12">
                    {{ Form::submit(isset($submitButtonText) ? $submitButtonText : trans('buttons.general.save'), ['class' => 'btn btn-lg btn-themecolor pull-right']) }}
                </div>
            </div>
        </div>
    </div>
</div>
