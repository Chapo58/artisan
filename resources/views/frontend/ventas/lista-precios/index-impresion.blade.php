@extends('frontend.layouts.app')

@section ('title', "Imprimir Lista de Precios")
@section('content')
    <div class="row page-titles">
        <div class="col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('navs.frontend.user.administration') }} {{ trans('labels.frontend.listaprecios_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item"><a href="{{url('/ventas/lista-precios')}}">{{ trans('labels.frontend.listaprecios_titulo') }}</a></li>
                <li class="breadcrumb-item active">Imprimir {{ trans('labels.frontend.listaprecios') }}</li>
            </ol>
        </div>
        <div class="col-4">
            <div class="pull-right">
                <a href="{{ url('/ventas/lista-precios') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans('buttons.general.crud.back') }}</span>
                </a>
            </div>
        </div>
    </div>

    {!! Form::open([
        'method'=>'POST',
        'class' => 'form-horizontal',
        'target' => '_blank',
        'url' => 'ventas/lista-precios/generarPdf'
    ]) !!}
    <div class="card">
        <div class="card-body">
            <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
            <br>
            <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('lista_precios_id', 'Lista') !!}
                    {!! Form::select('lista_precios_id', $listas, '', ['class' => 'form-control', 'required' => 'required']) !!}
                </div>
                <div class="col-md-4">
                    {!! Form::label('rubro_id', 'Rubro') !!}
                    {!! Form::select('rubro_id', $rubros, '', ['class' => 'form-control']) !!}
                </div>
                <div class="col-md-4">
                    {!! Form::label('orden', 'Ordenar Por') !!}
                    <select name="ordenamiento" class="form-control">
                        <option value="articulos.nombre">Nombre Articulo</option>
                        <option value="rubros.nombre">Rubros</option>
                        <option value="marcas.nombre">Marcas</option>
                        <option value="articulos.codigo">Codigo Interno</option>
                    </select>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-6 demo-checkbox">
                    <input type="checkbox" id="con_stock" name="con_stock" class="filled-in chk-col-cyan" checked />
                    <label for="con_stock"><strong>Imprimir solo articulos en stock</strong></label>
                </div>
                <div class="col-md-6 demo-checkbox">
                    <input type="checkbox" id="precio_final" name="precio_final" class="filled-in chk-col-cyan" checked />
                    <label for="precio_final"><strong>Precio Final</strong></label>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-2">
                    <input type="checkbox" id="codigos" name="codigos" class="filled-in chk-col-red" />
                    <label for="codigos"><strong>Codigo Interno</strong></label>
                </div>
                <div class="col-md-2">
                    <input type="checkbox" id="imagenes" name="imagenes" class="filled-in chk-col-red" />
                    <label for="imagenes"><strong>Imagen</strong></label>
                </div>
                <div class="col-md-2">
                    <input type="checkbox" id="marcas" name="marcas" class="filled-in chk-col-red" />
                    <label for="marcas"><strong>Marca</strong></label>
                </div>
                <div class="col-md-2">
                    <input type="checkbox" id="stock" name="stock" class="filled-in chk-col-red" />
                    <label for="stock"><strong>Stock</strong></label>
                </div>
                <div class="col-md-2">
                    <input type="checkbox" id="modelos" name="modelos" class="filled-in chk-col-red" />
                    <label for="modelos"><strong>Modelo</strong></label>
                </div>
                <div class="col-md-2">
                    <input type="checkbox" id="modelos_secundarios" name="modelos_secundarios" class="filled-in chk-col-red" />
                    <label for="modelos_secundarios"><strong>Modelo Secundario</strong></label>
                </div>
            </div>
            <hr>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-12">
                        {{ Form::submit('Imprimir', ['class' => 'btn btn-lg btn-primary pull-right']) }}
                    </div>
                </div>
            </div>
            <br>
        </div>
    </div>
    {!! Form::close() !!}

@endsection
