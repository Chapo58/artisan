<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <body class="no-skin">
    <style>
        .page-border { position: fixed; left: -15px; top: -15px; bottom: -15px; right: -15px; border: 2px solid orange; }

        .table-striped>tbody>tr:nth-child(odd)>td,
        .table-striped>tbody>tr:nth-child(odd)>th {
            background-color: #FFFAE6;
        }
    </style>
    <div class="page-border"></div>
        @include('includes.partials.logged-in-as')

        <div class="main-container ace-save-state" id="main-container">
            <div class="container">
                <table style="width:100%">
                    <tr>
                        <td style="width:30%">
                            <img class="img-responsive" style="max-width: 200px; max-height: 80px;" src="{{$user->empresa->logo(true)}}"/>
                        </td>
                        <td style="width:30%">
                            Tel.: {{$user->empresa->telefono}}<br>
                            Mail: {{$user->empresa->email}}<br>
                            Web: <a href="{{$user->empresa->web}}" target="_blank">{{$user->empresa->web}}</a><br>
                        </td>
                        <td><h1 style="color:orange;float:right">Lista de Precios</h1></td>
                    </tr>
                </table>
                <hr>
                <table class="table table-condensed table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr style="background-color: #FFE25E;">
                            @if($request->codigos)
                                <th>Codigo</th>
                            @endif
                            @if($request->imagenes)
                                <th>Imagen</th>
                            @endif
                            <th>Articulo</th>
                            @if($request->marcas)
                                <th>Marca</th>
                            @endif
                            @if($request->stock)
                                <th>Stock</th>
                            @endif
                            @if($request->modelos)
                                <th>Modelo</th>
                            @endif
                            @if($request->modelos_secundarios)
                                <th>Modelo</th>
                            @endif
                            <th>Precio</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($articulos as $articulo)
                            <tr>
                                @if($request->codigos)
                                    <td>{{ $articulo->codigo }}</td>
                                @endif
                                @if($request->imagenes)
                                    <td align="center">
                                        <img src="{{url($articulo->imagen_url)}}" alt='' width="60" style="margin-top:5px;">
                                    </td>
                                @endif
                                <td>{{ $articulo->nombre }}</td>
                                @if($request->marcas)
                                    <td>{{ $articulo->marca }}</td>
                                @endif
                                @if($request->stock)
                                    <td>{{ (int)$articulo->getStock($user->sucursal) }}</td>
                                @endif
                                @if($request->modelos)
                                    <td>{{ $articulo->modelo }}</td>
                                @endif
                                @if($request->modelos_secundarios)
                                    <td>{{ $articulo->modelo_secundario }}</td>
                                @endif
                                <td>$ {{ $articulo->getPrecio(null,$request->precio_final,$listaprecio) }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div><!-- container -->
        </div>
    </body>
</html>
