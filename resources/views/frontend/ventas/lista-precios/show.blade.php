@extends('frontend.layouts.app')

@section ('title', trans('labels.frontend.listaprecios_titulo')." - ".trans('buttons.general.crud.show'))

@section('content')
    <div class="row page-titles">
        <div class="col-6 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('navs.frontend.user.administration') }} {{ trans('labels.frontend.listaprecios_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item"><a href="{{url('/ventas/lista-precios')}}">{{ trans('labels.frontend.listaprecios_titulo') }}</a></li>
                <li class="breadcrumb-item">{{ trans('buttons.general.crud.show') }} {{ trans('labels.frontend.listaprecios') }}</li>
                <li class="breadcrumb-item active">{{ $listaprecio }}</li>
            </ol>
        </div>
        <div class="col-6">
            <div class="pull-right">
                <a href="{{ url('/ventas/lista-precios') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans('buttons.general.crud.back') }}</span>
                </a>
                <a href="{{ url('/ventas/lista-precios/' . $listaprecio->id . '/edit') }}" title="{{ trans('buttons.general.crud.edit') }}" class="btn btn-info">
                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans('buttons.general.crud.edit') }}</span>
                </a>
                <a href="javascript:void(0)" title="{{trans("buttons.general.crud.delete")." ".trans("labels.frontend.listaprecios")}}" class="btn btn-danger delete">
                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans("buttons.general.crud.delete") }}</span>
                </a>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            <div class="card-actions">
                <a class="mytooltip" data-action="collapse"><i class="ti-minus"></i><span class="tooltip-content3">Minimizar Ventana</span></a>
                <a class="btn-minimize mytooltip" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
            </div>
            <h4 class="card-title m-b-0">Información de Lista de Precios</h4>
        </div>
        <div class="card-body collapse show b-t">
            <div class="row">
                <div class="col-md-3 font-weight-bold">Nombre</div>
                <div class="col-md-9">{{ $listaprecio->nombre }}</div>
            </div><hr>
            <div class="row">
                <div class="col-md-3 font-weight-bold">Descripcion</div>
                <div class="col-md-9">{{ $listaprecio->descripcion }}</div>
            </div><hr>
            <div class="row">
                <div class="col-md-3 font-weight-bold">Porcentaje</div>
                <div class="col-md-9">
                    <b class="@if($listaprecio->porcentaje >= 0) green @else red @endif">
                        {{ $listaprecio->porcentaje }}%
                    </b>
                </div>
            </div><hr>
            <div class="row">
                <div class="col-md-3 font-weight-bold">Creado el</div>
                <div class="col-md-9">{{ $listaprecio->created_at->format('d/m/Y H:m') }}</div>
            </div><hr>
            <div class="row">
                <div class="col-md-3 font-weight-bold">Ultima Modificación</div>
                <div class="col-md-9">{{ $listaprecio->updated_at->format('d/m/Y H:m') }}</div>
            </div>
            <div class="form-group">
                {!! Form::open([
                    'url' => ['/ventas/precio/create', $listaprecio->id],
                    'style' => 'display:inline'
                ]) !!}
                    {!! Form::button('<i class="fa fa-plus" aria-hidden="true"></i> Nuevo Precio', array(
                        'type' => 'submit',
                        'class' => 'btn btn-success btn-sm pull-right',
                        'title' => 'Nuevo Precio'
                    )) !!}
                {!! Form::close() !!}
            </div>            
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            <div class="card-actions">
                <a class="mytooltip" data-action="collapse"><i class="ti-minus"></i><span class="tooltip-content3">Minimizar Ventana</span></a>
                <a class="btn-minimize mytooltip" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
            </div>
            <h4 class="card-title m-b-0">Articulos de la Lista</h4>
        </div>
        <div class="card-body collapse show b-t">
            <div class="table-responsive">
                <table id="articulos-table" class="table table-striped table-hover table-sm">
                    <thead class="thin-border-bottom">
                        <tr>
                            <th>Artículo</th>
                            <th>Precio</th>
                            <th>Fecha</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($listaprecio->precios as $precio)
                            @if($precio->articulo)
                                <tr>
                                    <td>
                                        <a href="{{url('/articulos/articulo/'.$precio->articulo->id)}}">({{ $precio->articulo->codigo }}) {{ $precio->articulo->nombre }}</a>
                                    </td>
                                    <td>
                                        <b class="green">$ {{ number_format($precio->precio, 2) }}</b>
                                    </td>
                                    <td>
                                        {{ date('d/m/Y H:i', strtotime($precio->updated_at)) }}
                                    </td>
                                    <td>
                                        <a href="{{ url('/ventas/precio/' . $precio->id . '/edit') }}" class="mytooltip">
                                            <i class="fa fa-pencil text-info"></i>
                                            <span class="tooltip-content3">{{ trans('buttons.general.crud.edit') }} {{ trans('labels.frontend.precio') }}</span>
                                        </a>
                                    </td>
                                </tr>
                            @endif
                        @endforeach
                    </tbody>
                </table>
            </div>         
        </div>
    </div>

@endsection

@section('after-scripts')
{{ Html::script("js/frontend/plugins/datatables/jquery.dataTables.min.js") }}

<script>
    $(document).ready(function() {
        $('#articulos-table').DataTable();
    } );

    $("body").on("click", ".delete", function () {
        swal({
            title: "Eliminar Lista de Precios",
            text: "¿Realmente desea eliminar este registro?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#ff8726',
            confirmButtonText: "Eliminar"
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '{{ url('/ventas/lista-precios/'.$listaprecio->id) }}',
                    data: {"_method" : 'DELETE'},
                    success: function (msg) {
                        swal("Eliminado!", "El registro ha sido correctamente eliminado.", "success").then((result) => { window.location.href = '{{url('/ventas/lista-precios')}}'; });
                    }
                });
            }
        });
    });
</script>

@endsection