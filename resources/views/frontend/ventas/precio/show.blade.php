@extends('frontend.layouts.app')

@section ('title', trans('labels.frontend.precio_titulo')." - ".trans('buttons.general.crud.show'))

@section('content')
    <div class="main-content">
        <div class="main-content-inner">
            @include ('frontend.includes.breadcrumbs')

            <div class="page-header">
                <h1>
                    {{ trans('labels.frontend.precio_titulo') }}
                    <small>
                        <i class="ace-icon fa fa-angle-double-right"></i>
                        <small>{{ trans('buttons.general.crud.show') }} Precio</small>
                        <small><i class="ace-icon fa fa-angle-double-right"></i></small>
                        <small>{{ $precio->id }}# {{ $precio }}</small>
                    </small>

                    <div class="box-tools pull-right">
                        <a href="{{ url('/ventas/precio') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning btn-sm">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> {{ trans('buttons.general.crud.back') }}
                        </a>
                        <a href="{{ url('/ventas/precio/' . $precio->id . '/edit') }}" title="{{ trans('buttons.general.crud.edit') }}" class="btn btn-primary btn-sm">
                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i> {{ trans('buttons.general.crud.edit') }}
                        </a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['ventas/precio', $precio->id],
                            'style' => 'display:inline',
                            'id' => 'form-delete'
                        ]) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> '.trans("buttons.general.crud.delete"), array(
                                    'type' => 'button',
                                    'class' => 'btn btn-danger btn-sm',
                                    'title' => trans("buttons.general.crud.delete")." ".trans("labels.frontend.precio"),
                                    'onclick'=> 'eliminar()'
                            ))!!}
                        {!! Form::close() !!}
                    </div>
                </h1>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-borderless">
                        <tbody>
                            <tr>
                                <th>ID</th><td>{{ $precio->id }}</td>
                            </tr>
                            <tr><th> Articulo Id </th><td> {{ $precio->articulo_id }} </td></tr><tr><th> Lista Precios Id </th><td> {{ $precio->lista_precios_id }} </td></tr><tr><th> Precio </th><td> {{ $precio->precio }} </td></tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('after-scripts')
    {{ Html::script("js/frontend/plugin/dialog/bootstrap-dialog.min.js") }}

    <script type="text/javascript">
        function eliminar(){
            BootstrapDialog.show({
                title: 'Eliminar {{ trans("labels.frontend.precio") }}',
                type: BootstrapDialog.TYPE_DANGER,
                message: '¿Esta seguro que desea eliminar este registro?',
                buttons: [{
                    icon: 'glyphicon glyphicon-trash',
                    label: ' Eliminar',
                    cssClass: 'btn-danger',
                    action: function(dialogItself){
                        $('form#form-delete').submit();
                    }
                },{
                    icon: 'glyphicon glyphicon-remove',
                    label: ' Cancelar',
                    cssClass: 'btn-default',
                    action: function(dialogItself){
                        dialogItself.close();
                    }
                }]
            });
        }
    </script>
@endsection