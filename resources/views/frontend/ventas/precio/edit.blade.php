@extends('frontend.layouts.app')

@section ('title', trans('labels.frontend.listaprecios_titulo')." - ".trans('buttons.general.crud.edit'))

@section('content')
    <div class="row page-titles">
        <div class="col-md-8 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('navs.frontend.user.administration') }} {{ trans('labels.frontend.listaprecios_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item"><a href="{{url('/ventas/lista-precios')}}">{{ trans('labels.frontend.listaprecios_titulo') }}</a></li>
                <li class="breadcrumb-item"><a href="{{url('/ventas/lista-precios/' . $precio->lista_precio_id)}}">{{ trans('buttons.general.crud.show') }} {{ trans('labels.frontend.listaprecios') }}</a></li>
                <li class="breadcrumb-item"><a href="{{url('/ventas/lista-precios/' . $precio->lista_precio_id)}}">{{ $precio->listaPrecio }}</a></li>
                <li class="breadcrumb-item">{{ trans('buttons.general.crud.edit') }} Precio</li>
                <li class="breadcrumb-item active">{{ $precio->articulo }}</li>
            </ol>
        </div>
        <div class="col-md-4">
            <div class="pull-right">
                <a href="{{url('/ventas/lista-precios/' . $precio->lista_precio_id)}}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i> {{ trans('buttons.general.crud.back') }}
                </a>
            </div>
        </div>
    </div>

    @include('includes.partials.messages')

    {!! Form::model($precio, [
        'method' => 'PATCH',
        'url' => ['/ventas/precio', $precio->id],
        'class' => 'form-horizontal',
        'files' => true
    ]) !!}

        @include ('frontend.ventas.precio.form', ['submitButtonText' => trans("buttons.general.crud.update")])

    {!! Form::close() !!}

@endsection
