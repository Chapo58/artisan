@extends ('frontend.layouts.app')

@section ('title', trans('labels.frontend.precio_titulo')." - ".trans('navs.frontend.user.administration'))

@section('after-styles')
    {{ Html::style("css/frontend/plugins/datatables/responsive.dataTables.min.css") }}
@endsection

@section('content')
    <div class="main-content">
        <div class="main-content-inner">
            @include ('frontend.includes.breadcrumbs')
            <div class="page-header">
                <h1>
                    {{ trans('labels.frontend.precio_titulo') }}
                    <small>
                        <i class="ace-icon fa fa-angle-double-right"></i>
                        <small>{{ trans('navs.frontend.user.administration') }}</small>
                    </small>

                    <div class="box-tools pull-right">
                        <a href="{{ url('/ventas/precio/create') }}" class="btn btn-success btn-sm" title="{{ trans('buttons.general.crud.create') }} Precio">
                            <i class="fa fa-plus" aria-hidden="true"></i> {{ trans('buttons.general.crud.create') }} {{ trans('labels.frontend.precio') }}
                        </a>
                    </div><!--box-tools pull-right-->
                </h1>
            </div>

            @tips($_SERVER['REQUEST_URI'])

            @include('includes.partials.messages')

            {!! Form::open(['method' => 'GET', 'url' => '/ventas/precio', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
            <div class="input-group">
                <input type="text" class="form-control" name="search" placeholder="{{ trans('strings.backend.general.search_placeholder') }}">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="submit">
                        <i class="fa fa-search"></i>
                    </button>
                </span>
            </div>
            {!! Form::close() !!}

            <div class="row">
                <div class="col-xs-12">
                    <div class="table-responsive">
                        <table id="precio-table" class="table table-condensed table-hover">
                            <thead>
                                <tr>
                                    <th>ID</th><th>Articulo Id</th><th>Lista Precios Id</th><th>Precio</th><th>{{ trans('labels.general.actions') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($precio as $item)
                                <tr>
                                    <td>{{ $item->id }}</td>
                                    <td>{{ $item->articulo_id }}</td><td>{{ $item->lista_precios_id }}</td><td>{{ $item->precio }}</td>
                                    <td>
                                        <a href="{{ url('/ventas/precio/' . $item->id) }}" title="{{ trans('buttons.general.crud.show') }} {{ trans('labels.frontend.precio') }}">
                                            <button class="btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i> {{ trans('buttons.general.crud.show') }}</button>
                                        </a>
                                        <a href="{{ url('/ventas/precio/' . $item->id . '/edit') }}" title="{{ trans('buttons.general.crud.edit') }} {{ trans('labels.frontend.precio') }}">
                                            <button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> {{ trans('buttons.general.crud.edit') }}</button>
                                        </a>
                                        {!! Form::open([
                                            'method'=>'DELETE',
                                            'url' => ['/ventas/precio', $item->id],
                                            'style' => 'display:inline',
                                            'id' => 'form-delete-'.$item->id
                                        ]) !!}
                                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> '.trans("buttons.general.crud.delete"), array(
                                                    'type' => 'button',
                                                    'class' => 'btn btn-danger btn-xs',
                                                    'title' => trans("buttons.general.crud.delete")." ".trans("labels.frontend.precio"),
                                                    'onclick'=> 'eliminar("form-delete-'.$item->id.'")'
                                            )) !!}
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="pagination-wrapper"> {!! $precio->appends(['search' => Request::get('search')])->render() !!} </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('after-scripts')
    {{ Html::script("js/frontend/plugins/datatables/jquery.dataTables.min.js") }}
    {{ Html::script("js/frontend/plugins/datatables/dataTables.responsive.min.js") }}
    {{ Html::script("js/frontend/plugins/datatables/dataTables.select.min.js") }}

<script>
    $(document).ready(function() {
        $('#precio-table').DataTable( {
            "paging":   false,
            "info":     false,
            "searching":   false
        });
    } );

    function eliminar(idFormDelete){
        BootstrapDialog.show({
            title: 'Eliminar {{ trans("labels.frontend.precio") }}',
            type: BootstrapDialog.TYPE_DANGER,
            message: '¿Esta seguro que desea eliminar este registro?',
            buttons: [{
                icon: 'glyphicon glyphicon-trash',
                label: ' Eliminar',
                cssClass: 'btn-danger',
                action: function(dialogItself){
                    $('form#'+idFormDelete).submit();
                }
            },{
                icon: 'glyphicon glyphicon-remove',
                label: ' Cancelar',
                cssClass: 'btn-default',
                action: function(dialogItself){
                    dialogItself.close();
                }
            }]
        });
    }
</script>

@endsection