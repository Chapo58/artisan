@extends('frontend.layouts.app')

@section ('title', trans('labels.frontend.listaprecios_titulo')." - ".trans('buttons.general.crud.create'))

@section('after-styles')
    {{ Html::style("css/frontend/plugins/jquery/jquery-ui.min.css") }}
@endsection

@section('content')
    <div class="row page-titles">
        <div class="col-md-8 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('navs.frontend.user.administration') }} {{ trans('labels.frontend.listaprecios_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item"><a href="{{url('/ventas/lista-precios')}}">{{ trans('labels.frontend.listaprecios_titulo') }}</a></li>
                <li class="breadcrumb-item"><a href="{{url('/ventas/lista-precios/' . $lista_precios->id)}}">{{ trans('buttons.general.crud.show') }} {{ trans('labels.frontend.listaprecios') }}</a></li>
                <li class="breadcrumb-item"><a href="{{url('/ventas/lista-precios/' . $lista_precios->id)}}">{{ $lista_precios }}</a></li>
                <li class="breadcrumb-item active">{{ trans('buttons.general.crud.create') }} Precio</li>
            </ol>
        </div>
        <div class="col-md-4">
            <div class="pull-right">
                <a href="{{url('/ventas/lista-precios/' . $lista_precios->id)}}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i> {{ trans('buttons.general.crud.back') }}
                </a>
            </div>
        </div>
    </div>

    @tips($_SERVER['REQUEST_URI'])

    @include('includes.partials.messages')

    {!! Form::open(['url' => '/ventas/precio', 'class' => 'form-horizontal', 'files' => true]) !!}
        @include ('frontend.ventas.precio.form')
    {!! Form::close() !!}

@endsection

@section('after-scripts')
    {{ Html::script("js/frontend/plugins/jquery/jquery-ui.min.js") }}
    <script>
        $( document ).ready(function() {
            $(document).on("keydown.autocomplete",".buscar-articulo",function(e){
                $(this).autocomplete({
                    source:'{{ url('/articulos/articulo/autocomplete') }}',
                    minlength:1,
                    autoFocus:true,
                    select:function(e,ui) {
                        inputNombre = this.attributes['name'].value;
                        $('.buscar-articulo[name="' + inputNombre + '"]').val(ui.item.value);
                        $('.buscar-articulo[name="' + inputNombre + '"]').parent().find('input.articulo-id').val(ui.item.id);
                        $('.buscar-articulo[name="' + inputNombre + '"]').parent().parent().find('td select.iva').val(ui.item.porcentaje_iva).change();
                    }
                });
            });
        });
    </script>
@endsection