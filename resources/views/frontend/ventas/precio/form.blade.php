<div class="card">
    <div class="card-body">
        <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
        <br>
        <div class="row form-group">
            <div class="col-md-4">
                {!! Form::label('lista_precio_id', 'Lista de Precio') !!}
                {!! Form::text('lista_precio', isset($precio) ? $precio->listaPrecio : $lista_precios, ['class' => 'form-control', 'readonly']) !!}
                <input type="hidden" name="lista_precio_id" value="{{ isset($precio) ? $precio->lista_precio_id : $lista_precios->id }}">
                {!! $errors->first('lista_precio_id', '<p class="text-danger">:message</p>') !!}
            </div>
            <div class="col-md-5">
                {!! Form::label('articulo_id', 'Artículo') !!}
                <input type="text" class="form-control buscar-articulo" name="articulo" placeholder="{{ trans('strings.backend.general.search_placeholder') }}" value="{{ isset($precio) ? '('.$precio->articulo->codigo.') '.$precio->articulo->nombre : null }}" @if(isset($precio)) readonly='readonly' @endif>
                <input type="hidden" class="articulo-id" name="articulo_id" value="{{ isset($precio) ? $precio->articulo_id : null }}">
                {!! $errors->first('articulo_id', '<p class="text-danger">:message</p>') !!}
            </div>
            <div class="col-md-3">
                {!! Form::label('numero', 'Numero') !!}
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-hashtag"></i>
                    </div>
                    {!! Form::number('numero', null, ['class' => 'form-control']) !!}
                </div>
                {!! $errors->first('numero', '<p class="text-danger">:message</p>') !!}
            </div>
        </div>
        <div class="row form-group">
            <div class="col-md-6">
                {!! Form::label('precio', 'Precio') !!}
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-dollar"></i>
                    </div>
                    {!! Form::number('precio', null, ['class' => 'form-control', 'step' => '0.01', 'placeholder' => 'Precio']) !!}
                </div>
                {!! $errors->first('precio', '<p class="text-danger">:message</p>') !!}
            </div>
        </div>
        <hr>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-12">
                    {{ Form::submit(isset($submitButtonText) ? $submitButtonText : trans('buttons.general.save'), ['class' => 'btn btn-lg btn-themecolor pull-right']) }}
                </div>
            </div>
        </div>
        <br>
    </div>
</div>
