@extends('frontend.layouts.app')

@section ('title', trans('labels.frontend.presupuesto_titulo')." - ".trans('buttons.general.crud.edit'))

@section('content')
    <div class="main-content">
        <div class="main-content-inner">
            @include ('frontend.includes.breadcrumbs')
            <div class="page-header">
                <h1>
                    {{ trans('labels.frontend.presupuesto_titulo') }}
                    <small>
                        <i class="ace-icon fa fa-angle-double-right"></i>
                        <small>{{ trans('buttons.general.crud.edit') }} Presupuesto </small>
                        <small><i class="ace-icon fa fa-angle-double-right"></i></small>
                        <small>{{ $presupuesto }}</small>
                    </small>

                    <div class="box-tools pull-right">
                        <a href="{{ url('/ventas/presupuesto') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> {{ trans('buttons.general.crud.back') }}
                        </a>
                    </div>
                </h1>
            </div>

            <div class="row">
                <div class="col-xs-12">

                    @include('includes.partials.messages')

                    {!! Form::model($presupuesto, [
                        'method' => 'PATCH',
                        'url' => ['/ventas/presupuesto', $presupuesto->id],
                        'class' => 'form-horizontal',
                        'files' => true
                    ]) !!}

                        @include ('frontend.ventas.presupuesto.form', ['submitButtonText' => trans("buttons.general.crud.update")])

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('after-scripts')
{{ Html::script("js/frontend/plugin/jquery/jquery-ui.min.js") }}
{{ Html::script("js/frontend/plugin/jquery/datepicker-es.js") }}
{{ Html::script("js/frontend/plugin/maskedinput/jquery.maskedinput.min.js") }}

<script>
    $( document ).ready(function() {
        $('input.fecha').datepicker({
            firstDay: 0,
            dateFormat: 'dd/mm/yy'
        }).mask("99/99/9999", {placeholder: "dd/mm/aaaa"});
        $('input#numero').mask("99999999",{placeholder:"########"});
        $(function() {
            $( document ).tooltip({
                position: {
                    my: "right top",
                    at: "left-5 top-5",
                    collision: "none"
                }
            });
        });
        $('input.buscar-articulo').keypress(function(event){
            if (event.keyCode === 10 || event.keyCode === 13){
                event.preventDefault();
            }
        });

        $(document).on("keydown.autocomplete", ".buscar-articulo", function (e) {
            if (e.keyCode == 13) {
                e.preventDefault();
            }else{
                $(this).autocomplete({
                    source: '{{ url('/articulos/articulo/obtenerPrecioArticulo') }}/'+$('select#lista_precios_id option:selected').val(),
                    minlength: 1,
                    autoFocus: true,
                    select: function (e, ui) {
                        inputNombre = this.attributes['name'].value;
                        $('.buscar-articulo[name="' + inputNombre + '"]').val(ui.item.value);
                        $('.buscar-articulo[name="' + inputNombre + '"]').parent().find('input.articulo-id').val(ui.item.id);
                        if($('#moneda_id option:selected').val() != ui.item.moneda_id){
                            if(ui.item.cotizacion == 1){
                                ui.item.precio = (ui.item.precio / parseFloat($("#cotizacion").val())).toFixed(2);
                            }else{
                                ui.item.precio = (ui.item.precio * ui.item.cotizacion).toFixed(2);
                            }
                        }
                        if($('#tipo_comprobante_id').val() != 0){
                            ui.item.precio = (ui.item.precio * (ui.item.iva/100 + 1)).toFixed(2);
                            ui.item.iva = 0;
                            ui.item.porcentaje_iva = 0;
                        }
                        $('.buscar-articulo[name="' + inputNombre + '"]').parent().parent().find('td input.precio').val(ui.item.precio);
                        $('.buscar-articulo[name="' + inputNombre + '"]').parent().parent().find('td input.iva').val(ui.item.iva);
                        $('.buscar-articulo[name="' + inputNombre + '"]').parent().parent().find('td input.porcentaje-iva').val(ui.item.porcentaje_iva);
                        if(ui.item.existencia != null){
                            ui.item.existencia = (ui.item.existencia).toFixed(0);
                            var cantidad = parseInt($('.buscar-articulo[name="' + inputNombre + '"]').parent().parent().find('td input.cantidad').val());
                            if(ui.item.existencia < cantidad && $.isNumeric(cantidad)) {
                                $('.buscar-articulo[name="' + inputNombre + '"]').parent().parent().find('td input.cantidad').val(ui.item.existencia);
                            }
                            $('.buscar-articulo[name="' + inputNombre + '"]').parent().parent().find('td input.cantidad').attr('max', ui.item.existencia);
                            $('.buscar-articulo[name="' + inputNombre + '"]').parent().parent().find('td input.cantidad').attr('title', 'En stock: '+ui.item.existencia);
                        }else{
                            $('.buscar-articulo[name="' + inputNombre + '"]').parent().parent().find('td input.cantidad').attr('max', '');
                            $('.buscar-articulo[name="' + inputNombre + '"]').parent().parent().find('td input.cantidad').attr('title', '');
                        }
                    }
                });
            }
        });

        $(document).on('click', '.borrar-detalle', function () {
            inputNombre = this.attributes['name'].value;
            idDetalle = parseInt($('.borrar-detalle[name="' + inputNombre + '"]').parent().parent().find('td input.id').val());

            $('.borrar-detalle[name="' + inputNombre + '"]').parent().parent().hide("slow");
            $('.borrar-detalle[name="' + inputNombre + '"]').parent().find('input.estado').val(0);
            $('.borrar-detalle[name="' + inputNombre + '"]').parent().parent().find('td input.subtotal-neto').val(0);
            $('.borrar-detalle[name="' + inputNombre + '"]').parent().parent().find('td input.subtotal-bruto').val(0);

            refrescarTotales();
        });

        calcularSubtotales();
        calcularImporteBruto();
        calcularPorcentajeImpuesto();
    });

    $("#agregar-detalle").click(function(){
        agregarDetalle();
    });

    $("#agregar-detalle-de-cantidad").on('focusin', function(){
        agregarDetalle('cantidad');
    });

    $("#agregar-detalle-de-articulo").on('focusin', function(){
        agregarDetalle('descripcion');
    });

    function agregarDetalle(inputName) {
        var numero_orden = $('table#tabla-detalles-presupuesto tbody tr').length+ 1;
        $("#tabla-detalles-presupuesto").append('<tr>' +
            '<td>' +
            numero_orden +
            '<input type="hidden" class="id" name="detalles['+(numero_orden*-1)+'][id]" value="'+(numero_orden*-1)+'">' +
            '</td>' +
            '<td>' +
            '<input class="form-control cantidad" name="detalles['+(numero_orden*-1)+'][cantidad]" type="number" value="1" max="" min="0" title="">' +
            '</td>' +
            '<td>' +
            '<input class="form-control buscar-articulo" name="detalles['+(numero_orden*-1)+'][descripcion]"  placeholder="Buscar..." type="text">' +
            '<input class="articulo-id" name="detalles['+(numero_orden*-1)+'][articulo_id]" type="hidden" value="">' +
            '</td>' +
            '<td>' +
            '<div class="input-group">' +
            '<div class="input-group-addon"><i class="fa fa-usd"></i></div>' +
            '<input class="form-control precio" name="detalles['+(numero_orden*-1)+'][precio]" type="number" step="0.01" readonly tabindex="99999">' +
            '</div>' +
            '</td>' +
            '<td>' +
            '<div class="input-group">' +
            '<div class="input-group-addon"><i class="fa fa-usd"></i></div>' +
            '<input class="form-control subtotal-neto" name="detalles['+(numero_orden*-1)+'][total_neto]" type="number" step="0.01" tabindex="99999">' +
            '</div>' +
            '</td>' +
            '<td>' +
            '<div class="input-group">' +
            '<div class="input-group-addon"><i class="fa fa-percent"></i></div>' +
            '<input class="form-control iva" name="detalles['+(numero_orden*-1)+'][iva]" type="number" step="0.01" readonly tabindex="99999">' +
            '<input type="hidden" class="porcentaje-iva" name="detalles['+(numero_orden*-1)+'][porcentaje_iva]" value="">' +
            '</div>' +
            '</td>' +
            '<td>' +
            '<div class="input-group">' +
            '<div class="input-group-addon"><i class="fa fa-usd"></i></div>' +
            '<input class="form-control subtotal-bruto" name="detalles['+(numero_orden*-1)+'][total_bruto]" type="number" step="0.01" readonly tabindex="99999">' +
            '</div>' +
            '</td>' +
            '<td>' +
            '<a class="btn btn-danger btn-sm borrar-detalle" name="detalles['+(numero_orden*-1)+'][eliminar]" tabindex="99999">' +
            '<i class="fa fa-trash-o" aria-hidden="true"></i>' +
            '</a>' +
            '<input type="hidden" class="estado" name="detalles['+(numero_orden*-1)+'][estado]" value="1">' +
            '</td>' +
            '</tr>');
        if(inputName != null){
            $("input[name='detalles["+(numero_orden*-1)+"]["+inputName+"]']").focus();
        }else{
            $("input[name='detalles["+(numero_orden*-1)+"][descripcion]']").focus();
        }
        calcularSubtotales();
    }

    $("#porcentaje-impuesto").on('change', function (){
        calcularImporteImpuesto();
        calcularTotal();
    });

    $("#importe-impuesto").on('change', function (){
        calcularPorcentajeImpuesto();
        calcularTotal();
    });

    function refrescarTotales() {
        calcularImporteNeto();
        calcularImporteBruto();
        calcularImporteIva();
        calcularImporteImpuesto();
        calcularTotal();
    }

    function calcularSubtotales(){
        var $tablaTodasFilas = $("#tabla-detalles-presupuesto tbody tr");
        $tablaTodasFilas.each(function (index) {
            var $tablaFila = $(this);

            $tablaFila.find('.cantidad ,.precio, .buscar-articulo').on('change', function () {
                var cantidad = $tablaFila.find('.cantidad').val();
                var precio = $tablaFila.find('.precio').val();
                var iva = $tablaFila.find('.iva').val();

                var subtotalNeto = parseInt(cantidad, 10) * parseFloat(precio);
                var importeIva = subtotalNeto * parseFloat(iva) / 100;
                var subtotalBruto = subtotalNeto + importeIva;

                if($('#tipo_comprobante_id').val() != 0){
                    subtotalNeto = subtotalBruto;
                }

                if (!isNaN(subtotalNeto)) {
                    $tablaFila.find('.subtotal-neto').val(subtotalNeto.toFixed(2));
                }
                if (!isNaN(subtotalBruto)) {
                    $tablaFila.find('.subtotal-bruto').val(subtotalBruto.toFixed(2));
                }else{
                    $tablaFila.find('.subtotal-bruto').val(subtotalNeto.toFixed(2));
                }
                refrescarTotales();
            });

            $tablaFila.find('.subtotal-bruto').on('change', function () {
                var cantidad = parseInt($tablaFila.find('.cantidad').val(), 10);
                var precioReal = parseFloat($tablaFila.find('.precio-real').val());
                var precio = parseFloat($tablaFila.find('.precio').val());
                var precioFinal = parseFloat($tablaFila.find('.subtotal-bruto').val());
                var iva = parseFloat($tablaFila.find('.iva').val());

                var subtotalNeto = precioFinal / ((iva / 100) + 1);
                precio = subtotalNeto / cantidad;

                if (!isNaN(subtotalNeto)) {
                    $tablaFila.find('.subtotal-neto').val(subtotalNeto.toFixed(2));
                }
                if (!isNaN(precio)) {
                    $tablaFila.find('.precio').val(precio.toFixed(2));
                }
                refrescarTotales();
            });

        });
    }

    function calcularImporteNeto() {
        var importeNeto = 0;
        $('.subtotal-neto').each(function () {
            var auxImporteNeto = parseFloat($(this).val());
            importeNeto += isNaN(auxImporteNeto) ? 0 : auxImporteNeto;
        });
        $('#importe-neto').val(importeNeto.toFixed(2));
    }

    function calcularImporteBruto() {
        var importeBruto = 0;
        $('.subtotal-bruto').each(function () {
            var auxImporteBruto = parseFloat($(this).val());
            importeBruto += isNaN(auxImporteBruto) ? 0 : auxImporteBruto;
        });
        $('#importe-bruto').val(importeBruto.toFixed(2));
    }

    function calcularImporteIva(){
        var importeNeto = parseFloat($('#importe-neto').val()).toFixed(2);
        var importeBruto = parseFloat($('#importe-bruto').val()).toFixed(2);
        var importeIva = importeBruto - importeNeto;
        if (!isNaN(importeIva)) {
            $('#importe-iva').val(importeIva.toFixed(2));
        }else{
            $('#importe-iva').val('0.0');
        }
    }

    function calcularImporteImpuesto(){
        var importeBruto = $('#importe-bruto').val();
        var procentajeImpuesto = $('#porcentaje-impuesto').val();
        var importeImpuesto = parseFloat(importeBruto) * parseFloat(procentajeImpuesto) / 100;
        if (!isNaN(importeImpuesto)) {
            $('#importe-impuesto').val(importeImpuesto.toFixed(2));
        }else{
            $('#importe-impuesto').val('0.0');
        }
    }

    function calcularPorcentajeImpuesto(){
        var importeBruto = $('#importe-bruto').val();
        var importeImpuesto = $('#importe-impuesto').val();
        var procentajeImpuesto = parseFloat(importeImpuesto) * 100 / parseFloat(importeBruto);
        if (!isNaN(procentajeImpuesto)) {
            $('#porcentaje-impuesto').val(procentajeImpuesto.toFixed(2));
        }else{
            $('#porcentaje-impuesto').val('0.0');
        }
    }

    function calcularTotal(){
        var importeBruto = $('#importe-bruto').val();
        var importeImpuesto = $('#importe-impuesto').val();
        var total = parseFloat(importeBruto) + parseFloat(importeImpuesto);
        if (!isNaN(total)) {
            $('#total').val(total.toFixed(2));
        }else{
            $('#total').val('0.0');
        }
        actualizarResto();
    }
</script>
@endsection
