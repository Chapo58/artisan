@extends('frontend.layouts.app')

@section ('title', trans('labels.frontend.presupuesto_titulo')." - ".trans('buttons.general.crud.show'))

@section('content')
    <div class="row page-titles">
        <div class="col-6 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('labels.frontend.presupuesto_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item"><a href="{{url('/ventas/presupuesto')}}">{{ trans('labels.frontend.presupuesto_titulo') }}</a></li>
                <li class="breadcrumb-item">{{ trans('buttons.general.crud.show') }} {{ trans('labels.frontend.presupuesto') }}</li>
                <li class="breadcrumb-item active">{{ $presupuesto }}</li>
            </ol>
        </div>
        <div class="col-6">
            <div class="pull-right">
                <a href="{{ url('/ventas/presupuesto') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans('buttons.general.crud.back') }}</span>
                </a>
                <a href="{{ url('/ventas/presupuesto/imprimir/' . $presupuesto->id) }}" target="_blank" title="Imprimir" class="btn btn-primary">
                    <i class="fa fa-print" aria-hidden="true"></i>
                    <span class="hidden-xs-down">Imprimir</span>
                </a>
                <a href="{{ url('/ventas/presupuesto/generar-venta/' . $presupuesto->id) }}" target="_blank" title="Facturar">
                    <button class="btn btn-info"><i class="fa fa-copy" aria-hidden="true"></i>
                        <span class="hidden-xs-down">Facturar</span>
                    </button>
                </a>
                <a href="javascript:void(0)" title="{{trans("buttons.general.crud.delete")." ".trans("labels.frontend.venta")}}" class="btn btn-danger delete">
                    <i class="fa fa-trash" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans("buttons.general.crud.delete") }}</span>
                </a>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            <div class="card-actions">
                <a class="mytooltip" data-action="collapse"><i class="ti-minus"></i><span class="tooltip-content3">Minimizar Ventana</span></a>
                <a class="btn-minimize mytooltip" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
                <a class="btn-close mytooltip" data-action="close"><i class="ti-close"></i><span class="tooltip-content3">Cerrar Ventana</span></a>
            </div>
            <h4 class="card-title m-b-0">
                Información del Presupuesto
            </h4>
        </div>
        <div class="card-body collapse show b-t">
            <div class="table-responsive">
                <table class="table table-borderless">
                    <tbody>
                    <tr>
                        <th>Cliente</th>
                        <td>
                            @if($presupuesto->cliente)
                                <a href="{{url('/personas/cliente/'.$presupuesto->cliente->id)}}">{{ $presupuesto->cliente }}</a>
                            @else
                                Cliente Eliminado
                            @endif
                        </td>
                        <th>Tipo Comprobante</th><td>{{ $tipos_comprobantes[$presupuesto->tipo_comprobante_id] }}</td>
                        <th>Número</th><td>{{ $presupuesto->numero }}</td>
                        <th>Lista</th>
                        <td>
                            <a href="{{url('/ventas/lista-precios/'.$presupuesto->listaPrecios->id)}}">{{ $presupuesto->listaPrecios }}</a>
                        </td>
                    </tr>
                    <tr>
                        <th>Fecha</th><td>{{ $presupuesto->fecha->format('d/m/Y') }}</td>
                        <th>Fecha Vencimiento</th><td>{{ $presupuesto->fecha_vencimiento->format('d/m/Y') }}</td>
                        <th>Moneda</th><td>{{ $presupuesto->moneda }}</td>
                        <th>Cotizacíon</th><td><b class="green">$ {{ $presupuesto->cotizacion }}</b></td>
                    </tr>
                    </tbody>
                </table>
                @if($presupuesto->descripcion)
                    <strong>Comentario: <i>{{ $presupuesto->descripcion }}</i></strong>
                @endif
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            <div class="card-actions">
                <a class="mytooltip" data-action="collapse"><i class="ti-minus"></i><span class="tooltip-content3">Minimizar Ventana</span></a>
                <a class="btn-minimize mytooltip" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
                <a class="btn-close mytooltip" data-action="close"><i class="ti-close"></i><span class="tooltip-content3">Cerrar Ventana</span></a>
            </div>
            <h4 class="card-title m-b-0">
                Detalles del Presupuesto
            </h4>
        </div>
        <div class="card-body collapse show b-t">
            <div class="table-responsive">
                <table class="table color-table muted-table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>
                            Nº
                        </th>
                        <th>
                            Cantidad
                        </th>
                        <th>
                            Artículo
                        </th>
                        <th>
                            Pre/Uni. Neto
                        </th>
                        <th>
                            Neto
                        </th>
                        <th>
                            IVA
                        </th>
                        <th>
                            Bruto
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!$presupuesto->detalles->isEmpty())
                        @foreach($presupuesto->detalles as $detalle)
                            <tr>
                                <td>
                                    {{ $loop->index + 1 }}
                                </td>
                                <td>
                                    {{ number_format($detalle->cantidad , 0) }}
                                </td>
                                <td>
                                    @if(isset($detalle->articulo))
                                        <a href="{{url('/articulos/articulo/'.$detalle->articulo->id)}}">
                                            @if($detalle->descripcion)
                                                {{$detalle->descripcion}}
                                            @else
                                               ({{ $detalle->articulo->codigo }}) {{ $detalle->articulo->nombre }}
                                            @endif
                                        </a>
                                    @else
                                        Artículo Eliminado
                                    @endif
                                </td>
                                <td>
                                    <b class="text-success">{{ $presupuesto->moneda->signo }} {{ number_format($detalle->precio_neto, 2) }}</b>
                                </td>
                                <td>
                                    <b class="text-success">{{ $presupuesto->moneda->signo }} {{ number_format($detalle->cantidad*$detalle->precio_neto , 2) }}</b>
                                </td>
                                <td>
                                    <b class="text-success"> {{ number_format($porcentajes_iva[$detalle->porcentaje_iva], 2) }} %</b>
                                </td>
                                <td>
                                    <b class="text-success">{{ $presupuesto->moneda->signo }} {{ number_format(($detalle->cantidad * $detalle->precio_neto) * (($porcentajes_iva[$detalle->porcentaje_iva]/100) + 1), 2) }}</b>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
                <table class="table table-borderless">
                    <tbody>
                    <tr>
                        <th class="text-right" colspan="3">Importe Neto</th><td><b class="text-success">{{ $presupuesto->moneda->signo }} {{ number_format($presupuesto->importe_neto, 2) }}</b></td>
                    </tr>
                    <tr>
                        <th class="text-right" colspan="3">Iva ($)</th><td><b class="text-success">{{ $presupuesto->moneda->signo }} {{ number_format($presupuesto->importe_iva, 2) }}</b></td>
                    </tr>
                    <tr>
                        <th class="text-right">Impuesto/Descuento (%)</th><td><b class="text-success">{{ number_format($presupuesto->porcentaje_descuento, 2) }} %</b></td>
                        <th class="text-right">Impuesto/Descuento ($)</th><td><b class="text-success">{{ $presupuesto->moneda->signo }} {{ number_format($presupuesto->importe_descuento, 2) }}</b></td>
                    </tr>
                    <tr>
                        <th class="text-right" colspan="3">Total</th><td><b class="text-success">{{ $presupuesto->moneda->signo }} {{ number_format($presupuesto->total, 2) }}</b></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection

@section('after-scripts')
<script>
    $("body").on("click", ".delete", function () {
        swal({
            title: "{{trans('buttons.general.crud.delete').' '.trans('labels.frontend.presupuesto')}}",
            text: "¿Realmente desea eliminar este registro?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#ff8726',
            confirmButtonText: "Eliminar"
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '{{ url('/ventas/presupuesto/'.$presupuesto->id) }}',
                    data: {"_method" : 'DELETE'},
                    success: function (msg) {
                        swal("Eliminado!", "El registro ha sido correctamente eliminado.", "success").then((result) => { window.location.href = '{{url('/ventas/presupuesto')}}'; });
                    }
                });
            }
        });
    });
</script>
@endsection