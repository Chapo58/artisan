<div class="card">
    <div class="card-body">
        <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
        <div class="row form-group">
            <div class="col-md-5">
                {!! Form::label('cliente_id', 'Cliente') !!}
                <div class="row">
                    <div class="col-md-10">
                        <input id="buscar_cliente" name="buscar_cliente" class="form-control" placeholder="Buscar..." type="text" required="required" value="{{ isset($presupuesto->cliente) ? $presupuesto->cliente : '' }}" {{ isset($editar) ? 'disabled' : '' }}>
                        <input id="cliente_id" name="cliente_id" type="hidden" value="{{ isset($presupuesto->cliente) ? $presupuesto->cliente->id : '' }}">
                        {!! $errors->first('cliente_id', '<p class="text-danger">:message</p>') !!}
                    </div>
                    <div class="col-md-2">
                        <button type="button" class="btn btn-secondary" id="agregar-cliente"><i class="fa fa-plus"></i> </button>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                {!! Form::label('tipo_comprobante_id', 'Comprobante') !!}
                {!! Form::select('tipo_comprobante_id', $tipos_comprobantes, isset($presupuesto) ? $presupuesto->tipo_comprobante_id : 1, array('class' => 'form-control', 'required' => 'required', isset($editar) ? 'disabled' : '')) !!}
                {!! $errors->first('tipo_comprobante_id', '<p class="text-danger">:message</p>') !!}
            </div>
            <div class="col-md-4">
                {!! Form::label('numero', 'Número') !!}
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-hashtag"></i>
                    </div>
                    {!! Form::text('numero', isset($presupuesto) ? $presupuesto->numero : $numero_presupuesto, ['id' =>'numero' ,'class' => 'form-control', 'required' => 'required', 'readonly']) !!}
                </div>
                {!! $errors->first('numero', '<p class="text-danger">:message</p>') !!}
            </div>
            <div class="col-md-2">
                {!! Form::label('lista_precios_id', 'Lista de Precios') !!}
                {!! Form::select('lista_precios_id', $lista_precios, isset($presupuesto->listaPrecios) ? $presupuesto->listaPrecios->id : 0, ['class' => 'form-control', 'required' => 'required', isset($editar) ? 'disabled' : '']) !!}
                {!! $errors->first('lista_precios_id', '<p class="text-danger">:message</p>') !!}
            </div>
            <div class="col-md-3">
                {!! Form::label('fecha', 'Fecha') !!}
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    {!! Form::text('fecha', isset($presupuesto) ? $presupuesto->fecha->format('d/m/Y') : $fecha_hoy, ['class' => 'form-control', 'required' => 'required', 'readonly']) !!}
                </div>
                {!! $errors->first('fecha', '<p class="text-danger">:message</p>') !!}
            </div>
            <div class="col-md-3">
                {!! Form::label('fecha_vencimiento', 'Fecha Vencimiento') !!}
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    {!! Form::text('fecha_vencimiento', isset($presupuesto) ? $presupuesto->fecha_vencimiento->format('d/m/Y') : $fecha_vencimiento, ['class' => 'form-control fecha', 'required' => 'required']) !!}
                </div>
                {!! $errors->first('fecha_vencimiento', '<p class="text-danger">:message</p>') !!}
            </div>
            <div class="col-md-4">
                {!! Form::label('moneda_id', 'Moneda') !!}
                <div class="row">
                    <div class="col-md-7">
                        {!! Form::select('moneda_id', $monedas, isset($presupuesto->moneda) ? $presupuesto->moneda->id : (isset($monedaPorDefecto) ? $monedaPorDefecto->id : ''), ['class' => 'form-control', 'id' => 'moneda_id', 'required' => 'required', isset($editar) ? 'disabled' : '']) !!}
                        {!! $errors->first('moneda_id', '<p class="text-danger">:message</p>') !!}
                    </div>
                    <div class="col-md-5">
                        {!! Form::number('cotizacion', isset($presupuesto) ? $presupuesto->cotizacion : (isset($monedaPorDefecto) ? $monedaPorDefecto->cotizacion : 1), ['class' => 'form-control', 'step' => '0.01', 'id' => 'cotizacion', isset($editar) ? 'readonly' : '']) !!}
                        {!! $errors->first('cotizacion', '<p class="text-danger">:message</p>') !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <table id="tabla-detalles-presupuesto" class="table color-table info-table form-material table-sm">
                <thead>
                    <tr>
                        <th class="text-center">#</th>
                        <th class="text-center" style="width:10%;">Cantidad</th>
                        <th style="width:30%;">Artículo</th>
                        <th class="text-center" style="width:15%;">Pre/Uni. Neto</th>
                        <th class="text-center" style="width:15%;">Neto</th>
                        <th class="text-center" style="width:15%;">IVA</th>
                        <th class="text-center" style="width:15%;">Bruto</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                @if (isset($presupuesto))
                    @foreach($presupuesto->detalles as $detalle)
                        <tr>
                            <td>
                                {{ $loop->index + 1 }}
                                <input type="hidden" class="id" name="detalles[{{ $detalle->id }}][id]" value="{{ $detalle->id }}">
                            </td>
                            <td>{!! Form::number('detalles['.$detalle->id.'][cantidad]', $detalle->cantidad, ['class' => 'form-control cantidad text-center', 'step' => '1', 'min' => '0', 'max' => '', 'title' => '']) !!}</td>
                            <td>
                                <input type="text" class="form-control buscar-articulo" name="detalles[{{ $detalle->id }}][articulo]" placeholder="{{ trans('strings.backend.general.search_placeholder') }}" value="({{ $detalle->articulo->codigo }}) {{ $detalle->articulo->nombre }}">
                                <input type="hidden" class="articulo-id" name="detalles[{{ $detalle->id }}][articulo_id]" value="{{ $detalle->articulo_id }}">
                            </td>
                            <td>
                                <div class="input-group">
                                    <div class="left-icon">$</div>
                                    {!! Form::number('detalles['.$detalle->id.'][precio_neto]', $detalle->precio_neto, ['class' => 'form-control precio text-right', 'step' => '0.01', 'readonly', 'tabindex' => '9999']) !!}
                                </div>
                            </td>
                            <td>
                                <div class="input-group">
                                    <div class="left-icon">$</div>
                                    {!! Form::number('detalles['.$detalle->id.'][total_neto]', $detalle->cantidad*$detalle->precio_neto, ['class' => 'form-control subtotal-neto text-right', 'readonly', 'step' => '0.01', 'tabindex' => '9999']) !!}
                                </div>
                            </td>
                            <td>
                                <div class="input-group">
                                    <div class="left-icon">%</div>
                                    {!! Form::text('detalles['.$detalle->id.'][iva]', number_format($porcentajes_iva[$detalle->porcentaje_iva], 2), array('class' => 'form-control iva text-right', 'readonly', 'tabindex' => '9999')) !!}
                                </div>
                                <input type="hidden" class="porcentaje-iva" name="detalles[{{ $detalle->id }}][porcentaje_iva]" value="{{ $detalle->porcentaje_iva }}">
                            </td>
                            <td>
                                <div class="input-group">
                                    <div class="left-icon">$</div>
                                    {!! Form::number('detalles['.$detalle->id.'][total_bruto]', number_format(($detalle->cantidad*$detalle->precio_neto) * ($porcentajes_iva[$detalle->porcentaje_iva]/100 + 1), 2), ['class' => 'form-control subtotal-bruto text-right', 'readonly', 'step' => '0.01', 'tabindex' => '9999']) !!}
                                    <input type="hidden" class="impuesto-interno" name="detalles[{{ $detalle->id }}][importe_impuesto_interno]" value="{{ $detalle->importe_impuesto_interno }}">
                                </div>
                            </td>
                            <td>
                                <button type="button" class="btn btn-danger btn-sm borrar-detalle" name="detalles[{{ $detalle->id }}][eliminar]">
                                    <i class="fa fa-trash-o" aria-hidden="true"></i>
                                </button>
                                <input type="hidden" class="estado" name="detalles[{{ $detalle->id }}][estado]" value="1">
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
                <tfoot>
                <tr>
                    <td>
                        ...
                    </td>
                    <td>{!! Form::number('', '', ['class' => 'form-control', 'id' => 'agregar-detalle-de-cantidad']) !!}</td>
                    <td>
                        <input type="text" class="form-control" id="agregar-detalle-de-articulo" placeholder="Nuevo Detalle...">
                    </td>
                    <td>
                        <div class="input-group">
                            <div class="left-icon">$</div>
                            {!! Form::number('', null, ['class' => 'form-control', 'readonly', 'tabindex' => '9999']) !!}
                        </div>
                    </td>
                    <td>
                        <div class="input-group">
                            <div class="left-icon">$</div>
                            {!! Form::number('', null, ['class' => 'form-control', 'readonly', 'tabindex' => '9999']) !!}
                        </div>
                    </td>
                    <td>
                        <div class="input-group">
                            <div class="left-icon">%</div>
                            {!! Form::number('', null, ['class' => 'form-control', 'readonly', 'tabindex' => '9999']) !!}
                        </div>
                    </td>
                    <td>
                        <div class="input-group">
                            <div class="left-icon">$</div>
                            {!! Form::number('', null, ['class' => 'form-control', 'readonly', 'tabindex' => '9999']) !!}
                        </div>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <th colspan="3">
                        <a href="javascript:void(0)" class="btn btn-info btn-sm" id="agregar-detalle">
                            <i class="fa fa-plus" aria-hidden="true"></i> Agregar Detalle
                        </a>
                    </th>
                    <th><span class="pull-right">Subtot<span class="hidden-sm hidden-xs">ales</span>.</span></th>
                    <th>
                        <div class="input-group">
                            <div class="left-icon">$</div>
                            {!! Form::number('importe_neto', null, ['id' => 'importe-neto', 'class' => 'form-control text-right', 'readonly']) !!}
                        </div>
                    </th>
                    <th>
                        <div class="input-group">
                            <div class="left-icon">$</div>
                        {!! Form::number('importe_iva', null, ['id' => 'importe-iva', 'class' => 'form-control text-right', 'step' => '0.01', 'readonly']) !!}
                    </th>
                    <th>
                        <div class="input-group">
                            <div class="left-icon">$</div>
                            {!! Form::number('importe_bruto', null, ['id' => 'importe-bruto', 'class' => 'form-control text-right', 'readonly']) !!}
                        </div>
                    </th>
                </tr>
                <tr>
                    <th colspan="4" rowspan="2">
                        <div class="form-group">
                            {!! Form::label('descripcion', 'Comentario') !!}
                            {!! Form::textarea('descripcion', null, ['class' => 'form-control', 'rows' => '4']) !!}
                            {!! $errors->first('descripcion', '<p class="text-danger">:message</p>') !!}
                        </div>
                    </th>
                    <th><span class="pull-right">Int<span class="hidden-sm hidden-xs">eres</span>.(+)/</br>Desc<span class="hidden-sm hidden-xs">uento</span>.(-)</span></th>
                    <th>
                        <div class="input-group">
                            <div class="left-icon">%</div>
                            {!! Form::number('porcentaje_descuento', null, ['id' => 'porcentaje-impuesto', 'class' => 'form-control text-right', 'step' => '0.01']) !!}
                        </div>
                    </th>
                    <th>
                        <div class="input-group">
                            <div class="left-icon">$</div>
                            {!! Form::number('importe_descuento', null, ['id' => 'importe-impuesto', 'class' => 'form-control text-right', 'step' => '0.01']) !!}
                        </div>
                    </th>
                    <th></th>
                </tr>
                <tr>
                    <th></th>
                    <th colspan="2">
                        <div class="card card-inverse card-info">
                            <div class="box bg-info text-center">
                                <h1 class="font-light text-white">
                                    <div class="pull-left">TOTAL:</div>
                                    <div class="pull-right" id="totalGrande">$ 0.00</div>
                                </h1>
                            </div>
                        </div>
                    </th>
                    {!! Form::number('total', null, ['id' => 'total', 'class' => 'd-none', 'step' => '0.01', 'required' => 'required']) !!}
                </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>

<div class="card">
    <div class="card-body">
        <div class="form-group">
            {{ Form::submit(isset($submitButtonText) ? $submitButtonText : trans('buttons.general.save'), ['class' => 'btn btn-themecolor btn-lg pull-right']) }}
        </div>
    </div>
</div>

@include('frontend.personas.cliente.cliente_dialog')
