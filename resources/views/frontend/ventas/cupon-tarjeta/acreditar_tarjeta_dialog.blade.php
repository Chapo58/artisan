<div class="modal fade" id="modalAcreditar" tabindex="-1" role="dialog" aria-labelledby="acreditarTarjetaModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="acreditarTarjetaModalLabel">Acreditar Cupon</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar"><span aria-hidden="true">&times;</span></button>
            </div>
            {!! Form::open([
                    'method'=>'POST',
                    'action' => ['Frontend\Ventas\CuponTarjetaController@acreditar'],
                    'style' => 'display:inline'
                ]) !!}
            <div class="modal-body">
                <div class="form-group">
                    {!! Form::label('cuenta_bancaria_id', 'Cuenta Bancaria') !!}
                    {!! Form::select('cuenta_bancaria_id', $cuentaBancaria, '', array('class' => 'form-control', 'required' => 'required')) !!}
                </div>
                {{ Form::hidden('idCupon',0,['id' => 'idCupon']) }}
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-themecolor">Depositar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
