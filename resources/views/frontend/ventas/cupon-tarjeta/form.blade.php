<div class="card">
    <div class="card-body">
        <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
        <br>
        <div class="row form-group">
            <div class="col-md-6">
                {!! Form::label('fecha', 'Fecha') !!}
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    {!! Form::date('fecha', isset($cuponTarjeta) ? $cuponTarjeta->fecha : $fecha, ['class' => 'form-control', 'required' => 'required', isset($editar) ? 'disabled' : '']) !!}
                </div>
                {!! $errors->first('fecha', '<p class="text-danger">:message</p>') !!}
            </div>
            <div class="col-md-6">
                {!! Form::label('importe', 'Importe') !!}
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-usd"></i>
                    </div>
                    {!! Form::number('importe', null, ['class' => 'form-control', 'required' => 'required', 'step' => '.01']) !!}
                </div>
                {!! $errors->first('importe', '<p class="text-danger">:message</p>') !!}
            </div>
        </div>
        <div class="row form-group">
            <div class="col-md-6">
                {!! Form::label('tarjeta_id', 'Tarjeta') !!}
                {!! Form::select('tarjeta_id', $tarjetas, isset($cuponTarjeta->tarjeta) ? $cuponTarjeta->tarjeta->id : '', ['class' => 'form-control', 'required' => 'required']) !!}
                {!! $errors->first('tarjeta_id', '<p class="text-danger">:message</p>') !!}
            </div>
            <div class="col-md-6">
                {!! Form::label('plan_tarjeta_id', 'Plan de Tarjeta') !!}
                {!! Form::select('plan_tarjeta_id', $planes_tarjeta, isset($cuponTarjeta->planTarjeta) ? $cuponTarjeta->planTarjeta->id : '', ['class' => 'form-control', 'required' => 'required']) !!}
                {!! $errors->first('plan_tarjeta_id', '<p class="text-danger">:message</p>') !!}
            </div>
        </div>
        <div class="row form-group">
            <div class="col-md-4">
                {!! Form::label('lote', 'Lote') !!}
                {!! Form::number('lote', null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'N° Lote']) !!}
                {!! $errors->first('lote', '<p class="text-danger">:message</p>') !!}
            </div>
            <div class="col-md-4">
                {!! Form::label('cupon', 'Cupon') !!}
                {!! Form::number('cupon', null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'N° Cupon']) !!}
                {!! $errors->first('cupon', '<p class="text-danger">:message</p>') !!}
            </div>
            <div class="col-md-4">
                {!! Form::label('autorizacion', 'Autorización') !!}
                {!! Form::number('autorizacion', null, ['class' => 'form-control', 'placeholder' => 'N° Autorización']) !!}
                {!! $errors->first('autorizacion', '<p class="text-danger">:message</p>') !!}
            </div>
        </div>
        <div class="row form-group">
            <div class="col-md-12">
                {!! Form::label('titular', 'Titular') !!}
                {!! Form::text('titular', null, ['class' => 'form-control', 'placeholder' => 'Nombre Completo del titular de la Tarjeta de Credito']) !!}
                {!! $errors->first('titular', '<p class="text-danger">:message</p>') !!}
            </div>
        </div>
        <hr>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-12">
                    {{ Form::submit(isset($submitButtonText) ? $submitButtonText : trans('buttons.general.save'), ['class' => 'btn btn-lg btn-themecolor pull-right']) }}
                </div>
            </div>
        </div>
    </div>
</div>
