@extends('frontend.layouts.app')

@section ('title', trans('labels.frontend.cupontarjeta_titulo')." - ".trans('buttons.general.crud.show'))

@section('content')
    <div class="row page-titles">
        <div class="col-6 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('labels.frontend.cupontarjeta_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item"><a href="{{url('/ventas/cupon-tarjeta')}}">{{ trans('labels.frontend.cupontarjeta_titulo') }}</a></li>
                <li class="breadcrumb-item">{{ trans('buttons.general.crud.show') }} {{ trans('labels.frontend.cupontarjeta') }}</li>
                <li class="breadcrumb-item active">{{ $cuponTarjeta }}</li>
            </ol>
        </div>
        <div class="col-6">
            <div class="pull-right">
                <a href="{{ url('/ventas/cupon-tarjeta') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans('buttons.general.crud.back') }}</span>
                </a>
                <a href="{{ url('/ventas/cupon-tarjeta/' . $cuponTarjeta->id . '/edit') }}" title="{{ trans('buttons.general.crud.edit') }}" class="btn btn-info">
                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans('buttons.general.crud.edit') }}</span>
                </a>
                <a href="javascript:void(0)" title="{{trans("buttons.general.crud.delete")." ".trans("labels.frontend.cupontarjeta")}}" class="btn btn-danger delete">
                    <i class="fa fa-trash" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans("buttons.general.crud.delete") }}</span>
                </a>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
            <h4 class="font-medium m-t-30">
                Cupon {{ $cuponTarjeta }}
                @if($cuponTarjeta->estado == 5)
                    <span class="label label-warning">{{$estados[$cuponTarjeta->estado]}}</span>
                @else
                    <span class="label label-success">{{$estados[$cuponTarjeta->estado]}}</span>
                @endif
            </h4>
            <hr>
            <div class="row">
                <div class="col-md-2 font-weight-bold">Fecha</div>
                <div class="col-md-3">{{ $cuponTarjeta->fecha->format('d/m/Y') }}</div>

                <div class="col-md-3 font-weight-bold">Importe</div>
                <div class="col-md-4">$ {{ $cuponTarjeta->importe }}</div>
            </div><hr>
            <div class="row">
                <div class="col-md-2 font-weight-bold">Tarjeta</div>
                <div class="col-md-3">{{ $cuponTarjeta->tarjeta }}</div>

                <div class="col-md-3 font-weight-bold">Plan Tarjeta</div>
                <div class="col-md-4">{{ isset($cuponTarjeta->planTarjeta) ? $cuponTarjeta->planTarjeta : '' }}</div>
            </div><hr>
            <div class="row">
                <div class="col-md-2 font-weight-bold">Lote / Cupon</div>
                <div class="col-md-3">{{ $cuponTarjeta->lote." / ".$cuponTarjeta->cupon }}</div>

                <div class="col-md-3 font-weight-bold">Autorizacion</div>
                <div class="col-md-4">{{ $cuponTarjeta->autorizacion }}</div>
            </div><hr>
            <div class="row">
                <div class="col-md-2 font-weight-bold">Titular</div>
                <div class="col-md-3">{{ $cuponTarjeta->titular }}</div>

                <div class="col-md-3 font-weight-bold">Comprobante Asociado</div>
                <div class="col-md-4">{{ isset($cuponTarjeta->entidadRelacionada) ? $cuponTarjeta->entidadRelacionada : '' }}</div>
            </div><hr>
            @if($cuponTarjeta->fecha_acreditacion)
            <div class="row">
                <div class="col-md-2 font-weight-bold">Fecha de Acreditacion</div>
                <div class="col-md-3">{{ $cuponTarjeta->fecha_acreditacion->format('d/m/Y') }}</div>

                <div class="col-md-3 font-weight-bold">Cuenta Bancaria</div>
                <div class="col-md-4">{{ ($cuponTarjeta->cuentaBancaria) ? $cuponTarjeta->cuentaBancaria : '' }}</div>
            </div><hr>
            @endif
            <div class="row">
                <div class="col-md-3 font-weight-bold">Estado</div>
                <div class="col-md-9">{{ $estados[$cuponTarjeta->estado] }}</div>
            </div><hr>
            <div class="row">
                <div class="col-md-3 font-weight-bold">Creado el</div>
                <div class="col-md-9">{{ $cuponTarjeta->created_at->format('d/m/Y H:m') }}</div>
            </div><hr>
            <div class="row">
                <div class="col-md-3 font-weight-bold">Ultima Modificación</div>
                <div class="col-md-9">{{ $cuponTarjeta->updated_at->format('d/m/Y H:m') }}</div>
            </div>
        </div>
    </div>
@endsection

@section('after-scripts')
    <script>
        $("body").on("click", ".delete", function () {
            swal({
                title: "{{trans('buttons.general.crud.delete').' '.trans('labels.frontend.cupontarjeta')}}",
                text: "¿Realmente desea eliminar este registro?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#ff8726',
                confirmButtonText: "Eliminar"
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        type: 'POST',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: '{{ url('/ventas/cupon-tarjeta/'.$cuponTarjeta->id) }}',
                        data: {"_method" : 'DELETE'},
                        success: function (msg) {
                            swal("Eliminado!", "El registro ha sido correctamente eliminado.", "success").then((result) => { window.location.href = '{{url('/ventas/cupon-tarjeta')}}'; });
                        }
                    });
                }
            });
        });
    </script>
@endsection
