<div class="modal fade" id="modal-cupon-tarjeta" tabindex="-1" role="dialog" aria-labelledby="cuponTarjetaModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="cuponTarjetaModalLabel">Cobro con Tarjeta</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    {!! Form::label('tarjeta_id', 'Tarjeta') !!}
                    {!! Form::select('tarjeta_id', $tarjetas, '',['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('plan_tarjeta_id', 'Planes Tarjeta') !!}
                    {!! Form::select('plan_tarjeta_id', ['' => 'Seleccione un plan...'], '',['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('importe_cupon_tarjeta', 'Importe') !!}
                    <div class="input-group">
                        <div class="input-group-addon"><i class="fa fa-usd"></i></div>
                        {!! Form::number('importe_cupon_tarjeta', '',['class' => 'form-control', 'step' => '0.01']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('calculo', 'Cálculo') !!}
                    Interés: <b><span id="interes_plan_tarjeta">0.0</span>%</b>, Cuotas: <b><span id="cuotas_plan_tarjeta">1</span></b>, Recargo: <b>$ <span id="recargo_plan_tarjeta">0</span></b>
                </div>
                <div class="row form-group">
                    <div class="col-md-6">
                        {!! Form::label('lote', 'Lote') !!}
                        {!! Form::number('lote', '',['class' => 'form-control']) !!}
                    </div>
                    <div class="col-md-6">
                        {!! Form::label('cupon', 'Numero Cupón') !!}
                        {!! Form::number('cupon', '',['class' => 'form-control']) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button id="guardar-cupon-tarjeta" type="button" class="btn btn-themecolor">Guardar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>

@section('scripts-dialog-tarjeta')
    <script>
        $('select#tarjeta_id').on('change', function () {
            buscarPlanesTarjeta();
        });

        $('select#plan_tarjeta_id').on('change', function () {
            buscarDatosPlanTarjeta();
        });

        $('input#importe_cupon_tarjeta').keyup(function() {
            calcularRecargoTarjeta();
        });

        function buscarPlanesTarjeta() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $( "input[name*='_token']" ).val()
                }
            });
            var tarjeta_id = $('select#tarjeta_id option:selected').val();
            $.ajax({
                type: "POST",
                url: '{{ url("/configuraciones/tarjeta/planesDeTarjeta") }}',
                data: {tarjeta_id: tarjeta_id},
                success: function( msg ) {
                    $('#plan_tarjeta_id').empty();
                    $('select#plan_tarjeta_id').append($('<option>', {
                        value: '',
                        text: 'Seleccione un plan...'
                    }));
                    $.each(msg, function(key,plan) {
                        $('select#plan_tarjeta_id').append($('<option>', {
                            value: plan.id,
                            text: plan.nombre
                        }));
                    });
                }
            });
        }

        function buscarDatosPlanTarjeta() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $( "input[name*='_token']" ).val()
                }
            });
            var plan_tarjeta_id = $('select#plan_tarjeta_id option:selected').val();
            $.ajax({
                type: "POST",
                url: '{{ url("/configuraciones/plan-tarjeta/datosDePlanTarjeta") }}',
                data: {plan_tarjeta_id: plan_tarjeta_id},
                success: function( msg ) {
                    $("span#interes_plan_tarjeta").text(msg.interes);
                    $("span#cuotas_plan_tarjeta").text(msg.cuotas);
                    calcularRecargoTarjeta();
                }
            });
        }

        function calcularRecargoTarjeta() {
            importe = parseFloat($('input#importe_cupon_tarjeta').val());
            interes = parseFloat($("span#interes_plan_tarjeta").text());
            if(importe == 0 || isNaN(importe)){
                $('input#importe_cupon_tarjeta').parent().addClass('has-error');
                $('input#importe_cupon_tarjeta').focus();
            }else if(interes != 0){
                recargo = (interes * importe/ 100).toFixed(2);
            }else{
                recargo = 0;
            }
            $("span#recargo_plan_tarjeta").text(recargo);
        }
    </script>
@endsection