<div class="modal fade" id="modal-cuenta-corriente" tabindex="-1" role="dialog" aria-labelledby="cuentaCorrienteModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="cuentaCorrienteModalLabel">Cobro a Cuenta Corriente</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    {!! Form::label('tipo_cuenta_corriente_id', 'Tipo Cuenta Corriente') !!}
                    {!! Form::select('tipo_cuenta_corriente_id', $tipos_cuenta_corriente, '',['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('importe_cuenta_corriente', 'Importe') !!}
                    <div class="input-group">
                        <div class="input-group-addon"><i class="fa fa-usd"></i></div>
                        {!! Form::number('importe_cuenta_corriente', '',['class' => 'form-control', 'step' => '0.01']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('calculo', 'Cálculo') !!}
                    Interés: <b><span id="interes_cuenta_corriente">0.0</span>%</b>, Cuotas: <b><span id="cuotas_cuenta_corriente">1</span></b>, Recargo: <b>$ <span id="recargo_cuenta_corriente">0</span></b>
                </div>
            </div>
            <div class="modal-footer">
                <button id="guardar-cuenta-corriente" type="button" class="btn btn-themecolor">Guardar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>

@section('scripts-dialog-cuenta-corriente')
    <script>
        $('select#tipo_cuenta_corriente_id').on('change', function () {
            buscarDatosTipo();
        });

        $('input#importe_cuenta_corriente').keyup(function() {
            calcularRecargoTipo();
        });

        function buscarDatosTipo() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $( "input[name*='_token']" ).val()
                }
            });
            var tipo_cuenta_corriente_id = $('select#tipo_cuenta_corriente_id option:selected').val();
            $.ajax({
                type: "POST",
                url: '{{ url("/configuraciones/tipo-cuenta-corriente/datosTipo") }}',
                data: {tipo_cuenta_corriente_id: tipo_cuenta_corriente_id},
                success: function( msg ) {
                    $("span#interes_cuenta_corriente").text(msg.interes);
                    $("span#cuotas_cuenta_corriente").text(msg.cuotas);
                    calcularRecargoTipo();
                }
            });
        }

        function calcularRecargoTipo() {
            importe = parseFloat($('input#importe_cuenta_corriente').val());
            interes = parseFloat($("span#interes_cuenta_corriente").text());
            if(importe == 0 || isNaN(importe)){
                $('input#importe_cuenta_corriente').parent().addClass('has-error');
                $('input#importe_cuenta_corriente').focus();
            }else if(interes != 0){
                recargo = (interes * importe/ 100).toFixed(2);
            }else{
                recargo = 0;
            }
            $("span#recargo_cuenta_corriente").text(recargo);
        }
    </script>
@endsection