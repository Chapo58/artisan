<div class="modal fade" id="modal-cheque-nuevo" tabindex="-1" role="dialog" aria-labelledby="chequeModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="chequeModalLabel">Nuevo Cheque</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    {!! Form::label('banco_id', 'Banco') !!}
                    {!! Form::select('banco_id', $bancos, 0,['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('numero_cheque', 'Número') !!}
                    <div class="input-group">
                        <div class="input-group-addon"><i class="fa fa-hashtag"></i></div>
                        {!! Form::text('numero_cheque', '',['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('importe_cheque', 'Importe') !!}
                    <div class="input-group">
                        <div class="input-group-addon"><i class="fa fa-usd"></i></div>
                        {!! Form::number('importe_cheque', '',['class' => 'form-control', 'step' => '0.01']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('firmante', 'Firmante') !!}
                    {!! Form::text('firmante', '',['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('vencimiento', 'Vencimiento') !!}
                    <div class="input-group">
                        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                        {!! Form::text('vencimiento', '',['class' => 'form-control fecha']) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button id="guardar-cheque" type="button" class="btn btn-themecolor">Guardar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>