<div class="modal fade" id="modal-deposito-nuevo" tabindex="-1" role="dialog" aria-labelledby="depositoModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="depositoModalLabel">Nuevo Depósito Bancario</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    {!! Form::label('cuenta_bancaria_id', 'Cuenta Bancaria') !!}
                    {!! Form::select('cuenta_bancaria_id', $cuentas_bancarias, 0,['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('importe_deposito_bancario', 'Importe') !!}
                    <div class="input-group">
                        <div class="input-group-addon"><i class="fa fa-usd"></i></div>
                        {!! Form::number('importe_deposito_bancario', '',['class' => 'form-control', 'step' => '0.01']) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button id="guardar-deposito-bancario" type="button" class="btn btn-themecolor">Guardar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>