<div class="card">
    <div class="card-header">
        <div class="card-actions">
            <a class="mytooltip" data-action="collapse"><i class="ti-minus"></i><span class="tooltip-content3">Minimizar Ventana</span></a>
            <a class="btn-minimize mytooltip" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
        </div>
        <h4 class="card-title m-b-0">
            Forma de Cobro
        </h4>
    </div>
    <div class="card-body collapse show b-t">
        <div class="row form-group">
            <div class="col-md-12">
                {!! Form::label('forma_cobro', 'Formas de Cobro') !!}
                <div class="row">
                    <div class="col-md-6">
                        {!! Form::select('forma_cobro', $formas_cobro, 0, ['class' => 'form-control']) !!}
                        {!! $errors->first('forma_cobro', '<p class="text-danger">:message</p>') !!}
                    </div>
                    <div class="col-md-2">
                        <button type="button" class="btn btn-success" id="agregar-detalle-cobro">
                            <i class="fa fa-plus" aria-hidden="true"></i> Agregar
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <table id="tabla-detalles-cobro" class="table color-table info-table form-material">
                <thead>
                <tr>
                    <th style="width:50%;">Descripción</th>
                    <th style="width:25%;">Monto</th>
                    <th style="width:25%;">Interés</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @if (isset($venta))
                    @foreach($venta->detallesCobros as $detalleCobro)
                        <tr>
                            <td>
                                {{ $detalleCobro->descripcion }}
                            </td>
                            <td>
                                <div class="input-group">
                                    <div class="left-icon">$</div>
                                    <input class="form-control green monto" name="detalles_cobro[{{ $detalleCobro->id }}][monto]" type="number" step="0.01" value="{{ $detalleCobro->monto }}" readonly>
                                </div>
                            </td>
                            <td>
                                <div class="input-group">
                                    <div class="left-icon">$</div>
                                    <input class="form-control green interes" name="detalles_cobro[{{ $detalleCobro->id }}][interes]" type="number" value="{{ $detalleCobro->interes }}" readonly>
                                </div>
                            </td>
                            <td>
                                <button class="btn btn-danger btn-circle borrar-detalle-cobro" name="detalles_cobro[{{ $detalleCobro->id }}][eliminar]" title="Borrar Detalle Cobro">
                                    <i class="fa fa-trash-o" aria-hidden="true"></i>
                                </button>
                                <input type="hidden" name="detalles_cobro[{{ $detalleCobro->id }}][estado]" class="estado" value="1">
                                <input type="hidden" name="detalles_cobro[{{ $detalleCobro->id }}][id]" value="{{ $detalleCobro->id }}">
                                <input type="hidden" name="detalles_cobro[{{ $detalleCobro->id }}][forma_cobro]" value="{{ $detalleCobro->forma_cobro }}">
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="card card-inverse card-danger">
                    <div class="box bg-danger text-center">
                        <h2 class="font-light text-white">
                            <div class="pull-left">DIFERENCIA:</div>
                            <div class="pull-right" id="diferencia">$ 0.00</div>
                        </h2>
                    </div>
                </div>
            </div>
            <div class="col-md-3"></div>
            <div class="col-md-5">
                <div class="card card-inverse card-info">
                    <div class="box bg-info text-center">
                        <h2 class="font-light text-white">
                            <div class="pull-left">TOTAL COBRO:</div>
                            <div class="pull-right" id="total-final">$ 0.00</div>
                        </h2>
                    </div>
                </div>
            </div>
        </div>
        {!! Form::number('total_cobro', null, ['id' => 'total-cobro','class' => 'd-none', 'readonly']) !!}
    </div>
</div>

    @include('frontend.ventas.cobro.cobro_cheque_dialog')
    @include('frontend.ventas.cobro.cobro_cuenta_bancaria_dialog')
    @include('frontend.ventas.cobro.cobro_cupon_tarjeta_dialog')
    @include('frontend.ventas.cobro.cobro_cuenta_corriente_dialog')

@section('scripts-cobros')
    <script>
          $('.form-horizontal').submit(function(e) {
              if(!$("[name='guardar']").val()) {
                  detalles = $('input[name^="detalles_cobro"]');
                  if(detalles.length == 0){
                      e.preventDefault();
                      swal("Cobros", "Por favor ingrese al menos un medio de cobro para finalizar.", "warning");
                  }
              }
         });

        $("#agregar-detalle-cobro").click(function(){
            formaPagoDescripcion = $('#forma_cobro option:selected').text();
            formaPagoValor = $('#forma_cobro option:selected').val();

            monto = calcularMontoCobro();
            if(formaPagoValor == '0'){//Efectivo
                agregarDetalleCobro(formaPagoValor, formaPagoDescripcion, monto, 0, null, false);
            }else if(formaPagoValor == '4'){//Cuenta Corriente
                $('select#tipo_cuenta_corriente_id').val('');
                $('span#interes_cuenta_corriente').text('0.0');
                $('span#cuotas_cuenta_corriente').text('1');
                $('span#recargo_cuenta_corriente').text('0.00');
                $('input#importe_cuenta_corriente').val(monto);
                $('#modal-cuenta-corriente').modal();
            }else if(formaPagoValor == '1'){//Cheque
                $('input#numero_cheque').val('');
                $('input#importe_cheque').val(monto);
                $('input#firmante').val('');
                $('#modal-cheque-nuevo').modal();
            }else if(formaPagoValor == '2'){//Depósito Bancario
                $('input#importe_deposito_bancario').val(monto);
                $('#modal-deposito-nuevo').modal();
            }else if(formaPagoValor == '3'){//Cupon Tarjeta
                $('select#tarjeta_id').val('');
                $('select#plan_tarjeta_id').val('');
                $('span#interes_plan_tarjeta').text('0.0');
                $('span#cuotas_plan_tarjeta').text('1');
                $('span#recargo_plan_tarjeta').text('0.00');
                $('input#importe_cupon_tarjeta').val(monto);
                $('input#lote').val('');
                $('input#cupon').val('');
                $('#modal-cupon-tarjeta').modal();
            }else if(formaPagoValor == '6'){//Mercado Pago
                agregarDetalleCobro(formaPagoValor, formaPagoDescripcion, monto, 0, null, false);
            }
        });

        $("#guardar-cheque").click(function(){
            $('select#banco_id').parent().removeClass('has-error');
            $('input#numero_cheque').parent().removeClass('has-error');
            $('input#importe_cheque').parent().removeClass('has-error');
            $('input#firmante').parent().removeClass('has-error');

            if($('select#banco_id option:selected').val() == '' || $('select#banco_id option:selected').val() == null) {
                $('select#banco_id').parent().addClass('has-error');
                $('select#banco_id').focus();
            }else if($('input#numero_cheque').val() == ''){
                $('input#numero_cheque').parent().addClass('has-error');
                $('input#numero_cheque').focus();
            }else if($('input#importe_cheque').val() == ''){
                $('input#importe_cheque').parent().addClass('has-error');
                $('input#importe_cheque').focus();
            }else if($('input#firmante').val() == ''){
                $('input#firmante').parent().addClass('has-error');
                $('input#firmante').focus();
            }else{
                cheque = [
                    ['banco_id', $('select#banco_id option:selected').val()],
                    ['numero_cheque', $('input#numero_cheque').val()],
                    ['firmante', $('input#firmante').val()],
                    ['vencimiento', $('input#vencimiento').val()]
                ];

                formaPagoValor = $('#forma_cobro option:selected').val();
                formaPagoDescripcion = $('#forma_cobro option:selected').text()+' Nº: '+$('input#numero_cheque').val()+' banco '+$('select#banco_id option:selected').text();
                monto = $('input#importe_cheque').val();

                $('#modal-cheque-nuevo').modal('hide');
                $('input#numero_cheque').val('');
                $('input#importe_cheque').val('');
                $('input#firmante').val('');
                $('input#vencimiento').val('');

                agregarDetalleCobro(formaPagoValor, formaPagoDescripcion, monto, 0, cheque, true);
            }
        });

        $("#guardar-deposito-bancario").click(function(){
            $('select#cuenta_bancaria_id').parent().removeClass('has-error');
            $('input#importe_deposito_bancario').parent().removeClass('has-error');

            if($('select#cuenta_bancaria_id option:selected').val() == '') {
                $('select#cuenta_bancaria_id').parent().addClass('has-error');
                $('select#cuenta_bancaria_id').focus();
            }else if($('input#importe_deposito_bancario').val() == ''){
                $('input#importe_deposito_bancario').parent().addClass('has-error');
                $('input#importe_deposito_bancario').focus();
            }else{
                depositoBancario = [
                    ['cuenta_bancaria_id', $('select#cuenta_bancaria_id option:selected').val()],
                    ['importe_deposito_bancario', $('input#importe_deposito_bancario').val()]
                ];

                formaPagoValor = $('#forma_cobro option:selected').val();
                formaPagoDescripcion = 'Cobro a '+$('#forma_cobro option:selected').text()+' Nº:'+$('select#cuenta_bancaria_id option:selected').text();
                monto = $('input#importe_deposito_bancario').val();

                $('#modal-deposito-nuevo').modal('hide');
                $('input#importe_deposito_bancario').val('');

                agregarDetalleCobro(formaPagoValor, formaPagoDescripcion, monto, 0, depositoBancario, true);
            }
        });

        $("#guardar-cupon-tarjeta").click(function(){
            $('select#tarjeta_id').parent().removeClass('has-error');
            $('select#plan_tarjeta_id').parent().removeClass('has-error');
            $('input#importe_cupon_tarjeta').parent().removeClass('has-error');

            if($('select#tarjeta_id option:selected').val() == ''){
                $('select#tarjeta_id').parent().addClass('has-error');
                $('select#tarjeta_id').focus();
            }else if($('select#plan_tarjeta_id option:selected').val() == ''){
                $('select#plan_tarjeta_id').parent().addClass('has-error');
                $('select#plan_tarjeta_id').focus();
            }else if($('input#importe_cupon_tarjeta').val() == '' || $('input#importe_cupon_tarjeta').val() == 0){
                $('input#importe_cupon_tarjeta').parent().addClass('has-error');
                $('input#importe_cupon_tarjeta').focus();
            }else{
                cuponTarjeta = [
                    ['tarjeta_id', $('select#tarjeta_id option:selected').val()],
                    ['plan_tarjeta_id', $('select#plan_tarjeta_id option:selected').val()],
                    ['importe_cupon_tarjeta', $('input#importe_cupon_tarjeta').val()],
                    ['lote', $('input#lote').val()],
                    ['cupon', $('input#cupon').val()]
                ];

                formaPagoValor = $('#forma_cobro option:selected').val();
                formaPagoDescripcion = 'Cobro con tarjeta '+$('select#tarjeta_id option:selected').text()+' con plan '+$('select#plan_tarjeta_id option:selected').text();
                monto = $('input#importe_cupon_tarjeta').val();
                interes = $("span#recargo_plan_tarjeta").text();

                $('#modal-cupon-tarjeta').modal('hide');
                $('input#importe_cupon_tarjeta').val('');

                agregarDetalleCobro(formaPagoValor, formaPagoDescripcion, monto, interes, cuponTarjeta, true);
            }
        });

        $("#guardar-cuenta-corriente").click(function(){
            $('select#tipo_cuenta_corriente_id').parent().removeClass('has-error');
            $('input#importe_cuenta_corriente').parent().removeClass('has-error');

            if($('input#importe_cuenta_corriente').val() == '' || $('input#importe_cuenta_corriente').val() == 0){
                $('input#importe_cuenta_corriente').parent().addClass('has-error');
                $('input#importe_cuenta_corriente').focus();
            }else{
                cuentaCorriente = [
                    ['tipo_cuenta_corriente_id', $('select#tipo_cuenta_corriente_id option:selected').val()],
                    ['importe_cuenta_corriente', $('input#importe_cuenta_corriente').val()]
                ];

                var tipoCuentaCorieente = $('select#tipo_cuenta_corriente_id option:selected').text();
                if($('select#tipo_cuenta_corriente_id option:selected').val() == ''){
                    tipoCuentaCorieente = '';
                }

                formaPagoValor = $('#forma_cobro option:selected').val();
                formaPagoDescripcion = 'A Cuenta Corriente '+tipoCuentaCorieente;
                monto = $('input#importe_cuenta_corriente').val();
                interes = $("span#recargo_cuenta_corriente").text();

                $('#modal-cuenta-corriente').modal('hide');
                $('input#importe_cuenta_corriente').val('');

                agregarDetalleCobro(formaPagoValor, formaPagoDescripcion, monto, interes, cuentaCorriente, true);
            }
        });

        $(document).on('click', '.borrar-detalle-cobro', function () {
            inputNombre = this.attributes['name'].value;

            $('.borrar-detalle-cobro[name="' + inputNombre + '"]').parent().find('input.estado').val(0);
            $('.borrar-detalle-cobro[name="' + inputNombre + '"]').parent().parent().hide("slow");
            $('.borrar-detalle-cobro[name="' + inputNombre + '"]').parent().parent().find('td input.monto').val(0);
            $('.borrar-detalle-cobro[name="' + inputNombre + '"]').parent().parent().find('td input.interes').val(0);

            refrescarTotales();
        });

        $(document).on('change','.monto',function(e){
            refrescarTotales();
        });

     /*   $('select#forma_cobro').on('change', function () {
            actualizarDiferencia();
        });*/

         function calcularMontoCobro() {
             total = parseFloat($("input#total").val());
             totalPagos = 0;
             cobrosAgregados = $('input[name^="detalles_cobro"].monto');
             for (var i = 0; i < cobrosAgregados.length; i++) {
                 totalPagos += parseFloat(cobrosAgregados[i].value);
             }
             return (total - totalPagos).toFixed(2);
         }

        function agregarDetalleCobro(valor, descripcion, monto, interes, objeto, readonly) {
            var numero_orden = $('table#tabla-detalles-cobro tbody tr').length+ 1;

            objetoString = '';
            if(objeto != null){
                for (var i = 0; i < objeto.length; i++) {
                    objetoString += '<input type="hidden" name="detalles_cobro['+(numero_orden*-1)+']['+objeto[i][0]+']" value="'+objeto[i][1]+'">'
                }
            }

            stringReadonly = '';
            if(readonly){
                stringReadonly = 'readonly';
            }

            $("#tabla-detalles-cobro").append('<tr>' +
                '<td>' +
                    descripcion +
                    '<input type="hidden" class="id" name="detalles_cobro['+(numero_orden*-1)+'][id]" value="'+(numero_orden*-1)+'">' +
                '</td>' +
                '<td>' +
                    '<div class="input-group">' +
                        '<div class="left-icon">$</div>' +
                        '<input class="form-control green monto" name="detalles_cobro['+(numero_orden*-1)+'][monto]" type="number" step="0.01" value="'+ monto +'" '+ stringReadonly +'>' +
                    '</div>' +
                '</td>' +
                '<td>' +
                    '<div class="input-group">' +
                        '<div class="left-icon">$</div>' +
                        '<input class="form-control green interes" name="detalles_cobro['+(numero_orden*-1)+'][interes]" type="number" value="'+ interes +'" readonly>' +
                    '</div>' +
                '</td>' +
                '<td>' +
                    '<button type="button" class="btn btn-danger btn-circle borrar-detalle-cobro" name="detalles_cobro['+(numero_orden*-1)+'][eliminar]" title="Borrar Cobro">' +
                        '<i class="fa fa-trash-o" aria-hidden="true"></i>' +
                    '</button>' +
                    '<input type="hidden" name="detalles_cobro['+(numero_orden*-1)+'][estado]" class="estado" value="1">' +
                    '<input type="hidden" name="detalles_cobro['+(numero_orden*-1)+'][id]" value="'+(numero_orden*-1)+'">' +
                    '<input type="hidden" name="detalles_cobro['+(numero_orden*-1)+'][forma_cobro]" value="'+valor+'">' +
                    objetoString +
                '</td>' +
            '</tr>');

            refrescarTotales();
        }
    </script>
    @yield('scripts-dialog-tarjeta')
    @yield('scripts-dialog-cuenta-corriente')
@endsection