@extends('frontend.layouts.app')

@section ('title', trans('labels.frontend.cuentacorriente_titulo')." - ".trans('buttons.general.crud.show'))

@section('after-styles')
    {{ Html::style("css/frontend/plugins/datatables/responsive.dataTables.min.css") }}
@endsection

@section('content')
    <div class="row page-titles">
        <div class="col-6 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('labels.frontend.cuentacorriente_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item">{{ trans('labels.frontend.cuentacorriente_titulo') }}</li>
                <li class="breadcrumb-item">{{ trans('buttons.general.crud.show') }} {{ trans('labels.frontend.cuentacorriente') }}</li>
                <li class="breadcrumb-item active">{{ $cliente }}</li>
            </ol>
        </div>
        <div class="col-6">
            <div class="pull-right">
                <a href="{{ url('/ventas/recibo/crearReciboCliente/'.$cliente->id) }}" class="btn btn-themecolor" title="{{ trans('buttons.general.crud.create').' '.trans('labels.frontend.recibo')  }}">
                    <i class="fa fa-plus" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans('buttons.general.crud.create').' '.trans('labels.frontend.recibo') }}</span>
                </a>
                <a href="{{ url('/personas/cliente/') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans('buttons.general.crud.back') }}</span>
                </a>
            </div>
        </div>
    </div>

    @tips($_SERVER['REQUEST_URI'])

    <div class="card">
        <div class="card-body">
            <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
            @include('includes.partials.messages')
            <div class="table-responsive">
                <table id="cuentacorriente-table" class="table selectable table-striped table-hover table-sm form-material">
                    <thead>
                    <tr >
                        <th></th>
                        <th>Fecha</th>
                        <th>Tipo Comprobante</th>
                        <th>Comprabante</th>
                        <th>Tipo de Pago</th>
                        <th>Vencimiento</th>
                        <th>Debe</th>
                        <th>Haber</th>
                        <th>Saldo</th>
                        <th>{{ trans('labels.general.actions') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($cuentasCorrientes as $item)
                        <tr id="{{$item->id}}">
                            <td>&nbsp;&nbsp;</td>
                            <td>{{ $item->created_at->format('d/m/Y H:i') }}</td>
                            <td>{{ ($item->tipo_comprobante) ? $tipos_comprobantes[$item->tipo_comprobante] : 'Recibo' }}</td>
                            <td>{{ $item->detalle }}</td>
                            <td>{{ ($item->tipo_cuenta_corriente_id) ? $item->tipoCuentaCorriente : '' }}</td>
                            <td>{{ ($item->proximo_vencimiento) ? $item->proximo_vencimiento->format('d/m/Y') : '' }}</td>
                            <td><b class="text-danger">$ {{ $item->debe }}</b></td>
                            <td><b class="text-success">$ {{ $item->haber }}</b></td>
                            <td>
                                <b class="@if($item->debe > $item->haber) text-danger @else text-success @endif">
                                    @if($item->debe >= $item->haber)
                                        $ {{ number_format(($item->debe - $item->haber), 2) }}
                                    @else
                                        $ {{ number_format(($item->haber - $item->debe), 2) }}
                                    @endif
                                </b>
                            </td>
                            <td>
                                <a href="{{ url($item->url).'/'.$item->entidad_id }}" data-toggle="tooltip" data-original-title="{{ trans('buttons.general.crud.show') }} Comprobante">
                                    <i class="fa fa-eye text-success m-r-10"></i>
                                </a>
                                <a href="javascript:void(0)" class="delete" data-id="{{$item->id}}" data-toggle="tooltip" data-original-title="{{trans("buttons.general.crud.delete")}} Comprobante">
                                    <i class="fa fa-close text-danger"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    @if($cuentasCorrientes->sum('debe') - $cuentasCorrientes->sum('haber') >= 0)
        <div class="card card-danger card-inverse">
            <div class="box text-center">
                <h1 class="font-light text-white">$ {{ number_format(($cuentasCorrientes->sum('debe') - $cuentasCorrientes->sum('haber')), 2) }}</h1>
                <h6 class="text-white">Saldo</h6>
            </div>
        </div>
    @else
        <div class="card card-success card-inverse">
            <div class="box text-center">
                <h1 class="font-light text-white">$ {{ number_format(($cuentasCorrientes->sum('haber') - $cuentasCorrientes->sum('debe')), 2) }}</h1>
                <h6 class="text-white">Saldo A FAVOR</h6>
            </div>
        </div>
    @endif

@endsection

@section('after-scripts')
    {{ Html::script("js/frontend/plugins/datatables/jquery.dataTables.min.js") }}
    {{ Html::script("js/frontend/plugins/datatables/dataTables.responsive.min.js") }}
    {{ Html::script("js/frontend/plugins/datatables/dataTables.select.min.js") }}

    <script>

      $(document).ready(function() {
            $('#cuentacorriente-table').DataTable({
                "aaSorting": [1,'asc'],
                "info":     false,
                "searching":   false,
                "info":     false,
                "searching":   false, 
                "ordering":false,
                responsive: {
                    details: {
                        type: 'column',
                        target: 'tr'
                    }
                },
                "columnDefs": [{
                    targets: 0,
                    className: 'control',
                    orderable: false,
                    searchable: false,
                    defaultContent: '&nbsp;&nbsp;'
                },
                { responsivePriority: 1, targets: -1 },
                { responsivePriority: 1, targets: -2 },
                { responsivePriority: 1, targets: 1 }],
            });
        } );

        $("body").on("click", ".delete", function () {
            var id = $(this).attr("data-id");
            swal({
                title: "{{trans('buttons.general.crud.delete').' Comprobante'}}",
                text: "¿Realmente desea eliminar este registro?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#ff8726',
                confirmButtonText: "Eliminar"
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        type: 'POST',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: '/ventas/cuenta-corriente/'+id,
                        data: {"_method" : 'DELETE'},
                        success: function (msg) {
                            $("#" + id).hide(1);
                            swal("Eliminado!", "El registro ha sido correctamente eliminado.", "success");
                        }
                    });
                }
            });
        });
    </script>
@endsection
