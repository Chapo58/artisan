@extends('frontend.layouts.app')

@section ('title', trans('labels.frontend.notadebito_titulo')." - ".trans('buttons.general.crud.show'))

@section('content')
    <div class="row page-titles">
        <div class="col-6 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('labels.frontend.notadebito_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item"><a href="{{url('/ventas/nota-debito')}}">{{ trans('labels.frontend.notadebito_titulo') }}</a></li>
                <li class="breadcrumb-item">{{ trans('buttons.general.crud.show') }} {{ trans('labels.frontend.notadebito') }}</li>
                <li class="breadcrumb-item active">{{ $notadebito }}</li>
            </ol>
        </div>
        <div class="col-6">
            <div class="pull-right">
                <a href="{{ url('/ventas/nota-debito') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans('buttons.general.crud.back') }}</span>
                </a>
                @if($notadebito->puntoVenta->tipo->nombre == "Comandera" || $notadebito->puntoVenta->tipo->nombre == "Impresora Manual")
                    <a href="{{ url('/ventas/nota-debito/imprimir/' . $notadebito->id) }}" target="_blank" title="Imprimir" class="btn btn-primary">
                        <i class="fa fa-print" aria-hidden="true"></i>
                        <span class="hidden-xs-down">Imprimir</span>
                    </a>
                    <a href="javascript:void(0)" title="{{trans("buttons.general.crud.delete")." ".trans("labels.frontend.notadebito")}}" class="btn btn-danger delete">
                        <i class="fa fa-trash" aria-hidden="true"></i>
                        <span class="hidden-xs-down">{{ trans("buttons.general.crud.delete") }}</span>
                    </a>
                @endif
                @if($notadebito->estado_facturacion == 2)
                    <a href="{{ url('/ventas/facturacion-fiscal/facturarNotaDeDebito/' . $notadebito->id) }}" target="_blank" title="Facturar">
                        <button class="btn btn-info">
                            <i class="fa fa-print" aria-hidden="true"></i>
                            <span class="hidden-xs-down">Volver a Facturar</span>
                        </button>
                    </a>
                @endif
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            <div class="card-actions">
                <a class="mytooltip" data-action="collapse"><i class="ti-minus"></i><span class="tooltip-content3">Minimizar Ventana</span></a>
                <a class="btn-minimize mytooltip" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
                <a class="btn-close mytooltip" data-action="close"><i class="ti-close"></i><span class="tooltip-content3">Cerrar Ventana</span></a>
            </div>
            <h4 class="card-title m-b-0">
                Información de la Nota de Debito
                @if($notadebito->estado_facturacion == 2)
                    <strong style="color:red">ERROR DE FACTURACION</strong>
                @endif
            </h4>
        </div>
        <div class="card-body collapse show b-t">
            <div class="table-responsive">
                <table class="table table-borderless">
                    <tbody>
                    <tr>
                        <th>Cliente</th>
                        <td>
                            @if($notadebito->cliente)
                                <a href="{{url('/personas/cliente/'.$notadebito->cliente->id)}}">{{ $notadebito->cliente }}</a>
                            @else
                                Cliente Eliminado
                            @endif
                        </td>
                        <th>Tipo Comprobante</th><td>{{ $tipos_comprobantes[$notadebito->tipo_comprobante_id] }}</td>
                        <th>Punto Venta</th><td>{{ $notadebito->puntoVenta }}</td>
                        <th>Número</th><td>{{ $notadebito->numero }}</td>
                    </tr>
                    <tr>
                        <th>Fecha</th><td>{{ $notadebito->fecha->format('d/m/Y H:i') }}</td>
                        <th>Lista</th>
                        <td>
                            <a href="{{url('/ventas/lista-precios/'.$notadebito->listaPrecios->id)}}">{{ $notadebito->listaPrecios }}</a>
                        </td>
                        <th>Moneda</th><td>{{ $notadebito->moneda }}</td>
                        <th>Cotizacíon</th><td><b class="green">$ {{ $notadebito->cotizacion }}</b></td>
                    </tr>
                    </tbody>
                </table>
                @if($notadebito->descripcion)
                    <strong>Comentario: <i>{{ $notadebito->descripcion }}</i></strong>
                @endif
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            <div class="card-actions">
                <a class="mytooltip" data-action="collapse"><i class="ti-minus"></i><span class="tooltip-content3">Minimizar Ventana</span></a>
                <a class="btn-minimize mytooltip" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
                <a class="btn-close mytooltip" data-action="close"><i class="ti-close"></i><span class="tooltip-content3">Cerrar Ventana</span></a>
            </div>
            <h4 class="card-title m-b-0">
                Detalles de la Nota de Debito
            </h4>
        </div>
        <div class="card-body collapse show b-t">
            <div class="table-responsive">
                <table class="table color-table muted-table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>
                            Nº
                        </th>
                        <th>
                            Cantidad
                        </th>
                        <th>
                            Artículo
                        </th>
                        <th>
                            Pre/Uni. Neto
                        </th>
                        <th>
                            Neto
                        </th>
                        <th>
                            IVA
                        </th>
                        <th>
                            Impuesto Interno
                        </th>
                        <th>
                            Bruto
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!$notadebito->detalles->isEmpty())
                        @foreach($notadebito->detalles as $detalle)
                            <tr>
                                <td>
                                    {{ $loop->index + 1 }}
                                </td>
                                <td>
                                    {{ number_format($detalle->cantidad , 0) }}
                                </td>
                                <td>
                                    @if(isset($detalle->articulo))
                                        <a href="{{url('/articulos/articulo/'.$detalle->articulo->id)}}">
                                            ({{ $detalle->articulo->codigo }}) {{ $detalle->articulo->nombre }}
                                        </a>
                                    @else
                                        Artículo Eliminado
                                    @endif
                                </td>
                                <td>
                                    <b class="text-success">{{ $notadebito->moneda->signo }} {{ number_format($detalle->precio_neto, 2) }}</b>
                                </td>
                                <td>
                                    <b class="text-success">{{ $notadebito->moneda->signo }} {{ number_format($detalle->cantidad*$detalle->precio_neto , 2) }}</b>
                                </td>
                                <td>
                                    <b class="text-success"> {{ number_format($porcentajes_iva[$detalle->porcentaje_iva], 2) }} %</b>
                                </td>
                                <td>
                                    <b class="text-success">{{ $notadebito->moneda->signo }} {{ number_format($detalle->cantidad*$detalle->importe_impuesto_interno, 2) }}</b>
                                </td>
                                <td>
                                    <b class="text-success">{{ $notadebito->moneda->signo }} {{ number_format(($detalle->cantidad * $detalle->precio_neto) * (($porcentajes_iva[$detalle->porcentaje_iva]/100) + 1) + ($detalle->cantidad*$detalle->importe_impuesto_interno), 2) }}</b>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
                <table class="table table-borderless">
                    <tbody>
                    <tr>
                        <th class="text-right" colspan="3">Importe Neto</th><td><b class="text-success">{{ $notadebito->moneda->signo }} {{ number_format($notadebito->importe_neto, 2) }}</b></td>
                    </tr>
                    <tr>
                        <th class="text-right" colspan="3">Iva ($)</th><td><b class="text-success">{{ $notadebito->moneda->signo }} {{ number_format($notadebito->importe_iva, 2) }}</b></td>
                    </tr>
                    <tr>
                        <th class="text-right">Impuesto/Descuento (%)</th><td><b class="text-success">{{ number_format($notadebito->porcentaje_descuento, 2) }} %</b></td>
                        <th class="text-right">Impuesto/Descuento ($)</th><td><b class="text-success">{{ $notadebito->moneda->signo }} {{ number_format($notadebito->importe_descuento, 2) }}</b></td>
                    </tr>
                    <tr>
                        <th class="text-right" colspan="3">Impuestos Internos</th><td><b class="text-success">{{ $notadebito->moneda->signo }} {{ number_format($notadebito->importe_impuesto_interno, 2) }}</b></td>
                    </tr>
                    <tr>
                        <th class="text-right" colspan="3">Total</th><td><b class="text-success">{{ $notadebito->moneda->signo }} {{ number_format($notadebito->total, 2) }}</b></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            <div class="card-actions">
                <a class="mytooltip" data-action="collapse"><i class="ti-minus"></i><span class="tooltip-content3">Minimizar Ventana</span></a>
                <a class="btn-minimize mytooltip" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
                <a class="btn-close mytooltip" data-action="close"><i class="ti-close"></i><span class="tooltip-content3">Cerrar Ventana</span></a>
            </div>
            <h4 class="card-title m-b-0">
                Cobro
            </h4>
        </div>
        <div class="card-body collapse show b-t">
            <div class="table-responsive">
                <table class="table color-table muted-table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>
                            Nº
                        </th>
                        <th>
                            Descripción
                        </th>
                        <th>
                            Monto
                        </th>
                        <th>
                            Interes
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!$notadebito->detallesCobros->isEmpty())
                        @foreach($notadebito->detallesCobros as $detalleCobro)
                            <tr>
                                <td>
                                    {{ $loop->index + 1 }}
                                </td>
                                <td>
                                    {{ $detalleCobro->descripcion }}
                                </td>
                                <td>
                                    <b class="text-success">{{ $notadebito->moneda->signo }} {{ number_format($detalleCobro->monto , 2) }}</b>
                                </td>
                                <td>
                                    <b class="text-success">{{ $notadebito->moneda->signo }} {{ number_format($detalleCobro->interes, 2) }}</b>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                    <tfoot>
                        <tr>
                            <td></td>
                            <td></td>
                            <td>
                                Total Cobro
                            </td>
                            <td>
                                <b class="text-success">{{ $notadebito->moneda->signo }} {{ number_format($notadebito->total_cobro, 2) }}</b>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>

@endsection

@section('after-scripts')

<script>
    $("body").on("click", ".delete", function () {
        swal({
            title: "{{trans('buttons.general.crud.delete').' '.trans('labels.frontend.notadebito')}}",
            text: "¿Realmente desea eliminar este registro?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#ff8726',
            confirmButtonText: "Eliminar"
        }).then((result) => {
            if (result.value) {
            $.ajax({
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ url('/ventas/nota-debito/'.$notadebito->id) }}',
                data: {"_method" : 'DELETE'},
                success: function (msg) {
                    swal("Eliminado!", "El registro ha sido correctamente eliminado.", "success").then((result) => { window.location.href = '{{url('/ventas/nota-debito')}}'; });
                }
            });
        }
    });
    });
</script>

@endsection