@extends('frontend.layouts.app')

@section ('title', trans('labels.frontend.notadebito_titulo'))

@section('content')
    <div class="main-content">
        <div class="main-content-inner">

            <div class="page-header">
                <h1>

                </h1>
            </div>

        </div>
    </div>
@endsection

@section('after-scripts')
    {{ Html::script("js/frontend/plugins/fiscal/ifu.js") }}
    {{ Html::script("js/frontend/custom.js") }}

<script>

    host = "{{ $notaDebito->puntoVenta->host_fiscal }}";
    modelo = {{ $notaDebito->puntoVenta->modelo_impresora_fiscal }};
    puerto = {{ $notaDebito->puntoVenta->puerto_fiscal }};
    baudios = {{ $notaDebito->puntoVenta->baudios_fiscal }};

    @if($notaDebito->cliente->condicionIva == "Responsable Inscripto")
        condicionIva = riResponsableInscripto;
        tipoNota = tcNota_Debito_A;
        tipoFactura = tcFactura_A;
    @elseif($notaDebito->cliente->condicionIva == "Monotributista")
        condicionIva = riMonotributo;
        tipoNota = tcNota_Debito_B;
        tipoFactura = tcFactura_B;
    @elseif($notaDebito->cliente->condicionIva == "Exento")
        condicionIva = riExento;
        tipoNota = tcNota_Debito_B;
        tipoFactura = tcFactura_B;
    @else
        condicionIva = riConsumidorFinal;
        tipoNota = tcNota_Debito_B;
        tipoFactura = tcFactura_B;
    @endif

    @if($empresa->condicionIva != "Responsable Inscripto")
        tipoNota = tcNota_Debito_C;
        tipoFactura = tcFactura_C;
    @endif

    function ultimoNumeroComprobante(){
        var driver = new Driver();
        driver.modelo = modelo;
        driver.puerto = puerto;
        driver.baudios = baudios;
        try {
            driver.iniciarTrabajo();
            driver.ultimoComprobante(tipoNota);
            driver.finalizarTrabajo();
            numeroComprobante = parseInt(driver.response.ultimoNumero) + 1;
        } catch (e){
            numeroComprobante = 0;
            cambiarEstadoFacturacion("ERROR");
            swal("Error", e, "error");
        }
    }

    function imprimir(){
        var driver = new Driver();
        driver.host = host //Cambiar por el ip de la maquina de la fiscal si es remota
        driver.modelo = modelo;
        driver.puerto = puerto;
        driver.baudios = baudios;

        @if(strlen($notaDebito->cliente->dni) > 8)
            tipoDocumento = tdCUIT;
        @else
            tipoDocumento = tdDNI;
        @endif

        try {
            driver.iniciarTrabajo();
            driver.cancelarComprobante();

            @if(!($notaDebito->cliente->condicionIva == "Consumidor Final" && $notaDebito->total <= 1000))
                driver.datosCliente("{{ $notaDebito->cliente->razon_social }}", tipoDocumento, "{{ $notaDebito->cliente->dni }}", condicionIva, "{{ (isset($notaDebito->cliente->domicilio->direccion)) ? $notaDebito->cliente->domicilio->direccion : ' ' }}");
            @endif

            @if($notaDebito->venta)
                //   Si es nota de credito enviar la factura de referencia
                driver.documentoDeReferencia2g(tipoFactura, "{{ $notaDebito->venta->puntoVenta.'-'.$notaDebito->venta->numero }}");
            @endif

            driver.abrirComprobante(tipoNota);

            @foreach($notaDebito->detalles as $detalle)
            <?php
            if($notaDebito->tipo_comprobante_id != 2){ // No es Nota de Debito A
                $detalle->precio_neto = $detalle->precio_neto + $detalle->precio_iva;
                if($detalle->importe_impuesto_interno){
                    $detalle->precio_neto = $detalle->precio_neto + $detalle->importe_impuesto_interno;
                }
            }
            ?>
             driver.imprimirItem2g("{{ $detalle->articulo->nombre }}", {{ number_format($detalle->cantidad , 0) }}, {{ number_format($detalle->precio_neto, 2, '.', '') }}, {{$porcentajes_iva[$detalle->porcentaje_iva]}}, {{($detalle->importe_impuesto_interno) ? $detalle->importe_impuesto_interno : 0}}, Gravado, tiFijo, 1, "{{ $detalle->articulo->codigo_barra }}", "{{ $detalle->articulo->codigo }}", Unidad);
            @endforeach

            @if($notaDebito->importe_descuento || $notaDebito->interesesCobro())
                driver.imprimirDescuentoGeneral("Impuesto/Descuento (%)", {{ number_format(($notaDebito->importe_descuento + $notaDebito->interesesCobro()) * -1, 2) }});
            @endif

            @foreach($notaDebito->detallesCobros as $detalleCobro)
             cuotas = 0;
             @if($detalleCobro->forma_cobro == 0)
                 formaDePago = Efectivo;
             @elseif($detalleCobro->forma_cobro == 1)
                 formaDePago = Cheque;
             @elseif($detalleCobro->forma_cobro == 2)
                 formaDePago = Deposito;
             @elseif($detalleCobro->forma_cobro == 3)
                 formaDePago = TarjetaDeCredito;
                 cuotas = 1;
             @else
                 formaDePago = CuentaCorriente;
             @endif
             driver.imprimirPago2g("{{ $detalleCobro->descripcion }}", {{ number_format($detalleCobro->monto + $detalleCobro->interes, 2, '.', '') }}, "", formaDePago, cuotas, "", "");
             // TODO Hacer el calculo de cuotas de tarjetas de credito
            @endforeach
            driver.cerrarComprobante();
            driver.finalizarTrabajo();

            cambiarEstadoFacturacion("FACTURADO");
            swal({
                title: "Operación realizada con éxito!",
                timer: 2000,
                type: "success",
                showConfirmButton: false
            }).then(
                window.location.href = "{{ url('/ventas/nota-debito') }}"
            )
        } catch (e){
            cambiarEstadoFacturacion("ERROR");
            swal("Error", e, "error");
        }
    }

    function cambiarEstadoFacturacion(estado) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            }
        });
        $.ajax({
            type: "POST",
            url: '{{ url("/ventas/nota-debito/setEstadoFacturacion") }}',
            data: { nota_id: {{ $notaDebito->id }}, estado: estado, numeroComprobante: numeroComprobante }
        });
    }

    $(document).ready(function() {
        swal({
            title: 'Realizando Nota de Debito',
            text: 'Imprimiendo Nota de Debito... Por favor aguarde...',
            timer: 10000,
            onOpen: () => {
                swal.showLoading()
            }
        });
        ultimoNumeroComprobante();
        imprimir();
    } );

</script>

@endsection
