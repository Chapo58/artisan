<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Nota de Debito</title>
        <style>
            * {
                font-size: 12px;
                font-family: 'Times New Roman';
            }

            td,
            th,
            tr,
            table {
                border-top: 1px solid black;
                border-collapse: collapse;
                width: 100%;
            }

            td.producto,
            th.producto {
                width: 50%;
                max-width: 50%;
            }

            td.cantidad,
            th.cantidad {
                width: 25%;
                max-width: 25%;
                word-break: break-all;
            }

            td.precio,
            th.precio {
                width: 25%;
                max-width: 25%;
                word-break: break-all;
            }

            .centrado {
                text-align: center;
                align-content: center;
            }

            .ticket {
                width: {{$notaDebito->puntoVenta->anchura_papel_comandera}}cm;
                max-width: {{$notaDebito->puntoVenta->anchura_papel_comandera}}cm;
            }

            img {
                max-width: inherit;
                width: inherit;
            }

            @page {
                margin: 0;
            }
        </style>

        {{ Html::style("css/frontend-dashboard/bootstrap.min.css") }}

    </head>
    <body>
        @include('includes.partials.logged-in-as')

        <div class="ticket">
            {{--<img class="img-responsive" src="{{ url("img/frontend/logos/logo_nasa_mini.png") }}" alt="Logotipo" />--}}
            <p class="centrado">Comprobante N° {{$notaDebito->puntoVenta.'-'.$notaDebito->numero }}
                {{--<br>Moreno 173, Cañada de Gomez--}}
                <br>Emision: {{date('d/m/Y')}}</p>
            <table>
                <thead>
                <tr>
                    <th class="cantidad">CANT</th>
                    <th class="producto">PRODUCTO</th>
                    <th class="precio">$</th>
                </tr>
                </thead>
                <tbody>
                @if(!$notaDebito->detalles->isEmpty())
                    @foreach($notaDebito->detalles as $detalle)
                        <tr>
                            <td class="cantidad">{{ number_format($detalle->cantidad , 2) }}</td>
                            <td class="producto">{{ $detalle->articulo->nombre }}</td>
                            <td class="precio">$ {{ number_format( ($detalle->cantidad * $detalle->precio_neto) * ( ($porcentajes_iva[$detalle->porcentaje_iva]/100) + 1), 2) }}</td>
                        </tr>
                    @endforeach
                @endif
                @if($notaDebito->impuesto != 0)
                    <tr>
                        <td colspan="2">Impuesto/Descuento (%)</td>
                        <td class="precio">$ {{ number_format(($notaDebito->importe_descuento), 2) }}</td>
                    </tr>
                @endif
                </tbody>
                <tfoot>
                <tr>
                    <th class="cantidad"></th>
                    <th class="producto"><center>TOTAL</center></th>
                    <th class="precio">$ {{ number_format($notaDebito->total, 2) }}</th>
                </tr>
                </tfoot>
            </table><br>
            <p class="centrado">¡GRACIAS POR SU COMPRA!</p>
        </div>

        <script>
            window.print();
            setTimeout(
                function()
                {
                    window.location.href="{{ url('/ventas/nota-debito') }}";
                }, 1000);
        </script>
    </body>

</html>
