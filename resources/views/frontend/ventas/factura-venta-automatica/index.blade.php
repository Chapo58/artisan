@extends ('frontend.layouts.app')

@section ('title', trans('labels.frontend.facturaventaautomatica_titulo')." - ".trans('navs.frontend.user.administration'))

@section('after-styles')
    {{ Html::style("css/frontend/plugins/datatables/responsive.dataTables.min.css") }}
@endsection

@section('content')
    <div class="row page-titles">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('labels.frontend.facturaventaautomatica_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item active">{{ trans('labels.frontend.facturaventaautomatica_titulo') }}</li>
            </ol>
        </div>
        <div class="col-md-6">
            <div class="pull-right">
                <a href="{{ url('/ventas/factura-venta-automatica/create') }}" class="btn btn-themecolor btn-lg" title="{{ trans('buttons.general.crud.create').' '.trans('labels.frontend.facturaventaautomatica')  }}">
                    <i class="fa fa-plus" aria-hidden="true"></i> {{ trans('buttons.general.crud.create').' '.trans('labels.frontend.facturaventaautomatica') }}
                </a>
            </div>
        </div>
    </div>

    @tips($_SERVER['REQUEST_URI'])

    <div class="card">
        <div class="card-body">
            <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
            @include('includes.partials.messages')
            <div class="table-responsive">
                <table id="facturaventaautomatica-table" class="table selectable table-striped table-hover table-sm">
                    <thead>
                    <tr>
                        <th></th>
                        <th>Cliente</th>
                        <th>Desde</th>
                        <th>Hasta</th>
                        <th>Día Fac.</th>
                        <th>Total</th>
                        <th>{{ trans('labels.general.actions') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($facturaventaautomatica as $item)
                        @if($item->venta)
                            <tr id="{{$item->id}}">
                                <td>&nbsp;&nbsp;</td>
                                <td>{{ $item->venta->cliente }}</td>
                                <td>{{ $item->fecha_inicio->format('d/m/Y') }}</td>
                                <td>{{ $item->fecha_fin->format('d/m/Y') }}</td>
                                <td>{{ $item->dia_facturacion }}</td>
                                <td>{{ $item->venta->moneda->signo }} {{ $item->venta->total }}</td>
                                <td>
                                    <a href="{{ url('/ventas/factura-venta-automatica/' . $item->id) }}" data-toggle="tooltip" data-original-title="{{ trans('buttons.general.crud.show') }} {{ trans('labels.frontend.facturaventaautomatica') }}">
                                        <i class="fa fa-eye text-success m-r-10"></i>
                                    </a>
                                    <a href="{{ url('/ventas/factura-venta-automatica/' . $item->id . '/edit') }}" data-toggle="tooltip" data-original-title="{{ trans('buttons.general.crud.edit') }} {{ trans('labels.frontend.facturaventaautomatica') }}">
                                        <i class="fa fa-pencil text-info m-r-10"></i>
                                    </a>
                                </td>
                            </tr>
                        @endif
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection

@section('after-scripts')
    {{ Html::script("js/frontend/plugins/datatables/jquery.dataTables.min.js") }}
    {{ Html::script("js/frontend/plugins/datatables/dataTables.responsive.min.js") }}
    {{ Html::script("js/frontend/plugins/datatables/dataTables.select.min.js") }}

<script>
    $(document).ready(function() {
        $('#facturaventaautomatica-table').DataTable({
            "aaSorting": [1,'asc'],
            responsive: {
                details: {
                    type: 'column',
                    target: 'tr'
                }
            },
            "columnDefs": [{
                targets: 0,
                className: 'control',
                orderable: false,
                searchable: false,
                defaultContent: '&nbsp;&nbsp;'
            },
                { responsivePriority: 1, targets: -1 }]
        });
    } );
</script>

@endsection