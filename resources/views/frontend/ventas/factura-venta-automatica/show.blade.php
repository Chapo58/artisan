@extends('frontend.layouts.app')

@section ('title', trans('labels.frontend.facturaventaautomatica_titulo')." - ".trans('buttons.general.crud.show'))

@section('content')
    <div class="row page-titles">
        <div class="col-md-8 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('labels.frontend.facturaventaautomatica_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item"><a href="{{url('/ventas/factura-venta-automatica')}}">{{ trans('labels.frontend.facturaventaautomatica_titulo') }}</a></li>
                <li class="breadcrumb-item">{{ trans('buttons.general.crud.show') }} {{ trans('labels.frontend.facturaventaautomatica') }}</li>
                <li class="breadcrumb-item active">{{ $facturaVentaAutomatica }}</li>
            </ol>
        </div>
        <div class="col-md-4">
            <div class="pull-right">
                <a href="{{ url('/ventas/factura-venta-automatica') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i> {{ trans('buttons.general.crud.back') }}
                </a>
                <a href="{{ url('/ventas/factura-venta-automatica/' . $facturaVentaAutomatica->id . '/edit') }}" title="{{ trans('buttons.general.crud.edit') }}" class="btn btn-info">
                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i> {{ trans('buttons.general.crud.edit') }}
                </a>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            <div class="card-actions">
                <a class="" data-action="collapse"><i class="ti-minus"></i></a>
                <a class="btn-minimize" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
                <a class="btn-close" data-action="close"><i class="ti-close"></i></a>
            </div>
            <h4 class="card-title m-b-0">
                Información de la Facturación
            </h4>
        </div>
        <div class="card-body collapse show b-t">
            <div class="table-responsive">
                <table class="table table-borderless">
                    <tbody>
                        <tr>
                            <th>Cliente</th><td>{{ $facturaVentaAutomatica->venta->cliente }}</td>
                            <th>Tipo Comprobante</th><td>{{ $facturaVentaAutomatica->venta->tipoComprobante() }}</td>
                            <th>Punto Venta</th><td>{{ $facturaVentaAutomatica->venta->puntoVenta }}</td>
                        </tr>
                        <tr>
                            <th>Día Facturación</th><td>día {{ $facturaVentaAutomatica->dia_facturacion }} del mes</td>
                            <th>Fecha Inicio</th><td>{{ $facturaVentaAutomatica->fecha_inicio->format('d/m/Y') }}</td>
                            <th>Fecha Hasta</th><td>{{ $facturaVentaAutomatica->fecha_fin->format('d/m/Y') }}</td>
                        </tr>
                        <tr>
                            <th>Lista</th><td>{{ $facturaVentaAutomatica->venta->listaPrecios }}</td>
                            <th>Moneda</th><td>{{ $facturaVentaAutomatica->venta->moneda }}</td>
                            <th>Cotizacíon</th><td><b class="green">$ {{ $facturaVentaAutomatica->venta->cotizacion }}</b></td>
                        </tr>
                    </tbody>
                </table>
                @if($facturaVentaAutomatica->venta->descripcion)
                    <strong>Comentario: <i>{{ $facturaVentaAutomatica->venta->descripcion }}</i></strong>
                @endif
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            <div class="card-actions">
                <a class="" data-action="collapse"><i class="ti-minus"></i></a>
                <a class="btn-minimize" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
                <a class="btn-close" data-action="close"><i class="ti-close"></i></a>
            </div>
            <h4 class="card-title m-b-0">
                Detalles de la Facturación
            </h4>
        </div>
        <div class="card-body collapse show b-t">
            <div class="table-responsive">
                <table class="table color-table muted-table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>
                            Nº
                        </th>
                        <th>
                            Cantidad
                        </th>
                        <th>
                            Artículo
                        </th>
                        <th>
                            Pre/Uni. Neto
                        </th>
                        <th>
                            Neto
                        </th>
                        <th>
                            IVA
                        </th>
                        <th>
                            Impuesto Interno
                        </th>
                        <th>
                            Bruto
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                        @if(!$facturaVentaAutomatica->venta->detalles->isEmpty())
                            @foreach($facturaVentaAutomatica->venta->detalles as $detalle)
                                <tr>
                                    <td>
                                        {{ $loop->index + 1 }}
                                    </td>
                                    <td>
                                        {{ number_format($detalle->cantidad , 0) }}
                                    </td>
                                    <td>
                                        @if(isset($detalle->articulo))
                                            ({{ $detalle->articulo->codigo }}) {{ $detalle->articulo->nombre }}
                                        @else
                                            Artículo Eliminado
                                        @endif
                                    </td>
                                    <td>
                                        <b class="text-success">{{ $facturaVentaAutomatica->venta->moneda->signo }} {{ number_format($detalle->precio, 2) }}</b>
                                    </td>
                                    <td>
                                        <b class="text-success">{{ $facturaVentaAutomatica->venta->moneda->signo }} {{ number_format($detalle->cantidad*$detalle->precio , 2) }}</b>
                                    </td>
                                    <td>
                                        <b class="text-success"> {{ number_format($detalle->porcentajeIva(), 2) }} %</b>
                                    </td>
                                    <td>
                                        <b class="text-success">{{ $facturaVentaAutomatica->venta->moneda->signo }} {{ number_format($detalle->cantidad*$detalle->impuesto_interno, 2) }}</b>
                                    </td>
                                    <td>
                                        <b class="text-success">{{ $facturaVentaAutomatica->venta->moneda->signo }} {{ number_format(($detalle->cantidad * $detalle->precio) * (($detalle->porcentajeIva()/100) + 1) + ($detalle->cantidad*$detalle->impuesto_interno), 2) }}</b>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
                <table class="table table-borderless">
                    <tbody>
                        <tr>
                            <th class="text-right" colspan="3">Importe Neto</th><td><b class="text-success">{{ $facturaVentaAutomatica->venta->moneda->signo }} {{ number_format($facturaVentaAutomatica->venta->importe_neto, 2) }}</b></td>
                        </tr>
                        <tr>
                            <th class="text-right" colspan="3">Iva ($)</th><td><b class="text-success">{{ $facturaVentaAutomatica->venta->moneda->signo }} {{ number_format($facturaVentaAutomatica->venta->importe_iva, 2) }}</b></td>
                        </tr>
                        <tr>
                            <th class="text-right">Impuesto/Descuento (%)</th><td><b class="text-success">{{ number_format($facturaVentaAutomatica->venta->impuesto*100/($facturaVentaAutomatica->venta->total - $facturaVentaAutomatica->venta->impuesto), 2) }} %</b></td>
                            <th class="text-right">Impuesto/Descuento ($)</th><td><b class="text-success">{{ $facturaVentaAutomatica->venta->moneda->signo }} {{ number_format($facturaVentaAutomatica->venta->impuesto, 2) }}</b></td>
                        </tr>
                        <tr>
                            <th class="text-right" colspan="3">Impuestos Internos</th><td><b class="text-success">{{ $facturaVentaAutomatica->venta->moneda->signo }} {{ number_format($facturaVentaAutomatica->venta->importe_impuesto_interno, 2) }}</b></td>
                        </tr>
                        <tr>
                            <th class="text-right" colspan="3">Total</th><td><b class="text-success">{{ $facturaVentaAutomatica->venta->moneda->signo }} {{ number_format($facturaVentaAutomatica->venta->total, 2) }}</b></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            <div class="card-actions">
                <a class="" data-action="collapse"><i class="ti-minus"></i></a>
                <a class="btn-minimize" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
                <a class="btn-close" data-action="close"><i class="ti-close"></i></a>
            </div>
            <h4 class="card-title m-b-0">
                Vencimientos (Intervalos)
            </h4>
        </div>
        <div class="card-body collapse show b-t">
            <div class="table-responsive">
                <table class="table color-table muted-table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>
                            Nº Intervalo
                        </th>
                        <th>
                            Desde
                        </th>
                        <th>
                            Hasta
                        </th>
                        <th>
                            Recargo
                        </th>
                        <th>
                            Monto Recargo
                        </th>
                        <th>
                            Total A Pagar
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!$facturaVentaAutomatica->intervalos->isEmpty())
                        @foreach($facturaVentaAutomatica->intervalos->sortBy('desde') as $intervalo)
                            <tr>
                                <td>
                                    {{ $loop->index + 1 }}
                                </td>
                                <td>
                                    Desde el día {{ $intervalo->desde }}
                                </td>
                                <td>
                                    Hasta el día {{ $intervalo->hasta }}
                                </td>
                                <td>
                                    <b class="text-success">% {{ number_format($intervalo->porcentaje_recargo, 2) }}</b>
                                </td>
                                <td>
                                    <b class="text-success">{{ $facturaVentaAutomatica->venta->moneda->signo }} {{ number_format($intervalo->montoRecargo(), 2) }}</b>
                                </td>
                                <td>
                                    <b class="text-success">{{ $facturaVentaAutomatica->venta->moneda->signo }} {{ $facturaVentaAutomatica->totalIntervalo($intervalo->id) }}</b>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection