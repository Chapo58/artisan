<div class="card">
    <div class="card-header">
        <div class="card-actions">
            <a class="" data-action="collapse"><i class="ti-minus"></i></a>
            <a class="btn-minimize" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
        </div>
        <h4 class="card-title m-b-0">
            Vencimientos (Intervalos)
        </h4>
    </div>
    <div class="card-body collapse show b-t">
        <div class="row form-group">
            <div class="col-md-4">
                {!! Form::label('dias_intervalo', 'Intervalo (Cant. Días)') !!}
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-hashtag"></i>
                    </div>
                    {!! Form::number('dias_intervalo', null, ['class' => 'form-control', 'min' => '1', 'max' => '31']) !!}
                    <input type="hidden" id="dias_intervalo_anterior" value="{{ isset($facturaVentaAutomatica) ? $ultimo_dia_invervalo : 0 }}">
                    <input type="hidden" id="total_intervalo_anterior" value="{{ isset($facturaVentaAutomatica) ? $monto_recargo : 0 }}">
                </div>
            </div>
            <div class="col-md-4">
                {!! Form::label('recargo_intervalo', 'Recargo') !!}
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-percent"></i>
                    </div>
                    {!! Form::number('recargo_intervalo', null, ['class' => 'form-control', 'min' => '0']) !!}
                </div>
            </div>
            <div class="col-md-2">
                <button type="button" style="position:absolute;bottom:0;" class="btn btn-success" id="agregar-intervalo">
                    <i class="fa fa-plus" aria-hidden="true"></i> Agregar
                </button>
            </div>
            <div class="col-md-2">
                <button type="button" style="position:absolute;bottom:0;" class="btn btn-danger" id="borrar-intervalos">
                    <i class="fa fa-trash" aria-hidden="true"></i> Eliminar Todo
                </button>
            </div>
        </div>
        <div class="table-responsive">
            <table id="tabla-intervalos" class="table color-table info-table">
                <thead>
                <tr>
                    <th style="width:16%;">Nº Intervalo</th>
                    <th style="width:16%;">Desde</th>
                    <th style="width:16%;">Hasta</th>
                    <th style="width:16%;">Recargo</th>
                    <th style="width:16%;">Monto</th>
                    <th style="width:16%;">Total</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                    @if(isset($facturaVentaAutomatica->intervalos))
                        @foreach($facturaVentaAutomatica->intervalos->sortBy('desde') as $intervalo)
                            <tr>
                                <td>
                                    {{ $loop->iteration }}
                                    <input type="hidden" name="intervalos[{{ $intervalo->id }}][id]" class="id" value="{{ $intervalo->id }}">
                                    <input type="hidden" name="intervalos[{{ $intervalo->id }}][estado]" class="estado intervalo" value="1">
                                </td>
                                <td>
                                    <div class="input-group">
                                        <div class="input-group-addon">día</div>
                                        <input class="form-control" name="intervalos[{{ $intervalo->id }}][dias_desde]" type="number" step="0.01" value="{{ $intervalo->desde }}" readonly>
                                    </div>
                                </td>
                                <td>
                                    <div class="input-group">
                                        <div class="input-group-addon">día</div>
                                        <input class="form-control" name="intervalos[{{ $intervalo->id }}][dias_hasta]" type="number" step="0.01" value="{{ $intervalo->hasta }}" readonly>
                                    </div>
                                </td>
                                <td>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-percent"></i>
                                        </div>
                                        <input class="form-control" name="intervalos[{{ $intervalo->id }}][recargo]" type="number" step="0.01" value="{{ $intervalo->porcentaje_recargo }}" readonly>
                                    </div>
                                </td>
                                <td>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-usd"></i>
                                        </div>
                                        <input class="form-control" name="intervalos[{{ $intervalo->id }}][monto_recargo]" type="number" step="0.01" value="{{ $intervalo->montoRecargo() }}" readonly>
                                    </div>
                                </td>
                                <td>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-usd"></i>
                                        </div>
                                        <input class="form-control" name="intervalos[{{ $intervalo->id }}][monto_total]" type="number" step="0.01" value="{{ $facturaVentaAutomatica->totalIntervalo($intervalo->id) }}" readonly>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>

@section('scripts-intervalos')
    <script>
        $( document ).ready(function() {

        });

        $("#agregar-intervalo").click(function(){
            $('input#dias_vencimientos').parent().removeClass('has-error');
            $('input#recargo_vencimientos').parent().removeClass('has-error');

            if($('input#dias_intervalo').val() == '' || $('input#dias_intervalo').val() == null) {
                $('input#dias_intervalo').parent().addClass('has-error');
                $('input#dias_intervalo').focus();
            }else if($('input#recargo_intervalo').val() == '' || $('input#recargo_intervalo').val() == null) {
                $('input#recargo_intervalo').parent().addClass('has-error');
                $('input#recargo_intervalo').focus();
            }else {
                var diasAnterior = parseInt($('input#dias_intervalo_anterior').val());
                var totalAnterior = parseFloat($('input#total_intervalo_anterior').val());

                var diasIntervalo = parseInt($('input#dias_intervalo').val()) + diasAnterior;
                $('input#dias_intervalo').val('');
                var recargoIntervalo = parseFloat($('input#recargo_intervalo').val());
                $('input#recargo_intervalo').val('');
                var total = parseFloat($('input#total').val());

                var montoRecargo = total*recargoIntervalo/100;
                var montoTotal = (total + totalAnterior + montoRecargo).toFixed(2);

                var numero_orden = $('table#tabla-intervalos tbody tr').length+ 1;

                $('input#dias_intervalo_anterior').val(diasIntervalo);
                $('input#total_intervalo_anterior').val(totalAnterior + montoRecargo);

                $("#tabla-intervalos").append('<tr>' +
                    '<td>' +
                        numero_orden +
                        '<input type="hidden" name="intervalos['+(numero_orden*-1)+'][id]" class="id" value="'+(numero_orden*-1)+'">' +
                        '<input type="hidden" name="intervalos['+(numero_orden*-1)+'][estado]" class="estado intervalo" value="1">' +
                    '</td>' +
                    '<td>' +
                        '<div class="input-group">' +
                            '<div class="input-group-addon">día</div>' +
                            '<input class="form-control" name="intervalos['+(numero_orden*-1)+'][dias_desde]" type="number" step="0.01" value="'+ diasAnterior +'" readonly>' +
                        '</div>' +
                    '</td>' +
                    '<td>' +
                        '<div class="input-group">' +
                            '<div class="input-group-addon">día</div>' +
                            '<input class="form-control" name="intervalos['+(numero_orden*-1)+'][dias_hasta]" type="number" step="0.01" value="'+ diasIntervalo +'" readonly>' +
                        '</div>' +
                    '</td>' +
                    '<td>' +
                        '<div class="input-group">' +
                            '<div class="input-group-addon">' +
                                '<i class="fa fa-percent"></i>' +
                            '</div>' +
                            '<input class="form-control" name="intervalos['+(numero_orden*-1)+'][recargo]" type="number" step="0.01" value="'+ recargoIntervalo +'" readonly>' +
                        '</div>' +
                    '</td>' +
                    '<td>' +
                        '<div class="input-group">' +
                            '<div class="input-group-addon">' +
                                '<i class="fa fa-usd"></i>' +
                            '</div>' +
                            '<input class="form-control" name="intervalos['+(numero_orden*-1)+'][monto_recargo]" type="number" step="0.01" value="'+ (montoRecargo).toFixed(2) +'" readonly>' +
                        '</div>' +
                    '</td>' +
                    '<td>' +
                        '<div class="input-group">' +
                            '<div class="input-group-addon">' +
                                '<i class="fa fa-usd"></i>' +
                            '</div>' +
                            '<input class="form-control" name="intervalos['+(numero_orden*-1)+'][monto_total]" type="number" step="0.01" value="'+ montoTotal +'" readonly>' +
                        '</div>' +
                    '</td>' +
                '</tr>');
            }
        });

        $("#borrar-intervalos").click(function(){
            $('input.estado.intervalo').val(0);
            $('input.estado.intervalo').parent().parent().hide('slow');

            $('input#dias_intervalo_anterior').val(0);
            $('input#total_intervalo_anterior').val(0);
        });
    </script>
    @yield('scripts-dialog-tarjeta')
    @yield('scripts-dialog-cuenta-corriente')
@endsection