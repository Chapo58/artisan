@extends('frontend.layouts.app')

@section ('title', trans('labels.frontend.facturaventaautomatica_titulo')." - ".trans('buttons.general.crud.edit'))

@section('after-styles')
    {{ Html::style("css/frontend/plugins/jquery/jquery-ui.min.css") }}
@endsection

@section('content')
    <div class="row page-titles">
        <div class="col-md-8 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('labels.frontend.facturaventaautomatica_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item"><a href="{{url('/ventas/factura-venta-automatica')}}">{{ trans('labels.frontend.facturaventaautomatica_titulo') }}</a></li>
                <li class="breadcrumb-item">{{ trans('buttons.general.crud.edit') }} {{ trans('labels.frontend.facturaventaautomatica') }}</li>
                <li class="breadcrumb-item active">{{ $facturaVentaAutomatica }}</li>
            </ol>
        </div>
        <div class="col-md-4">
            <div class="pull-right">
                <a href="{{ url('/ventas/factura-venta-automatica') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i> {{ trans('buttons.general.crud.back') }}
                </a>
            </div>
        </div>
    </div>

    @include('includes.partials.messages')

    {!! Form::model($facturaVentaAutomatica, [
        'method' => 'PATCH',
        'url' => ['/ventas/factura-venta-automatica', $facturaVentaAutomatica->id],
        'class' => 'form-horizontal',
        'files' => true
    ]) !!}

        @include ('frontend.ventas.factura-venta-automatica.form', ['submitButtonText' => trans("buttons.general.crud.update")])

    {!! Form::close() !!}

@endsection

@section('after-scripts')
    {{ Html::script("js/frontend/plugins/jquery/jquery-ui.min.js") }}
    {{ Html::script("js/frontend/plugins/jquery/datepicker-es.js") }}
    {{ Html::script("js/frontend/plugins/maskedinput/jquery.maskedinput.min.js") }}

<script>
    $( document ).ready(function() {
        $('input#buscar_cliente').focus();

        $('input.fecha').datepicker({
            firstDay: 0,
            dateFormat: 'dd/mm/yy'
        }).mask("99/99/9999", {placeholder: "dd/mm/aaaa"});
        $('input#numero').mask("99999999",{placeholder:"########"});

        $('select#moneda_id').on('change', function () {
            buscarCotizacion();
        });

        function buscarCotizacion() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $( "input[name*='_token']" ).val()
                }
            });
            var moneda_id = $('#moneda_id option:selected').val();
            $.ajax({
                type: "POST",
                url: '{{ url("/configuraciones/moneda/obtenerCotizacion") }}',
                data: {moneda_id: moneda_id},
                success: function( msg ) {
                    $("#cotizacion").val(msg);
                }
            });
        }

        $(document).on("keydown.autocomplete", ".buscar-articulo", function (e) {
            if (e.keyCode == 13) {
                e.preventDefault();
            }else{
                $(this).autocomplete({
                    source: '{{ url('/articulos/articulo/obtenerPrecioArticulo') }}/'+$('select#lista_precios_id option:selected').val(),
                    minLength: 1,
                    autoFocus: true,
                    select: function (e, ui) {
                        var fila = $(this).parent().parent();
                        var cotizacion = parseFloat($("#cotizacion").val());

                        $(this).val(ui.item.value);
                        $(this).parent().find('input.articulo-id').val(ui.item.id);
                        if($('#moneda_id option:selected').val() != ui.item.moneda_id){
                            if(ui.item.cotizacion == 1){
                                ui.item.precio = (ui.item.precio / cotizacion).toFixed(2);
                            }else{
                                ui.item.precio = (ui.item.precio * ui.item.cotizacion).toFixed(2);
                            }
                        }
                        if($('#tipo_comprobante_id').val() != 0){
                            ui.item.precio = (ui.item.precio * (ui.item.iva/100 + 1)).toFixed(2);
                            ui.item.iva = 0;
                            ui.item.porcentaje_iva = 0;
                        }
                        fila.find('td input.precio').val(ui.item.precio);
                        fila.find('td input.iva').val(ui.item.iva);
                        fila.find('td input.porcentaje-iva').val(ui.item.porcentaje_iva);
                        fila.find('td input.impuesto-interno').val(ui.item.impuesto_interno);
                    }
                });
            }
        });

        $(document).on('click', '.borrar-detalle', function () {
            var fila = $(this).parent().parent();

            fila.hide('slow');
            fila.find('td input.estado').val(0);
            fila.find('td input.subtotal-neto').val(0);
            fila.find('td input.subtotal-impuesto-interno').val(0);
            fila.find('td input.subtotal-bruto').val(0);
            fila.find('input').removeAttr('required');

            refrescarTotales();
        });

        calcularSubtotales();
        calcularImporteBruto();
        calcularPorcentajeImpuesto();
        refrescarTotales();
    });

    $("#buscar_cliente").autocomplete({
        source: '{{ url('/personas/cliente/obtenerClienteParaVenta') }}/',
        minlength: 1,
        autoFocus: true,
        select: function (e, ui) {
            $('input#buscar_cliente').val(ui.item.value);
            $('input#cliente_id').val(ui.item.id);
            if(ui.item.value != 'Consumidor Final'){
                $("strong#saldo_maximo").text(' $ '+ui.item.saldo_maximo);
                $("strong#saldo_actual").text(' $ '+ui.item.saldo_actual);
            }
            if(ui.item.tipo_comprobante_id != null){
                $('select#tipo_comprobante_id').val(ui.item.tipo_comprobante_id);
            }
            if(ui.item.lista_precios_id != null)
                $('select#lista_precios_id').val(ui.item.lista_precios_id);
        }
    });

    $("#agregar-detalle").click(function(){
        agregarDetalle();
    });

    $("#agregar-detalle-de-cantidad").on('focusin', function(){
        agregarDetalle('cantidad');
    });

    $("#agregar-detalle-de-articulo").on('focusin', function(){
        agregarDetalle('articulo');
    });

    function agregarDetalle(inputName) {
        var numero_orden = $('table#tabla-detalles-venta tbody tr').length+ 1;
        $("#tabla-detalles-venta").append('<tr>' +
            '<td>' +
            numero_orden +
            '<input type="hidden" class="id" name="detalles['+(numero_orden*-1)+'][id]" value="'+(numero_orden*-1)+'">' +
            '</td>' +
            '<td>' +
            '<input class="form-control cantidad" name="detalles['+(numero_orden*-1)+'][cantidad]" type="number" value="1" max="" min="0" title="" required="required">' +
            '</td>' +
            '<td>' +
            '<input class="form-control buscar-articulo" name="detalles['+(numero_orden*-1)+'][articulo]"  placeholder="Buscar..." type="text" required="required">' +
            '<input class="articulo-id" name="detalles['+(numero_orden*-1)+'][articulo_id]" type="hidden" value="">' +
            '</td>' +
            '<td>' +
            '<input class="form-control precio" name="detalles['+(numero_orden*-1)+'][precio]" type="number" step="0.01" tabindex="99999" required="required" placeholder="$">' +
            '</td>' +
            '<td>' +
            '<input class="form-control subtotal-neto" name="detalles['+(numero_orden*-1)+'][total_neto]" type="number" step="0.01" readonly tabindex="99999" placeholder="$">' +
            '</td>' +
            '<td>' +
            '<input class="form-control iva" name="detalles['+(numero_orden*-1)+'][iva]" type="number" step="0.01" readonly tabindex="99999" placeholder="%">' +
            '<input type="hidden" class="porcentaje-iva" name="detalles['+(numero_orden*-1)+'][porcentaje_iva]" value="">' +
            '</td>' +
            '<td>' +
            '<input class="form-control subtotal-bruto" name="detalles['+(numero_orden*-1)+'][total_bruto]" type="number" step="0.01" readonly tabindex="99999" placeholder="$">' +
            '<input type="hidden" class="impuesto-interno" name="detalles['+(numero_orden*-1)+'][impuesto_interno]" value="">' +
            '<input type="hidden" class="subtotal-impuesto-interno" name="detalles['+(numero_orden*-1)+'][subtotal_impuesto_interno]" value="0">' +
            '</td>' +
            '<td>' +
            '<a class="btn btn-danger btn-circle borrar-detalle" name="detalles['+(numero_orden*-1)+'][eliminar]" tabindex="99999">' +
            '<i class="fa fa-trash-o" aria-hidden="true"></i>' +
            '</a>' +
            '<input type="hidden" class="estado" name="detalles['+(numero_orden*-1)+'][estado]" value="1">' +
            '</td>' +
            '</tr>');
        if(inputName != null){
            $("input[name='detalles["+(numero_orden*-1)+"]["+inputName+"]']").focus();
        }else{
            $("input[name='detalles["+(numero_orden*-1)+"][articulo]']").focus();
        }
        calcularSubtotales();
    }

    $("#porcentaje-impuesto").on('change', function (){
        calcularImporteImpuesto();
        calcularTotal();
    });

    $("#importe-impuesto").on('change', function (){
        calcularPorcentajeImpuesto();
        calcularTotal();
    });

    function refrescarTotales() {
        calcularImporteNeto();
        calcularImporteBruto();
        calcularImporteIva();
        calcularImporteImpuesto();
        calcularTotal();
    }

    function calcularSubtotales(){
        var $tablaTodasFilas = $("#tabla-detalles-venta tbody tr");
        $tablaTodasFilas.each(function (index) {
            var $tablaFila = $(this);

            $tablaFila.find('.cantidad ,.precio, .buscar-articulo').on('change', function () {
                var cantidad = parseInt($tablaFila.find('.cantidad').val(), 10);
                var precio = parseFloat($tablaFila.find('.precio').val());
                var iva = parseFloat($tablaFila.find('.iva').val());
                var impuesto = parseFloat($tablaFila.find('.impuesto-interno').val());

                var subtotalNeto = precio * cantidad;
                var importeIva = subtotalNeto * iva / 100;
                var subtotalImpuestoInterno = impuesto * cantidad;
                var subtotalBruto = subtotalNeto + importeIva + subtotalImpuestoInterno;

                if($('#tipo_comprobante_id').val() != 0){
                    subtotalNeto = subtotalNeto + importeIva;
                }

                if (!isNaN(subtotalNeto)) {
                    $tablaFila.find('.subtotal-neto').val(subtotalNeto.toFixed(2));
                }
                if (!isNaN(subtotalImpuestoInterno)) {
                    $tablaFila.find('.subtotal-impuesto-interno').val(subtotalImpuestoInterno.toFixed(2));
                }
                if (!isNaN(subtotalBruto)) {
                    $tablaFila.find('.subtotal-bruto').val(subtotalBruto.toFixed(2));
                }else{
                    $tablaFila.find('.subtotal-bruto').val(subtotalNeto.toFixed(2));
                }
                refrescarTotales();
            });
        });
    }

    function calcularImporteNeto() {
        var importeNeto = 0;
        $('.subtotal-neto').each(function () {
            var auxImporteNeto = parseFloat($(this).val());
            importeNeto += isNaN(auxImporteNeto) ? 0 : auxImporteNeto;
        });
        $('#importe-neto').val(importeNeto.toFixed(2));
    }

    function calcularImporteBruto() {
        var importeBruto = 0;
        $('.subtotal-bruto').each(function () {
            var auxImporteBruto = parseFloat($(this).val());
            importeBruto += isNaN(auxImporteBruto) ? 0 : auxImporteBruto;
        });
        $('#importe-bruto').val(importeBruto.toFixed(2));

        var importeImpuestoInterno = 0;
        $('.subtotal-impuesto-interno').each(function () {
            var auxImporteImpuestoInterno = parseFloat($(this).val());
            importeImpuestoInterno += isNaN(auxImporteImpuestoInterno) ? 0 : auxImporteImpuestoInterno;
        });
        $('#total-impuesto-interno').val(importeImpuestoInterno.toFixed(2));
    }

    function calcularImporteIva(){
        var importeNeto = parseFloat($('#importe-neto').val()).toFixed(2);
        var importeBruto = parseFloat($('#importe-bruto').val()).toFixed(2);
        var importeInpuestoInterno = parseFloat($('#total-impuesto-interno').val()).toFixed(2);
        var importeIva = importeBruto - importeNeto - importeInpuestoInterno;

        if (!isNaN(importeIva)) {
            $('#importe-iva').val(importeIva.toFixed(2));
        }else{
            $('#importe-iva').val('0.0');
        }
    }

    function calcularImporteImpuesto(){
        var importeBruto = $('#importe-bruto').val();
        var procentajeImpuesto = $('#porcentaje-impuesto').val();
        var importeImpuesto = parseFloat(importeBruto) * parseFloat(procentajeImpuesto) / 100;
        if (!isNaN(importeImpuesto)) {
            $('#importe-impuesto').val(importeImpuesto.toFixed(2));
        }else{
            $('#importe-impuesto').val('0.0');
        }
    }

    function calcularPorcentajeImpuesto(){
        var importeBruto = $('#importe-bruto').val();
        var importeImpuesto = $('#importe-impuesto').val();
        var procentajeImpuesto = parseFloat(importeImpuesto) * 100 / parseFloat(importeBruto);
        if (!isNaN(procentajeImpuesto)) {
            $('#porcentaje-impuesto').val(procentajeImpuesto.toFixed(2));
        }else{
            $('#porcentaje-impuesto').val('0.0');
        }
    }

    function calcularTotal(){
        var importeBruto = parseFloat($('#importe-bruto').val());
        var importeImpuesto = parseFloat($('#importe-impuesto').val());
        var total = importeBruto + importeImpuesto;
        if (!isNaN(total)) {
            $('#total').val(total.toFixed(2));
        }else{
            $('#total').val('0.0');
        }
    }
</script>

@yield('scripts-dialog-cliente')
@yield('scripts-intervalos')

@endsection