<div class="card">
    <div class="card-header">
        <div class="card-actions">
            <a class="" data-action="collapse"><i class="ti-minus"></i></a>
            <a class="btn-minimize" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
        </div>
        <h4 class="card-title m-b-0">
            Información de la Venta
        </h4>
    </div>
    <div class="card-body collapse show b-t">
        <div class="row form-group">
            <div class="col-md-4">
                {!! Form::label('cliente_id', 'Cliente') !!}
                <div class="row">
                    <div class="col-md-10">
                        <input id="buscar_cliente" name="buscar_cliente" class="form-control" placeholder="Buscar..." type="text" required="required" value="{{ isset($facturaVentaAutomatica->venta->cliente) ? $facturaVentaAutomatica->venta->cliente : '' }}"  {{ isset($facturaVentaAutomatica) ? 'disabled' : '' }}>
                        <input id="cliente_id" name="cliente_id" type="hidden" value="{{ isset($venta->cliente) ? $venta->cliente->id : '' }}">
                        {!! $errors->first('cliente_id', '<p class="text-danger">:message</p>') !!}
                    </div>
                    @if(!isset($facturaVentaAutomatica))
                    <div class="col-md-2">
                        <button type="button" class="btn btn-secondary" id="agregar-cliente"><i class="fa fa-plus"></i> </button>
                    </div>
                    @endif
                </div>
            </div>
            <div class="col-md-4">
                {!! Form::label('tipo_comprobante_id', 'Tipo de Comprobante') !!}
                {!! Form::select('tipo_comprobante_id', $tipos_comprobantes, isset($facturaVentaAutomatica) ? $facturaVentaAutomatica->venta->tipo_comprobante_id : '', array('class' => 'form-control', 'required' => 'required', isset($facturaVentaAutomatica) ? 'disabled' : '')) !!}
                {!! $errors->first('tipo_comprobante_id', '<p class="text-danger">:message</p>') !!}
            </div>
            <div class="col-md-4">
                {!! Form::label('numero', 'Punto de Venta') !!}
                    {!! Form::select('punto_venta_id', $puntos_de_ventas,  isset($facturaVentaAutomatica) ? $facturaVentaAutomatica->venta->punto_venta_id : 0, ['id' =>'punto_venta_id' ,'class' => 'form-control', 'required' => 'required', isset($facturaVentaAutomatica) ? 'disabled' : '']) !!}
                {!! $errors->first('numero', '<p class="text-danger">:message</p>') !!}
            </div>
        </div>
        <div class="row form-group">
            <div class="col-md-4">
                {!! Form::label('dia_facturacion', 'Día Facturación') !!}
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-hashtag"></i>
                    </div>
                    {!! Form::number('dia_facturacion', isset($facturaVentaAutomatica) ? $facturaVentaAutomatica->dia_facturacion : '', ['class' => 'form-control', 'min' => '1', 'max' => '31']) !!}
                </div>
                {!! $errors->first('dia_facturacion', '<p class="text-danger">:message</p>') !!}
            </div>
            <div class="col-md-4">
                {!! Form::label('fecha_inicio', 'Fecha Inicio') !!}
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    {!! Form::text('fecha_inicio', isset($facturaVentaAutomatica->fecha_inicio) ? $facturaVentaAutomatica->fecha_inicio->format('d/m/Y') : $fecha_hoy, ['class' => 'form-control fecha', 'required' => 'required']) !!}
                </div>
                {!! $errors->first('fecha_inicio', '<p class="text-danger">:message</p>') !!}
            </div>
            <div class="col-md-4">
                {!! Form::label('fecha_fin', 'Fecha Fin') !!}
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    {!! Form::text('fecha_fin', isset($facturaVentaAutomatica->fecha_fin) ? $facturaVentaAutomatica->fecha_fin->format('d/m/Y') : $fecha_hoy, ['class' => 'form-control fecha']) !!}
                </div>
                {!! $errors->first('fecha_fin', '<p class="text-danger">:message</p>') !!}
            </div>
        </div>
        <div class="row form-group">
            <div class="col-md-4">
                {!! Form::label('lista_precios_id', 'Lista de Precios') !!}
                {!! Form::select('lista_precios_id', $lista_precios, isset($facturaVentaAutomatica->venta->listaPrecios) ? $facturaVentaAutomatica->venta->listaPrecios->id : 0, ['class' => 'form-control', 'required' => 'required', isset($facturaVentaAutomatica) ? 'disabled' : '']) !!}
                {!! $errors->first('lista_precios_id', '<p class="text-danger">:message</p>') !!}
            </div>
            <div class="col-md-4">
                {!! Form::label('moneda_id', 'Moneda') !!}
                <div class="row">
                    <div class="col-md-7">
                        {!! Form::select('moneda_id', $monedas, isset($facturaVentaAutomatica->venta->moneda) ? $facturaVentaAutomatica->venta->moneda->id : (isset($monedaPorDefecto) ? $monedaPorDefecto->id : ''), ['class' => 'form-control', 'id' => 'moneda_id', 'required' => 'required', isset($facturaVentaAutomatica) ? 'disabled' : '']) !!}
                        {!! $errors->first('moneda_id', '<p class="text-danger">:message</p>') !!}
                    </div>
                    <div class="col-md-5">
                        {!! Form::number('cotizacion', isset($facturaVentaAutomatica) ? $facturaVentaAutomatica->venta->cotizacion : (isset($monedaPorDefecto) ? $monedaPorDefecto->cotizacion : 1), ['class' => 'form-control', 'step' => '0.01', 'id' => 'cotizacion', isset($facturaVentaAutomatica) ? 'readonly' : '']) !!}
                        {!! $errors->first('cotizacion', '<p class="text-danger">:message</p>') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="card">
    <div class="card-header">
        <div class="card-actions">
            <a class="" data-action="collapse"><i class="ti-minus"></i></a>
            <a class="btn-minimize" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
        </div>
        <h4 class="card-title m-b-0">
            Detalle de la Venta
        </h4>
    </div>
    <div class="card-body collapse show b-t">
        <div class="table-responsive">
            <table id="tabla-detalles-venta" class="table color-table info-table form-material">
                <thead>
                <tr>
                    <th>#</th>
                    <th style="width:10%;">Cantidad</th>
                    <th style="width:30%;">Artículo</th>
                    <th style="width:15%;">Pre/Uni. Neto</th>
                    <th style="width:15%;">Neto</th>
                    <th style="width:15%;">IVA</th>
                    <th style="width:15%;">Bruto</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                    @if(isset($facturaVentaAutomatica))
                        @foreach($facturaVentaAutomatica->venta->detalles as $detalle)
                            <tr>
                                <td>
                                    {{ $loop->index + 1 }}
                                    <input type="hidden" class="id" name="detalles[{{ $detalle->id }}][id]" value="{{ $detalle->id }}">
                                </td>
                                <td>{!! Form::number('detalles['.$detalle->id.'][cantidad]', $detalle->cantidad, ['class' => 'form-control cantidad', 'step' => '1', 'min' => '0', 'max' => '', 'title' => '']) !!}</td>
                                <td>
                                    <input type="text" class="form-control buscar-articulo" name="detalles[{{ $detalle->id }}][articulo]" placeholder="{{ trans('strings.backend.general.search_placeholder') }}" value="({{ $detalle->articulo->codigo }}) {{ $detalle->articulo->nombre }}">
                                    <input type="hidden" class="articulo-id" name="detalles[{{ $detalle->id }}][articulo_id]" value="{{ $detalle->articulo_id }}">
                                </td>
                                <td>
                                    {!! Form::number('detalles['.$detalle->id.'][precio]', $detalle->precio, ['class' => 'form-control precio', 'step' => '0.01', 'readonly', 'tabindex' => '9999', 'placeholder' => '$']) !!}
                                </td>
                                <td>
                                    {!! Form::number('detalles['.$detalle->id.'][total_neto]', $detalle->cantidad*$detalle->precio, ['class' => 'form-control subtotal-neto', 'readonly', 'step' => '0.01', 'tabindex' => '9999', 'placeholder' => '$']) !!}
                                </td>
                                <td>
                                        {!! Form::text('detalles['.$detalle->id.'][iva]', number_format($porcentajes_iva[$detalle->porcentaje_iva], 2), array('class' => 'form-control iva', 'readonly', 'tabindex' => '9999', 'placeholder' => '%')) !!}
                                        <input type="hidden" class="porcentaje-iva" name="detalles[{{ $detalle->id }}][porcentaje_iva]" value="{{ $detalle->porcentaje_iva }}">
                                </td>
                                <td>
                                    {!! Form::number('detalles['.$detalle->id.'][total_bruto]', number_format(($detalle->cantidad*$detalle->precio) * ($porcentajes_iva[$detalle->porcentaje_iva]/100 + 1), 2, '.', ''), ['class' => 'form-control subtotal-bruto', 'readonly', 'step' => '0.01', 'tabindex' => '9999', 'placeholder' => '$']) !!}
                                    <input type="hidden" class="impuesto-interno" name="detalles[{{$detalle->id}}][impuesto_interno]" value="{{ $detalle->impuesto_interno }}">
                                    <input type="hidden" class="subtotal-impuesto-interno" name="detalles[{{$detalle->id}}][subtotal_impuesto_interno]" value="{{ $detalle->impuesto_interno*$detalle->cantidad }}">
                                </td>
                                <td>
                                    <a class="btn btn-danger btn-circle borrar-detalle" name="detalles[{{ $detalle->id }}][eliminar]">
                                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                                    </a>
                                    <input type="hidden" class="estado" name="detalles[{{ $detalle->id }}][estado]" value="1">
                                </td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
                <tfoot>
                <tr>
                    <td>
                        ...
                    </td>
                    <td>{!! Form::number('', '', ['class' => 'form-control', 'id' => 'agregar-detalle-de-cantidad']) !!}</td>
                    <td>
                        <input type="text" class="form-control" id="agregar-detalle-de-articulo" placeholder="Nuevo Detalle...">
                    </td>
                    <td>
                        {!! Form::number('', null, ['class' => 'form-control', 'readonly', 'tabindex' => '9999', 'placeholder' => '$']) !!}
                    </td>
                    <td>
                        {!! Form::number('', null, ['class' => 'form-control', 'readonly', 'tabindex' => '9999', 'placeholder' => '$']) !!}
                    </td>
                    <td>
                        {!! Form::number('', null, ['class' => 'form-control', 'readonly', 'tabindex' => '9999', 'placeholder' => '%']) !!}
                    </td>
                    <td>
                        {!! Form::number('', null, ['class' => 'form-control', 'readonly', 'tabindex' => '9999', 'placeholder' => '$']) !!}
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <th colspan="3">
                        <a href="javascript:void(0)" class="btn btn-info btn-sm" id="agregar-detalle">
                            <i class="fa fa-plus" aria-hidden="true"></i> Agregar Detalle
                        </a>
                    </th>
                    <th><span class="pull-right">Subtot<span class="hidden-sm hidden-xs">ales</span>.</span></th>
                    <th>
                        {!! Form::number('importe_neto', isset($facturaVentaAutomatica) ? $facturaVentaAutomatica->venta->importe_neto : '', ['id' => 'importe-neto', 'class' => 'form-control', 'readonly', 'placeholder' => '$']) !!}
                    </th>
                    <th>
                        {!! Form::number('importe_iva', isset($facturaVentaAutomatica) ? $facturaVentaAutomatica->venta->importe_iva : '', ['id' => 'importe-iva', 'class' => 'form-control', 'step' => '0.01', 'readonly', 'placeholder' => '$']) !!}
                    </th>
                    <th>
                        {!! Form::number('importe_bruto', isset($facturaVentaAutomatica) ? $facturaVentaAutomatica->venta->importe_bruto : '', ['id' => 'importe-bruto', 'class' => 'form-control', 'readonly', 'placeholder' => '$']) !!}
                    </th>
                </tr>
                <tr>
                    <th colspan="4">
                    </th>
                    <th><span class="pull-right">Imp<span class="hidden-sm hidden-xs">uesto</span>.(+)/</br>Desc<span class="hidden-sm hidden-xs">uento</span>.(-)</span></th>
                    <th>
                            {!! Form::number('', 0, ['id' => 'porcentaje-impuesto', 'class' => 'form-control', 'step' => '0.01', 'placeholder' => '%']) !!}
                    </th>
                    <th>
                        {!! Form::number('impuesto', isset($facturaVentaAutomatica) ? $facturaVentaAutomatica->venta->impuesto : 0, ['id' => 'importe-impuesto', 'class' => 'form-control', 'step' => '0.01', 'placeholder' => '$']) !!}
                    </th>
                    <th></th>
                </tr>
                <tr>
                    <th colspan="5">
                    </th>
                    <th><span class="pull-right">Imp<span class="hidden-sm hidden-xs">uestos</span>. Int<span class="hidden-sm hidden-xs">ernos</span>.</span></th>
                    <th>
                        {!! Form::text('importe_impuesto_interno', isset($facturaVentaAutomatica) ? $facturaVentaAutomatica->venta->importe_impuesto_interno : 0, ['id' => 'total-impuesto-interno', 'class' => 'form-control', 'readonly', 'placeholder' => '$']) !!}
                    </th>
                    <th></th>
                </tr>
                <tr>
                    <th colspan="5">
                    </th>
                    <th><span class="pull-right">Total</span></th>
                    <th>
                        {!! Form::number('total', isset($facturaVentaAutomatica) ? $facturaVentaAutomatica->venta->total : 0, ['id' => 'total', 'class' => 'form-control', 'step' => '0.01', 'required' => 'required', 'placeholder' => '$']) !!}
                    </th>
                    <th></th>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>

@include('frontend.ventas.factura-venta-automatica.intervalos')


<div class="card">
    <div class="card-body">
        <div class="form-group">
            {{ Form::submit(isset($submitButtonText) ? $submitButtonText : trans('buttons.general.save'), ['class' => 'btn btn-themecolor btn-lg pull-right']) }}
        </div>
    </div>
</div>

<div class="card">
    <div class="card-body">
        <div class="row form-group">
            <div class="col-md-12">
                {!! Form::label('descripcion', 'Comentario') !!}
                {!! Form::textarea('descripcion', null, ['class' => 'form-control']) !!}
                {!! $errors->first('descripcion', '<p class="text-danger">:message</p>') !!}
            </div>
        </div>
    </div>
</div>

@include('frontend.personas.cliente.cliente_dialog')