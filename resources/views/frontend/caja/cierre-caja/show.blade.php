@extends('frontend.layouts.app')

@section ('title', trans('labels.frontend.cierrecaja_titulo')." - ".trans('buttons.general.crud.show'))

@section('after-styles')
    {{ Html::style("css/frontend/plugins/datatables/responsive.dataTables.min.css") }}
@endsection

@section('content')
    <div class="row page-titles">
        <div class="col-6 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">Detalles de Movimientos</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item"><a href="{{url('/caja/cierre-caja')}}">{{ trans('labels.frontend.cierrecaja_titulo') }}</a></li>
                <li class="breadcrumb-item">{{ trans('buttons.general.crud.show') }} {{ trans('labels.frontend.cierrecaja') }}</li>
                <li class="breadcrumb-item active">{{ $cierreCaja }}</li>
            </ol>
        </div>
        <div class="col-6">
            <div class="pull-right">
                <a href="{{ url('/caja/cierre-caja') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans('buttons.general.crud.back') }}</span>
                </a>
                <a href="{{ url('/caja/movimiento-caja/listar/' . $cierreCaja->id) }}" title="{{ trans('buttons.general.crud.edit') }}" class="btn btn-info">
                    <i class="fa fa-print" aria-hidden="true"></i>
                    <span class="hidden-xs-down">Imprimir</span>
                </a>
                @roles(['Usuario Admin', 'Usuario Gerente'])
                    <a href="javascript:void(0)" title="{{trans("buttons.general.crud.delete")." ".trans("labels.frontend.cierrecaja")}}" class="btn btn-danger delete">
                        <i class="fa fa-trash" aria-hidden="true"></i>
                        <span class="hidden-xs-down">{{ trans("buttons.general.crud.delete") }}</span>
                    </a>
                @endauth
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6 col-lg-6">
            <div class="card card-inverse card-info">
                <div class="box bg-info text-center text-white">
                    <h1 class="font-light text-white">APERTURA</h1>
                    <strong>Fecha:</strong> {{$cierreCaja->fecha_apertura->format('d/m/Y H:i:s')}}<br>
                    <strong>Usuario:</strong> {{ ($cierreCaja->usuarioApertura) ? $cierreCaja->usuarioApertura->empleado : 'Usuario Eliminado' }}<br>
                    <strong>Saldo:</strong> $ {{ $cierreCaja->saldo_apertura  }}
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-6">
            <div class="card card-inverse card-dark">
                <div class="box bg-dark text-center text-white">
                    <h1 class="font-light text-white">CIERRE</h1>
                    <strong>Fecha:</strong> {{$cierreCaja->fecha_cierre->format('d/m/Y H:i:s')}}<br>
                    <strong>Usuario:</strong> {{ ($cierreCaja->usuarioCierre) ? $cierreCaja->usuarioCierre->empleado : 'Usuario Eliminado' }}<br>
                    <strong>Saldo:</strong> $ {{ $cierreCaja->saldo_cierre  }}
                </div>
            </div>
        </div>
    </div>


    <div class="table-responsive">
        <table class="table full-color-table full-inverse-table color-bordered-table inverse-bordered-table hover-table">
            <thead>
            <tr>
                <th>Forma de Pago</th>
                <th>
                    <i class="ace-icon fa fa-plus bigger-110"></i>
                    Entrada
                </th>
                <th>
                    <i class="ace-icon fa fa-minus bigger-110"></i>
                    Salida
                </th>
                <th>
                    <i class="ace-icon fa fa-money bigger-110"></i>
                    Saldo
                </th>
            </tr>
            </thead>
            <tbody>
            @foreach($totales as $item)
                <tr>
                    <td>{{ $formasDePago[$item->forma_de_pago] }}</td>
                    <td>$ {{ $item->entrada }} </td>
                    <td>$ {{ $item->salida }} </td>
                    <td>$ {{ number_format(($item->entrada - $item->salida), 2, '.', ',') }}</td>
                </tr>
            @endforeach
            <tr class="warning">
                <td>Totales</td>
                <td>$ {{ $totales->sum('entrada') }} </td>
                <td>$ {{ $totales->sum('salida') }} </td>
                <td>$ {{ number_format(($totales->sum('entrada') - $totales->sum('salida')), 2, '.', ',') }}</td>
            </tr>
            </tbody>
        </table>
    </div>


    <div class="card">
        <div class="card-header">
            <div class="card-actions">
                <a class="mytooltip" data-action="collapse"><i class="ti-minus"></i><span class="tooltip-content3">Minimizar Ventana</span></a>
                <a class="btn-minimize mytooltip" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
                <a class="btn-close mytooltip" data-action="close"><i class="ti-close"></i><span class="tooltip-content3">Cerrar Ventana</span></a>
            </div>
            <h4 class="card-title m-b-0">Movimientos de Caja</h4>
        </div>
        <div class="card-body show b-t">
            @include('includes.partials.messages')
            <div class="table-responsive">
                <table id="movimientocaja-table" class="table selectable table-striped table-hover table-sm">
                    <thead>
                    <tr>
                        <th></th>
                        <th>Hora</th>
                        <th>Form. Pago</th>
                        <th>Concepto</th>
                        <th>Detalle</th>
                        <th>Entrada</th>
                        <th>Salida</th>
                        <th>{{ trans('labels.general.actions') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($movimientocaja as $item)
                        <tr>
                            <td></td>
                            <td>{{ $item->created_at->format('H:i:s') }}</td>
                            <td>
                                @if($item->forma_de_pago == 0)
                                    <span class="label label-success arrowed-right">
                                        @elseif($item->forma_de_pago == 1)
                                            <span class="label label-warning arrowed-right">
                                        @else
                                                    <span class="label label-purple arrowed-right">
                                        @endif
                                                        {{ $formasDePago[$item->forma_de_pago] }}
                                        </span>
                            </td>
                            <td>
                                @if($item->concepto_de_caja)
                                    {{ $conceptosDeCaja[$item->concepto_de_caja] }}
                                @else
                                    {{ $item->conceptoPersonalizado }}
                                @endif
                            </td>
                            <td>
                                @if($item->url != null)
                                    <a href="{{ url($item->url).'/'.$item->entidad_id }}" title="Ver">{{ $item->detalle }}</a>
                                @else
                                    {{ $item->detalle }}
                                @endif
                            </td>
                            <td style="{{($item->entrada > 0) ? 'color:green;font-weight:bold;':''}}">{{ $item->entrada }}</td>
                            <td style="{{($item->salida > 0) ? 'color:red;font-weight:bold;':''}}">{{ $item->salida }}</td>
                            <td>
                                <a href="{{ url('/caja/movimiento-caja/' . $item->id) }}" class="mytooltip">
                                    <i class="fa fa-eye text-success"></i><span class="tooltip-content3">{{ trans('buttons.general.crud.show') }} {{ trans('labels.frontend.movimientocaja') }}</span>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('after-scripts')
    {{ Html::script("js/frontend/plugins/datatables/jquery.dataTables.min.js") }}
    {{ Html::script("js/frontend/plugins/datatables/dataTables.responsive.min.js") }}
    {{ Html::script("js/frontend/plugins/datatables/dataTables.select.min.js") }}

    <script>
        $(document).ready(function() {
            $('#movimientocaja-table').DataTable({
                "aaSorting": [],
                responsive: {
                    details: {
                        type: 'column',
                        target: 'tr'
                    }
                },
                "columnDefs": [{
                    targets: 0,
                    className: 'control',
                    orderable: false,
                    searchable: false,
                    defaultContent: ''
                },
                    { responsivePriority: 1, targets: 3 }]
            });
        } );

        $("body").on("click", ".delete", function () {
            swal({
                title: "{{trans('buttons.general.crud.delete').' '.trans('labels.frontend.cierrecaja')}}",
                text: "¿Realmente desea eliminar este registro?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#ff8726',
                confirmButtonText: "Eliminar"
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        type: 'POST',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: '{{ url('/caja/cierre-caja/'.$cierreCaja->id) }}',
                        data: {"_method" : 'DELETE'},
                        success: function (msg) {
                            swal("Eliminado!", "El registro ha sido correctamente eliminado.", "success").then((result) => { window.location.href = '{{url('/caja/cierre-caja')}}'; });
                        }
                    });
                }
            });
        });
    </script>
@endsection
