@extends ('frontend.layouts.app')

@section ('title', trans('labels.frontend.cierrecaja_titulo')." - ".trans('navs.frontend.user.administration'))

@section('after-styles')
    {{ Html::style("css/frontend/plugins/datatables/responsive.dataTables.min.css") }}
@endsection

@section('content')
    <div class="row page-titles">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('navs.frontend.user.administration') }} {{ trans('labels.frontend.cierrecaja_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item active">{{ trans('labels.frontend.cierrecaja_titulo') }}</li>
            </ol>
        </div>
    </div>

    @tips($_SERVER['REQUEST_URI'])

    <div class="card">
        <div class="card-body">
            <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
            @include('includes.partials.messages')
            <div class="table-responsive">
                <table id="cierrecaja-table" class="table selectable table-striped table-hover table-sm">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Caja</th>
                            <th>Fecha Apertura</th>
                            <th>Fecha Cierre</th>
                            <th>Saldo Apertura</th>
                            <th>Saldo Cierre</th>
                            <th>{{ trans('labels.general.actions') }}</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('after-scripts')
    {{ Html::script("js/frontend/plugins/datatables/jquery.dataTables.min.js") }}
    {{ Html::script("js/frontend/plugins/datatables/dataTables.responsive.min.js") }}
    {{ Html::script("js/frontend/plugins/datatables/dataTables.select.min.js") }}

<script>
    $(document).ready(function() {
        $('#cierrecaja-table').DataTable({
            "aaSorting": [],
            processing: true,
            serverSide: true,
            responsive: {
                details: {
                    type: 'column',
                    target: 'tr'
                }
            },
            "columnDefs": [{
                targets: 0,
                className: 'control',
                orderable: false,
                searchable: false,
                defaultContent: '&nbsp;&nbsp;'
            },
                { responsivePriority: 1, targets: -1 }],
            ajax: '{!! route('frontend.caja.CierreCajaData') !!}',
            columns: [
                {data: null, name: ''},
                {data: 'caja', name: 'caja'},
                {data: 'fecha_apertura', name: 'fecha_apertura'},
                {data: 'fecha_cierre', name: 'fecha_cierre'},
                {data: 'saldo_apertura', name: 'saldo_apertura'},
                {data: 'saldo_cierre', name: 'saldo_cierre'},
                {data: 'action', name: 'action', orderable: false, searchable: false}
            ]
        });
    } );

    $("body").on("click", ".delete", function () {
        var id = $(this).attr("data-id");
        swal({
            title: "Eliminar {{trans('labels.frontend.cierrecaja')}}",
            text: "¿Realmente desea eliminar este registro?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#ff8726',
            confirmButtonText: "Eliminar"
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/caja/cierre-caja/'+id,
                    data: {"_method" : 'DELETE'},
                    success: function (msg) {
                        $("#" + id).hide(1);
                        swal("Eliminado!", "El registro ha sido correctamente eliminado.", "success");
                    }
                });
            }
        });
    });
</script>

@endsection
