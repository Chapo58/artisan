<div class="card">
    <div class="card-body">
        <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
        <br>
        <div class="row form-group">
            <div class="col-md-6">
                {!! Form::label('numero', 'Numero') !!}
                {!! Form::number('numero', null, ['class' => 'form-control', 'required' => 'required']) !!}
                {!! $errors->first('numero', '<p class="text-danger">:message</p>') !!}
            </div>
            <div class="col-md-6">
                {!! Form::label('nombre', 'Nombre') !!}
                {!! Form::text('nombre', null, ['class' => 'form-control', 'required' => 'required']) !!}
                {!! $errors->first('nombre', '<p class="text-danger">:message</p>') !!}
            </div>
        </div>
        <div class="row form-group">
            <div class="col-md-6">
                {!! Form::label('moneda_id', 'Moneda') !!}
                {!! Form::select('moneda_id', $monedas, isset($caja->moneda) ? $caja->moneda->id : '', array('class' => 'form-control', 'required' => 'required')) !!}
                {!! $errors->first('moneda_id', '<p class="text-danger">:message</p>') !!}
            </div>
            <div class="col-md-6">
                {!! Form::label('cuenta_bancaria_id', 'Cuenta Bancaria Asociada') !!}
                {!! Form::select('cuenta_bancaria_id', $cuentaBancaria, isset($caja->cuentaBancaria) ? $caja->cuentaBancaria->id : '', array('class' => 'form-control')) !!}
                {!! $errors->first('cuenta_bancaria_id', '<p class="text-danger">:message</p>') !!}
            </div>
        </div>
        @roles(['Usuario Admin', 'Usuario Gerente'])
        <div class="row form-group">
            <div class="col-md-12">
                {!! Form::label('sucursal_id', trans('Sucursal')) !!}
                {!! Form::select('sucursal_id', $sucursales, isset($caja->sucursal) ? $caja->sucursal->id : '', array('class' => 'form-control', 'required' => 'required')) !!}
                {!! $errors->first('sucursal_id', '<p class="text-danger">:message</p>') !!}
            </div>
        </div>
        @endauth
        <hr>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-12">
                    {{ Form::submit(isset($submitButtonText) ? $submitButtonText : trans('buttons.general.save'), ['class' => 'btn btn-lg btn-themecolor pull-right']) }}
                </div>
            </div>
        </div>
    </div>
</div>
