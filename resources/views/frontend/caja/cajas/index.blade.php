@extends ('frontend.layouts.app')

@section ('title', trans('labels.frontend.cajas_titulo')." - ".trans('navs.frontend.user.administration'))

@section('after-styles')
    {{ Html::style("css/frontend/plugins/datatables/responsive.dataTables.min.css") }}
@endsection

@section('content')
    <div class="row page-titles">
        <div class="col-6 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('labels.frontend.cajas_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item active">{{ trans('labels.frontend.cajas_titulo') }}</li>
            </ol>
        </div>
        <div class="col-6">
            <div class="pull-right">
                <a href="{{ url('/caja/cajas/create') }}" class="btn btn-themecolor btn-lg" title="{{ trans('buttons.general.crud.create').' '.trans('labels.frontend.cajas')  }}">
                    <i class="fa fa-plus" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans('buttons.general.crud.create').' '.trans('labels.frontend.cajas') }}</span>
                </a>
            </div>
        </div>
    </div>

    @tips($_SERVER['REQUEST_URI'])

    <div class="card">
        <div class="card-body">
            <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
            @include('includes.partials.messages')
            <div class="table-responsive">
                <table id="cajas-table" class="table selectable table-striped table-hover table-sm">
                    <thead>
                    <tr>
                        <th></th>
                        <th>Numero</th>
                        <th>Nombre</th>
                        <th>Moneda</th>
                        <th>Cuanta Bancaria Asociada</th>
                        <th>Estado</th>
                        <th>{{ trans('labels.general.actions') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($cajas as $item)
                        <tr id="{{$item->id}}">
                            <td>&nbsp;&nbsp;</td>
                            <td>{{ $item->numero }}</td>
                            <td>{{ $item->nombre }}</td>
                            <td>{{ isset($item->moneda) ? $item->moneda->nombre: 'Ninguna' }}</td>
                            <td>{{ isset($item->cuentaBancaria) ? $item->cuentaBancaria->numero.' - '.$item->cuentaBancaria->banco->nombre : 'Ninguna' }}</td>
                            <td>
                                @if($item->estado == 1)
                                    <span class="label label-success">
                                @else
                                    <span class="label label-danger">
                                @endif
                                    {{ $estadosCaja[$item->estado] }}
                                </span>
                            </td>
                            <td>
                                <a href="{{ url('/caja/cajas/' . $item->id) }}" class="mytooltip">
                                    <i class="fa fa-eye text-success m-r-10"></i>
                                    <span class="tooltip-content3">{{ trans('buttons.general.crud.show') }} {{ trans('labels.frontend.cajas') }}</span>
                                </a>
                                <a href="{{ url('/caja/cajas/' . $item->id . '/edit') }}" class="mytooltip">
                                    <i class="fa fa-pencil text-info m-r-10"></i>
                                    <span class="tooltip-content3">{{ trans('buttons.general.crud.edit') }} {{ trans('labels.frontend.cajas') }}</span>
                                </a>
                                <a href="javascript:void(0)" class="delete mytooltip" data-id="{{$item->id}}">
                                    <i class="fa fa-close text-danger"></i>
                                    <span class="tooltip-content3">{{trans("buttons.general.crud.delete")." ".trans("labels.frontend.cajas")}}</span>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('after-scripts')
    {{ Html::script("js/frontend/plugins/datatables/jquery.dataTables.min.js") }}
    {{ Html::script("js/frontend/plugins/datatables/dataTables.responsive.min.js") }}
    {{ Html::script("js/frontend/plugins/datatables/dataTables.select.min.js") }}

<script>
    $(document).ready(function() {
        $('#cajas-table').DataTable({
            "aaSorting": [1,'asc'],
            responsive: {
                details: {
                    type: 'column',
                    target: 'tr'
                }
            },
            "columnDefs": [{
                targets: 0,
                className: 'control',
                orderable: false,
                searchable: false,
                defaultContent: '&nbsp;&nbsp;'
            },
                { responsivePriority: 1, targets: -1 },
                { responsivePriority: 1, targets: 2 },
                { responsivePriority: 1, targets: -2 }],
        });
    } );

    $("body").on("click", ".delete", function () {
        var id = $(this).attr("data-id");
        swal({
            title: "{{trans('buttons.general.crud.delete').' '.trans('labels.frontend.cajas')}}",
            text: "¿Realmente desea eliminar este registro?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#ff8726',
            confirmButtonText: "Eliminar"
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/caja/cajas/'+id,
                    data: {"_method" : 'DELETE'},
                    success: function (msg) {
                        $("#" + id).hide(1);
                        swal("Eliminado!", "El registro ha sido correctamente eliminado.", "success");
                    }
                });
            }
        });
    });
</script>

@endsection
