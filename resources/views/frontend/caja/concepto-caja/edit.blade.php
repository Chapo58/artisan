@extends('frontend.layouts.app')

@section ('title', trans('labels.frontend.conceptocaja_titulo')." - ".trans('buttons.general.crud.edit'))

@section('content')
    <div class="row page-titles">
        <div class="col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('labels.frontend.conceptocaja_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item"><a href="{{url('/caja/concepto-caja')}}">{{ trans('labels.frontend.conceptocaja_titulo') }}</a></li>
                <li class="breadcrumb-item">{{ trans('buttons.general.crud.edit') }} {{ trans('labels.frontend.conceptocaja') }}</li>
                <li class="breadcrumb-item active">{{ $conceptocaja }}</li>
            </ol>
        </div>
        <div class="col-4">
            <div class="pull-right">
                <a href="{{ url('/caja/concepto-caja') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans('buttons.general.crud.back') }}</span>
                </a>
            </div>
        </div>
    </div>

    @include('includes.partials.messages')

    {!! Form::model($conceptocaja, [
        'method' => 'PATCH',
        'url' => ['/caja/concepto-caja', $conceptocaja->id],
        'class' => 'form-horizontal',
        'files' => true
    ]) !!}

        @include ('frontend.caja.concepto-caja.form', ['submitButtonText' => trans("buttons.general.crud.update")])

    {!! Form::close() !!}

@endsection
