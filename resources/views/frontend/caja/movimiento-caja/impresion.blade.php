<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{$movimientoCaja}}</title>

        {{ Html::style('css/frontend/plugins/bootstrap/css/bootstrap.min.css') }}

        <style>
            @media print {
                .table th {
                    background-color: #D9EDF7 !important;
                }
            }
        </style>

    </head>
    <body>
    <table style="width:100%">
        <tr>
            <td><img class="img-responsive" style="max-width: 200px;" src="{{ ($empresa->imagen_url) ? url($empresa->imagen_url) : url("images/frontend/logos/logo_mini.png") }}"/></td>
            <td><h1 style="color:#5692CB;">{{$movimientoCaja->caja}}</h1></td>
        </tr>
    </table>
    <hr>
    <table class="table table-condensed table-bordered" style="width:100%">
        <tr>
            <td><strong>Fecha:</strong></td>
            <td>{{ $movimientoCaja->created_at->format('d/m/Y H:i:s') }}</td>
        </tr>
        <tr>
            <td><strong>Forma de Pago:</strong></td>
            <td>{{ $formasDePago[$movimientoCaja->forma_de_pago] }}</td>
        </tr>
        <tr>
            <td><strong>Concepto:</strong></td>
            <td>
                @if($movimientoCaja->concepto_de_caja)
                    {{ $conceptosDeCaja[$movimientoCaja->concepto_de_caja] }}
                @else
                    {{ $movimientoCaja->conceptoPersonalizado }}
                @endif
            </td>
        </tr>
        <tr>
            <td><strong>Detalle:</strong></td>
            <td>{{ $movimientoCaja->detalle }}</td>
        </tr>
        <tr>
            @if($movimientoCaja->salida == 0)
                <td><strong>Entrada</strong></td>
                <td>{{$movimientoCaja->moneda->signo}} {{ $movimientoCaja->entrada }}</td>
            @else
                <td><strong>Salida</strong></td>
                <td>{{$movimientoCaja->moneda->signo}} {{ $movimientoCaja->salida }}</td>
            @endif
        </tr>
        <tr>
            <td><strong>Comentario:</strong></td>
            <td>{{ $movimientoCaja->comentario }}</td>
        </tr>
    </table>

    <script>
        window.print();
    </script>
    </body>
</html>
