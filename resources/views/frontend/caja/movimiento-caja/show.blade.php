@extends('frontend.layouts.app')

@section ('title', trans('labels.frontend.movimientocaja_titulo')." - ".trans('buttons.general.crud.show'))

@section('content')
    <div class="row page-titles">
        <div class="col-6 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('navs.frontend.user.administration') }} {{ trans('labels.frontend.movimientocaja_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item"><a href="{{url('/caja/movimiento-caja')}}">{{ trans('labels.frontend.movimientocaja_titulo') }}</a></li>
                <li class="breadcrumb-item">{{ trans('buttons.general.crud.show') }} {{ trans('labels.frontend.movimientocaja') }}</li>
                <li class="breadcrumb-item active">{{ $movimientocaja }}</li>
            </ol>
        </div>
        <div class="col-6">
            <div class="pull-right">
                <a href="{{ url('/caja/movimiento-caja') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans('buttons.general.crud.back') }}</span>
                </a>
                @roles(['Usuario Admin', 'Usuario Gerente'])
                    <a href="{{ url('/caja/movimiento-caja/' . $movimientocaja->id . '/edit') }}" title="{{ trans('buttons.general.crud.edit') }}" class="btn btn-info">
                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                        <span class="hidden-xs-down">{{ trans('buttons.general.crud.edit') }}</span>
                    </a>
                    <a href="javascript:void(0)" title="{{trans("buttons.general.crud.delete")." ".trans("labels.frontend.movimientocaja")}}" class="btn btn-danger delete">
                        <i class="fa fa-trash" aria-hidden="true"></i>
                        <span class="hidden-xs-down">{{ trans("buttons.general.crud.delete") }}</span>
                    </a>
                @endauth
                <a href="{{ url('/caja/movimiento-caja/imprimir/' . $movimientocaja->id) }}" target="_blank" title="Imprimir" class="btn btn-primary">
                    <i class="fa fa-print" aria-hidden="true"></i>
                </a>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
            <h4 class="font-medium m-t-30">{{ $movimientocaja }}</h4>
            <hr>
            <div class="row">
                <div class="col-md-3 font-weight-bold">Fecha</div>
                <div class="col-md-9">{{ $movimientocaja->created_at->format('d/m/Y') }}</div>
            </div><hr>
            <div class="row">
                <div class="col-md-3 font-weight-bold">Hora</div>
                <div class="col-md-9">{{ $movimientocaja->created_at->format('H:i:s') }}</div>
            </div><hr>
            <div class="row">
                <div class="col-md-3 font-weight-bold">Forma de Pago</div>
                <div class="col-md-9">{{ $formasDePago[$movimientocaja->forma_de_pago] }}</div>
            </div><hr>
            <div class="row">
                <div class="col-md-3 font-weight-bold">Concepto</div>
                <div class="col-md-9">
                    @if($movimientocaja->concepto_de_caja)
                        {{ $conceptosDeCaja[$movimientocaja->concepto_de_caja] }}
                    @else
                        {{ $movimientocaja->conceptoPersonalizado }}
                    @endif
                </div>
            </div><hr>
            <div class="row">
                <div class="col-md-3 font-weight-bold">Detalle</div>
                <div class="col-md-9">
                    @if($movimientocaja->url != null)
                        <a href="{{ url($movimientocaja->url).'/'.$movimientocaja->entidad_id }}">{{ $movimientocaja->detalle }}</a>
                    @else
                        {{ $movimientocaja->detalle }}
                    @endif
                </div>
            </div><hr>
            <div class="row">
                @if($movimientocaja->salida == 0)
                    <div class="col-md-3 font-weight-bold">Entrada</div>
                    <div class="col-md-9">{{ $movimientocaja->entrada }}</div>
                @else
                    <div class="col-md-3 font-weight-bold">Salida</div>
                    <div class="col-md-9">{{ $movimientocaja->salida }}</div>
                @endif
            </div><hr>
            <div class="row">
                <div class="col-md-3 font-weight-bold">Moneda</div>
                <div class="col-md-9">{{ $movimientocaja->moneda->nombre }}</div>
            </div><hr>
            <div class="row">
                <div class="col-md-3 font-weight-bold">Comentario</div>
                <div class="col-md-9">{{ $movimientocaja->comentario }}</div>
            </div>
        </div>
    </div>
@endsection

@section('after-scripts')

<script>
    $("body").on("click", ".delete", function () {
        swal({
            title: "Eliminar Movimiento de Caja",
            text: "¿Realmente desea eliminar este registro?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#ff8726',
            confirmButtonText: "Eliminar"
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '{{ url('/caja/movimiento-caja/'.$movimientocaja->id) }}',
                    data: {"_method" : 'DELETE'},
                    success: function (msg) {
                        swal("Eliminado!", "El registro ha sido correctamente eliminado.", "success").then((result) => { window.location.href = '{{url('/caja/movimiento-caja')}}'; });
                    }
                });
            }
        });
    });
</script>

@endsection
