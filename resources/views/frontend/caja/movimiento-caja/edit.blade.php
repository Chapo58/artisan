@extends('frontend.layouts.app')

@section ('title', trans('labels.frontend.movimientocaja_titulo')." - ".trans('buttons.general.crud.edit'))

@section('content')
    <div class="row page-titles">
        <div class="col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('navs.frontend.user.administration') }} {{ trans('labels.frontend.movimientocaja_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item"><a href="{{url('/caja/movimiento-caja')}}">{{ trans('labels.frontend.movimientocaja_titulo') }}</a></li>
                <li class="breadcrumb-item">{{ trans('buttons.general.crud.edit') }} {{ trans('labels.frontend.movimientocaja') }}</li>
                <li class="breadcrumb-item active">{{ $movimientocaja }}</li>
            </ol>
        </div>
        <div class="col-4">
            <div class="pull-right">
                <a href="{{ url('/caja/movimiento-caja') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                    <span class="hidden-xs-down">{{ trans('buttons.general.crud.back') }}</span>
                </a>
            </div>
        </div>
    </div>

    @include('includes.partials.messages')

    {!! Form::model($movimientocaja, [
        'method' => 'PATCH',
        'url' => ['/caja/movimiento-caja', $movimientocaja->id],
        'class' => 'form-horizontal',
        'files' => true
    ]) !!}

        @include ('frontend.caja.movimiento-caja.form', ['submitButtonText' => trans("buttons.general.crud.update")])

    {!! Form::close() !!}

@endsection
