<div class="card">
    <div class="card-body">
        <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
        <br>
        <div class="row form-group">
            <div class="col-md-6">
                {!! Form::label('importe', 'Importe') !!}
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-usd"></i>
                    </div>
                    {!! Form::number('importe', null, ['class' => 'form-control', 'required' => 'required', 'step' => '.01']) !!}
                </div>
                {!! $errors->first('importe', '<p class="text-danger">:message</p>') !!}
            </div>
            @if($tipo == 5 && !isset($movimientocaja))
                <div class="col-md-6">
                    {!! Form::label('caja_destino', 'Caja Destino') !!}
                    {!! Form::select('caja_destino', $cajas, '', array('class' => 'form-control', 'required' => 'required')) !!}
                    {!! $errors->first('caja_destino', '<p class="text-danger">:message</p>') !!}
                </div>
            @else
                @if(!isset($movimientocaja))
                    <div class="col-md-6">
                        {!! Form::label('concepto_de_caja', trans('Concepto')) !!}
                        {!! Form::select('concepto_de_caja', $conceptosPersonalizados, isset($movimientocaja) ? $movimientocaja->concepto_personalizado_id : '', array('class' => 'form-control', 'required' => 'required')) !!}
                        {!! $errors->first('concepto_de_caja', '<p class="text-danger">:message</p>') !!}
                    </div>
                @elseif(isset($movimientocaja) && $movimientocaja->concepto_personalizado_id)
                    <div class="col-md-6">
                        {!! Form::label('concepto_de_caja', trans('Concepto')) !!}
                        {!! Form::select('concepto_de_caja', $conceptosPersonalizados, isset($movimientocaja) ? $movimientocaja->concepto_personalizado_id : '', array('class' => 'form-control', 'required' => 'required')) !!}
                        {!! $errors->first('concepto_de_caja', '<p class="text-danger">:message</p>') !!}
                    </div>
                @endif
            @endif
        </div>
        <div class="row form-group">
            <div class="col-md-12">
                {!! Form::label('detalle', 'Detalle') !!}
                {!! Form::text('detalle', null, ['class' => 'form-control']) !!}
                {!! $errors->first('detalle', '<p class="text-danger">:message</p>') !!}
            </div>
        </div>
        <div class="row form-group">
            <div class="col-md-12">
                {!! Form::label('comentario', 'Comentario') !!}
                {!! Form::textarea('comentario', null, ['class' => 'form-control', 'size' => '10x3']) !!}
                {!! $errors->first('comentario', '<p class="text-danger">:message</p>') !!}
            </div>
        </div>
        {{ Form::hidden('tipo', $tipo) }}
        <hr>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-12">
                    {{ Form::submit(isset($submitButtonText) ? $submitButtonText : trans('buttons.general.save'), ['class' => 'btn btn-lg btn-themecolor pull-right']) }}
                </div>
            </div>
        </div>
    </div>
</div>
