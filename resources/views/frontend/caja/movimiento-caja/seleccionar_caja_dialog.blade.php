<div class="modal fade" id="modalCambioCaja" tabindex="-1" role="dialog" aria-labelledby="cambiarCajaModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="cambiarCajaModalLabel">Seleccionar Caja</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar"><span aria-hidden="true">&times;</span></button>
            </div>
            {!! Form::open([
                'method'=>'POST',
                'action' => ['Frontend\Caja\MovimientoCajaController@asignarCaja'],
                'style' => 'display:inline'
            ]) !!}
            <div class="modal-body">
                <div class="form-group">
                    {!! Form::label('caja_id', 'Caja') !!}
                    <div class="input-group">
                        <div class="input-group-addon"><i class="fa fa-inbox" aria-hidden="true"></i> </div>
                        {!! Form::select('caja_id', $cajas, ($empleado->caja) ? $empleado->caja->id : '', array('class' => 'form-control', 'required' => 'required')) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-themecolor">Aceptar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>