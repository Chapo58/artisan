<div class="modal fade" id="modalCambioEstado" tabindex="-1" role="dialog" aria-labelledby="cerrarCajaModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="cerrarCajaModalLabel">Control de Cierre de Caja</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar"><span aria-hidden="true">&times;</span></button>
            </div>
            {!! Form::open([
                'method'=>'POST',
                'action' => ['Frontend\Caja\MovimientoCajaController@cerrarCaja'],
                'style' => 'display:inline'
            ]) !!}
            <div class="modal-body">
                <div class="form-group">
                    {!! Form::label('saldoEfectivo', 'Saldo Efectivo') !!}
                    <div class="input-group">
                        <div class="input-group-addon"><i class="fa fa-money" aria-hidden="true"></i> </div>
                        {!! Form::number('saldoEfectivo', ($totales->where('forma_de_pago',0)->sum('entrada') - $totales->where('forma_de_pago',0)->sum('salida')), ['class' => 'form-control', 'required' => 'required', 'step' => '.01']) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-themecolor">Cerrar Caja</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>