@extends ('frontend.layouts.app')

@section ('title', trans('labels.frontend.movimientocaja_titulo')." - ".trans('navs.frontend.user.administration'))

@section('after-styles')
    {{ Html::style("css/frontend/plugins/datatables/responsive.dataTables.min.css") }}
@endsection

@section('content')
    <div class="row page-titles">
        <div class="col-6 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('navs.frontend.user.administration') }} {{ trans('labels.frontend.movimientocaja_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item active">{{ trans('labels.frontend.movimientocaja_titulo') }}</li>
            </ol>
        </div>
        <div class="col-6">
            <div class="pull-right">
                <div class="btn-group">
                    <button type="button" class="btn btn-themecolor btn-lg dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Acciones
                    </button>
                    <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 33px, 0px); top: 0px; left: 0px; will-change: transform;">
                        @if(isset($cierreCaja))
                            <a class="dropdown-item" href="{{ url('/caja/movimiento-caja/create/3') }}" title="Crear Nuevo Ingreso">
                                <i class="fa fa-plus"></i> Nueva Entrada
                            </a>
                            <a class="dropdown-item" href="{{ url('/caja/movimiento-caja/create/4') }}" title="Crear Nuevo Egreso">
                                <i class="fa fa-minus" aria-hidden="true"></i> Nueva Salida
                            </a>
                            <a class="dropdown-item" href="{{ url('/caja/movimiento-caja/listar/'.$cierreCaja->id) }}" target="_blank" title="Imprimir">
                                <i class="fa fa-print" aria-hidden="true"></i> Imprimir
                            </a>
                            <a class="dropdown-item" href="javascript:void(0)" data-backdrop="static" data-toggle="modal" data-target="#modalCambioEstado" title="Cerrar Caja">
                                <span class="fa fa-close"></span> Cerrar Caja
                            </a>
                        @else
                            @if(isset($empleado->caja))
                                <a class="dropdown-item" href="javascript:void(0)" data-backdrop="static" data-toggle="modal" data-target="#modalCambioEstado" title="Abrir Caja">
                                    <span class="fa fa-check"></span> Abrir Caja
                                </a>
                            @endif
                        @endif
                        @if($user->controladoraFiscal())
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="javascript:void(0)" onclick="cierreX()" title="Emitir Cierre X">
                                <span class="fa fa-paper-plane"></span> Emitir Cierre X
                            </a>
                            <a class="dropdown-item" href="javascript:void(0)" onclick="cierreZ()" title="Emitir Cierre Z">
                                <span class="fa fa-paper-plane-o"></span> Emitir Cierre Z
                            </a>
                        @endif
                        @roles(['Usuario Admin', 'Usuario Gerente'])
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{ url('/caja/movimiento-caja/create/5') }}" title="Crear Nueva Transferencia">
                                <i class="fa fa-exchange" aria-hidden="true"></i> Transferir Saldo
                            </a>
                            <a class="dropdown-item" href="javascript:void(0)" data-backdrop="static" data-toggle="modal" data-target="#modalCambioCaja" title="Seleccionar Caja">
                                <span class="fa fa-refresh"></span> Cambiar Caja
                            </a>
                        @endauth
                    </div>
                </div>
            </div>
        </div>
    </div>

@if(isset($empleado->caja))
    @if(isset($cierreCaja))

        @tips($_SERVER['REQUEST_URI'])

        <div class="row">
            <div class="col-lg-4 col-md-6">
                <div class="card">
                    <div class="d-flex flex-row">
                        <div class="p-10 bg-success">
                            <h3 class="text-white box m-b-0"><i class="fa fa-plus"></i></h3></div>
                        <div class="align-self-center m-l-20">
                            <h3 class="m-b-0 text-success" style="color:green;">$ {{ number_format($totales->sum('entrada'), 2, ',', '.') }}</h3>
                            <h5 class="text-muted m-b-0">Entrada</h5></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="card">
                    <div class="d-flex flex-row">
                        <div class="p-10 bg-danger">
                            <h3 class="text-white box m-b-0"><i class="fa fa-minus"></i></h3></div>
                        <div class="align-self-center m-l-20">
                            <h3 class="m-b-0 text-danger">$ {{ number_format($totales->sum('salida'), 2, ',', '.') }}</h3>
                            <h5 class="text-muted m-b-0">Salida</h5></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="card">
                    <div class="d-flex flex-row">
                        <div class="p-10 bg-info">
                            <h3 class="text-white box m-b-0"><i class="fa fa-money"></i></h3></div>
                        <div class="align-self-center m-l-20">
                            <h3 class="m-b-0 {{(($totales->sum('entrada') - $totales->sum('salida')) > 0) ? 'text-success' : 'text-danger'}}">$ {{ number_format($totales->sum('entrada') - $totales->sum('salida'), 2, ',', '.') }}</h3>
                            <h5 class="text-muted m-b-0">Saldo</h5></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-body">
                <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
                @include('includes.partials.messages')
                <div class="table-responsive">
                    <table id="movimientocaja-table" class="table selectable table-striped table-hover table-sm">
                        <thead>
                        <tr>
                            <th></th>
                            <th>Hora</th>
                            <th>Form. Pago</th>
                            <th>Concepto</th>
                            <th>Detalle</th>
                            <th>Entrada</th>
                            <th>Salida</th>
                            <th>{{ trans('labels.general.actions') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($movimientosDeCaja as $item)
                            <tr id="{{$item->id}}">
                                <td>&nbsp;&nbsp;</td>
                                <td>{{ $item->created_at->format('H:i:s') }}</td>
                                <td>
                                    <span class="label @if($item->forma_de_pago == 0)label-success @elseif($item->forma_de_pago == 1)label-warning @elseif($item->forma_de_pago == 2)label-primary @elseif($item->forma_de_pago == 3)label-info @else label-danger @endif">
                                        {{ $formasDePago[$item->forma_de_pago] }}
                                    </span>
                                </td>
                                <td>
                                    @if($item->concepto_de_caja)
                                        {{ $conceptosDeCaja[$item->concepto_de_caja] }}
                                    @else
                                        {{ $item->conceptoPersonalizado }}
                                    @endif
                                </td>
                                <td>
                                    @if($item->url != null)
                                        <a href="{{ url($item->url).'/'.$item->entidad_id }}" title="Ver">{{ $item->detalle }}</a>
                                    @else
                                        {{ $item->detalle }}
                                    @endif
                                </td>
                                <td style="{{($item->entrada > 0) ? 'color:green;' : ''}}"><b>{{ $item->moneda->signo }}  {{ $item->entrada }}</b></td>
                                <td style="{{($item->salida > 0) ? 'color:red;' : ''}}"><b>{{ $item->moneda->signo }} {{ $item->salida }}</b></td>
                                <td>
                                    <a href="{{ url('/caja/movimiento-caja/' . $item->id) }}" class="mytooltip">
                                        <i class="fa fa-eye text-success m-r-10"></i>
                                        <span class="tooltip-content3">{{ trans('buttons.general.crud.show') }} {{ trans('labels.frontend.movimientocaja') }}</span>
                                    </a>
                                    @roles(['Usuario Admin', 'Usuario Gerente'])
                                    <a href="{{ url('/caja/movimiento-caja/' . $item->id . '/edit') }}" class="mytooltip">
                                        <i class="fa fa-pencil text-info m-r-10"></i>
                                        <span class="tooltip-content3">{{ trans('buttons.general.crud.edit') }} {{ trans('labels.frontend.movimientocaja') }}</span>
                                    </a>
                                    <a href="javascript:void(0)" class="delete mytooltip" data-id="{{$item->id}}">
                                        <i class="fa fa-close text-danger"></i>
                                        <span class="tooltip-content3">{{trans("buttons.general.crud.delete")." ".trans("labels.frontend.movimientocaja")}}</span>
                                    </a>
                                    @endauth
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="table-responsive">
            <table class="table full-color-table full-muted-table color-bordered-table muted-bordered-table hover-table">
                <thead>
                <tr>
                    <th>Forma de Pago</th>
                    <th>
                        <i class="ace-icon fa fa-plus bigger-110"></i> Entrada
                    </th>
                    <th>
                        <i class="ace-icon fa fa-minus bigger-110"></i> Salida
                    </th>
                    <th>
                        <i class="ace-icon fa fa-money bigger-110"></i> Saldo
                    </th>
                </tr>
                </thead>
                <tbody>
                @foreach($totales as $item)
                    <tr>
                        <td>{{ $formasDePago[$item->forma_de_pago] }}</td>
                        <td>$ {{ number_format($item->entrada, 2, ',', '.') }} </td>
                        <td>$ {{ number_format($item->salida, 2, ',', '.') }} </td>
                        <td>$ {{ number_format(($item->entrada - $item->salida), 2, '.', ',') }}</td>
                    </tr>
                @endforeach
                <tr class="warning">
                    <td>Totales</td>
                    <td>$ {{ number_format($totales->sum('entrada'), 2, ',', '.') }} </td>
                    <td>$ {{ number_format($totales->sum('salida'), 2, ',', '.') }} </td>
                    <td>$ {{ number_format(($totales->sum('entrada') - $totales->sum('salida')), 2, ',', '.') }}</td>
                </tr>
                </tbody>
            </table>
        </div>

    @else
        <h1 class="text-info" align="center"><i class="fa fa-info-circle" aria-hidden="true"></i> {{ $empleado->caja->nombre }} Cerrada.</h1>
        <center>
            <a href="javascript:void(0)" class="btn btn-success btn-lg" data-backdrop="static" data-toggle="modal" data-target="#modalCambioEstado">
                <i class="fa fa-check" aria-hidden="true"></i> Abrir Caja
            </a>
        </center>
    @endif

@else

    <h2>Para ver los movimientos de caja debes seleccionar una caja primero</h2>

@endif

    @if(isset($cierreCaja))
        @include('frontend.caja.movimiento-caja.cerrar_caja_dialog')
    @else
        @include('frontend.caja.movimiento-caja.abrir_caja_dialog')
    @endif

    @role('Usuario Admin' || 'Usuario Gerente')
        @include('frontend.caja.movimiento-caja.seleccionar_caja_dialog')
    @endauth
@endsection

@section('after-scripts')
{{ Html::script("js/frontend/plugins/datatables/jquery.dataTables.min.js") }}
{{ Html::script("js/frontend/plugins/datatables/dataTables.responsive.min.js") }}
{{ Html::script("js/frontend/plugins/datatables/dataTables.select.min.js") }}
{{ Html::script("js/frontend/plugins/fiscal/ifu.js") }}
{{ Html::script("js/frontend/custom.js") }}

<script>
    $(document).ready(function() {
        $('#movimientocaja-table').DataTable({
            "aaSorting": [],
            responsive: {
                details: {
                    type: 'column',
                    target: 'tr'
                }
            },
            "columnDefs": [{
                targets: 0,
                className: 'control',
                orderable: false,
                searchable: false,
                defaultContent: ''
            },
                { responsivePriority: 1, targets: -1 },
                { responsivePriority: 1, targets: 3 }]
            });

        @if(!isset($empleado->caja))
            $('#modalCambioCaja').modal('show');
        @endif

    });

    $("body").on("click", ".delete", function () {
        var id = $(this).attr("data-id");
        swal({
            title: "Eliminar Movimiento de Caja",
            text: "¿Realmente desea eliminar este registro?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#ff8726',
            confirmButtonText: "Eliminar"
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/caja/movimiento-caja/'+id,
                    data: {"_method" : 'DELETE'},
                    success: function (msg) {
                        $("#" + id).hide(1);
                        swal("Eliminado!", "El registro ha sido correctamente eliminado.", "success");
                    }
                });
            }
        });
    });

    @if($user->controladoraFiscal())

    modelo = {{ $user->controladoraFiscal()->modelo_impresora_fiscal }};
    puerto = {{ $user->controladoraFiscal()->puerto_fiscal }};
    baudios = {{ $user->controladoraFiscal()->baudios_fiscal }};

    function cierreX(){
        var driver = new Driver();
        driver.modelo = modelo;
        driver.puerto = puerto;
        driver.baudios = baudios;

        try {
            driver.iniciarTrabajo();
            driver.cierreX();
            driver.finalizarTrabajo();
            swal({
                title: "Operación realizada con éxito!",
                timer: 2000,
                type: "success",
                showConfirmButton: false
            });
        } catch (e){
            swal("Error", e, "error");
        }
    }

    function cierreZ(){
        var driver = new Driver();
        driver.modelo = modelo;
        driver.puerto = puerto;
        driver.baudios = baudios;
        try {
            driver.iniciarTrabajo();
            driver.cierreZ();
            driver.finalizarTrabajo();
            swal({
                title: "Operación realizada con éxito!",
                timer: 2000,
                type: "success",
                showConfirmButton: false
            });
        } catch (e){
            swal("Error", e, "error");
        }
    }

    @endif
</script>

@endsection
