@extends('frontend.layouts.app')

@section ('title', 'Regimen de Compras y Ventas')

@section('after-styles')
    {{ Html::style("css/frontend/plugins/jquery/jquery-ui.min.css") }}

    <style>
        .ui-datepicker-calendar {
            display: none;
        }
        button.ui-datepicker-current { display: none; }
    </style>
@endsection

@section('content')
    <div class="row page-titles">
        <div class="col-md-12 col-12 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">Regimen de Compras y Ventas</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item active">Regimen de Compras y Ventas</li>
            </ol>
        </div>
    </div>

    @tips($_SERVER['REQUEST_URI'])

    <div class="card">
        <div class="card-header">
            <div class="card-actions">
                <a class="btn-minimize" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
            </div>
            <h4 class="card-title m-b-0">Regimen de Compras y Ventas</h4>
        </div>
        <div class="card-body">
            @include('includes.partials.messages')
            {!! Form::open([
                'method'=>'POST',
                'class' => 'form-horizontal',
                'target' => '_blank',
                'id' => 'formRegimen'
            ]) !!}
            <div class="row form-group justify-content-md-center">
                <div class="col-md-6">
                    {!! Form::label('periodo', 'Mes / Año') !!}
                    {!! Form::text('periodo', date('m/Y'), ['class' => 'form-control fecha']) !!}
                </div>
            </div>
            <hr>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-3">
                        {{ Form::button('<i class="ace-icon fa fa-money bigger-110"></i> Comprobantes Ventas', array('class' => 'btn btn-primary', 'onclick' => 'enviarFormulario("comprobantesVentas")')) }}
                    </div>
                    <div class="col-md-3">
                        {{ Form::button('<i class="ace-icon fa fa-money bigger-110"></i> Alicuotas Ventas', array('class' => 'btn btn-info', 'onclick' => 'enviarFormulario("alicuotasVentas")')) }}
                    </div>
                    <div class="col-md-3">
                        {{ Form::button('<i class="ace-icon fa fa-cart-arrow-down bigger-110"></i> Alicuotas Compras', array('class' => 'btn btn-info', 'onclick' => 'enviarFormulario("alicuotasCompras")')) }}
                    </div>
                    <div class="col-md-3">
                        {{ Form::button('<i class="ace-icon fa fa-cart-arrow-down bigger-110"></i> Comprobantes Compras', array('class' => 'btn btn-primary', 'onclick' => 'enviarFormulario("comprobantesCompras")')) }}
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection

@section('after-scripts')
    {{ Html::script("js/frontend/plugins/jquery/jquery-ui.min.js") }}
    {{ Html::script("js/frontend/plugins/jquery/datepicker-es.js") }}
    {{ Html::script("js/frontend/plugins/maskedinput/jquery.maskedinput.min.js") }}
    <script>
        $( document ).ready(function() {
            $('input.fecha').datepicker({
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,
                closeText : "Aceptar",
                dateFormat: 'mm/yy',
                onClose: function(dateText, inst) {
                  $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
                }
            }).mask("99/9999", {placeholder: "mm/aaaa"});
        });

        function enviarFormulario(tipo){
            $('#formRegimen').attr('action', "{{url('/informes/regimen').'/'}}"+tipo);
            $('#formRegimen').submit();
        }
    </script>
@endsection
