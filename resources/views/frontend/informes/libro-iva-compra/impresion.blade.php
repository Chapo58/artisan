<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Libro de Iva Compra</title>

    {{ Html::style('css/frontend/plugins/bootstrap/css/bootstrap.min.css') }}

    <style>
        @media print{@page {size: landscape}}
        @media print {
            .table th {
                background-color: #D9EDF7 !important;
            }
        }
    </style>

</head>
<body>
        <table style="width:100%">
            <tr>
                <td><img class="img-responsive" style="max-width: 200px;" src="{{$empresa->logo()}}"/></td>
                <td><h1 style="color:#5692CB;">Libro de Iva Compra</h1></td>
            </tr>
        </table>
        <hr>
        <table class="table table-condensed table-bordered" style="width:100%">
            <tr>
                <td>Fecha: {{ $periodo }}</td>
            </tr>
        </table>
        <table class="table table-condensed table-striped table-bordered" style="width:100%">
            <thead>
            <tr class="info">
                <th>Tipo de Comprobante</th>
                <th>Nº Comprobante</th>
                <th>Fecha</th>
                <th>Proveedor</th>
                <th>C.U.I.T.</th>
                @foreach($porcentajes as $item)
                    <th>Neto {{ $porcentajes_iva[$item->porcentaje_iva] }} %</th>
                @endforeach
                @foreach($porcentajes as $item)
                    <th>{{ $porcentajes_iva[$item->porcentaje_iva] }} %</th>
                @endforeach
                <th>Total</th>
            </tr>
            </thead>
            <tbody>
            @foreach($compras as $compra)
                <tr>
                    <td>{{ $tipos_comprobantes[$compra->tipo_comprobante_id] }}</td>
                    <td>{{ $compra->punto_venta.'-'.$compra->numero }}</td>
                    <td>{{ $compra->fecha->format('d/m/Y') }}</td>
                    <td>{{ $compra->proveedor }}</td>
                    <td>{{ (isset($compra->proveedor)) ? $compra->proveedor->cuit : '' }}</td>
                    @foreach($porcentajes as $item)
                        @if ($compra->tipo_comprobante_id == 1 || $compra->tipo_comprobante_id == 6 || $compra->tipo_comprobante_id == 11)
                            <td class="success">{{ $importes[$compra->id]['neto'][$item->porcentaje_iva] }}</td>
                        @else
                            <td class="danger">{{ $importes[$compra->id]['neto'][$item->porcentaje_iva] }}</td>
                        @endif
                    @endforeach
                    @foreach($porcentajes as $item)
                        @if ($compra->tipo_comprobante_id == 1 || $compra->tipo_comprobante_id == 6 || $compra->tipo_comprobante_id == 11)
                            <td class="success">{{ $importes[$compra->id]['iva'][$item->porcentaje_iva] }}</td>
                        @else
                            <td class="danger">{{ $importes[$compra->id]['iva'][$item->porcentaje_iva] }}</td>
                        @endif
                    @endforeach
                    <td>{{ $compra->total }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <hr><br>
        <?php $totalFinal = 0; ?>
        <table align="center" class="table table-condensed table-striped table-bordered" style="width:50%">
            @foreach($porcentajes as $item)
                <tr>
                    <td style="width:50%;">Total Neto {{ $porcentajes_iva[$item->porcentaje_iva] }}% :</td>
                    <td style="width:50%;">{{$importes['sumatoriaNeto'][$item->porcentaje_iva]}}</td>
                </tr>
            @endforeach
            @foreach($porcentajes as $item)
                <tr>
                    <td style="width:50%;">IVA {{ $porcentajes_iva[$item->porcentaje_iva] }}% :</td>
                    <td style="width:50%;">{{$importes['sumatoriaIva'][$item->porcentaje_iva]}}</td>
                </tr>
            @endforeach
            <tr style="font-weight:bold;">
                <td style="width:50%;">Total:</td>
                <td style="width:50%;">{{$importes['sumatoriaTotal']}}</td>
            </tr>
        </table>
<script>
    window.print();
</script>
</body>
</html>
