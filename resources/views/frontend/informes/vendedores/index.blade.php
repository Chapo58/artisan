@extends('frontend.layouts.app')

@section ('title', trans('labels.frontend.vendedor_titulo')." - Listar")

@section('after-styles')
    {{ Html::style("css/frontend/plugins/select2/select2.min.css") }}
@endsection

@section('content')
    <div class="row page-titles">
        <div class="col-md-12 col-12 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('labels.frontend.vendedor_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item active">{{ trans('labels.frontend.vendedor_titulo') }}</li>
            </ol>
        </div>
    </div>

    @tips($_SERVER['REQUEST_URI'])

    {!! Form::open(['method' => 'GET', 'url' => 'informes/vendedor', 'class' => 'form-horizontal', 'id' => 'form'])  !!}
    <div class="card">
        <div class="card-header">
            <div class="card-actions">
                <a class="" data-action="collapse"><i class="ti-minus"></i></a>
                <a class="btn-minimize mytooltip" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
            </div>
            <h4 class="card-title m-b-0">Parametros</h4>
        </div>
        <div class="card-body collapse show b-t">
            @include('includes.partials.messages')
            <div class="row">
                <div class="form-group col-md-3">
                    {!! Form::label('desde', 'Desde') !!}
                    {!! Form::date('desde', $desde, ['class' => 'form-control fecha']) !!}
                </div>
                <div class="form-group col-md-3">
                    {!! Form::label('hasta', 'Hasta') !!}
                    {!! Form::date('hasta', null, ['class' => 'form-control fecha']) !!}
                </div>
                <div class="form-group col-md-6">
                    {!! Form::label('vendedor_id', 'Vendedor') !!}
                    {!! Form::select('vendedor_id', $vendedores, null, ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-6">
                    <label class="control-label" for="rubros">Rubros Excluidos</label>
                    <select id="rubros" name="rubros[]" class="select2 m-b-10 select2-multiple" style="width: 100%" multiple="multiple" data-placeholder="Seleccionar">
                        @foreach($rubros as $item)
                            <option value="{{$item->id}}">{{$item}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-md-4">
                    {!! Form::label('comision', 'Comisión') !!}
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-percent"></i>
                        </div>
                        {!! Form::number('comision', '', ['class' => 'form-control','step' => '0.01']) !!}
                    </div>
                </div>
            </div>
            {{ Form::hidden('impresion') }}
            {{ Form::hidden('liquidar') }}
            <hr>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-12">
                        {{ Form::button('<i class="ace-icon fa fa-eye bigger-110"></i> Visualizar', array('type' => 'submit', 'class' => 'btn btn-info btn-lg pull-right')) }}
                        {{ Form::button('<i class="ace-icon fa fa-print bigger-110"></i> Imprimir', array('type' => 'button', 'class' => 'btn btn-primary btn-lg pull-right', 'onclick' => 'imprimir()', 'style' => 'margin-right:10px;')) }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}

    @if($encabezadosVentas)
    <div class="card">
        <div class="card-header">
            <div class="card-actions">
                <a class="" data-action="collapse"><i class="ti-minus"></i></a>
                <a class="btn-minimize" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
            </div>
            <h4 class="card-title m-b-0">Previsualizacion del Informe</h4>
        </div>
        <div class="card-body collapse show b-t">
            <table id="ventas-table" class="table color-table muted-table table-striped table-bordered table-hover table-condensed">
                @if($encabezadosVentas)
                    <thead>
                    <tr>
                        <th>Comprobante</th>
                        <th>Importe</th>
                        @if($comision)
                            <th>Comisión</th>
                        @endif
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($encabezadosVentas as $item)
                        <tr>
                            <td>{{ $tipos_comprobantes[$item->tipo_comprobante_id] }} {{ $item->punto_venta }}-{{ $item->numero }}</td>
                            <td>$ {{$detallesVentas->where('venta_id',$item->id)->sum('total')}}</td>
                            @if($comision)
                                <td>$ {{ number_format(($comision * $detallesVentas->where('venta_id',$item->id)->sum('total')) / 100, 2) }}</td>
                            @endif
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot style="font-weight: bold">
                    <tr class="info">
                        <td>Total</td>
                        <td class="green">
                            $ {{$detallesVentas->sum('total')}}
                        </td>
                        @if($comision)
                            <td class="green">
                                $ {{ number_format(($comision * $detallesVentas->sum('total')) / 100, 2) }}
                            </td>
                        @endif
                    </tr>
                    </tfoot>
                @endif
            </table>
        </div>
    </div>
    @endif

    @if($comision)
        <div class="card">
            <div class="card-body">
                <div class="form-group">
                    {{ Form::button('<i class="ace-icon fa fa-handshake bigger-110"></i> Liquidar Comisión', array('type' => 'button', 'class' => 'btn btn-lg btn-themecolor pull-right', 'onclick' => 'liquidar()')) }}
                </div>
            </div>
        </div>
    @endif

@endsection

@section('after-scripts')
    {{ Html::script("js/frontend/plugins/select2/select2.full.min.js") }}

    <script>
        jQuery(document).ready(function() {
            $(".select2").select2();
            $('.selectpicker').selectpicker();
        });

        function imprimir(){
            $('[name=impresion]').val(1);
            $('#form').submit();
        }

        function liquidar(){
            swal({
                title: "Liquidar Comisión",
                text: "¿Esta seguro que desea crear la liquidación? Esto generara automaticamente un movimiento de caja en su caja actual.",
                type: "info",
                showCancelButton: true,
                confirmButtonColor: '#2EB800',
                confirmButtonText: "Liquidar"
            }).then((result) => {
                if (result.value) {
                    $('[name=liquidar]').val(1);
                    $('#form').submit();
                }
            });
        }
    </script>
@endsection
