<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Informe de Ventas</title>

        {{ Html::style('css/frontend/plugins/bootstrap/css/bootstrap.min.css') }}

        <style>
            @media print {
                .table th {
                    background-color: #D9EDF7 !important;
                }
            }
        </style>

    </head>
    <body class="no-skin">
        @include('includes.partials.logged-in-as')

        <div class="main-container ace-save-state" id="main-container">
            <div class="container">
                <table style="width:100%">
                    <tr>
                        <td><img class="img-responsive" style="max-width: 200px; max-height: 80px;" src="{{(isset($vendedor->empresa)) ? $vendedor->empresa->logo() : ''}}"/></td>
                        <td><h1 class="pull-right" style="color:#5692CB;">Informe de Ventas</h1></td>
                    </tr>
                </table>
                <div class="panel-body">
                    <hr>
                    <div class="row" style="font-weight: bold">
                        <div class="col-6">Desde: {{ $desde }} </div>
                        <div class="col-6">Hasta: {{ $hasta }} </div>
                        <div class="col-6">Vendedor: {{ $vendedor }} </div>
                        <div class="col-6">Comisión: {{ $comision }}% </div>
                    </div>
                    <hr>
                    <div class="table-responsive">
                        <table id="ventas-table" class="table table-condensed table-striped table-hover">
                            @if($encabezadosVentas)
                                <thead>
                                <tr>
                                    <th>Comprobante</th>
                                    <th>Importe</th>
                                    @if($comision)
                                        <th>Comisión</th>
                                    @endif
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($encabezadosVentas as $item)
                                    <tr>
                                        <td>{{ $tipos_comprobantes[$item->tipo_comprobante_id] }} {{ $item->punto_venta }}-{{ $item->numero }}</td>
                                        <td>{{$detallesVentas->where('venta_id',$item->id)->sum('total')}}</td>
                                        @if($comision)
                                            <td>{{ number_format(($comision * $detallesVentas->where('venta_id',$item->id)->sum('total')) / 100, 2) }}</td>
                                        @endif
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot style="font-weight: bold">
                                <tr>
                                    <td>Total</td>
                                    <td class="green">
                                        {{$detallesVentas->sum('total')}}
                                    </td>
                                    @if($comision)
                                        <td class="green">
                                            {{ number_format(($comision * $detallesVentas->sum('total')) / 100, 2) }}
                                        </td>
                                    @endif
                                </tr>
                                </tfoot>
                            @endif
                        </table>
                    </div>
                </div>
            </div>
            </div><!-- container -->
        </div>
        <script>
            window.print();
        </script>
    </body>

</html>
