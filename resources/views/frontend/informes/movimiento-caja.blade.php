<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <body class="no-skin">
        <link rel="stylesheet" href="css/frontend/plugins/bootstrapPDFs/bootstrap.min.css" />
        @include('includes.partials.logged-in-as')

        <div class="main-container ace-save-state" id="main-container">
            <div class="container">
                <table style="width:100%">
                    <tr>
                        <td><img class="img-responsive" style="max-width: 200px; max-height: 80px;" src="{{$cierreCaja->caja->empresa->logo(true)}}"/></td>
                        <td><h1 class="pull-right" style="color:#5692CB;">Movimientos de Caja</h1></td>
                    </tr>
                </table>
                <hr>
                <table class="table table-condensed table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th>APERTURA</th>
                            <th>CIERRE</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <strong>Fecha:</strong> {{$cierreCaja->fecha_apertura->format('d/m/Y H:i:s')}}<br>
                                <strong>Usuario:</strong> {{ $cierreCaja->usuarioApertura->empleado }}<br>
                                <strong>Saldo:</strong> $ {{ $cierreCaja->saldo_apertura  }}
                            </td>
                            <td>
                                <strong>Fecha:</strong> {{($cierreCaja->fecha_cierre) ? $cierreCaja->fecha_cierre->format('d/m/Y H:i:s') : ''}}<br>
                                <strong>Usuario:</strong> {{ ($cierreCaja->usuarioCierre) ? $cierreCaja->usuarioCierre->empleado : '' }}<br>
                                <strong>Saldo:</strong> $ {{ ($cierreCaja->saldo_cierre) ? $cierreCaja->saldo_cierre : '' }}
                            </td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th colspan="2"align="center">Caja: {{ $cierreCaja->caja }}</th>
                        </tr>
                    </tfoot>
                </table>
                <table class="table table-condensed table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr class="info">
                            <th>Hora</th>
                            <th>Form. Pago</th>
                            <th>Concepto</th>
                            <th>Detalle</th>
                            <th>Entrada</th>
                            <th>Salida</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($movimientosDeCaja as $item)
                            <tr>
                                <td>{{ $item->created_at->format('H:i:s') }}</td>
                                <td>{{ $formasDePago[$item->forma_de_pago] }}</td>
                                @if($item->concepto_de_caja)
                                  <td>{{ $conceptosDeCaja[$item->concepto_de_caja] }}</td>
                                @else
                                  <td>{{ $item->conceptoPersonalizado }}</td>
                                @endif
                                <td>{{ $item->detalle }}</td>
                                <td style="color:green;">$ {{ $item->entrada }}</td>
                                <td style="color:red;">$ {{ $item->salida }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <hr><br>
                <table class="table table-striped table-bordered table-condensed no-margin-bottom no-border-top" style="width:70%">
                    <thead>
                        <tr>
                            <th colspan="4" style="text-align: center;">RESUMEN</th>
                        </tr>
                        <tr class="info">
                            <th>Forma de Pago</th>
                            <th><i class="ace-icon fa fa-plus bigger-110"></i> Entrada</th>
                            <th><i class="ace-icon fa fa-minus bigger-110"></i> Salida</th>
                            <th><i class="ace-icon fa fa-money bigger-110"></i> Saldo</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($totales as $item)
                            <tr>
                                <td>{{ $formasDePago[$item->forma_de_pago] }}</td>
                                <td style="color:green;">$ {{ $item->entrada }}</td>
                                <td style="color:red;">$ {{ $item->salida }}</td>
                                <td style="@if($item->entrada > $item->salida) color:green; @else color:red; @endif">$ {{ number_format(($item->entrada - $item->salida), 2, '.', ',') }}</td>
                            </tr>
                        @endforeach
                        <tr class="warning">
                            <td>Totales</td>
                            <td style="color:green;">$ {{ $totales->sum('entrada') }}</td>
                            <td style="color:red;">$ {{ $totales->sum('salida') }}</td>
                            <td style="@if($item->entrada > $item->salida) color:green; @else color:red; @endif">$ {{ number_format($totales->sum('entrada') - $totales->sum('salida'), 2, '.', ',') }}</td>
                        </tr>
                    </tbody>
                </table>
            </div><!-- container -->
        </div>
    </body>
</html>
