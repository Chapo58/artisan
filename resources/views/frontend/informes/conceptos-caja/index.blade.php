@extends('frontend.layouts.app')

@section ('title', trans('labels.frontend.conceptocaja_titulo')." - Listar")

@section('after-styles')
    {!! Charts::styles() !!}
@endsection

@section('content')
    <div class="row page-titles">
        <div class="col-md-12 col-12 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('labels.frontend.conceptocaja_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item active">{{ trans('labels.frontend.conceptocaja_titulo') }}</li>
            </ol>
        </div>
    </div>

    @tips($_SERVER['REQUEST_URI'])

    <div class="card">
        <div class="card-header">
            <div class="card-actions">
                <a class="mytooltip" data-action="collapse"><i class="ti-minus"></i><span class="tooltip-content3">Minimizar Ventana</span></a>
                <a class="btn-minimize mytooltip" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
            </div>
            <h4 class="card-title m-b-0">Parametros</h4>
        </div>
        <div class="card-body collapse show b-t">
            {!! Form::open(['method' => 'GET', 'url' => 'informes/conceptos-caja', 'class' => 'form-horizontal', 'id' => 'form'])  !!}
            <div class="row">
                <div class="form-group col-md-3">
                    {!! Form::label('desde', 'Desde') !!}
                    {!! Form::date('desde', $desde, ['class' => 'form-control fecha']) !!}
                </div>
                <div class="form-group col-md-3">
                    {!! Form::label('hasta', 'Hasta') !!}
                    {!! Form::date('hasta', null, ['class' => 'form-control fecha']) !!}
                </div>
                <div class="form-group col-md-3">
                    {!! Form::label('caja_id', 'Caja') !!}
                    {!! Form::select('caja_id', $cajas, null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group col-md-3">
                    {!! Form::label('concepto_personalizado', trans('Concepto')) !!}
                    {!! Form::select('concepto_personalizado_id', $conceptosPersonalizados, null, array('class' => 'form-control')) !!}
                </div>
            </div>
            {{ Form::hidden('impresion') }}
            <hr>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-12">
                        {{ Form::button('<i class="ace-icon fa fa-filter bigger-110"></i> Filtrar', array('type' => 'submit', 'class' => 'btn btn-lg btn-info pull-right')) }}
                        {{ Form::button('<i class="ace-icon fa fa-print bigger-110"></i> Imprimir', array('type' => 'button', 'class' => 'btn btn-lg btn-primary pull-right', 'onclick' => 'imprimir()','style'=>'margin-right:10px;')) }}
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            <div class="card-actions">
                <a class="mytooltip" data-action="collapse"><i class="ti-minus"></i><span class="tooltip-content3">Minimizar Ventana</span></a>
                <a class="btn-minimize mytooltip" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
            </div>
            <h4 class="card-title m-b-0">Previsualizacion del Informe</h4>
        </div>
        <div class="card-body collapse show b-t">
            <table class="table color-table muted-table table-striped table-bordered table-hover table-condensed">
                @if($conceptoSeleccionado)
                    <thead>
                    <tr>
                        <th>Fecha</th>
                        <th>Caja</th>
                        <th>Sucursal</th>
                        <th>Entrada</th>
                        <th>Salida</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($conceptosLista as $item)
                        <tr>
                            <td>{{ $item->created_at->format('d/m/Y H:i:s') }}</td>
                            <td>{{ $item->caja }}</td>
                            <td>{{ $item->caja->sucursal }}</td>
                            <td class="{{($item->entrada > 0) ? 'text-success' : ''}}">{{ $item->entrada }}</td>
                            <td class="{{($item->salida > 0) ? 'text-danger' : ''}}">{{ $item->salida }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot style="font-weight: bold">
                    <tr class="info">
                        <td colspan="3">Subtotales</td>
                        <td class="{{($conceptosLista->sum('entrada') > 0) ? 'text-success' : ''}}">
                            {{number_format($conceptosLista->sum('entrada'),2)}}
                        </td>
                        <td class="{{($conceptosLista->sum('salida') > 0) ? 'text-danger' : ''}}">
                            {{number_format($conceptosLista->sum('salida'),2)}}
                        </td>
                    </tr>
                    <tr class="info">
                        <td colspan="4">Total</td>
                        <td class="{{(($conceptosLista->sum('entrada') - $conceptosLista->sum('salida')) > 0) ? 'text-success' : 'text-danger'}}">
                            {{number_format($conceptosLista->sum('entrada') - $conceptosLista->sum('salida'),2)}}
                        </td>
                    </tr>
                    </tfoot>
                @else
                    <thead>
                    <tr>
                        <th>Concepto</th>
                        <th>Entrada</th>
                        <th>Salida</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($conceptosLista as $item)
                        <tr>
                            <td>{{ ($item->conceptoPersonalizado) ? $item->conceptoPersonalizado->nombre : 'Concepto Eliminado' }}</td>
                            <td class="{{($item->entrada > 0) ? 'text-success' : ''}}">{{ $item->entrada }}</td>
                            <td class="{{($item->salida > 0) ? 'text-danger' : ''}}">{{ $item->salida }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot style="font-weight: bold">
                    <tr class="info">
                        <td>Subtotales</td>
                        <td class="{{($conceptosLista->sum('entrada') > 0) ? 'text-success' : ''}}">
                            {{number_format($conceptosLista->sum('entrada'),2)}}
                        </td>
                        <td class="{{($conceptosLista->sum('salida') > 0) ? 'text-danger' : ''}}">
                            {{number_format($conceptosLista->sum('salida'),2)}}
                        </td>
                    </tr>
                    <tr class="info">
                        <td colspan="2">Total</td>
                        <td class="{{(($conceptosLista->sum('entrada') - $conceptosLista->sum('salida')) > 0) ? 'text-success' : 'text-danger'}}">
                            {{number_format($conceptosLista->sum('entrada') - $conceptosLista->sum('salida'),2)}}
                        </td>
                    </tr>
                    </tfoot>
                @endif
            </table>
        </div>
    </div>

    @if($conceptoSeleccionado)
        <div class="card">
            <div class="card-body">
                <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
                {!! $chartConcepto->html() !!}
            </div>
        </div>
    @else
        <div class="card">
            <div class="card-body">
                <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
                {!! $chartEntrada->html() !!}
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
                {!! $chartSalida->html() !!}
            </div>
        </div>
    @endif

@endsection

@section('after-scripts')

<!-- LIBRERIAS GRAFICOS -->
<script type="text/javascript" src="https://cdn.rawgit.com/Mikhus/canvas-gauges/gh-pages/download/2.1.2/all/gauge.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/chartist/0.10.1/chartist.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
<script type="text/javascript" src="https://static.fusioncharts.com/code/latest/fusioncharts.js"></script>
<script type="text/javascript" src="https://static.fusioncharts.com/code/latest/themes/fusioncharts.theme.fint.js"></script>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">google.charts.load('current', {'packages':['corechart', 'gauge', 'geochart', 'bar', 'line']})</script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/highcharts/5.0.7/highcharts.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/highcharts/5.0.7/js/modules/offline-exporting.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/highmaps/5.0.7/js/modules/map.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/highmaps/5.0.7/js/modules/data.js"></script>
<script type="text/javascript" src="https://code.highcharts.com/mapdata/custom/world.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.2.6/raphael.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/justgage/1.2.2/justgage.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.2.6/raphael.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.5/d3.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/plottable.js/2.8.0/plottable.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/progressbar.js/1.0.1/progressbar.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.5/d3.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/c3/0.4.11/c3.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/echarts/3.6.2/echarts.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/amcharts/3.21.2/amcharts.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/amcharts/3.21.2/serial.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/amcharts/3.21.2/plugins/export/export.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/amcharts/3.21.2/themes/light.js"></script>
<!-- LIBRERIAS GRAFICOS -->

    @if($conceptoSeleccionado)
        {!! $chartConcepto->script() !!}
    @else
        {!! $chartEntrada->script() !!}
        {!! $chartSalida->script() !!}
    @endif
<script>
    function imprimir(){
        $('[name=impresion]').val(1);
        $('#form').submit();
    }
</script>
@endsection
