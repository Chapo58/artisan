<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Conceptos de Caja</title>

        {{ Html::style('css/frontend/plugins/bootstrap/css/bootstrap.min.css') }}

        <style>
            @media print {
                .table th {
                    background-color: #D9EDF7 !important;
                }
            }
        </style>

    </head>
    <body class="no-skin">
        @include('includes.partials.logged-in-as')

        <div class="main-container ace-save-state" id="main-container">
            <div class="container">
                <table style="width:100%">
                    <tr>
                        <td><img class="img-responsive" style="max-width: 200px; max-height: 80px;" src="{{ $empresa->logo() }}"/></td>
                        <td><h1 class="pull-right" style="color:#5692CB;">Conceptos de Caja</h1></td>
                    </tr>
                </table>
                <div class="panel-body">
                    <hr>
                    <div class="row" style="font-weight: bold">
                        <div class="col-4">Desde: {{ $desde }} </div>
                        <div class="col-4">Hasta: {{ $hasta }} </div>
                        <div class="col-4">Concepto: {{ $conceptosPersonalizados[$conceptoSeleccionado] }} </div>
                    </div>
                    <hr>
                    <div class="table-responsive">
                        <table id="conceptocaja-table" class="table table-condensed table-striped table-hover">
                            @if($conceptoSeleccionado)
                                <thead>
                                <tr class="info">
                                    <th>Fecha</th>
                                    <th>Caja</th>
                                    <th>Sucursal</th>
                                    <th>Entrada</th>
                                    <th>Salida</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($conceptosLista as $item)
                                    <tr>
                                        <td>{{ $item->created_at->format('d/m/Y H:i:s') }}</td>
                                        <td>{{ $item->caja }}</td>
                                        <td>{{ $item->caja->sucursal }}</td>
                                        <td class="{{($item->entrada > 0) ? 'green' : ''}}">{{ $item->entrada }}</td>
                                        <td class="{{($item->salida > 0) ? 'red' : ''}}">{{ $item->salida }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot style="font-weight: bold">
                                <tr class="info">
                                    <td colspan="3">Subtotales</td>
                                    <td class="{{($conceptosLista->sum('entrada') > 0) ? 'green' : ''}}">
                                        {{number_format($conceptosLista->sum('entrada'),2)}}
                                    </td>
                                    <td class="{{($conceptosLista->sum('salida') > 0) ? 'red' : ''}}">
                                        {{number_format($conceptosLista->sum('salida'),2)}}
                                    </td>
                                </tr>
                                <tr class="info">
                                    <td colspan="4">Total</td>
                                    <td class="{{(($conceptosLista->sum('entrada') - $conceptosLista->sum('salida')) > 0) ? 'green' : 'red'}}">
                                        {{number_format($conceptosLista->sum('entrada') - $conceptosLista->sum('salida'),2)}}
                                    </td>
                                </tr>
                                </tfoot>
                            @else
                                <thead>
                                <tr class="info">
                                    <th>Concepto</th>
                                    <th>Entrada</th>
                                    <th>Salida</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($conceptosLista as $item)
                                    <tr>
                                        <td>{{ ($item->conceptoPersonalizado) ? $item->conceptoPersonalizado->nombre : 'Concepto Eliminado' }}</td>
                                        <td class="{{($item->entrada > 0) ? 'green' : ''}}">{{ $item->entrada }}</td>
                                        <td class="{{($item->salida > 0) ? 'red' : ''}}">{{ $item->salida }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot style="font-weight: bold">
                                <tr class="info">
                                    <td>Subtotales</td>
                                    <td class="{{($conceptosLista->sum('entrada') > 0) ? 'green' : ''}}">
                                        {{number_format($conceptosLista->sum('entrada'),2)}}
                                    </td>
                                    <td class="{{($conceptosLista->sum('salida') > 0) ? 'red' : ''}}">
                                        {{number_format($conceptosLista->sum('salida'),2)}}
                                    </td>
                                </tr>
                                <tr class="info">
                                    <td colspan="2">Total</td>
                                    <td class="{{(($conceptosLista->sum('entrada') - $conceptosLista->sum('salida')) > 0) ? 'green' : 'red'}}">
                                        {{number_format($conceptosLista->sum('entrada') - $conceptosLista->sum('salida'),2)}}
                                    </td>
                                </tr>
                                </tfoot>
                            @endif
                        </table>
                    
                </div>
            </div>
            </div><!-- container -->
        </div>
        <script>
            window.print();
        </script>
    </body>

</html>
