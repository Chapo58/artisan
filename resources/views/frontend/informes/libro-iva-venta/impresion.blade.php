<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Libro de Iva Venta</title>

        {{ Html::style('css/frontend/plugins/bootstrap/css/bootstrap.min.css') }}

        <style>
            @media print{@page {size: landscape}}
            @media print {
                .table th {
                    background-color: #D9EDF7 !important;
                }
            }
        </style>

    </head>
    <body>
    <table style="width:100%">
        <tr>
            <td><img class="img-responsive" style="max-width: 200px; max-height: 80px;" src="{{ $empresa->logo() }}"/></td>
            <td><h1 style="color:#5692CB;">Libro de Iva Venta</h1></td>
        </tr>
    </table>
    <hr>
    <table class="table table-condensed table-bordered" style="width:100%">
        <tr>
            <td>Fecha: {{ $periodo }}</td>
        </tr>
    </table>
    <table class="table table-condensed table-striped table-bordered" style="width:100%">
        <thead>
        <tr class="info">
            <th>Tipo de Comprobante</th>
            <th>Nº Comprobante</th>
            <th>Fecha</th>
            <th>Cliente</th>
            <th>C.U.I.T.</th>
            @foreach($porcentajes as $item)
                <th>Neto {{ $porcentajes_iva[$item->porcentaje_iva] }} %</th>
            @endforeach
            @foreach($porcentajes as $item)
                <th>{{ $porcentajes_iva[$item->porcentaje_iva] }} %</th>
            @endforeach
            <th>Imp. Int.</th>
            <th>Total</th>
        </tr>
        </thead>
        <tbody>
            @foreach($comprobantes as $comprobante)
                <tr>
                    <td>{{ $tipos_comprobantes[$comprobante->tipo_comprobante_id] }}</td>
                    <td>{{ $comprobante->puntoVenta.'-'.$comprobante->numero }}</td>
                    <td>{{ $comprobante->fecha->format('d/m/Y') }}</td>
                    <td>{{ $comprobante->cliente }}</td>
                    <td>{{ ($comprobante->cliente) ? $comprobante->cliente->dni : '' }}</td>
                    @foreach($porcentajes as $item)
                        @if($comprobante['table'] == 'ventas')
                            <td class="success">{{ $importes['venta'.$comprobante->id]['neto'][$item->porcentaje_iva] }}</td>
                        @else
                            <td class="danger">{{ $importes['notacredito'.$comprobante->id]['neto'][$item->porcentaje_iva] }}</td>
                        @endif
                    @endforeach
                    @foreach($porcentajes as $item)
                        @if($comprobante['table'] == 'ventas')
                            <td class="success">{{ $importes['venta'.$comprobante->id]['iva'][$item->porcentaje_iva] }}</td>
                        @else
                            <td class="danger">{{ $importes['notacredito'.$comprobante->id]['iva'][$item->porcentaje_iva] }}</td>
                        @endif
                    @endforeach
                    <td class="success">{{ $comprobante->importe_impuesto_interno }}</td>
                    <td class="success">{{ $comprobante->total }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <hr><br>
    <table align="center" class="table table-condensed table-striped table-bordered" style="width:50%">
        @foreach($porcentajes as $item)
            <tr>
                <td style="width:50%;">Total Neto {{ $porcentajes_iva[$item->porcentaje_iva] }}% :</td>
                <td style="width:50%;">{{$importes['sumatoriaNeto'][$item->porcentaje_iva]}}</td>
            </tr>
        @endforeach
        @foreach($porcentajes as $item)
            <tr>
                <td style="width:50%;">IVA {{ $porcentajes_iva[$item->porcentaje_iva] }}% :</td>
                <td style="width:50%;">{{$importes['sumatoriaIva'][$item->porcentaje_iva]}}</td>
            </tr>
        @endforeach
        <tr style="font-weight:bold;">
            <td style="width:50%;">Total:</td>
            <td style="width:50%;">{{$importes['sumatoriaTotal']}}</td>
        </tr>
    </table>
    <script>
        window.print();
    </script>
    </body>
</html>
