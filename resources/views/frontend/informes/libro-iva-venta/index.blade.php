@extends('frontend.layouts.app')

@section ('title', trans('labels.frontend.libroivaventa_titulo')." - Listar")

@section('after-styles')
    {{ Html::style("css/frontend/plugins/jquery/jquery-ui.min.css") }}

    <style>
        .ui-datepicker-calendar {
            display: none;
        }
        button.ui-datepicker-current { display: none; }
    </style>
@endsection

@section('content')
    <div class="row page-titles">
        <div class="col-md-12 col-12 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('labels.frontend.libroivaventa_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item active">{{ trans('labels.frontend.libroivaventa_titulo') }}</li>
            </ol>
        </div>
    </div>

    @tips($_SERVER['REQUEST_URI'])

    <div class="card">
        <div class="card-header">
            <div class="card-actions">
                <a class="btn-minimize mytooltip" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
            </div>
            <h4 class="card-title m-b-0">Listar Libro de Iva Venta</h4>
        </div>
        <div class="card-body">
            @include('includes.partials.messages')
            {!! Form::open([
                'method'=>'POST',
                'class' => 'form-horizontal',
                'target' => '_blank',
                'action' => ['Frontend\Informes\LibroIvaVentaController@imprimir']
            ]) !!}
            <div class="row form-group justify-content-md-center">
                <div class="col-md-6">
                    {!! Form::label('periodo', 'Mes / Año') !!}
                    {!! Form::text('periodo', date('m/Y'), ['class' => 'form-control fecha']) !!}
                </div>
            </div>
            <hr>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-12">
                        {{ Form::button('<i class="ace-icon fa fa-print bigger-110"></i> Imprimir', array('type' => 'submit', 'class' => 'btn btn-lg btn-primary pull-right')) }}
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>

@endsection

@section('after-scripts')
    {{ Html::script("js/frontend/plugins/jquery/jquery-ui.min.js") }}
    {{ Html::script("js/frontend/plugins/jquery/datepicker-es.js") }}
    {{ Html::script("js/frontend/plugins/maskedinput/jquery.maskedinput.min.js") }}
    <script>
        $( document ).ready(function() {
            $('input.fecha').datepicker({
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,
                closeText : "Aceptar",
                dateFormat: 'mm/yy',
                onClose: function(dateText, inst) {
                  $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
                }
            }).mask("99/9999", {placeholder: "mm/aaaa"});
        });
    </script>
@endsection
