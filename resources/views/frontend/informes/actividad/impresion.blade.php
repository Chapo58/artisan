<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Informe por Actividad</title>

        {{ Html::style('css/frontend/plugins/bootstrap/css/bootstrap.min.css') }}

        <style>
            @media print{@page {size: landscape}}
            @media print {
                .table th {
                    background-color: #D9EDF7 !important;
                }
            }
        </style>

    </head>

    <body>

      <table style="width:100%">
        <tr>
          <td><img class="img-responsive" style="max-width: 200px; max-height: 80px;" src="{{$empresa->logo()}}"/></td>
          <td><h1 style="color:#5692CB;">Informe por Actividad</h1></td>
        </tr>
      </table>

      <hr>

      <table class="table table-condensed table-bordered" style="width:100%">
        <tr>
          <td>Fecha: {{ $periodo }}</td>
        </tr>
      </table>

      @foreach($actividades as $actividad)

          <table class="table table-condensed table-bordered" style="width:100%">
              <tr>
                  <td>Actividad: {{ $actividad }}</td>
              </tr>
          </table>
          <table class="table table-condensed table-striped table-bordered" style="width:100%">
              <thead>
                  <tr class="info">
                      <th>Condición de IVA</th>
                      @foreach($porcentajes as $item)
                          <th>Neto {{ $porcentajes_iva[$item->porcentaje_iva] }} %</th>
                      @endforeach
                      @foreach($porcentajes as $item)
                          <th>Iva {{ $porcentajes_iva[$item->porcentaje_iva] }} %</th>
                      @endforeach
                      <th>Imp. Int</th>
                      <th>Total</th>
                  </tr>
              </thead>
              <tbody>
                  @foreach($condicionesIva as $condicion)
                      <tr>
                          <td>{{$condicion}}</td>
                          @foreach($porcentajes as $item)
                              <td>
                                  {{number_format($importes[$actividad->id][$condicion->id]['neto'.$item->porcentaje_iva], 2, ',', '.')}}
                              </td>
                          @endforeach
                          @foreach($porcentajes as $item)
                              <td>
                                  {{number_format($importes[$actividad->id][$condicion->id][$item->porcentaje_iva], 2, ',', '.')}}
                              </td>
                          @endforeach
                          <td>
                              {{number_format($importes[$actividad->id][$condicion->id]['impInt'], 2, ',', '.')}}
                          </td>
                          <td>
                              {{number_format($importes[$actividad->id][$condicion->id]['total'], 2, ',', '.')}}
                          </td>
                      </tr>
                  @endforeach
              </tbody>
          </table>

      @endforeach

      <script>
        window.print();
      </script>

    </body>

</html>
