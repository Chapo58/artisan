@extends('frontend.layouts.app')

@section ('title', trans('labels.frontend.tipotela_titulo')." - ".trans('buttons.general.crud.create'))

@section('content')
    <div class="row page-titles">
        <div class="col-md-8 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('labels.frontend.tipotela_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item"><a href="{{url('/produccion/tipo-tela')}}">{{ trans('labels.frontend.tipotela_titulo') }}</a></li>
                <li class="breadcrumb-item active">{{ trans('buttons.general.crud.create') }} {{ trans('labels.frontend.tipotela') }}</li>
            </ol>
        </div>
        <div class="col-md-4">
            <div class="pull-right">
                <a href="{{ url('/produccion/tipo-tela') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i> {{ trans('buttons.general.crud.back') }}
                </a>
            </div>
        </div>
    </div>

    @include('includes.partials.messages')

    {!! Form::open(['url' => '/produccion/tipo-tela', 'class' => 'form-horizontal', 'files' => true]) !!}
        @include ('frontend.produccion.tipo-tela.form')
    {!! Form::close() !!}

@endsection
