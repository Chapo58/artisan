<div class="card">
  <div class="card-body">
    <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
    <br>
    <div class="row form-group">
      <div class="col-md-6">
        {!! Form::label('razon_social', 'Razon Social') !!}
        {!! Form::text('razon_social', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('razon_social', '<p class="text-danger">:message</p>') !!}
      </div>
      <div class="col-md-6">
        {!! Form::label('cuit', 'Cuit') !!}
        <div class="input-group">
            <div class="input-group-addon">
                <i class="fa fa-hashtag"></i>
            </div>
            {!! Form::text('cuit', null, ['class' => 'form-control', 'required' => 'required']) !!}
        </div>
        {!! $errors->first('cuit', '<p class="text-danger">:message</p>') !!}
      </div>
    </div>
    <div class="row form-group">
      <div class="col-md-6">
        {!! Form::label('condicion_iva_id', 'Condición Iva') !!}
        {!! Form::select('condicion_iva_id', $condiciones_iva, isset($taller->condicionIva) ? $taller->condicionIva->id : '', array('class' => 'form-control')) !!}
        {!! $errors->first('condicion_iva_id', '<p class="text-danger">:message</p>') !!}
      </div>
      <div class="col-md-6">
        {!! Form::label('email', 'Email') !!}
        {!! Form::email('email', null, ['class' => 'form-control']) !!}
        {!! $errors->first('email', '<p class="text-danger">:message</p>') !!}
      </div>
    </div>
    <div class="row form-group">
      <div class="col-md-6">
        {!! Form::label('telefono', 'Teléfono') !!}
        <div class="input-group">
            <div class="input-group-addon">
                <i class="fa  fa-phone"></i>
            </div>
            {!! Form::text('telefono', null, ['class' => 'form-control telefono']) !!}
        </div>
        {!! $errors->first('telefono', '<p class="text-danger">:message</p>') !!}
      </div>
    </div>
    <h4 class="font-medium m-t-30">Datos Adicionales</h4>
    <hr>
    <div class="row form-group">
        <div class="col-md-6">
            {!! Form::label('direccion', 'Dirección') !!}
            {!! Form::text('domicilio[direccion]', null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! $errors->first('direccion', '<p class="text-danger">:message</p>') !!}
        </div>
        <div class="col-md-6">
            {!! Form::label('localidad_id', 'Localidad') !!}
            <div class="row">
                <div class="col-md-10">
                    {!! Form::select('domicilio[localidad_id]', $localidades, isset($taller->domicilio->localidad) ? $taller->domicilio->localidad->id : '', ['id' => 'localidad_id', 'class' => 'form-control', 'required' => 'required']) !!}
                </div>
                <div class="col-md-1">
                    <button type="button" class="btn btn-secondary" id="agregar-localidad"><i class="fa fa-plus"></i> </button>
                </div>
            </div>
            {!! $errors->first('localidad_id', '<p class="text-danger">:message</p>') !!}
        </div>
    </div>
    <div class="row form-group">
        <div class="col-md-6">
            {!! Form::label('codigo_postal', 'Código Postal') !!}
            {!! Form::text('domicilio[codigo_postal]', isset($taller->domicilio->codigo_postal) ? $taller->domicilio->domicilio->codigo_postal : '', ['class' => 'form-control','id' => 'codigo_postal']) !!}
            {!! $errors->first('codigo_postal', '<p class="text-danger">:message</p>') !!}
        </div>
        <div class="col-md-6">
            {!! Form::label('provincia_id', 'Provincia') !!}
            <div class="row">
                <div class="col-md-10">
                    {!! Form::select('domicilio[provincia_id]', $provincias, isset($taller->domicilio->provincia) ? $taller->domicilio->provincia->id : '', ['id' => 'provincia_id', 'class' => 'form-control', 'required' => 'required']) !!}
                </div>
                <div class="col-md-1">
                    <button type="button" class="btn btn-secondary" id="agregar-provincia"><i class="fa fa-plus"></i> </button>
                </div>
            </div>
            {!! $errors->first('provincia_id', '<p class="text-danger">:message</p>') !!}
        </div>
    </div>
    <hr>
    <div class="form-actions">
      <div class="row">
        <div class="col-md-12">
          {{ Form::submit(isset($submitButtonText) ? $submitButtonText : trans('buttons.general.save'), ['class' => 'btn btn-lg btn-themecolor pull-right']) }}
        </div>
      </div>
    </div>
  </div>
</div>

@include('backend.configuraciones.provincia.provincia_dialog')
@include('backend.configuraciones.localidad.localidad_dialog')