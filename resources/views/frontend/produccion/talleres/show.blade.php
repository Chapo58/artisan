@extends('frontend.layouts.app')

@section ('title', trans('labels.frontend.talleres_titulo')." - ".trans('buttons.general.crud.show'))

@section('content')
    <div class="row page-titles">
        <div class="col-md-8 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('labels.frontend.talleres_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item"><a href="{{url('/produccion/talleres')}}">{{ trans('labels.frontend.talleres_titulo') }}</a></li>
                <li class="breadcrumb-item">{{ trans('buttons.general.crud.show') }} {{ trans('labels.frontend.talleres') }}</li>
                <li class="breadcrumb-item active">{{ $taller }}</li>
            </ol>
        </div>
        <div class="col-md-4">
            <div class="pull-right">
                <a href="{{ url('/produccion/talleres') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i> {{ trans('buttons.general.crud.back') }}
                </a>
                <a href="{{ url('/produccion/talleres/' . $taller->id . '/edit') }}" title="{{ trans('buttons.general.crud.edit') }}" class="btn btn-info">
                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i> {{ trans('buttons.general.crud.edit') }}
                </a>
                <a href="javascript:void(0)" title="{{trans("buttons.general.crud.delete")." ".trans("labels.frontend.talleres")}}" class="btn btn-danger delete">
                    <i class="fa fa-trash" aria-hidden="true"></i> {{ trans("buttons.general.crud.delete") }}
                </a>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
            <br>
            <div class="table-responsive">
                <table class="table table-borderless">
                    <tbody>
                        <tr>
                            <th> Condicion Iva </th>
                            <td> {{ $taller->condicion_iva_id }} </td>
                        </tr>
                        <tr>
                            <th> Domicilio </th>
                            <td> {{ $taller->domicilio_id }} </td>
                        </tr>
                        <tr><th> Razon Social </th>
                            <td> {{ $taller->razon_social }} </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection

@section('after-scripts')
    <script>
        $("body").on("click", ".delete", function () {
            swal({
                title: "{{trans('buttons.general.crud.delete').' '.trans('labels.frontend.talleres')}}",
                text: "¿Realmente desea eliminar este registro?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#ff8726',
                confirmButtonText: "Eliminar"
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        type: 'POST',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: '{{ url('/produccion/talleres/'.$taller->id) }}',
                        data: {"_method" : 'DELETE'},
                        success: function (msg) {
                            swal("Eliminado!", "El registro ha sido correctamente eliminado.", "success").then((result) => { window.location.href = '{{url('/produccion/talleres')}}'; });
                        }
                    });
                }
            });
        });
    </script>
@endsection