@extends ('frontend.layouts.app')

@section ('title', trans('labels.frontend.talleres_titulo')." - ".trans('navs.frontend.user.administration'))

@section('content')
    <div class="row page-titles">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('labels.frontend.talleres_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item active">{{ trans('labels.frontend.talleres_titulo') }}</li>
            </ol>
        </div>
        <div class="col-md-6">
            <div class="pull-right">
                <a href="{{ url('/produccion/talleres/create') }}" class="btn btn-themecolor btn-lg" title="{{ trans('buttons.general.crud.create').' '.trans('labels.frontend.talleres')  }}">
                    <i class="fa fa-plus" aria-hidden="true"></i> {{ trans('buttons.general.crud.create').' '.trans('labels.frontend.talleres') }}
                </a>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
            @include('includes.partials.messages')
            <div class="table-responsive">
                <table id="talleres-table" class="table table-striped table-hover table-sm">
                    <thead>
                    <tr>
                        <th>Razon Social</th>
                        <th>Condicion Iva</th>
                        <th>Cuit</th>
                        <th>Email</th>
                        <th>Telefono</th>
                        <th>{{ trans('labels.general.actions') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($talleres as $item)
                        <tr id="{{$item->id}}">
                            <td>{{ $item->razon_social }}</td>
                            <td>{{ $item->condicionIva }}</td>
                            <td>{{ $item->cuit }}</td>
                            <td>{{ $item->email }}</td>
                            <td>{{ $item->telefono }}</td>
                            <td>
                                <a href="{{ url('/produccion/talleres/' . $item->id) }}" data-toggle="tooltip" data-original-title="{{ trans('buttons.general.crud.show') }} {{ trans('labels.frontend.talleres') }}">
                                    <i class="fa fa-eye text-success m-r-10"></i>
                                </a>
                                <a href="{{ url('/produccion/talleres/' . $item->id . '/edit') }}" data-toggle="tooltip" data-original-title="{{ trans('buttons.general.crud.edit') }} {{ trans('labels.frontend.talleres') }}">
                                    <i class="fa fa-pencil text-info m-r-10"></i>
                                </a>
                                <a href="javascript:void(0)" class="delete" data-id="{{$item->id}}" data-toggle="tooltip" data-original-title="{{trans("buttons.general.crud.delete")." ".trans("labels.frontend.talleres")}}">
                                    <i class="fa fa-close text-danger"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection

@section('after-scripts')
    {{ Html::script("js/frontend/plugins/datatables/jquery.dataTables.min.js") }}

<script>
    $(document).ready(function() {
        $('#talleres-table').DataTable();
    } );

    $("body").on("click", ".delete", function () {
        var id = $(this).attr("data-id");
        swal({
            title: "{{trans('buttons.general.crud.delete').' '.trans('labels.frontend.talleres')}}",
            text: "¿Realmente desea eliminar este registro?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#ff8726',
            confirmButtonText: "Eliminar"
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/produccion/talleres/'+id,
                    data: {"_method" : 'DELETE'},
                    success: function (msg) {
                        $("#" + id).hide(1);
                        swal("Eliminado!", "El registro ha sido correctamente eliminado.", "success");
                    }
                });
            }
        });
    });
</script>

@endsection