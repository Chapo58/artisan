@extends('frontend.layouts.app')

@section ('title', trans('labels.frontend.planillacorte_titulo')." - ".trans('buttons.general.crud.create'))

@section('after-styles')
    {{ Html::style("css/frontend/plugins/jquery/jquery-ui.min.css") }}
@endsection

@section('content')
    <div class="row page-titles">
        <div class="col-md-8 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('labels.frontend.planillacorte_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item"><a href="{{url('/produccion/planilla-corte')}}">{{ trans('labels.frontend.planillacorte_titulo') }}</a></li>
                <li class="breadcrumb-item active">{{ trans('buttons.general.crud.create') }} {{ trans('labels.frontend.planillacorte') }}</li>
            </ol>
        </div>
        <div class="col-md-4">
            <div class="pull-right">
                <a href="{{ url('/produccion/planilla-corte') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i> {{ trans('buttons.general.crud.back') }}
                </a>
            </div>
        </div>
    </div>

    @include('includes.partials.messages')

    {!! Form::open(['url' => '/produccion/planilla-corte', 'class' => 'form-horizontal', 'files' => true]) !!}
        @include ('frontend.produccion.planilla-corte.form')
    {!! Form::close() !!}

@endsection

@section('after-scripts')
    {{ Html::script("js/frontend/plugins/jquery/jquery-ui.min.js") }}
    {{ Html::script("js/frontend/plugins/jquery/datepicker-es.js") }}
    {{ Html::script("js/frontend/plugins/maskedinput/jquery.maskedinput.min.js") }}

    <script>
        $( document ).ready(function() {
            $('input.fecha').datepicker({
                firstDay: 0,
                dateFormat: 'dd/mm/yy'
            }).mask("99/99/9999", {placeholder: "dd/mm/aaaa"});

            $(document).on("keydown.autocomplete", ".buscar-rollo", function (e) {
                if (e.keyCode == 13) {
                    e.preventDefault();
                }else{
                    $(this).autocomplete({
                        source: '{{ url('/produccion/rollo/obtenerRollo') }}',
                        minlength: 1,
                        autoFocus: true,
                        select: function (e, ui) {
                            inputNombre = this.attributes['name'].value;

                            $('.buscar-rollo[name="' + inputNombre + '"]').parent().find('input.rollo-id').val(ui.item.id);
                            $('.buscar-rollo[name="' + inputNombre + '"]').parent().parent().find('td input.color').val(ui.item.color);
                            $('.buscar-rollo[name="' + inputNombre + '"]').parent().parent().find('td input.tela').val(ui.item.tela);
                            $('.buscar-rollo[name="' + inputNombre + '"]').parent().parent().find('td input.peso').val(ui.item.peso);
                        }
                    });
                }
            });

            $(document).on('click', '.borrar-detalle', function () {
                inputNombre = this.attributes['name'].value;

                $('.borrar-detalle[name="' + inputNombre + '"]').parent().parent().hide("slow");
                $('.borrar-detalle[name="' + inputNombre + '"]').parent().find('input.estado').val(0);
            });

            calcularSubtotales();
        });

        $("#agregar-detalle").click(function(){
            agregarDetalle();
        });

        $("#agregar-detalle-de-codigo").on('focusin', function(){
            agregarDetalle('cantidad');
        });

        function agregarDetalle() {
            var numero_orden = $('table#tabla-detalles-cortes tbody tr').length+ 1;
            $("#tabla-detalles-cortes").append('<tr>' +
                '<td>' +
                    '<input class="form-control buscar-rollo" name="detalles['+(numero_orden*-1)+'][rollo]"  placeholder="Buscar..." type="text">' +
                    '<input type="hidden" class="rollo-id" name="detalles['+(numero_orden*-1)+'][rollo_id]" value="">' +
                    '<input type="hidden" class="id" name="detalles['+(numero_orden*-1)+'][id]" value="'+(numero_orden*-1)+'">' +
                '</td>' +
                '<td>' +
                    '<input class="form-control color" name="detalles['+(numero_orden*-1)+'][color]" readonly type="text">' +
                '</td>' +
                '<td>' +
                    '<input class="form-control tela" name="detalles['+(numero_orden*-1)+'][tela]" readonly type="text">' +
                '</td>' +
                '<td>' +
                    '<input class="form-control peso" name="detalles['+(numero_orden*-1)+'][peso]" readonly type="number" step="0.01" placeholder="kg.">' +
                '</td>' +
                '<td>' +
                    '<input class="form-control tiradas" name="detalles['+(numero_orden*-1)+'][tiradas]" type="number" step="0.01" placeholder="N°">' +
                '</td>' +
                '<td>' +
                    '<input class="form-control" name="detalles['+(numero_orden*-1)+'][restos]" type="number" step="0.01" placeholder="N°">' +
                '</td>' +
                '<td>' +
                    '<input class="form-control prendas" name="detalles['+(numero_orden*-1)+'][prendas]" type="number" step="0.01" placeholder="N°">' +
                '</td>' +
                '<td>' +
                    '<a class="btn btn-danger btn-circle borrar-detalle" name="detalles['+(numero_orden*-1)+'][eliminar]" tabindex="99999">' +
                        '<i class="fa fa-trash-o" aria-hidden="true"></i>' +
                    '</a>' +
                    '<input type="hidden" class="estado" name="detalles['+(numero_orden*-1)+'][estado]" value="1">' +
                '</td>' +
            '</tr>');
            $("input[name='detalles["+(numero_orden*-1)+"][rollo]']").focus();
            calcularSubtotales();
        }

        function calcularSubtotales(){
            var $tablaTodasFilas = $("#tabla-detalles-cortes tbody tr");
            $tablaTodasFilas.each(function (index) {
                var $tablaFila = $(this);

                $tablaFila.find('.tiradas, .buscar-rollo').on('change', function () {
                    var tiradas = $tablaFila.find('.tiradas').val();

                    var total = 0;
                    $('.matriz').each(function() {
                        if($(this).val() != ''){
                            total += parseFloat($(this).val());
                            console.log('total'+total);
                            console.log('val'+$(this).val());
                        }
                    });
                    var prendas = total * tiradas;

                    if (!isNaN(prendas)) {
                        $tablaFila.find('.prendas').val(prendas.toFixed(2));
                    }
                });
            });
        }
  </script>

@endsection