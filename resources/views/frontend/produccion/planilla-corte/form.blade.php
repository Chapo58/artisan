<div class="card">
    <div class="card-header">
        <div class="card-actions">
            <a class="" data-action="collapse"><i class="ti-minus"></i></a>
            <a class="btn-minimize" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
        </div>
        <h4 class="card-title m-b-0">
            Información de la Planilla
        </h4>
    </div>
    <div class="card-body collapse show b-t">
        <div class="row form-group">
            <div class="col-md-6">
                {!! Form::label('numero', 'Numero') !!}
                {!! Form::number('numero', null, ['class' => 'form-control', 'required' => 'required']) !!}
                {!! $errors->first('numero', '<p class="text-danger">:message</p>') !!}
            </div>
            <div class="col-md-6">
                {!! Form::label('empresa', 'Empresa') !!}
                {!! Form::text('empresa', null, ['class' => 'form-control']) !!}
                {!! $errors->first('empresa', '<p class="text-danger">:message</p>') !!}
            </div>
        </div>
        <div class="row form-group">
            <div class="col-md-6">
                {!! Form::label('fecha', 'Fecha') !!}
                {!! Form::text('fecha', isset($planillaCorte->fecha) ? $planillaCorte->fecha->format('d/m/Y') : $fecha_hoy, ['class' => 'form-control fecha', 'required' => 'required']) !!}
                {!! $errors->first('fecha', '<p class="text-danger">:message</p>') !!}
            </div>
            <div class="col-md-6">
                {!! Form::label('tipo_corte', 'Tipo Corte') !!}
                {!! Form::text('tipo_corte', null, ['class' => 'form-control']) !!}
                {!! $errors->first('tipo_corte', '<p class="text-danger">:message</p>') !!}
            </div>
        </div>
    </div>
</div>

<div class="card">
    <div class="card-header">
        <div class="card-actions">
            <a class="" data-action="collapse"><i class="ti-minus"></i></a>
            <a class="btn-minimize" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
        </div>
        <h4 class="card-title m-b-0">
            Matriz
        </h4>
    </div>
    <div class="card-body collapse show b-t">
        <div class="table-responsive">
            <table class="table color-bordered-table inverse-bordered-table">
                <thead>
                    <tr>
                        <th><center><strong>14</strong></center></th>
                        <th><center><strong>16</strong></center></th>
                        <th><center><strong>S</strong></center></th>
                        <th><center><strong>M</strong></center></th>
                        <th><center><strong>L</strong></center></th>
                        <th><center><strong>XL</strong></center></th>
                        <th><center><strong>XXL</strong></center></th>
                        <th><center><strong>XXXL</strong></center></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{!! Form::number('talle14', null, ['class' => 'form-control matriz']) !!}</td>
                        <td>{!! Form::number('talle16', null, ['class' => 'form-control matriz']) !!}</td>
                        <td>{!! Form::number('talleS', null, ['class' => 'form-control matriz']) !!}</td>
                        <td>{!! Form::number('talleM', null, ['class' => 'form-control matriz']) !!}</td>
                        <td>{!! Form::number('talleL', null, ['class' => 'form-control matriz']) !!}</td>
                        <td>{!! Form::number('talleXL', null, ['class' => 'form-control matriz']) !!}</td>
                        <td>{!! Form::number('talleXXL', null, ['class' => 'form-control matriz']) !!}</td>
                        <td>{!! Form::number('talleXXXL', null, ['class' => 'form-control matriz']) !!}</td>
                    </tr>
                </tbody>
            </table>

            <table class="table color-bordered-table purple-bordered-table">
                <thead>
                    <tr>
                        <th><center><strong>0</strong></center></th>
                        <th><center><strong>2</strong></center></th>
                        <th><center><strong>4</strong></center></th>
                        <th><center><strong>6</strong></center></th>
                        <th><center><strong>8</strong></center></th>
                        <th><center><strong>10</strong></center></th>
                        <th><center><strong>12</strong></center></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{!! Form::number('talle0', null, ['class' => 'form-control matriz']) !!}</td>
                        <td>{!! Form::number('talle2', null, ['class' => 'form-control matriz']) !!}</td>
                        <td>{!! Form::number('talle4', null, ['class' => 'form-control matriz']) !!}</td>
                        <td>{!! Form::number('talle6', null, ['class' => 'form-control matriz']) !!}</td>
                        <td>{!! Form::number('talle8', null, ['class' => 'form-control matriz']) !!}</td>
                        <td>{!! Form::number('talle10', null, ['class' => 'form-control matriz']) !!}</td>
                        <td>{!! Form::number('talle12', null, ['class' => 'form-control matriz']) !!}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="card">
    <div class="card-header">
        <div class="card-actions">
            <a class="" data-action="collapse"><i class="ti-minus"></i></a>
            <a class="btn-minimize" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
        </div>
        <h4 class="card-title m-b-0">
            Detalle de la Planilla
        </h4>
    </div>
    <div class="card-body collapse show b-t">
        <div class="table-responsive">
            <table id="tabla-detalles-cortes" class="table color-table info-table form-material">
                <thead>
                <tr>
                    <th style="width:20%;">Rollo</th>
                    <th style="width:15%;">Color</th>
                    <th style="width:15%;">Tela</th>
                    <th style="width:10%;">Peso</th>
                    <th style="width:10%;">Tiradas</th>
                    <th style="width:10%;">Restos</th>
                    <th style="width:10%;">Prendas</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @if (isset($planillaCorte))
                    @foreach($planillaCorte->detalles as $detalle)
                        <tr>
                            <td>
                                {!! Form::text('detalles['.$detalle->id.'][rollo]', $detalle->rollo, ['class' => 'form-control', 'readonly']) !!}
                                {!! Form::hidden('detalles['.$detalle->id.'][rollo_id]', $detalle->rollo_id, ['class' => 'form-control rollo-id', 'readonly']) !!}
                                {!! Form::hidden('detalles['.$detalle->id.'][id]', $detalle->id, ['class' => 'form-control id', 'readonly']) !!}
                            </td>
                            <td>
                                {!! Form::text('detalles['.$detalle->id.'][color]', $detalle->color, ['class' => 'form-control', 'readonly']) !!}
                            </td>
                            <td>
                                {!! Form::text('detalles['.$detalle->id.'][tela]', $detalle->tela, ['class' => 'form-control', 'readonly']) !!}
                            </td>
                            <td>
                                {!! Form::text('detalles['.$detalle->id.'][peso]', $detalle->peso, ['class' => 'form-control', 'readonly', 'placeholder' => 'kg.']) !!}
                            </td>
                            <td>
                                {!! Form::text('detalles['.$detalle->id.'][tiradas]', $detalle->tiradas, ['class' => 'form-control', 'readonly', 'placeholder' => 'N°']) !!}
                            </td>
                            <td>
                                {!! Form::text('detalles['.$detalle->id.'][restos]', $detalle->restos, ['class' => 'form-control', 'readonly', 'placeholder' => 'N°']) !!}
                            </td>
                            <td>
                                {!! Form::text('detalles['.$detalle->id.'][prendas]', $detalle->prendas, ['class' => 'form-control', 'readonly', 'placeholder' => 'N°']) !!}
                            </td>
                            <td>
                                <button type="button" class="btn btn-danger btn-circle" disabled="">
                                    <i class="fa fa-trash-o" aria-hidden="true"></i>
                                </button>
                                {!! Form::hidden('detalles['.$detalle->id.'][estado]', 1, ['class' => 'form-control estado', 'readonly']) !!}
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
                <tfoot>
                    <tr>
                        <td>{!! Form::text('', null, ['id' => 'agregar-detalle-de-codigo','class' => 'form-control', 'tabindex' => '9999']) !!}</td>
                        <td>{!! Form::text('', null, ['class' => 'form-control', 'readonly', 'tabindex' => '9999']) !!}</td>
                        <td>{!! Form::text('', null, ['class' => 'form-control', 'readonly', 'tabindex' => '9999']) !!}</td>
                        <td>
                            {!! Form::number('', null, ['class' => 'form-control', 'readonly', 'tabindex' => '9999', 'placeholder' => 'kg.']) !!}
                        </td>
                        <td>
                            {!! Form::number('', null, ['class' => 'form-control', 'readonly', 'tabindex' => '9999', 'placeholder' => 'N°']) !!}
                        </td>
                        <td>
                            {!! Form::number('', null, ['class' => 'form-control', 'readonly', 'tabindex' => '9999', 'placeholder' => 'N°']) !!}
                        </td>
                        <td>
                            {!! Form::number('', null, ['class' => 'form-control', 'readonly', 'tabindex' => '9999', 'placeholder' => 'N°']) !!}
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <th colspan="10">
                            <a href="javascript:void(0)" class="btn btn-info" id="agregar-detalle">
                                <i class="fa fa-plus" aria-hidden="true"></i> Agregar Rollo
                            </a>
                        </th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>

<div class="card">
    <div class="card-body">
        <div class="form-group">
            {{ Form::submit(isset($submitButtonText) ? $submitButtonText : trans('buttons.general.save'), ['class' => 'btn btn-themecolor btn-lg pull-right']) }}
        </div>
    </div>
</div>
