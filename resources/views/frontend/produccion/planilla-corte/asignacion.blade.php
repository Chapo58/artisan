@extends('frontend.layouts.app')

@section ('title', trans('labels.frontend.planillacorte_titulo')." - Asignación")

@section('content')
    <div class="row page-titles">
        <div class="col-md-8 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('labels.frontend.planillacorte_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item"><a href="{{url('/produccion/planilla-corte')}}">{{ trans('labels.frontend.planillacorte_titulo') }}</a></li>
                <li class="breadcrumb-item active">Asignar Taller</li>
            </ol>
        </div>
        <div class="col-md-4">
            <div class="pull-right">
                <a href="{{ url('/produccion/planilla-corte') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i> {{ trans('buttons.general.crud.back') }}
                </a>
            </div>
        </div>
    </div>

    {!! Form::open(['url' => '/produccion/planilla-corte', 'class' => 'form-horizontal', 'files' => true]) !!}

    <div class="card">
        <div class="card-body">
            <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
            <br>
            <div class="row form-group">
                <div class="col-md-6">
                    {!! Form::label('taller_id', 'Taller') !!}
                    {!! Form::select('taller_id', $talleres, null, array('class' => 'form-control', 'required' => 'required')) !!}
                    {!! $errors->first('taller_id', '<p class="text-danger">:message</p>') !!}
                </div>
            </div>

            <table class="table color-bordered-table inverse-bordered-table">
                <thead>
                <tr>
                    <th><center><strong>14</strong></center></th>
                    <th><center><strong>16</strong></center></th>
                    <th><center><strong>S</strong></center></th>
                    <th><center><strong>M</strong></center></th>
                    <th><center><strong>L</strong></center></th>
                    <th><center><strong>XL</strong></center></th>
                    <th><center><strong>XXL</strong></center></th>
                    <th><center><strong>XXXL</strong></center></th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>{!! Form::number('talle14', null, ['class' => 'form-control matriz']) !!}</td>
                    <td>{!! Form::number('talle16', null, ['class' => 'form-control matriz']) !!}</td>
                    <td>{!! Form::number('talleS', null, ['class' => 'form-control matriz']) !!}</td>
                    <td>{!! Form::number('talleM', null, ['class' => 'form-control matriz']) !!}</td>
                    <td>{!! Form::number('talleL', null, ['class' => 'form-control matriz']) !!}</td>
                    <td>{!! Form::number('talleXL', null, ['class' => 'form-control matriz']) !!}</td>
                    <td>{!! Form::number('talleXXL', null, ['class' => 'form-control matriz']) !!}</td>
                    <td>{!! Form::number('talleXXXL', null, ['class' => 'form-control matriz']) !!}</td>
                </tr>
                </tbody>
            </table>

            <table class="table color-bordered-table purple-bordered-table">
                <thead>
                <tr>
                    <th><center><strong>0</strong></center></th>
                    <th><center><strong>2</strong></center></th>
                    <th><center><strong>4</strong></center></th>
                    <th><center><strong>6</strong></center></th>
                    <th><center><strong>8</strong></center></th>
                    <th><center><strong>10</strong></center></th>
                    <th><center><strong>12</strong></center></th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>{!! Form::number('talle0', null, ['class' => 'form-control matriz']) !!}</td>
                    <td>{!! Form::number('talle2', null, ['class' => 'form-control matriz']) !!}</td>
                    <td>{!! Form::number('talle4', null, ['class' => 'form-control matriz']) !!}</td>
                    <td>{!! Form::number('talle6', null, ['class' => 'form-control matriz']) !!}</td>
                    <td>{!! Form::number('talle8', null, ['class' => 'form-control matriz']) !!}</td>
                    <td>{!! Form::number('talle10', null, ['class' => 'form-control matriz']) !!}</td>
                    <td>{!! Form::number('talle12', null, ['class' => 'form-control matriz']) !!}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <div class="form-group">
                {{ Form::submit(isset($submitButtonText) ? $submitButtonText : trans('buttons.general.save'), ['class' => 'btn btn-themecolor btn-lg pull-right']) }}
            </div>
        </div>
    </div>

    {!! Form::close() !!}

@endsection
