@extends('frontend.layouts.app')

@section ('title', trans('labels.frontend.planillacorte_titulo')." - ".trans('buttons.general.crud.show'))

@section('content')
    <div class="row page-titles">
        <div class="col-md-8 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('labels.frontend.planillacorte_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item"><a href="{{url('/produccion/planilla-corte')}}">{{ trans('labels.frontend.planillacorte_titulo') }}</a></li>
                <li class="breadcrumb-item">{{ trans('buttons.general.crud.show') }} {{ trans('labels.frontend.planillacorte') }}</li>
                <li class="breadcrumb-item active">{{ $planillaCorte }}</li>
            </ol>
        </div>
        <div class="col-md-4">
            <div class="pull-right">
                <a href="{{ url('/produccion/planilla-corte') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i> {{ trans('buttons.general.crud.back') }}
                </a>
                <a href="{{ url('/produccion/planilla-corte/' . $planillaCorte->id . '/edit') }}" title="{{ trans('buttons.general.crud.edit') }}" class="btn btn-info">
                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i> {{ trans('buttons.general.crud.edit') }}
                </a>
                <a href="javascript:void(0)" title="{{trans("buttons.general.crud.delete")." ".trans("labels.frontend.planillacorte")}}" class="btn btn-danger delete">
                    <i class="fa fa-trash" aria-hidden="true"></i> {{ trans("buttons.general.crud.delete") }}
                </a>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            <div class="card-actions">
                <a class="" data-action="collapse"><i class="ti-minus"></i></a>
                <a class="btn-minimize" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
                <a class="btn-close" data-action="close"><i class="ti-close"></i></a>
            </div>
            <h4 class="card-title m-b-0">
                Información de la Planilla
            </h4>
        </div>
        <div class="card-body collapse show b-t">
            <div class="table-responsive">
                <table class="table table-borderless">
                    <tbody>
                        <tr>
                            <th> Numero </th><td> {{ $planillaCorte->numero }} </td>
                            <th> Fecha </th><td> {{ $planillaCorte->fecha->format('d/m/Y') }} </td>
                        </tr>
                        <tr>
                            <th> Empresa </th><td> {{ $planillaCorte->empresa }} </td>
                            <th> Tipo de Corte </th><td> {{ $planillaCorte->tipo_corte }} </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            <div class="card-actions">
                <a class="" data-action="collapse"><i class="ti-minus"></i></a>
                <a class="btn-minimize" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
                <a class="btn-close" data-action="close"><i class="ti-close"></i></a>
            </div>
            <h4 class="card-title m-b-0">
                Detalles de la Planilla
            </h4>
        </div>
        <div class="card-body collapse show b-t">
            <div class="table-responsive">
                <table class="table color-table muted-table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>Rollo</th>
                            <th>Color</th>
                            <th>Tela</th>
                            <th>Peso</th>
                            <th>Tiradas</th>
                            <th>Restos</th>
                            <th>Prendas</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($planillaCorte->detalles as $detalle)
                        <tr>
                            <td>{{ $detalle->rollo->codigo }}</td>
                            <td>{{ $detalle->color }}</td>
                            <td>{{ $detalle->tela }}</td>
                            <td>{{ number_format($detalle->peso, 2) }}</td>
                            <td>{{ $detalle->tiradas }}</td>
                            <td>{{ $detalle->restos }}</td>
                            <td>{{ $detalle->prendas }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('after-scripts')
    <script>
        $("body").on("click", ".delete", function () {
            swal({
                title: "{{trans('buttons.general.crud.delete').' '.trans('labels.frontend.planillacorte')}}",
                text: "¿Realmente desea eliminar este registro?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#ff8726',
                confirmButtonText: "Eliminar"
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        type: 'POST',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: '{{ url('/produccion/planilla-corte/'.$planillaCorte->id) }}',
                        data: {"_method" : 'DELETE'},
                        success: function (msg) {
                            swal("Eliminado!", "El registro ha sido correctamente eliminado.", "success").then((result) => { window.location.href = '{{url('/produccion/planilla-corte')}}'; });
                        }
                    });
                }
            });
        });
    </script>
@endsection
