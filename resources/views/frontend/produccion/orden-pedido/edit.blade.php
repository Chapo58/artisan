@extends('frontend.layouts.app')

@section ('title', trans('labels.frontend.ordenpedido_titulo')." - ".trans('buttons.general.crud.edit'))

@section('content')

<div class="row page-titles">
        <div class="col-md-8 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('labels.frontend.ordenpedido_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item"><a href="{{url('/produccion/orden-pedido')}}">{{ trans('labels.frontend.ordenpedido_titulo') }}</a></li>
                <li class="breadcrumb-item">{{ trans('buttons.general.crud.edit') }} {{ trans('labels.frontend.ordenpedido') }}</li>
                <li class="breadcrumb-item active">{{ $ordenpedido->nombre }}</li>
            </ol>
        </div>
        <div class="col-md-4">
            <div class="pull-right">
                <a href="{{ url('/produccion/orden-pedido') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i> {{ trans('buttons.general.crud.back') }}
                </a>
            </div>
        </div>
    </div>
@include('includes.partials.messages')

                    {!! Form::model($ordenpedido, [
                        'method' => 'PATCH',
                        'url' => ['/produccion/orden-pedido', $ordenpedido->id],
                        'class' => 'form-horizontal',
                        'files' => true
                    ]) !!}

                        @include ('frontend.produccion.orden-pedido.form', ['submitButtonText' => trans("buttons.general.crud.update")])

                    {!! Form::close() !!}
@endsection

@section('after-scripts')
    {{ Html::script("js/frontend/plugins/maskedinput/jquery.maskedinput.min.js") }}

@endsection
