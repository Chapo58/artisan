<h4 class="widget-title">Encabezado</h4>
<div class="card" >
    <div class="card-body">
        <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
        <br>
        <div class="row form-group">
                <div class="col-md-3">
                    {!! Form::label('cliente_id', 'Cliente') !!}
                    {!! Form::text('cliente_id', null, ['class' => 'form-control']) !!}
                            <!--Ver Boton-->
                            <a class="btn btn-themecolor btn-sm" id="agregar-cliente">
                                <i class="fa fa-plus" aria-hidden="true"></i> Nuevo
                            </a>
                        
                    {!! $errors->first('cliente_id', '<p class="text-danger">:message</p>') !!}
                </div>
                <div class="col-md-3">
                    {!! Form::label('numero_opm', 'Numero OPM') !!}
                    {!! Form::text('numero_opm', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('numero_opm', '<p class="text-danger">:message</p>') !!}
                </div>
                <div class="col-md-3">
                    {!! Form::label('fecha_pedido', 'Fecha Pedido') !!}
                    {!! Form::date('fecha_pedido', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('fecha_pedido', '<p class="text-danger">:message</p>') !!}
                </div>
                <div class="col-md-3">
                    {!! Form::label('fecha_entrega', 'Fecha Entrega') !!}
                    {!! Form::date('fecha_entrega', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('fecha_entrega', '<p class="text-danger">:message</p>') !!}
                </div>
        </div>
    </div>
</div>
<!--
<h4 class="widget-title">Imagenes</h4>
<div class="card" >
    <div class="card-body">
        <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
        <br>
        <div class="row form-group">
                <div class="col-md-4">
                    {!! Form::label('imagen1', 'Imagen') !!}
                    {!! Form::file('imagen1', array('class' => 'image')) !!}
                    {!! $errors->first('imagen1', '<p class="text-danger">:message</p>') !!}
                </div>
                <div class="col-md-4">
                    {!! Form::label('imagen2', 'Imagen') !!}
                    {!! Form::file('imagen2', array('class' => 'image')) !!}
                    {!! $errors->first('imagen2', '<p class="text-danger">:message</p>') !!}
                </div>
                <div class="col-md-4">
                    {!! Form::label('imagen3', 'Imagen') !!}
                    {!! Form::file('imagen3', array('class' => 'image')) !!}
                    {!! $errors->first('imagen3', '<p class="text-danger">:message</p>') !!}
                </div>

        </div>
    </div>
</div>-->
<h4 class="widget-title">Opciones</h4>
<div class="card" >
    <div class="card-body">
        <ul class="nav nav-tabs customtab" role="tablist">
            <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#imagenes" role="tab"><span class="hidden-sm-up"><i class="ti-image"></i></span> <span class="hidden-xs-down">Imagenes</span></a> </li>
            <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#detalle" role="tab"><span class="hidden-sm-up"><i class="ti-info"></i></span> <span class="hidden-xs-down">Detalle</span></a> </li>
            <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#curva" role="tab"><span class="hidden-sm-up"><i class="ti-package"></i></span> <span class="hidden-xs-down">Curva de Talles</span></a> </li>
            <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#observaciones" role="tab"><span class="hidden-sm-up"><i class="ti-money"></i></span> <span class="hidden-xs-down">Observaciones</span></a> </li>
                    
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="imagenes" role="tabpanel">
                <div class="p-20">
                            <div class="row form-group">
                                    <div class="col-md-4">
                                        {!! Form::label('imagen1', 'Imagen 1 ') !!}
                                        {!! Form::file('imagen1', array('class' => 'image')) !!}
                                        {!! $errors->first('imagen1', '<p class="text-danger">:message</p>') !!}
                                    </div>
                                    <div class="col-md-4">
                                        {!! Form::label('imagen2', 'Imagen 2') !!}
                                        {!! Form::file('imagen2', array('class' => 'image')) !!}
                                        {!! $errors->first('imagen2', '<p class="text-danger">:message</p>') !!}
                                    </div>
                                    <div class="col-md-4">
                                        {!! Form::label('imagen3', 'Imagen 3') !!}
                                        {!! Form::file('imagen3', array('class' => 'image')) !!}
                                        {!! $errors->first('imagen3', '<p class="text-danger">:message</p>') !!}
                                    </div>
                            </div>
                </div>
            </div>
            <div class="tab-pane p-20" id="detalle" role="tabpanel">
                    <h4 class="widget-title">Detalle</h4>
                            <div class="row form-group">
                                    <div class="col-md-4">
                                        {!! Form::label('tipo_corte_id', 'Prenda') !!}
                                        {!! Form::select('tipo_corte_id', $tipos_cortes, isset($ordenpedido->tipoCorte) ? $ordenpedido->tipoCorte->id : '', array('class' => 'form-control', 'required' => 'required')) !!} 
                                        {!! $errors->first('tipo_corte_id', '<p class="text-danger">:message</p>') !!}
                                    </div>
                                    <div class="col-md-4">
                                        {!! Form::label('tipo_tela_id', 'Tela') !!}
                                        {!! Form::select('tipo_tela_id', $tipos_telas, isset($ordenpedido->tipoTela) ? $ordenpedido->tipoTela->id : '', array('class' => 'form-control', 'required' => 'required')) !!}
                                        {!! $errors->first('tipo_tela_id', '<p class="text-danger">:message</p>') !!}
                                    </div>
                                    <div class="col-md-4">
                                        {!! Form::label('color_id', 'Color') !!}
                                        {!! Form::select('color_id', $colores, isset($ordenpedido->color) ? $ordenpedido->color->id : '', array('class' => 'form-control', 'required' => 'required')) !!}
                                        {!! $errors->first('color_id', '<p class="text-danger">:message</p>') !!}
                                    </div>
                                    <div class="col-md-4">
                                        {!! Form::label('prueba', 'Largo Mangas') !!}
                                        {!! Form::text('prueba', null, ['class' => 'form-control']) !!}
                                        {!! $errors->first('prueba', '<p class="text-danger">:message</p>') !!}
                                    </div>
                                    <div class="col-md-4">
                                        {!! Form::label('prueba', 'Diseño Mangas') !!}
                                        {!! Form::text('prueba', null, ['class' => 'form-control']) !!}
                                        {!! $errors->first('prueba', '<p class="text-danger">:message</p>') !!}
                                    </div>
                                    <div class="col-md-4">
                                        {!! Form::label('prueba', 'Cuello') !!}
                                        {!! Form::text('prueba', null, ['class' => 'form-control']) !!}
                                        {!! $errors->first('prueba', '<p class="text-danger">:message</p>') !!}
                                    </div>
                                    <div class="col-md-4">
                                        {!! Form::label('prueba', 'Puño') !!}
                                        {!! Form::text('prueba', null, ['class' => 'form-control']) !!}
                                        {!! $errors->first('prueba', '<p class="text-danger">:message</p>') !!}
                                    </div>
                                    <div class="col-md-4">
                                        {!! Form::label('cantidad_estampas', 'Cant. Estampas') !!}
                                        {!! Form::number('cantidad_estampas', null, ['class' => 'form-control']) !!}
                                        {!! $errors->first('cantidad_estampas', '<p class="text-danger">:message</p>') !!}
                                    </div>
                                    <div class="col-md-4">
                                        {!! Form::label('diseño', 'Diseño') !!}
                                        <div class="switch">
                                            <label>No
                                                {!! Form::checkbox('diseño', 'yes' ) !!}<span class="lever"></span>
                                            Si</label>
                                        </div><!--Es un Select-->
                                        {!! $errors->first('diseño', '<p class="text-danger">:message</p>') !!}
                                    </div>
                            </div>
            </div>
            <div class="tab-pane p-20" id="curva" role="tabpanel">
                    <h4 class="widget-title">Curva de Talles</h4>
                            <div class="row form-group">
                                    <div class="col-md-4">
                                        {!! Form::label('talle12', 'Talle 12') !!}
                                        {!! Form::number('talle12', null, ['class' => 'form-control']) !!}
                                        {!! $errors->first('talle12', '<p class="text-danger">:message</p>') !!}
                                    </div>
                                    <div class="col-md-4">
                                        {!! Form::label('talle14', 'Talle 14') !!}
                                        {!! Form::number('talle14', null, ['class' => 'form-control']) !!}
                                        {!! $errors->first('talle14', '<p class="text-danger">:message</p>') !!}
                                    </div>
                                    <div class="col-md-4">
                                        {!! Form::label('talle16', 'Talle 16') !!}
                                        {!! Form::number('talle16', null, ['class' => 'form-control']) !!}
                                        {!! $errors->first('talle16', '<p class="text-danger">:message</p>') !!}
                                    </div>
                                    <div class="col-md-4">
                                        {!! Form::label('talleS', 'Talle S') !!}
                                        {!! Form::number('talleS', null, ['class' => 'form-control']) !!}
                                        {!! $errors->first('talleS', '<p class="text-danger">:message</p>') !!}
                                    </div>
                                    <div class="col-md-4">
                                        {!! Form::label('talleM', 'Talle M') !!}
                                        {!! Form::number('talleM', null, ['class' => 'form-control']) !!}
                                        {!! $errors->first('talleM', '<p class="text-danger">:message</p>') !!}
                                    </div>
                                    <div class="col-md-4">
                                        {!! Form::label('talleL', 'Talle L') !!}
                                        {!! Form::number('talleL', null, ['class' => 'form-control']) !!}
                                        {!! $errors->first('talleL', '<p class="text-danger">:message</p>') !!}
                                    </div>
                                    <div class="col-md-4">
                                        {!! Form::label('talleXL', 'Talle XL') !!}
                                        {!! Form::number('talleXL', null, ['class' => 'form-control']) !!}
                                        {!! $errors->first('talleXL', '<p class="text-danger">:message</p>') !!}
                                    </div>
                                    <div class="col-md-4">
                                        {!! Form::label('talleXXL', 'Talle XXL') !!}
                                        {!! Form::number('talleXXL', null, ['class' => 'form-control']) !!}
                                        {!! $errors->first('talleXXL', '<p class="text-danger">:message</p>') !!}
                                    </div>
                                    <div class="col-md-4">
                                        {!! Form::label('talleXXXL', 'Talle XXXL') !!}
                                        {!! Form::number('talleXXXL', null, ['class' => 'form-control']) !!}
                                        {!! $errors->first('talleXXXL', '<p class="text-danger">:message</p>') !!}
                                    </div>
                                    <div class="col-md-4">
                                        {!! Form::label('total_unidades', 'Total Unidades') !!}
                                        {!! Form::number('total_unidades', null, ['class' => 'form-control']) !!}
                                        {!! $errors->first('total_unidades', '<p class="text-danger">:message</p>') !!}
                                    </div>
                            </div>
            </div>
            <div class="tab-pane p-20" id="observaciones" role="tabpanel">
                            <div class="row form-group">
                                    <div class="col-md-12">
                                        {!! Form::label('observaciones', 'Observaciones') !!}
                                        {!! Form::textarea('observaciones', null, ['class' => 'form-control']) !!}
                                        {!! $errors->first('observaciones', '<p class="text-danger">:message</p>') !!}
                                    </div>
                            </div>
            </div>
        </div>
    </div>
</div>

        <div class="form-actions">
            <div class="row">
                <div class="col-md-12">
                    {{ Form::submit(isset($submitButtonText) ? $submitButtonText : trans('buttons.general.save'), ['class' => 'btn btn-lg btn-themecolor pull-right']) }}
                </div>
                <div class="col-md-6"> </div>
            </div>
        </div>


@include('frontend.personas.cliente.cliente_dialog')


