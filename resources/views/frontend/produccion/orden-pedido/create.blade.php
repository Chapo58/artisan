@extends('frontend.layouts.app')

@section ('title', trans('labels.frontend.ordenpedido_titulo')." - ".trans('buttons.general.crud.create'))

@section('content')


    <div class="row page-titles">
        <div class="col-md-8 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('labels.frontend.ordenpedido_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item"><a href="{{url('/produccion/orden-pedido')}}">{{ trans('labels.frontend.ordenpedido_titulo') }}</a></li>
                <li class="breadcrumb-item active">{{ trans('buttons.general.crud.create') }} {{ trans('labels.frontend.ordenpedido') }}</li>
            </ol>
        </div>
        <div class="col-md-4">
            <div class="pull-right">
                <a href="{{ url('/produccion/orden-pedido') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i> {{ trans('buttons.general.crud.back') }}
                </a>
            </div>
        </div>
    </div>


@include('includes.partials.messages')

        {!! Form::open(['url' => '/produccion/orden-pedido', 'class' => 'form-horizontal', 'files' => true]) !!}
             @include ('frontend.produccion.orden-pedido.form')
        {!! Form::close() !!}
               
@endsection

@section('after-scripts')
    {{ Html::script("js/frontend/plugins/maskedinput/jquery.maskedinput.min.js") }}
    <script>
        $( document ).ready(function() {
            $('input.fecha').datepicker({
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,
                closeText : "Aceptar",
                dateFormat: 'mm/yy',
                onClose: function(dateText, inst) {
                  $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
                }
            }).mask("99/9999", {placeholder: "mm/aaaa"});
        });
    </script>
@endsection
