<h4 class="widget-title">Principal</h4>
<div class="card" >
    <div class="card-body">
        <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
        <br>
        <div class="row form-group">
                <div class="col-md-3">
                    {!! Form::label('nombre', 'Nombre') !!}
                    {!! Form::text('nombre', null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('nombre', '<p class="text-danger">:message</p>') !!}
                </div>
                <div class="col-md-3">
                    {!! Form::label('tipo_corte_id', 'Tipo de Corte') !!}
                    {!! Form::select('tipo_corte_id', $tipos_cortes, isset($tipoproducto->tipoCorte) ? $tipoproducto->tipoCorte->id : '', array('class' => 'form-control', 'required' => 'required')) !!}
                    {!! $errors->first('tipo_corte_id', '<p class="text-danger">:message</p>') !!}
                </div>
        </div>
    </div>
</div>
<div class="form-actions">
    <div class="row">
        <div class="col-md-12">
            {{ Form::submit(isset($submitButtonText) ? $submitButtonText : trans('buttons.general.save'), ['class' => 'btn btn-lg btn-themecolor pull-right']) }}
        </div>
    </div>
</div>

