<div class="card">
    <div class="card-header">
        <div class="card-actions">
            <a class="" data-action="collapse"><i class="ti-minus"></i></a>
            <a class="btn-minimize" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
        </div>
        <h4 class="card-title m-b-0">
            Información
        </h4>
    </div>
    <div class="card-body collapse show b-t">
        <div class="row form-group">
            <div class="col-md-4">
                {!! Form::label('compra_id', 'Compra') !!}
                {!! Form::select('compra_id', $compras_activas, '', ['class' => 'form-control', 'required' => 'required']) !!}
                {!! $errors->first('compra_id', '<p class="text-danger">:message</p>') !!}
            </div>
            <div class="col-md-4">
                {!! Form::label('datos_compra', 'Datos Compra') !!}
                {!! Form::text('datos_compra', null, ['id' => 'datos_compra', 'class' => 'form-control', 'readonly']) !!}
            </div>
            <div class="col-md-4">
                {!! Form::label('moneda_id', 'Moneda') !!}
                {!! Form::select('moneda_id', $monedas, isset($rollo->moneda) ? $rollo->moneda->id : '', ['class' => 'form-control', 'required' => 'required']) !!}
                {!! $errors->first('moneda_id', '<p class="text-danger">:message</p>') !!}
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="numeracion-rollo" name="numeracion-rollo" value="{{ $valor_numeracion_rollo }}">

<div class="card">
    <div class="card-header">
        <div class="card-actions">
            <a class="" data-action="collapse"><i class="ti-minus"></i></a>
            <a class="btn-minimize" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
        </div>
        <h4 class="card-title m-b-0">
            Detalle de la Venta
        </h4>
    </div>
    <div class="card-body collapse show b-t">
        <div class="table-responsive">
            <table id="tabla-detalles-rollos" class="table color-table info-table form-material">
                <thead>
                <tr>
                    <th style="width:10%;">Código</th>
                    <th style="width:15%;">Tipo Tela</th>
                    <th style="width:10%;">Color</th>
                    <th style="width:10%;">Peso</th>
                    <th style="width:10%;">Partida</th>
                    <th style="width:15%;">Tintorería</th>
                    <th style="width:10%;">Costo</th>
                    <th style="width:10%;">IVA</th>
                    <th style="width:10%;">Total</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>

                </tbody>
                <tfoot>
                <tr>
                    <td>{!! Form::number('', null, ['class' => 'form-control agregar-detalle', 'tabindex' => '9999', 'style' => 'width: 80px;']) !!}</td>
                    <td>{!! Form::select('', $tipos_telas, '', ['id' => 'select_tipo_tela', 'class' => 'form-control agregar-detalle', 'tabindex' => '9999', 'readonly', 'disabled']) !!}</td>
                    <td>{!! Form::select('', $colores, '', ['id' => 'select_color', 'class' => 'form-control agregar-detalle', 'tabindex' => '9999', 'readonly', 'disabled']) !!}</td>
                    <td>{!! Form::number('', null, ['class' => 'form-control', 'readonly', 'tabindex' => '9999', 'style' => 'width: 80px;']) !!}</td>
                    <td>{!! Form::text('', null, ['class' => 'form-control', 'readonly', 'tabindex' => '9999']) !!}</td>
                    <td>{!! Form::text('', null, ['class' => 'form-control', 'readonly', 'tabindex' => '9999']) !!}</td>
                    <td>{!! Form::number('', null, ['class' => 'form-control', 'readonly', 'tabindex' => '9999']) !!}</td>
                    <td>{!! Form::select('', $porcentajes_iva, '', ['id' => 'select_porcentaje_iva', 'class' => 'form-control', 'tabindex' => '9999', 'readonly', 'disabled']) !!}</td>
                    <td>{!! Form::number('', null, ['class' => 'form-control', 'readonly', 'tabindex' => '9999']) !!}</td>
                    <td></td>
                </tr>
                    <tr>
                        <th colspan="10">
                            <a href="javascript:void(0)" class="btn btn-themecolor" id="agregar-detalle">
                                <i class="fa fa-plus" aria-hidden="true"></i> Agregar Rollo
                            </a>
                        </th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>


<div class="card">
    <div class="card-body">
        <div class="form-group">
            {{ Form::submit(isset($submitButtonText) ? $submitButtonText : trans('buttons.general.save'), ['class' => 'btn btn-themecolor btn-lg pull-right']) }}
        </div>
    </div>
</div>
