@extends('frontend.layouts.app')

@section ('title', trans('labels.frontend.rollo_titulo')." - Ingreso Masivo")

@section('after-styles')
    {{ Html::style("css/frontend/plugins/jquery/jquery-ui.min.css") }}
@endsection

@section('content')
    <div class="row page-titles">
        <div class="col-md-8 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">Ingreso Masivo de Rollos</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item"><a href="{{url('/produccion/rollo')}}">{{ trans('labels.frontend.rollo_titulo') }}</a></li>
                <li class="breadcrumb-item active">Ingreso Masivo de Rollos</li>
            </ol>
        </div>
        <div class="col-md-4">
            <div class="pull-right">
                <a href="{{ url('/produccion/rollo') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i> {{ trans('buttons.general.crud.back') }}
                </a>
            </div>
        </div>
    </div>

    @include('includes.partials.messages')

    {!! Form::open(['url' => '/produccion/rollo/guardarIngresoMasivo', 'class' => 'form-horizontal', 'files' => true, 'method' => 'POST']) !!}
        @include ('frontend.produccion.rollo.form_ingreso_masivo')
    {!! Form::close() !!}

@endsection

@section('after-scripts')
    {{ Html::script("js/frontend/plugins/jquery/jquery-ui.min.js") }}
    {{ Html::script("js/frontend/plugins/jquery/datepicker-es.js") }}
    {{ Html::script("js/frontend/plugins/maskedinput/jquery.maskedinput.min.js") }}

<script>
    $( document ).ready(function() {
        $('input.fecha').datepicker({
            firstDay: 0,
            dateFormat: 'dd/mm/yy'
        }).mask("99/99/9999", {placeholder: "dd/mm/aaaa"});
        $('input#numero').mask("99999999",{placeholder:"########"});

        $('select#compra_id').on('change', function () {
            buscarDatosCompra();
        });

        function buscarDatosCompra() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $( "input[name*='_token']" ).val()
                }
            });
            var compra_id = $('#compra_id option:selected').val();
            $.ajax({
                type: "GET",
                url: '{{ url("/compras/compra/obtenerDatos") }}',
                data: {compra_id: compra_id},
                success: function( msg ) {
                    console.log(msg);
                    $("#datos_compra").val(msg);
                }
            });
        }

        $(document).on('click','.borrar-detalle', function() {
            inputNombre = this.attributes['name'].value;
            $('.borrar-detalle[name="' + inputNombre + '"]').parent().parent().hide("slow");
            $('.borrar-detalle[name="' + inputNombre + '"]').parent().find('input.estado').val(0);
        });

        calcularSubtotales();
    });

    function calcularSubtotales(){
        var $tablaTodasFilas = $("#tabla-detalles-rollos tbody tr");
        $tablaTodasFilas.each(function (index) {
            var $tablaFila = $(this);

            $tablaFila.find('.costo, .iva').on('change', function () {
                var costo = parseFloat($tablaFila.find('.costo').val());
                var iva = parseFloat($tablaFila.find('.iva').find(":selected").text());

                var total = costo + costo * iva / 100;

                if (!isNaN(total)) {
                    $tablaFila.find('.total').val(total.toFixed(2));
                }
            });
        });
    }

    $("#agregar-detalle").click(function(){
        agregarDetalle();
    });

    $(".agregar-detalle").on('focusin', function(){
        agregarDetalle();
    });

    function agregarDetalle() {
        var options_tipo_tela = $("select#select_tipo_tela").html();
        var options_color = $("select#select_color").html();
        var options_procentaje_iva = $("select#select_porcentaje_iva").html();

        var numeracion = parseInt($('input#numeracion-rollo').val()) + $('table.table tbody tr').length - $('table.table tbody tr:hidden').length;
        var numero_orden = $('table.table tbody tr').length+ 1;

        $("#tabla-detalles-rollos").append('<tr>' +
            '<td>' +
                '<input class="form-control name="detalles['+(numero_orden*-1)+'][codigo]" type="text" value="' + numeracion + '" style="width: 80px;">' +
                '<input type="hidden" class="id" name="detalles['+(numero_orden*-1)+'][id]" value="'+(numero_orden*-1)+'">' +
            '</td>' +
            '<td>' +
                '<select id="detalles['+(numero_orden*-1)+'][tipo_tela_id]" class="form-control" name="detalles['+(numero_orden*-1)+'][tipo_tela_id]">' +
                    options_tipo_tela +
                '</select>' +
            '</td>' +
            '<td>' +
                '<select id="detalles['+(numero_orden*-1)+'][color_id]" class="form-control" name="detalles['+(numero_orden*-1)+'][color_id]">' +
                    options_color +
                '</select>' +
            '</td>' +
            '<td>' +
                '<input class="form-control" name="detalles['+(numero_orden*-1)+'][peso]" type="number" value="" step="0.01" style="width: 80px;">' +
            '</td>' +
            '<td>' +
                '<input class="form-control" name="detalles['+(numero_orden*-1)+'][partida]" type="text" value="">' +
            '</td>' +
            '<td>' +
                '<input class="form-control" name="detalles['+(numero_orden*-1)+'][tintoreria]" type="text" value="">' +
            '</td>' +
            '<td>' +
                '<input class="form-control costo" name="detalles['+(numero_orden*-1)+'][costo]" type="number" step="0.01">' +
            '</td>' +
            '<td>' +
                '<select id="detalles['+(numero_orden*-1)+'][porcentaje_iva]" class="form-control iva" name="detalles['+(numero_orden*-1)+'][porcentaje_iva]">' +
                    options_procentaje_iva +
                '</select>' +
            '</td>' +
            '<td>' +
                '<input class="form-control total" name="detalles['+(numero_orden*-1)+'][total]" type="number" step="0.01">' +
            '</td>' +
            '<td>' +
                '<a class="btn btn-danger btn-circle borrar-detalle" name="detalles['+(numero_orden*-1)+'][eliminar]" tabindex="99999">' +
                    '<i class="fa fa-trash-o" aria-hidden="true"></i>' +
                '</a>' +
                '<input type="hidden" class="estado" name="detalles['+(numero_orden*-1)+'][estado]" value="1">' +
            '</td>' +
            '</tr>');
        $("select[name='detalles["+(numero_orden*-1)+"][tipo_tela_id]']").focus();
        calcularSubtotales();
    }
</script>
@endsection