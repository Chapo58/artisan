@extends('frontend.layouts.app')

@section ('title', trans('labels.frontend.rollo_titulo')." - ".trans('buttons.general.crud.show'))

@section('content')
    <div class="row page-titles">
        <div class="col-md-8 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('labels.frontend.rollo_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item"><a href="{{url('/produccion/rollo')}}">{{ trans('labels.frontend.rollo_titulo') }}</a></li>
                <li class="breadcrumb-item">{{ trans('buttons.general.crud.show') }} {{ trans('labels.frontend.rollo') }}</li>
                <li class="breadcrumb-item active">{{ $rollo }}</li>
            </ol>
        </div>
        <div class="col-md-4">
            <div class="pull-right">
                <a href="{{ url('/produccion/rollo') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i> {{ trans('buttons.general.crud.back') }}
                </a>
                <a href="{{ url('/produccion/rollo/' . $rollo->id . '/edit') }}" title="{{ trans('buttons.general.crud.edit') }}" class="btn btn-info">
                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i> {{ trans('buttons.general.crud.edit') }}
                </a>
                <a href="javascript:void(0)" title="{{trans("buttons.general.crud.delete")." ".trans("labels.frontend.rollo")}}" class="btn btn-danger delete">
                    <i class="fa fa-trash" aria-hidden="true"></i> {{ trans("buttons.general.crud.delete") }}
                </a>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
            <br>
            <div class="table-responsive">
                <table class="table table-borderless">
                    <tbody>
                    <tr>
                        <th> Codigo </th><td> {{ $rollo->codigo }} </td>
                        <th> Estado </th><td> {{ $estados[$rollo->estado] }} </td>
                    </tr>
                    <tr>
                        <th> Tipo de Tela </th><td> {{ $rollo->tipoTela }} </td>
                        <th> Color </th><td> {{ $rollo->color }} </td>
                    </tr>
                    <tr>
                        <th> Partida </th><td> {{ $rollo->partida }} </td>
                        <th> Tintorería </th><td> {{ $rollo->tintoreria }} </td>
                    </tr>
                    <tr>
                        <th> Peso </th><td> {{ $rollo->peso }} kg</td>
                        <th> Costo </th><td>{{ $rollo->moneda->signo }} {{ $rollo->costo }} </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection

@section('after-scripts')
    <script>
        $("body").on("click", ".delete", function () {
            swal({
                title: "{{trans('buttons.general.crud.delete').' '.trans('labels.frontend.rollo')}}",
                text: "¿Realmente desea eliminar este registro?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#ff8726',
                confirmButtonText: "Eliminar"
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        type: 'POST',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: '{{ url('/produccion/rollo/'.$rollo->id) }}',
                        data: {"_method" : 'DELETE'},
                        success: function (msg) {
                            swal("Eliminado!", "El registro ha sido correctamente eliminado.", "success").then((result) => { window.location.href = '{{url('/produccion/rollo')}}'; });
                        }
                    });
                }
            });
        });
    </script>
@endsection
