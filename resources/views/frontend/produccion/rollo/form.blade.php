<div class="card">
    <div class="card-body">
        <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
        <br>
        <div class="row form-group">
            <div class="col-md-2">
                {!! Form::label('codigo', 'Codigo') !!}
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-hashtag"></i>
                    </div>
                    {!! Form::text('codigo', isset($valor_numeracion_rollo) ? $valor_numeracion_rollo : '', ['class' => 'form-control', 'required' => 'required']) !!}
                </div>
                {!! $errors->first('codigo', '<p class="text-danger">:message</p>') !!}
            </div>
            <div class="col-md-5">
                {!! Form::label('tipo_tela_id', 'Tipo de Tela') !!}
                {!! Form::select('tipo_tela_id', $tipos_telas, isset($rollo->tipoTela) ? $rollo->tipoTela->id : '', array('class' => 'form-control', 'required' => 'required')) !!}
                {!! $errors->first('tipo_tela_id', '<p class="text-danger">:message</p>') !!}
            </div>
            <div class="col-md-5">
                {!! Form::label('color_id', 'Color') !!}
                {!! Form::select('color_id', $colores, isset($rollo->color) ? $rollo->color->id : '', array('class' => 'form-control', 'required' => 'required')) !!}
                {!! $errors->first('color_id', '<p class="text-danger">:message</p>') !!}
            </div>
        </div>
        <div class="row form-group">
            <div class="col-md-4">
                {!! Form::label('partida', 'Partida') !!}
                {!! Form::text('partida', null, ['class' => 'form-control']) !!}
                {!! $errors->first('partida', '<p class="text-danger">:message</p>') !!}
            </div>
            <div class="col-md-4">
                {!! Form::label('peso', 'Peso') !!}
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-balance-scale"></i>
                    </div>
                    {!! Form::number('peso', null, ['class' => 'form-control', 'step' => '0.01']) !!}
                </div>
                {!! $errors->first('peso', '<p class="text-danger">:message</p>') !!}
            </div>
            <div class="col-md-4">
                {!! Form::label('tintoreria', 'Tintorería') !!}
                {!! Form::text('tintoreria', null, ['class' => 'form-control']) !!}
                {!! $errors->first('tintoreria', '<p class="text-danger">:message</p>') !!}
            </div>
        </div>
        <div class="row form-group">
            <div class="col-md-4">
                {!! Form::label('costo', 'Costo') !!}
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-usd"></i>
                    </div>
                    {!! Form::number('costo', null, ['class' => 'form-control', 'step' => '0.01']) !!}
                </div>
                {!! $errors->first('costo', '<p class="text-danger">:message</p>') !!}
            </div>
            <div class="col-md-4">
                {!! Form::label('porcentaje_iva', 'Porcentaje Iva') !!}
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-percent"></i>
                    </div>
                    {!! Form::select('porcentaje_iva', $porcentajes_iva, isset($rollo) ? $rollo->porcentaje_iva : '', array('class' => 'form-control', 'required' => 'required')) !!}
                </div>
                {!! $errors->first('porcentaje_iva', '<p class="text-danger">:message</p>') !!}
            </div>
            <div class="col-md-4">
                {!! Form::label('moneda_id', 'Moneda') !!}
                {!! Form::select('moneda_id', $monedas, isset($rollo->moneda) ? $rollo->moneda->id : '', array('class' => 'form-control', 'required' => 'required')) !!}
                {!! $errors->first('moneda_id', '<p class="text-danger">:message</p>') !!}
            </div>
        </div>
        <div class="row form-group">
            <div class="col-md-6">
                {!! Form::label('estado', 'Estado') !!}
                {!! Form::select('estado', $estados, isset($rollo) ? $rollo->estado : '', array('class' => 'form-control', 'required' => 'required')) !!}
                {!! $errors->first('estado', '<p class="text-danger">:message</p>') !!}
            </div>
        </div>
        <hr>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-12">
                    {{ Form::submit(isset($submitButtonText) ? $submitButtonText : trans('buttons.general.save'), ['class' => 'btn btn-lg btn-themecolor pull-right']) }}
                </div>
            </div>
        </div>
    </div>
</div>