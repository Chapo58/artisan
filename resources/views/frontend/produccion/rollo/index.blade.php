@extends ('frontend.layouts.app')

@section ('title', trans('labels.frontend.rollo_titulo')." - ".trans('navs.frontend.user.administration'))

@section('content')
    <div class="row page-titles">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('labels.frontend.rollo_titulo') }}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
                <li class="breadcrumb-item active">{{ trans('labels.frontend.rollo_titulo') }}</li>
            </ol>
        </div>
        <div class="col-md-6">
            <div class="pull-right">
                <a href="{{ url('/produccion/rollo/ingresoMasivoDeRollos') }}" class="btn btn-primary btn-lg" title="Ingreso Masivo">
                    <i class="fa fa-plus" aria-hidden="true"></i> Ingreso Masivo
                </a>
                <a href="{{ url('/produccion/rollo/create') }}" class="btn btn-themecolor btn-lg" title="{{ trans('buttons.general.crud.create').' '.trans('labels.frontend.rollo')  }}">
                    <i class="fa fa-plus" aria-hidden="true"></i> {{ trans('buttons.general.crud.create').' '.trans('labels.frontend.rollo') }}
                </a>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <a class="btn-minimize btn-expandir mytooltip" href="javascript:void(0)" data-action="expand"><i class="mdi mdi-arrow-expand"></i><span class="tooltip-content3">Expandir Ventana</span></a>
            @include('includes.partials.messages')
            <div class="table-responsive">
                <table id="rollo-table" class="table table-striped table-hover table-sm">
                    <thead>
                        <tr>
                            <th>Codigo</th>
                            <th>Tipo de Tela</th>
                            <th>Color</th>
                            <th>Partida</th>
                            <th>Peso</th>
                            <th>Tintorería</th>
                            <th>Costo</th>
                            <th>Estado</th>
                            <th>{{ trans('labels.general.actions') }}</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

@endsection

@section('after-scripts')
    {{ Html::script("js/frontend/plugins/datatables/jquery.dataTables.min.js") }}

<script>
    $(document).ready(function() {
        $('#rollo-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('frontend.produccion.RollosData') !!}',
            columns: [
                {data: 'codigo', name: 'codigo'},
                {data: 'tipo_tela', name: 'tipo_tela'},
                {data: 'color', name: 'color'},
                {data: 'partida', name: 'partida'},
                {data: 'peso', name: 'peso'},
                {data: 'tintoreria', name: 'tintoreria'},
                {data: 'costo', name: 'costo'},
                {data: 'estado', name: 'estado'},
                {data: 'action', name: 'action', orderable: false, searchable: false}
            ]
        });
    } );

    $("body").on("click", ".delete", function () {
        var id = $(this).attr("data-id");
        swal({
            title: "{{trans('buttons.general.crud.delete').' '.trans('labels.frontend.rollo')}}",
            text: "¿Realmente desea eliminar este registro?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#ff8726',
            confirmButtonText: "Eliminar"
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/produccion/rollo/'+id,
                    data: {"_method" : 'DELETE'},
                    success: function (msg) {
                        $("#" + id).hide(1);
                        swal("Eliminado!", "El registro ha sido correctamente eliminado.", "success");
                    }
                });
            }
        });
    });
</script>

@endsection
