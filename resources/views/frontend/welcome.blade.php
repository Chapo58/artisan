@extends('frontend.layouts.app')

@section ('title', '¡Bienvenido a Artisan!')

@section('after-styles')
    {{ Html::style("css/frontend/plugins/wizard/steps.css") }}
@endsection

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body wizard-content">
                    {!! Form::open(['url' => '/configuraciones/primerosPasos', 'method' => 'POST', 'class' => 'tab-wizard wizard-circle', 'files' => true, 'id' => 'formWelcome']) !!}

                    <h6>¡Bienvenido!</h6>
                    <section>
                        <div class="row">
                            <div class="col-md-12">
                                <h1 class="text-center">¡Bienvendio a <img style="width:150px;margin-top:-10px;" src="{{url('images/frontend/logos/logo_mini.png')}}">!</h1>
                                <h2 class="text-center">Bienvenido a tu nuevo sistema de gestión. Ahora te guiaremos en tus primeros pasos por el sistema. Para consultas no dudes en usar el chat ubicado en la esquina inferior derecha para contactarte con nosotros.</h2>
                            </div>
                        </div>
                    </section>
                    <h6>Personaliza tu sistema</h6>
                    <section>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="alert alert-info alert-rounded">
                                    <h2 class="text-center">Artisan se adapta a tus gustos, asi que lo primero sera personalizarlo un poco</h2>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-6">
                                        <ul id="themecolors" class="colores m-t-20">
                                            <li><h4 class="font-medium m-t-30">Menu Luminoso</h4><hr></li>
                                            <li><a href="javascript:void(0)" data-theme="default" class="default-theme">1</a></li>
                                            <li><a href="javascript:void(0)" data-theme="green" class="green-theme">2</a></li>
                                            <li><a href="javascript:void(0)" data-theme="red" class="red-theme">3</a></li>
                                            <li><a href="javascript:void(0)" data-theme="blue" class="blue-theme">4</a></li>
                                            <li><a href="javascript:void(0)" data-theme="purple" class="purple-theme">5</a></li>
                                            <li><a href="javascript:void(0)" data-theme="megna" class="megna-theme">6</a></li>
                                        </ul>
                                    </div>
                                    <div class="col-md-6">
                                        <ul id="themecolors" class="colores m-t-20">
                                            <li class="d-block m-t-30"><h4 class="font-medium m-t-30">Menu Oscuro</h4><hr></li>
                                            <li><a href="javascript:void(0)" data-theme="default-dark" class="default-dark-theme">7</a></li>
                                            <li><a href="javascript:void(0)" data-theme="green-dark" class="green-dark-theme">8</a></li>
                                            <li><a href="javascript:void(0)" data-theme="red-dark" class="red-dark-theme">9</a></li>
                                            <li><a href="javascript:void(0)" data-theme="blue-dark" class="blue-dark-theme">10</a></li>
                                            <li><a href="javascript:void(0)" data-theme="purple-dark" class="purple-dark-theme">11</a></li>
                                            <li><a href="javascript:void(0)" data-theme="megna-dark" class="megna-dark-theme ">12</a></li>
                                        </ul>
                                    </div>
                                    {{ Form::hidden('color') }}
                                    <div class="col-md-12">
                                        <h4 class="font-medium m-t-30">Tipo de Menu</h4> <i>(Esta modificación sera visible al finalizar los pasos)</i><hr>
                                        <div class="demo-radio-button">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <input name="menu" value="horizontal" type="radio" id="horizontal" class="radio-col-cyan" />
                                                        <label for="horizontal">Horizontal</label>
                                                    </td>
                                                    <td style="width:20px;"></td>
                                                    <td>
                                                        <input name="menu" value="vertical" type="radio" id="vertical" class="radio-col-cyan" />
                                                        <label for="vertical">Vertical</label>
                                                    </td>
                                                    <td style="width:20px;"></td>
                                                    <td>
                                                        <input  name="menu" value="dark" type="radio" id="dark" class="radio-col-cyan" />
                                                        <label  for="dark">Modo Nocturno</label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <img width="250" src="{{ url('images/frontend/horizontal.jpg') }}" alt="Horizontal">
                                                    </td>
                                                    <td></td>
                                                    <td>
                                                        <img width="250" src="{{ url('images/frontend/vertical.jpg') }}" alt="Vertical">
                                                    </td>
                                                    <td></td>
                                                    <td>
                                                        <img width="250" src="{{ url('images/frontend/dark.jpg') }}" alt="Horizontal">
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <h6>Acceso Directo</h6>
                    <section>
                        <div class="alert alert-info alert-rounded">
                            <h2 class="text-center">Para que sea mas fácil ingresar al sistema, hemos creado unos accesos directos que podés descargar y colocar en el escritorio de tu computadora. Selecciona uno dependiendo del navegador de internet que uses habitualmente.</h2>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <a href="{{url('downloads/Chrome/Artisan.exe')}}" target="_blank">
                                    <h1 class="text-center"><i class="fa fa-chrome"></i> Chrome</h1>
                                </a>
                            </div>
                            <div class="col-md-4">
                                <a href="{{url('downloads/Kiosko/Artisan.exe')}}" target="_blank">
                                    <h1 class="text-center"><i class="fa fa-window-maximize"></i> <i class="fa fa-chrome"></i> Chrome</h1>
                                    <h2 class="text-center">(Pantalla Completa)</h2>
                                </a>
                            </div>
                            <div class="col-md-4">
                                <a href="{{url('downloads/Firefox/Artisan.exe')}}" target="_blank">
                                    <h1 class="text-center"><i class="fa fa-firefox"></i> FireFox</h1>
                                </a>
                            </div>
                        </div>
                    </section>
                    <h6>Tu primer artículo</h6>
                    <section>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="alert alert-info alert-rounded">
                                    <h2 class="text-center">Tu primer artículo es fundamental para comenzar a vender. Te hemos creado dos listas de precios para que comiences, pero podés modificarlas en cualquier momento <a href="{{url('ventas/lista-precios')}}" target="_blank">desde acá</a></h2>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row form-group">
                            <div class="col-md-6">
                                {!! Form::label('nombre', 'Nombre del Artículo') !!}
                                {!! Form::text('nombre', null, ['class' => 'form-control', 'required' => 'required']) !!}
                                {!! $errors->first('nombre', '<p class="text-danger">:message</p>') !!}
                            </div>
                            <div class="form-group col-md-2">
                                {!! Form::label('lleva_stock', '¿Lleva Stock?') !!}
                                <div class="switch">
                                    <label>
                                        {!! Form::checkbox('lleva_stock', true, isset($articulo->lleva_stock) ? $articulo->lleva_stock : true) !!}<span class="lever"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-4" id="parametros_stock">
                                {!! Form::label('stock', 'Stock') !!}
                                <i class="fa fa-question-circle text-info mytooltip">
                                    <span class="tooltip-content3">Stock actual del articulo disponible para venta</span>
                                </i>
                                {!! Form::number('stock', 1, ['class' => 'form-control', 'required' => 'required']) !!}
                                {!! $errors->first('stock', '<p class="text-danger">:message</p>') !!}
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-6">
                                {!! Form::label('ultimo_costo', 'Costo Neto') !!}
                                <i class="fa fa-question-circle text-info mytooltip">
                                    <span class="tooltip-content3">Precio base neto del artículo</span>
                                </i>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-usd"></i>
                                    </div>
                                    {!! Form::number('ultimo_costo', '', ['class' => 'form-control', 'step' => '0.01', 'required' => 'required']) !!}
                                </div>
                                {!! $errors->first('ultimo_costo', '<p class="text-danger">:message</p>') !!}
                            </div>
                            <div class="col-md-6">
                                {!! Form::label('porcentaje_iva', 'Porcentaje IVA') !!}
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-percent"></i>
                                    </div>
                                    {!! Form::select('porcentaje_iva', $porcentajesIva, '', ['id' => 'porcentaje_iva', 'class' => 'form-control', 'required' => 'required']) !!}
                                </div>
                                {!! $errors->first('porcentaje_iva', '<p class="text-danger">:message</p>') !!}
                            </div>
                        </div>
                        <table id="tabla-precios" class="table color-bordered-table muted-bordered-table table-hover table-striped  col-md-12 form-material">
                            <thead>
                            <tr>
                                <th>Lista Precio</th>
                                <th>
                                    Porcentaje
                                    <i class="fa fa-question-circle text-info mytooltip">
                                        <span class="tooltip-content3">Porcentaje de ganancia que debe dejar el artículo</span>
                                    </i>
                                </th>
                                <th>Precio Neto</th>
                                <th>Precio Bruto</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(isset($listas_sin_usar))
                                @foreach($listas_sin_usar as $lista)
                                    <tr>
                                        <td class="align-middle">
                                            <span class="font-weight-bold">{{ $lista->nombre }}</span>
                                            <input type="hidden" name="precios[{{ ($loop->index+1)*-1 }}][id]" value="{{ ($loop->index+1)*-1 }}">
                                            <input type="hidden" name="precios[{{ ($loop->index+1)*-1 }}][lista_precio_id]" value="{{ $lista->id }}">
                                        </td>
                                        <td>
                                            <div class="input-group">
                                                <div class="left-icon">%</div>
                                                {!! Form::number('precios['.(($loop->index+1)*-1).'][porcentaje_lista]', isset($lista->porcentaje) ? $lista->porcentaje : 0, ['class' => 'form-control porcentaje-lista', 'step' => '0.01']) !!}
                                            </div>
                                        </td>
                                        <td>
                                            <div class="input-group">
                                                <div class="left-icon">$</div>
                                                {!! Form::number('precios['.(($loop->index+1)*-1).'][precio_neto]', '', ['class' => 'precio-neto form-control', 'step' => '0.01', 'readonly']) !!}
                                            </div>
                                        </td>
                                        <td>
                                            <div class="input-group">
                                                <div class="left-icon">$</div>
                                                {!! Form::number('precios['.(($loop->index+1)*-1).'][precio_venta]', '', ['class' => 'precio-venta form-control', 'step' => '0.01', 'readonly']) !!}
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </section>
                    <h6>Finalizar</h6>
                    <section>
                        <div class="jumbotron jumbotron-fluid">
                            <div class="container">
                                <h1 class="text-center">
                                    ¡Muy Bien! <br>
                                    Ahora ya estás listo para comenzar a explorar tu nuevo sistema de gestión. A lo largo del sistema iras encontrando distintos tips y ayudas que harán tu experiencia más intuitiva y simple, pero no olvides que siempre puedes comunicarte con nosotros a través del <a href="javascript:void()" onclick="javascript:void(Tawk_API.toggle())">chat integrado</a> para que te ayudemos
                                </h1>
                            </div>
                        </div>
                    </section>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@endsection

@section('after-scripts')
    {{ Html::script("js/frontend/plugins/wizard/jquery.steps.min.js") }}
    {{ Html::script("js/frontend/plugins/wizard/jquery.validate.min.js") }}
    {{ Html::script("js/frontend/plugins/styleswitcher/jQuery.style.switcher.js") }}

    <script>
        stepsWizard = $(".tab-wizard").steps({
            headerTag: "h6"
            , bodyTag: "section"
            , transitionEffect: "fade"
            , titleTemplate: '<span class="step">#index#</span> #title#'
            , labels: {
                finish: "FINALIZAR"
            },
            onFinished: function (event, currentIndex) {
                $('form#formWelcome').submit();
            }
        });

        $(document).ready(function() {
            $('ul.colores li').find('a.blue-theme').addClass('working');
            $('[name=color]').val('blue');
            $('#horizontal').prop('checked', true);

            $('ul.colores li').click(function(e) {
                var color = $(this).find("a").attr("data-theme");
                $('[name=color]').val(color);
            });

            actualizarPreciosListas();
        });

        $('select#porcentaje_iva').on('change', function () {
            actualizarPreciosListas();
        });

        $(document).on('change', 'input#ultimo_costo', function () {
            $('input#ultimo_costo').val(this.value);
            actualizarPreciosListas();
        });

        $(document).on('change', 'input#lleva_stock', function () {
            if($('input#lleva_stock').is(':checked')){
                $('#parametros_stock').show();
            } else {
                $('#parametros_stock').hide();
            }
        });

        $(document).on('change', 'input.porcentaje-lista', function () {
            var iva = parseFloat($('select#porcentaje_iva option:selected').text());
            var costo = parseFloat($('input#ultimo_costo').val());

            if(isNaN(costo) || costo == 0 || isNaN(iva)){
                swal("Debe definir un costo y un porcentaje de iva al articulo");
            } else {
                actualizarPreciosListas();
            }
        });

        function actualizarPreciosListas(){
            var $filasTablaPrecios = $("#tabla-precios tbody tr");
            $filasTablaPrecios.each(function (index) {
                var $filaPrecio = $(this);

                var iva = parseFloat($('select#porcentaje_iva option:selected').text());
                var porLista = parseFloat($filaPrecio.find('.porcentaje-lista').val());
                var costo = parseFloat($('input#ultimo_costo').val());

                if(!isNaN(costo) && costo != 0 && !isNaN(iva)){
                    var precioNeto = (costo * porLista)/100 + costo;
                    $filaPrecio.find('.precio-neto').val(precioNeto.toFixed(2));

                    var precioVenta = (precioNeto * iva)/100 + precioNeto;
                    $filaPrecio.find('.precio-venta').val(precioVenta.toFixed(2));
                }
            });
        }
    </script>
@endsection
