@extends('frontend.layouts.app')

@section('content')
    <div class="row page-titles">
      <div class="col-md-5 col-8 align-self-center">
        <h3 class="text-themecolor">Panel de Control</h3>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="javascript:void(0)">Inicio</a></li>
          <li class="breadcrumb-item active">Panel de Control</li>
        </ol>
      </div>
      <div class="col-md-7 col-4 align-self-center">
        <div class="d-flex m-t-10 justify-content-end">
          <div class="d-flex m-r-20 m-l-10 hidden-md-down">
            <div class="chart-text m-r-10">
              <h6 class="m-b-0"><small>ESTE MES</small></h6>
              <h4 class="m-t-0 text-info">${{number_format($totalVentas, 2, ',', '.')}}</h4></div>
            <div class="spark-chart">
              <div id="monthchart"></div>
            </div>
          </div>
          <div class="d-flex m-r-20 m-l-10 hidden-md-down">
            <div class="chart-text m-r-10">
              <h6 class="m-b-0"><small>MES PASADO</small></h6>
              <h4 class="m-t-0 text-primary">${{number_format($totalVentasMesPasado, 2, ',', '.')}}</h4></div>
            <div class="spark-chart">
              <div id="lastmonthchart"></div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
        @if($mostrarAlertaVencimiento)
            <div class="col-md-12">
                <div class="card">
                    <div class="d-flex flex-row">
                        <div class="p-10 bg-warning">
                            <h3 class="text-white box m-b-0"><i class="fa fa-exclamation-triangle"></i></h3>
                        </div>
                        <div class="align-self-center m-l-20" style="width:50%;">
                            <h3 class="m-b-0 text-info">Su cuenta esta próxima a vencer</h3>
                            <h5 class="text-muted m-b-0">Fecha de Vencimiento: <strong>{{$user->empresa->fecha_vencimiento->format('d/m/Y')}}</strong></h5>
                        </div>
                        @role('Usuario Admin')
                            <div class="align-self-center col-md-6 col-xs-12">
                                <a href="{{ url('/configuraciones/pago/create') }}" class="btn btn-themecolor btn-lg pull-right" title="{{ trans('buttons.general.crud.create').' '.trans('labels.frontend.pago')  }}">
                                    <i class="fa fa-plus" aria-hidden="true"></i> Realizar {{trans('labels.frontend.pago') }}
                                </a>
                            </div>
                        @endauth
                    </div>
                </div>
            </div>
        @endif
      @roles(['Usuario Admin', 'Usuario Gerente'])
      <!-- Column -->
      <div class="col-lg-3 col-md-6">
        <div class="card">
            <a href="{{url('/ventas/venta')}}" class="mytooltip">
              <div class="card-body">
                <div class="d-flex flex-row">
                  <div class="round round-lg align-self-center round-info"><i class="fa fa-shopping-cart"></i></div>
                  <div class="m-l-10 align-self-center">
                    <h3 class="m-b-0 font-light">${{number_format($totalVentas, 2, ',', '.')}}</h3>
                    <h5 class="text-muted m-b-0">Ventas</h5></div>
                </div>
              </div>
                <span class="tooltip-content5">
                    <span class="tooltip-text3">
                        <span class="tooltip-inner2">
                            Importe total de las ventas de este mes.
                        </span>
                    </span>
                </span>
            </a>
        </div>
      </div>
      <!-- Column -->
      @endauth
      <!-- Column -->
      <div class="col-lg-3 col-md-6">
        <div class="card">
            <a href="{{url('/articulos/articulo')}}" class="mytooltip">
              <div class="card-body">
                <div class="d-flex flex-row">
                  <div class="round round-lg align-self-center round-warning"><i class="fa fa-barcode"></i></div>
                  <div class="m-l-10 align-self-center">
                    <h3 class="m-b-0 font-lgiht">{{$articulos}}</h3>
                    <h5 class="text-muted m-b-0">Articulos</h5></div>
                </div>
              </div>
                <span class="tooltip-content5">
                    <span class="tooltip-text3">
                        <span class="tooltip-inner2">
                            Cantidad total de articulos cargados en el sistema.
                        </span>
                    </span>
                </span>
            </a>
        </div>
      </div>
      <!-- Column -->
      <!-- Column -->
      <div class="col-lg-3 col-md-6">
        <div class="card">
            <a href="{{url('/caja/movimiento-caja')}}" class="mytooltip">
              <div class="card-body">
                <div class="d-flex flex-row">
                  <div class="round round-lg align-self-center round-primary"><i class="fa fa-ticket"></i></div>
                  <div class="m-l-10 align-self-center">
                    <h3 class="m-b-0 font-lgiht">{{($dineroCaja != 'Cerrada') ? '$ '.number_format($dineroCaja, 2, ',', '.') : 'Caja Cerrada'}}</h3>
                    <h5 class="text-muted m-b-0">Caja Actual</h5></div>
                </div>
              </div>
                @if($dineroCaja != 'Cerrada')
                <span class="tooltip-content5">
                    <span class="tooltip-text3">
                        <span class="tooltip-inner2">
                            Importe total de la caja actual considerando todas las formas de pago.
                        </span>
                    </span>
                </span>
                @endif
            </a>
        </div>
      </div>
      <!-- Column -->
      <!-- Column -->
      <div class="col-lg-3 col-md-6">
        <div class="card">
            <a href="{{url('/ventas/cuenta-corriente')}}" class="mytooltip">
              <div class="card-body">
                <div class="d-flex flex-row">
                  <div class="round round-lg align-self-center round-danger"><i class="fa fa-usd"></i></div>
                  <div class="m-l-10 align-self-center">
                    <h3 class="m-b-0 font-lgiht">${{abs(number_format($importesCuentasCorrientes, 2, ',', '.'))}}</h3>
                      <h5 class="text-muted m-b-0">C. Corrientes</h5></div>
                </div>
              </div>
                <span class="tooltip-content5">
                    <span class="tooltip-text3">
                        <span class="tooltip-inner2">
                            Importe total de deudas de cuentas corrientes de todos los clientes.
                        </span>
                    </span>
                </span>
            </a>
        </div>
      </div>
      <!-- Column -->
    </div>

    <!-- Row -->
    <div class="row">
      <div class="col-lg-8">
        <div class="card">
          <div class="card-body">
              {!! Form::open(['method' => 'GET', 'url' => '/dashboard', 'class' => 'form-horizontal', 'id' => 'formArticulos'])  !!}
            <select id="ventasArticulos" name="ventasArticulos" class="custom-select pull-right">
                <option selected="0">Siempre</option>
                <option value="1">Esta Semana</option>
                <option value="2">Este Mes</option>
                <option value="3">Este Año</option>
            </select>
              {!! Form::close() !!}
            <h4 class="card-title">Articulos mas Vendidos</h4>
            {!! $chartArticulos->html() !!}
          </div>
        </div>
      </div>

      <div class="col-lg-4">
        <div class="card">
          <div class="card-body">
              {!! Form::open(['method' => 'GET', 'url' => '/dashboard', 'class' => 'form-horizontal', 'id' => 'formClientes'])  !!}
            <select id="rankingClientes" name="rankingClientes" class="custom-select pull-right">
                <option selected="0">Siempre</option>
                <option value="1">Esta Semana</option>
                <option value="2">Este Mes</option>
                <option value="3">Este Año</option>
            </select>
              {!! Form::close() !!}
            <h4 class="card-title">Ranking Clientes</h4>
            <div class="table-responsive m-t-20">
              <table class="table stylish-table">
                <thead>
                  <tr>
                    <th>
                      Cliente
                    </th>
                    <th>
                      Facturado
                    </th>
                  </tr>
                </thead>
                <tbody>
                @foreach($rankingClientes as $item)
                  <tr>
                    <td><a href="{{url('personas/cliente/'.$item->id)}}">{{ $item->razon_social }}</a></td>
                    <td>
                        <b class="text-success"><strong>${{ number_format($item->total, 2, ',', '.') }}</strong></b>
                    </td>
                  </tr>
                @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>

      <div class="col-lg-6">
        <div class="card">
          <div class="card-body">
            <h4 class="card-title">Ultimas Ventas</h4>
            <div class="table-responsive m-t-20">
              <table class="table stylish-table">
                <thead>
                <tr>
                  <th>
                    Numero
                  </th>
                  <th>
                    Cliente
                  </th>
                  <th>
                    Total
                  </th>
                </tr>
                </thead>
                <tbody>
                @foreach($venta as $item)
                  <tr>
                      <td><a href="{{url('ventas/venta/'.$item->id)}}">{{ $tipos_comprobantes[$item->tipo_comprobante_id] }} {{ $item->punto_venta }}-{{ $item->numero }}</a></td>
                    <td>
                      {{ $item->razon_social }}
                    </td>
                    <td>
                      <b class="text-success">{{ $item->signo }} {{ $item->total_cobro }}</b>
                    </td>
                  </tr>
                @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-6">
        <div class="card">
          <div class="card-body">
            <h4 class="card-title">Cheques Pendientes</h4>
            <div class="table-responsive m-t-20">
              <table class="table stylish-table">
                <thead>
                <tr>
                  <th>
                    Numero
                  </th>
                  <th>
                    Banco
                  </th>
                  <th>
                    Importe
                  </th>
                  <th>
                    Vencimiento
                  </th>
                </tr>
                </thead>
                <tbody>
                @foreach($cheques as $item)
                  <tr>
                      <td><a href="{{url('fondos/cheque/'.$item->id)}}">{{ $item->numero }}</a></td>
                    <td>{{ isset($item->banco) ? $item->banco->nombre: 'Ninguno' }}</td>
                    <td>
                      <b class="text-success">$ {{ $item->importe }}</b>
                    </td>
                    <td>{{ ($item->vencimiento) ? $item->vencimiento->format('d/m/Y') : "" }}</td>
                  </tr>
                @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
        <div class="col-lg-4 col-md-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Eventos de esta semana</h4>
                    <ul class="feeds">
                        @foreach($eventos as $item)
                        <li>
                            <div style="background-color:{{($item->color) ? $item->color : '#3A87AD'}}"></div> {{$item->titulo}} <span class="text-muted">{{$diasSemana[$item->fecha_inicio->dayOfWeek]}}{{($item->hora_inicio) ? ' a las '. substr($item->hora_inicio, 0, -3): ''}}</span>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- Row -->

    <div class="">
      <button class="right-side-toggle waves-effect waves-light btn-success btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
    </div>

<div class="right-sidebar">
    <div class="slimscrollright">
        <div class="rpanel-title"> Panel de Diseño <span><i class="ti-close right-side-toggle"></i></span> </div>
        <div class="r-panel-body">
            {!! Form::open([
                'method'=>'POST',
                'class' => 'form-horizontal',
                'action' => ['Frontend\Configuraciones\AparienciaController@actualizar']
            ]) !!}
            <ul id="themecolors" class="colores m-t-20">
                <li><h4 class="font-medium m-t-30">Menu Luminoso</h4><hr></li>
                <li><a href="javascript:void(0)" data-theme="default" class="default-theme">1</a></li>
                <li><a href="javascript:void(0)" data-theme="green" class="green-theme">2</a></li>
                <li><a href="javascript:void(0)" data-theme="red" class="red-theme">3</a></li>
                <li><a href="javascript:void(0)" data-theme="blue" class="blue-theme">4</a></li>
                <li><a href="javascript:void(0)" data-theme="purple" class="purple-theme">5</a></li>
                <li><a href="javascript:void(0)" data-theme="megna" class="megna-theme">6</a></li>
                <li class="d-block m-t-30"><h4 class="font-medium m-t-30">Menu Oscuro</h4><hr></li>
                <li><a href="javascript:void(0)" data-theme="default-dark" class="default-dark-theme">7</a></li>
                <li><a href="javascript:void(0)" data-theme="green-dark" class="green-dark-theme">8</a></li>
                <li><a href="javascript:void(0)" data-theme="red-dark" class="red-dark-theme">9</a></li>
                <li><a href="javascript:void(0)" data-theme="blue-dark" class="blue-dark-theme">10</a></li>
                <li><a href="javascript:void(0)" data-theme="purple-dark" class="purple-dark-theme">11</a></li>
                <li><a href="javascript:void(0)" data-theme="megna-dark" class="megna-dark-theme ">12</a></li>
            </ul>
            <br>
            <h4 class="font-medium m-t-30">Tipo de Menu</h4><hr>
            <div class="demo-radio-button">
                <table>
                    <tr>
                        <td>
                            <input name="menu" value="horizontal" type="radio" id="horizontal" class="radio-col-cyan" />
                            <label for="horizontal">Horizontal</label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <img width="200" src="{{ url('images/frontend/horizontal.jpg') }}" alt="Horizontal">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input name="menu" style="margin-top:15px;" value="vertical" type="radio" id="vertical" class="radio-col-cyan" />
                            <label style="margin-top:15px;" for="vertical">Vertical</label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <img width="200" src="{{ url('images/frontend/vertical.jpg') }}" alt="Vertical">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input style="margin-top:15px;" name="menu" value="dark" type="radio" id="dark" class="radio-col-cyan" />
                            <label style="margin-top:15px;" for="dark">Vertical Oscuro</label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <img width="200" src="{{ url('images/frontend/dark.jpg') }}" alt="Horizontal">
                        </td>
                    </tr>
                </table>
            </div>
            {{ Form::hidden('color') }}
            <hr>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-12">
                        {{ Form::submit(isset($submitButtonText) ? $submitButtonText : trans('buttons.general.save').' Cambios', ['class' => 'btn btn-lg btn-themecolor']) }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('after-scripts')
    <!-- ============================================================== -->
    <!-- Style switcher -->
    <!-- ============================================================== -->
    {{ Html::script("js/frontend/plugins/styleswitcher/jQuery.style.switcher.js") }}

<!-- LIBRERIAS GRAFICOS -->
<script type="text/javascript" src="https://cdn.rawgit.com/Mikhus/canvas-gauges/gh-pages/download/2.1.2/all/gauge.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/chartist/0.10.1/chartist.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
<script type="text/javascript" src="https://static.fusioncharts.com/code/latest/fusioncharts.js"></script>
<script type="text/javascript" src="https://static.fusioncharts.com/code/latest/themes/fusioncharts.theme.fint.js"></script>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">google.charts.load('current', {'packages':['corechart', 'gauge', 'geochart', 'bar', 'line']})</script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/highcharts/5.0.7/highcharts.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/highcharts/5.0.7/js/modules/offline-exporting.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/highmaps/5.0.7/js/modules/map.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/highmaps/5.0.7/js/modules/data.js"></script>
<script type="text/javascript" src="https://code.highcharts.com/mapdata/custom/world.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.2.6/raphael.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/justgage/1.2.2/justgage.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.2.6/raphael.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.5/d3.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/plottable.js/2.8.0/plottable.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/progressbar.js/1.0.1/progressbar.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.5/d3.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/c3/0.4.11/c3.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/echarts/3.6.2/echarts.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/amcharts/3.21.2/amcharts.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/amcharts/3.21.2/serial.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/amcharts/3.21.2/plugins/export/export.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/amcharts/3.21.2/themes/light.js"></script>
<!-- LIBRERIAS GRAFICOS -->
  
    {!! $chartArticulos->script() !!}

    <script>
        $(document).ready(function() {
            @if($user->panel_color)
                $('ul.colores li').find('a.{{$user->panel_color}}-theme').addClass('working');
                $('[name=color]').val('{{$user->panel_color}}');
            @else
                $('ul.colores li').find('a.blue-theme').addClass('working');
                $('[name=color]').val('blue');
            @endif

            @if($user->panel_orientacion)
                $('#{{$user->panel_orientacion}}').prop('checked', true);
            @else
                $('#horizontal').prop('checked', true);
            @endif

            $('ul.colores li').click(function(e) {
                var color = $(this).find("a").attr("data-theme");
                $('[name=color]').val(color);
            });

            $('#ventasArticulos').on('change', function() {
                $('form#formArticulos').submit();
            })

            $('#rankingClientes').on('change', function() {
                $('form#formClientes').submit();
            })

            @if($periodoArticulos)
                $('#ventasArticulos').val({{$periodoArticulos}});
            @endif
            @if($periodoClientes)
                $('#rankingClientes').val({{$periodoClientes}});
            @endif
        });
    </script>
@endsection