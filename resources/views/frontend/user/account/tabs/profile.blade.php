<div class="row">
    <div class="col-md-4 font-weight-bold">{{ trans('labels.frontend.user.profile.email') }}</div>
    <div class="col-md-8">{{ $usuario->email }}</div>
</div><hr>
<div class="row">
    <div class="col-md-4 font-weight-bold">Usuario</div>
    <div class="col-md-8">{{ $usuario->name  }}</div>
</div><hr>
<div class="row">
    <div class="col-md-4 font-weight-bold">{{ trans('labels.frontend.user.profile.created_at') }}</div>
    <div class="col-md-8">{{ $usuario->created_at->format('d/m/Y H:i') }} ({{ $usuario->created_at->diffForHumans() }})</div>
</div><hr>
<div class="row">
    <div class="col-md-4 font-weight-bold">{{ trans('labels.frontend.user.profile.last_updated') }}</div>
    <div class="col-md-8">{{ $usuario->updated_at->format('d/m/Y H:i') }} ({{ $usuario->updated_at->diffForHumans() }})</div>
</div><hr>
<div class="row">
    <div class="col-md-4 font-weight-bold">Estado</div>
    <div class="col-md-8">{{ ($usuario->status == 1) ? 'Activado' : 'Inhabilitado' }}</div>
</div><hr>
<div class="row">
    <div class="col-md-4 font-weight-bold">Sucursal</div>
    <div class="col-md-8">{{ isset($usuario->sucursal) ? $usuario->sucursal : 'Sin sucursal asociada.' }}</div>
</div><hr>
<div class="row">
    <div class="col-md-4 font-weight-bold">Empleado</div>
    <div class="col-md-8">{{ isset($usuario->empleado) ? $usuario->empleado->nombre : 'Sin empleado asociado.' }}</div>
</div><hr>
<div class="row">
    <div class="col-md-4 font-weight-bold">Cuil</div>
    <div class="col-md-8">{{ isset($usuario->empleado) ? $usuario->empleado->cuil : '' }}</div>
</div><hr>
<div class="row">
    <div class="col-md-4 font-weight-bold">Caja por Defecto</div>
    <div class="col-md-8">
    @if(isset($usuario->empleado->caja))
        <a href="{{url('caja/cajas/'.$usuario->empleado->caja->id)}}">{{ $usuario->empleado->caja }}</a>
    @else
        Sin Caja Asignada
    @endif
    </div>
</div><hr>
<div class="row">
    <div class="col-md-4 font-weight-bold">Punto Venta por Defecto</div>
    <div class="col-md-8">
    @if(isset($usuario->empleado->puntoVenta))
        <a href="{{url('configuraciones/punto-de-venta/'.$usuario->empleado->puntoVenta->id)}}">{{ $usuario->empleado->puntoVenta }}</a>
    @else
        Sin Punto de Venta Asignado
    @endif
    </div>
</div>