@include('includes.partials.messages')

{!! Form::model($usuario, [
    'method' => 'PATCH',
    'url' => ['/configuraciones/usuario', $usuario->id],
    'class' => 'form-horizontal',
    'files' => true
]) !!}

    <h4 class="font-medium m-t-30">Información del Usuario</h4>
    <hr>
    <div class="row form-group">
        <div class="col-md-6">
            {!! Form::label('email', trans('validation.attributes.backend.access.users.email')) !!}
            {{ Form::email('email', null, ['class' => 'form-control', 'required' => 'required']) }}
            {!! $errors->first('email', '<p class="text-danger">:message</p>') !!}
        </div>
        <div class="col-md-6">
            {!! Form::label('name', trans('validation.attributes.backend.access.users.name').' usuario') !!}
            {{ Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) }}
            {!! $errors->first('name', '<p class="text-danger">:message</p>') !!}
        </div>
    </div>
    @role('Usuario Admin')
        <div class="row form-group">
            <div class="col-md-6">
                {!! Form::label('status', 'Activo') !!}
                <div class="switch">
                    <label>DESACTIVADO
                        {!! Form::checkbox('status', true, ($usuario->status == 1) ? true : false) !!}<span class="lever"></span>
                        ACTIVADO</label>
                </div>
                {!! $errors->first('status', '<p class="text-danger">:message</p>') !!}
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Cambiar Avatar</label>
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="input-group image-preview">
                                <input type="text" class="form-control image-preview-filename" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
                                <span class="input-group-btn">
                                    <!-- image-preview-clear button -->
                                    <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                                      <span class="glyphicon glyphicon-remove"></span> Limpiar
                                    </button>
                                    <!-- image-preview-input -->
                                    <div class="btn btn-default image-preview-input">
                                      <span class="glyphicon glyphicon-folder-open"></span>
                                      <span class="image-preview-input-title">Buscar</span>
                                      <input type="file" name="image" accept=".png, .jpg, .jpeg">
                                    </div>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endauth

    <h4 class="font-medium m-t-30">Información del Empleado</h4>
    <hr>
    <div class="row form-group">
        <div class="col-md-6">
            {!! Form::label('empleado[nombre]', 'Nombre Empleado') !!}
            {!! Form::text('empleado[nombre]', null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! $errors->first('empleado[nombre]', '<p class="text-danger">:message</p>') !!}
        </div>
        <div class="col-md-6">
            {!! Form::label('empleado[cuil]', 'Cuil') !!}
            <div class="input-group">
                <div class="input-group-addon">
                    <i class="fa fa-hashtag"></i>
                </div>
                {!! Form::text('empleado[cuil]', null, ['class' => 'form-control', 'required' => 'required']) !!}
            </div>
            {!! $errors->first('empleado[cuil]', '<p class="text-danger">:message</p>') !!}
        </div>
    </div>
    @role('Usuario Admin')
        <div class="row form-group">
            <div class="col-md-6">
                {!! Form::label('empleado[caja_id]', 'Caja por defecto') !!}
                {!! Form::select('empleado[caja_id]', $cajas, isset($usuario->empleado) ? $usuario->empleado->caja_id : '', ['class' => 'form-control']) !!}
                {!! $errors->first('empleado[caja_id]', '<p class="text-danger">:message</p>') !!}
            </div>
            <div class="col-md-6">
                {!! Form::label('empleado[punto_venta_id]', 'Punto de Venta por defecto') !!}
                {!! Form::select('empleado[punto_venta_id]', $puntos_venta, isset($usuario->empleado) ? $usuario->empleado->punto_venta_id : '', ['class' => 'form-control']) !!}
                {!! $errors->first('empleado[punto_venta_id]', '<p class="text-danger">:message</p>') !!}
            </div>
        </div>
    @endauth

    <div class="row form-group">
        <div class="col-md-6">
            {!! Form::label('direccion', 'Dirección') !!}
            {!! Form::text('domicilio[direccion]', isset($usuario->empleado->domicilio->direccion) ? $usuario->empleado->domicilio->direccion : '', ['class' => 'form-control', 'required' => 'required','id' => 'direccion']) !!}
            {!! $errors->first('direccion', '<p class="text-danger">:message</p>') !!}
        </div>
        <div class="col-md-6">
            {!! Form::label('localidad', 'Localidad') !!}
            {!! Form::text('domicilio[localidad]', isset($usuario->empleado->domicilio->localidad) ? $usuario->empleado->domicilio->localidad : '', ['class' => 'form-control', 'required' => 'required','id' => 'localidad']) !!}
            {!! $errors->first('localidad', '<p class="text-danger">:message</p>') !!}
        </div>
    </div>
    <div class="row form-group">
        <div class="col-md-6">
            {!! Form::label('codigo_postal', 'Código Postal') !!}
            {!! Form::text('domicilio[codigo_postal]', isset($usuario->empleado->domicilio->codigo_postal) ? $usuario->empleado->domicilio->codigo_postal : '', ['class' => 'form-control','id' => 'codigo_postal']) !!}
            {!! $errors->first('codigo_postal', '<p class="text-danger">:message</p>') !!}
        </div>
        <div class="col-md-6">
            {!! Form::label('provincia_id', 'Provincia') !!}
            {!! Form::select('domicilio[provincia_id]', $provincias, isset($usuario->empleado->domicilio->provincia) ? $usuario->empleado->domicilio->provincia->id : '', ['id' => 'provincia_id', 'class' => 'form-control', 'required' => 'required']) !!}
            {!! $errors->first('provincia_id', '<p class="text-danger">:message</p>') !!}
        </div>
    </div>
    <div class="row form-group">
        <div class="col-md-6">
            {!! Form::label('telefono', 'Teléfono') !!}
            <div class="input-group">
                <div class="input-group-addon">
                    <i class="fa  fa-phone"></i>
                </div>
            {!! Form::text('empleado[telefono]', isset($usuario->empleado->telefono) ? $usuario->empleado->telefono : '', ['class' => 'form-control','id' => 'telefono']) !!}
            </div>
            {!! $errors->first('telefono', '<p class="text-danger">:message</p>') !!}
        </div>
        <div class="col-md-6">
            {!! Form::label('sucursal_id', 'Sucursal') !!}
            {{ Form::select('sucursal_id', $sucursales, isset($usuario->sucursal) ? $usuario->sucursal_id : 0, ['class' => 'form-control']) }}
            {!! $errors->first('sucursal_id', '<p class="text-danger">:message</p>') !!}
        </div>
    </div>
    
    <hr>
    <div class="form-actions">
      <div class="row">
        <div class="col-md-12">
          {{ Form::submit(isset($submitButtonText) ? $submitButtonText : trans('buttons.general.save'), ['class' => 'btn btn-lg btn-success pull-right']) }}
        </div>
      </div>
    </div>

{!! Form::close() !!}