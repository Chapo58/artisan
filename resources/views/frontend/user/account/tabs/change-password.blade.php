{{ Form::open(['route' => ['frontend.auth.password.change'], 'class' => 'form-horizontal', 'method' => 'patch']) }}

    <div class="row form-group">
      <div class="col-md-8">
        {!! Form::label('old_password', trans('validation.attributes.frontend.old_password')) !!}
        {{ Form::input('password', 'old_password', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.old_password')]) }}
        {!! $errors->first('nombre', '<p class="text-danger">:message</p>') !!}
      </div>
    </div>

    <div class="row form-group">
      <div class="col-md-6">
        {!! Form::label('password', trans('validation.attributes.frontend.new_password')) !!}
        {{ Form::input('password', 'password', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.new_password')]) }}
        {!! $errors->first('password', '<p class="text-danger">:message</p>') !!}
      </div>
      <div class="col-md-6">
        {!! Form::label('password_confirmation', trans('validation.attributes.frontend.new_password_confirmation')) !!}
        {{ Form::input('password', 'password_confirmation', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.new_password_confirmation')]) }}
        {!! $errors->first('password_confirmation', '<p class="text-danger">:message</p>') !!}
      </div>
    </div>

    <hr>
    <div class="form-actions">
      <div class="row">
        <div class="col-md-12">
          {{ Form::submit(trans('labels.general.buttons.update'), ['class' => 'btn btn-lg btn-success pull-right', 'id' => 'change-password']) }}
        </div>
      </div>
    </div>

{{ Form::close() }}