@extends('frontend.layouts.app')

@section('content')

<div class="row page-titles">
    <div class="col-md-6 col-6 align-self-center">
        <h3 class="text-themecolor m-b-0 m-t-0">{{ trans('labels.frontend.usuario_titulo') }}</h3>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/dashboard">Inicio</a></li>
            <li class="breadcrumb-item"><a href="{{url('/configuraciones/usuario')}}">{{ trans('labels.frontend.usuario_titulo') }}</a></li>
            <li class="breadcrumb-item">{{ trans('buttons.general.crud.show') }} {{ trans('labels.frontend.usuario') }}</li>
            <li class="breadcrumb-item active">{{ $user }}</li>
        </ol>
    </div>
    <div class="col-md-6">
        <div class="pull-right">
            <a href="{{ url('/configuraciones/usuario') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning btn-sm">
                <i class="fa fa-arrow-left" aria-hidden="true"></i> {{ trans('buttons.general.crud.back') }}
            </a>
            @role('Usuario Admin')
                <a href="{{ url('/configuraciones/usuario/' . $user->id . '/edit') }}" title="{{ trans('buttons.general.crud.edit') }}" class="btn btn-info btn-sm">
                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i> {{ trans('buttons.general.crud.edit') }}
                </a>
                <a href="javascript:void(0)" title="{{trans("buttons.general.crud.delete")." ".trans("labels.frontend.usuario")}}" class="btn btn-danger btn-sm delete">
                    <i class="fa fa-trash" aria-hidden="true"></i> {{ trans("buttons.general.crud.delete") }}
                </a>
            @endauth
        </div>
    </div>
</div>

 <!-- Row -->
<div class="row">
    <!-- Column -->
    <div class="col-lg-4 col-xlg-3 col-md-5">
        <div class="card">
            <div class="card-body">
                <center class="m-t-30"> <img src="{{ $user->picture }}" class="img-circle" width="150" />
                    <h4 class="card-title m-t-10">{{ $user->name }}</h4>
                    <h6 class="card-subtitle">{{ $user->rol }}</h6>
                </center>
            </div>
            <div>
                <hr> </div>
            <div class="card-body"> 
                <small class="text-muted">Email</small>
                <h6>{{ $usuario->email }}</h6> 
                <small class="text-muted p-t-30 db">Telefono</small>
                <h6>+91 654 784 547</h6> 
                <small class="text-muted p-t-30 db">Dirección</small>
                <h6>71 Pilgrim Avenue Chevy Chase, MD 20815</h6>
            </div>
        </div>
    </div>
    <!-- Column -->
    <!-- Column -->
    <div class="col-lg-8 col-xlg-9 col-md-7">
        <div class="card">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs profile-tab" role="tablist">
                <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#perfil" role="tab">{{ trans('navs.frontend.user.profile') }}</a> </li>
                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#actualizar" role="tab">{{ trans('labels.frontend.user.profile.update_information') }}</a> </li>
                @if ($user->canChangePassword())
                    <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#pass" role="tab">{{ trans('navs.frontend.user.change_password') }}</a> </li>
                @endif
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
                <div class="tab-pane active" id="perfil" role="tabpanel">
                    <div class="card-body">
                        @include('frontend.user.account.tabs.profile')
                    </div>
                </div>
                <!--second tab-->
                <div class="tab-pane" id="actualizar" role="tabpanel">
                    <div class="card-body">
                        @include('frontend.user.account.tabs.edit')
                    </div>
                </div>
                @if ($user->canChangePassword())
                    <div class="tab-pane" id="pass" role="tabpanel">
                        <div class="card-body">
                            {{ trans('navs.frontend.user.change_password') }}
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
    <!-- Column -->
</div>
<!-- Row -->

@endsection