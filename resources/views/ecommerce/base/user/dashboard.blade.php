@extends ('ecommerce.layouts.app')

@section ('title', 'Dashboard')

@section('after-styles')
    {{ Html::style("css/ecommerce/select2.min.css") }}
@endsection

@section('content')
    <div class="inner-page-banner-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="breadcrumb-area">
                        <h1>Dashboard</h1>
                        <ul>
                            <li><a href="/ecommerce">Inicio</a> /</li>
                            <li>Dashboard</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="my-account-page-area">
        <div class="container">
            <div class="woocommerce">
                <div class="row">
                    <div class="col-md-12">
                        @include('includes.partials.messages')
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                        <nav class="woocommerce-MyAccount-navigation">
                            <ul>
                                <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--dashboard active"><a href="#dashboard" data-toggle="tab" aria-expanded="false">Inicio</a></li>
                                <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--dashboard"><a href="#orders" data-toggle="tab" aria-expanded="false">Ordenes de Compra</a></li>
                                <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--dashboard"><a href="#addresses" data-toggle="tab" aria-expanded="false">Dirección</a></li>
                                <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--dashboard"><a href="#details" data-toggle="tab" aria-expanded="false">Información de la cuenta</a></li>
                                <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--dashboard"><a href="#logout" data-toggle="tab" aria-expanded="false">Salir</a></li>
                            </ul>
                        </nav>
                    </div>
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <div class="tab-content">
                            <div class="tab-pane fade active in" id="dashboard">
                                <div class="woocommerce-MyAccount-content">
                                    <p>Bienvenido <strong>{{$user->cliente}}</strong> (No sos <strong>{{$user->cliente}}</strong>? <a href="/ecommerce/logout">Salir</a>)</p>
                                    <p>Desde aqui podras ver tus <a href="#">ordenes de compra</a>, gestionar tu <a href="#">dirección de entrega</a> y <a href="#">cambiar tu contraseña y detalles de cuenta</a>.</p>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="orders">
                                <div class="woocommerce-message"><a class="woocommerce-Button button" href="/ecommerce">Ir de compras</a>No has realizado ninguna orden de compra aun.
                                </div>
                            </div>
                            <div class="tab-pane fade" id="addresses">
                                <div class="woocommerce-MyAccount-content wd-myaccount-content-wrapper">
                                    <header class="row woocommerce-Address-title title">
                                        <h3 class="col-xs-12 metro-title">DIRECCION DE ENVIO</h3>
                                    </header>
                                    {!! Form::model($user, [
                                        'method' => 'POST',
                                        'url' => ['/ecommerce/actualizarDireccion'],
                                        'class' => 'row woocommerce-EditAccountForm edit-account'
                                    ]) !!}
                                    <p class="col-md-6 col-sm-12 woocommerce-form-row woocommerce-form-row--first form-row form-row-first">
                                        <label for="direccion">Dirección</label>
                                        <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="direccion" value="{{(isset($user->cliente->domicilio) ? $user->cliente->domicilio->direccion : '')}}" id="direccion">
                                    </p>
                                    <p class="col-md-6 woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                        <label for="localidad">Localidad</label>
                                        <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="localidad" value="{{(isset($user->cliente->domicilio) ? $user->cliente->domicilio->localidad : '')}}" id="localidad">
                                    </p>
                                    <div class="clear"></div>
                                    <p class="col-md-6 woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                        <label for="codigo_postal">Codigo Postal</label>
                                        <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="codigo_postal" value="{{(isset($user->cliente->domicilio) ? $user->cliente->domicilio->codigo_postal : '')}}" id="codigo_postal">
                                    </p>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="control-label" for="provincia_id">Provincia</label>
                                            <div class="custom-select">
                                                <select id="provincia_id" name="provincia_id" class='select2'>
                                                    <option value="0">Seleccionar Provincia</option>
                                                    @foreach($provincias as $provincia)
                                                        <option value="{{$provincia->id}}">{{$provincia}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                    <p class="col-xs-12">
                                        <input type="submit" class="woocommerce-Button button btn-shop-now-fill" name="save_account_details" value="Guardar">
                                    </p>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                            <div class="tab-pane fade" id="details">
                                <div class="woocommerce-MyAccount-content">
                                    <header class="row woocommerce-Address-title title">
                                        <h3 class="col-xs-12 metro-title">INFORMACION DE LA CUENTA</h3>
                                    </header>
                                    {!! Form::model($user, [
                                        'method' => 'POST',
                                        'url' => ['/ecommerce/actualizarDatosUsuario'],
                                        'class' => 'row woocommerce-EditAccountForm edit-account'
                                    ]) !!}
                                        <p class="col-md-12 col-sm-12 woocommerce-form-row woocommerce-form-row--first form-row form-row-first">
                                            <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="razon_social" id="razon_social" value="{{$user->cliente->razon_social}}" placeholder="Nombre Completo" required>
                                        </p>
                                        <div class="clear"></div>
                                        <p class="col-md-12 col-sm-12 woocommerce-form-row woocommerce-form-row--last form-row form-row-last">
                                            <input type="email" class="woocommerce-Input woocommerce-Input--email input-text" name="email" id="email" value="{{$user->email}}" placeholder="Direccion de Email" required>
                                        </p>
                                        <div class="clear"></div>
                                        <p class="col-md-6 col-sm-12 woocommerce-form-row woocommerce-form-row--first form-row form-row-first">
                                            <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="dni" id="dni" value="{{$user->cliente->dni}}" placeholder="DNI/CUIT">
                                        </p>
                                        <p class="col-md-6 col-sm-12 woocommerce-form-row woocommerce-form-row--last form-row form-row-last">
                                            <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="telefono" id="telefono" value="{{$user->cliente->telefono}}" placeholder="Teléfono">
                                        </p>
                                        <fieldset class="col-xs-12">
                                            <legend>Cambiar Contraseña</legend>
                                            <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                                <label for="old_password">Contraseña actual (dejar vacio para no modificar)</label>
                                                <input type="password" class="woocommerce-Input woocommerce-Input--password input-text" name="old_password" id="old_password">
                                            </p>
                                            <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                                <label for="password">Nueva Contraseña (dejar vacio para no modificar)</label>
                                                <input type="password" class="woocommerce-Input woocommerce-Input--password input-text" name="password" id="password">
                                            </p>
                                            <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                                <label for="password_confirmation">Confirmar nueva contraseña</label>
                                                <input type="password" class="woocommerce-Input woocommerce-Input--password input-text" name="password_confirmation" id="password_confirmation">
                                            </p>
                                        </fieldset>
                                        <div class="clear"></div>
                                        <p class="col-xs-12">
                                            <input type="hidden" id="_wpnonce" name="_wpnonce" value="96df2c51c6"><input type="hidden" name="_wp_http_referer" value="/my-account/edit-account/">
                                            <input type="submit" class="woocommerce-Button button btn-shop-now-fill" name="save_account_details" value="Guardar Cambios">
                                            <input type="hidden" name="action" value="save_account_details">
                                        </p>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                            <div class="tab-pane fade" id="logout">
                                <div class="woocommerce-message">¿Estas seguro de que deseas cerrar session? <a href="/ecommerce/logout">Confirmar y salir</a></div>
                                <div class="woocommerce-MyAccount-content wd-myaccount-content-wrapper">
                                    <p>Puedes volver a ingresar con tus datos en cualquier momento desde <a href="/ecommerce/login">el panel de inicio de sesion</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('after-scripts')
    {{ Html::script("js/ecommerce/select2.min.js") }}
@endsection