@extends ('ecommerce.'.$estilo.'.layouts.app')

@section ('title', 'Carrito de Compras')

@section('content')
    <div class="main-slider3">
        <div class="bend niceties preview-1">
            <div id="ensign-nivoslider-3" class="slides">
                @foreach($sliders as $slider)
                    <img src="{{url($slider->imagen_url)}}" alt="{{$slider}}" title="#slider-{{$slider->id}}" />
                @endforeach
            </div>
            @foreach($sliders as $slider)
            <div id="slider-{{$slider->id}}" class="slider-direction">
                <div class="slider-content t-lfr s-tb slider-1">
                    <div class="title-container s-tb-c">
                        <h2 style="{{($slider->color) ? 'color:'.$slider->color : ''}}" class="title1">{{$slider}}</h2>
                        <p style="{{($slider->color) ? 'color:'.$slider->color : ''}}">{{$slider->texto}}</p>
                        @if($slider->link)
                            <a href="{{$slider->link}}" class="btn-shop-now-fill-slider">Ver Mas</a>
                        @endif
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>

    <div class="product2-area">
        <div class="container-fluid" id="home-isotope">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="section-title">
                        <span class="title-bar-left"></span>
                        <h2>Productos Destacados</h2>
                        <span class="title-bar-right"></span>
                    </div>
                </div>
            </div>
            <div class="row featuredContainer">
                @foreach($productosDestacados as $producto)
                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6 kid">
                        <div class="product-box1">
                            <ul class="product-social">
                                <li data-id="{{$producto->id}}" class="comprar-producto"><a href="javascript:void(0)"><i class="fa fa-shopping-cart" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-heart-o" aria-hidden="true"></i></a></li>
                                <li data-id="{{$producto->id}}" class="modal-producto"><a href="javascript:void(0)" data-toggle="modal" data-target="#myModal"><i class="fa fa-eye" aria-hidden="true"></i></a></li>
                            </ul>
                            <div class="product-img-holder">
                                @if($producto->esNuevo())
                                <div class="hot-sale">
                                    <span>Nuevo</span>
                                </div>
                                @endif
                                <a href="#">
                                    <img src="{{url($producto->imagen_url)}}" alt="{{$producto->nombre}}">
                                </a>
                            </div>
                            <div class="product-content-holder">
                                <h3><a href="{{url('/ecommerce/producto/'.$producto->id)}}">{{$producto->nombre}}</a></h3>
                                @if($empresa->ecommerce->mostrar_precios)
                                    <span>$ {{$producto->getPrecio()}}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

    <div class="best-seller2-area padding-top-0-after-desk">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="section-title">
                        <span class="title-bar-left"></span>
                        <h2>Mas Vendidos</h2>
                        <span class="title-bar-right"></span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="metro-carousel" data-loop="true" data-items="6" data-margin="15" data-autoplay="true" data-autoplay-timeout="10000" data-smart-speed="2000" data-dots="false" data-nav="true" data-nav-speed="false" data-r-x-small="1" data-r-x-small-nav="true" data-r-x-small-dots="false" data-r-x-medium="2" data-r-x-medium-nav="false" data-r-x-medium-dots="true" data-r-small="3" data-r-small-nav="false" data-r-small-dots="true" data-r-medium="4" data-r-medium-nav="false" data-r-medium-dots="true" data-r-large="6" data-r-large-nav="false" data-r-large-dots="true">
                    @foreach($productosMasVendidos as $producto)
                        <div class="product-box1">
                            <ul class="product-social">
                                <li data-id="{{$producto->id}}" class="comprar-producto"><a href="javascript:void(0)"><i class="fa fa-shopping-cart" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-heart-o" aria-hidden="true"></i></a></li>
                                <li data-id="{{$producto->id}}" class="modal-producto"><a href="javascript:void(0)" data-toggle="modal" data-target="#myModal"><i class="fa fa-eye" aria-hidden="true"></i></a></li>
                            </ul>
                            <div class="product-img-holder">
                                <div class="hot-sale">
                                    <span>Nuevo</span>
                                </div>
                                <a href="#"><img src="{{($producto->imagen_url) ? $producto->imagen_url : '/articulos/no-image.jpg'}}" alt="{{$producto->nombre}}"></a>
                            </div>
                            <div class="product-content-holder">
                                <h3><a href="{{url('/ecommerce/producto/'.$producto->id)}}">{{$producto->nombre}}</a></h3>
                                @if($empresa->ecommerce->mostrar_precios)
                                    <span>$ {{$objetoArticulo->getPrecioPorArticuloId($producto->id)}}</span>
                                @endif
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

    <div class="advantage1-area">
        <div class="container">
            <div class="row">
                @foreach($caracteristicas as $caracteristica)
                    @if($caracteristica->imagen_url)
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div class="advantage-area-box">
                                <div class="media">
                                    <a class="pull-left" style="margin-left:20px;" href="#">
                                        <img class="img-responsive" src="{{url($caracteristica->imagen_url)}}" style="height:50px;width:50px;" alt="{{$caracteristica}}">
                                    </a>
                                    <div class="media-body">
                                        <h3>{{$caracteristica}}</h3>
                                        <p>{{$caracteristica->texto}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="solid-divider"></div>
            </div>
        </div>
    </div>

    <div class="brand-area">
        <div class="container">
            <div class="metro-carousel" data-loop="true" data-items="6" data-margin="30" data-autoplay="true" data-autoplay-timeout="10000" data-smart-speed="2000" data-dots="false" data-nav="true" data-nav-speed="false" data-r-x-small="2" data-r-x-small-nav="true" data-r-x-small-dots="false" data-r-x-medium="3" data-r-x-medium-nav="true" data-r-x-medium-dots="false" data-r-small="4" data-r-small-nav="true" data-r-small-dots="false" data-r-medium="5" data-r-medium-nav="true" data-r-medium-dots="false" data-r-large="6" data-r-large-nav="true" data-r-large-dots="false">
                @foreach($marcas as $marca)
                    <div class="brand-area-box">
                        <a href="{{url('ecommerce/marca/'.$marca->id)}}"><img src="{{url($marca->imagen_url)}}" style="height:98px;width:170px;" alt="brand"></a>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

@endsection
