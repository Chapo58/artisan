@extends ('ecommerce.'.$estilo.'.layouts.app')

@section ('title', 'Politica de Privacidad')

@section('content')
    <div class="inner-page-banner-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="breadcrumb-area">
                        <h1>Politica de Privacidad</h1>
                        <ul>
                            <li><a href="/ecommerce">Inicio</a> /</li>
                            <li>Politica de Privacidad</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="about-us-page-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    {!! $empresa->ecommerce->politica_privacidad !!}
                </div>
            </div>
        </div>
    </div>

@endsection
