@extends ('ecommerce.'.$estilo.'.layouts.app')

@section ('title', 'Contacto')

@section('content')
    <div class="inner-page-banner-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="breadcrumb-area">
                        <h1>Contactate con nosotros</h1>
                        <ul>
                            <li><a href="/ecommerce">Inicio</a> /</li>
                            <li>Contacto</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="contact-us-page-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    @include('includes.partials.messages')
                </div>
                <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">
                    <div class="contact-us-left">
                        <h2>Envianos un mensaje</h2>
                        <div class="row">
                            <div class="contact-form">
                                {{ Form::open(['url' => '/ecommerce/contacto/mailContacto', 'method' => 'POST']) }}
                                    <fieldset>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <input type="text" placeholder="Nombre *" name="nombre" class="form-control" id="form-name" required>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <input type="email" placeholder="Email *" name="email" class="form-control" id="form-email" required>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <textarea placeholder="Mensaje *" name="mensaje" class="textarea form-control" id="form-message" rows="8" cols="20" required></textarea>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <button type="submit" class="btn-send-message">Enviar Mensaje</button>
                                            </div>
                                        </div>
                                    </fieldset>
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                    <div class="contact-us-right">
                        <h2 class="title-sidebar">Información de Contacto</h2>
                        <ul>
                            <li class="con-address">{{$empresa->ecommerce->domicilio}}.</li>
                            <li class="con-envelope">{{$empresa->ecommerce->email}}</li>
                            @if($empresa->ecommerce->email2)
                                <li class="con-envelope">{{$empresa->ecommerce->email2}}</li>
                            @endif
                            <li class="con-phone">{{$empresa->ecommerce->telefono}}
                            @if($empresa->ecommerce->telefono2)
                                <li class="con-phone">{{$empresa->ecommerce->telefono2}}
                            @endif
                            @if($empresa->ecommerce->telefono3)
                                <li class="con-phone">{{$empresa->ecommerce->telefono3}}
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
