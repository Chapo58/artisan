<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-body">
            <button type="button" class="close myclose" data-dismiss="modal">&times;</button>
            <div class="product-details1-area">
                <div class="product-details-info-area">
                    <div class="row">
                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                            <div class="inner-product-details-left">
                                <div class="tab-content">
                                    <div id="metro-related1" class="tab-pane fade active in">
                                        <a href="#"><img class="img-responsive" src="{{url('articulos/no-image.jpg')}}" alt="single"></a>
                                    </div>
                                    <div id="metro-related2" class="tab-pane fade">
                                        <a href="#"><img class="img-responsive" src="{{url('articulos/no-image.jpg')}}" alt="single"></a>
                                    </div>
                                    <div id="metro-related3" class="tab-pane fade">
                                        <a href="#"><img class="img-responsive" src="{{url('articulos/no-image.jpg')}}" alt="single"></a>
                                    </div>
                                </div>
                                <ul>
                                    <li class="active">
                                        <a aria-expanded="false" data-toggle="tab" href="#metro-related1"><img class="img-responsive" src="{{url('articulos/no-image.jpg')}}" alt="related1"></a>
                                    </li>
                                    <li>
                                        <a aria-expanded="false" data-toggle="tab" href="#metro-related2"><img class="img-responsive" src="{{url('articulos/no-image.jpg')}}" alt="related2"></a>
                                    </li>
                                    <li>
                                        <a aria-expanded="false" data-toggle="tab" href="#metro-related3"><img class="img-responsive" src="{{url('articulos/no-image.jpg')}}" alt="related3"></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                            <div class="inner-product-details-right">
                                <h3 id='modal-title'></h3>
                                @if($empresa->ecommerce->mostrar_precios)
                                    <p class="price" id='modal-precio'></p>
                                @endif
                                <p id="modal-descripcion"></p>
                                <div class="product-details-content">
                                    <p id='modal-codigo'></p>
                                    <p id='modal-stock'></p>
                                    <p id='modal-marca'></p>
                                    <p id='modal-modelo'></p>
                                </div>
                                <ul class="product-details-social">
                                    <li>Compartir:</li>
                                    <li><a href="#"><i aria-hidden="true" class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i aria-hidden="true" class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i aria-hidden="true" class="fa fa-linkedin"></i></a></li>
                                    <li><a href="#"><i aria-hidden="true" class="fa fa-pinterest"></i></a></li>
                                </ul>
                                <ul class="inner-product-details-cart">
                                    <li><a href="javascript:void(0)" id="modal-comprar" data-id="" class="comprar-producto">Comprar</a></li>
                                    <li>
                                        <div class="input-group quantity-holder" id="quantity-holder">
                                            <input type="text" placeholder="1" value="1" class="form-control quantity-input" name="quantity">
                                            <div class="input-group-btn-vertical">
                                                <button type="button" class="btn btn-default quantity-plus"><i aria-hidden="true" class="fa fa-plus"></i></button>
                                                <button type="button" class="btn btn-default quantity-minus"><i aria-hidden="true" class="fa fa-minus"></i></button>
                                            </div>
                                        </div>
                                    </li>
                                    <li><a href="#"><i class="fa fa-heart-o" aria-hidden="true"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <a href="#" class="btn-services-shop-now" data-dismiss="modal">Cerrar</a>
        </div>
    </div>
</div>

@section('after-scripts')
    <script>

        $("body").on("click", ".modal-producto", function () {
            var id = $(this).attr("data-id");
            $('#modal-comprar').attr("data-id", id)
            $.ajax({
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/ecommerce/modal-producto',
                data: {"id" : id},
                success: function( producto ) {
                    $('#modal-title').html(producto['titulo']);
                    $('#modal-descripcion').html(producto['descripcion']);
                    @if($empresa->ecommerce->mostrar_precios)
                    $('#modal-precio').html(producto['precio']);
                    @endif
                    $('#metro-related1').html(
                        '<a href="#"><img class="img-responsive" src="'+producto['imagen1']+'" alt="'+producto['nombre']+'"></a>'
                    );
                    $('#metro-related2').html(
                        '<a href="#"><img class="img-responsive" src="'+producto['imagen1']+'" alt="'+producto['nombre']+'"></a>'
                    );
                    $('#metro-related3').html(
                        '<a href="#"><img class="img-responsive" src="'+producto['imagen1']+'" alt="'+producto['nombre']+'"></a>'
                    );
                    if(producto['codigo'])
                    $('#modal-codigo').html('<span>Codigo:</span> '+producto['codigo']);
                    if(producto['stock'])
                    $('#modal-stock').html('<span>Stock:</span> '+producto['stock']);
                    if(producto['marca'])
                    $('#modal-marca').html('<span>Marca:</span> '+producto['marca']);
                    if(producto['modelo'])
                    $('#modal-modelo').html('<span>Modelo:</span> '+producto['modelo']);
                }
            });
        });

    </script>
@endsection