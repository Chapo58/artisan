@extends ('ecommerce.'.$estilo.'.layouts.app')

@section ('title', 'Listado de Articulos')

@section('content')
    <div class="inner-page-banner-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="breadcrumb-area">
                        <h1>{{$titulo}}</h1>
                        <ul>
                            {!! $ruta !!}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="shop-page-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3">
                    <div class="sidebar hidden-after-desk">
                        <h2 class="title-sidebar">CATEGORIAS</h2>
                        <div class="category-menu-area sidebar-section-margin" id="category-menu-area">
                            <ul>
                                @foreach($rubros as $rubro)
                                <li>
                                    <a href="{{url('ecommerce/categoria/'.$rubro->id)}}">
                                        <i class="flaticon-next"></i>{{$rubro}}
                                        @if($rubro->subrubros->first())<span><i class="flaticon-next"></i></span>@endif
                                    </a>
                                    @if($rubro->subrubros->first())
                                        <ul class="dropdown-menu">
                                            @foreach($rubro->subrubros as $subrubro)
                                                <li><a href="{{url('ecommerce/subcategoria/'.$subrubro->id)}}">{{$subrubro}}</a></li>
                                            @endforeach
                                        </ul>
                                    @endif
                                </li>
                                @endforeach
                            </ul>
                        </div>
                        <h2 class="title-sidebar">DESTACADOS</h2>
                        <div class="best-products sidebar-section-margin">
                            @foreach($listadoDestacados as $producto)
                                <div class="media">
                                    <a href="{{url('/ecommerce/producto/'.$producto->id)}}" class="pull-left">
                                        <img alt="{{$producto->nombre}}" src="{{url($producto->imagen_url)}}" style="max-width:107px;max-height:107px;" class="img-responsive">
                                    </a>
                                    <div class="media-body">
                                        <h3 class="media-heading"><a href="{{url('/ecommerce/producto/'.$producto->id)}}">{{$producto->nombre}}</a></h3>
                                        @if($empresa->ecommerce->mostrar_precios)
                                            <p>$ {{$producto->getPrecio()}}</p>
                                        @endif
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-8">
                            <div class="inner-shop-top-left">
                                <div class="dropdown">
                                    <button class="btn sorting-btn dropdown-toggle" type="button" data-toggle="dropdown">Orden por defecto<span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">Fecha</a></li>
                                        <li><a href="#">Mas Vendido</a></li>
                                        <li><a href="#">Mayor Precio</a></li>
                                        <li><a href="#">Menor Precio</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-4">
                            <div class="inner-shop-top-right">
                                <ul>
                                    <li class="active"><a href="#gried-view" data-toggle="tab" aria-expanded="false"><i class="fa fa-th-large"></i></a></li>
                                    <li><a href="#list-view" data-toggle="tab" aria-expanded="true"><i class="fa fa-list"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="row inner-section-space-top">
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active clear products-container" id="gried-view">
                                <div class="row">
                                    <?php $counter = 0; ?>
                                    @foreach($articulos as $item)
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                                            <div class="product-box1">
                                                <ul class="product-social">
                                                    <li data-id="{{$item->id}}" class="comprar-producto"><a href="javascript:void(0)"><i class="fa fa-shopping-cart" aria-hidden="true"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-heart-o" aria-hidden="true"></i></a></li>
                                                    <li data-id="{{$item->id}}" class="modal-producto"><a href="javascript:void(0)" data-toggle="modal" data-target="#myModal"><i class="fa fa-eye" aria-hidden="true"></i></a></li>
                                                </ul>
                                                <div class="product-img-holder">
                                                    @if($item->esNuevo())
                                                        <div class="hot-sale">
                                                            <span>Nuevo</span>
                                                        </div>
                                                    @endif
                                                    <a href="#"><img class="img-responsive" src="{{url($item->imagen_url)}}" alt="product"></a>
                                                </div>
                                                <div class="product-content-holder">
                                                    <h3><a href="{{url('/ecommerce/producto/'.$item->id)}}">{{$item->nombre}}</a></h3>
                                                    @if($empresa->ecommerce->mostrar_precios)
                                                        <span>$ {{$item->getPrecio()}}</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        @if(++$counter % 3 === 0)
                                            </div>
                                            <div class="row">
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                            <!-- List Style -->
                            <div role="tabpanel" class="tab-pane clear products-container" id="list-view">
                                @foreach($articulos as $item)
                                <div class="col-lg-12 col-md-12 col-sm-4 col-xs-12">
                                    <div class="product-box2">
                                        <div class="media">
                                            <a class="pull-left" href="#">
                                                <img class="img-responsive" style="max-width:273px;max-height:294px;" src="{{$item->imagen_url}}" alt="{{$item->nombre}}" />
                                            </a>
                                            <div class="media-body">
                                                <div class="product-box2-content">
                                                    <h3><a href="{{url('/ecommerce/producto/'.$item->id)}}">{{$item->nombre}}</a></h3>
                                                    @if($empresa->ecommerce->mostrar_precios)
                                                        <span>$ {{$item->getPrecio()}}</span>
                                                    @endif
                                                    <p>{{$item->descripcion}}</p>
                                                </div>
                                                <ul class="product-box2-cart">
                                                    <li data-id="{{$item->id}}" class="comprar-producto"><a href="javascript:void(0)">Agregar al Carrito</a></li>
                                                    <li><a href="#"><i class="fa fa-heart-o" aria-hidden="true"></i></a></li>
                                                    <li data-id="{{$item->id}}" class="modal-producto"><a href="javascript:void(0)" data-toggle="modal" data-target="#myModal"><i class="fa fa-eye" aria-hidden="true"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    {{--<div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <ul class="mypagination">
                                <li class="active"><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                            </ul>
                        </div>
                    </div>--}}
                </div>
            </div>
        </div>
    </div>

@endsection
