<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="csrf-token" content="{{ csrf_token() }}">

<title>@yield('title', app_name())</title>

<!-- Meta -->
<meta name="description" content="@yield('meta_description', 'Carrito de Compras')">
<meta name="author" content="@yield('meta_author', 'Luciano Ciattaglia')">
@yield('meta')

<!-- Favicon -->
<link rel="shortcut icon" href="{{{ asset('favicon.png') }}}">
<!-- For-Mobile-Apps-and-Meta-Tags -->
<meta name="mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-capable" content="yes">
<link rel="icon" sizes="192x192" href="{{{ asset('favicon.png') }}}">
<meta name="apple-mobile-web-app-title" content="Artisan">
<link href="{{{ asset('apple-touch-icon.png') }}}" rel="apple-touch-icon" />
<!-- //For-Mobile-Apps-and-Meta-Tags -->

<!-- Styles -->
@yield('before-styles')

{{ Html::style("css/ecommerce/normalize.css") }}
{{ Html::style("css/ecommerce/main.css") }}
{{ Html::style("css/ecommerce/bootstrap.min.css") }}
{{ Html::style("css/ecommerce/animate.min.css") }}
{{ Html::style("css/ecommerce/font-awesome.min.css") }}
{{ Html::style("css/ecommerce/fonts/flaticon.css") }}
{{ Html::style("css/ecommerce/owl.carousel.min.css") }}
{{ Html::style("css/ecommerce/owl.theme.default.min.css") }}
{{ Html::style("css/ecommerce/meanmenu.min.css") }}
{{ Html::style("js/ecommerce/plugins/custom-slider/css/nivo-slider.css") }}
{{ Html::style("js/ecommerce/plugins/custom-slider/css/preview.css") }}
{{ Html::style("css/ecommerce/style.css") }}
{{ Html::style("css/frontend/sweetalert2.min.css") }}

{{ Html::script("js/ecommerce/modernizr-2.8.3.min.js") }}

@yield('after-styles')