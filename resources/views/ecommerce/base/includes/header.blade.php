<header>
    <div class="header-area-style3" id="sticker">
        <div class="header-top">
            <div class="header-top-inner-top">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                            <div class="header-contact">
                                <ul>
                                    <li><i class="fa fa-phone" aria-hidden="true"></i><a href="tel:{{$empresa->ecommerce->telefono}}">{{$empresa->ecommerce->telefono}}</a></li>
                                    <li><i class="fa fa-envelope" aria-hidden="true"></i><a href="mailto:{{$empresa->ecommerce->email}}"> {{$empresa->ecommerce->email}}</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                            <div class="account-wishlist">
                                <ul>
                                    <li>
                                        @if(isset($user))
                                            <a href="{{url('ecommerce/dashboard')}}">
                                                <i class="fa fa-lock" aria-hidden="true"></i> Mi Cuenta
                                            </a>
                                        @else
                                            <a href="{{url('ecommerce/login')}}">
                                                <i class="fa fa-lock" aria-hidden="true"></i> Ingresar
                                            </a>
                                        @endif
                                    </li>
                                    <li><a href="#"><i class="fa fa-heart-o" aria-hidden="true"></i> Lista de Deseos</a></li>
                                    <li><a href="#"><i class="fa fa-usd" aria-hidden="true"></i> Pesos</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="header-top-inner-bottom">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <div class="logo-area">
                                <a href="/ecommerce"><img class="img-responsive" src="{{($empresa->ecommerce->logo_url) ? $empresa->ecommerce->logo() : $empresa->logo()}}" style="max-height: 32px;" alt="logo"></a>
                            </div>
                        </div>
                        {!! Form::open(['method' => 'POST', 'url' => '/ecommerce/buscar', 'role' => 'search'])  !!}
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                            <div class="search-area">
                                <div class="input-group" id="adv-search">
                                    <input type="text" name="palabra_clave" class="form-control" placeholder="Buscar Producto" />
                                    <div class="input-group-btn">
                                        <div class="btn-group" role="group">
                                            {{-- <div class="dropdown dropdown-lg">
                                                 <button type="button" class="btn btn-metro dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span>Todas las Categorias</span><i class="fa fa-caret-up" aria-hidden="true"></i><i class="fa fa-caret-down" aria-hidden="true"></i></button>
                                                 <div class="dropdown-menu dropdown-menu-right" role="menu">
                                                     <ul class="sidenav-nav">
                                                         @foreach($rubros as $rubro)
                                                             <li>
                                                                 <a href="{{url('ecommerce/categoria/'.$rubro->id)}}">
                                                                     <i class="flaticon-next"></i>{{$rubro}}
                                                                 </a>
                                                             </li>
                                                         @endforeach
                                                     </ul>
                                                 </div>
                                            </div>--}}
                                            <button type="submit" class="btn btn-metro-search"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                            <ul class="header-cart-area">
                                <li>
                                    <div class="cart-area">
                                        <a href="{{url('/ecommerce/carrito')}}"><i class="fa fa-shopping-cart" aria-hidden="true"></i><span id="carrito-cantidad">{{Cart::count()}}</span></a>
                                        <ul>
                                            <li id="articulos-carrito">
                                                @foreach(Cart::content() as $row)
                                                    <div class="cart-single-product ocultar">
                                                        <div class="media">
                                                            <div class="pull-left cart-product-img">
                                                                <a href="#">
                                                                    <img class="img-responsive" alt="product" src="{{url($row->model->imagen_url)}}">
                                                                </a>
                                                            </div>
                                                            <div class="media-body cart-content">
                                                                <ul>
                                                                    <li>
                                                                        <h2><a href="#">{{$row->name}}</a></h2>
                                                                        @if($empresa->ecommerce->mostrar_precios)
                                                                            <p>${{$row->price}}</p>
                                                                        @endif
                                                                    </li>
                                                                    <li>
                                                                        <p>X {{$row->qty}}</p>
                                                                    </li>
                                                                    <li data-id="{{$row->rowId}}" class="eliminar-producto">
                                                                        <a class="trash" href="#"><i class="fa fa-trash-o"></i></a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </li>
                                            @if($empresa->ecommerce->mostrar_precios)
                                            <li>
                                                <span><span>Sub Total</span></span><span id="carrito-subtotal">${{Cart::subtotal(2, ',', '.')}}</span>
                                                {{--<span><span>IVA</span></span><span id="carrito-iva">${{Cart::tax(2, ',', '.')}}</span>--}}
                                                <span><span>Total</span></span><span id="carrito-total">${{Cart::total(2, ',', '.')}}</span>
                                            </li>
                                            @endif
                                            <li>
                                                <ul class="checkout">
                                                    <li><a href="{{url('/ecommerce/carrito')}}" class="btn-checkout"><i class="fa fa-shopping-cart" aria-hidden="true"></i>Ver Carrito</a></li>
                                                    <li><a href="{{url('ecommerce/comprar')}}" class="btn-checkout"><i class="fa fa-share" aria-hidden="true"></i>Comprar</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                <li>
                                    <div class="additional-menu-area" id="additional-menu-area">
                                        <div id="mySidenav" class="sidenav">
                                            <a href="#" class="closebtn">×</a>
                                            <div class="sidenav-search">
                                                <div class="input-group stylish-input-group">
                                                    <input type="text" placeholder="Buscar . . ." class="form-control">
                                                    <span class="input-group-addon">
											                        <button type="submit">
											                            <span class="glyphicon glyphicon-search"></span>
                                                            </button>
                                                            </span>
                                                </div>
                                            </div>
                                            <ul class="sidenav-login-registration">
                                                <li data-toggle="collapse" data-target="#login" class="collapsed">
                                                    <a href="#">Ingresar<span class="arrow"></span></a>
                                                    <div class="collapse" id="login">
                                                        <div class="login-registration-field">
                                                            <form method="post">
                                                                <label>Email</label>
                                                                <input type="text">
                                                                <label>Contraseña</label>
                                                                <input type="password">
                                                                <button value="Ingresar" type="submit" class="btn-side-nav disabled">Ingresar</button>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li data-toggle="collapse" data-target="#registration" class="collapsed">
                                                    <a href="#">Registrarse<span class="arrow"></span></a>
                                                    <div class="collapse" id="registration">
                                                        <div class="login-registration-field">
                                                            <form method="post">
                                                                <label>Nombre</label>
                                                                <input type="text">
                                                                <label>Email</label>
                                                                <input type="email">
                                                                <label>Contraseña</label>
                                                                <input type="password">
                                                                <button value="Registrarse" type="submit" class="btn-side-nav disabled">Registrarse</button>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                            <h3 class="ctg-name-title">Categorias</h3>
                                            <ul class="sidenav-nav">
                                                @foreach($rubros as $rubro)
                                                    <li>
                                                        <a href="{{url('ecommerce/categoria/'.$rubro->id)}}">
                                                            <i class="flaticon-next"></i>{{$rubro}}
                                                        </a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                            <!-- times-->
                                        </div>
                                        <span class="side-menu-open side-menu-trigger"><i class="fa fa-bars" aria-hidden="true"></i></span>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-4">
                        <div class="logo-area">
                            <a href="/ecommerce"><img class="img-responsive" src="{{url($empresa->logo())}}" style="max-height: 32px;" alt="logo"></a>
                        </div>
                        <div class="category-menu-area" id="category-menu-area">
                            <h2 class="category-menu-title"><a href="#"><i class="fa fa-bars" aria-hidden="true"></i></a>Categorias</h2>
                            <ul class="category-menu-area-inner">
                                @foreach($rubros as $rubro)
                                <li>
                                    <a href="{{url('ecommerce/categoria/'.$rubro->id)}}"><i class="flaticon-next"></i>{{$rubro}}
                                        @if($rubro->subrubros->first())<span><i class="flaticon-next"></i></span>@endif
                                    </a>
                                    @if($rubro->subrubros->first())
                                    <ul class="dropdown-menu">
                                        @foreach($rubro->subrubros as $subrubro)
                                        <li><a href="{{url('ecommerce/subcategoria/'.$subrubro->id)}}">{{$subrubro}}</a></li>
                                        @endforeach
                                    </ul>
                                    @endif
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-9 col-md-9 col-sm-8">
                        <div class="main-menu-area">
                            <nav>
                                <ul>
                                    @foreach($paginas as $pagina)
                                        <li>
                                            <a href="{{url('ecommerce/pagina/'.$pagina->id)}}">
                                                {{$pagina}}
                                            </a>
                                        </li>
                                    @endforeach
                                    <li>
                                        <a href="{{url('ecommerce/contacto')}}">
                                            Contacto
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Mobile Menu Area Start Here -->
            <div class="mobile-menu-area">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="mobile-menu">
                                <nav id="dropdown">
                                    <ul>
                                        @foreach($rubros as $rubro)
                                        <li><a href="{{url('ecommerce/categoria/'.$rubro->id)}}">{{$rubro}}</a>
                                            @if($rubro->subrubros->first())
                                            <ul>
                                                @foreach($rubro->subrubros as $subrubro)
                                                    <li><a href="{{url('ecommerce/subcategoria/'.$subrubro->id)}}">{{$subrubro}}</a></li>
                                                @endforeach
                                            </ul>
                                            @endif
                                        </li>
                                        @endforeach
                                        <li><a href="{{url('ecommerce/contacto')}}">Contacto</a></li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Mobile Menu Area End Here -->
        </div>
    </div>
</header>
