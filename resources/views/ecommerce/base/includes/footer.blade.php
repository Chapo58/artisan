<footer>
    <div class="footer-area">
        <div class="footer-area-top">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="footer-box">
                            <h3>Informacion</h3>
                            <ul class="info-list">
                                <li><a href="{{url('ecommerce/informacion/privacidad')}}">Politicas de Privacidad</a></li>
                                <li><a href="{{url('ecommerce/informacion/terminos-condiciones')}}">Terminos y Condiciones</a></li>
                                <li><a href="{{url('ecommerce/informacion/contacto')}}">Contacto</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="footer-box">
                            @if(isset($user))
                                <h3>Mi Cuenta</h3>
                                <ul class="info-list">
                                    <li><a href="{{url('ecommerce/dashboard')}}">Mi Cuenta</a></li>
                                    <li><a href="{{url('ecommerce/historial')}}">Historial de Pedidos</a></li>
                                    <li><a href="{{url('ecommerce/lista-deseos')}}">Lista de Deseos</a></li>
                                    <li><a href="{{url('ecommerce/carrito')}}">Ver Carrito</a></li>
                                    <li><a href="{{url('ecommerce/logout')}}">Salir</a></li>
                                </ul>
                            @else
                                <h3>Ingresar</h3>
                                <ul class="info-list">
                                    <li><a href="{{url('ecommerce/login')}}">Registrarse</a></li>
                                    <li><a href="{{url('ecommerce/carrito')}}">Ver Carrito</a></li>
                                </ul>
                            @endif
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="footer-box">
                            <div class="newsletter-area">
                            <h3>Suscribete!</h3>
                                {{ Form::open(['id' => 'FormSuscribe']) }}
                                <div class="input-group stylish-input-group">
                                    <input type="text" name="email" id="EmailSuscripcion" class="form-control" placeholder="E-mail . . ." required="required">
                                    <span class="input-group-addon">
                                        <button type="submit">
                                            <i class="fa fa-chevron-right" aria-hidden="true"></i>
                                        </button>
                                    </span>
                                </div>
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="footer-box">
                            <h3>Conocenos...</h3>
                            <p>{{$empresa->ecommerce->descripcion}}.</p>
                            <ul class="footer-social">
                                @if($empresa->ecommerce->facebook)
                                <li>
                                    <a target="_blank" href="{{$empresa->ecommerce->facebook}}">
                                        <i class="fa fa-facebook" aria-hidden="true"></i>
                                    </a>
                                </li>
                                @endif
                                @if($empresa->ecommerce->twitter)
                                <li>
                                    <a target="_blank" href="{{$empresa->ecommerce->twitter}}">
                                        <i class="fa fa-twitter" aria-hidden="true"></i>
                                    </a>
                                </li>
                                @endif
                                @if($empresa->ecommerce->instagram)
                                <li>
                                    <a target="_blank" href="{{$empresa->ecommerce->instagram}}">
                                        <i class="fa fa-instagram" aria-hidden="true"></i>
                                    </a>
                                </li>
                                @endif
                                @if($empresa->ecommerce->google_plus)
                                <li>
                                    <a target="_blank" href="{{$empresa->ecommerce->google_plus}}">
                                        <i class="fa fa-google-plus" aria-hidden="true"></i>
                                    </a>
                                </li>
                                @endif
                                @if($empresa->ecommerce->linkedin)
                                    <li>
                                        <a target="_blank" href="{{$empresa->ecommerce->linkedin}}">
                                            <i class="fa fa-linkedin" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                @endif
                                @if($empresa->ecommerce->skype)
                                    <li>
                                        <a target="_blank" href="{{$empresa->ecommerce->skype}}">
                                            <i class="fa fa-skype" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-area-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                        <p>© {{date('Y')}} {{$empresa->ecommerce}} Todos los derechos reservados. <a href="https://artisan.com.ar" target="_blank"> Artisan Gestión</a></p>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <ul class="payment-method">
                            <li>
                                <a href="#"><img src="{{url('images/ecommerce/pago1.png')}}" alt="Efectivo"></a>
                            </li>
                            <li>
                                <a href="#"><img src="{{url('images/ecommerce/pago2.png')}}" alt="Tarjeta de Credito"></a>
                            </li>
                            <li>
                                <a href="#"><img src="{{url('images/ecommerce/pago3.png')}}" alt="Mercado Pago"></a>
                            </li>
                            <li>
                                <a href="#"><img src="{{url('images/ecommerce/pago4.png')}}" alt="PayPal"></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>