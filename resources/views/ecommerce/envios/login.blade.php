@extends ('ecommerce.layouts.app')

@section ('title', 'Iniciar Sesion')

@section('content')

        <div class="inner-page-banner-area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="breadcrumb-area">
                            <h1>Mi Cuenta</h1>
                            <ul>
                                <li><a href="{{ url('ecommerce') }}">Home</a> /</li>
                                <li>Cuenta</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Inner Page Banner Area End Here -->
        <!-- Login Registration Page Area Start Here -->
        <div class="login-registration-page-area">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        @include('includes.partials.messages')
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="login-registration-field">
                            <h2 class="cart-area-title">Ingresar</h2>
                            {{ Form::open(['route' => 'frontend.auth.login', 'class' => 'form-horizontal form-material', 'id' => 'loginform']) }}
                         
                                <label>Email *</label>
                                {{ Form::input('email', 'email', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.email'), 'required' => 'required']) }}

                                <label>Contraseña *</label>
                                {{ Form::input('password', 'password', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.password'), 'required' => 'required']) }}

                                 <a href="javascript:void(0)" id="to-recover" class="text-dark"><i class="fa fa-lock m-r-5"></i> {{trans('labels.frontend.passwords.forgot_password')}}</a>
                                <br>
                                 {{ Form::submit(isset($submitButtonText) ? $submitButtonText : trans('labels.frontend.auth.login_button'), ['class' => 'btn-send-message disabled']) }}
                            
                                <span><input type="checkbox" name="remember"/>Recordarme</span>
                            {!! Form::close() !!}
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="login-registration-field">
                            <h2 class="cart-area-title">Registrarse</h2>
                            {!! Form::open(['url' => '/ecommerce/registro', 'method' => 'POST', 'class' => 'form-horizontal']) !!}

                                <label>Nombre y Apellido *</label>
                                 {{ Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nombre y Apellido', 'required' => 'required']) }}

                                <label>Email *</label>
                                {{ Form::email('email', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.email'), 'required' => 'required']) }}

                                <label>Contraseña *</label>
                                {{ Form::password('password', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.password'), 'required' => 'required']) }}

                                <label>Confirmar Contraseña *</label>
                                {{ Form::password('password_confirmation', null, ['class' => 'form-control', 'placeholder' => 'Confirmar Contraseña', 'required' => 'required']) }}

                            {{ Form::submit(isset($submitButtonText) ? $submitButtonText : trans('labels.frontend.auth.register_button'), ['class' => 'btn-send-message disabled']) }}

                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection

