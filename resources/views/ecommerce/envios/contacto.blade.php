@extends ('ecommerce.'.$estilo.'.layouts.app')

@section ('title', 'Contacto')

@section('content')
<br>
    <section class="py-5 flex-center">
        <div class="container py-5">
            @include('includes.partials.messages')
            <h1 class="text-center pb-4">Contactanos</h1>
            {{ Form::open(['url' => '/ecommerce/contacto/mailContacto', 'method' => 'POST', 'class' => 'form-style-1 placeholder-1 col-12 py-3']) }}
            <div class="row m-0">
                <div class="col-lg-6 form-group">
                    <input id="frm-reg-nomap" class="form-control" name="nombre" type="text" placeholder="Nombre y apellido" required>
                </div>
                <div class="col-lg-6 form-group">
                    <input id="frm-reg-email" class="form-control" name="email" type="email" placeholder="E-mail" required>
                </div>
                <div class="col-12 form-group">
                    <textarea class="col-12 form-control" rows="5" name="mensaje" style="resize: none;" placeholder="Mensaje"></textarea>
                </div>
            </div>
            <div class="form-group text-center">
                <input type="submit" class="button-green" value="ENVIAR">
            </div>
            {{ Form::close() }}
        </div>
    </section>

@endsection
