<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        @include('ecommerce.'.$estilo.'.includes.head')
    </head>

    <body>

        <div class="wrapper-area">
            @include('ecommerce.'.$estilo.'.includes.header')

                @yield('content')

            @include('ecommerce.'.$estilo.'.includes.footer')
        </div>

        @include('ecommerce.'.$estilo.'.producto.modal')

        <div id="preloader"></div>

        @yield('before-scripts')

        @include('ecommerce.'.$estilo.'.includes.scripts')

        @yield('after-scripts')

    </body>
</html>
