@extends ('ecommerce.'.$estilo.'.layouts.app')

@section ('title', 'Carrito de Compras')

@section('content')

    <section class="main-cover h100	flex-center">
        <div class="col-lg-6 text-center z-i-1">
            <h3 class="text-grey">En Cañada de Gómez</h3>
            <h1 class="my-3">La forma más rápida y sencilla de comprar</h1>
            <button type="button" class="button-green">CONOCÉ MAS</button>
        </div>
    </section>
    <section class="py-5 flex-center">
        <div class="container py-5">
            <h1 class="text-center pb-4">Últimos comercios adheridos</h1>
            <div class="row pb-5 col-12 m-0 p-0">
                @foreach($rubros as $rubro)
                <div class="col-lg-3 col-md-6 p-lg-3 text-center py-lg-2 py-3">
                    <div class="inner">
                        <img src="{{($rubro->imagen_url) ? url($rubro->imagen_url) : ''}}" class="img-fluid">
                        <h5 class="mt-3 font-light text-dark-grey">{{$rubro->descripcion}}</h5>
                        <h3 class="my-0">{{$rubro}}</h3>
                        <button type="button" class="button-green my-3" onclick="location.href='{{url('ecommerce/categoria/'.$rubro->id)}}';">
                            Ver más
                        </button>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>

    <section class="index-banner flex-center">
        <div class="container text-center py-5">
            <h1>Te lo acerca a tu hogar</h1>
        </div>
    </section>

    <section class="py-5 flex-center">
        <div class="container py-5">
            <h1 class="text-center pb-4">Contactanos</h1>
            {{ Form::open(['url' => '/ecommerce/contacto/mailContacto', 'method' => 'POST', 'class' => 'form-style-1 placeholder-1 col-12 py-3']) }}
                <div class="row m-0">
                    <div class="col-lg-6 form-group">
                        <input id="frm-reg-nomap" class="form-control" name="nombre" type="text" placeholder="Nombre y apellido" required>
                    </div>
                    <div class="col-lg-6 form-group">
                        <input id="frm-reg-email" class="form-control" name="email" type="email" placeholder="E-mail" required>
                    </div>
                    <div class="col-12 form-group">
                        <textarea class="col-12 form-control" rows="5" name="mensaje" style="resize: none;" placeholder="Mensaje"></textarea>
                    </div>
                </div>
                <div class="form-group text-center">
                    <input type="submit" class="button-green" value="ENVIAR">
                </div>
            {{ Form::close() }}
        </div>
    </section>

@endsection
