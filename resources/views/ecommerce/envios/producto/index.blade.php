@extends ('ecommerce.'.$estilo.'.layouts.app')

@section ('title', $producto->nombre)

@section('content')
    <div class="inner-page-banner-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="breadcrumb-area">
                        <h1>{{$producto->nombre}}</h1>
                        <ul>
                            <li><a href="/ecommerce">Inicio</a> /</li>
                            <li>Productos /</li>
                            <li>{{$producto->nombre}}</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="product-details1-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 col-md-push-3">
                    <div class="inner-shop-details">
                        <div class="product-details-info-area">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <div class="inner-product-details-left">
                                        <div class="tab-content">
                                            <div class="tab-pane fade active in" id="related1">
                                                <a href="#" class="zoom ex1"><img alt="single" src="{{url($producto->imagen_url)}}" class="img-responsive"></a>
                                            </div>
                                            <div class="tab-pane fade" id="related2">
                                                <a href="#" class="zoom ex1"><img alt="single" src="{{url($producto->imagen_url)}}" class="img-responsive"></a>
                                            </div>
                                            <div class="tab-pane fade" id="related3">
                                                <a href="#" class="zoom ex1"><img alt="single" src="{{url($producto->imagen_url)}}" class="img-responsive"></a>
                                            </div>
                                        </div>
                                        <ul>
                                            <li class="active">
                                                <a href="#related1" data-toggle="tab" aria-expanded="false"><img alt="related1" src="{{url($producto->imagen_url)}}" class="img-responsive"></a>
                                            </li>
                                            <li>
                                                <a href="#related2" data-toggle="tab" aria-expanded="false"><img alt="related2" src="{{url($producto->imagen_url)}}" class="img-responsive"></a>
                                            </li>
                                            <li>
                                                <a href="#related3" data-toggle="tab" aria-expanded="false"><img alt="related3" src="{{url($producto->imagen_url)}}" class="img-responsive"></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <div class="inner-product-details-right">
                                        <h3>{{$producto->nombre}}</h3>
                                        @if($empresa->ecommerce->mostrar_precios)
                                            <p class="price">$ {{$producto->getPrecio()}}</p>
                                        @endif
                                        <p>{{$producto->descripcion}}</p>
                                        <div class="product-details-content">
                                            <p><span>Codigo Interno:</span> {{$producto->codigo}}</p>
                                            <p><span>Stock:</span> {{$producto->getStock()}}</p>
                                            <p><span>Modelo:</span> {{$producto->modelo}}</p>
                                        </div>
                                        <form id="checkout-form">
                                            <ul class="inner-product-details-cart">
                                                <li>
                                                    <div class="input-group quantity-holder" id="quantity-holder">
                                                        <input type="text" name='quantity' class="form-control quantity-input" value="1" placeholder="1">
                                                        <div class="input-group-btn-vertical">
                                                            <button class="btn btn-default quantity-plus" type="button"><i class="fa fa-plus" aria-hidden="true"></i></button>
                                                            <button class="btn btn-default quantity-minus" type="button"><i class="fa fa-minus" aria-hidden="true"></i></button>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li data-id="{{$producto->id}}" class="comprar-producto"><a href="javascript:void(0)">Comprar</a></li>
                                                <li><a href="#"><i aria-hidden="true" class="fa fa-heart-o"></i></a></li>
                                                <li><a href="#" data-toggle="modal" data-target="#myModal"><i class="fa fa-eye" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </form>
                                        <ul class="product-details-social">
                                            <li>Compartir en:</li>
                                            <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                            <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                            <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                            <li><a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="product-details-tab-area">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <ul>
                                        <li class="active"><a href="#descripcion" data-toggle="tab" aria-expanded="false">Descripción</a></li>
                                        <li><a href="#detalles" data-toggle="tab" aria-expanded="false">Detalles</a></li>
                                    </ul>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="tab-content">
                                        <div class="tab-pane fade active in" id="descripcion">
                                            <p>{{$producto->descripcion}}</p>
                                        </div>
                                        <div class="tab-pane fade" id="detalles">
                                            <p>

                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="featured-products-area2">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <h2 class="title-carousel">Productos Relacionados</h2>
                                </div>
                            </div>
                            <div class="metro-carousel" data-loop="true" data-items="3" data-margin="30" data-autoplay="true" data-autoplay-timeout="10000" data-smart-speed="2000" data-dots="false" data-nav="true" data-nav-speed="false" data-r-x-small="1" data-r-x-small-nav="true" data-r-x-small-dots="false" data-r-x-medium="2" data-r-x-medium-nav="true" data-r-x-medium-dots="false" data-r-small="2" data-r-small-nav="true" data-r-small-dots="false" data-r-medium="3" data-r-medium-nav="true" data-r-medium-dots="false" data-r-large="3" data-r-large-nav="true" data-r-large-dots="false">
                                @foreach($listadoRelacionados as $producto)
                                <div class="product-box1">
                                    <ul class="product-social">
                                        <li data-id="{{$producto->id}}" class="comprar-producto"><a href="javascript:void(0)"><i class="fa fa-shopping-cart" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fa fa-heart-o" aria-hidden="true"></i></a></li>
                                        <li data-id="{{$producto->id}}" class="modal-producto"><a href="javascript:void(0)" data-toggle="modal" data-target="#myModal"><i class="fa fa-eye" aria-hidden="true"></i></a></li>
                                    </ul>
                                    <div class="product-img-holder">
                                        @if($producto->esNuevo())
                                            <div class="hot-sale">
                                                <span>Nuevo</span>
                                            </div>
                                        @endif
                                        <a href="javascript:void(0)"><img src="{{url($producto->imagen_url)}}" alt="product"></a>
                                    </div>
                                    <div class="product-content-holder">
                                        <h3><a href="{{url('/ecommerce/producto/'.$producto->id)}}">{{$producto->nombre}}</a></h3>
                                        @if($empresa->ecommerce->mostrar_precios)
                                            <span>$ {{$producto->getPrecio()}}</span>
                                        @endif
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 col-md-pull-9">
                    <div class="sidebar hidden-before-tab">
                        <div class="category-menu-area sidebar-section-margin" id="category-menu-area">
                            <h2 class="title-sidebar">CATEGORIAS</h2>
                            <ul>
                                @foreach($rubros as $rubro)
                                    <li>
                                        <a href="{{url('ecommerce/categoria/'.$rubro->id)}}">
                                            <i class="flaticon-next"></i>{{$rubro}}
                                            @if($rubro->subrubros->first())<span><i class="flaticon-next"></i></span>@endif
                                        </a>
                                        @if($rubro->subrubros->first())
                                            <ul class="dropdown-menu">
                                                @foreach($rubro->subrubros as $subrubro)
                                                    <li><a href="{{url('ecommerce/subcategoria/'.$subrubro->id)}}">{{$subrubro}}</a></li>
                                                @endforeach
                                            </ul>
                                        @endif
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="best-products sidebar-section-margin">
                            <h2 class="title-sidebar">DESTACADOS</h2>
                            @foreach($listadoDestacados as $producto)
                                <div class="media">
                                    <a href="{{url('/ecommerce/producto/'.$producto->id)}}" class="pull-left">
                                        <img alt="{{$producto->nombre}}" src="{{url($producto->imagen_url)}}" style="max-width:107px;max-height:107px;" class="img-responsive">
                                    </a>
                                    <div class="media-body">
                                        <h3 class="media-heading"><a href="{{url('/ecommerce/producto/'.$producto->id)}}">{{$producto->nombre}}</a></h3>
                                        @if($empresa->ecommerce->mostrar_precios)
                                            <p>$ {{$producto->getPrecio()}}</p>
                                        @endif
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
