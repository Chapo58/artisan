@extends ('ecommerce.'.$estilo.'.layouts.app')

@section ('title', 'Listado de Articulos')

@section('content')
    <section class="py-5 bg-gastr flex-center">
        <h1 class="cover-title">{{$titulo}}</h1>
    </section>

    <section class="py-5 flex-center">
        <div class="container py-5">
            <h1 class="text-center text-dark-grey">PRODUCTOS</h1>
            <div class="d-flex row pb-5 col-12 m-0 p-0">
                @foreach($articulos as $item)
                <div class="col-lg-3 col-md-6 mt-lg-1 mt-5 p-lg-3 text-center">
                    <div class="inner mh-500 d-flex flex-column align-items-center justify-content-between">
                        <div class="col-12 m-0 p-0">
                            <img src="{{url($item->imagen_url)}}" class="img-fluid">
                            <h5 class="mt-3 font-light text-dark-grey">$ {{$item->getPrecio()}}</h5>
                            <h3 class="my-0">{{$item->nombre}}</h3>
                        </div>
                        <button data-toggle="modal" data-target="#productModal" type="button" class="button-green my-3">Ordenar</button>
                    </div>
                </div>
                @endforeach
            </div>
        </div>

        <!-- Product modal -->
        <div class="modal fade" id="productModal">
            <div class="modal-dialog modal-lg modal-dialog-centered">
                <div class="modal-content bg-transparent border-0">
                    <!-- Modal Header -->
                    <div class="modal-header border-0">
                        <button type="button" class="close text-light" data-dismiss="modal">&times;</button>
                    </div>
                    <!-- Modal body -->
                    <div class="modal-body bg-light row m-0 p-2">
                        <div class="row col-12 m-0 p-1">
                            <div class="col-lg-6 p-3 flex-center border bg-light">
                                <img src="img/junior-b-logo.png" class="col-10">
                            </div>
                            <div class="col-lg-6">
                                <div class="row">
                                    <h3 class="text-secondary col-12">Mex Food</h3>
                                    <h3 class="col-12">Fajitas para dos personas</h3>
                                    <h5 class="col-12 mb-3 color-grey">Seis tortillas de trigo para armar sabrosos vegetales salteados y trocitos de carne, acompañadas con salsas cottage, provenzal, guacamole y chili.</h5>
                                    <form id="registerForm" class="col-12 px-0 py-3">
                                        <div class="row m-0">
                                            <div class="col-12 form-group">
                                                <input id="frm-reg-password" type="text" placeholder="Cantidad">
                                            </div>
                                            <div class="col-12 form-group">
                                                <textarea placeholder="Comentarios"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group text-center pr-3">
                                            <input type="submit" class="float-right button-green mb-2" value="AGREGAR AL CARRITO">
                                            <input type="submit" class="float-right button-green" value="REALIZAR PEDIDO">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>

@endsection
