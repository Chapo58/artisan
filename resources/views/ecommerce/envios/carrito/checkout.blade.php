@extends ('ecommerce.'.$estilo.'.layouts.app')

@section ('title', 'Carrito de Compras')

@section('after-styles')
    {{ Html::style("css/ecommerce/select2.min.css") }}
@endsection

@section('content')

    <div class="inner-page-banner-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="breadcrumb-area">
                        <h1>Carrito</h1>
                        <ul>
                            <li><a href="/ecommerce">Inicio</a> /</li>
                            <li>Verificación</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="checkout-page-area">
        <div class="container">
            @include('includes.partials.messages')
        {!! Form::open(['url' => '/ecommerce/realizarCompra','methos' => 'POST', 'id' => 'checkout-form', 'files' => true]) !!}
        @if(!isset($user))
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="checkout-page-top">
                        <p><i class="fa fa-check" aria-hidden="true"></i><a href="/ecommerce/login"> ¿Eres Cliente? Ingresa aqui para inciar sesión</a></p>
                    </div>
                </div>
            </div>
            @endif
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="billing-details-area">
                        <h2 class="cart-area-title">DETALLES DE FACTURACIÓN</h2>
                            <div class="row">
                                <!-- First Name -->
                                <div class="col-lg-12 col-md-12 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label" for="first-name">Nombre Completo *</label>
                                        <input type="text" name="razon_social" id="first-name" class="form-control" value="{{(isset($user->cliente) ? $user->cliente->razon_social : '')}}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <!-- Email -->
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label" for="email">Dirección de E-mail *</label>
                                        <input type="text" name="email" id="email" class="form-control" value="{{(isset($user->cliente) ? $user->cliente->email : '')}}">
                                    </div>
                                </div>
                                <!-- Phone -->
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label" for="phone">Teléfono *</label>
                                        <input type="text" id="phone" name="telefono" class="form-control" value="{{(isset($user->cliente) ? $user->cliente->telefono : '')}}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <!-- Country -->
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label" for="country">Pais</label>
                                        <div class="custom-select">
                                            <select id="country" name="pais_id" class='select2'>
                                                @foreach($paises as $pais)
                                                    <option value="{{$pais->id}}">{{$pais}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <!-- Address -->
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Dirección</label>
                                        <input type="text" id="address-line1" name="direccion" class="form-control" value="{{(isset($user->cliente->domicilio) ? $user->cliente->domicilio->direccion : '')}}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <!-- Town / City -->
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label" for="town-city">Localidad</label>
                                        <input type="text" id="town-city" name="localidad" class="form-control" value="{{(isset($user->cliente->domicilio) ? $user->cliente->domicilio->localidad : '')}}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <!-- District -->
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label" for="district">Provincia</label>
                                        <div class="custom-select">
                                            <select id="district" name="provincia_id" class='select2'>
                                                <option value="0">Seleccionar Provincia</option>
                                                @foreach($provincias as $provincia)
                                                    <option value="{{$provincia->id}}">{{$provincia}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <!-- Postcode / ZIP -->
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label" for="postcode">Codigo Postal</label>
                                        <input type="text" id="postcode" name="codigo_postal" class="form-control" value="{{(isset($user->cliente->domicilio) ? $user->cliente->domicilio->codigo_postal : '')}}">
                                    </div>
                                </div>
                            </div>
                            @if(!isset($user))
                            <div class="row">
                                <!-- CREAT AN ACCOUNT? -->
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <span><input type="checkbox" name="crear_cuenta"/>¿CREAR UNA CUENTA?</span>
                                    </div>
                                </div>
                            </div>
                            @endif
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="order-sheet">
                        <h2>TU PEDIDO</h2>
                        <ul>
                            <?php $contador = 1; ?>
                            @foreach(Cart::content() as $row)
                                <li>
                                    {{$contador++}}.- {{$row->name}}
                                    <i>X {{$row->qty}}</i>
                                    @if($empresa->ecommerce->mostrar_precios)
                                        <span>${{$row->price * $row->qty}}</span>
                                    @endif
                                </li>
                            @endforeach
                        </ul>
                        @if($empresa->ecommerce->mostrar_precios)
                        <h3>Subtotal<span>$ {{Cart::subtotal(2, ',', '.')}}</span></h3>
                        <h3>Total<span>$ {{Cart::total(2, ',', '.')}}</span></h3>
                        @endif
                    </div>
                    <div class="ship-to-another-area">
                        <h2 class="cart-area-title">NOTAS DE LA COMPRA</h2>
                        <textarea class="form-control" rows="10" name="nota" placeholder="Aclaraciones relacionadas a esta compra, ej horarios recomendables de entregas, colores, etc" style="min-width: 100%"></textarea>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="payment-option">
                        @if($deposito->habilitado)
                        <div class="form-group">
                            <span><input type="radio" id="deposito" value="{{$deposito->id}}" name="pago"/><label for="deposito">DEPOSITO O TRANSFERENCIA BANCARIA</label></span>
                            @if($deposito->instrucciones)
                                <p>{!! $deposito->instrucciones !!}</p>
                            @endif
                        </div>
                        @endif
                        @if($efectivo->habilitado)
                        <div class="form-group">
                            <span><input type="radio" id="efectivo" value="{{$efectivo->id}}" name="pago"/><label for="efectivo">EFECTIVO</label></span>
                            @if($efectivo->instrucciones)
                                <p>{!! $efectivo->instrucciones !!}</p>
                            @endif
                        </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="pLace-order">
                        <button class="btn-send-message disabled" type="submit" value="Login">Realizar Pedido</button>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>

@endsection