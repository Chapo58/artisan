@extends ('ecommerce.'.$estilo.'.layouts.app')

@section ('title', 'Carrito de Compras')

@section('content')

    <div class="inner-page-banner-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="breadcrumb-area">
                        <h1>Carrito</h1>
                        <ul>
                            <li><a href="/ecommerce">Inicio</a> /</li>
                            <li>Carrito</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="cart-page-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    {!! Form::open(['url' => '/ecommerce/actualizar-carrito', 'method' => 'POST']) !!}
                    <div class="cart-page-top table-responsive">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <td class="cart-form-heading"></td>
                                    <td class="cart-form-heading">Producto</td>
                                    @if($empresa->ecommerce->mostrar_precios)
                                    <td class="cart-form-heading">Precio</td>
                                    @endif
                                    <td class="cart-form-heading">Cantidad</td>
                                    @if($empresa->ecommerce->mostrar_precios)
                                    <td class="cart-form-heading">Total</td>
                                    @endif
                                    <td class="cart-form-heading"></td>
                                </tr>
                            </thead>
                            <tbody id="quantity-holder">
                            @foreach(Cart::content() as $row)
                                <tr class="ocultar">
                                    <td class="cart-img-holder">
                                        <a href="{{url('/ecommerce/producto/'.$row->id)}}"><img src="{{url($row->model->imagen_url)}}" alt="cart" class="img-responsive"></a>
                                    </td>
                                    <td>
                                        <h3><a href="{{url('/ecommerce/producto/'.$row->id)}}">{{$row->name}}</a></h3>
                                    </td>
                                    @if($empresa->ecommerce->mostrar_precios)
                                    <td class="amount">${{$row->price}}</td>
                                    @endif
                                    <td class="quantity">
                                        <div class="input-group quantity-holder">
                                            <input type="text" name="detalles[{{$row->rowId}}][cantidad]" class="form-control quantity-input" value="{{$row->qty}}" placeholder="1">
                                            <div class="input-group-btn-vertical">
                                                <button class="btn btn-default quantity-plus" type="button"><i class="fa fa-plus" aria-hidden="true"></i></button>
                                                <button class="btn btn-default quantity-minus" type="button"><i class="fa fa-minus" aria-hidden="true"></i></button>
                                            </div>
                                        </div>
                                    </td>
                                    @if($empresa->ecommerce->mostrar_precios)
                                    <td class="amount">${{$row->price * $row->qty}}</td>
                                    @endif
                                    <td data-id="{{$row->rowId}}" class="dismiss eliminar-producto"><a href="javascript:void(0)"><i class="fa fa-times" aria-hidden="true"></i></a></td>
                                    {{ Form::hidden('detalles['.$row->rowId.'][rowId]', $row->rowId) }}
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="update-button">
                            <button class="btn-apply-coupon disabled" type="submit" value="Actualizar">Actualizar Carrito</button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12"></div>
                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                    <div class="cart-page-bottom-right">
                        @if($empresa->ecommerce->mostrar_precios)
                        <h2>Total</h2>
                        <h3>Subtotal<span>$ {{Cart::subtotal(2, ',', '.')}}</span></h3>
                        {{--<h3>Iva<span>$ {{Cart::tax(2, ',', '.')}}</span></h3>--}}
                        <h3>Total<span>$ {{Cart::total(2, ',', '.')}}</span></h3>
                        @endif
                        <div class="proceed-button">
                            <a href="/ecommerce/checkout" class="btn-apply-coupon disabled">Siguiente</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
