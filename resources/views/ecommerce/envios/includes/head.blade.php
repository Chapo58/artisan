<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="csrf-token" content="{{ csrf_token() }}">

<title>@yield('title', app_name())</title>

<!-- Meta -->
<meta name="description" content="@yield('meta_description', 'Carrito de Compras')">
<meta name="author" content="@yield('meta_author', 'Luciano Ciattaglia')">
@yield('meta')

<!-- Favicon -->
<link rel="shortcut icon" href="{{{ asset('e-commerce/envios/img/logo-simple.png') }}}">
<!-- For-Mobile-Apps-and-Meta-Tags -->
<meta name="mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-capable" content="yes">
<link rel="icon" sizes="192x192" href="{{{ asset('e-commerce/envios/img/logo-simple.png') }}}">
<meta name="apple-mobile-web-app-title" content="Artisan">
<link href="{{{ asset('e-commerce/envios/img/logo-simple.png') }}}" rel="apple-touch-icon" />
<!-- //For-Mobile-Apps-and-Meta-Tags -->

<!-- Styles -->
@yield('before-styles')

{{ Html::style("e-commerce/envios/css/style.css") }}
{{ Html::style("e-commerce/envios/css/bootstrap.min.css") }}

@yield('after-styles')