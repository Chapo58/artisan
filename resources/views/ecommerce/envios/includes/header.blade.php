<!-- login form -->
<div class="modal fade" id="loginRegisterModal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content bg-transparent  border-0 rounded-lg">
            <!-- Modal Header -->
            <div class="modal-header border-0">
                <button type="button" class="close text-light" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body bg-light row m-0 p-0">
                <button data-form="login" class="btn btn-active col-6 py-3 d-flex align-items-center justify-content-center"><i class="ion-android-exit pr-2 fs-2"></i><b>INICIAR SESIÓN</b></button>
                <button data-form="register" class="btn btn-disabled col-6 py-3 d-flex align-items-center justify-content-center"><i class="ion-android-person pr-2 fs-2"></i><b>REGISTRARSE</b></button>
                <!-- LOGIN FORM-->
                <form id="loginForm" class="form-style-1 placeholder-1 col-12 py-3">
                    <div class="row m-0">
                        <div class="col-12 form-group">
                            <input id="frm-log-email" type="email" placeholder="E-mail" required>
                        </div>
                        <div class="col-12 form-group">
                            <input id="frm-log-password" type="text" placeholder="Contraseña">
                        </div>
                        <p class="col-12 text-center">¿Olvidaste tu contraseña? <a href="#" class='text-success'>Recuperar contraseña</a></p>
                    </div>
                    <div class="form-group text-center">
                        <input type="submit" class="button-green" value="INGRESAR">
                    </div>
                </form>

                <!-- REGISTER FORM-->
                <form id="registerForm" class="hidden form-style-1 placeholder-1 col-12 py-3">
                    <div class="row m-0">
                        <div class="col-12 form-group">
                            <input id="frm-reg-nomap" type="text" placeholder="Nombre y apellido" required>
                        </div>
                        <div class="col-12 form-group">
                            <input id="frm-reg-email" type="email" placeholder="E-mail" required>
                        </div>
                        <div class="col-12 form-group">
                            <input id="frm-reg-password" type="text" placeholder="Contraseña">
                        </div>
                        <div class="col-12 form-group">
                            <input id="frm-reg-password-rep" type="text" placeholder="Repetir contraseña">
                        </div>
                        <!--p class="col-12 text-center"><input type="checkbox" id="frm-reg-tyc"> Acepto los <a href="#" class='text-success'>términos y condiciones</a></p-->
                    </div>
                    <div class="form-group text-center">
                        <input type="submit" class="button-green" value="ACEPTAR">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- cart modal -->
<div class="modal fade" id="cartModal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content bg-transparent rounded-lg border-0">
            <!-- Modal Header -->
            <div class="modal-header border-0">
                <button type="button" class="close text-light" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body bg-light row m-0 p-0">
                <div class="btn-disabled col-12 py-3 d-flex align-items-center justify-content-center"><i class="ion-android-cart pr-2 fs-2"></i><b>MIS COMPRAS</b></div>
                <!-- REGISTER FORM-->
                <form id="registerForm" class="form-style-1 placeholder-1 col-12 py-3">
                    <div class="row m-0">
                        <table class="table">
                            <thead>
                            <th class="w-50">DESCRIPCION</th><th>CANTIDAD</th><th>PRECIO</th>
                            </thead>
                            <thead>
                            <tr><td>Articulo 1</td><td>1</td><td>$100.00</td></tr>
                            <tr><td>Articulo 2</td><td>1</td><td>$150.00</td></tr>
                            <tr><td><b>TOTAL:</b></td><td></td><td>$250.00</td></tr>
                            </thead>
                        </table>
                    </div>
                    <div class="form-group text-center">
                        <a href="#" class="button-green p-2">FINALIZAR COMPRA</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<nav id="header" class="navbar navbar-expand-md navbar-dark pt-2 fixed-top fixed-nav">
    <div class="row p-0 col-lg-10 offset-lg-1 justify-content-between">
        <div class="col-2">
            <a class="navbar-brand" href="{{url('/ecommerce')}}"><img src="{{url('e-commerce/envios/img/logo.png')}}"></a>
        </div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
            <span class="icon icon-menu"></span>
        </button>

        <div class="collapse navbar-collapse pt-3 col-lg-10 px-0" id="collapsibleNavbar">
            <ul class="navbar-nav mx-auto pl-3">
                @foreach($paginas as $pagina)
                <li class="nav-item pt-2">
                    <a class="nav-link text-black" href="{{url('ecommerce/pagina/'.$pagina->id)}}" id="navbardrop">
                        {{$pagina}}
                    </a>
                </li>
                @endforeach
            </ul>
            <ul class="navbar-nav pl-3">
                <li class="nav-item ml-lg-auto">
                    <a data-toggle="modal" data-target="#loginRegisterModal" class="nav-link text-light" href="#"><button type="button" class="btn btn-dark-green"><i class="icon icon-user"></i></button></a>
                </li>
                <li class="nav-item">
                    <a data-toggle="modal" data-target="#cartModal" class="nav-link text-light" href="#"><button type="button" class="btn btn-dark-green"><i class="icon icon-cart"></i></button></a>
                </li>
            </ul>
        </div>
    </div>
</nav>