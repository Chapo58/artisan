{{ Html::script("e-commerce/envios/js/jquery.min.js") }}
{{ Html::script("e-commerce/envios/js/popper.min.js") }}
{{ Html::script("e-commerce/envios/js/bootstrap.min.js") }}
{{ Html::script("e-commerce/envios/js/login.js") }}

<script>
    $("body").on("click", ".comprar-producto", function () {
        var id = $(this).attr("data-id");
        var cantidad = $('.quantity-input').map(function() {
            return parseFloat(this.value);
        }).get().sort().pop();
        $.ajax({
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '/ecommerce/agregar-producto-carrito',
            data: {
                "id": id,
                "cantidad": cantidad
            },
            success: function( carrito ) {
                $("#articulos-carrito").append(
                    '<div class="cart-single-product">'+
                    '<div class="media">'+
                    '<div class="pull-left cart-product-img">'+
                    '<a href="#">'+
                    '<img class="img-responsive" alt="product" src="'+carrito['imagen']+'">'+
                    '</a>'+
                    '</div>'+
                    '<div class="media-body cart-content">'+
                    '<ul>'+
                    '<li>'+
                    '<h2><a href="#">'+carrito['titulo']+'</a></h2>'+
                    @if($empresa->ecommerce->mostrar_precios)
                        '<p>'+carrito['precio']+'</p>'+
                    @endif
                    '</li>'+
                    '<li>'+
                    '<p>X '+carrito['cantidad']+'</p>'+
                    '</li>'+
                    '<li data-id="'+carrito['rowId']+'" class="eliminar-producto">'+
                    '<a class="trash" href="#"><i class="fa fa-trash-o"></i></a>'+
                    '</li>'+
                    '</ul>'+
                    '</div>'+
                    '</div>'+
                    '</div>');

                $('#carrito-cantidad').html(carrito['cantidad-articulos']);
                @if($empresa->ecommerce->mostrar_precios)
                    $('#carrito-subtotal').html(carrito['subtotal']);
                    // $('#carrito-iva').html(carrito['iva']);
                    $('#carrito-total').html(carrito['total']);
                @endif

                swal({
                    title: "Producto agregado al carrito!",
                    timer: 2000,
                    type: "success",
                    showConfirmButton: false
                });
            },
            error: function () {
                var errMessage = self._formatString(self.options.messages.cannotLoadOptionsFor, fieldName);
                alert(errMessage);
            }
        });
    });

    $("body").on("click", ".eliminar-producto", function () {
        var id = $(this).attr("data-id");
        $(this).closest('.ocultar').hide();
        $.ajax({
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '/ecommerce/eliminar-producto-carrito',
            data: {"id" : id},
            success: function( carrito ) {
                $('#carrito-cantidad').html(carrito['cantidad-articulos']);
                @if($empresa->ecommerce->mostrar_precios)
                    $('#carrito-subtotal').html(carrito['subtotal']);
                    $('#carrito-iva').html(carrito['iva']);
                    $('#carrito-total').html(carrito['total']);
                @endif
            }
        });
    });

    $('#FormSuscribe').on('submit',function(event){
        event.preventDefault();
        data = $(this).serialize();
        $.ajax({
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '/ecommerce/nuevaSuscripcion',
            data: data,
            success: function() {
                $('#EmailSuscripcion').val('');
                swal({
                    title: "¡Gracias por Suscribirte!",
                    timer: 2000,
                    type: "success",
                    showConfirmButton: false
                });
            }
        });

    });
</script>