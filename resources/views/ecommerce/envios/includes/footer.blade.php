<footer>
    <div class="row m-0 bg-dark p-4">
        <div class="col-lg-4">
            <a class="logo" href="#"><img src="{{url('e-commerce/envios/img/logo.png')}}" width="200" alt="Logo"></a>
        </div>
        <div class="col-lg-4">
            <h4 class="mb-3"><span class="underline-green">Categorías</span></h4>
            @foreach($rubros as $rubro)
            <p><a href="{{url('ecommerce/categoria/'.$rubro->id)}}">{{$rubro}}</a></p>
            @endforeach
        </div>
        <div class="col-lg-4">
            <h4 class="mb-3"><span class="underline-green">Seguinos</span></h4>
            @if($empresa->ecommerce->facebook)
                <p><a target="_blank" href="{{$empresa->ecommerce->facebook}}"><i class="icon icon-facebook pr-2"></i>Facebook</a></p>
            @endif
            @if($empresa->ecommerce->instagram)
                <p><a target="_blank" href="{{$empresa->ecommerce->facebook}}"><i class="icon icon-instagram pr-2"></i>Instagram</a></p>
            @endif
        </div>
    </div>
    <p class="col-12 m-0 text-center py-2 text-small">&copy; {{date('Y')}}. Todos los derechos reservados. <a href="https://artisan.com.ar" target="_blank"> Artisan Gestión</a></p>
</footer>