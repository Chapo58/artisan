@extends ('ecommerce.'.$estilo.'.layouts.app')

@section ('title', 'Terminos y Condiciones')

@section('content')
    <div class="inner-page-banner-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="breadcrumb-area">
                        <h1>Terminos y Condiciones</h1>
                        <ul>
                            <li><a href="/ecommerce">Inicio</a> /</li>
                            <li>Terminos y Condiciones</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="about-us-page-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    {!! $empresa->ecommerce->terminos_condiciones !!}
                </div>
            </div>
        </div>
    </div>

@endsection
