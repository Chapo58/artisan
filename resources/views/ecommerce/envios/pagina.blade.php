@extends ('ecommerce.'.$estilo.'.layouts.app')

@section ('title', $pagina)

@section('content')
    <div class="inner-page-banner-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="breadcrumb-area">
                        <h1>{{$pagina}}</h1>
                        <ul>
                            <li><a href="/ecommerce">Inicio</a> /</li>
                            <li>{{$pagina}}</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="about-us-page-area">
        <div class="container">
            <div class="row">
                @if($pagina->imagen_url)
                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                        <div class="about-us-page-left">
                            <a href="#"><img src="{{url($pagina->imagen_url)}}" alt="Imagen" style="height:520px;width:432px;" class="img-responsive"></a>
                        </div>
                    </div>
                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                        <div class="about-us-page-right">
                            <h2>{{$pagina->titulo}}</h2>
                            {!! $pagina->contenido !!}
                        </div>
                    </div>
                @else
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h2>{{$pagina->titulo}}</h2>
                        {!! $pagina->contenido !!}
                    </div>
                @endif
            </div>
        </div>
    </div>

@endsection
