<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ access()->user()->picture }}" class="img-circle" alt="User Image" />
            </div><!--pull-left-->
            <div class="pull-left info">
                <p>{{ access()->user()->name }}</p>
                <!-- Status -->
                <a href="#"><i class="fa fa-circle text-success"></i> {{ trans('strings.backend.general.status.online') }}</a>
            </div><!--pull-left-->
        </div><!--user-panel-->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">{{ trans('menus.backend.sidebar.general') }}</li>

            <li class="{{ active_class(Active::checkUriPattern('admin/dashboard')) }}">
                <a href="{{ route('admin.dashboard') }}">
                    <i class="fa fa-dashboard"></i>
                    <span>{{ trans('menus.backend.sidebar.dashboard') }}</span>
                </a>
            </li>

            <li class="header">{{ trans('menus.backend.sidebar.system') }}</li>

            @role('Administrator')
            <li class="{{ active_class(Active::checkUriPattern('admin/access/*')) }} treeview">
                <a href="#">
                    <i class="fa fa-users"></i>
                    <span>{{ trans('menus.backend.access.title') }}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>

                <ul class="treeview-menu {{ active_class(Active::checkUriPattern('admin/access/*'), 'menu-open') }}" style="display: none; {{ active_class(Active::checkUriPattern('admin/access/*'), 'display: block;') }}">
                    @permission('manage-users')
                    <li class="{{ active_class(Active::checkUriPattern('admin/access/user*')) }}">
                        <a href="{{ route('admin.access.user.index') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('labels.backend.access.users.management') }}</span>
                        </a>
                    </li>
                    @endauth

                    <li class="{{ active_class(Active::checkUriPattern('admin/access/role*')) }}">
                        <a href="{{ route('admin.access.role.index') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('labels.backend.access.roles.management') }}</span>
                        </a>
                    </li>
                </ul>
            </li>

            <li class="{{ active_class(Active::checkUriPattern('admin/log-viewer*')) }} treeview">
                <a href="#">
                    <i class="fa fa-list"></i>
                    <span>{{ trans('menus.backend.log-viewer.main') }}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu {{ active_class(Active::checkUriPattern('admin/log-viewer*'), 'menu-open') }}" style="display: none; {{ active_class(Active::checkUriPattern('admin/log-viewer*'), 'display: block;') }}">
                    <li class="{{ active_class(Active::checkUriPattern('admin/log-viewer')) }}">
                        <a href="{{ route('log-viewer::dashboard') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('menus.backend.log-viewer.dashboard') }}</span>
                        </a>
                    </li>

                    <li class="{{ active_class(Active::checkUriPattern('admin/log-viewer/logs')) }}">
                        <a href="{{ route('log-viewer::logs.list') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('menus.backend.log-viewer.logs') }}</span>
                        </a>
                    </li>
                </ul>
            </li>
            @endauth

            <li class="{{ active_class(Active::checkUriPattern('admin/empresas*')) }} treeview">
                <a href="#">
                    <i class="fa fa-industry"></i>
                    <span>Empresas</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu {{ active_class(Active::checkUriPattern('admin/empresas*'), 'menu-open') }}" style="display: none; {{ active_class(Active::checkUriPattern('admin/empresas*'), 'display: block;') }}">
                    <li class="{{ active_class(Active::checkUriPattern('admin/empresas/empresa*')) }}">
                        <a href="{{ route('admin.empresas.empresa.index') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>Lista de Empresas</span>
                        </a>
                    </li>

                    @role('Administrator')
                        <li class="{{ active_class(Active::checkUriPattern('admin/empresas/distribuidor*')) }}">
                            <a href="{{ route('admin.empresas.distribuidor.index') }}">
                                <i class="fa fa-circle-o"></i>
                                <span>Lista de Distribuidores</span>
                            </a>
                        </li>
                        <li class="{{ active_class(Active::checkUriPattern('admin/empresas/factura-venta-automatica*')) }}">
                            <a href="{{ url('/admin/factura-venta-automatica/generar-facturas') }}">
                                <i class="fa fa-circle-o"></i>
                                <span>Generar Fact. Automáticas</span>
                            </a>
                        </li>
                    @endauth
                </ul>
            </li>

            <li class="{{ active_class(Active::checkUriPattern('admin/planes/plan')) }} treeview">
                <a href="#">
                    <i class="fa fa-paper-plane-o"></i>
                    <span>Planes</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu {{ active_class(Active::checkUriPattern('admin/planes/plan'), 'menu-open') }}" style="display: none; {{ active_class(Active::checkUriPattern('admin/planes*'), 'display: block;') }}">
                    <li class="{{ active_class(Active::checkUriPattern('admin/planes/plan')) }}">
                        <a href="{{ url('/admin/planes/plan') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>Lista de Planes</span>
                        </a>
                    </li>
                    <li class="{{ active_class(Active::checkUriPattern('admin/planes/efectivo')) }}">
                        <a href="{{ url('/admin/planes/efectivo') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>Efectivo</span>
                        </a>
                    </li>
                    <li class="{{ active_class(Active::checkUriPattern('admin/planes/deposito')) }}">
                        <a href="{{ url('/admin/planes/deposito') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>Deposito/Transferencia</span>
                        </a>
                    </li>
                    <li class="{{ active_class(Active::checkUriPattern('admin/planes/pago*')) }}">
                        <a href="{{ url('/admin/planes/pago') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>Pagos</span>
                        </a>
                    </li>
                </ul>
            </li>

            <li class="{{ active_class(Active::checkUriPattern('admin/configuraciones*')) }} treeview">
                <a href="#">
                    <i class="fa fa-cog"></i>
                    <span>Configuraciones</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>

                <ul class="treeview-menu {{ active_class(Active::checkUriPattern('admin/configuraciones/*'), 'menu-open') }}" style="display: none; {{ active_class(Active::checkUriPattern('admin/configuraciones/*'), 'display: block;') }}">
                    <li>
                        <a href="{{ url('admin/configuraciones/tawkto') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>Tawk.to</span>
                        </a>
                    </li>

                    <li>
                        <a href="{{ url('/admin/empresas/distribuidor/' . $user->distribuidor()->id . '/edit') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>Información Institucional</span>
                        </a>
                    </li>

                    @role('Administrator')
                    <li>
                        <a href="{{ url('admin/configuraciones/unidad') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>Unidades</span>
                        </a>
                    </li>

                    <li>
                        <a href="{{ url('admin/configuraciones/localidad') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>Localidades</span>
                        </a>
                    </li>

                    <li>
                        <a href="{{ url('admin/configuraciones/provincia') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>Provincia</span>
                        </a>
                    </li>

                    <li>
                        <a href="{{ url('admin/configuraciones/pais') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>Pais</span>
                        </a>
                    </li>

                    <li>
                        <a href="{{ url('admin/configuraciones/tips') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>Tips</span>
                        </a>
                    </li>

                    <li>
                        <a href="{{ url('admin/configuraciones/actualizar') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>Actualizar</span>
                        </a>
                    </li>
                    @endauth
                </ul>
            </li>
        </ul><!-- /.sidebar-menu -->
    </section><!-- /.sidebar -->
</aside>