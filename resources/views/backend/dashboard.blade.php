@extends('backend.layouts.app')

@section('page-header')
    <h1>
        {{ app_name() }}
        <small>{{ trans('strings.backend.dashboard.title') }}</small>
    </h1>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-green">
                <div class="inner">
                    <h3>{{$empresasActivas}}</h3>

                    <p>Empresas Activas</p>
                </div>
                <div class="icon">
                    <i class="fa fa-hourglass-2"></i>
                </div>
                <a href="{{url('/admin/empresas/empresa')}}" class="small-box-footer">Mas información <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-red">
                <div class="inner">
                    <h3>{{$empresasVencidas}}</h3>

                    <p>Empresas Vencidas</p>
                </div>
                <div class="icon">
                    <i class="fa fa-hourglass-3"></i>
                </div>
                <a href="{{url('/admin/empresas/empresa')}}" class="small-box-footer">Mas información <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3>{{$pagosPendientes}}</h3>

                    <p>Pagos Pendientes</p>
                </div>
                <div class="icon">
                    <i class="fa fa-history"></i>
                </div>
                <a href="{{url('/admin/planes/pago')}}" class="small-box-footer">Mas información <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3>$ {{number_format($ingresoMensual,2,',','.')}}</h3>

                    <p>Ingreso Mensual</p>
                </div>
                <div class="icon">
                    <i class="fa fa-money"></i>
                </div>
                <a href="{{url('/admin/planes/pago')}}" class="small-box-footer">Mas información <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
    </div>

    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">{{ trans('strings.backend.dashboard.welcome') }} {{ $user->name }}!</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div><!-- /.box tools -->
        </div><!-- /.box-header -->
        <div class="box-body">
            Este es el backend administrativo del sistema de gestion
        </div><!-- /.box-body -->
    </div><!--box box-success-->
@endsection