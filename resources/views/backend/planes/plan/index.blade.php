@extends ('backend.layouts.app')

@section ('title', trans('labels.frontend.plan_titulo')." - ".trans('navs.frontend.user.administration'))

@section('after-styles')
    {{ Html::style("css/backend/plugin/datatables/dataTables.bootstrap.min.css") }}
    {{ Html::style("css/frontend/sweetalert2.min.css") }}
@endsection

@section('content')
    <div class="main-content">
        <div class="main-content-inner">
            
            <div class="page-header">
                <h1>
                    {{ trans('labels.frontend.plan_titulo') }}
                    <small>
                        <i class="ace-icon fa fa-angle-double-right"></i>
                        <small>{{ trans('navs.frontend.user.administration') }}</small>
                    </small>

                    <div class="box-tools pull-right">
                        <a href="{{ url('/admin/planes/plan/create') }}" class="btn btn-success btn-sm" title="{{ trans('buttons.general.crud.create') }} {{ trans('labels.frontend.plan') }}">
                            <i class="fa fa-plus" aria-hidden="true"></i> {{ trans('buttons.general.crud.create') }} {{ trans('labels.frontend.plan') }}
                        </a>
                    </div><!--box-tools pull-right-->
                </h1>
            </div>

            {!! Form::open(['method' => 'GET', 'url' => '/admin/planes/plan', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
            <div class="input-group">
                <input type="text" class="form-control" name="search" placeholder="{{ trans('strings.backend.general.search_placeholder') }}">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="submit">
                        <i class="fa fa-search"></i>
                    </button>
                </span>
            </div>
            {!! Form::close() !!}

            <div class="row">
                <div class="col-xs-12">
                    <div class="table-responsive box box-success">
                        <table id="plan-table" class="table table-condensed table-hover">
                            <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Usuarios</th>
                                    <th>Puntos de Venta</th>
                                    <th>Sucursales</th>
                                    <th>Chat</th>
                                    <th>Precio</th>
                                    <th>{{ trans('labels.general.actions') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($planes as $item)
                                <tr>
                                    <td>{{ $item->nombre }}</td>
                                    <td>{{ $item->cantidad_usuarios }}</td>
                                    <td>{{ $item->cantidad_puntos_venta }}</td>
                                    <td>{{ $item->cantidad_sucursales }}</td>
                                    <td>
                                        @if($item->chat)
                                            <label class="label label-success">Sí</label>
                                        @else
                                            <label class="label label-danger">No</label>
                                        @endif
                                    </td>
                                    <td>{{ $item->precio }}</td>
                                    <td>
                                        <a href="{{ url('/admin/planes/plan/' . $item->id) }}" title="{{ trans('buttons.general.crud.show') }} {{ trans('labels.frontend.plan') }}">
                                            <button class="btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i> {{ trans('buttons.general.crud.show') }}</button>
                                        </a>
                                        @role('Administrador')
                                        <a href="{{ url('/admin/planes/plan/' . $item->id . '/edit') }}" title="{{ trans('buttons.general.crud.edit') }} {{ trans('labels.frontend.plan') }}">
                                            <button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> {{ trans('buttons.general.crud.edit') }}</button>
                                        </a>
                                        {!! Form::open([
                                            'method'=>'DELETE',
                                            'url' => ['/admin/planes/plan', $item->id],
                                            'style' => 'display:inline',
                                            'id' => 'form-delete-'.$item->id
                                        ]) !!}
                                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> '.trans("buttons.general.crud.delete"), array(
                                                    'type' => 'button',
                                                    'class' => 'btn btn-danger btn-xs',
                                                    'title' => trans("buttons.general.crud.delete")." ".trans("labels.frontend.plan"),
                                                    'onclick'=> 'eliminar("form-delete-'.$item->id.'")'
                                            )) !!}
                                        {!! Form::close() !!}
                                        @endauth
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="pagination-wrapper"> {!! $planes->appends(['search' => Request::get('search')])->render() !!} </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('after-scripts')
{{ Html::script("js/backend/plugin/datatables/jquery.dataTables.min.js") }}
{{ Html::script("js/backend/plugin/datatables/dataTables.bootstrap.min.js") }}
{{ Html::script("js/frontend/sweetalert2.min.js") }}

<script>
    $(document).ready(function() {
        $('#plan-table').DataTable( {
            "paging":   false,
            "info":     false,
            "searching":   false
        });
    } );

    function eliminar(idFormDelete){
        swal({
            title: "Eliminar {{ trans("labels.frontend.plan") }}",
            text: "¿Realmente desea eliminar este Plan? Esta accion es irreversible.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#ff8726',
            confirmButtonText: "Eliminar"
        }).then((result) => {
            if (result.value) {
                $('form#'+idFormDelete).submit();
            }
        });
    }
</script>

@endsection