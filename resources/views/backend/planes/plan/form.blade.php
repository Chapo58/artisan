<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Datos del Plan</h3>
    </div>
    <div class="box-body">
        <div class="form-group">
            <div class="{{ $errors->has('nombre') ? 'has-error' : ''}}">
                {!! Form::label('nombre', 'Nombre', ['class' => 'col-md-2 control-label']) !!}
                <div class="col-md-4">
                    {!! Form::text('nombre', null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('nombre', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="{{ $errors->has('cantidad_usuarios') ? 'has-error' : ''}}">
                {!! Form::label('cantidad_usuarios', 'Cant. Usuarios', ['class' => 'col-md-2 control-label']) !!}
                <div class="col-md-4">
                    {!! Form::number('cantidad_usuarios', null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('cantidad_usuarios', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="{{ $errors->has('cantidad_puntos_venta') ? 'has-error' : ''}}">
                {!! Form::label('cantidad_puntos_venta', 'Cant. Puntos de Venta', ['class' => 'col-md-2 control-label']) !!}
                <div class="col-md-4">
                    {!! Form::number('cantidad_puntos_venta', null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('cantidad_puntos_venta', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="{{ $errors->has('cantidad_sucursales') ? 'has-error' : ''}}">
                {!! Form::label('cantidad_sucursales', 'Cant. Sucursales', ['class' => 'col-md-2 control-label']) !!}
                <div class="col-md-4">
                    {!! Form::number('cantidad_sucursales', null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('cantidad_sucursales', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('chat', 'Chat', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-lg-1">
                {{ Form::checkbox('chat', '1', true) }}
            </div><!--col-lg-1-->
        </div><!--form control-->
        <div class="form-group">
            <div class="{{ $errors->has('precio') ? 'has-error' : ''}}">
                {!! Form::label('precio', 'Precio', ['class' => 'col-md-2 control-label']) !!}
                <div class="col-md-4">
                    {!! Form::number('precio', null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('precio', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="{{ $errors->has('link_mercadopago_mensual') ? 'has-error' : ''}}">
                {!! Form::label('link_mercadopago_mensual', 'Link Mercado Pago Mensual', ['class' => 'col-md-2 control-label']) !!}
                <div class="col-md-4">
                    {!! Form::text('link_mercadopago_mensual', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('link_mercadopago_mensual', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="{{ $errors->has('link_mercadopago_anual') ? 'has-error' : ''}}">
                {!! Form::label('link_mercadopago_anual', 'Link Mercado Pago Anual', ['class' => 'col-md-2 control-label']) !!}
                <div class="col-md-4">
                    {!! Form::text('link_mercadopago_anual', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('link_mercadopago_anual', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
        </div>
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-10 col-md-2">
        {{ Form::submit(isset($submitButtonText) ? $submitButtonText : trans('buttons.general.save'), array('class' => 'btn btn-primary btn-lg pull-right')) }}
    </div>
</div>
