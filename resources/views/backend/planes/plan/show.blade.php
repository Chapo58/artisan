@extends ('backend.layouts.app')

@section ('title', trans('labels.frontend.plan_titulo')." - ".trans('buttons.general.crud.show'))

@section('after-styles')
    {{ Html::style("css/frontend/sweetalert2.min.css") }}
@endsection

@section('content')
    <div class="main-content">
        <div class="main-content-inner">
            

            <div class="page-header">
                <h1>
                    {{ trans('labels.frontend.plan_titulo') }}
                    <small>
                        <i class="ace-icon fa fa-angle-double-right"></i>
                        <small>{{ trans('buttons.general.crud.show') }} Plan</small>
                        <small><i class="ace-icon fa fa-angle-double-right"></i></small>
                        <small>{{ $plan->nombre }}</small>
                    </small>

                    <div class="box-tools pull-right">
                        <a href="{{ url('/admin/planes/plan') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning btn-sm">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> {{ trans('buttons.general.crud.back') }}
                        </a>
                        @role('Administrador')
                        <a href="{{ url('/admin/planes/plan/' . $plan->id . '/edit') }}" title="{{ trans('buttons.general.crud.edit') }}" class="btn btn-primary btn-sm">
                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i> {{ trans('buttons.general.crud.edit') }}
                        </a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['admin/planes/plan/', $plan->id],
                            'style' => 'display:inline',
                            'id' => 'form-delete'
                        ]) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> '.trans("buttons.general.crud.delete"), array(
                                    'type' => 'button',
                                    'class' => 'btn btn-danger btn-sm',
                                    'title' => trans("buttons.general.crud.delete")." ".trans("labels.frontend.pago"),
                                    'onclick'=> 'eliminar()'
                            ))!!}
                        {!! Form::close() !!}
                        @endauth
                    </div>
                </h1>
            </div>

            <div class="row">
                <div class="col-xs-12">
                        <div class="box box-success">
                            <div class="box-header with-border">
                                <h3 class="box-title">Información Plan</h3>
                            </div>
                            <table class="table table-borderless">
                                <tbody>
                                <tr>
                                    <th> Nombre </th><td> {{ $plan->nombre }} </td>
                                    <th> Cant. Max. Usuarios </th><td> {{ $plan->cantidad_usuarios }} </td>
                                </tr>
                                <tr>
                                    <th> Puntos de Venta </th><td> {{ $plan->cantidad_puntos_venta }} </td>
                                    <th> Sucursales </th><td> {{ $plan->cantidad_sucursales }} </td>
                                </tr>
                                <tr>
                                    <th> Chat </th><td> {{ $plan->chat }} </td>
                                    <th> Precio </th><td> {{ $plan->precio }} </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('after-scripts')
    {{ Html::script("js/frontend/sweetalert2.min.js") }}

    <script type="text/javascript">
        function eliminar(){
            swal({
                title: "Eliminar {{ trans("labels.frontend.plan") }}",
                text: "¿Realmente desea eliminar este Plan? Esta accion es irreversible.",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#ff8726',
                confirmButtonText: "Eliminar"
            }).then((result) => {
                if (result.value) {
                    $('form#form-delete').submit();
                }
            });
        }
    </script>
@endsection