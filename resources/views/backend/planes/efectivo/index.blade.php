@extends ('backend.layouts.app')

@section ('title', 'Pagos en Efectivo')

@section('after-styles')
    {{ Html::style("css/backend/custom.css") }}
    {{ Html::style("css/backend/plugin/editor/bootstrap3-wysihtml5.min.css") }}
@endsection

@section('content')
    <div class="main-content">
        <div class="main-content-inner">
            
            <div class="page-header">
                <h1>
                    {{ trans('labels.frontend.plan_titulo') }}
                    <small>
                        <i class="ace-icon fa fa-angle-double-right"></i>
                        <small>Pagos </small>
                        <small><i class="ace-icon fa fa-angle-double-right"></i></small>
                        <small>Pagos en Efectivo</small>
                    </small>
                </h1>
            </div>

            <div class="row">
                <div class="col-xs-12">
                        {!! Form::model($efectivo, [
                            'method' => 'POST',
                            'url' => ['/admin/planes/efectivo/guardar'],
                            'class' => 'form-horizontal',
                            'files' => true
                        ]) !!}

                    <div class="box box-success">
                        <div class="box-header with-border">
                            <h3 class="box-title">Instrucciones para los pagos en Efectivo</h3>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <div class="{{ $errors->has('instrucciones') ? 'has-error' : ''}}">
                                            <div class="col-md-12">
                                                {!! Form::textarea('instrucciones', null, ['class' => 'form-control textarea', 'required' => 'required']) !!}
                                                {!! $errors->first('instrucciones', '<p class="help-block">:message</p>') !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('habilitado', 'Habilitado', ['class' => 'col-md-2 control-label']) !!}
                                        <div class="col-lg-1">
                                            <label class="custom-control custom-checkbox">
                                                {{ Form::checkbox('habilitado', 1, null, ['class' => 'custom-control-input', 'style' => 'display:none;']) }}
                                                <span class="custom-control-indicator"></span>
                                            </label>
                                        </div><!--col-lg-1-->
                                    </div><!--form control-->
                                </div>
                            </div>
                        </div>
                    </div>

                    @if($efectivo)
                        {{ Form::hidden('id', $efectivo->id) }}
                    @endif

                    <div class="form-group">
                        <div class="col-md-offset-10 col-md-2">
                            {{ Form::submit(isset($submitButtonText) ? $submitButtonText : trans('buttons.general.save'), array('class' => 'btn btn-primary btn-lg pull-right')) }}
                        </div>
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@endsection

@section('after-scripts')
    {{ Html::script("js/backend/plugin/editor/bootstrap3-wysihtml5.all.min.js") }}

    <script>
        $(function () {
            //bootstrap WYSIHTML5 - text editor
            $('.textarea').wysihtml5()
        })
    </script>

@endsection