@extends ('backend.layouts.app')

@section ('title', trans('labels.frontend.pago_titulo')." - ".trans('navs.frontend.user.administration'))

@section('after-styles')
    {{ Html::style("css/backend/plugin/datatables/dataTables.bootstrap.min.css") }}
    {{ Html::style("css/frontend/sweetalert2.min.css") }}
@endsection

@section('content')
    <div class="main-content">
        <div class="main-content-inner">
            
            <div class="page-header">
                <h1>
                    {{ trans('labels.frontend.pago_titulo') }}
                    <small>
                        <i class="ace-icon fa fa-angle-double-right"></i>
                        <small>{{ trans('navs.frontend.user.administration') }}</small>
                    </small>

                <!--<div class="box-tools pull-right">
                        <a href="{{ url('/admin/planes/pago/create') }}" class="btn btn-success btn-sm" title="{{ trans('buttons.general.crud.create') }} {{ trans('labels.frontend.pago') }}">
                            <i class="fa fa-plus" aria-hidden="true"></i> {{ trans('buttons.general.crud.create') }} {{ trans('labels.frontend.pago') }}
                        </a>
                    </div>-->
                </h1>
            </div>

            {!! Form::open(['method' => 'GET', 'url' => '/admin/planes/pago', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
            <div class="input-group">
                <input type="text" class="form-control" name="search" placeholder="{{ trans('strings.backend.general.search_placeholder') }}">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="submit">
                        <i class="fa fa-search"></i>
                    </button>
                </span>
            </div>
            {!! Form::close() !!}

            <div class="row">
                <div class="col-xs-12">
                    <div class="table-responsive box box-success">
                        <table id="pagos-table" class="table table-condensed table-hover">
                            <thead>
                                <tr>
                                    <th>Fecha</th>
                                    <th>Empresa</th>
                                    <th>Forma de Pago</th>
                                    <th>Importe</th>
                                    <th>Periodo</th>
                                    <th>Plan</th>
                                    <th>Estado</th>
                                    <th>{{ trans('labels.general.actions') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($pagos as $item)
                                <tr>
                                    <td>{{ $item->created_at->format('d/m/Y') }}</td>
                                    <td>{{ $item->empresa }}</td>
                                    <td>{{ $item->formaPago }}</td>
                                    <td>$ {{ number_format($item->importe,2,',','.') }}</td>
                                    <td>{{ $item->periodo }}</td>
                                    <td>{{ $item->plan }}</td>
                                    <td>
                                        <span class="label @if($item->estado == 0)label-warning @elseif($item->estado == 1)label-success @elseif($item->estado == 2)label-danger @else label-primary @endif">
                                            {{ $estadosPagos[$item->estado] }}
                                        </span>
                                    </td>
                                    <td>
                                        <a href="{{ url('/admin/planes/pago/' . $item->id) }}" title="{{ trans('buttons.general.crud.show') }} {{ trans('labels.frontend.pago') }}">
                                            <button class="btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i> {{ trans('buttons.general.crud.show') }}</button>
                                        </a>
                                        <a href="{{ url('/admin/planes/pago/' . $item->id . '/edit') }}" title="{{ trans('buttons.general.crud.edit') }} {{ trans('labels.frontend.pago') }}">
                                            <button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> {{ trans('buttons.general.crud.edit') }}</button>
                                        </a>
                                        @role('Administrador')
                                        {!! Form::open([
                                            'method'=>'DELETE',
                                            'url' => ['/admin/planes/pago', $item->id],
                                            'style' => 'display:inline',
                                            'id' => 'form-delete-'.$item->id
                                        ]) !!}
                                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> '.trans("buttons.general.crud.delete"), array(
                                                    'type' => 'button',
                                                    'class' => 'btn btn-danger btn-xs',
                                                    'title' => trans("buttons.general.crud.delete")." ".trans("labels.frontend.pago"),
                                                    'onclick'=> 'eliminar("form-delete-'.$item->id.'")'
                                            )) !!}
                                        {!! Form::close() !!}
                                        @endauth
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="pagination-wrapper"> {!! $pagos->appends(['search' => Request::get('search')])->render() !!} </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('after-scripts')
{{ Html::script("js/backend/plugin/datatables/jquery.dataTables.min.js") }}
{{ Html::script("js/backend/plugin/datatables/dataTables.bootstrap.min.js") }}
{{ Html::script("js/frontend/sweetalert2.min.js") }}

<script>
    $(document).ready(function() {
        $('#pagos-table').DataTable( {
            "paging":   false,
            "info":     false,
            "searching":   false
        });
    } );

    function eliminar(idFormDelete){
        swal({
            title: "Eliminar {{ trans("labels.frontend.pago") }}",
            text: "¿Realmente desea eliminar este Pago? Esta accion es irreversible.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#ff8726',
            confirmButtonText: "Eliminar"
        }).then((result) => {
            if (result.value) {
                $('form#'+idFormDelete).submit();
            }
        });
    }
</script>

@endsection