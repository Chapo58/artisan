<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">Información del Pago</h3>
    </div>
    <div class="box-body">
        <table class="table table-borderless">
            <tbody>
            <tr>
                <th> Empresa </th><td> {{ $pago->empresa }} </td>
                <th> Forma de Pago </th><td> {{ $pago->formaPago }} </td>
            </tr>
            <tr>
                <th> Plan </th><td> {{ $pago->plan }} </td>
                <th> Periodo </th><td> {{ $pago->periodo }} </td>
            </tr>
            <tr>
                <th> Importe </th><td>$ {{ number_format($pago->importe,2,',','.') }} </td>
            </tr>
            <tr>
                <th> Fecha Creación </th><td> {{ $pago->created_at->format('d/m/Y H:i:s') }} </td>
                <th> Fecha Modificación </th><td> {{ $pago->updated_at->format('d/m/Y H:i:s') }} </td>
            </tr>
            <tr>
                <th> Nota </th><td> {{ $pago->nota }} </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Estado del Pago</h3>
    </div>
    <div class="box-body">
        <div class="">
            <label class="radio-label">
                <input type="radio" class="radio-input radio-inline" name="estado" value="0" {{($pago->estado == 0) ? 'checked' : ''}}>
                <span class="outside"><span class="inside"></span></span>Pendiente
            </label>
            <label class="radio-label">
                <input type="radio" class="radio-input radio-inline" name="estado" value="1" {{($pago->estado == 1) ? 'checked' : ''}}>
                <span class="outside"><span class="inside"></span></span>Aceptado
            </label>
            <label class="radio-label">
                <input type="radio" class="radio-input radio-inline" name="estado" value="2" {{($pago->estado == 2) ? 'checked' : ''}}>
                <span class="outside"><span class="inside"></span></span>Rechazado
            </label>
        </div>
        <div class="form-group">
            <div class="{{ $errors->has('observaciones') ? 'has-error' : ''}}">
                {!! Form::label('observaciones', 'Observaciones', ['class' => 'col-md-2 control-label']) !!}
                <div class="col-md-4">
                    {!! Form::textarea('observaciones', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('observaciones', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
        </div>
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-10 col-md-2">
        {{ Form::submit(isset($submitButtonText) ? $submitButtonText : trans('buttons.general.save'), array('class' => 'btn btn-primary btn-lg pull-right')) }}
    </div>
</div>
