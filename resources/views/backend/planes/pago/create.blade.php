@extends ('backend.layouts.app')

@section ('title', trans('labels.frontend.plan_titulo')." - ".trans('buttons.general.crud.create'))

@section('content')
    <div class="main-content">
        <div class="main-content-inner">
            
            <div class="page-header">
                <h1>
                    {{ trans('labels.frontend.plan_titulo') }}
                    <small>
                        <i class="ace-icon fa fa-angle-double-right"></i>
                        <small>{{ trans('buttons.general.crud.create') }} {{ trans('labels.frontend.plan') }}</small>
                    </small>

                    <div class="box-tools pull-right">
                        <a href="{{ url('/admin/planes/plan') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning btn-sm">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> {{ trans('buttons.general.crud.back') }}
                        </a>
                    </div>
                </h1>
            </div>

            <div class="row">
                        <div class="col-xs-12">

                    @include('includes.partials.messages')

                    {!! Form::open(['url' => '/admin/planes/plan', 'class' => 'form-horizontal', 'files' => true]) !!}
                        @include ('backend.planes.plan.form')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
