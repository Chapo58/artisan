@extends ('backend.layouts.app')

@section ('title', trans('labels.frontend.pago_titulo')." - ".trans('buttons.general.crud.show'))

@section('after-styles')
    {{ Html::style("css/frontend/sweetalert2.min.css") }}
@endsection

@section('content')
    <div class="main-content">
        <div class="main-content-inner">
            

            <div class="page-header">
                <h1>
                    {{ trans('labels.frontend.pago_titulo') }}
                    <small>
                        <i class="ace-icon fa fa-angle-double-right"></i>
                        <small>{{ trans('buttons.general.crud.show') }} Plan</small>
                        <small><i class="ace-icon fa fa-angle-double-right"></i></small>
                        <small>{{ $pago }}</small>
                    </small>

                    <div class="box-tools pull-right">
                        <a href="{{ url('/admin/planes/pago') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning btn-sm">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> {{ trans('buttons.general.crud.back') }}
                        </a>
                        @role('Administrador')
                        <a href="{{ url('/admin/planes/pago/' . $pago->id . '/edit') }}" title="{{ trans('buttons.general.crud.edit') }}" class="btn btn-primary btn-sm">
                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i> {{ trans('buttons.general.crud.edit') }}
                        </a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['admin/planes/pago/', $pago->id],
                            'style' => 'display:inline',
                            'id' => 'form-delete'
                        ]) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> '.trans("buttons.general.crud.delete"), array(
                                    'type' => 'button',
                                    'class' => 'btn btn-danger btn-sm',
                                    'title' => trans("buttons.general.crud.delete")." ".trans("labels.frontend.pago"),
                                    'onclick'=> 'eliminar()'
                            ))!!}
                        {!! Form::close() !!}
                        @endauth
                    </div>
                </h1>
            </div>

            <div class="row">
                <div class="col-xs-12">
                        <div class="box box-success">
                            <div class="box-header with-border">
                                <h3 class="box-title">Información del Pago</h3>
                            </div>
                            <table class="table table-borderless">
                                <tbody>
                                <tr>
                                    <th> Empresa </th><td> {{ $pago->empresa }} </td>
                                    <th> Forma de Pago </th><td> {{ $pago->formaPago }} </td>
                                </tr>
                                <tr>
                                    <th> Plan </th><td> {{ $pago->plan }} </td>
                                    <th> Periodo </th><td> {{ $pago->periodo }} </td>
                                </tr>
                                <tr>
                                    <th> Importe </th><td>$ {{ number_format($pago->importe,2,',','.') }} </td>
                                    <th> Estado </th>
                                    <td>
                                        <span class="label @if($pago->estado == 0)label-warning @elseif($pago->estado == 1)label-success @elseif($pago->estado == 2)label-danger @else label-primary @endif">
                                            {{ $estadosPagos[$pago->estado] }}
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <th> Fecha Creación </th><td> {{ $pago->created_at->format('d/m/Y H:i:s') }} </td>
                                    <th> Fecha Modificación </th><td> {{ $pago->updated_at->format('d/m/Y H:i:s') }} </td>
                                </tr>
                                <tr>
                                    <th> Nota </th><td> {{ $pago->nota }} </td>
                                </tr>
                                <tr>
                                    <th> Observaciones </th><td> {{ $pago->observaciones }} </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('after-scripts')
    {{ Html::script("js/frontend/sweetalert2.min.js") }}

    <script type="text/javascript">
        function eliminar(){
            swal({
                title: "Eliminar {{ trans("labels.frontend.pago") }}",
                text: "¿Realmente desea eliminar este Pago? Esta accion es irreversible.",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#ff8726',
                confirmButtonText: "Eliminar"
            }).then((result) => {
                if (result.value) {
                    $('form#form-delete').submit();
                }
            });
        }
    </script>
@endsection