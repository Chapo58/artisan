@extends ('backend.layouts.app')

@section ('title', trans('labels.frontend.pago_titulo')." - ".trans('buttons.general.crud.edit'))

@section('after-styles')
    {{ Html::style("css/backend/custom.css") }}
@endsection

@section('content')
    <div class="main-content">
        <div class="main-content-inner">
            
            <div class="page-header">
                <h1>
                    {{ trans('labels.frontend.pago_titulo') }}
                    <small>
                        <i class="ace-icon fa fa-angle-double-right"></i>
                        <small>{{ trans('buttons.general.crud.edit') }} Pagos </small>
                        <small><i class="ace-icon fa fa-angle-double-right"></i></small>
                        <small>{{ $pago }}</small>
                    </small>

                    <div class="box-tools pull-right">
                        <a href="{{ url('/admin/planes/pago') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning btn-sm">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> {{ trans('buttons.general.crud.back') }}
                        </a>
                    </div>
                </h1>
            </div>

            <div class="row">
                <div class="col-xs-12">

                    @include('includes.partials.messages')

                    {!! Form::model($pago, [
                        'method' => 'PATCH',
                        'url' => ['/admin/planes/pago', $pago->id],
                        'class' => 'form-horizontal',
                        'files' => true
                    ]) !!}

                        @include ('backend.planes.pago.form', ['submitButtonText' => trans("buttons.general.crud.update")])

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@endsection
