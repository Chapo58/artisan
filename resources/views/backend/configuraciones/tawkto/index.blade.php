@extends ('backend.layouts.app')

@section ('title', 'Tawk.to')

@section('after-styles')
    {{ Html::style("css/backend/custom.css") }}
@endsection

@section('content')
    <div class="main-content">
        <div class="main-content-inner">
            
            <div class="page-header">
                <h1>
                    Chat
                    <small>
                        <i class="ace-icon fa fa-angle-double-right"></i>
                        <small>Configuraciones </small>
                        <small><i class="ace-icon fa fa-angle-double-right"></i></small>
                        <small>Tawk.to</small>
                    </small>
                </h1>
            </div>

            <div class="row">
                <div class="col-xs-12">
                        {!! Form::model($tawk, [
                            'method' => 'POST',
                            'url' => ['/admin/configuraciones/tawkto/guardar'],
                            'class' => 'form-horizontal',
                            'files' => true
                        ]) !!}

                    <div class="box box-success">
                        <div class="box-header with-border">
                            <h3 class="box-title">Codigo del Widget de Tawk.to</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <div class="{{ $errors->has('codigo') ? 'has-error' : ''}}">
                                    {!! Form::label('codigo', 'Codigo', ['class' => 'col-md-2 control-label']) !!}
                                    <div class="col-md-4">
                                        {!! Form::textarea('codigo', null, ['class' => 'form-control', 'required' => 'required']) !!}
                                        {!! $errors->first('codigo', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                {!! Form::label('habilitado', 'Habilitado', ['class' => 'col-md-2 control-label']) !!}
                                <div class="col-lg-1">
                                    <label class="custom-control custom-checkbox">
                                        {{ Form::checkbox('habilitado', 1, null, ['class' => 'custom-control-input', 'style' => 'display:none;']) }}
                                        <span class="custom-control-indicator"></span>
                                    </label>
                                </div><!--col-lg-1-->
                            </div><!--form control-->
                        </div>
                    </div>

                    @if($tawk)
                        {{ Form::hidden('id', $tawk->id) }}
                    @endif

                    <div class="form-group">
                        <div class="col-md-offset-10 col-md-2">
                            {{ Form::submit(isset($submitButtonText) ? $submitButtonText : trans('buttons.general.save'), array('class' => 'btn btn-primary btn-lg pull-right')) }}
                        </div>
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@endsection
