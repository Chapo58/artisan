<div class="modal fade" id="modal-provincia-nueva" tabindex="-1" role="dialog" aria-labelledby="provinciaModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="provinciaModalLabel">Nuevo Provincia</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    {!! Form::label('nombre_provincia', 'Nombre') !!}
                    {!! Form::text('nombre_provincia', '',['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="modal-footer">
                <button id="guardar-provincia" type="button" class="btn btn-themecolor">Guardar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>

@section('scripts-dialog-provincia')
    <script>
        $("#agregar-provincia").click(function(){
            $('input#nombre_provincia').parent().removeClass('has-error');
            $('input#nombre_provincia').val('');
            $('#modal-provincia-nueva').modal();
        });

        $("#guardar-provincia").click(function(){
            $('input#nombre_provincia').parent().removeClass('has-error');
            if($('input#nombre_provincia').val() == ''){
                $('input#nombre_provincia').parent().addClass('has-error');
                $('input#nombre_provincia').focus();
            }else{
                $('#modal-provincia-nueva').modal('hide');
                guardarNuevaProvincia();
            }
        });

        function guardarNuevaProvincia() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $( "input[name*='_token']" ).val()
                }
            });
            var nombre = $('input#nombre_provincia').val();
            var pais_id = $('select#pais_id option:selected').val();
            $.ajax({
                type: "POST",
                url: '{{ url("/configuraciones/provincia/guardarNuevaProvincia") }}',
                data: {nombre: nombre, pais_id: pais_id},
                success: function( msg ) {
                    $('select#provincia_id').append($('<option>', {
                        value: msg.id,
                        text: msg.nombre
                    }));
                    $('select#provincia_id').val(msg.id);
                }
            });
        }
    </script>
@endsection