<div class="form-group {{ $errors->has('nombre') ? 'has-error' : ''}}">
    {!! Form::label('nombre', 'Nombre', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('nombre', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('nombre', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('pais_id') ? 'has-error' : ''}}">
    {!! Form::label('pais_id', 'Pais Id', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::select('pais_id', $paises,null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('pais_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {{ Form::submit(isset($submitButtonText) ? $submitButtonText : trans('buttons.general.save'), array('class' => 'btn btn-primary')) }}
    </div>
</div>
