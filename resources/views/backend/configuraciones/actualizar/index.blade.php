@extends ('backend.layouts.app')

@section ('title', 'Actualizar Sistema')

@section('after-styles')
    {{ Html::style("css/backend/custom.css") }}
@endsection

@section('content')
    <div class="main-content">
        <div class="main-content-inner">
            
            <div class="page-header">
                <h1>
                    Actualizar
                    <small>
                        <i class="ace-icon fa fa-angle-double-right"></i>
                        <small>Configuraciones </small>
                        <small><i class="ace-icon fa fa-angle-double-right"></i></small>
                        <small>Actualizar Sistema</small>
                    </small>
                </h1>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    {!! Form::open(['method' => 'POST', 'url' => '/admin/configuraciones/actualizar/actualizar', 'class' => 'form-horizontal']) !!}

                    <div class="box box-success">
                        <div class="box-header with-border">
                            <h3 class="box-title">Actualizar Sistema</h3>
                        </div>
                        <div class="box-body">
                            {{ Form::submit('ACTUALIZAR SISTEMA', array('class' => 'btn btn-primary btn-lg')) }}
                        </div>
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@endsection
