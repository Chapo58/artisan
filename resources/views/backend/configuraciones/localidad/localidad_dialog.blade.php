<div class="modal fade" id="modal-localidad-nueva" tabindex="-1" role="dialog" aria-labelledby="localidadModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="localidadModalLabel">Nueva Localidad</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    {!! Form::label('nombre_localidad', 'Nombre') !!}
                    {!! Form::text('nombre_localidad', '',['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="modal-footer">
                <button id="guardar-localidad" type="button" class="btn btn-themecolor">Guardar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>

@section('scripts-dialog-localidad')
    <script>
        $("#agregar-localidad").click(function(){
            $('input#nombre_localidad').parent().removeClass('has-error');
            $('input#nombre_localidad').val('');
            $('#modal-localidad-nueva').modal();
        });

        $("#guardar-localidad").click(function(){
            $('input#nombre_localidad').parent().removeClass('has-error');
            if($('input#nombre_localidad').val() == ''){
                $('input#nombre_localidad').parent().addClass('has-error');
                $('input#nombre_localidad').focus();
            }else{
                $('#modal-localidad-nueva').modal('hide');
                guardarNuevaLocalidad();
            }
        });

        function guardarNuevaLocalidad() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $( "input[name*='_token']" ).val()
                }
            });
            var nombre = $('input#nombre_localidad').val();
            var provincia_id = $('select#provincia_id option:selected').val()
            $.ajax({
                type: "POST",
                url: '{{ url("/configuraciones/localidad/guardarNuevaLocalidad") }}',
                data: {nombre: nombre, provincia_id: provincia_id},
                success: function( msg ) {
                    $('select#localidad_id').append($('<option>', {
                        value: msg.id,
                        text: msg.nombre
                    }));
                    $('select#localidad_id').val(msg.id);
                }
            });
        }
    </script>
@endsection