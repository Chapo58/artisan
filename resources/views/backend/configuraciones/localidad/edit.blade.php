@extends ('backend.layouts.app')

@section ('title', trans('labels.frontend.localidad_titulo')." - ".trans('buttons.general.crud.edit'))

@section('content')
    <div class="main-content">
        <div class="main-content-inner">
            
            <div class="page-header">
                <h1>
                    {{ trans('labels.frontend.localidad_titulo') }}
                    <small>
                        <i class="ace-icon fa fa-angle-double-right"></i>
                        <small>{{ trans('buttons.general.crud.edit') }} Localidad </small>
                        <small><i class="ace-icon fa fa-angle-double-right"></i></small>
                        <small>{{ $localidad->id }}# {{ $localidad }}</small>
                    </small>

                    <div class="box-tools pull-right">
                        <a href="{{ url('/admin/configuraciones/localidad') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning btn-sm">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> {{ trans('buttons.general.crud.back') }}
                        </a>
                    </div>
                </h1>
            </div>

            <div class="row">
                <div class="col-xs-12">

                    @include('includes.partials.messages')

                    {!! Form::model($localidad, [
                        'method' => 'PATCH',
                        'url' => ['/admin/configuraciones/localidad', $localidad->id],
                        'class' => 'form-horizontal',
                        'files' => true
                    ]) !!}

                        @include ('backend.configuraciones.localidad.form', ['submitButtonText' => trans("buttons.general.crud.update")])

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
