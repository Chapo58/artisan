@extends('backend.layouts.app')

@section ('title', trans('labels.frontend.unidad_titulo')." - ".trans('buttons.general.crud.show'))

@section('content')
    <div class="main-content">
        <div class="main-content-inner">
            

            <div class="page-header">
                <h1>
                    {{ trans('labels.frontend.unidad_titulo') }}
                    <small>
                        <i class="ace-icon fa fa-angle-double-right"></i>
                        <small>{{ trans('buttons.general.crud.show') }} unidad</small>
                        <small><i class="ace-icon fa fa-angle-double-right"></i></small>
                        <small>{{ $unidad->id }}# {{ $unidad }}</small>
                    </small>

                    <div class="box-tools pull-right">
                        <a href="{{ url('/admin/configuraciones/unidad') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning btn-sm">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> {{ trans('buttons.general.crud.back') }}
                        </a>
                        <a href="{{ url('/admin/configuraciones/unidad/' . $unidad->id . '/edit') }}" title="{{ trans('buttons.general.crud.edit') }}" class="btn btn-primary btn-sm">
                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i> {{ trans('buttons.general.crud.edit') }}
                        </a>
                    </div>
                </h1>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-borderless">
                        <tbody>
                            <tr>
                                <th>ID</th><td>{{ $unidad->id }}</td>
                            </tr>
                            <tr><th> Nombre </th><td> {{ $unidad->nombre }} </td></tr><tr><th> Presentacion </th><td> {{ $unidad->presentacion }} </td></tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
