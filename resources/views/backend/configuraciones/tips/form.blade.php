<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Gestion de Tips del Sistema</h3>
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-xs-12">
                <div class="form-group {{ $errors->has('ruta') ? 'has-error' : ''}}">
                    {!! Form::label('ruta', 'Ruta', ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::text('ruta', null, ['class' => 'form-control', 'required' => 'required']) !!}
                        {!! $errors->first('ruta', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>

                <div class="form-group {{ $errors->has('texto') ? 'has-error' : ''}}">
                    <div class="{{ $errors->has('texto') ? 'has-error' : ''}}">
                        <div class="col-md-12">
                            {!! Form::textarea('texto', null, ['class' => 'form-control textarea', 'required' => 'required']) !!}
                            {!! $errors->first('texto', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-10 col-md-2">
        {{ Form::submit(isset($submitButtonText) ? $submitButtonText : trans('buttons.general.save'), array('class' => 'btn btn-primary btn-lg pull-right')) }}
    </div>
</div>
