@extends ('backend.layouts.app')

@section ('title', 'Crear Tip')

@section('after-styles')
    {{ Html::style("css/backend/custom.css") }}
    {{ Html::style("css/backend/plugin/editor/bootstrap3-wysihtml5.min.css") }}
@endsection

@section('content')
    <div class="main-content">
        <div class="main-content-inner">
            
            <div class="page-header">
                <h1>
                    Tips
                    <small>
                        <i class="ace-icon fa fa-angle-double-right"></i>
                        <small>{{ trans('buttons.general.crud.create') }} Tip</small>
                    </small>

                    <div class="box-tools pull-right">
                        <a href="{{ url('/admin/configuraciones/tips') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning btn-sm">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> {{ trans('buttons.general.crud.back') }}
                        </a>
                    </div>
                </h1>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    {!! Form::open(['url' => '/admin/configuraciones/tips', 'class' => 'form-horizontal', 'files' => true]) !!}
                        @include ('backend.configuraciones.tips.form')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('after-scripts')
    {{ Html::script("js/backend/plugin/editor/bootstrap3-wysihtml5.all.min.js") }}

    <script>
        $(function () {
            //bootstrap WYSIHTML5 - text editor
            $('.textarea').wysihtml5()
        })
    </script>
@endsection
