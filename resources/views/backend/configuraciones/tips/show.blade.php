@extends ('backend.layouts.app')

@section ('title', 'Ver Tip')

@section('content')
    <div class="main-content">
        <div class="main-content-inner">
            

            <div class="page-header">
                <h1>
                    Tips
                    <small>
                        <i class="ace-icon fa fa-angle-double-right"></i>
                        <small>{{ trans('buttons.general.crud.show') }} Tip</small>
                        <small><i class="ace-icon fa fa-angle-double-right"></i></small>
                        <small>{{ $tip->ruta }}</small>
                    </small>

                    <div class="box-tools pull-right">
                        <a href="{{ url('/admin/configuraciones/tips') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning btn-sm">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> {{ trans('buttons.general.crud.back') }}
                        </a>
                        <a href="{{ url('/admin/configuraciones/tips/' . $tip->id . '/edit') }}" title="{{ trans('buttons.general.crud.edit') }}" class="btn btn-primary btn-sm">
                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i> {{ trans('buttons.general.crud.edit') }}
                        </a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['configuraciones/tips', $tip->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> '.trans("buttons.general.crud.delete"), array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-sm',
                                    'title' => trans("buttons.general.crud.delete")." ".trans("labels.frontend.pais"),
                                    'onclick'=>'return confirm("¿Desea eliminar este tip?")'
                            ))!!}
                        {!! Form::close() !!}
                    </div>
                </h1>
            </div>

            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Gestion de Tips del Sistema</h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="table-responsive">
                                <table class="table table-borderless">
                                    <tbody>
                                        <tr>
                                            <th>Ruta</th>
                                            <td>{{ $tip->ruta }}</td>
                                        </tr>
                                        <tr>
                                            <th> Texto </th>
                                            <td> {!! $tip->texto !!}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
