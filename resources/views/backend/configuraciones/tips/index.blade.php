@extends ('backend.layouts.app')

@section ('title', 'Tips del sistema')

@section('after-styles')
    {{ Html::style("css/backend/plugin/datatables/dataTables.bootstrap.min.css") }}
@endsection

@section('content')
    <div class="main-content">
        <div class="main-content-inner">
            
            <div class="page-header">
                <h1>
                    Tips
                    <small>
                        <i class="ace-icon fa fa-angle-double-right"></i>
                        <small>{{ trans('navs.frontend.user.administration') }}</small>
                    </small>

                    <div class="box-tools pull-right">
                        <a href="{{ url('/admin/configuraciones/tips/create') }}" class="btn btn-success btn-sm" title="{{ trans('buttons.general.crud.create') }} Pai">
                            <i class="fa fa-plus" aria-hidden="true"></i> {{ trans('buttons.general.crud.create') }} Tip
                        </a>
                    </div><!--box-tools pull-right-->
                </h1>
            </div>

            {!! Form::open(['method' => 'GET', 'url' => '/admin/configuraciones/tips', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
            <div class="input-group">
                <input type="text" class="form-control" name="search" placeholder="{{ trans('strings.backend.general.search_placeholder') }}">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="submit">
                        <i class="fa fa-search"></i>
                    </button>
                </span>
            </div>
            {!! Form::close() !!}

            <div class="row">
                <div class="col-xs-12">
                    <div class="table-responsive box box-success">
                        <table id="tips-table" class="table table-condensed table-hover">
                            <thead>
                                <tr>
                                    <th>Ruta</th>
                                    <th>Texto</th>
                                    <th>{{ trans('labels.general.actions') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($tips as $item)
                                <tr>
                                    <td>{{ $item->ruta }}</td>
                                    <td>{!! $item->texto !!}</td>
                                    <td>
                                        <a href="{{ url('/admin/configuraciones/tips/' . $item->id) }}" title="{{ trans('buttons.general.crud.show') }} Tip">
                                            <button class="btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i> {{ trans('buttons.general.crud.show') }}</button>
                                        </a>
                                        <a href="{{ url('/admin/configuraciones/tips/' . $item->id . '/edit') }}" title="{{ trans('buttons.general.crud.edit') }} Tip">
                                            <button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> {{ trans('buttons.general.crud.edit') }}</button>
                                        </a>
                                        {!! Form::open([
                                            'method'=>'DELETE',
                                            'url' => ['/admin/configuraciones/tips', $item->id],
                                            'style' => 'display:inline'
                                        ]) !!}
                                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> '.trans("buttons.general.crud.delete"), array(
                                                    'type' => 'submit',
                                                    'class' => 'btn btn-danger btn-xs',
                                                    'title' => trans("buttons.general.crud.delete")." ".trans("labels.frontend.pais"),
                                                    'onclick'=>'return confirm("¿Desea eliminar este tip?")'
                                            )) !!}
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="pagination-wrapper"> {!! $tips->appends(['search' => Request::get('search')])->render() !!} </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('after-scripts')
{{ Html::script("js/backend/plugin/datatables/jquery.dataTables.min.js") }}
{{ Html::script("js/backend/plugin/datatables/dataTables.bootstrap.min.js") }}

<script>
    $(document).ready(function() {
        $('#tips-table').DataTable( {
            "paging":   false,
            "info":     false,
            "searching":   false
        });
    } );
</script>

@endsection