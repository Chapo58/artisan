@extends ('backend.layouts.app')

@section ('title', trans('labels.backend.access.users.management') . ' | ' . trans('labels.backend.access.users.edit'))

@section('page-header')
    <h1>
        {{ trans('labels.backend.access.users.management') }}
        <small>{{ trans('labels.backend.access.users.edit') }}</small>
    </h1>
@endsection

@section('content')

<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">{{ trans('labels.backend.access.users.edit') }}</h3>

        @role('Administrador')
            <div class="box-tools pull-right">
                @include('backend.access.includes.partials.user-header-buttons')
            </div><!--box-tools pull-right-->
        @endauth
    </div><!-- /.box-header -->

    <div class="box-body">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#usuario" data-toggle="tab">Datos</a></li>
                <li><a href="#pass" data-toggle="tab">Contraseña</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="usuario">
                    {{ Form::model($user, ['route' => ['admin.access.user.update', $user], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH']) }}
                    @role('Administrador')
                        <div class="form-group">
                            {{ Form::label('empresa_id', 'Empresa', ['class' => 'col-lg-2 control-label']) }}

                            <div class="col-lg-10">
                                {{ Form::select('empresa_id', $empresas, isset($user->empresa_id) ? $user->empresa_id : 0, ['class' => 'form-control']) }}
                            </div><!--col-lg-10-->
                        </div><!--form control-->
                    @endauth

                    <div class="form-group">
                        {{ Form::label('name', trans('validation.attributes.backend.access.users.name'), ['class' => 'col-lg-2 control-label']) }}

                        <div class="col-lg-10">
                            {{ Form::text('name', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.access.users.name')]) }}
                        </div><!--col-lg-10-->
                    </div><!--form control-->

                    <div class="form-group">
                        {{ Form::label('email', trans('validation.attributes.backend.access.users.email'), ['class' => 'col-lg-2 control-label']) }}

                        <div class="col-lg-10">
                            {{ Form::text('email', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.access.users.email')]) }}
                        </div><!--col-lg-10-->
                    </div><!--form control-->

                    @if ($user->id != 1)
                        <div class="form-group">
                            {{ Form::label('status', trans('validation.attributes.backend.access.users.active'), ['class' => 'col-lg-2 control-label']) }}

                            <div class="col-lg-1">
                                {{ Form::checkbox('status', '1', $user->status == 1) }}
                            </div><!--col-lg-1-->
                        </div><!--form control-->
                        @role('Administrador')
                        <div class="form-group">
                            {{ Form::label('confirmed', trans('validation.attributes.backend.access.users.confirmed'), ['class' => 'col-lg-2 control-label']) }}

                            <div class="col-lg-1">
                                {{ Form::checkbox('confirmed', '1', $user->confirmed == 1) }}
                            </div><!--col-lg-1-->
                        </div><!--form control-->
                        @endauth
                        <div class="form-group" style="display:none;">
                            {{ Form::label('status', trans('validation.attributes.backend.access.users.associated_roles'), ['class' => 'col-lg-2 control-label']) }}

                            <div class="col-lg-3">
                                @if (count($roles) > 0)
                                    @foreach($roles as $role)
                                        <input type="checkbox" value="{{$role->id}}" name="assignees_roles[{{ $role->id }}]" {{ is_array(old('assignees_roles')) ? (in_array($role->id, old('assignees_roles')) ? 'checked' : '') : (in_array($role->id, $user_roles) ? 'checked' : '') }} id="role-{{$role->id}}" /> <label for="role-{{$role->id}}">{{ $role->name }}</label>
                                            <a href="#" data-role="role_{{$role->id}}" class="show-permissions small">
                                                (
                                                    <span class="show-text">{{ trans('labels.general.show') }}</span>
                                                    <span class="hide-text hidden">{{ trans('labels.general.hide') }}</span>
                                                    {{ trans('labels.backend.access.users.permissions') }}
                                                )
                                            </a>
                                        <br/>
                                        <div class="permission-list hidden" data-role="role_{{$role->id}}">
                                            @if ($role->all)
                                                {{ trans('labels.backend.access.users.all_permissions') }}<br/><br/>
                                            @else
                                                @if (count($role->permissions) > 0)
                                                    <blockquote class="small">{{--
                                                --}}@foreach ($role->permissions as $perm){{--
                                                --}}{{$perm->display_name}}<br/>
                                                        @endforeach
                                                    </blockquote>
                                                @else
                                                    {{ trans('labels.backend.access.users.no_permissions') }}<br/><br/>
                                                @endif
                                            @endif
                                        </div><!--permission list-->
                                    @endforeach
                                @else
                                    {{ trans('labels.backend.access.users.no_roles') }}
                                @endif
                            </div><!--col-lg-3-->
                        </div><!--form control-->
                    @endif
                    @if ($user->id == 1)
                        {{ Form::hidden('status', 1) }}
                        {{ Form::hidden('confirmed', 1) }}
                        {{ Form::hidden('assignees_roles[]', 1) }}
                    @endif
                    <hr>
                    <div class="form-group">
                        <div class="col-lg-12">
                            <div class="pull-left">
                                {{ link_to_route('admin.access.user.index', trans('buttons.general.cancel'), [], ['class' => 'btn btn-danger btn-lg']) }}
                            </div><!--pull-left-->

                            <div class="pull-right">
                                {{ Form::submit(trans('buttons.general.crud.update'), ['class' => 'btn btn-success btn-lg']) }}
                            </div><!--pull-right-->
                        </div>
                    </div>
                    {{ Form::close() }}
                </div>
                <div class="tab-pane" id="pass">
                    {{ Form::open(['route' => ['admin.access.user.change-password', $user], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'patch']) }}
                    <div class="form-group">
                        {{ Form::label('password', 'Nueva '.trans('validation.attributes.backend.access.users.password'), ['class' => 'col-lg-2 control-label', 'placeholder' => trans('validation.attributes.backend.access.users.password')]) }}

                        <div class="col-lg-10">
                            {{ Form::password('password', ['class' => 'form-control']) }}
                        </div><!--col-lg-10-->
                    </div><!--form control-->

                    <div class="form-group">
                        {{ Form::label('password_confirmation', trans('validation.attributes.backend.access.users.password_confirmation'), ['class' => 'col-lg-2 control-label', 'placeholder' => trans('validation.attributes.backend.access.users.password_confirmation')]) }}

                        <div class="col-lg-10">
                            {{ Form::password('password_confirmation', ['class' => 'form-control']) }}
                        </div><!--col-lg-10-->
                    </div><!--form control-->
                    <hr>
                    <div class="form-group">
                        <div class="col-lg-12">
                            <div class="pull-left">
                                {{ link_to_route('admin.access.user.index', trans('buttons.general.cancel'), [], ['class' => 'btn btn-danger btn-lg']) }}
                            </div><!--pull-left-->

                            <div class="pull-right">
                                {{ Form::submit(trans('buttons.general.crud.update'), ['class' => 'btn btn-success btn-lg']) }}
                            </div><!--pull-right-->
                        </div>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div><!-- /.box-body -->
</div><!--box-->

@endsection

@section('after-scripts')
    {{ Html::script('js/backend/access/users/script.js') }}
@endsection
