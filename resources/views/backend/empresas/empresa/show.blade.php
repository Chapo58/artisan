@extends ('backend.layouts.app')

@section ('title', trans('labels.frontend.empresa_titulo')." - ".trans('buttons.general.crud.show'))

@section('after-styles')
    {{ Html::style("css/frontend/sweetalert2.min.css") }}
@endsection

@section('content')
    <div class="main-content">
        <div class="main-content-inner">
            

            <div class="page-header">
                <h1>
                    {{ trans('labels.frontend.empresa_titulo') }}
                    <small>
                        <i class="ace-icon fa fa-angle-double-right"></i>
                        <small>{{ trans('buttons.general.crud.show') }} Empresa</small>
                        <small><i class="ace-icon fa fa-angle-double-right"></i></small>
                        <small>{{ $empresa->nombre }}</small>
                    </small>

                    <div class="box-tools pull-right">
                        <a href="{{ url('/admin/empresas/empresa') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning btn-sm">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> {{ trans('buttons.general.crud.back') }}
                        </a>
                        <a href="{{ url('/admin/empresas/empresa/' . $empresa->id . '/edit') }}" title="{{ trans('buttons.general.crud.edit') }}" class="btn btn-primary btn-sm">
                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i> {{ trans('buttons.general.crud.edit') }}
                        </a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['admin/empresas/empresa', $empresa->id],
                            'style' => 'display:inline',
                            'id' => 'form-delete'
                        ]) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> '.trans("buttons.general.crud.delete"), array(
                                    'type' => 'button',
                                    'class' => 'btn btn-danger btn-sm',
                                    'title' => trans("buttons.general.crud.delete")." ".trans("labels.frontend.empresa"),
                                    'onclick'=> 'eliminar()'
                            ))!!}
                        {!! Form::close() !!}
                    </div>
                </h1>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-success">
                        <div class="box-header with-border">
                            <h3 class="box-title">Datos Empresa</h3>
                        </div>
                        <table class="table table-borderless">
                            <tbody>
                                <tr>
                                    <th> Razon Social </th><td> {{ $empresa->razon_social }} </td>
                                    <th> Nombre </th><td> {{ $empresa->nombre }} </td>
                                    <td rowspan="4">
                                        <img src="{{ isset($empresa->imagen_url) ? $empresa->imagen_url : '/empresas/no-image.png' }}" alt="{{ isset($empresas->nombre) ? $empresas->nombre : '' }}" width="100" style="border: 2px solid lightslategrey;">
                                    </td>
                                </tr>
                                <tr>
                                    <th> Cuit </th><td> {{ $empresa->cuit }} </td>
                                    <th> Email </th><td> {{ $empresa->email }} </td>
                                </tr>
                                <tr>
                                    <th> Contacto </th><td> {{ $empresa->contacto }} </td>
                                    <th> Teléfono </th><td> {{ $empresa->telefono }} </td>
                                </tr>
                                <tr>
                                    <th> Ingresos Brutos </th><td> {{ $empresa->ingresos_brutos }} </td>
                                    <th> Condición IVA </th><td> {{ $empresa->condicionIva }} </td>
                                </tr>
                                @if(isset($empresa->inicio_actividad))
                                    <tr>
                                        <th> Inicio Actividad </th><td> {{ $empresa->inicio_actividad->format('d/m/Y') }} </td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>

                    @if(isset($empresa->domicilio))
                        <div class="box box-success">
                            <div class="box-header with-border">
                                <h3 class="box-title">Domicilio Fiscal</h3>
                            </div>
                            <table class="table table-borderless">
                                <tbody>
                                <tr>
                                    <th> Dirección </th><td> {{ $empresa->domicilio->direccion }} </td>
                                    <th> País </th><td> {{ $empresa->domicilio->pais }} </td>
                                </tr>
                                <tr>
                                    <th> Provincia </th><td> {{ $empresa->domicilio->provincia }} </td>
                                    <th> Localidad </th><td> {{ $empresa->domicilio->localidad }} </td>
                                </tr>
                                <tr>
                                    <th> Código Postal </th><td> {{ $empresa->domicilio->codigo_postal }} </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    @endif

                    <div class="box box-success">
                        <div class="box-header with-border">
                            <h3 class="box-title">Usuarios</h3>
                        </div>
                        <table class="table table-bordered">
                            <thead class="">
                            <tr>
                                <th><i class="ace-icon fa fa-caret-right blue"></i>Nombre</th>
                                <th><i class="ace-icon fa fa-caret-right blue"></i>Email</th>
                                <th><i class="ace-icon fa fa-caret-right blue"></i>Rol</th>
                                <th><i class="ace-icon fa fa-caret-right blue"></i>Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(!$usuarios->isEmpty())
                                @foreach($usuarios as $usuario)
                                <tr>
                                    <td>{{ isset($usuario->empleado) ? $usuario->empleado->nombre : 'Sin empleado' }}</td>
                                    <td>{{ $usuario->email }}</td>
                                    <td>{{ $usuario->rol_string }}</td>
                                    <td>
                                        <a href="{{url('/admin/access/user/'.$usuario->id.'/login-as')}}" class="btn btn-xs btn-success">
                                            <i class="fa fa-lock" data-toggle="tooltip" data-placement="top" title="Iniciar sesión como {{$usuario}}"></i>
                                        </a>
                                        <a href="{{url('/admin/access/user/'.$usuario->id.'/edit')}}" class="btn btn-xs btn-primary">
                                            <i class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="" data-original-title="Modificar"></i>
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                    <div class="box box-success">
                        <div class="box-header with-border">
                            <h3 class="box-title">Datos Sucursales</h3>
                        </div>
                        <table class="table table-bordered">
                            <thead class="">
                            <tr>
                                <th><i class="ace-icon fa fa-caret-right blue"></i>Nº</th>
                                <th><i class="ace-icon fa fa-caret-right blue"></i>Nombre</th>
                                <th><i class="ace-icon fa fa-caret-right blue"></i>Email</th>
                                <th><i class="ace-icon fa fa-caret-right blue"></i>Teléfono</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(!$empresa->sucursales->isEmpty())
                            @foreach($empresa->sucursales as $sucursal)
                            <tr>
                                <td>{{ $loop->index + 1 }}</td>
                                <td>{{ $sucursal->nombre }}</td>
                                <td>{{ $sucursal->email }}</td>
                                <td>{{ $sucursal->telefono }}</td>
                            </tr>
                            @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('after-scripts')
    {{ Html::script("js/frontend/sweetalert2.min.js") }}

    <script type="text/javascript">
        function eliminar(){
            swal({
                title: "Eliminar {{ trans("labels.frontend.plan") }}",
                text: "¿Realmente desea eliminar esta Empresa? Esta accion es irreversible.",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#ff8726',
                confirmButtonText: "Eliminar"
            }).then((result) => {
                if (result.value) {
                    $('form#form-delete').submit();
                }
            });
        }
    </script>
@endsection