@extends ('backend.layouts.app')

@section ('title', trans('labels.frontend.empresa_titulo')." - ".trans('buttons.general.crud.create'))

@section('content')
    <div class="main-content">
        <div class="main-content-inner">
            
            <div class="page-header">
                <h1>
                    {{ trans('labels.frontend.empresa_titulo') }}
                    <small>
                        <i class="ace-icon fa fa-angle-double-right"></i>
                        <small>{{ trans('buttons.general.crud.create') }} Empresa</small>
                    </small>

                    <div class="box-tools pull-right">
                        <a href="{{ url('/admin/empresas/empresa') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning btn-sm">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> {{ trans('buttons.general.crud.back') }}
                        </a>
                    </div>
                </h1>
            </div>

            <div class="row">
                        <div class="col-xs-12">

                    @include('includes.partials.messages')

                    {!! Form::open(['url' => '/admin/empresas/empresa', 'class' => 'form-horizontal', 'files' => true]) !!}
                        @include ('backend.empresas.empresa.form')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('after-scripts')
{{ Html::script("js/frontend/plugins/jquery/jquery-ui.min.js") }}
{{ Html::script("js/frontend/plugins/maskedinput/jquery.maskedinput.min.js") }}

<script>
    $( document ).ready(function() {
        $('input.fecha').mask("99/99/9999", {placeholder: "dd/mm/aaaa"});
        $('input.cuit').mask("99-99999999-9",{placeholder:"##-########-#"});
        $('input.numero').mask("9999999999",{placeholder:"##########"});

        $(document).on('click','.borrar-detalle', function() {
            inputNombre = this.attributes['name'].value;
            idDetalle = parseInt($('.borrar-detalle[name="' + inputNombre + '"]').parent().parent().find('td input.id').val());

            $('.borrar-detalle[name="' + inputNombre + '"]').parent().parent().hide("slow");
            $('.borrar-detalle[name="' + inputNombre + '"]').parent().parent().hide("slow");
            $('.borrar-detalle[name="' + inputNombre + '"]').parent().find('input.estado').val(0);
            $('.borrar-detalle[name="' + inputNombre + '"]').parent().parent().find('td input.subtotal-neto').val(0);
            $('.borrar-detalle[name="' + inputNombre + '"]').parent().parent().find('td input.subtotal-bruto').val(0);

            $('.borrar-detalle[name="' + inputNombre + '"]').parent().parent().find('input').removeAttr('required')
        });
    });

    $("#agregar-detalle").click(function(){
        agregarDetalle();
    });

    $(".agregar-detalle").on('focusin', function(){
        agregarDetalle();
    });

    function agregarDetalle() {
        var numero_orden = $('table#tabla-sucursales tbody tr').length+ 1;
        $("#tabla-sucursales").append('<tr>' +
            '<td>' +
                '<input type="text" name="detalles['+(numero_orden*-1)+'][nombre]" value="" class="form-control" required>' +
                '<input type="hidden" class="id" name="detalles['+(numero_orden*-1)+'][id]" value="'+(numero_orden*-1)+'">' +
            '</td>' +
            '<td>' +
                '<a class="btn btn-danger btn-sm borrar-detalle" name="detalles['+(numero_orden*-1)+'][eliminar]" tabindex="99999">' +
                    '<i class="fa fa-trash-o" aria-hidden="true"></i>' +
                '</a>' +
                '<input type="hidden" class="estado" name="detalles['+(numero_orden*-1)+'][estado]" value="1">' +
            '</td>' +
            '</tr>');
        $("input[name='detalles["+(numero_orden*-1)+"][nombre]']").focus();
    }
</script>

    @yield('scripts-dialog-provincia')
    @yield('scripts-dialog-localidad')

@endsection
