@extends ('backend.layouts.app')

@section ('title', 'Permisos Empresa')

@section('content')
    <div class="main-content">
        <div class="main-content-inner">
            
            <div class="page-header">
                <h1>
                    {{ trans('labels.frontend.plan_titulo') }}
                    <small>
                        <i class="ace-icon fa fa-angle-double-right"></i>
                        <small>Empresas </small>
                        <small><i class="ace-icon fa fa-angle-double-right"></i></small>
                        <small>Permisos de Empresa</small>
                    </small>
                </h1>
            </div>

            <div class="row">
                <div class="col-xs-12">
                        {!! Form::model($empresa, [
                            'method' => 'POST',
                            'url' => ['/admin/empresas/empresa/permisos-empresa/guardar'],
                            'class' => 'form-horizontal',
                            'files' => true
                        ]) !!}

                    @foreach($modulos as $modulo)
                        <div class="box box-success">
                            <div class="box-header with-border">
                                <h3 class="box-title">Modulo {{$modulo}}</h3>
                            </div>
                            <div class="box-body">
                                <div class="row">
                                    @foreach($modulo->permisos as $item)
                                        <div class="col-xs-4">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="permisos[{{$item->nombre}}]" value="{{$item->id}}" {{ ($empresa->permisos->contains($item->id)) ? 'checked' : '' }}> {{$item}}
                                                </label>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    @endforeach

                    @if($empresa)
                        {{ Form::hidden('empresa_id', $empresa->id) }}
                    @endif

                    <div class="form-group">
                        <div class="col-md-offset-10 col-md-2">
                            {{ Form::submit(trans('buttons.general.save'), array('class' => 'btn btn-primary btn-lg pull-right')) }}
                        </div>
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@endsection

@section('after-scripts')


@endsection