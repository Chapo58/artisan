<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Datos Empresa</h3>
    </div>
    <div class="box-body">
        <div class="form-group">
            {!! Form::label('imagen_url', 'Logo', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-2">
                <img src="{{ isset($empresa->imagen_url) ? $empresa->imagen_url : '/empresas/no-image.png' }}" alt="{{ isset($empresas->nombre) ? $empresas->nombre : '' }}" width="100" style="border: 2px solid lightslategrey;">
            </div>
            <div class="col-md-8">
                {!! Form::file('image', array('class' => 'image')) !!}
            </div>
        </div>
        <div class="form-group">
            <div class="{{ $errors->has('razon_social') ? 'has-error' : ''}}">
                {!! Form::label('razon_social', 'Razon Social', ['class' => 'col-md-2 control-label']) !!}
                <div class="col-md-4">
                    {!! Form::text('razon_social', null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('razon_social', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="{{ $errors->has('cuit') ? 'has-error' : ''}}">
                {!! Form::label('cuit', 'Cuit', ['class' => 'col-md-2 control-label']) !!}
                <div class="col-md-4">
                    {!! Form::text('cuit', null, ['class' => 'form-control cuit', 'required' => 'required']) !!}
                    {!! $errors->first('cuit', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="{{ $errors->has('nombre') ? 'has-error' : ''}}">
                {!! Form::label('nombre', 'Nombre', ['class' => 'col-md-2 control-label']) !!}
                <div class="col-md-4">
                    {!! Form::text('nombre', null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('nombre', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="{{ $errors->has('email') ? 'has-error' : ''}}">
                {!! Form::label('email', 'Email', ['class' => 'col-md-2 control-label']) !!}
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                        {!! Form::text('email', null, ['class' => 'form-control', 'required' => 'required']) !!}
                    </div>
                    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="{{ $errors->has('telefono') ? 'has-error' : ''}}">
                {!! Form::label('telefono', 'Telefono', ['class' => 'col-md-2 control-label']) !!}
                <div class="col-md-4">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-phone"></i>
                        </div>
                        {!! Form::text('telefono', null, ['class' => 'form-control numero']) !!}
                    </div>
                    {!! $errors->first('telefono', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="{{ $errors->has('web') ? 'has-error' : ''}}">
                {!! Form::label('web', 'Sitio Web', ['class' => 'col-md-2 control-label']) !!}
                <div class="col-md-4">
                    {!! Form::text('web', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('web', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="{{ $errors->has('condicion_iva_id') ? 'has-error' : ''}}">
                {!! Form::label('condicion_iva_id', 'Condicion Iva', ['class' => 'col-md-2 control-label']) !!}
                <div class="col-md-4">
                    {!! Form::select('condicion_iva_id', $condiciones_iva, isset($empresa) ? $empresa->condicion_iva_id : null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('condicion_iva_id', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="{{ $errors->has('ingresos_brutos') ? 'has-error' : ''}}">
                {!! Form::label('ingresos_brutos', 'Ingresos Brutos', ['class' => 'col-md-2 control-label']) !!}
                <div class="col-md-4">
                    {!! Form::text('ingresos_brutos', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('ingresos_brutos', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="{{ $errors->has('inicio_actividad') ? 'has-error' : ''}}">
                {!! Form::label('inicio_actividad', 'Fecha Inicio Actividad', ['class' => 'col-md-2 control-label']) !!}
                <div class="col-md-4">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        {!! Form::text('inicio_actividad', isset($empresa->inicio_actividad) ? $empresa->inicio_actividad->format('d/m/Y') : '', ['class' => 'form-control fecha']) !!}
                    </div>
                    {!! $errors->first('inicio_actividad', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            @role('Administrador')
            <div class="{{ $errors->has('fecha_vencimiento') ? 'has-error' : ''}}">
                {!! Form::label('inicio_actividad', 'Fecha Vencimiento', ['class' => 'col-md-2 control-label']) !!}
                <div class="col-md-4">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        {!! Form::date('fecha_vencimiento', isset($empresa->fecha_vencimiento) ? $empresa->fecha_vencimiento : '', ['class' => 'form-control']) !!}
                    </div>
                    {!! $errors->first('fecha_vencimiento', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            @endauth
        </div>
        <div class="form-group">
            <div class="{{ $errors->has('plan_id') ? 'has-error' : ''}}">
                {!! Form::label('plan_id', 'Plan', ['class' => 'col-md-2 control-label']) !!}
                <div class="col-md-4">
                    {!! Form::select('plan_id', $planes, isset($empresa) ? $empresa->plan_id : null, ['class' => 'form-control']) !!}
                    {!! $errors->first('plan_id', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            @role('Administrador')
                <div class="{{ $errors->has('distribuidor_id') ? 'has-error' : ''}}">
                    {!! Form::label('distribuidor_id', 'Distribuidor', ['class' => 'col-md-2 control-label']) !!}
                    <div class="col-md-4">
                        {!! Form::select('distribuidor_id', $distribuidores, isset($empresa) ? $empresa->distribuidor_id : null, ['class' => 'form-control']) !!}
                        {!! $errors->first('distribuidor_id', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
            @endauth
        </div>
    </div>
</div>

<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Domicilio Fiscal</h3>
    </div>
    <div class="box-body">
        <div class="form-group">
            <div class="{{ $errors->has('direccion') ? 'has-error' : ''}}">
                {!! Form::label('direccion', 'Dirección', ['class' => 'col-md-2 control-label']) !!}
                <div class="col-md-4">
                    {!! Form::text('domicilio[direccion]', isset($empresa->domicilio) ? $empresa->domicilio->direccion : '', ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('direccion', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="{{ $errors->has('pais_id') ? 'has-error' : ''}}">
                {!! Form::label('pais_id', 'País', ['class' => 'col-md-2 control-label']) !!}
                <div class="col-md-3">
                    {!! Form::select('domicilio[pais_id]', $paises, isset($empresa->domicilio->pais) ? $empresa->domicilio->pais->id : 1, ['id' => 'pais_id', 'class' => 'form-control']) !!}
                    {!! $errors->first('pais_id', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="{{ $errors->has('provincia_id') ? 'has-error' : ''}}">
                {!! Form::label('provincia_id', 'Provincia', ['class' => 'col-md-2 control-label']) !!}
                <div class="col-md-3">
                    {!! Form::select('domicilio[provincia_id]', $provincias, isset($empresa->domicilio->provincia) ? $empresa->domicilio->provincia->id : '', ['id' => 'provincia_id', 'class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('provincia_id', '<p class="help-block">:message</p>') !!}
                </div>
                <div class="col-md-1">
                    <a class="btn btn-success btn-sm" id="agregar-provincia">
                        <i class="fa fa-plus" aria-hidden="true"></i> Nueva
                    </a>
                </div>
            </div>
            <div class="{{ $errors->has('codigo_postal') ? 'has-error' : ''}}">
                {!! Form::label('codigo_postal', 'Código Postal', ['class' => 'col-md-2 control-label']) !!}
                <div class="col-md-3">
                    {!! Form::text('domicilio[codigo_postal]', isset($empresa->domicilio) ? $empresa->domicilio->codigo_postal : '', ['class' => 'form-control']) !!}
                    {!! $errors->first('codigo_postal', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="{{ $errors->has('localidad') ? 'has-error' : ''}}">
                {!! Form::label('localidad', 'Localidad', ['class' => 'col-md-2 control-label']) !!}
                <div class="col-md-4">
                    {!! Form::text('domicilio[localidad]', isset($empresa->domicilio) ? $empresa->domicilio->localidad : '', ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('localidad', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

        </div>
    </div>
</div>

@if (!isset($empresa))
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Datos Login</h3>
        </div>
        <div class="box-body">
            <div class="form-group {{ $errors->has('admin_email') ? 'has-error' : ''}}">
                {!! Form::label('admin_email', 'Email', ['class' => 'col-md-2 control-label']) !!}
                <div class="col-md-4">
                    {!! Form::text('admin_email', null, ['class' => 'form-control', 'required']) !!}
                    {!! $errors->first('admin_email', '<p class="help-block">:message</p>') !!}
                </div>
                {!! Form::label('admin_password', 'Contraseña', ['class' => 'col-md-2 control-label']) !!}
                <div class="col-md-4">
                    {!! Form::password('admin_password', null, ['class' => 'form-control', 'required']) !!}
                    {!! $errors->first('admin_password', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
        </div>
    </div>
@endif

<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Sucursales</h3>
    </div>
    <div class="box-body">
        <table id="tabla-sucursales" class="table table-bordered">
            <thead>
            <tr>
                <th>Nombre</th>
                <th>Acciones</th>
            </tr>
            </thead>
            <tbody>
            @if (isset($empresa))
                @foreach($empresa->sucursales as $sucursal)
                    <tr>
                        <td>{!! $sucursal->nombre !!}</td>
                        <td></td>
                    </tr>
                @endforeach
            @endif
            </tbody>
            <tfoot>
            <tr>
                <td>{!! Form::text('', '', ['class' => 'form-control agregar-detalle']) !!}</td>
                <td></td>
            </tr>
            <tr>
                <th colspan="2">
                    <a class="btn btn-success btn-sm" id="agregar-detalle">
                        <i class="fa fa-plus" aria-hidden="true"></i> Agregar Sucursal
                    </a>
                </th>
                <th></th>
            </tr>
            </tfoot>
        </table>
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-10 col-md-2">
        {{ Form::submit(isset($submitButtonText) ? $submitButtonText : trans('buttons.general.save'), array('class' => 'btn btn-primary btn-lg pull-right')) }}
    </div>
</div>

@include('backend.configuraciones.provincia.provincia_dialog')
@include('backend.configuraciones.localidad.localidad_dialog')
