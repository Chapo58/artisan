<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Datos Distribuidor</h3>
    </div>
    <div class="box-body">
        <div class="form-group">
            <div class="col-md-1"></div>
            <div class="col-md-1">
                {!! Form::label('logo_url', 'Logo') !!}
                <img src="{{ isset($distribuidor->logo_url) ? $distribuidor->logo_url : '/empresas/no-image.png' }}" alt="{{ isset($distribuidor->razon_social) ? $distribuidor->razon_social : '' }}" width="100" style="border: 2px solid lightslategrey;">
            </div>
            <div class="col-md-4">
                {!! Form::file('logo', array('class' => 'image')) !!}
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-1">
                {!! Form::label('favicon_url', 'Favicon (50x50)') !!}
                <img src="{{ isset($distribuidor->favicon_url) ? $distribuidor->favicon_url : '/empresas/no-image.png' }}" alt="{{ isset($distribuidor->razon_social) ? $distribuidor->razon_social : '' }}" width="100" style="border: 2px solid lightslategrey;">
            </div>
            <div class="col-md-4">
                {!! Form::file('favicon', array('class' => 'image')) !!}
            </div>
        </div>
        <div class="form-group">
            <div class="{{ $errors->has('razon_social') ? 'has-error' : ''}}">
                {!! Form::label('razon_social', 'Razon Social', ['class' => 'col-md-2 control-label']) !!}
                <div class="col-md-4">
                    {!! Form::text('razon_social', null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('razon_social', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="{{ $errors->has('cuit') ? 'has-error' : ''}}">
                {!! Form::label('cuit', 'Cuit', ['class' => 'col-md-2 control-label']) !!}
                <div class="col-md-4">
                    {!! Form::text('cuit', null, ['class' => 'form-control cuit']) !!}
                    {!! $errors->first('cuit', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="{{ $errors->has('email') ? 'has-error' : ''}}">
                {!! Form::label('email', 'Email', ['class' => 'col-md-2 control-label']) !!}
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                        {!! Form::text('email', null, ['class' => 'form-control']) !!}
                    </div>
                    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="{{ $errors->has('telefono') ? 'has-error' : ''}}">
                {!! Form::label('telefono', 'Telefono', ['class' => 'col-md-2 control-label']) !!}
                <div class="col-md-4">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-phone"></i>
                        </div>
                        {!! Form::text('telefono', null, ['class' => 'form-control numero']) !!}
                    </div>
                    {!! $errors->first('telefono', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="{{ $errors->has('web') ? 'has-error' : ''}}">
                {!! Form::label('web', 'Sitio Web', ['class' => 'col-md-2 control-label']) !!}
                <div class="col-md-4">
                    {!! Form::text('web', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('web', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="{{ $errors->has('condicion_iva_id') ? 'has-error' : ''}}">
                {!! Form::label('condicion_iva_id', 'Condicion Iva', ['class' => 'col-md-2 control-label']) !!}
                <div class="col-md-4">
                    {!! Form::select('condicion_iva_id', $condiciones_iva, isset($distribuidor) ? $distribuidor->condicion_iva_id : null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('condicion_iva_id', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
        </div>
    </div>
</div>

<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Domicilio Fiscal</h3>
    </div>
    <div class="box-body">
        <div class="form-group">
            <div class="{{ $errors->has('direccion') ? 'has-error' : ''}}">
                {!! Form::label('direccion', 'Dirección', ['class' => 'col-md-2 control-label']) !!}
                <div class="col-md-4">
                    {!! Form::text('domicilio[direccion]', isset($distribuidor->domicilio) ? $distribuidor->domicilio->direccion : '', ['class' => 'form-control']) !!}
                    {!! $errors->first('direccion', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="{{ $errors->has('codigo_postal') ? 'has-error' : ''}}">
                {!! Form::label('codigo_postal', 'Código Postal', ['class' => 'col-md-2 control-label']) !!}
                <div class="col-md-3">
                    {!! Form::text('domicilio[codigo_postal]', isset($distribuidor->domicilio) ? $distribuidor->domicilio->codigo_postal : '', ['class' => 'form-control']) !!}
                    {!! $errors->first('codigo_postal', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="{{ $errors->has('provincia_id') ? 'has-error' : ''}}">
                {!! Form::label('provincia_id', 'Provincia', ['class' => 'col-md-2 control-label']) !!}
                <div class="col-md-3">
                    {!! Form::select('domicilio[provincia_id]', $provincias, isset($distribuidor->domicilio->provincia) ? $distribuidor->domicilio->provincia->id : '', ['id' => 'provincia_id', 'class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('provincia_id', '<p class="help-block">:message</p>') !!}
                </div>
                <div class="col-md-1">
                    <a class="btn btn-success btn-sm" id="agregar-provincia">
                        <i class="fa fa-plus" aria-hidden="true"></i> Nueva
                    </a>
                </div>
            </div>
            <div class="{{ $errors->has('localidad_id') ? 'has-error' : ''}}">
                {!! Form::label('localidad_id', 'Localidad', ['class' => 'col-md-2 control-label']) !!}
                <div class="col-md-4">
                    {!! Form::text('domicilio[localidad]', isset($distribuidor->domicilio) ? $distribuidor->domicilio->localidad : '', ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('localidad_id', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
        </div>
    </div>
</div>

@if (!isset($distribuidor))
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Datos Login</h3>
        </div>
        <div class="box-body">
            <div class="form-group {{ $errors->has('admin_email') ? 'has-error' : ''}}">
                {!! Form::label('admin_email', 'Email', ['class' => 'col-md-2 control-label']) !!}
                <div class="col-md-4">
                    {!! Form::text('admin_email', null, ['class' => 'form-control', 'required']) !!}
                    {!! $errors->first('admin_email', '<p class="help-block">:message</p>') !!}
                </div>
                {!! Form::label('admin_password', 'Contraseña', ['class' => 'col-md-2 control-label']) !!}
                <div class="col-md-4">
                    {!! Form::password('admin_password', null, ['class' => 'form-control', 'required']) !!}
                    {!! $errors->first('admin_password', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
        </div>
    </div>
@endif

<div class="form-group">
    <div class="col-md-offset-10 col-md-2">
        {{ Form::submit(isset($submitButtonText) ? $submitButtonText : trans('buttons.general.save'), array('class' => 'btn btn-primary btn-lg pull-right')) }}
    </div>
</div>
