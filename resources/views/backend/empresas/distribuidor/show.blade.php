@extends ('backend.layouts.app')

@section ('title', trans('labels.frontend.distribuidor_titulo')." - ".trans('buttons.general.crud.show'))

@section('after-styles')
    {{ Html::style("css/frontend/sweetalert2.min.css") }}
@endsection

@section('content')
    <div class="main-content">
        <div class="main-content-inner">
            

            <div class="page-header">
                <h1>
                    {{ trans('labels.frontend.distribuidor_titulo') }}
                    <small>
                        <i class="ace-icon fa fa-angle-double-right"></i>
                        <small>{{ trans('buttons.general.crud.show') }} Distribuidor</small>
                        <small><i class="ace-icon fa fa-angle-double-right"></i></small>
                        <small>{{ $distribuidor }}</small>
                    </small>

                    <div class="box-tools pull-right">
                        <a href="{{ url('/admin/empresas/distribuidor') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning btn-sm">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> {{ trans('buttons.general.crud.back') }}
                        </a>
                        @role('Administrador')
                        <a href="{{ url('/admin/empresas/distribuidor/' . $distribuidor->id . '/edit') }}" title="{{ trans('buttons.general.crud.edit') }}" class="btn btn-primary btn-sm">
                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i> {{ trans('buttons.general.crud.edit') }}
                        </a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['admin/empresas/distribuidor/', $distribuidor->id],
                            'style' => 'display:inline',
                            'id' => 'form-delete'
                        ]) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> '.trans("buttons.general.crud.delete"), array(
                                    'type' => 'button',
                                    'class' => 'btn btn-danger btn-sm',
                                    'title' => trans("buttons.general.crud.delete")." ".trans("labels.frontend.distribuidor"),
                                    'onclick'=> 'eliminar()'
                            ))!!}
                        {!! Form::close() !!}
                        @endauth
                    </div>
                </h1>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-success">
                        <div class="box-header with-border">
                            <h3 class="box-title">Datos Distribuidor</h3>
                        </div>
                        <table class="table table-borderless">
                            <tbody>
                            <tr>
                                <td rowspan="5">
                                    <img src="{{ isset($distribuidor->favicon_url) ? $distribuidor->favicon_url : '/empresas/no-image.png' }}" alt="{{ $distribuidor }}" width="100" style="border: 2px solid lightslategrey;">
                                </td>
                                <td rowspan="5">
                                    <img src="{{ isset($distribuidor->logo_url) ? $distribuidor->logo_url : '/empresas/no-image.png' }}" alt="{{ $distribuidor }}" width="100" style="border: 2px solid lightslategrey;">
                                </td>
                            </tr>
                            <tr>
                                <th> Razon Social </th><td> {{ $distribuidor->razon_social }} </td>
                                <th> Condicion Iva </th><td> {{ $distribuidor->condicionIva }} </td>
                            </tr>
                            <tr>
                                <th> Cuit </th><td> {{ $distribuidor->cuit }} </td>
                                <th> Email </th><td> {{ $distribuidor->email }} </td>
                            </tr>
                            <tr>
                                <th> Sitio Web </th><td> {{ $distribuidor->web }} </td>
                                <th> Teléfono </th><td> {{ $distribuidor->telefono }} </td>
                            </tr>
                            <tr>
                                <th> Usuario </th><td> {{ $distribuidor->usuario }} </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                    @if(isset($distribuidor->domicilio))
                        <div class="box box-success">
                            <div class="box-header with-border">
                                <h3 class="box-title">Domicilio Fiscal</h3>
                            </div>
                            <table class="table table-borderless">
                                <tbody>
                                <tr>
                                    <th> Dirección </th><td> {{ $distribuidor->domicilio->direccion }} </td>
                                    <th> País </th><td> {{ $distribuidor->domicilio->pais }} </td>
                                </tr>
                                <tr>
                                    <th> Provincia </th><td> {{ $distribuidor->domicilio->provincia }} </td>
                                    <th> Localidad </th><td> {{ $distribuidor->domicilio->localidad }} </td>
                                </tr>
                                <tr>
                                    <th> Código Postal </th><td> {{ $distribuidor->domicilio->codigo_postal }} </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    @endif

                </div>
            </div>
        </div>
    </div>
@endsection

@section('after-scripts')
    {{ Html::script("js/frontend/sweetalert2.min.js") }}

    <script type="text/javascript">
        function eliminar(){
            swal({
                title: "Eliminar {{ trans("labels.frontend.distribuidor") }}",
                text: "¿Realmente desea eliminar este Distribuidor? Esta accion es irreversible.",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#ff8726',
                confirmButtonText: "Eliminar"
            }).then((result) => {
                if (result.value) {
                    $('form#form-delete').submit();
                }
            });
        }
    </script>
@endsection