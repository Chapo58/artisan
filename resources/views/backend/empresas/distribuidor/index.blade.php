@extends ('backend.layouts.app')

@section ('title', trans('labels.frontend.distribuidor_titulo')." - ".trans('navs.frontend.user.administration'))

@section('after-styles')
    {{ Html::style("css/backend/plugin/datatables/dataTables.bootstrap.min.css") }}
    {{ Html::style("css/frontend/sweetalert2.min.css") }}
@endsection

@section('content')
    <div class="main-content">
        <div class="main-content-inner">
            
            <div class="page-header">
                <h1>
                    {{ trans('labels.frontend.distribuidor_titulo') }}
                    <small>
                        <i class="ace-icon fa fa-angle-double-right"></i>
                        <small>{{ trans('navs.frontend.user.administration') }}</small>
                    </small>

                    <div class="box-tools pull-right">
                        <a href="{{ url('/admin/empresas/distribuidor/create') }}" class="btn btn-success btn-sm" title="{{ trans('buttons.general.crud.create') }} {{ trans('labels.frontend.distribuidor') }}">
                            <i class="fa fa-plus" aria-hidden="true"></i> {{ trans('buttons.general.crud.create') }} {{ trans('labels.frontend.distribuidor') }}
                        </a>
                    </div><!--box-tools pull-right-->
                </h1>
            </div>

            {!! Form::open(['method' => 'GET', 'url' => '/admin/empresas/distribuidor', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
            <div class="input-group">
                <input type="text" class="form-control" name="search" placeholder="{{ trans('strings.backend.general.search_placeholder') }}">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="submit">
                        <i class="fa fa-search"></i>
                    </button>
                </span>
            </div>
            {!! Form::close() !!}

            <div class="row">
                <div class="col-xs-12">
                    <div class="table-responsive box box-success">
                        <table id="distribuidor-table" class="table table-condensed table-hover">
                            <thead>
                                <tr>
                                    <th>Razon Social</th>
                                    <th>Email</th>
                                    <th>Telefono</th>
                                    <th>Usuario</th>
                                    <th>Ciudad</th>
                                    <th>Cant. Clientes</th>
                                    <th>{{ trans('labels.general.actions') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($distribuidores as $item)
                                <tr>
                                    <td>{{ $item->razon_social }}</td>
                                    <td>{{ $item->email }}</td>
                                    <td>{{ $item->telefono }}</td>
                                    <td>{{ $item->usuario->email }}</td>
                                    <td>{{ ($item->domicilio) ? $item->domicilio->localidad : '' }}</td>
                                    <td>{{ $item->empresas->count() }}</td>
                                    <td>
                                        <a href="{{ url('/admin/empresas/distribuidor/' . $item->id) }}" title="{{ trans('buttons.general.crud.show') }} {{ trans('labels.frontend.distribuidor') }}">
                                            <button class="btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i> {{ trans('buttons.general.crud.show') }}</button>
                                        </a>
                                        <a href="{{ url('/admin/empresas/distribuidor/' . $item->id . '/edit') }}" title="{{ trans('buttons.general.crud.edit') }} {{ trans('labels.frontend.distribuidor') }}">
                                            <button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> {{ trans('buttons.general.crud.edit') }}</button>
                                        </a>
                                        {!! Form::open([
                                            'method'=>'DELETE',
                                            'url' => ['/admin/empresas/distribuidor', $item->id],
                                            'style' => 'display:inline',
                                            'id' => 'form-delete-'.$item->id
                                        ]) !!}
                                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> '.trans("buttons.general.crud.delete"), array(
                                                    'type' => 'button',
                                                    'class' => 'btn btn-danger btn-xs',
                                                    'title' => trans("buttons.general.crud.delete")." ".trans("labels.frontend.distribuidor"),
                                                    'onclick'=> 'eliminar("form-delete-'.$item->id.'")'
                                            )) !!}
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="pagination-wrapper"> {!! $distribuidores->appends(['search' => Request::get('search')])->render() !!} </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('after-scripts')
{{ Html::script("js/backend/plugin/datatables/jquery.dataTables.min.js") }}
{{ Html::script("js/backend/plugin/datatables/dataTables.bootstrap.min.js") }}
{{ Html::script("js/frontend/sweetalert2.min.js") }}

<script>
    $(document).ready(function() {
        $('#distribuidor-table').DataTable( {
            "paging":   false,
            "info":     false,
            "searching":   false
        });
    } );

    function eliminar(idFormDelete){
        swal({
            title: "Eliminar {{ trans("labels.frontend.distribuidor") }}",
            text: "¿Realmente desea eliminar este Distribuidor?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#ff8726',
            confirmButtonText: "Eliminar"
        }).then((result) => {
            if (result.value) {
                $('form#'+idFormDelete).submit();
            }
        });
    }
</script>

@endsection