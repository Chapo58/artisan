@extends ('backend.layouts.app')

@section ('title', trans('labels.frontend.distribuidor_titulo')." - ".trans('buttons.general.crud.edit'))

@section('content')
    <div class="main-content">
        <div class="main-content-inner">
            
            <div class="page-header">
                <h1>
                    {{ trans('labels.frontend.distribuidor_titulo') }}
                    <small>
                        <i class="ace-icon fa fa-angle-double-right"></i>
                        <small>{{ trans('buttons.general.crud.edit') }} Distribuidor </small>
                        <small><i class="ace-icon fa fa-angle-double-right"></i></small>
                        <small>{{ $distribuidor }}</small>
                    </small>

                    <div class="box-tools pull-right">
                        <a href="{{ url('/admin/empresas/distribuidor') }}" title="{{ trans('buttons.general.crud.back') }}" class="btn btn-warning btn-sm">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> {{ trans('buttons.general.crud.back') }}
                        </a>
                    </div>
                </h1>
            </div>

            <div class="row">
                <div class="col-xs-12">

                    {!! Form::model($distribuidor, [
                        'method' => 'PATCH',
                        'url' => ['/admin/empresas/distribuidor', $distribuidor->id],
                        'class' => 'form-horizontal',
                        'files' => true
                    ]) !!}

                        @include ('backend.empresas.distribuidor.form', ['submitButtonText' => trans("buttons.general.crud.update")])

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@endsection

@section('after-scripts')

    @yield('scripts-dialog-provincia')
    @yield('scripts-dialog-localidad')

@endsection