@extends ('backend.layouts.app')

@section ('title', trans('labels.frontend.empresa_titulo')." - ".trans('navs.frontend.user.administration'))

@section('after-styles')
    {{ Html::style("css/backend/plugin/datatables/dataTables.bootstrap.min.css") }}
@endsection

@section('content')
    <div class="main-content">
        <div class="main-content-inner">
            
            <div class="page-header">
                <h1>
                    {{ trans('labels.frontend.empresa_titulo') }}
                    <small>
                        <i class="ace-icon fa fa-angle-double-right"></i>
                        <small>Generar Facturas Automáticamente</small>
                    </small>

                    <div class="box-tools pull-right">
                    </div><!--box-tools pull-right-->
                </h1>
            </div>

            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Generar Facturas Automaticamente</h3>
                </div>
                <div class="box-body">
                    Facturas Actualizadas: {{ $actualizadas }}</br>
                    Facturas Generadas: {{ $generadas }}
                </div>
            </div>
        </div>
    </div>
@endsection