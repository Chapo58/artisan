@extends('landing.layouts.app')

@section('title', app_name())

@section('after-styles')
    <link href="{{asset('landing/css/blocks.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

    <div class="content-wrapper">
        <div class="header-spacer"></div>
        <!-- Main Slider -->

        <div class="container-full-width">
            <div class="swiper-container main-slider" data-effect="fade" data-autoplay="4000">

                <!-- Additional required wrapper -->
                <div class="swiper-wrapper">
                    <!-- Slides -->
                    <div class="swiper-slide bg-border-color">

                        <div class="container">
                            <div class="row table-cell">

                                <div class="col-lg-12">

                                    <div class="slider-content align-center">

                                        <h1 class="slider-content-title" data-swiper-parallax="-100">¡Artisan Gratuito!</h1>
                                        <h5 class="slider-content-text c-gray" data-swiper-parallax="-200">Podes comenzar a usar el sistema ahora mismo, es gratis!</h5>

                                        <div class="main-slider-btn-wrap" data-swiper-parallax="-300">

                                            <a href="{{url('landing/registro')}}"
                                               class="btn btn-medium btn--dark btn-hover-shadow">
                                                <span class="text">¿Gratis? ¡Lo quiero!</span>
                                                <span class="semicircle"></span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="slider-thumb" data-swiper-parallax="-400" data-swiper-parallax-duration="600">
                                        <img src="{{url('landing/img/slider1.png')}}" alt="slider">
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="swiper-slide bg-primary-color main-slider-bg-dark thumb-left">

                        <div class="container table full-height">
                            <div class="row table-cell">

                                <div class="col-lg-5 table-cell">
                                    <div class="slider-content">
                                        <h3 class="slider-content-title" data-swiper-parallax="-100"><span class="c-dark">E-Commerce Integrado</span>
                                            ¡Digitalizá tu empresa!</h3>
                                        <h6 class="slider-content-text" data-swiper-parallax="-200">El sistema cuenta con un ecommerce integrado totalmente sincronizado con tus clientes y artículos. Vende en todo el mundo!
                                        </h6>

                                        <div class="main-slider-btn-wrap" data-swiper-parallax="-300">

                                            <a href="{{url('landing/ecommerce')}}"
                                               class="btn btn-medium btn--dark btn-hover-shadow">
                                                <span class="text">Quiero saber más</span>
                                                <span class="semicircle"></span>
                                            </a>

                                            <a href="{{url('landing/servicios')}}" class="btn btn-small btn--primary"
                                               data-swiper-parallax="-300">
                                                <span class="text">Nuestros Servicios</span>
                                                <i class="seoicon-right-arrow"></i>
                                            </a>

                                        </div>

                                    </div>
                                </div>

                                <div class="col-lg-7 table-cell">
                                    <div class="slider-thumb" data-swiper-parallax="-300" data-swiper-parallax-duration="500">
                                        <img src="{{url('landing/img/slider2.png')}}" alt="slider">
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide bg-secondary-color main-slider-bg-dark">

                        <div class="container table full-height">
                            <div class="row table-cell">
                                <div class="col-lg-6 table-cell">

                                    <div class="slider-content">

                                        <h3 class="h1 slider-content-title c-dark" data-swiper-parallax="-100">Fácil, Intuitivo y Veloz
                                        </h3>

                                        <h5 class="slider-content-text" data-swiper-parallax="-200">Artisan cuenta con secciones simplificadas como "Cargas Rápidas de Artículos" y "Ventas Rápidas" para que en minutos ya estés vendiendo.
                                        </h5>

                                        <div class="main-slider-btn-wrap" data-swiper-parallax="-300">

                                            <a href="{{url('landing/servicios')}}" class="btn btn-medium btn--dark btn-hover-shadow">
                                                <span class="text">Ver Más</span>
                                                <span class="semicircle"></span>
                                            </a>

                                            <a href="{{url('landing/registro')}}" class="btn btn-medium btn-border">
                                                <span class="text">Registrarse</span>
                                                <span class="semicircle"></span>
                                            </a>

                                        </div>

                                    </div>

                                </div>
                                <div class="col-lg-6 table-cell">
                                    <div class="slider-thumb" data-swiper-parallax="-300" data-swiper-parallax-duration="500">
                                        <img src="{{url('landing/img/slider3.png')}}" alt="slider">
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide bg-orange-color main-slider-bg-dark">
                        <div class="container table full-height">
                            <div class="row table-cell">

                                <div class="col-lg-12">

                                    <div class="slider-content align-center">
                                        <h3 class="h1 slider-content-title c-dark" data-swiper-parallax="-100">Facturación Electrónica</h3>
                                        <h5 class="slider-content-text" data-swiper-parallax="-200">
                                            Cada vez que realices una factura electrónica, el comprobante le llegara automáticamente a tu cliente a su mail para que no gastes más papel!
                                        </h5>

                                        <div class="main-slider-btn-wrap" data-swiper-parallax="-300">

                                            <a href="{{url('landing/servicios')}}"
                                               class="btn btn-medium btn--dark btn-hover-shadow">
                                                <span class="text">Ver Más</span>
                                                <span class="semicircle"></span>
                                            </a>

                                        </div>

                                    </div>

                                </div>

                                <div class="col-lg-12">
                                    <div class="slider-thumb" data-swiper-parallax="-400" data-swiper-parallax-duration="600">
                                        <img src="{{url('landing/img/slider4.png')}}" alt="slider">
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide bg-green-color main-slider-bg-dark">

                        <div class="container table full-height">
                            <div class="row table-cell">

                                <div class="col-lg-6 table-cell">
                                    <div class="slider-content">

                                        <h3 class="h1 slider-content-title c-dark" data-swiper-parallax="-100">Pagos Electrónicos
                                        </h3>

                                        <h5 class="slider-content-text" data-swiper-parallax="-200">
                                            Con tu ecommerce integrado simplifica los pagos de tus clientes mediante MercadoPago, PayPal o trasferencias bancarias.
                                        </h5>

                                        <div class="main-slider-btn-wrap" data-swiper-parallax="-300">

                                            <a href="{{url('landing/servicios')}}"
                                               class="btn btn-medium btn--dark btn-hover-shadow">
                                                <span class="text">Ver Más</span>
                                                <span class="semicircle"></span>
                                            </a>

                                            <a href="{{url('landing/registro')}}"
                                               class="btn btn-medium btn-border btn-hover-shadow">
                                                <span class="text">¡Quiero Comenzar!</span>
                                                <span class="semicircle"></span>
                                            </a>

                                        </div>

                                    </div>
                                </div>

                                <div class="col-lg-6 table-cell">
                                    <div class="slider-thumb" data-swiper-parallax="-300" data-swiper-parallax-duration="500">
                                        <img src="{{url('landing/img/slider5.png')}}" alt="slider">
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <!--Prev next buttons-->

                <svg class="btn-next btn-next-black">
                    <use xlink:href="#arrow-right"></use>
                </svg>

                <svg class="btn-prev btn-prev-black">
                    <use xlink:href="#arrow-left"></use>
                </svg>

                <!--Pagination tabs-->

                <div class="slider-slides">
                    <a href="#" class="slides-item bg-border-color main-slider-bg-light">
                        <div class="content">
                            <div class="text-wrap">
                                <h4 class="slides-title">¡Artisan Gratuito!</h4>
                            </div>
                            <div class="slides-number">01</div>
                        </div>
                    </a>

                    <a href="#" class="slides-item bg-primary-color">
                        <div class="content">
                            <div class="text-wrap">
                                <h4 class="slides-title">E-Commerce Integrado</h4>
                                <div class="slides-sub-title">¡Digitalizá tu empresa!</div>
                            </div>
                            <div class="slides-number">02</div>
                        </div>
                    </a>

                    <a href="#" class="slides-item bg-secondary-color">
                        <div class="content">
                            <div class="text-wrap">
                                <h4 class="slides-title">Fácil, Intuitivo y Veloz</h4>
                            </div>
                            <div class="slides-number">03</div>
                        </div>
                    </a>

                    <a href="#" class="slides-item bg-orange-color">
                        <div class="content">
                            <div class="text-wrap">
                                <h4 class="slides-title">Facturación Electrónica</h4>
                            </div>
                            <div class="slides-number">04</div>
                        </div>
                    </a>

                    <a href="#" class="slides-item bg-green-color">
                        <div class="content">
                            <div class="text-wrap">
                                <h4 class="slides-title">Pagos Electrónicos</h4>
                            </div>
                            <div class="slides-number">05</div>
                        </div>
                    </a>
                </div>
            </div>
        </div>


        <!-- ... End Main Slider -->


        <!-- Register -->

        <div class="container">
            <div class="row medium-padding120">
                <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">

                    <div class="heading">
                        <h4 class="h1 heading-title">Comenzá ahora <br>¡Es Gratis!</h4>
                        <div class="heading-line">
                            <span class="short-line"></span>
                            <span class="long-line"></span>
                        </div>

                        <h5 class="heading-subtitle">¡Regístrate en segundos y comenzá a optimizar tu tiempo!</h5>

                        <p>Te ayudamos a automatizar la gestión de tu empresa, minimizar el riesgo de errores y reducir el tiempo empleado en el levamiento y registro de datos en tareas administrativas.
                        </p>
                    </div>
                </div>

                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">

                    {{ Form::open(['route' => 'frontend.auth.register', 'class' => 'contact-form']) }}

                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <input name="razon_social" class="email input-standard-grey" placeholder="Razón Social" type="text" required>
                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <select name="condicion_iva_id" required>
                                    <option data-display="Condición IVA">Condición IVA</option>
                                    @foreach($condiciones_iva as $condicion)
                                        <option value="{{$condicion->id}}">{{$condicion}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <input name="email" class="email input-standard-grey" required placeholder="Email" type="email">
                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <input name="password" class="email input-standard-grey" required placeholder="Contraseña" type="password">
                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <input name="password_confirmation" class="email input-standard-grey" placeholder="Confirmar Contraseña" type="password" required>
                            </div>
                        </div>
                        <div class="submit-block">
                            <div class="row table">
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 table-cell">
                                    <button id="registrarse" class="btn btn-medium btn--breez btn-hover-shadow">
                                        <span class="text">Registrarse</span>
                                        <span class="semicircle"></span>
                                    </button>
                                </div>

                                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 table-cell">
                                    <div class="remember-wrap">
                                        <div class="checkbox">
                                            <input id="terminos" type="checkbox" name="terminos" value="terminos" checked>
                                            <label for="terminos">He leído y acepto los <a style="color:#768723" href="{{url('/landing/terminos')}}" target="_blank">términos y condiciones</a> de uso.</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    {{ Form::close() }}

                </div>
            </div>
        </div>

        <!-- ... End Register -->


        <!-- Login -->

        <div class="container-fluid">
            <div class="row">

                <div class="seo-score scrollme">

                    <div class="container">
                        <div class="row">
                            <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-xs-12 col-sm-12">
                                <div class="seo-score-content align-center">

                                    <div class="heading align-center">
                                        <h4 class="h1 heading-title">¿Ya sos cliente?</h4>
                                        <p class="heading-text">Ingresá tus datos a continuación para acceder al sistema</p>
                                    </div>

                                    <div class="seo-score-form">

                                        {{ Form::open(['route' => 'frontend.auth.login', 'class' => 'seo-score-form input-inline', 'method' => 'POST']) }}

                                            <div class="row">

                                                <div class="col-lg-6 no-padding col-md-12 col-xs-12 col-sm-12">
                                                    <input name="email" class="input-dark site" required="required" placeholder="Usuario" type="email">
                                                </div>
                                                <div class="col-lg-6 no-padding col-md-12 col-xs-12 col-sm-12">
                                                    <input name="password" class="input-dark e-mail" required="required" placeholder="Contraseña" type="password">
                                                </div>

                                            </div>

                                            <button type="submit" class="btn btn-medium btn--green btn-hover-shadow">
                                                <span class="text">Ingresar!</span>
                                                <span class="semicircle"></span>
                                            </button>

                                            <button type="button" class="btn btn-medium btn--olive btn-hover-shadow" onclick="demoLogin()">
                                                <span class="text">Demo</span>
                                                <span class="semicircle"></span>
                                            </button>

                                        {{ Form::close() }}

                                    </div>

                                    @include('includes.partials.messages')

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="images">
                        <img src="{{url('landing/img/seoscore1.png')}}" alt="image">
                        <img src="{{url('landing/img/seoscore2.png')}}" alt="image">
                        <img src="{{url('landing/img/seoscore3.png')}}" alt="image">
                    </div>

                </div>
            </div>
        </div>

        <!-- ... End Login -->

        <div class="container">
            <div class="row medium-padding120">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mb30">
                    <img src="{{url('landing/img/mac-book.png')}}" alt="pc">
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <h5 class="mb30">Te ayudamos a crecer y vender más</h5>
                    <p>
                        Aumentá la productividad de tu negocio con un sistema de gestión comercial fácil de usar, y con toda la funcionalidad que necesitas para potenciar tu éxito.
                    </p>
                    <p>
                        Controlá todas tus operaciones de ventas, compras, stock y fondos. Revisá las cuentas corrientes de tus clientes y proveedores. Obtén estadísticas detalladas de todos los movimientos de caja, movimientos bancarios, órdenes de compras y mucho más!
                    </p>
                </div>
            </div>
        </div>


        <div class="container-fluid">
            <div class="row">
                <div class="our-video js-equal-child">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 no-padding">
                        <div class="video theme-module">
                            <div class="video-thumb">
                                <div class="overlay"></div>
                                <a href="https://www.youtube.com/watch?v=0ZjN_GtC-QA" class="video-control js-popup-iframe">
                                    <img src="{{url('landing/svg/video-control.svg')}}" alt="go">
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-12 col-xs-12 no-padding">
                        <div class="content theme-module centered-on-mobile medium-padding100">
                            <div class="heading">
                                <h4 class="h1 heading-title">Intro Artisan</h4>
                                <div class="heading-line">
                                    <span class="short-line"></span>
                                    <span class="long-line"></span>
                                </div>
                                <p class="heading-text">
                                    Maximizá el rendimiento de tu negocio con la herramienta de gestión más potente del mercado.
                                </p>
                                <ul>
                                    <li>Listas de precios ilimitadas</li>
                                    <li>Facturación ilimitada</li>
                                    <li>Clientes y Proveedores ilimitados</li>
                                    <li>Articulos, marcas y categorias ilimitadas</li>
                                    <li>Cajas, movimientos, cuentas bancarias, cheques ilimitados</li>
                                    <li>¡Y mucho más!</li>
                                </ul>
                            </div>
                            <a href="{{url('landing/servicios')}}" class="btn btn-medium btn--secondary">
                                <span class="text">Más Información</span>
                                <span class="semicircle"></span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container-fluid">
            <div class="row medium-padding120 bg-orange-color">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                            <div class="heading">
                                <h4 class="h1 heading-title">Tienda Online</h4>
                                <div class="heading-line">
                                    <span class="short-line bg-yellow-color"></span>
                                    <span class="long-line bg-yellow-color"></span>
                                </div>
                                <p class="heading-text c-white">
                                    Artisan te da la posibilidad de crear tu propia tienda virtual totalmente sincronizada con el sistema de gestión, para que tengas todo en un solo lugar. Al crear tu empresa y cargar tus artículos y servicios, el sistema creará automáticamente un E-Commerce que puedes comenzar a utilizar de inmediato o dejarlo desactivado si lo prefieres.
                                </p>
                            </div>

                            <a href="{{url('landing/ecommerce')}}" style="color:#fff;">
                                <div class="btn btn-medium btn--dark btn-hover-shadow mb30">
                                    <span class="text">Mas Información</span>
                                    <span class="semicircle"></span>
                                </div>
                            </a>
                        </div>

                        <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                            <center><img src="{{url('landing/img/offers1.png')}}" alt="analysis"></center>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <br>

        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
            <div class="heading mb60">
                <h4 class="h1 heading-title text-center">Sistemas en la nube</h4>
                <div class="heading-line">
                    <span class="short-line"></span>
                    <span class="long-line"></span>
                </div>
            </div>
            <ul class="accordion" id="accordion">
                <li class="accordion-panel">
                    <div class="panel-heading">
                        <a href="#noInstalacion"class="accordion-heading collapsed" data-toggle="collapse" data-parent="#accordion" aria-expanded="false">
                            <span class="icon">
                                <i class="fa fa-angle-right" aria-hidden="true"></i>
                                <i class="fa fa-angle-down active" aria-hidden="true"></i>
                            </span>
                            <span class="ovh">NO NECESITA INSTALACIÓN</span>
                        </a>
                    </div>

                    <div id="noInstalacion" class="panel-collapse collapse" aria-expanded="false" role="tree">
                        <div class="panel-info">
                            Artisan no necesita ser instalado y puede utilizarse en cualquier dispositivo, en cualquier momento.
                        </div>
                    </div>
                </li>

                <li class="accordion-panel">
                    <div class="panel-heading">
                        <a href="#noEspacio" class="accordion-heading collapsed" data-toggle="collapse" data-parent="#accordion" aria-expanded="false">
                            <span class="icon">
                                <i class="fa fa-angle-right" aria-hidden="true"></i>
                                <i class="fa fa-angle-down active" aria-hidden="true"></i>
                            </span>
                            <span class="ovh">NO CONSUME ESPACIO</span>
                        </a>
                    </div>
                    <div id="noEspacio" class="panel-collapse collapse" aria-expanded="false" role="tree">
                        <div class="panel-info">
                            No consumen espacio de su disco ni tampoco recursos, por lo que no importa que tan nuevo o antiguo sea su equipo, los sistemas siempre funcionaran a su máxima potencia.
                        </div>
                    </div>
                </li>

                <li class="accordion-panel">
                    <div class="panel-heading">
                        <a href="#backAuto" class="accordion-heading collapsed" data-toggle="collapse" data-parent="#accordion" aria-expanded="false">
                            <span class="icon">
                                <i class="fa fa-angle-right" aria-hidden="true"></i>
                                <i class="fa fa-angle-down active" aria-hidden="true"></i>
                            </span>
                            <span class="ovh">BACKUPs AUTOMÁTICOS</span>
                        </a>
                    </div>
                    <div id="backAuto" class="panel-collapse collapse" aria-expanded="false" role="tree">
                        <div class="panel-info">
                            Garantizamos la seguridad de su información realizando copias de seguridad permanentes para que no necesite preocuparse de volver a perder sus datos. Las copias de seguridad son almacenadas y encriptadas en nuestros servidores para garantizar así también su privacidad.
                        </div>
                    </div>
                </li>

                <li class="accordion-panel">
                    <div class="panel-heading">
                        <a href="#actuAuto" class="accordion-heading collapsed" data-toggle="collapse" data-parent="#accordion" aria-expanded="false">
                            <span class="icon">
                                <i class="fa fa-angle-right" aria-hidden="true"></i>
                                <i class="fa fa-angle-down active" aria-hidden="true"></i>
                            </span>
                            <span class="ovh">ACTUALIZACIONES AUTOMÁTICAS</span>
                        </a>
                    </div>
                    <div id="actuAuto" class="panel-collapse collapse" aria-expanded="false" role="tree">
                        <div class="panel-info">
                            Nuestros sistemas mejoran y evolucionan constantemente a la par de las exigencias de los usuarios.
                            Todas las actualizaciones son instaladas de forma automática sin entorpecer el uso del sistema y sin necesidad de molestar a nuestros clientes de forma presencial.
                        </div>
                    </div>
                </li>

                <li class="accordion-panel">
                    <div class="panel-heading">
                        <a href="#facturacionIlimitada" class="accordion-heading collapsed" data-toggle="collapse" data-parent="#accordion" aria-expanded="false">
                            <span class="icon">
                                <i class="fa fa-angle-right" aria-hidden="true"></i>
                                <i class="fa fa-angle-down active" aria-hidden="true"></i>
                            </span>
                            <span class="ovh">FACTURACIÓN ILIMITADA</span>
                        </a>
                    </div>
                    <div id="facturacionIlimitada" class="panel-collapse collapse" aria-expanded="false" role="tree">
                        <div class="panel-info">
                            Artisan tienen la posibilidad de poder emitir facturas electrónicas desde cualquier dispositivo desde el cual se acceda. Esto significa que podrías facturar desde tu celular si estuvieras de viaje.
                        </div>
                    </div>
                </li>

            </ul>
        </div>

        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
            <div class="heading mb60">
                <h4 class="h1 heading-title text-center">Sistemas instalables</h4>
                <div class="heading-line">
                    <span class="short-line"></span>
                    <span class="long-line"></span>
                </div>
            </div>

            <ul class="accordion" id="accordion2">

                <li class="accordion-panel">
                    <div class="panel-heading">
                        <a href="#necesitaInstalacion" class="accordion-heading collapsed" data-toggle="collapse" data-parent="#accordion" aria-expanded="false">
                            <span class="icon">
                                <i class="fa fa-angle-right" aria-hidden="true"></i>
                                <i class="fa fa-angle-down active" aria-hidden="true"></i>
                            </span>
                            <span class="ovh">NECESITA INSTALACIÓN</span>
                        </a>
                    </div>
                    <div id="necesitaInstalacion" class="panel-collapse collapse" aria-expanded="false" role="tree">
                        <div class="panel-info">
                            Necesitan ser previamente instalados y solamente pueden ser utilizados en algunos equipos.
                        </div>
                    </div>
                </li>

                <li class="accordion-panel">
                    <div class="panel-heading">
                        <a href="#consumenEspacio" class="accordion-heading collapsed" data-toggle="collapse" data-parent="#accordion" aria-expanded="false">
                            <span class="icon">
                                <i class="fa fa-angle-right" aria-hidden="true"></i>
                                <i class="fa fa-angle-down active" aria-hidden="true"></i>
                            </span>
                            <span class="ovh">CONSUMEN MUCHO ESPACIO</span>
                        </a>
                    </div>
                    <div id="consumenEspacio" class="panel-collapse collapse" aria-expanded="false" role="tree">
                        <div class="panel-info">
                            Los sistemas, al ser instalados, automáticamente dependen de su equipo para su funcionamiento, por lo que a medida que se van utilizando van consumiendo cada vez mas espacio de su disco y más recursos de su computadora, siendo extremadamente lentos en muchos casos.
                        </div>
                    </div>
                </li>


                <li class="accordion-panel">
                    <div class="panel-heading">
                        <a href="#sinBack" class="accordion-heading collapsed" data-toggle="collapse" data-parent="#accordion" aria-expanded="false">
                            <span class="icon">
                                <i class="fa fa-angle-right" aria-hidden="true"></i>
                                <i class="fa fa-angle-down active" aria-hidden="true"></i>
                            </span>
                            <span class="ovh">SIN BACKUPs AUTOMÁTICOS</span>
                        </a>
                    </div>
                    <div id="sinBack" class="panel-collapse collapse" aria-expanded="false" role="tree">
                        <div class="panel-info">
                            Por lo general, estos sistemas no cuentan con copias de seguridad automáticas sino que la mayoría de veces deben realizarse de forma manual y alojándose también en su computadora, consumiendo más espacio aun en disco por cada copia y con riesgo de pérdida de información si su equipo falla.
                        </div>
                    </div>
                </li>

                <li class="accordion-panel">
                    <div class="panel-heading">
                        <a href="#actuManuales" class="accordion-heading collapsed" data-toggle="collapse" data-parent="#accordion" aria-expanded="false">
                            <span class="icon">
                                <i class="fa fa-angle-right" aria-hidden="true"></i>
                                <i class="fa fa-angle-down active" aria-hidden="true"></i>
                            </span>
                            <span class="ovh">ACTUALIZACIONES MANUALES</span>
                        </a>
                    </div>
                    <div id="actuManuales" class="panel-collapse collapse" aria-expanded="false" role="tree">
                        <div class="panel-info">
                            Actualizaciones esporádicas, muchas veces con costos adicionales, que requieren por lo general molestar al cliente para realizar las actualizaciones correspondientes de un equipo a la vez, perdiendo tiempo. Y en caso de haber fallas, la operación debe volver a repetirse una y otra vez.
                        </div>
                    </div>
                </li>

                <li class="accordion-panel">
                    <div class="panel-heading">
                        <a href="#facturacionLimitada" class="accordion-heading collapsed" data-toggle="collapse" data-parent="#accordion" aria-expanded="false">
                            <span class="icon">
                                <i class="fa fa-angle-right" aria-hidden="true"></i>
                                <i class="fa fa-angle-down active" aria-hidden="true"></i>
                            </span>
                            <span class="ovh">FACTURACIÓN LIMITADA</span>
                        </a>
                    </div>
                    <div id="facturacionLimitada" class="panel-collapse collapse" aria-expanded="false" role="tree">
                        <div class="panel-info">
                            En los sistemas enlatados solo es posible facturar desde un dispositivo por punto de venta.
                        </div>
                    </div>
                </li>

            </ul>
        </div>

        <!-- Background-mountains -->

        <div class="container-fluid">
            <div class="row">
                <div class="background-mountains medium-padding120 scrollme">

                    <div class="images">
                        <img src="{{url('landing/img/mountain1.png')}}" alt="mountain">
                        <img src="{{url('landing/img/mountain2.png')}}" alt="mountain">
                    </div>

                    <div class="container">
                        <div class="row">
                            <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                                <div class="heading align-center">
                                    <h4 class="h1 heading-title">Organízate con Artisan</h4>
                                    <div class="heading-line">
                                        <span class="short-line"></span>
                                        <span class="long-line"></span>
                                    </div>
                                    <p class="heading-text">Con Artisan toda la información de tu empresa está en su lugar para que puedas ver lo que quieras cuando quieras ¡Sin rebuscar entre papeles!
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                <div class="info-box--standard-centered">
                                    <div class="info-box-image">
                                        <img src="{{url('landing/img/info-box7.png')}}" alt="image">
                                    </div>
                                    <div class="info-box-content">
                                        <h4 class="info-box-title">Preciso</h4>
                                        <p class="text">Olvídate de las calculadoras, la información en Artisan siempre es exacta.
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                <div class="info-box--standard-centered">
                                    <div class="info-box-image">
                                        <img src="{{url('landing/img/info-box8.png')}}" alt="image">
                                    </div>
                                    <div class="info-box-content">
                                        <h4 class="info-box-title">Intuitivo</h4>
                                        <p class="text">Fácil de usar, no necesita de cursos de capacitación ni manuales de instrucciones.
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                <div class="info-box--standard-centered">
                                    <div class="info-box-image">
                                        <img src="{{url('landing/img/info-box9.png')}}" alt="image">
                                    </div>
                                    <div class="info-box-content">
                                        <h4 class="info-box-title">Innovador</h4>
                                        <p class="text">Los sistemas de gestión en la nube son la vanguardia de la administración empresarial.
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                <div class="info-box--standard-centered">
                                    <div class="info-box-image">
                                        <img src="{{url('landing/img/info-box10.png')}}" alt="image">
                                    </div>
                                    <div class="info-box-content">
                                        <h4 class="info-box-title">Estadísticas</h4>
                                        <p class="text">Información agrupada y ordenada de las ventas, cuentas corrientes, caja y más.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row align-center">
                            <div class="btn-block">
                                <a href="{{url('landing/servicios')}}" class="btn btn-medium btn--dark">
                                    <span class="text">Más Información</span>
                                    <span class="semicircle"></span>
                                </a>
                                <a href="{{url('landing/registro')}}" class="btn btn-medium btn--breez">
                                    <span class="text">¡Comenzá Ahora!</span>
                                    <span class="semicircle"></span>
                                </a>
                            </div>
                        </div>

                    </div>


                </div>
            </div>
        </div>

        <!-- End Background-mountains -->

        <!-- Subscribe Form -->

        <div class="container-fluid bg-green-color">
            <div class="row">
                <div class="container">

                    <div class="row">

                        <div class="subscribe scrollme">

                            <div class="col-lg-6 col-lg-offset-5 col-md-6 col-md-offset-5 col-sm-12 col-xs-12">
                                <h4 class="subscribe-title">¡Novedades via Email!</h4>
                                    {{ Form::open(['route' => 'landing.suscribirse', 'class' => 'subscribe-form', 'method' => 'POST']) }}
                                    <input class="email input-standard-grey input-white" name="email" required="required" placeholder="Tu dirección de Email" type="email">
                                    <button type="submit" class="subscr-btn">suscribirse
                                        <span class="semicircle--right"></span>
                                    </button>
                                    {{ Form::close() }}
                                <div class="sub-title">Suscríbete a nuestro boletín informativo para estar siempre informado.</div>

                            </div>

                            <div class="images-block">
                                <img src="{{url('landing/img/subscr-gear.png')}}" alt="gear" class="gear">
                                <img src="{{url('landing/img/subscr1.png')}}" alt="mail" class="mail">
                                <img src="{{url('landing/img/subscr-mailopen.png')}}" alt="mail" class="mail-2">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- End Subscribe Form -->
    </div>

@endsection
