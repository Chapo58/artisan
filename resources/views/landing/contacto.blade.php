@extends('landing.layouts.app')

@section('title', app_name().' | Contacto')

@section('after-styles')
    <link href="{{asset('landing/css/blocks.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

<div class="header-spacer"></div>

<div class="content-wrapper">

<!-- Stunning header -->

<div class="stunning-header stunning-header-bg-blue">
    <div class="stunning-header-content">
        <h1 class="stunning-header-title">Información de Contacto</h1>
        <ul class="breadcrumbs">
            <li class="breadcrumbs-item">
                <a href="{{url('/')}}">Inicio</a>
                <i class="seoicon-right-arrow"></i>
            </li>
            <li class="breadcrumbs-item active">
                <span href="#">Información de Contacto</span>
                <i class="seoicon-right-arrow"></i>
            </li>
        </ul>
    </div>
</div>

<!-- End Stunning header -->

<div class="container">
    <div class="row pt120 pb80">
        <div class="col-lg-12">
            <div class="heading">
                <h4 class="h1 heading-title">Contactanos</h4>
                <div class="heading-line">
                    <span class="short-line"></span>
                    <span class="long-line"></span>
                </div>
                <p class="heading-text">Si tiene alguna duda o desea adquirir Artisan por favor no dude en contactarnos!
                </p>
            </div>
        </div>
    </div>
</div>

@include('includes.partials.messages')

<!-- Contacts -->

<div class="container-fluid">
    <div class="row medium-padding80 bg-border-color contacts-shadow">
        <div class="container">
            <div class="row">
                <div class="contacts">
                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                        <div class="contacts-item" onclick="javascript:void(Tawk_API.toggle())">
                            <img src="{{url('landing/img/contact7.png')}}" alt="phone">
                            <div class="content">
                                <a href="javascript:void(0)" class="title">Chat</a>
                                <p class="sub-title">Haz clic para abrir el chat</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                        <div class="contacts-item">
                            <img src="{{url('landing/img/contact8.png')}}" alt="phone">
                            <div class="content">
                                <a href="#" class="title">info@artisan.com.ar</a>
                                <p class="sub-title">Soporte Online</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                        <div class="contacts-item">
                            <img src="{{url('landing/img/contact9.png')}}" alt="phone">
                            <div class="content">
                                <a href="https://wa.me/5493534815522" target="_blank" class="title">(0353) - 4815522</a>
                                <p class="sub-title">Lun-Dom 09:00 - 00:00</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- End Contacts -->

<!-- Contact form -->

<div class="container">
    <div class="contact-form medium-padding120">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="heading">
                    <h4 class="heading-title">¿Tienes alguna consulta?</h4>
                    <div class="heading-line">
                        <span class="short-line"></span>
                        <span class="long-line"></span>
                    </div>
                    <p class="heading-text">Por favor contáctenos utilizando el formulario y nos comunicaremos con usted lo antes posible.</p>
                </div>
            </div>
        </div>

        {{ Form::open(['route' => 'landing.contacto.enviarMail', 'class' => 'contact-form', 'method' => 'POST']) }}

            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <input name="nombre" class="email input-standard-grey" placeholder="Su Nombre" type="text" required>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <input name="email" class="email input-standard-grey" placeholder="Dirección Email" type="email" required>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <input name="telefono" class="email input-standard-grey" placeholder="Teléfono" type="text">
                </div>
            </div>


            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <textarea name="mensaje" class="email input-standard-grey" placeholder="Detalles"></textarea>
                </div>
            </div>

            <div class="row">
                <div class="submit-block table">
                    <div class="col-lg-3 table-cell">
                        <button type="submit" class="btn btn-small btn--primary">
                            <span class="text">Enviar Ahora</span>
                        </button>
                    </div>

                  <div class="col-lg-5 table-cell">
                        <div class="submit-block-text">
                            Por favor, háganos saber cualquier cosa en particular para verificar y el mejor momento para contactarlo por teléfono (si se proporciona).
                        </div>
                  </div>
                </div>
            </div>

        {{ Form::close() }}

    </div>
</div>

<!-- End Contact form -->

</div>

@endsection