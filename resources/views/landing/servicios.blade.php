@extends('landing.layouts.app')

@section('title', app_name().' | Servicios')

@section('after-styles')
    <link href="{{asset('landing/css/blocks.css')}}" rel="stylesheet" type="text/css" />
    <style>
        @media only screen and (max-width: 767px) {
            .mpt80 {
                padding-top: 80px;
            }
        }
    </style>
@endsection

@section('content')

<div class="header-spacer"></div>

<div class="content-wrapper">

    <div class="stunning-header with-photo stunning-header-bg-photo4">
        <div class="stunning-header-content">
            <h1 class="stunning-header-title">Servicios</h1>
            <ul class="breadcrumbs">
                <li class="breadcrumbs-item">
                    <a href="{{url('/')}}">Inicio</a>
                    <i class="seoicon-right-arrow"></i>
                </li>
                <li class="breadcrumbs-item active">
                    <span href="#">Servicios</span>
                    <i class="seoicon-right-arrow"></i>
                </li>
            </ul>
        </div>
        <div class="overlay overlay-orange"></div>
    </div>

    <div class="container-fluid">
        <div class="row pt120">
            <div class="container">
                <div class="row">
                    <div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-12 col-xs-12">
                        <div class="heading align-center pb120">
                            <h4 class="h1 heading-title">
                                Gestioná los recursos de tu empresa y tomá decisiones apoyadas en información en tiempo real con Artisan
                            </h4>
                            <div class="heading-line">
                                <span class="short-line"></span>
                                <span class="long-line"></span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="local-seo">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <img src="{{url('landing/img/local-seo1.jpg')}}" alt="local-seo">
                            <img src="{{url('landing/img/local-seo2.jpg')}}" alt="local-seo">
                            <img src="{{url('landing/img/ciatt-demo.gif')}}" style="max-height:438px;" alt="local-seo" class="shadow-image">
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="row medium-padding120 bg-border-color">
            <div class="container">

                <div class="row align-center mpt80">

                    <div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-12 col-xs-12">
                        <div class="h5 c-gray pt100">
                            Una solución completa y totalmente adaptable a las necesidades de las Pymes comerciales e industriales, permite resolver de manera simple y desde cualquier lugar, todas las operaciones de pedidos, venta, facturación, stock, compras, impuestos, contabilidad y demás procesos de tu empresa.
                        </div>

                        <a href="{{url('landing/registro')}}" class="btn btn-medium btn--blue btn-hover-shadow mt60">
                            <span class="text">¡Comenzá a usarlo, es GRATIS!</span>
                            <span class="semicircle"></span>
                        </a>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row mb30">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="info-box--modern">
                    <div class="info-box-image">
                        <img src="{{url('landing/img/info-box25.png')}}" alt="image">
                    </div>
                    <div class="info-box-content">
                        <h5 class="info-box-title">Disponible en todos los dispositivos</h5>
                        <p class="text">
                            Artisan funciona en cualquier dispositivo que tenga acceso a internet, ya sean computadoras, notebooks, tablets o celulares. ¡Accede desde dónde sea y cuándo quieras!
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="info-box--modern">
                    <div class="info-box-image">
                        <img src="{{url('landing/img/info-box26.png')}}" alt="image">
                    </div>
                    <div class="info-box-content">
                        <h5 class="info-box-title">Listas de precios totalmente personalizables</h5>
                        <p class="text">
                            Podés dividir a tus clientes y artículos en precios minoristas, mayoristas, descuentos sin límites. ¡Vos creas tus propias listas!
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <div class="row mb60">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="info-box--modern">
                    <div class="info-box-image">
                        <img src="{{url('landing/img/info-box27.png')}}" alt="image">
                    </div>
                    <div class="info-box-content">
                        <h5 class="info-box-title">Facturación electrónica y controladora fiscal</h5>
                        <p class="text">
                            El sistema funciona con cualquier tipo de punto de venta y es muy fácil de configurar para que en minutos ya estés facturando.
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="info-box--modern">
                    <div class="info-box-image">
                        <img src="{{url('landing/img/info-box28.png')}}" alt="image">
                    </div>
                    <div class="info-box-content">
                        <h5 class="info-box-title">Múltiples depósitos y sucursales</h5>
                        <p class="text">
                            Podes gestionar tus depósitos y sucursales fácilmente desde la vista de artículos, realizar movimientos de stock, crear gerentes, agrupar los productos y mucho más.
                        </p>
                    </div>
                </div>
            </div>
        </div>

    </div>


    <div class="section bg-greendark-color">

        <div class="container">

            <div class="slider-profit-wrap">

                <!-- Slider main container -->
                <div class="swiper-container auto-height pagination-vertical" data-direction="vertical" data-loop="false" data-mouse-scroll="true">

                    <div class="swiper-wrapper">

                        <div class="slider-profit swiper-slide">
                            <div class="row medium-padding120">
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <div class="heading">
                                        <h4 class="h1 heading-title c-white mb30">Lleva tu empresa con vos</h4>
                                        <p class="c-white">
                                            Artisan está en la nube, lo que significa que puedes acceder desde cualquier parte al sistema ya sea desde tu casa o a través de tu celular en un viaje, no importa donde vayas, siempre podrás acceder al sistema con información en tiempo real.
                                        </p>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <div class="slider-profit-thumb">
                                        <center><img src="{{url('landing/img/local-seo3.jpg')}}" style="height:300px;" alt="Ubicacion"></center>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="slider-profit swiper-slide">
                            <div class="row medium-padding120">
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <div class="heading">
                                        <h4 class="h1 heading-title c-white mb30">Organiza tu tiempo</h4>
                                        <p class="c-white">
                                            Las tareas administrativas se vuelven mucho más sencillas con Artisan además contas con un calendario empresarial para poder organizar mejor las responsabilidades y tareas de cada uno a lo largo de la semana o el mes.
                                        </p>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <div class="slider-profit-thumb">
                                        <img src="{{url('landing/img/profit.png')}}" alt="Escritorio">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="slider-profit swiper-slide">
                            <div class="row medium-padding120">
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <div class="heading">
                                        <h4 class="h1 heading-title c-white mb30">¡E-Commerce Integrado!</h4>
                                        <p class="c-white">
                                            Artisan te da la posibilidad de crear tu propia tienda virtual totalmente sincronizada con el sistema de gestión, para que tengas todo en un solo lugar. Al crear tu empresa y cargar tus artículos y servicios, el sistema creará automáticamente un <br> E-Commerce que puedes comenzar a utilizar de inmediato.
                                            ¡Digitalizá tus ventas, llevá tu empresa a todas partes y vendé las 24 horas del día con tu E-Commerce!
                                        </p>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <div class="slider-profit-thumb">
                                        <center><img src="{{url('landing/img/offers1.png')}}" style="height:300px;" alt="Ecommerce"></center>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="slider-profit swiper-slide">
                            <div class="row medium-padding120">
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <div class="heading">
                                        <h4 class="h1 heading-title c-white mb30">App para celulares</h4>
                                        <p class="c-white">
                                            Podés usar la aplicación exclusiva para celulares para acceder, desde la palma de tu mano, a toda la información de tu empresa en tiempo real. Los celulares son importantes ya que permiten acceso instantáneo a tu panel de administración del sistema estes donde estes.
                                        </p>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <div class="slider-profit-thumb">
                                        <center><img src="{{url('landing/img/ppc.png')}}" style="height:300px;" alt="profit"></center>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- If we need pagination -->
                    <div class="swiper-pagination"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row pt120 pb30">
            <div class="col-lg-12">

                <div class="product-description-challenge">

                    <div class="row">

                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">

                            <div class="product-description-thumb">
                                <img src="{{url('landing/img/solutions.jpg')}}" alt="solution" class="shadow-image">
                            </div>
                        </div>

                        <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                            <div class="product-description-content">
                                <div class="heading">
                                    <h4 class="h1 heading-title">Simple, Práctico e Intuitivo</h4>
                                    <div class="heading-line">
                                        <span class="short-line"></span>
                                        <span class="long-line"></span>
                                    </div>
                                    <p>
                                        La interfaz del sistema esta optimizada y organizada de forma que pueda ser comprendida de un simple vistazo. Así mismo cada sección del sistema contiene "Tips" de ayuda para orientar al usuario en la utilización de la herramienta.
                                        Nada es al azar, cada opción del sistema tiene un fin practico que se adapta a las necesidades de cada negocio, cuidando siempre la simplicidad de la interfaz.
                                    </p>
                                </div>
                            </div>
                        </div>


                    </div>

                    <div class="product-description-border"></div>

                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row pt80 pb120">
            <div class="col-lg-12">

                <div class="product-description-solution">

                    <div class="row">

                        <div class="col-lg-5 col-lg-offset-1 col-md-5 col-md-offset-1 col-sm-12 col-xs-12">
                            <div class="product-description-content">
                                <div class="heading">
                                    <h4 class="h1 heading-title">Personalizable y Configurable</h4>
                                    <div class="heading-line">
                                        <span class="short-line"></span>
                                        <span class="long-line"></span>
                                    </div>

                                    <p>
                                        El sistema puede personalizarse a gusto de cada usuario, ya sea modificando su color, el logo de su empresa o incluso la ubicación y orientación del menú del sistema. Estos cambios se guardan en su usuario para que cada vez que ingrese al sistema éste ya esté personalizado y adaptado a su gusto.
                                        Así mismo en la sección de "Configuración" del sistema puede modificar todas las variables que utiliza, aunque la mayoría ya vengan configuradas por defecto para simplificar esta tarea.
                                    </p>

                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">

                            <div class="product-description-thumb">
                                <img src="{{url('landing/img/product-solution.png')}}" alt="challenge" class="shadow-image">
                            </div>
                        </div>

                    </div>

                    <div class="product-description-border"></div>

                </div>
            </div>
        </div>
    </div>

</div>

@endsection