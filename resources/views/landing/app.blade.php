@extends('landing.layouts.app')

@section('title', app_name().' | App Android')

@section('after-styles')
    <link href="{{asset('landing/css/blocks2.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

<div class="header-spacer"></div>

<div class="content-wrapper">

    <div class="stunning-header stunning-header-bg-lime">
        <div class="stunning-header-content">
            <h1 class="stunning-header-title">Aplicación para Android</h1>
            <ul class="breadcrumbs">
                <li class="breadcrumbs-item">
                    <a href="{{url('/')}}">Inicio</a>
                    <i class="seoicon-right-arrow"></i>
                </li>
                <li class="breadcrumbs-item active">
                    <span href="#">Aplicación para Android</span>
                    <i class="seoicon-right-arrow"></i>
                </li>
            </ul>
        </div>
    </div>

    <div class="container">
        <div class="row pt120">
            <div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-12 col-xs-12">
                <div class="heading align-center mb60">
                    <h4 class="h1 heading-title">Tu empresa en tu bolsillo</h4>
                    <div class="heading-line">
                        <span class="short-line"></span>
                        <span class="long-line"></span>
                    </div>

                    <p class="heading-text">
                        Artisan cuenta con una app gratuita que podés instalar desde el Play Store de Android para acceder al sistema de gestión cuando quieras y desde tu celular!
                    </p>
                </div>
            </div>

            <div class="col-lg-12 col-sm-12 col-xs-12">
                <center><img src="{{url('landing/img/ppc.png')}}" alt="App Android"></center>
            </div>

        </div>
    </div>

    <div class="container-fluid">
        <div class="row pt80 pb80 bg-boxed-blue">
            <div class="container">
                <div class="row">

                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 table-cell">

                        <div class="heading">
                            <h4 class="h1 heading-title c-white no-margin">Descargar App</h4>
                            <p class="heading-text c-white no-margin">Haz clic en el siguiente botón para ir a descargar la app:
                            </p>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 table-cell">
                        <a href="https://play.google.com/store/apps/details?id=ar.com.ciatt.www.artisan" target="_blank" class="btn btn-medium btn--dark btn-hover-shadow">
                            <span class="text">Descargar App</span>
                            <span class="semicircle"></span>
                        </a>
                    </div>

                </div>
            </div>
        </div>
    </div>

</div>

@endsection