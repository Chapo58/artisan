<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title', app_name())</title>
	
	<!-- SEO -->
	<meta name="keywords" content="Gestion, Informes, Ciatt, Software, Tienda Virtual, Ecommerce, E-Commerce, Comercio Electronico, App Movil, Factura Electronica, Controladora Fiscal, Listas de Precios, Sucursales" />
    <meta name="description" content="@yield('meta_description', 'Sistema de gestión gratuito con E-Commerce y App Móvil integrados. Una solución completa, moderna, intuitiva y adaptada a tu empresa. Intégrate a la nube con Artisan.')">
    <meta name="author" content="@yield('meta_author', 'Ciatt Software')">

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{{ asset('favicon.png') }}}">
    <!-- For-Mobile-Apps-and-Meta-Tags -->
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link rel="icon" sizes="192x192" href="{{{ asset('favicon.png') }}}">
    <meta name="apple-mobile-web-app-title" content="Artisan Gestión">
    <link href="{{{ asset('apple-touch-icon.png') }}}" rel="apple-touch-icon" />
    <!-- //For-Mobile-Apps-and-Meta-Tags -->
@yield('meta')

@yield('before-styles')

    <link href="{{asset('landing/css/fonts.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('landing/css/crumina-fonts.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('landing/css/normalize.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('landing/css/grid.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('landing/css/base.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('landing/css/layouts.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('landing/css/modules.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('landing/css/widgets-styles.css')}}" rel="stylesheet" type="text/css" />
	<link href="{{asset('landing/css/custom.css')}}" rel="stylesheet" type="text/css" />

    <!--Plugins styles-->
    <link href="{{asset('landing/css/jquery.mCustomScrollbar.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('landing/css/swiper.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('landing/css/primary-menu.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('landing/css/magnific-popup.css')}}" rel="stylesheet" type="text/css" />


    <!--External fonts-->
    <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet" type="text/css" />

@yield('after-styles')

</head>