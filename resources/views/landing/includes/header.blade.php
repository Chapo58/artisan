<div class="top-bar top-bar-dark">
    <div class="container">
        <div class="top-bar-contact">
            <div class="contact-item">
                <i class="fa fa-phone"></i> (0353) - 4815522
            </div>

            <div class="contact-item">
                <i class="fa fa-envelope"></i> <a href="#">info@artisan.com.ar</a>
            </div>

            <div class="contact-item">
                <span>Lun. - Dom.</span> 09:00 - 00:00
            </div>
        </div>

        <div class="follow_us">
            <span>Siguenos:</span>
            <div class="socials">

                <a href="https://www.facebook.com/CiattSoftware/" target="_blank" class="social__item">
                    <img src="{{url('landing/svg/circle-facebook.svg')}}" alt="facebook">
                </a>

                <a href="https://www.instagram.com/ciattsoftware/" target="_blank" class="social__item">
                    <img src="{{url('landing/svg/instagram.svg')}}" alt="instagram">
                </a>

                <a href="" class="social__item">
                    <img src="{{url('landing/svg/google.svg')}}" alt="google">
                </a>

                <a href="https://www.youtube.com/channel/UCLLS2_8vAlZAMy7stvlCUFQ" target="_blank" class="social__item">
                    <img src="{{url('landing/svg/youtube.svg')}}" alt="youtube">
                </a>

            </div>
        </div>

        <a href="#" class="top-bar-close" id="top-bar-close-js">
            <span></span>
            <span></span>
        </a>

    </div>
</div>

<header class="header" id="site-header">

    <div class="container">

        <div class="header-content-wrapper">

            <div class="logo">
                <a href="{{url('/')}}" class="full-block-link"></a>
                <img src="{{url('landing/img/logo2.png')}}" alt="Artisan">
            </div>

            <nav id="primary-menu" class="primary-menu">

                <a href='javascript:void(0)' id="menu-icon-trigger" class="menu-icon-trigger showhide">
                    <span class="mob-menu--title">Menu</span>
                    <span id="menu-icon-wrapper" class="menu-icon-wrapper" style="visibility: hidden">
                            <svg width="1000px" height="1000px">
                                <path id="pathD" d="M 300 400 L 700 400 C 900 400 900 750 600 850 A 400 400 0 0 1 200 200 L 800 800"></path>
                                <path id="pathE" d="M 300 500 L 700 500"></path>
                                <path id="pathF" d="M 700 600 L 300 600 C 100 600 100 200 400 150 A 400 380 0 1 1 200 800 L 800 200"></path>
                            </svg>
                        </span>
                </a>

                <!-- menu-icon-wrapper -->

                <ul class="primary-menu-menu">
                    <li class="menu-item-has-children">
                        <a href="{{url('/')}}">Inicio</a>
                    </li>
                    <li class="">
                        <a href="{{url('landing/servicios')}}">Servicios</a>
                    </li>
                    <li class="">
                        <a href="{{url('landing/ecommerce')}}">E-Commerce</a>
                    </li>
                    <li class="">
                        <a href="{{url('landing/app')}}">App</a>
                    </li>
                    <li class="">
                        <a href="{{url('landing/registro')}}">Comenzar</a>
                    </li>
                    <li class="">
                        <a href="{{url('landing/contacto')}}">Contacto</a>
                    </li>
                </ul>
            </nav>

            <div class="user-menu open-overlay">
                <a href="#" class="user-menu-content  js-open-aside">
                    <span></span>
                    <span></span>
                    <span></span>
                </a>
            </div>
        </div>

    </div>

</header>

<div class="mCustomScrollbar" data-mcs-theme="dark">

    <div class="popup right-menu">

        <div class="right-menu-wrap">

            <div class="user-menu-close js-close-aside">
                <a href="#" class="user-menu-content  js-clode-aside">
                    <span></span>
                    <span></span>
                </a>
            </div>

            <div class="logo">
                <a href="{{url('/')}}" class="full-block-link"></a>
                <img src="{{url('landing/img/logo.png')}}" alt="Artisan">
            </div>

            <p class="text">El mundo es tu empresa. ¡Podes acceder al sistema desde cualquier dispositivo con acceso a internet!
            </p>

        </div>
        @include('includes.partials.messages')
        <div class="widget login">

            {{ Form::open(['route' => 'frontend.auth.login', 'method' => 'POST']) }}
            <h4 class="login-title">Ingresa al sistema</h4>
            <input class="email input-standard-grey" name="email" placeholder="Usuario" type="text">
            <input class="password input-standard-grey" name="password" placeholder="Contraseña" type="password">
            <div class="login-btn-wrap">

                <button type="submit" class="btn btn-medium btn--dark btn-hover-shadow">
                    <span class="text">Ingresar</span>
                    <span class="semicircle"></span>
                </button>

                <button type="button" class="btn btn-medium btn--olive btn-hover-shadow" onclick="demoLogin()">
                    <span class="text">Demo</span>
                    <span class="semicircle"></span>
                </button>

            </div>

            <div class="remember-wrap">
                <div class="checkbox">
                    <input id="remember" type="checkbox" name="remember" value="remember">
                    <label for="remember">Recordarme</label>
                </div>
            </div>
            {{ Form::close() }}

            <hr>
            <div class="helped">¿Olvidaste tu contraseña?</div>
            <div class="helped"><a href="{{url('landing/registro')}}">¡Registrate Ahora! ¡Es Gratis!</a></div>
            <hr>

        </div>



        <div class="widget contacts">

            <h4 class="contacts-title">Contactanos</h4>
            <p class="contacts-text">Estamos aqui para ayudarte.
            </p>

            <div class="contacts-item">
                <img src="{{url('landing/img/contact4.png')}}" alt="phone">
                <div class="content">
                    <a href="https://wa.me/5493534815522" target="_blank" class="title">(0353) - 4815522</a>
                    <p class="sub-title">Lun-Dom 9am-12pm</p>
                </div>
            </div>

            <div class="contacts-item">
                <img src="{{url('landing/img/contact5.png')}}" alt="phone">
                <div class="content">
                    <a href="#" class="title">info@artisan.com.ar</a>
                    <p class="sub-title">Soporte en linea</p>
                </div>
            </div>

            <div class="contacts-item" onclick="javascript:void(Tawk_API.toggle())">
                <img src="{{url('landing/img/contact6.png')}}" alt="phone">
                <div class="content">
                    <a href="javascript:void(0)" class="title">Chat</a>
                    <p class="sub-title">Haz clic para abrir el chat</p>
                </div>
            </div>

        </div>

    </div>

</div>