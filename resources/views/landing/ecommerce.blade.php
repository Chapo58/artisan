@extends('landing.layouts.app')

@section('title', app_name().' | E-Commerce')

@section('after-styles')
    <link href="{{asset('landing/css/blocks.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

<div class="header-spacer"></div>

<div class="content-wrapper">

<!-- Stunning header -->

<div class="stunning-header stunning-header-bg-blue">
    <div class="stunning-header-content">
        <h1 class="stunning-header-title">Comercio Electrónico</h1>
        <ul class="breadcrumbs">
            <li class="breadcrumbs-item">
                <a href="{{url('/')}}">Inicio</a>
                <i class="seoicon-right-arrow"></i>
            </li>
            <li class="breadcrumbs-item active">
                <span href="#">Comercio Electrónico</span>
                <i class="seoicon-right-arrow"></i>
            </li>
        </ul>
    </div>
</div>

<!-- End Stunning header -->

    <div class="container">
        <div class="row pt120 pb360">

            <div class="product-description-ver3">

                <div class="col-lg-12">
                    <div class="heading align-center">
                        <h4 class="h1 heading-title">Tienda Virtual</h4>
                        <div class="heading-line">
                            <span class="short-line"></span>
                            <span class="long-line"></span>
                        </div>
                    </div>
                </div>

                <div class="col-lg-12">
                    <div class="product-description-ver3-thumb">
                        <img src="{{url('landing/img/description-mac.png')}}" alt="Notebook">
                    </div>
                </div>

            </div>

        </div>
    </div>

    <div class="container-fluid">
        <div class="row pt400 bg-border-color">

            <div class="container">


                <div class="row align-center">

                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="heading">

                            <h5 class="mb30">
                                Vende en todo el país, las 24 horas del día con tu tienda virtual ¡Nunca dejes de crecer!
                            </h5>

                            <p>
                                Desde el momento en que creas tu empresa, se creará un E-Commerce totalmente gratuito para que puedas incluir en tu sitio web de forma rápida y fácil. ¿No tienes sitio web? Tan solo contáctate con nosotros a través de cualquiera de los canales de comunicación y te asesoraremos sobre los pasos a seguir para crear tu propio sitio web o ahorrarte el trabajo haciéndolo nosotros mismos en un plazo de 2 días abonando solo los gastos administrativos básicos.
                            </p>

                        </div>

                        <a href="{{url('landing/registro')}}" class="btn btn-medium btn--dark btn-hover-shadow">
                            <span class="text">¡Quiero mi E-Commerce!</span>
                            <span class="semicircle"></span>
                        </a>

                    </div>

                </div>

            </div>


        </div>
    </div>

    <div class="container-fluid">
        <div class="row medium-padding80 bg-border-color">
            <div class="container">

                <div class="row mb60">
                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                        <div class="info-box--standard-bg">
                            <div class="info-box-content">
                                <h4 class="info-box-title">Seguimiento de Envíos</h4>
                                <p class="text">Cada orden de venta cuenta con un estado de proceso y de envío para que tus clientes estén siempre informados sobre sus compras.
                                </p>

                                <div class="info-box-image">
                                    <img src="{{url('landing/img/info-box15.png')}}" alt="image">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                        <div class="info-box--standard-bg">
                            <div class="info-box-content">
                                <h4 class="info-box-title">Mercado Pago</h4>
                                <p class="text">
                                    ¡Todas las comodidades de Mercado Pago en tu Tienda Virtual! Tus clientes podrán pagar con tarjetas de crédito, PagoFacil, RapiPago con descuentos y promociones exclusivas de Mercado Pago!
                                </p>

                                <div class="info-box-image">
                                    <img src="{{url('landing/img/info-box16.png')}}" alt="image">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                        <div class="info-box--standard-bg">
                            <div class="info-box-content">
                                <h4 class="info-box-title">Cuentas Corrientes</h4>
                                <p class="text">
                                    Los clientes que posean cuenta corriente podrán visualizar el estado de la misma desde su mismo panel de E-Commerce además de poder pagar con todas las facilidades del mismo.
                                </p>

                                <div class="info-box-image">
                                    <img src="{{url('landing/img/info-box17.png')}}" alt="image">
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>

<div class="container-fluid">
    <div class="row medium-padding120 bg-orange-color">
        <div class="container">
            <div class="row">
                <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                    <div class="heading">
                        <h4 class="h1 heading-title">Tienda Online</h4>
                        <div class="heading-line">
                            <span class="short-line bg-yellow-color"></span>
                            <span class="long-line bg-yellow-color"></span>
                        </div>
                        <p class="heading-text c-white">
                            Artisan te da la posibilidad de crear tu propia tienda virtual totalmente sincronizada con el sistema de gestión, para que tengas todo en un solo lugar. Al crear tu empresa y cargar tus artículos y servicios, el sistema creará automáticamente un E-Commerce que puedes comenzar a utilizar de inmediato o dejarlo desactivado si lo prefieres.
                        </p>
                    </div>
                </div>

                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                    <center><img src="{{url('landing/img/offers1.png')}}" alt="analysis"></center>
                </div>
            </div>
        </div>
    </div>
</div>

</div>

@endsection