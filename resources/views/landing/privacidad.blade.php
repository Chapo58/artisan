@extends('landing.layouts.app')

@section('title', app_name().' | Politicas de Privacidad')

@section('after-styles')
    <link href="{{asset('landing/css/blocks.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

<div class="header-spacer"></div>

<div class="content-wrapper">

    <div class="stunning-header stunning-header-bg-lime">
        <div class="stunning-header-content">
            <h1 class="stunning-header-title">Politicas de Privacidad</h1>
            <ul class="breadcrumbs">
                <li class="breadcrumbs-item">
                    <a href="{{url('/')}}">Inicio</a>
                    <i class="seoicon-right-arrow"></i>
                </li>
                <li class="breadcrumbs-item active">
                    <span href="#">Politicas de Privacidad</span>
                    <i class="seoicon-right-arrow"></i>
                </li>
            </ul>
        </div>
    </div>

    <div class="container">
        <div class="row pt80">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: justify;color:#353535;">

                <div class="heading align-left mb60" style="text-align: justify;">
                    <h4 class="h4 heading-title">1. NUESTRO COMPROMISO CON LA PRIVACIDAD</h4>
                    <div class="heading-line">
                        <span class="short-line"></span>
                        <span class="long-line"></span>
                    </div>

                    <p>
                        Esta Política de Privacidad indica la información que se puede recopilar a través del sitio
                        https://artisan.com.ar (en adelante, el “SITIO”) y el uso que ARTISAN puede dar a esa información.
                        También explica las medidas de seguridad tomadas para proteger su información, su posibilidad de
                        acceder a su información, y a quién podrá contactar en ARTISAN para que sus preguntas en relación con
                        esta Política de Privacidad sean contestadas, y le sean resueltos otros asuntos que puedan surgir.
                    </p>
                </div>

                <div class="heading align-left mb60" style="text-align: justify;">
                    <h4 class="h4 heading-title">2. DEFINICIONES</h4>
                    <div class="heading-line">
                        <span class="short-line"></span>
                        <span class="long-line"></span>
                    </div>

                    <p>
                        A los fines de la presente Política, y en concordancia con la Ley de Protección de Datos Personales
                        N° 25.326 (en adelante, “LPDP”), los siguientes términos tendrán la acepción que a continuación se da cuenta:
                    </p>
                    <p>
                        a) “Datos Personales”: Información de cualquier tipo referida a personas físicas o de
                        existencia ideal determinadas o determinables.
                    </p>
                    <p>
                        b) “Responsable de la Base de Datos”: A los fines de esta Política, Ciatt Software es el titular de la base de datos.
                    </p>
                    <p>
                        c) “Titular de los datos”: Toda persona física o persona de existencia ideal con domicilio legal o
                        delegaciones o sucursales en el país, cuyos datos sean objeto del tratamiento al que se refiere la presente ley.
                    </p>
                    <p>
                        d) “Tratamiento de datos”: Operaciones y procedimientos sistemáticos, electrónicos o no,
                        que permitan la recolección, conservación, ordenación, almacenamiento, modificación, relacionamiento,
                        evaluación, bloqueo, destrucción, y en general el procesamiento de Datos Personales,
                        así como también su cesión a terceros a través de comunicaciones, consultas, interconexiones o transferencias.
                    </p>
                </div>

                <div class="heading align-left mb60" style="text-align: justify;">
                    <h4 class="h4 heading-title">3. RECOPILACIÓN Y UTILIZACIÓN DE SU INFORMACIÓN</h4>
                    <div class="heading-line">
                        <span class="short-line"></span>
                        <span class="long-line"></span>
                    </div>

                    <p>
                        3.1. Esta Política de Privacidad contempla la recopilación y uso de Datos Personales en el SITIO.
                    </p>
                    <p>
                        3.2. En la medida que usted nos brinda Datos Personales, le comunicamos que esos Datos Personales
                        serán objeto de tratamiento automatizado e incorporados a la base de datos de Ciatt Software.
                    </p>
                    <p>
                        3.3. Se deja constancia que al hacer uso de las aplicaciones existentes en la página ARTISAN,
                        usted acepta los términos de esta política.
                    </p>
                </div>

                <div class="heading align-left mb60" style="text-align: justify;">
                    <h4 class="h4 heading-title">4. DATOS PERSONALES</h4>
                    <div class="heading-line">
                        <span class="short-line"></span>
                        <span class="long-line"></span>
                    </div>

                    <p>
                        4.1. ARTISAN recopila Datos Personales en línea cuando, entre otros casos:<br>
                        • Usted se registra para hacer uso de alguno de los servicios disponibles del SITIO.<br>
                        • Usted utiliza el SITIO o los servicios de ARTISAN.<br>
                        • Usted nos envía preguntas, consultas o comentarios.<br>
                        • Usted solicita información o materiales.<br>
                        • Usted provee de cualquier forma información al SITIO (para la elaboración de reportes,
                        inventarios, facturas, etc.).<br>
                        • Usted efectúa algún reclamo.
                    </p>
                    <p>
                        4.2. El tipo de información recopilada puede incluir nombre, tipo y número de documento,
                        teléfono, dirección de correo electrónico, información financiera y/o cualquier
                        otra información que permita individualizarlo. En todos los casos que usted brinde sus Datos Personales,
                        y de acuerdo a la legislación vigente, usted declara que los Datos Personales brindados son ciertos.
                    </p>
                    <p>
                        4.3. En los casos que usted nos brinde sus Datos Personales, usted acepta y presta su consentimiento libre,
                        expreso e informado para que dichos Datos Personales sean utilizados con las finalidades arriba
                        mencionadas, y autoriza a que la misma sea tratada, almacenada, recopilada en las bases de datos de
                        propiedad de ARTISAN. Asimismo, usted acepta y presta su consentimiento libre, expreso e informado
                        con los términos de la presente Política de Privacidad.
                    </p>
                </div>

                <div class="heading align-left mb60" style="text-align: justify;">
                    <h4 class="h4 heading-title">5. OTRA INFORMACIÓN - COOKIES</h4>
                    <div class="heading-line">
                        <span class="short-line"></span>
                        <span class="long-line"></span>
                    </div>

                    <p>
                        Cuando usted ingresa en el SITIO podemos almacenar alguna información en su computadora bajo la
                        forma de una "Cookie" o archivo similar que puede sernos útil de varias formas. Por ejemplo,
                        las Cookies nos permiten diseñar un sitio Web de forma tal de poder satisfacer en mayor medida sus
                        intereses y preferencias. Con la mayoría de los exploradores para Internet, usted puede borrar
                        las Cookies del disco rígido de su computadora, bloquear todas las Cookies o recibir un mensaje de
                        alerta antes de que se almacene una Cookie. Remitirse a las instrucciones de su explorador o a la
                        pantalla de ayuda para conocer más sobre estas funciones.
                    </p>
                </div>

                <div class="heading align-left mb60" style="text-align: justify;">
                    <h4 class="h4 heading-title">6. CESIÓN DE INFORMACIÓN</h4>
                    <div class="heading-line">
                        <span class="short-line"></span>
                        <span class="long-line"></span>
                    </div>

                    <p>
                        6.1. ARTISAN no vende, alquila ni comercializa a terceros los Datos Personales sobre usted, ni tenemos
                        intenciones de hacerlo.
                    </p>
                    <p>
                        6.2. ARTISAN no transfiere a terceros los Datos Personales que usted nos haya proporcionado para
                        que los mismos sean utilizados en su marketing directo a menos que se lo hubiéramos notificado
                        y obtenido su consentimiento explícito en forma previa.
                    </p>
                    <p>
                        6.3. Los Datos Personales recolectados a través del SITIO serán almacenados en servidores de
                        proveedores de servicios alojados en los EE.UU. en empresas de primera línea como Google y/o
                        Amazon y/u otras de similares estándares de calidad. Al utilizar este SITIO Usted declara conocer
                        y aceptar que los Datos Personales puedan ser transferidos a los proveedores alojados en los EE.UU.
                        a los efectos de la prestación de los servicios proporcionados por ARTISAN.
                    </p>
                </div>

                <div class="heading align-left mb60" style="text-align: justify;">
                    <h4 class="h4 heading-title">7. PROTEGIENDO SUS DATOS PERSONALES</h4>
                    <div class="heading-line">
                        <span class="short-line"></span>
                        <span class="long-line"></span>
                    </div>

                    <p>
                        7.1. Para prevenir acceso no autorizado, mantener la precisión de los datos y
                        asegurar el uso correcto de sus Datos Personales, ARTISAN ha puesto en uso
                        ciertos medios físicos, electrónicos, administrativos y procedimientos de seguridad
                        para resguardar y asegurar los Datos Personales que recopilamos en línea.<br>
                        Nosotros resguardamos los Datos Personales de acuerdo a estándares y procedimientos
                        de seguridad establecidos y continuamente evaluamos nueva tecnología para proteger
                        esa información. ARTISAN garantiza que los procesos internos propios de las bases de
                        datos cumplen con las obligaciones legales de seguridad y confidencialidad
                        impuestas por la Ley de Protección de Datos Personales N° 25.326 y disposiciones
                        complementarias y reglamentarias.
                    </p>
                    <p>
                        7.2. Sin embargo, usted reconoce que los medios técnicos existentes que brindan
                        seguridad no son inexpugnables, y que aun cuando se adopten todos los recaudos
                        razonables de seguridad es posible sufrir manipulaciones, destrucción y/o pérdida
                        de información.
                    </p>
                    <p>
                        7.3. Todas las personas que desarrollen o administren la página, sean empleados,
                        jerárquicos o no, directores, agentes, proveedores, contratistas, etc., y que
                        tengan acceso a Datos Personales recolectados están obligados al secreto
                        profesional inherentes a su cargo. Tal obligación subsiste aun después de
                        finalizada su relación con ARTISAN.
                    </p>
                </div>

                <div class="heading align-left mb60" style="text-align: justify;">
                    <h4 class="h4 heading-title">8. MENORES DE EDAD</h4>
                    <div class="heading-line">
                        <span class="short-line"></span>
                        <span class="long-line"></span>
                    </div>

                    <p>
                        8.1. La utilización de este sitio está prohibida para menores de 18 años y por lo tanto,
                        ARTISAN no tiene intenciones de recopilar Datos Personales de menores de 18 años.
                    </p>
                    <p>
                        8.2. Le informamos que en su condición de padre, tutor legal o representante será
                        el responsable de que sus hijos menores o bajo su tutela accedan al SITIO, por lo
                        que recomendamos enfáticamente tomar las precauciones oportunas durante la navegación en el SITIO.
                        A este fin, le informamos que algunos navegadores permiten configurarse para que los niños no
                        puedan acceder a páginas determinadas.
                    </p>
                    <p>
                        8.3. Usted podrá ponerse en contacto con ARTISAN para realizar las sugerencias o advertencias
                        que considere oportunas.
                    </p>
                </div>

                <div class="heading align-left mb60" style="text-align: justify;">
                    <h4 class="h4 heading-title">9. LINKS EXTERNOS</h4>
                    <div class="heading-line">
                        <span class="short-line"></span>
                        <span class="long-line"></span>
                    </div>

                    <p>
                        El SITIO puede contener links hacia y provenientes de otros sitios de Internet.
                        ARTISAN no es responsable por las prácticas de privacidad ni el tratamiento de
                        los Datos Personales de esos sitios. ARTISAN los alienta a que averigüen las
                        prácticas de privacidad de dichos sitios de Internet antes de su utilización.
                    </p>
                </div>

                <div class="heading align-left mb60" style="text-align: justify;">
                    <h4 class="h4 heading-title">10. MANTENIENDO PRECISOS SUS DATOS PERSONALES</h4>
                    <div class="heading-line">
                        <span class="short-line"></span>
                        <span class="long-line"></span>
                    </div>

                    <p>
                        10.1. Si usted ha proporcionado Datos Personales a través de los servicios disponibles en el
                        SITIO y cualquiera de sus Datos Personales cambia, usted podrá revisar, modificar,
                        eliminar y actualizar sus datos personales en el momento que desee a través del
                        propio SITIO accediendo a su cuenta mediante el ingreso de cuenta y contraseña.
                    </p>
                    <p>
                        10.2. En tal sentido, y de conformidad con la legislación vigente, usted podrá
                        solicitar el ejercicio de su derecho de acceso, rectificación o supresión de sus
                        Datos Personales en línea.
                    </p>
                    <p>
                        10.3. El derecho de acceso a los Datos Personales podrá ser ejercido en forma
                        gratuita por el interesado en intervalos no inferiores a seis meses, salvo que
                        se acredite un interés legítimo al efecto, conforme lo dispuesto por la LPDP y la
                        Disposición 10/2008 de la Dirección Nacional de Protección de Datos Personales.
                    </p>
                    <p>
                        10.4. Si los datos son incorrectos, desea actualizarlos y/o suprimirlos, Usted
                        podrá corregir, actualizar y/o suprimir directamente esa información sin costo
                        alguno, conforme artículo 16 de la Ley 25.326. Para ello, por favor tenga a bien
                        acceder a su cuenta y realizar la corrección, actualización y/o supresión de la
                        información que corresponda, conjuntamente con el objeto de su requerimiento. En
                        caso de requerir asistencia en realizar la tarea puede contactarse con
                        <a href="mailto:info@artisan.com.ar" style="color:blue;">info@artisan.com.ar</a>.
                    </p>
                    <p>
                        10.5. Asimismo, podrá dar de baja su suscripción a cualquiera de los planes
                        de ARTISAN en cualquier momento.
                    </p>
                    <p>
                        10.6. ARTISAN hará todos los esfuerzos razonables para hacer que sus solicitudes
                        sean cumplidas. No obstante, quizás necesitemos información adicional antes
                        de poder procesar su solicitud.
                    </p>
                    <p>
                        10.7. Se hace saber que la DNPDP, Órgano de Control de la Ley de Protección de
                        Datos Personales N° 25.326, tiene la atribución de atender las denuncias y
                        reclamos que se interpongan con relación al incumplimiento de las normas
                        sobre protección de datos personales.
                    </p>
                </div>

                <div class="heading align-left mb60" style="text-align: justify;">
                    <h4 class="h4 heading-title">11. ENCARGADO DE LA PROTECCIÓN DE LOS DATOS PERSONALES</h4>
                    <div class="heading-line">
                        <span class="short-line"></span>
                        <span class="long-line"></span>
                    </div>

                    <p>
                        Atención al cliente
                        (<a href="mailto:info@artisan.com.ar" style="color:blue;">info@artisan.com.ar</a>) es el Encargado de la
                        protección de los Datos Personales y estará a cargo de atender
                        las solicitudes de acceso, corrección y supresión de los Datos
                        Personales que realicen los Titulares de los datos.
                    </p>
                </div>

                <div class="heading align-left mb60" style="text-align: justify;">
                    <h4 class="h4 heading-title">12. CAMBIOS A ESTA POLITICA DE PRIVACIDAD</h4>
                    <div class="heading-line">
                        <span class="short-line"></span>
                        <span class="long-line"></span>
                    </div>

                    <p>
                        ARTISAN se reserva el derecho a modificar esta política de privacidad periódicamente.
                        En tal caso, ARTISAN cursará una notificación a la cuenta de correo electrónico
                        registrada con su cuenta alertando la modificación a efectos de que el
                        Usuario pueda acceder a la misma.
                    </p>
                </div>

                <p>
                    Esta Política de Privacidad fue actualizada por última vez el 07/03/2019.
                </p>

            </div>
        </div>
        <br>
    </div>

</div>

@endsection