@extends('landing.layouts.app')

@section('title', app_name().' | Politicas de Privacidad')

@section('after-styles')
    <link href="{{asset('landing/css/blocks.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

<div class="header-spacer"></div>

<div class="content-wrapper">

    <div class="stunning-header stunning-header-bg-lime">
        <div class="stunning-header-content">
            <h1 class="stunning-header-title">Términos de uso</h1>
            <ul class="breadcrumbs">
                <li class="breadcrumbs-item">
                    <a href="{{url('/')}}">Inicio</a>
                    <i class="seoicon-right-arrow"></i>
                </li>
                <li class="breadcrumbs-item active">
                    <span href="#">Términos de uso</span>
                    <i class="seoicon-right-arrow"></i>
                </li>
            </ul>
        </div>
    </div>

    <div class="container">
        <div class="row pt80">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: justify;color:#353535;">

                <p>
                    Bienvenido al Sitio de Internet artisan.com.ar (en adelante, “ARTISAN” y/o el “Sitio” en forma indistinta), ofrecido por Ciatt Software.
                </p>

                <div class="heading align-left mb60" style="text-align: justify;">
                    <h4 class="h4 heading-title">1. CONDICIONES DE USO DEL SITIO</h4>
                    <div class="heading-line">
                        <span class="short-line"></span>
                        <span class="long-line"></span>
                    </div>

                    <p>
                        Utilizando el Sitio el usuario (en adelante “Usuario” o “Usuarios” según corresponda) acepta sin restricciones, reservas,
                        ni modificaciones sus términos de uso (en adelante, “Términos de Uso”).
                    </p>
                    <p>
                        El Usuario declara poseer capacidad legal para contratar y no tener ningún impedimento según las leyes de la Argentina y/o
                        de las jurisdicciones aplicables, y se obliga a utilizar el Sitio para un propósito lícito.
                    </p>
                    <p>
                        El acceso al Sitio y su utilización, en cualquier forma, implica la aceptación y conocimiento por parte del Usuario de
                        los Términos de Uso. Asimismo, al utilizar los servicios de ARTISAN, el Usuario se obliga a seguir los lineamientos
                        publicados aplicables a estos.
                    </p>
                    <p>
                        Cualquier Usuario que no acepte los presentes Términos de Uso, los cuales tienen carácter obligatorio y vinculante, o no
                        esté satisfecho con ARTISAN por cualquier motivo, deberá abstenerse de utilizar el Sitio y/o los servicios.
                    </p>
                    <p>
                        Los Términos de Uso podrán ser periódicamente modificados y actualizados por ARTISAN. En tal caso, ARTISAN cursará una
                        notificación a la cuenta de correo electrónico registrada con su cuenta y/o a través de mensajes en el propio Sitio
                        alertando la modificación a efectos de que el Usuario pueda acceder a la misma.
                    </p>
                </div>

                <div class="heading align-left mb60" style="text-align: justify;">
                    <h4 class="h4 heading-title">2. SERVICIO Y ACEPTACIÓN</h4>
                    <div class="heading-line">
                        <span class="short-line"></span>
                        <span class="long-line"></span>
                    </div>

                    <p>
                        a) Servicio: ARTISAN ofrece una serie de recursos en línea que permiten emitir facturas electrónicas, generar presupuestos,
                        generar reportes, calcular impuestos, elaborar la contabilidad (en adelante, el “Servicio” o los “Servicios” según
                        corresponda), todos ellos sujetos a los Términos de Uso.
                    </p>
                    <p>
                        b) Calidad: ARTISAN no incurrirá en responsabilidad en caso de que existan interrupciones o un mal funcionamiento de los
                        Servicios ofrecidos en Internet, sin importar la causa, magnitud o tiempo. ARTISAN no se hace responsable por caídas de
                        la red, daños a consecuencia de dichas caídas, o cualquier otro tipo de daño indirecto que les pueda ser causado a los
                        Usuarios.
                    </p>
                    {{--<p>
                        c) Funcionamiento y utilización: El Sitio funciona “TAL CUAL ES”, “CON TODOS SUS FALLOS” y según su “DISPONIBILIDAD” por
                        lo que ARTISAN no otorga garantía de ningún tipo y es el Usuario quien utiliza el Sitio bajo su propio riesgo.
                    </p>--}}
                    <p>
                        c) Suspensión y terminación: ARTISAN podrá, sin necesidad de notificación previa, determinar la suspensión, cancelación y/o
                        terminación inmediata del Sitio; por lo que no será responsable frente al Usuario ni frente a ningún tercero por ningún
                        concepto, ante cualquiera de estas circunstancias.
                    </p>
                    <p>
                        d) Limitaciones: ARTISAN es libre de limitar el acceso al Sitio y/o al Servicio sin necesidad de justificación ni
                        notificación previa.
                    </p>
                    <p>
                        e) Alcance del Sitio: ARTISAN no garantiza que el Sitio sea apropiado o este disponible para su uso en otros lugares
                        fuera del ámbito del territorio de la República Argentina. Aquellos que accedan o hagan uso del Sitio desde otras
                        jurisdicciones lo hacen a su propia voluntad y son responsables de cumplir con las leyes locales correspondientes.
                    </p>
                </div>

                <div class="heading align-left mb60" style="text-align: justify;">
                    <h4 class="h4 heading-title">3. REGISTRACIÓN Y PRIVACIDAD DE LA INFORMACIÓN</h4>
                    <div class="heading-line">
                        <span class="short-line"></span>
                        <span class="long-line"></span>
                    </div>

                    <p>
                        Para poder utilizar los Servicios el Usuario deberá registrarse en el Sitio completando el formulario de
                        registración en todos sus campos con datos personales válidos, manera exactos, precisos y verdaderos
                        (“Datos Personales”). El Usuario asume el compromiso de actualizar los Datos Personales conforme resulte
                        necesario. Cada Usuario garantiza y responde, en cualquier caso, de la veracidad, exactitud, vigencia y
                        autenticidad de los Datos Personales ingresados. El Usuario también deberá seleccionar una contraseña para
                        concluir la registración.
                    </p>
                    <p>
                        En caso de que el Usuario se registre como contador, ARTISAN se reserva el derecho de solicitarle al profesional
                        su número de matrícula o título universitario y constancia de CUIT.
                    </p>
                    <p>
                        El Usuario accederá a su cuenta mediante el ingreso de su email y contraseña seleccionada.
                    </p>
                    <p>
                        El Usuario se obliga a mantener la confidencialidad de su contraseña. La confidencialidad de los datos de
                        la cuenta y contraseña del Usuario así como todas las actividades realizadas en el Sitio bajo su nombre
                        de Usuario y contraseña son de su exclusiva responsabilidad.
                    </p>
                    <p>
                        Todos los Datos Personales que el Usuario proporcione a ARTISAN en relación al Servicio se encontrarán
                        sujetos a las normas aplicables en la República Argentina en cuanto a privacidad, entre ellas la Ley 25.326
                        y sus disposiciones modificatorias y complementarias conforme lo establecido en la <a href="{{url('/landing/privacidad')}}" target="_blank" style="color:blue;">Política de Privacidad</a>.
                    </p>
                    <p>
                        Al crear una cuenta en ARTISAN el Usuario acepta que ARTISAN le envíe emails con información y novedades de ARTISAN.
                    </p>
                </div>

                <div class="heading align-left mb60" style="text-align: justify;">
                    <h4 class="h4 heading-title">4. ALCANCE DE LOS SERVICIOS DE ARTISAN</h4>
                    <div class="heading-line">
                        <span class="short-line"></span>
                        <span class="long-line"></span>
                    </div>

                    <p>
                        Estos Términos de Uso no crean ningún contrato de sociedad, de mandato, de franquicia, o relación laboral ni vínculo
                        contractual alguno entre ARTISAN y el Usuario. El Usuario reconoce y acepta que ARTISAN no es parte en ninguna operación,
                        ni tiene control alguno sobre la calidad, seguridad, legalidad, veracidad o exactitud de los datos vertidos en su
                        software, ni sobre la capacidad e idoneidad de los Usuarios para realizar operaciones mediante la información
                        obtenida mediante el mismo.
                    </p>
                    <p>
                        ARTISAN no podrá verificar la identidad o Datos Personales ingresados por los Usuarios.
                    </p>
                    <p>
                        ARTISAN no garantiza la veracidad de los datos de terceros que aparezcan en el Sitio y no será responsable por la
                        correspondencia o contratos que el Usuario celebre con dichos terceros o con otros Usuarios.
                    </p>
                </div>

                <div class="heading align-left mb60" style="text-align: justify;">
                    <h4 class="h4 heading-title">5. DERECHOS DE PROPIEDAD DE ARTISAN</h4>
                    <div class="heading-line">
                        <span class="short-line"></span>
                        <span class="long-line"></span>
                    </div>

                    <p>
                        El Usuario reconoce y acepta que el Sitio, como todos los contenidos provistos por el Sitio, ya sea cualquier
                        documentación, archivo, material, aplicación, diseño, herramienta, código fuente o cualquier tipo de obra o idea
                        que sea una obra en el sentido de la legislación sobre propiedad intelectual se encuentra protegida bajo las leyes
                        aplicables y Ciatt Software tiene los derechos para su explotación. Asimismo, quedan incluidas dentro del
                        mismo reconocimiento las publicaciones, publicidades o información incorporadas al Sitio por anunciantes
                        o terceras partes.
                    </p>
                    <p>
                        Queda prohibida cualquier forma de reproducción, distribución, exhibición, transmisión, retransmisión, emisión en
                        cualquier forma, almacenamiento en cualquier forma, digitalización, puesta a disposición, traducción, adaptación,
                        arreglo, comunicación pública o cualquier otro tipo de acto por el cual el Usuario pueda servirse comercial o no
                        comercialmente, directa o indirectamente, en su totalidad o parcialmente de cualquiera de los contenidos de las
                        obras sujetas a los derechos descriptos anteriormente. El incumplimiento de lo señalado dará lugar a la aplicación
                        de las sanciones penales y civiles que correspondan.
                    </p>
                </div>

                <div class="heading align-left mb60" style="text-align: justify;">
                    <h4 class="h4 heading-title">6. UTILIZACIÓN DE SOFTWARE REGISTRADO DE ARTISAN</h4>
                    <div class="heading-line">
                        <span class="short-line"></span>
                        <span class="long-line"></span>
                    </div>

                    <p>
                        Ciatt Software tiene los derechos legales y de propiedad intelectual sobre el software ARTISAN y
                        el nombre de dominio artisan.com.ar. El Usuario tiene prohibido copiar, modificar, reproducir o usar de
                        manera alguna estos derechos y cualquier otro derecho de propiedad de ARTISAN sin contar con la previa
                        autorización por escrito de Ciatt Software.
                    </p>
                </div>

                <div class="heading align-left mb60" style="text-align: justify;">
                    <h4 class="h4 heading-title">7. PROHIBICIÓN DE USO DE RECURSOS DE ARTISAN</h4>
                    <div class="heading-line">
                        <span class="short-line"></span>
                        <span class="long-line"></span>
                    </div>

                    <p>
                        El Usuario reconoce que queda prohibido el uso de cualquier recurso técnico por el cual el Usuario o
                        algún tercero puedan beneficiarse, directa o indirectamente, con o sin lucro, de cualquier parte o de la
                        totalidad del Sitio o cualquier fruto del esfuerzo de ARTISAN. Concretamente, queda prohibido todo vinculo (link),
                        hipervínculo (hiperlink), framing o similar al Sitio sin el consentimiento previo, expreso y por escrito por parte
                        de Ciatt Software. Cualquier trasgresión a lo dispuesto en este punto será considerado como lesión a los derechos de
                        Propiedad Intelectual de Ciatt Software sobre el Sitio y todos sus contenidos.
                    </p>
                    <p>
                        El Usuario se compromete a no reproducir, duplicar, copiar, vender, comercializar, revender o explotar con
                        cualquier propósito comercial ninguna parte del Sitio.
                    </p>
                </div>

                <div class="heading align-left mb60" style="text-align: justify;">
                    <h4 class="h4 heading-title">8. POLÍTICA ANTI-SPAM</h4>
                    <div class="heading-line">
                        <span class="short-line"></span>
                        <span class="long-line"></span>
                    </div>

                    <p>
                        El Usuario entiende y conviene en que el envío de publicidad no solicitada a las direcciones de email de ARTISAN
                        (comúnmente denominado “SPAM”) o a través de los sistemas informáticos de ARTISAN queda expresamente prohibido.
                        Todo uso no autorizado de los sistemas informáticos de ARTISAN representa una violación a los Términos de Uso y
                        a las normas vigentes. Dichas violaciones pueden someter al remitente o a sus representantes a
                        sanciones civiles y/o penales.
                    </p>
                </div>

                <div class="heading align-left mb60" style="text-align: justify;">
                    <h4 class="h4 heading-title">9. NEGOCIACIONES CON ORGANIZACIONES E INDIVIDUOS</h4>
                    <div class="heading-line">
                        <span class="short-line"></span>
                        <span class="long-line"></span>
                    </div>

                    <p>
                        ARTISAN no es responsable de las relaciones de los Usuarios con otras organizaciones y/o individuos
                        que el Usuario encuentre en el Sitio o mediante el Servicio. Esto incluye, sin carácter
                        restrictivo los servicios profesionales brindados por los contadores mediante el sitio.
                        En estas negociaciones sólo interviene el Usuario y dichas organizaciones y/o individuos, sin que ARTISAN
                        tenga injerencia en ellas.
                    </p>
                    <p>
                        El Usuario conviene en que ARTISAN no será responsable de las pérdidas ni de los daños en que pueda
                        incurrir el Usuario como resultado de dichas negociaciones. En caso de existir un conflicto entre los
                        Usuarios del Sitio y/o entre los Usuarios y un tercero, el Usuario entiende y conviene en que
                        ARTISAN no posee la obligación de involucrarse en la cuestión. En caso de que el Usuario tenga un
                        conflicto con uno o más Usuarios, el Usuario libera por el presente a ARTISAN, a sus representantes,
                        empleados, agentes y sucesores de los reclamos, las demandas y los daños y perjuicios (efectivos o emergentes)
                        de todo tipo o naturaleza, conocidos o no, que se sospechen o no, divulgados o no, resultantes de o bien
                        en cualquier forma relacionados con dichos conflictos y/o el Servicio.
                    </p>
                </div>

                <div class="heading align-left mb60" style="text-align: justify;">
                    <h4 class="h4 heading-title">10. EMISIÓN DE FACTURAS ELECTRÓNICAS Y TRÁMITES ANTE LA AFIP</h4>
                    <div class="heading-line">
                        <span class="short-line"></span>
                        <span class="long-line"></span>
                    </div>

                    <p>
                        El Usuario se compromete a efectuar los trámites pertinentes ante la AFIP previo a la utilización del
                        sistema de Facturación Electrónica ofrecido por ARTISAN.
                    </p>
                    <p>
                        Asimismo, el Usuario acepta que ARTISAN no se hace responsable de los datos ingresados por el Usuario
                        para la emisión de facturas electrónicas.
                    </p>
                </div>

                <div class="heading align-left mb60" style="text-align: justify;">
                    <h4 class="h4 heading-title">11. PRESCRIPCIÓN SOBRE USO DEL SERVICIO</h4>
                    <div class="heading-line">
                        <span class="short-line"></span>
                        <span class="long-line"></span>
                    </div>

                    <p>
                        El Usuario reconoce que ARTISAN puede establecer límites al uso del Servicio, con inclusión,
                        entre otras cosas, de la cantidad máxima de días que el contenido será retenido por el Servicio,
                        la cantidad de las facturas que pueden emitirse, los mensajes de email, o de todo otro contenido que
                        se puede transmitir o almacenar a través del Servicio, y la frecuencia o cantidad de usuarios que pueden
                        acceder al Servicio. El Usuario conviene en que ARTISAN no es responsable de la eliminación o el fracaso
                        en el almacenamiento de aquel Contenido que se conserva o que se transmite a través del Servicio
                    </p>
                </div>

                <div class="heading align-left mb60" style="text-align: justify;">
                    <h4 class="h4 heading-title">12. FACULTADES SANCIONATORIAS DE ARTISAN</h4>
                    <div class="heading-line">
                        <span class="short-line"></span>
                        <span class="long-line"></span>
                    </div>

                    <p>
                        El Usuario reconoce y acepta que ARTISAN, -a su entera y sola discreción- tiene el derecho
                        (pero no la obligación) de eliminar o desactivar la cuenta del Usuario, de bloquear su dirección
                        de email o IP, o bien de cualquier otra forma cancelar el acceso o el uso del Servicio
                        (o parte de este) por parte del Usuario, en forma inmediata y sin notificación previa.
                        Asimismo, ARTISAN podrá remover o descartar todo contenido que, por cualquier motivo, contraríe
                        la letra o el espíritu de los Términos de Uso, las leyes vigentes así como la moral y las buenas costumbres.
                    </p>
                    <p>
                        En relación a ello, ARTISAN se reserva el derecho de investigar, auditar, advertir, suspender
                        transitoria o definitivamente al Usuario que, a razonable criterio de ARTISAN, incumpla alguna/s
                        de las obligaciones a su cargo contempladas en los Términos de Uso -incluyendo la Política de
                        Privacidad que regula el Servicio. Ello, sin perjuicio de cualquier otra acción legal y/o
                        judicial que pueda resultar de aplicación. Se aclara a su vez que la suspensión transitoria
                        y/o definitiva de un Usuario dispuesta por ARTISAN, importará también la de toda la
                        información proporcionada y obtenida mediante el sitio.
                    </p>
                    <p>
                        El Usuario conviene en que ARTISAN no será responsable ante el Usuario ni ante un tercero
                        por la cancelación del acceso del Usuario al Servicio. Además, el Usuario conviene en
                        no intentar usar el Servicio luego de la cancelación por parte de ARTISAN.
                    </p>
                </div>

                <div class="heading align-left mb60" style="text-align: justify;">
                    <h4 class="h4 heading-title">13. LIMITACIÓN DE RESPONSABILIDAD DE ARTISAN</h4>
                    <div class="heading-line">
                        <span class="short-line"></span>
                        <span class="long-line"></span>
                    </div>

                    <p>
                        El Usuario reconoce que ARTISAN no asume responsabilidad alguna, ya sea directa o indirecta, prevista o
                        imprevista, por cualquier tipo de daños, ya sea emergente o lucro cesante, derivada del mal
                        uso del Sitio o sus contenidos realizado por el Usuario o terceros.
                    </p>
                    <p>
                        Bajo ninguna circunstancia ARTISAN será responsable por cualquier contenido del Sitio, incluyendo,
                        pero sin limitarse, a cualquier error u omisión en cualquier contenido, o por la pérdida o daño de
                        cualquier contenido.
                    </p>
                    <p>
                        El Usuario acepta expresamente que ARTISAN no será responsable por los daños causados a raíz de la falla o
                        cualquier discontinuidad en el Sitio. Eventualmente, y de corresponder, ARTISAN será responsable hasta el
                        límite de lo abonado por el Usuario en razón de la utilización de los servicios del Sitio, y en la medida
                        que la causa que originara alguna afectación a los derechos del Usuario pueda ser directamente
                        imputable a ARTISAN.
                    </p>
                    <p>
                        El Usuario acepta expresamente que ARTISAN no será responsable por el acceso no autorizado, copias,
                        alteraciones o eliminaciones de datos que puedan realizar terceros en el Sitio, ni de sus
                        consecuencias y/o daños.
                    </p>
                    <p>
                        El Usuario reconoce y acepta que ARTISAN no será responsable por la información vertida por el Usuario o un
                        tercero en el Sitio ni por la utilización que los mismos hagan de esta.
                    </p>
                    <p>
                        El Usuario reconoce y acepta que ARTISAN no será responsable por los servicios profesionales brindados mediante el sitio.
                    </p>
                </div>

                <div class="heading align-left mb60" style="text-align: justify;">
                    <h4 class="h4 heading-title">14. INDEMNIZACIÓN</h4>
                    <div class="heading-line">
                        <span class="short-line"></span>
                        <span class="long-line"></span>
                    </div>

                    <p>
                        El Usuario se compromete a indemnizar y mantener indemne y libre de daños a ARTISAN, sus
                        subsidiarias, controlantes, empresas vinculadas de y contra toda y cualquier acción o
                        juicio de responsabilidad, reclamo, denuncia, penalidad, intereses, costos, gastos, multas,
                        honorarios, iniciado por terceros debido a o con origen en cualquiera de sus acciones en el Sitio.
                    </p>
                </div>

                <div class="heading align-left mb60" style="text-align: justify;">
                    <h4 class="h4 heading-title">15. NOTIFICACIONES</h4>
                    <div class="heading-line">
                        <span class="short-line"></span>
                        <span class="long-line"></span>
                    </div>

                    <p>
                        Se considerará que una notificación, intimación, o cualquier otro tipo de comunicación hecha al
                        Usuario ha sido válidamente cursada cuando se dirija a una dirección, ya sea física o electrónica
                        (por ejemplo, casilla de correo electrónico) que haya sido suministrada por el Usuario al
                        momento de la registración o actualización de los datos de registro, o desde la cual éste opere asiduamente.
                    </p>
                </div>

                <div class="heading align-left mb60" style="text-align: justify;">
                    <h4 class="h4 heading-title">16. TOLERANCIA </h4>
                    <div class="heading-line">
                        <span class="short-line"></span>
                        <span class="long-line"></span>
                    </div>

                    <p>
                        La tolerancia que ARTISAN pueda realizar respecto del ejercicio de algún derecho o
                        disposición de los Términos de Uso nunca constituirán una renuncia al mismo.
                    </p>
                </div>

                <div class="heading align-left mb60" style="text-align: justify;">
                    <h4 class="h4 heading-title">17. LEGISLACIÓN APLICABLE</h4>
                    <div class="heading-line">
                        <span class="short-line"></span>
                        <span class="long-line"></span>
                    </div>

                    <p>
                        Los presentes Términos de Uso, así como la relación entre el Usuario y ARTISAN están regidos
                        por las leyes de la República Argentina.
                    </p>
                    <p>
                        El Usuario y ARTISAN acuerdan someterse a la jurisdicción de los Tribunales Ordinarios en
                        lo Comercial de la Ciudad de Villa Maria, con exclusión de cualquier otro fuero o
                        jurisdicción que pudiera corresponder, para el caso de cualquier divergencia relacionada con el Sitio.
                    </p>
                    <p>
                        La utilización que el Usuario realice del Sitio no establecerá entre el Usuario y ARTISAN una
                        relación del tipo de consumo.
                    </p>
                </div>

                <div class="heading align-left mb60" style="text-align: justify;">
                    <h4 class="h4 heading-title">18. AMPLIACIÓN DE LA AFECTACIÓN </h4>
                    <div class="heading-line">
                        <span class="short-line"></span>
                        <span class="long-line"></span>
                    </div>

                    <p>
                        Todas las condiciones estipuladas respecto de ARTISAN en los presentes Términos de Uso se aplican
                        también a sus sociedades contratantes, controlantes, controladas o vinculadas bajo su control común.
                    </p>
                </div>

                <div class="heading align-left mb60" style="text-align: justify;">
                    <h4 class="h4 heading-title">19. DENUNCIAS</h4>
                    <div class="heading-line">
                        <span class="short-line"></span>
                        <span class="long-line"></span>
                    </div>

                    <p>
                        En caso que Ud. observe lo que considere una violación a estos Términos de Uso,
                        ARTISAN le solicita que denuncie este hecho en <a href="mailto:info@artisan.com.ar" style="color:blue;">info@artisan.com.ar</a>.
                    </p>
                </div>

                <p>
                    Estos Términos de Uso fueron actualizados por última vez el 27/02/2019.
                </p>

            </div>
        </div>
        <br>
    </div>

</div>

@endsection