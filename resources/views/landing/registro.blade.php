@extends('landing.layouts.app')

@section('title', app_name().' | Registro')

@section('after-styles')
    <link href="{{asset('landing/css/blocks2.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

<div class="header-spacer"></div>

<div class="content-wrapper">

    <div class="container-fluid">
        <div class="row pt80 pb80 bg-boxed-blue">
            <div class="container">
                <div class="row">

                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 table-cell">

                        <div class="heading">
                            <h4 class="h1 heading-title c-white no-margin">¿Ya sos cliente?</h4>
                            <p class="heading-text c-white no-margin">Usa tu dirección de email y contraseña para inciar sesión.
                            </p>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 table-cell">
                        <a href="{{url('/login')}}" class="btn btn-medium btn--dark btn-hover-shadow">
                            <span class="text">Ingresar a Artisan</span>
                            <span class="semicircle"></span>
                        </a>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row pt100">

            <div class="col-lg-12">

                <div class="heading align-center pb80">
                    <h4 class="h1 heading-title">Comenzá a usar Artisan ¡Es GRATIS!</h4>
                    <div class="heading-line">
                        <span class="short-line"></span>
                        <span class="long-line"></span>
                    </div>
                    <p class="heading-text">¡Regístrate en segundos y comenzá a optimizar tu tiempo!
                    </p>
                </div>

            </div>

        </div>
    </div>

    <div class="container-fluid">

        <div class="row bg-boxed-black medium-padding120">

            <div class="container">

                <div>

                    {{ Form::open(['route' => 'frontend.auth.register', 'class' => 'contact-form']) }}

                        <div class="col-lg-8 col-lg-offset-2 col-md-12 col-md-offset-0 col-sm-12 col-xs-12">
                            <div class="row">
                                <div class="col-lg-12">
                                    <label for="contact_website" class="input-title">Ingrese el nombre o razón social de su empresa<abbr class="required" title="required">*</abbr></label>
                                    <span class="checked-icon">
                                <input class="email focus-white input-standard-grey input-dark" id="razon_social"  name="razon_social" required placeholder="Razón Social" type="text">
                            </span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <label for="contact_name" class="input-title">Condición de IVA de la empresa<abbr class="required" title="required">*</abbr></label>
                                    <select name="condicion_iva_id" required>
                                        <option data-display="Condición IVA">Condición IVA</option>
                                        @foreach($condiciones_iva as $condicion)
                                            <option value="{{$condicion->id}}">{{$condicion}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <label for="contact_email" class="input-title">Dirección de Email<abbr class="required" title="required">*</abbr></label>
                                    <span class="checked-icon">
                                    <input class="email focus-white input-standard-grey input-dark" id="email" required name="email" placeholder="Email" type="email">
                                </span>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <label for="contact_phone" class="input-title">Contraseña<abbr class="required" title="required">*</abbr></label>
                                    <input class="email focus-white input-standard-grey input-dark" id="password" name="password" placeholder="Contraseña" required type="password">
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <label for="contact_company" class="input-title">Confirmar Contraseña<abbr class="required" title="required">*</abbr></label>
                                    <input class="email focus-white input-standard-grey input-dark" id="password_confirmation" name="password_confirmation" placeholder="Confirmar"  requiredtype="password">
                                </div>
                            </div>

                            <div class="row">
                                <div class="submit-block table">
                                    <div class="col-lg-5 table-cell">
                                        <button id="registrarse" class="btn btn-medium btn--green btn-hover-shadow">
                                            <span class="text">¡Comenzar Ahora!</span>
                                            <span class="semicircle"></span>
                                        </button>
                                    </div>

                                    <div class="col-lg-7 table-cell">
                                        <div class="remember-wrap">
                                            <div class="checkbox">
                                                <input id="terminos" type="checkbox" name="terminos" value="terminos" checked>
                                                <label for="terminos" style="color:#C5C5C5">He leído y acepto los <a style="color:#fff" href="{{url('/landing/terminos')}}" target="_blank">términos y condiciones</a> de uso.</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    {{ Form::close() }}

                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row pt100">

            <div class="col-lg-12">

                <img src="{{url('/landing/img/seo-analysis.png')}}" alt="Escritorio">

            </div>

        </div>
    </div>

</div>

@endsection