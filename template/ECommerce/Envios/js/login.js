$(document).ready(function() {
	$("#loginRegisterModal button").on('click', function(){
		$(".btn-active").addClass("btn-disabled");
		$(".btn-disabled").removeClass("btn-active");
		$(this).addClass("btn-active");
		$(this).removeClass("btn-disabled");

		if($(this).attr("data-form") == "register"){
			$("#registerForm").removeClass("hidden");
			$("#loginForm").addClass("hidden");
			$("#frm-reg-nomap").focus();
		}
		else{
			$("#loginForm").removeClass("hidden");
			$("#registerForm").addClass("hidden");
			$("#frm-log-email").focus();
		}
	});
  
});