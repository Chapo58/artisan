<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePreciosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('precios', function(Blueprint $table) {
            $table->increments('id');
            $table->decimal('precio',10,2);
            $table->timestamp('fecha_hasta')->nullable();
            $table->integer('articulo_id')->unsigned();
            $table->foreign('articulo_id')->references('id')->on('articulos');
            $table->integer('lista_precio_id')->unsigned();
            $table->foreign('lista_precio_id')->references('id')->on('lista_precios');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('precios');
    }
}
