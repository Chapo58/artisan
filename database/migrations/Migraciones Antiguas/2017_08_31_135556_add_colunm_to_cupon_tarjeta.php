<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColunmToCuponTarjeta extends Migration{

    public function up(){
        Schema::table('cupon_tarjetas', function (Blueprint $table) {
            $table->dropForeign('cupon_tarjetas_venta_id_foreign');
            $table->dropColumn('venda_id');
            $table->integer('entidad_relacionada_link_id')->unsigned()->nullable();
            $table->foreign('entidad_relacionada_link_id')->references('id')->on('entidades_relacionadas_link');
        });
    }

    public function down(){
        Schema::table('cupon_tarjetas', function (Blueprint $table) {
            $table->integer('venta_id')->nullable()->unsigned();
            $table->foreign('venta_id')->references('id')->on('ventas');
            $table->dropForeign('cupon_tarjetas_entidad_relacionada_link_id_foreign');
            $table->dropColumn('entidad_relacionada_link_id');
        });
    }
}
