<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPrecioFinalToEcommerceTable extends Migration{

    public function up(){
        Schema::table('ecommerce', function (Blueprint $table) {
            $table->boolean('precio_final')->default(true);
            $table->boolean('mostrar_precios')->default(true);
            $table->boolean('compras_sin_stock')->default(true);
            $table->boolean('articulos_sin_stock')->default(true);
            $table->integer('moneda_id')->unsigned();
            $table->integer('lista_precio_id')->unsigned();
        });
    }

    public function down(){
        Schema::table('ecommerce', function (Blueprint $table) {
            $table->dropColumn('precio_final');
            $table->dropColumn('mostrar_precios');
            $table->dropColumn('compras_sin_stock');
            $table->dropColumn('articulos_sin_stock');
            $table->dropColumn('moneda_id');
            $table->dropColumn('lista_precio_id');
        });
    }
}
