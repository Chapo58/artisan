<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DetalleCobrosNotasDebito extends Migration
{
    public function up()
    {
        Schema::create('detalles_cobros_notas_debito', function(Blueprint $table) {
            $table->increments('id');
            $table->timestamp('fecha');
            $table->integer('nota_debito_id')->unsigned();
            $table->foreign('nota_debito_id')->references('id')->on('notas_debito');
            $table->integer('entidad_id')->nullable()->unsigned();
            $table->integer('forma_cobro');
            $table->decimal('monto',10,2);
            $table->decimal('interes',10,2)->nullable();
            $table->string('descripcion')->nullable();
            $table->integer('estado');
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    public function down()
    {
        Schema::drop('detalles_cobros_notas_debito');
    }
}
