<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnListaPreciosVenta extends Migration
{

    public function up()
    {
        Schema::table('ventas', function (Blueprint $table) {
            $table->integer('lista_precios_id')->unsigned();
            $table->foreign('lista_precios_id')->references('id')->on('lista_precios');
        });
    }

    public function down()
    {
        Schema::table('ventas', function (Blueprint $table) {
            $table->dropForeign('ventas_lista_precios_id_foreign');
            $table->dropColumn('lista_precios_id');
        });
    }
}
