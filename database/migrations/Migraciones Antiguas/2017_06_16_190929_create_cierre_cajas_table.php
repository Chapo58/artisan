<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCierreCajasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cierre_cajas', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('caja_id')->unsigned();
            $table->foreign('caja_id')->references('id')->on('cajas');
            $table->dateTime('fecha_apertura');
            $table->dateTime('fecha_cierre')->nullable();
            $table->integer('usuario_apertura_id')->unsigned()->nullable();
            $table->foreign('usuario_apertura_id')->references('id')->on('users');
            $table->integer('usuario_cierre_id')->unsigned()->nullable();
            $table->foreign('usuario_cierre_id')->references('id')->on('users');
            $table->integer('cuenta_bancaria_id')->unsigned()->nullable();
            $table->foreign('cuenta_bancaria_id')->references('id')->on('cuentas_bancarias');
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cierre_cajas');
    }
}
