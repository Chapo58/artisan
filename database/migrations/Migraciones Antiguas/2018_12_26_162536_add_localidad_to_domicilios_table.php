<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLocalidadToDomiciliosTable extends Migration{

    public function up(){
        Schema::table('domicilios', function (Blueprint $table) {
            $table->dropForeign('domicilios_localidad_id_foreign');
            $table->dropColumn('localidad_id');
            $table->string('localidad');
        });
    }

    public function down(){
        Schema::table('domicilios', function (Blueprint $table) {
            $table->integer('localidad_id')->unsigned();
            $table->foreign('localidad_id')->references('id')->on('localidades');
            $table->dropColumn('localidad');
        });
    }

}
