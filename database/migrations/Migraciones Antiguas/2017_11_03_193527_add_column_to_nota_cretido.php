<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToNotaCretido extends Migration{
    public function up(){
        Schema::table('notas_credito', function (Blueprint $table) {
            $table->integer('venta_id')->unsigned()->nullable()->after('sucursal_id');
            $table->tinyInteger('concepto_nota_credito')->unsigned()->nullable()->after('venta_id');
            $table->foreign('venta_id')->references('id')->on('ventas');
        });
    }

    public function down(){
        Schema::table('notas_credito', function (Blueprint $table) {
            $table->dropForeign('notas_credito_venta_id_foreign');
            $table->dropColumn('venta_id');
            $table->dropColumn('concepto_nota_credito');
        });
    }
}
