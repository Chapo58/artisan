<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnUsuarioToEmpresaTable extends Migration{

    public function up(){
        Schema::table('empresas', function (Blueprint $table) {
            $table->integer('usuario_id')->unsigned()->nullable()->after('direccion_id');
            $table->foreign('usuario_id')->references('id')->on('users');
        });
    }

    public function down(){
        Schema::table('empresas', function (Blueprint $table) {
            $table->dropForeign('empresas_usuario_id_foreign');
            $table->dropColumn('usuario_id');
        });
    }
}
