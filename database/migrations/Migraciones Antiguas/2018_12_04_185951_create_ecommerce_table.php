<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEcommerceTable extends Migration{

    public function up(){
        Schema::create('ecommerce', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('empresa_id')->unsigned();
            $table->foreign('empresa_id')->references('id')->on('empresas');
            $table->string('nombre');
            $table->string('logo_url')->nullable();
            $table->string('telefono');
            $table->string('telefono2')->nullable();
            $table->string('telefono3')->nullable();
            $table->string('email');
            $table->string('email2')->nullable();
            $table->integer('domicilio_id')->unsigned();
            $table->foreign('domicilio_id')->references('id')->on('domicilios');
            $table->string('facebook')->nullable();
            $table->string('twitter')->nullable();
            $table->string('instagram')->nullable();
            $table->string('google_plus')->nullable();
            $table->string('linkedin')->nullable();
            $table->string('skype')->nullable();
            $table->boolean('habilitado')->default(true);
            $table->string('estilo')->nullable();
            $table->string('color')->nullable();
            $table->string('descripcion')->nullable();
            $table->longtext('politica_privacidad')->nullable();
            $table->longtext('terminos_condiciones')->nullable();
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    public function down(){
        Schema::drop('ecommerce');
    }

}
