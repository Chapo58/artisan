<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnCompra extends Migration{
    public function up(){
        Schema::table('compras', function (Blueprint $table) {
            $table->decimal('impuesto_interno',10,2)->nullable()->default(0)->after('impuesto');
            $table->decimal('retencion',10,2)->nullable()->default(0)->after('impuesto_interno');
            $table->decimal('conpecto_no_grabado',10,2)->nullable()->default(0)->after('retencion');
        });
    }

    public function down(){
        Schema::table('compras', function (Blueprint $table) {
            $table->dropColumn('impuesto_interno');
            $table->dropColumn('retencion');
            $table->dropColumn('conpecto_no_grabado');
        });
    }
}
