<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTalleresTable extends Migration{

    public function up(){
        Schema::create('talleres', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('empresa_id')->nullable()->unsigned();
            $table->foreign('empresa_id')->references('id')->on('empresas');
            $table->integer('condicion_iva_id')->unsigned();
            $table->foreign('condicion_iva_id')->references('id')->on('condiciones_iva');
            $table->integer('domicilio_id')->nullable()->unsigned();
            $table->foreign('domicilio_id')->references('id')->on('domicilios');
            $table->string('razon_social');
            $table->string('cuit');
            $table->string('email')->nullable();
            $table->string('telefono')->nullable();
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    public function down(){
        Schema::drop('talleres');
    }
}
