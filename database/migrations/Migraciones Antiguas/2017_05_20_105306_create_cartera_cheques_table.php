<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateChequesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cheques', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('banco_id')->unsigned();
            $table->foreign('banco_id')->references('id')->on('cuentas_bancarias');
            $table->decimal('importe',10,2);
            $table->string('numero');
            $table->date('emision')->nullable();
            $table->date('vencimiento')->nullable();
            $table->string('firmante');
            $table->string('sucursal')->nullable();
            $table->integer('estado')->default(5);
            $table->date('fecha_final')->nullable();
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cheques');
    }
}
