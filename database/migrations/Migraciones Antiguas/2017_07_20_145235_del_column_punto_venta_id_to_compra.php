<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DelColumnPuntoVentaIdToCompra extends Migration{

    public function up(){
        Schema::table('compras', function (Blueprint $table) {
            $table->dropForeign('compras_punto_venta_id_foreign');
            $table->dropColumn('punto_venta_id');
            $table->string('punto_venta',8)->nullable();
        });
    }

    public function down(){
        Schema::table('compras', function (Blueprint $table) {
            $table->integer('punto_venta_id')->unsigned()->nullable();
            $table->foreign('punto_venta_id')->references('id')->on('puntos_de_ventas');
            $table->dropColumn('punto_venta');
        });
    }
}
