<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnCajaIdToCarteraCheques extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('cheques', function (Blueprint $table) {
          $table->integer('caja_id')->unsigned();
          $table->foreign('caja_id')->references('id')->on('cajas');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('cheques', function (Blueprint $table) {
          $table->dropForeign('cheques_caja_id_foreign');
          $table->dropColumn('caja_id');
      });
    }
}
