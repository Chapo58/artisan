<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetalleVencimientosFacturaAutomatica extends Migration{

    public function up(){
        Schema::create('detalles_vencimientos_fa', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('factura_automatica_id')->unsigned();
            $table->foreign('factura_automatica_id')->references('id')->on('facturas_ventas_automaticas');
            $table->tinyInteger('desde')->unsigned();
            $table->tinyInteger('hasta')->unsigned();
            $table->decimal('porcentaje_recargo',10,2);
            $table->timestamps();
        });
    }

    public function down(){
        Schema::drop('detalles_vencimientos_fa');
    }
}
