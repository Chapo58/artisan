<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDestacadoToArticulosTable extends Migration
{
    public function up(){
        Schema::table('articulos', function (Blueprint $table) {
            $table->boolean('lleva_stock')->default(true);
            $table->boolean('destacado')->default(false);
        });
    }

    public function down(){
        Schema::table('articulos', function (Blueprint $table) {
            $table->dropColumn('lleva_stock');
            $table->dropColumn('destacado');
        });
    }
}
