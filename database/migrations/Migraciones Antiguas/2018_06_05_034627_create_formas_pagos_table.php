<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormasPagosTable extends Migration{

    public function up(){
        Schema::create('formas_pagos', function(Blueprint $table) {
            $table->increments('id');
            $table->string('tipo_forma_pago');
            $table->integer('revendedor_id')->unsigned();
            $table->foreign('revendedor_id')->references('id')->on('users');
            $table->text('instrucciones')->nullable();
            $table->boolean('habilitado')->default(true);
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }


    public function down(){
        Schema::drop('formas_pagos');
    }
}
