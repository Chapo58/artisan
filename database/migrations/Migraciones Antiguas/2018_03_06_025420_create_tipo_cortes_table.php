<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTipoCortesTable extends Migration{

    public function up(){
        Schema::create('tipo_cortes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->integer('empresa_id')->unsigned();
                $table->foreign('empresa_id')->references('id')->on('empresas');
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    public function down(){
        Schema::drop('tipo_cortes');
    }
}
