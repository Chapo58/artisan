<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTipoProductosTable extends Migration{

    public function up(){
        Schema::create('tipo_productos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->integer('tipo_corte_id')->unsigned();
                $table->foreign('tipo_corte_id')->references('id')->on('tipo_cortes');
            $table->integer('empresa_id')->unsigned();
                $table->foreign('empresa_id')->references('id')->on('empresas');
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    public function down(){
        Schema::drop('tipo_productos');
    }
}
