<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDetalleOrdenesComprasTable extends Migration{

    public function up(){
        Schema::create('detalles_ordenes_compras', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('orden_compra_id')->unsigned();
            $table->foreign('orden_compra_id')->references('id')->on('ordenes_compras');
            $table->integer('articulo_id')->nullable()->unsigned();
            $table->foreign('articulo_id')->references('id')->on('articulos');
            $table->string('articulo_nombre')->nullable();
            $table->decimal('cantidad',10,2);
            $table->integer('estado');
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    public function down(){
        Schema::drop('detalles_ordenes_compras');
    }
}
