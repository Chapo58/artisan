<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetalleOrdenTable extends Migration{

    public function up(){
        Schema::create('detalles_ordenes', function(Blueprint $table) {
            $table->increments('id');
            $table->string('descripcion', 200)->nullable();
            $table->decimal('monto', 10, 2);
            $table->integer('orden_pago_id')->unsigned();
            $table->foreign('orden_pago_id')->references('id')->on('ordenes_pagos');
            $table->integer('cuenta_corriente_id')->unsigned()->nullable();
            $table->foreign('cuenta_corriente_id')->references('id')->on('cuentas_corrientes');
            $table->integer('nota_credito_id')->unsigned()->nullable();
//            $table->foreign('nota_credito_id')->references('id')->on('notas_creditos');
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    public function down(){
        Schema::drop('detalles_ordenes');
    }
}
