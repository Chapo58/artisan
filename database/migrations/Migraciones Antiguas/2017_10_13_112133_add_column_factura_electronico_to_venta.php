<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnFacturaElectronicoToVenta extends Migration{

    public function up(){
        Schema::table('ventas', function (Blueprint $table) {
            $table->string('cae',20)->nullable();
            $table->timestamp('fecha_vencimiento')->nullable();
        });
        Schema::table('compras', function (Blueprint $table) {
            $table->string('cae',20)->nullable();
            $table->timestamp('fecha_vencimiento')->nullable();
        });
        Schema::table('notas_credito', function (Blueprint $table) {
            $table->string('cae',20)->nullable();
            $table->timestamp('fecha_vencimiento')->nullable();
        });
        Schema::table('notas_debito', function (Blueprint $table) {
            $table->string('cae',20)->nullable();
            $table->timestamp('fecha_vencimiento')->nullable();
        });
    }

    public function down(){
        Schema::table('ventas', function (Blueprint $table) {
            $table->dropColumn('cae');
            $table->dropColumn('fecha_vencimiento');
        });
        Schema::table('Compras', function (Blueprint $table) {
            $table->dropColumn('cae');
            $table->dropColumn('fecha_vencimiento');
        });
        Schema::table('notas_credito', function (Blueprint $table) {
            $table->dropColumn('cae');
            $table->dropColumn('fecha_vencimiento');
        });
        Schema::table('notas_debito', function (Blueprint $table) {
            $table->dropColumn('cae');
            $table->dropColumn('fecha_vencimiento');
        });
    }
}
