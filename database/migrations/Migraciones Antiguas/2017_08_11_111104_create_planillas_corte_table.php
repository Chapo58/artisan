<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePlanillasCorteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('planillas_corte', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('numero');
            $table->string('empresa')->nullable();
            $table->date('fecha');
            $table->string('tipo_corte')->nullable();
            $table->decimal('talle0',10,2)->nullable();
            $table->decimal('talle2',10,2)->nullable();
            $table->decimal('talle4',10,2)->nullable();
            $table->decimal('talle6',10,2)->nullable();
            $table->decimal('talle8',10,2)->nullable();
            $table->decimal('talle10',10,2)->nullable();
            $table->decimal('talle12',10,2)->nullable();
            $table->decimal('talle14',10,2)->nullable();
            $table->decimal('talle16',10,2)->nullable();
            $table->decimal('talleS',10,2)->nullable();
            $table->decimal('talleM',10,2)->nullable();
            $table->decimal('talleL',10,2)->nullable();
            $table->decimal('talleXL',10,2)->nullable();
            $table->decimal('talleXXL',10,2)->nullable();
            $table->decimal('talleXXXL',10,2)->nullable();
            $table->integer('empresa_id')->unsigned();
              $table->foreign('empresa_id')->references('id')->on('empresas');
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('planillas_corte');
    }
}
