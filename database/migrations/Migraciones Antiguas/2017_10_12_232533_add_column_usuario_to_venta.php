<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnUsuarioToVenta extends Migration{

    public function up(){
        Schema::table('ventas', function(Blueprint $table) {
            $table->integer('usuario_id')->unsigned()->nullable();
            $table->foreign('usuario_id')->references('id')->on('users');
        });
    }

    public function down(){
        Schema::table('ventas', function(Blueprint $table) {
            $table->dropForeign('ventas_usuario_id_foreign');
            $table->dropColumn('usuario_id');
        });
    }
}
