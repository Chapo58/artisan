<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrdenPagosTable extends Migration{

    public function up(){
        Schema::create('ordenes_pagos', function(Blueprint $table) {
            $table->increments('id');
            $table->timestamp('fecha');
            $table->string('numero',10);
            $table->decimal('cotizacion',10,2);
            $table->decimal('importe',10,2);
            $table->decimal('total_pago',10,2);
            $table->integer('estado');
            $table->string('descripcion', 200)->nullable();
            $table->integer('empresa_id')->unsigned();
            $table->foreign('empresa_id')->references('id')->on('empresas');
            $table->integer('sucursal_id')->unsigned()->nullable();
            $table->foreign('sucursal_id')->references('id')->on('sucursales');
            $table->integer('proveedor_id')->unsigned();
            $table->foreign('proveedor_id')->references('id')->on('proveedores');
            $table->integer('moneda_id')->unsigned();
            $table->foreign('moneda_id')->references('id')->on('monedas');
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    public function down(){
        Schema::drop('ordenes_pagos');
    }
}
