<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlanesTable extends Migration{

    public function up(){
        Schema::create('planes', function(Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->integer('cantidad_usuarios')->unsigned();
            $table->integer('cantidad_puntos_venta');
            $table->integer('cantidad_sucursales');
            $table->integer('chat');
            $table->decimal('precio',10,2);
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    public function down(){
        Schema::drop('planes');
    }
}
