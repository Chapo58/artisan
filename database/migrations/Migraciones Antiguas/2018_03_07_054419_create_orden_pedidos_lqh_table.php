<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrdenPedidosLqhTable extends Migration{

    public function up(){
        Schema::create('orden_pedidos_lqh', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cliente_id')->unsigned();
                $table->foreign('cliente_id')->references('id')->on('clientes');
            $table->string('numero_opm')->nullable();
            $table->date('fecha_pedido')->nullable();
            $table->date('fecha_entrega')->nullable();
            $table->string('imagen1')->nullable();
            $table->string('imagen2')->nullable();
            $table->string('imagen3')->nullable();
            $table->integer('tipo_corte_id')->unsigned();
                $table->foreign('tipo_corte_id')->references('id')->on('tipo_cortes');
            $table->integer('tipo_tela_id')->unsigned();
                $table->foreign('tipo_tela_id')->references('id')->on('tipos_telas');
            $table->integer('color_id')->unsigned();
                $table->foreign('color_id')->references('id')->on('colores');
            $table->integer('cantidad_estampas')->nullable();
            $table->integer('diseño')->nullable();
            $table->string('observaciones')->nullable();
            $table->integer('talle12')->nullable();
            $table->integer('talle14')->nullable();
            $table->integer('talle16')->nullable();
            $table->integer('talleS')->nullable();
            $table->integer('talleM')->nullable();
            $table->integer('talleL')->nullable();
            $table->integer('talleXL')->nullable();
            $table->integer('talleXXL')->nullable();
            $table->integer('talleXXXL')->nullable();
            $table->integer('total_unidades');
            $table->integer('empresa_id')->unsigned();
                $table->foreign('empresa_id')->references('id')->on('empresas');
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    public function down(){
        Schema::drop('orden_pedidos');
    }
}
