<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnCuentaBancariaIdToMovimientosCaja extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('movimiento_cajas', function (Blueprint $table) {
          $table->integer('cuenta_bancaria_id')->unsigned()->nullable();
          $table->foreign('cuenta_bancaria_id')->references('id')->on('cuentas_bancarias');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('movimiento_cajas', function (Blueprint $table) {
          $table->dropForeign('movimiento_cajas_cuenta_bancaria_id_foreign');
          $table->dropColumn('cuenta_bancaria_id');
      });
    }
}
