<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePresupuestosTable extends Migration{

    public function up(){
        Schema::create('presupuestos', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('empresa_id')->unsigned();
            $table->foreign('empresa_id')->references('id')->on('empresas');
            $table->integer('sucursal_id')->unsigned();
            $table->foreign('sucursal_id')->references('id')->on('sucursales');
            $table->integer('cliente_id')->unsigned();
            $table->foreign('cliente_id')->references('id')->on('clientes');
            $table->integer('moneda_id')->unsigned();
            $table->foreign('moneda_id')->references('id')->on('monedas');
            $table->integer('tipo_comprobante_id')->nullable()->unsigned();
            $table->integer('lista_precios_id')->unsigned();
            $table->foreign('lista_precios_id')->references('id')->on('lista_precios');
            $table->decimal('cotizacion');
            $table->timestamp('fecha');
            $table->timestamp('fecha_vencimiento')->nullable();
            $table->string('numero',20);
            $table->decimal('importe_neto',10,2);
            $table->decimal('importe_iva',10,2);
            $table->decimal('impuesto',10,2);
            $table->decimal('total',10,2);
            $table->integer('estado')->unsigned();
            $table->string('descripcion',255)->nullable();
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    public function down(){
        Schema::drop('presupuestos');
    }
}
