<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePlanTarjetasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plan_tarjetas', function(Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->integer('tarjeta_id')->unsigned();
            $table->foreign('tarjeta_id')->references('id')->on('tarjetas');
            $table->decimal('interes',10,2)->nullable();
            $table->integer('cuotas')->nullable();
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('plan_tarjetas');
    }
}
