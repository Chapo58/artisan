<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetalleRecibo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalles_recibos', function(Blueprint $table) {
            $table->increments('id');
            $table->string('descripcion', 200)->nullable();
            $table->decimal('monto', 10, 2);
            $table->integer('recibo_id')->unsigned();
            $table->foreign('recibo_id')->references('id')->on('recibos');
            $table->integer('cuenta_corriente_id')->unsigned()->nullable();
            $table->foreign('cuenta_corriente_id')->references('id')->on('cuentas_corrientes');
            $table->integer('nota_debito_id')->unsigned()->nullable();
//            $table->foreign('nota_debito_id')->references('id')->on('notas_debitos');
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('detalles_recibos');
    }
}
