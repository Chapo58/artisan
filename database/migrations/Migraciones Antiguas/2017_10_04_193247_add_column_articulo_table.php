<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnArticuloTable extends Migration{

    public function up(){
        Schema::table('articulos', function (Blueprint $table) {
            $table->decimal('ultimo_costo',10,2)->nullable();
        });
    }

    public function down(){
        Schema::table('articulos', function (Blueprint $table) {
            $table->dropColumn('ultimo_costo');
        });
    }
}
