<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTransportesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transportes', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('codigo_interno');
            $table->string('nombre');
            $table->string('direccion')->nullable();
            $table->decimal('codigo_postal')->nullable();
            $table->string('telefono')->nullable();
            $table->string('email')->nullable();
            $table->string('observaciones')->nullable();
            $table->integer('empresa_id')->unsigned();
                $table->foreign('empresa_id')->references('id')->on('empresas');
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('transportes');
    }

}
