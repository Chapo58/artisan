<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMovimientoStocksTable extends Migration
{
    public function up()
    {
        Schema::create('movimientos_stock', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('stock_id')->unsigned();
            $table->foreign('stock_id')->references('id')->on('stocks');
            $table->integer('tipo_movimiento_stock_id')->unsigned();
            $table->foreign('tipo_movimiento_stock_id')->references('id')->on('tipos_movimientos_stock');
            $table->decimal('cantidad',10,2);
            $table->string('descripcion')->nullable();
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    public function down()
    {
        Schema::drop('movimientos_stock');
    }
}
