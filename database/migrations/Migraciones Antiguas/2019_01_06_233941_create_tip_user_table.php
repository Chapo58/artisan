<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTipUserTable extends Migration{

    public function up(){
        Schema::create('tip_user', function(Blueprint $table) {
            $table->integer('tip_id')->unsigned();
            $table->foreign('tip_id')->references('id')->on('tips');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->boolean('entendido');
        });
    }


    public function down(){
        Schema::drop('tip_user');
    }
}
