<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDistribuidoresTable extends Migration{

    public function up(){
        Schema::create('distribuidores', function(Blueprint $table) {
            $table->increments('id');
            $table->string('razon_social');
            $table->integer('usuario_id')->unsigned();
            $table->foreign('usuario_id')->references('id')->on('users');
            $table->integer('condicion_iva_id')->unsigned();
            $table->foreign('condicion_iva_id')->references('id')->on('condiciones_iva');
            $table->integer('domicilio_id')->nullable()->unsigned();
            $table->foreign('domicilio_id')->references('id')->on('domicilios');
            $table->string('cuit')->nullable();
            $table->string('email')->nullable();
            $table->string('telefono')->nullable();
            $table->string('web')->nullable();
            $table->string('logo_url')->nullable();
            $table->string('favicon_url')->nullable();
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }


    public function down(){
        Schema::drop('distribuidores');
    }
}
