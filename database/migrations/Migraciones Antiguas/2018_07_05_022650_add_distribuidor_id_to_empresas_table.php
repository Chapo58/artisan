<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDistribuidorIdToEmpresasTable extends Migration{

    public function up(){
        Schema::table('empresas', function (Blueprint $table) {
            $table->integer('distribuidor_id')->unsigned()->nullable();
            $table->foreign('distribuidor_id')->references('id')->on('distribuidores');
            $table->dropForeign('empresas_usuario_id_foreign');
            $table->dropColumn('usuario_id');
        });

        Schema::table('pagos', function (Blueprint $table) {
            $table->integer('distribuidor_id')->unsigned()->after('empresa_id');
            $table->foreign('distribuidor_id')->references('id')->on('distribuidores');
            $table->dropForeign('pagos_revendedor_id_foreign');
            $table->dropColumn('revendedor_id');
        });

        Schema::table('tawkto', function (Blueprint $table) {
            $table->integer('distribuidor_id')->unsigned()->after('id');
            $table->foreign('distribuidor_id')->references('id')->on('distribuidores');
            $table->dropForeign('tawkto_usuario_id_foreign');
            $table->dropColumn('usuario_id');
        });

        Schema::table('formas_pagos', function (Blueprint $table) {
            $table->integer('distribuidor_id')->unsigned()->after('tipo_forma_pago');
            $table->foreign('distribuidor_id')->references('id')->on('distribuidores');
            $table->dropForeign('formas_pagos_revendedor_id_foreign');
            $table->dropColumn('revendedor_id');
        });
    }


    public function down(){
        Schema::table('empresas', function (Blueprint $table) {
            //
        });
    }
}
