<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserIdToClientesTable extends Migration{

    public function up(){
        Schema::table('clientes', function (Blueprint $table) {
            $table->integer('user_id')->unsigned()->nullable();
        });
    }

    public function down(){
        Schema::table('clientes', function (Blueprint $table) {
            $table->dropColumn('user_id');
        });
    }
}
