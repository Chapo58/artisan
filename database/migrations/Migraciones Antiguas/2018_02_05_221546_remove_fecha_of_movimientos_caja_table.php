<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveFechaOfMovimientosCajaTable extends Migration{
    public function up(){
        Schema::table('movimiento_cajas', function (Blueprint $table) {
            $table->dropColumn('fecha');
        });
    }

    public function down(){
        Schema::table('movimiento_cajas', function (Blueprint $table) {
            $table->dateTime('fecha');
        });
    }
}
