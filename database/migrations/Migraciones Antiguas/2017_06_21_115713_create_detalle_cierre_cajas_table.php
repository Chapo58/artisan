<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDetalleCierreCajasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalle_cierre_cajas', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('cierre_caja_id')->unsigned();
              $table->foreign('cierre_caja_id')->references('id')->on('cierre_cajas');
            $table->integer('forma_de_pago');
            $table->decimal('saldo_inicial',10,2)->default(0);
            $table->decimal('saldo_final',10,2)->default(0);
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('detalle_cierre_cajas');
    }
}
