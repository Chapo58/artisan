<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnStockIdToArticulo extends Migration
{

    public function up()
    {
        Schema::table('articulos', function (Blueprint $table) {
            $table->unsignedInteger('stock_id')->nullable()->unsigned();
            $table->foreign('stock_id')->references('id')->on('stocks');
            $table->unsignedInteger('unidad_id')->unsigned();
            $table->foreign('unidad_id')->references('id')->on('unidades');
            $table->decimal('cantidad', 10, 2);
        });
    }

    public function down()
    {
        Schema::table('articulos', function (Blueprint $table) {
            $table->dropForeign('articulos_stock_id_foreign');
            $table->dropColumn('stock_id');
            $table->dropForeign('articulos_unidad_id_foreign');
            $table->dropColumn('unidad_id');
            $table->dropColumn('cantidad');
        });
    }
}