<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnPuntoVentaToOpciones extends Migration{
    public function up(){
        Schema::table('opciones', function (Blueprint $table) {
            $table->integer('punto_venta_id')->unsigned()->nullable()->after('empresa_id');
            $table->foreign('punto_venta_id')->references('id')->on('puntos_de_ventas');
        });
    }

    public function down(){
        Schema::table('opciones', function (Blueprint $table) {
            $table->dropForeign('opciones_punto_venta_id_foreign');
            $table->dropColumn('punto_venta_id');
        });
    }
}
