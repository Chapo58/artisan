<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLinkMercadopagoToPlanesTable extends Migration{

    public function up(){
        Schema::table('planes', function (Blueprint $table) {
            $table->string('link_mercadopago_mensual')->nullable();
            $table->string('link_mercadopago_anual')->nullable();
        });
    }

    public function down(){
        Schema::table('planes', function (Blueprint $table) {
            $table->dropColumn('link_mercadopago_mensual');
            $table->dropColumn('link_mercadopago_anual');
        });
    }
}
