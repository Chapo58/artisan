<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsMovimientosBancarios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('movimientos_bancarios', function (Blueprint $table) {
            $table->integer('compra_id')->unsigned()->nullable();
            $table->foreign('compra_id')->references('id')->on('compras');
            $table->integer('venta_id')->unsigned()->nullable();
            $table->foreign('venta_id')->references('id')->on('ventas');
            $table->integer('cheque_id')->unsigned()->nullable();
            $table->foreign('cheque_id')->references('id')->on('cheques');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('movimientos_bancarios', function (Blueprint $table) {
            $table->dropForeign('movimientos_bancarios_compra_id_foreign');
            $table->dropColumn('compra_id');
            $table->dropForeign('movimientos_bancarios_venta_id_foreign');
            $table->dropColumn('venta_id');
            $table->dropForeign('movimientos_bancarios_cheque_id_foreign');
            $table->dropColumn('cheque_id');
        });
    }
}
