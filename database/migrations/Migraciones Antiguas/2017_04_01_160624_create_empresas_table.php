<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEmpresasTable extends Migration{

    public function up(){
        Schema::create('empresas', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('condicion_iva_id')->nullable()->unsigned();
            $table->foreign('condicion_iva_id')->references('id')->on('condiciones_iva');
            $table->integer('domicilio_id')->nullable()->unsigned();
            $table->foreign('domicilio_id')->references('id')->on('domicilios');
            $table->string('razon_social');
            $table->string('nombre');
            $table->string('cuit');
            $table->string('email');
            $table->string('contacto')->nullable();
            $table->string('telefono');
            $table->string('ingresos_brutos')->nullable();
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    public function down(){
        Schema::drop('empresas');
    }
}
