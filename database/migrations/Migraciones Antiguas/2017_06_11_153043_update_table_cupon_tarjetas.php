<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableCuponTarjetas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cupon_tarjetas', function (Blueprint $table) {
            $table->dropColumn('comprobante_id');
            $table->integer('venta_id')->nullable()->unsigned()->after('plan_tarjeta_id');
            $table->foreign('venta_id')->references('id')->on('ventas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cupon_tarjetas', function (Blueprint $table) {
            $table->dropColumn('venta_id');
            $table->integer('comprobante_id')->nullable()->unsigned()->after('plan_tarjeta_id');
            $table->dropForeign('cupon_tarjetas_venta_id_foreign');
        });
    }
}
