<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrdenesVentasTable extends Migration{

    public function up(){
        Schema::create('ordenes_ventas', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('empresa_id')->unsigned();
            $table->foreign('empresa_id')->references('id')->on('empresas');
            $table->integer('cliente_id')->unsigned();
            $table->integer('forma_pago_id')->unsigned();
            $table->integer('venta_id')->nullable()->unsigned();
            $table->integer('moneda_id')->unsigned();
            $table->integer('lista_precio_id')->unsigned();
            $table->decimal('total',10,2);
            $table->string('numero');
            $table->string('mensaje')->nullable();
            $table->integer('estado');
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    public function down(){
        Schema::drop('ordenes_ventas');
    }
}
