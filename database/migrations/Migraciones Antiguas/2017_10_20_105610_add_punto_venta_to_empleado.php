<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPuntoVentaToEmpleado extends Migration{

    public function up(){
        Schema::table('empleados', function (Blueprint $table) {
            $table->integer('punto_venta_id')->unsigned()->nullable();
            $table->foreign('punto_venta_id')->references('id')->on('puntos_de_ventas');
        });
    }

    public function down(){
        Schema::table('empleados', function (Blueprint $table) {
            $table->dropForeign('empleados_punto_venta_id_foreign');
            $table->dropColumn('punto_venta_id');
        });
    }
}
