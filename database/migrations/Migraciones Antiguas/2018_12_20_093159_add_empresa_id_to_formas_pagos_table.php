<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEmpresaIdToFormasPagosTable extends Migration{

    public function up(){
        Schema::table('formas_pagos', function (Blueprint $table) {
            $table->integer('empresa_id')->unsigned()->nulleable()->after('distribuidor_id');
        });
    }

    public function down(){
        Schema::table('formas_pagos', function (Blueprint $table) {
        });
    }
}
