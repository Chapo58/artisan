<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddImagenUrlToMarcasTable extends Migration{
    public function up(){
        Schema::table('marcas', function (Blueprint $table) {
            $table->string('imagen_url')->nullable();
        });
    }

    public function down(){
        Schema::table('marcas', function (Blueprint $table) {
            $table->dropColumn('imagen_url');
        });
    }
}
