<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnUrlimgToEmpresa extends Migration{

    public function up(){
        Schema::table('empresas', function (Blueprint $table) {
            $table->string('imagen_url', 254)->nullable()->after('nombre');
        });
    }

    public function down(){
        Schema::table('empresas', function (Blueprint $table) {
            $table->dropColumn('imagen_url');
        });
    }
}
