<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnImpuestoInternoToDetalles extends Migration{
    public function up(){
        Schema::table('detalles_ventas', function (Blueprint $table) {
            $table->decimal('impuesto_interno',10,2)->nullable()->default(0)->after('porcentaje_iva');
        });
        Schema::table('detalles_notas_credito', function (Blueprint $table) {
            $table->decimal('impuesto_interno',10,2)->nullable()->default(0)->after('porcentaje_iva');
        });
        Schema::table('detalles_notas_debito', function (Blueprint $table) {
            $table->decimal('impuesto_interno',10,2)->nullable()->default(0)->after('porcentaje_iva');
        });
    }

    public function down(){
        Schema::table('detalles_ventas', function (Blueprint $table) {
            $table->dropColumn('impuesto_interno');
        });
        Schema::table('detalles_notas_credito', function (Blueprint $table) {
            $table->dropColumn('impuesto_interno');
        });
        Schema::table('detalles_notas_debito', function (Blueprint $table) {
            $table->dropColumn('impuesto_interno');
        });
    }
}
