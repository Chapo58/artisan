<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUsuarioFiscalToPuntosDeVentasTable extends Migration{

    public function up(){
        Schema::table('puntos_de_ventas', function (Blueprint $table) {
            $table->integer('usuario_id_fiscal')->unsigned()->nullable()->after('baudios_fiscal');
            $table->foreign('usuario_id_fiscal')->references('id')->on('users');
        });
    }


    public function down(){
        Schema::table('puntos_de_ventas', function (Blueprint $table) {
            $table->dropForeign('puntos_de_ventas_usuario_id_fiscal_foreign');
            $table->dropColumn('usuario_id_fiscal');
        });
    }
}
