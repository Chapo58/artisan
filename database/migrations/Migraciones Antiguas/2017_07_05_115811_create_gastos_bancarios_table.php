<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGastosBancariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gastos_bancarios', function(Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->decimal('porcentaje',10,2)->nullable();
            $table->decimal('importe',10,2)->nullable();
            $table->integer('empresa_id')->unsigned();
              $table->foreign('empresa_id')->references('id')->on('empresas');
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('gasto_bancarios');
    }
}
