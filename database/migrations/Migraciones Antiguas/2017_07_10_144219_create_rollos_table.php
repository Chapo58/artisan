<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRollosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rollos', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('tipo_tela_id')->unsigned();
            $table->foreign('tipo_tela_id')->references('id')->on('tipos_telas');
            $table->integer('color_id')->unsigned();
            $table->foreign('color_id')->references('id')->on('colores');
            $table->string('partida')->nullable();
            $table->decimal('peso',10,2)->nullable();
            $table->string('tintoreria')->nullable();
            $table->string('codigo');
            $table->decimal('costo',10,2)->nullable();
            $table->integer('moneda_id')->unsigned();
            $table->foreign('moneda_id')->references('id')->on('monedas');
            $table->integer('empresa_id')->unsigned();
            $table->foreign('empresa_id')->references('id')->on('empresas');
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rollos');
    }
}
