<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnActAfipToArticulo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('articulos', function (Blueprint $table) {
            $table->integer('actividad_afip_id')->unsigned()->nullable();
            $table->foreign('actividad_afip_id')->references('id')->on('actividades_afip');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('articulos', function (Blueprint $table) {
            $table->dropForeign('articulos_actividad_afip_id_foreign');
            $table->dropColumn('actividad_afip_id');
        });
    }
}
