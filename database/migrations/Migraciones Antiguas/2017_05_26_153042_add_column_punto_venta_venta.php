<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnPuntoVentaVenta extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ventas', function (Blueprint $table) {
            $table->integer('punto_venta_id')->unsigned();
            $table->foreign('punto_venta_id')->references('id')->on('puntos_de_ventas');
            $table->integer('tipo_comprobante_id')->unsigned();
            //TODO agregar relacion
//            $table->foreign('tipo_comprobante_id')->references('id')->on('puntos_de_ventas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ventas', function (Blueprint $table) {
            $table->dropForeign('ventas_punto_venta_id_foreign');
            $table->dropColumn('punto_venta_id');
            //$table->dropForeign('tipo_comprobante_id');
            $table->dropColumn('tipo_comprobante_id');
        });
    }
}
