<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnEmpresaToChequesPropios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cheques_propios', function(Blueprint $table) {
            $table->integer('chequera_id')->unsigned();
            $table->foreign('chequera_id')->references('id')->on('chequeras');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cheques_propios', function(Blueprint $table) {
            $table->dropForeign('cheques_propios_chequera_id_foreign');
            $table->dropColumn('chequera_id');
        });
    }
}
