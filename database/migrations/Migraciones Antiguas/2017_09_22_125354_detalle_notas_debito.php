<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DetalleNotasDebito extends Migration
{
    public function up()
    {
        Schema::create('detalles_notas_debito', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('nota_debito_id')->unsigned();
            $table->foreign('nota_debito_id')->references('id')->on('notas_debito');
            $table->integer('articulo_id')->nullable()->unsigned();
            $table->foreign('articulo_id')->references('id')->on('articulos');
            $table->decimal('precio',10,2);
            $table->integer('porcentaje_iva')->nullable();
            $table->string('descripcion')->nullable();
            $table->decimal('cantidad',10,2);
            $table->integer('estado');
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    public function down()
    {
        Schema::drop('detalles_notas_debito');
    }
}
