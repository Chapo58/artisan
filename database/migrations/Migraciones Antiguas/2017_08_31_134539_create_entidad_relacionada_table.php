<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEntidadRelacionadaTable extends Migration{

    public function up(){
        Schema::create('entidades_relacionadas_link', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('entidad_id')->unsigned();
            $table->string('entidad_clase',100);
            $table->string('url');
            $table->string('presentacion',200)->nullable();
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    public function down(){
        Schema::drop('entidades_relacionadas_link');
    }
}
