<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToCompra extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('compras', function (Blueprint $table) {
            $table->integer('punto_venta_id')->unsigned();
            $table->foreign('punto_venta_id')->references('id')->on('puntos_de_ventas');
            $table->integer('sucursal_id')->unsigned();
            $table->foreign('sucursal_id')->references('id')->on('sucursales');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('compras', function (Blueprint $table) {
            $table->dropForeign('compras_punto_venta_id_foreign');
            $table->dropColumn('punto_venta_id');
            $table->dropForeign('compras_tipo_comprobante_id_foreign');
            $table->dropColumn('tipo_comprobante_id');
        });
    }
}
