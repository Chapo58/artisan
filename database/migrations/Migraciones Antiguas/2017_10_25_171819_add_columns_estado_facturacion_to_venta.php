<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsEstadoFacturacionToVenta extends Migration
{

    public function up(){
        Schema::table('ventas', function (Blueprint $table) {
            $table->integer('estado_facturacion')->unsigned()->nullable()->after('estado');
        });
        Schema::table('notas_credito', function (Blueprint $table) {
            $table->integer('estado_facturacion')->unsigned()->nullable()->after('estado');
        });
        Schema::table('notas_debito', function (Blueprint $table) {
            $table->integer('estado_facturacion')->unsigned()->nullable()->after('estado');
        });
    }

    public function down(){
        Schema::table('ventas', function (Blueprint $table) {
            $table->dropColumn('estado_facturacion');
        });
        Schema::table('notas_credito', function (Blueprint $table) {
            $table->dropColumn('estado_facturacion');
        });
        Schema::table('notas_debito', function (Blueprint $table) {
            $table->dropColumn('estado_facturacion');
        });
    }
}
