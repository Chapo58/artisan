<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDetalleVentasTable extends Migration
{

    public function up()
    {
        Schema::create('detalles_ventas', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('venta_id')->unsigned();
            $table->foreign('venta_id')->references('id')->on('ventas');
            $table->integer('articulo_id')->nullable()->unsigned();
            $table->foreign('articulo_id')->references('id')->on('articulos');
            $table->decimal('precio',10,2);
            $table->decimal('importe_iva',10,2)->nullable();
            $table->string('descripcion')->nullable();
            $table->decimal('cantidad',10,2);
            $table->integer('estado');
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    public function down()
    {
        Schema::drop('detalles_ventas');
    }
}
