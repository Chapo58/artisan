<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnInicioActividadToEmpresas extends Migration{
    public function up(){
        Schema::table('empresas', function (Blueprint $table) {
            $table->timestamp('inicio_actividad')->nullable()->after('ingresos_brutos');
        });
    }

    public function down(){
        Schema::table('empresas', function (Blueprint $table) {
            $table->dropColumn('inicio_actividad');
        });
    }
}
