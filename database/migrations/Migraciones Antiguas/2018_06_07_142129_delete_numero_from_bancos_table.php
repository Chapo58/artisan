<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeleteNumeroFromBancosTable extends Migration{

    public function up(){
        Schema::table('bancos', function (Blueprint $table) {
            $table->dropColumn('numero');
        });
    }

    public function down(){
        Schema::table('bancos', function (Blueprint $table) {
            $table->integer('numero');
        });
    }

}
