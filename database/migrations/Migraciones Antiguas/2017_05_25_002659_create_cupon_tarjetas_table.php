<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCuponTarjetasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cupon_tarjetas', function(Blueprint $table) {
            $table->increments('id');
            $table->date('fecha');
            $table->integer('tarjeta_id')->unsigned();
            $table->foreign('tarjeta_id')->references('id')->on('tarjetas');
            $table->integer('plan_tarjeta_id')->nullable()->unsigned();
            $table->foreign('plan_tarjeta_id')->references('id')->on('plan_tarjetas');
            $table->integer('comprobante_id')->nullable()->unsigned();
            $table->integer('cuenta_bancaria_id')->nullable()->unsigned();
            $table->foreign('cuenta_bancaria_id')->references('id')->on('cuentas_bancarias');
            $table->string('lote');
            $table->string('cupon');
            $table->string('titular')->nullable();
            $table->decimal('importe',10,2);
            $table->string('autorizacion');
            $table->date('fecha_acreditacion')->nullable();
            $table->integer('estado')->default(5);
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cupon_tarjetas');
    }
}
