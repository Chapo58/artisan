<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDetalleOrdenesVentasTable extends Migration{

    public function up(){
        Schema::create('detalles_ordenes_ventas', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('orden_venta_id')->unsigned();
            $table->integer('articulo_id')->nullable()->unsigned();
            $table->decimal('precio_neto',10,2);
            $table->decimal('precio_iva',10,2);
            $table->decimal('porcentaje_iva',10,2);
            $table->decimal('importe_impuesto_interno',10,2);
            $table->decimal('cantidad',10,2);
            $table->integer('estado');
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    public function down(){
        Schema::drop('detalles_ordenes_ventas');
    }
}
