<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsSaldosInicialesYFinalesToCierreCaja extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('cierre_cajas', function (Blueprint $table) {
          $table->decimal('saldo_apertura',10,2)->default(0);
          $table->decimal('saldo_cierre',10,2)->default(0)->nullable();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('cierre_cajas', function (Blueprint $table) {
          $table->dropColumn('saldo_apertura');
          $table->dropColumn('saldo_cierre');
      });
    }
}
