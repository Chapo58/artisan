<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPlanesIdToEmpresasTable extends Migration{

    public function up(){
        Schema::table('empresas', function (Blueprint $table) {
            $table->date('fecha_vencimiento')->nullable();
            $table->integer('plan_id')->unsigned()->nullable();
            $table->foreign('plan_id')->references('id')->on('planes');
        });
    }

    public function down(){
        Schema::table('empresas', function (Blueprint $table) {
            $table->dropColumn('fecha_vencimiento');
            $table->dropForeign('empresas_plan_id_foreign');
            $table->dropColumn('plan_id');
        });
    }
}
