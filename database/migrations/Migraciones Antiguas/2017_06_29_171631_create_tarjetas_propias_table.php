<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTarjetasPropiasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tarjetas_propias', function(Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('titular')->nullable();
            $table->dateTime('fecha_vencimiento')->nullable();
            $table->string('numero')->nullable();
            $table->decimal('limite',10,2)->unsigned()->nullable();
            $table->integer('banco_id')->unsigned();
              $table->foreign('banco_id')->references('id')->on('bancos');
            $table->integer('tipo')->unsigned();
            $table->integer('sucursal_id')->unsigned()->nullable();
              $table->foreign('sucursal_id')->references('id')->on('sucursales');
            $table->integer('empresa_id')->unsigned();
              $table->foreign('empresa_id')->references('id')->on('empresas');
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tarjetas_propias');
    }
}
