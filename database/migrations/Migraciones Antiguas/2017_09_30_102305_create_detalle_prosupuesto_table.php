<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetalleProsupuestoTable extends Migration{

    public function up(){
        Schema::create('detalles_presupuestos', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('presupuesto_id')->unsigned();
            $table->foreign('presupuesto_id')->references('id')->on('presupuestos');
            $table->integer('articulo_id')->nullable()->unsigned();
            $table->foreign('articulo_id')->references('id')->on('articulos');
            $table->decimal('precio',10,2);
            $table->tinyInteger('porcentaje_iva')->default(0);
            $table->string('descripcion',255)->nullable();
            $table->decimal('cantidad',10,2);
            $table->tinyInteger('estado');
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    public function down(){
        Schema::drop('detalles_presupuestos');
    }
}
