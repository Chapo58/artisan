<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDeletedAtToRubro extends Migration{

    public function up(){
        Schema::table('rubros', function (Blueprint $table) {
            $table->timestamp('deleted_at')->nullable();
        });
        Schema::table('subrubros', function (Blueprint $table) {
            $table->timestamp('deleted_at')->nullable();
        });
        Schema::table('marcas', function (Blueprint $table) {
            $table->timestamp('deleted_at')->nullable();
        });
    }

    public function down(){
        Schema::table('rubros', function (Blueprint $table) {
            $table->dropColumn('deleted_at');
        });
        Schema::table('subrubros', function (Blueprint $table) {
            $table->dropColumn('deleted_at');
        });
        Schema::table('marcas', function (Blueprint $table) {
            $table->dropColumn('deleted_at');
        });
    }
}
