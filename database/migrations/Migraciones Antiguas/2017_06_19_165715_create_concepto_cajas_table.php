<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateConceptoCajasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('concepto_cajas', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('cuenta_id')->unsigned()->nullable();
            $table->string('nombre');
            $table->integer('empresa_id')->unsigned();
              $table->foreign('empresa_id')->references('id')->on('empresas');
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('concepto_cajas');
    }
}
