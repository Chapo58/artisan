<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSuscripcionesTable extends Migration{

    public function up(){
        Schema::create('suscripciones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email');
            $table->integer('empresa_id')->unsigned();
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    public function down(){
        Schema::drop('suscripciones');
    }
}
