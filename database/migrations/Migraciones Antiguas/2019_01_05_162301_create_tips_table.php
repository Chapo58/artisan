<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTipsTable extends Migration{

    public function up(){
        Schema::create('tips', function (Blueprint $table) {
            $table->increments('id');
            $table->text('texto');
            $table->string('ruta');
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    public function down(){
        Schema::drop('tips');
    }
}
