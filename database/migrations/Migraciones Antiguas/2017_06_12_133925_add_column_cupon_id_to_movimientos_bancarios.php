<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnCuponIdToMovimientosBancarios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('movimientos_bancarios', function (Blueprint $table) {
            $table->integer('cupon_id')->unsigned()->nullable();
            $table->foreign('cupon_id')->references('id')->on('cupon_tarjetas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('movimientos_bancarios', function (Blueprint $table) {
            $table->dropForeign('movimientos_bancarios_cupon_tarjetas_id_foreign');
            $table->dropColumn('cupon_id');
        });
    }
}
