<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMovimientosBancariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movimientos_bancarios', function(Blueprint $table) {
            $table->increments('id');
            $table->decimal('importe',10, 2);
            $table->string('numero')->nullable();
            $table->integer('cuenta_bancaria_origen_id')->nullable()->unsigned();
            $table->foreign('cuenta_bancaria_origen_id')->references('id')->on('cuentas_bancarias');
            $table->integer('cuenta_bancaria_destino_id')->unsigned();
            $table->foreign('cuenta_bancaria_destino_id')->references('id')->on('cuentas_bancarias');
            $table->integer('moneda_id')->unsigned();
            $table->foreign('moneda_id')->references('id')->on('monedas');
            $table->integer('tipo')->nullable();
            $table->date('fecha');
            $table->string('comentario')->nullable();
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('movimiento_bancarios');
    }
}
