<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePaginasTable extends Migration{

    public function up(){
        Schema::create('paginas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('empresa_id')->unsigned();
            $table->foreign('empresa_id')->references('id')->on('empresas');
            $table->string('titulo');
            $table->string('subtitulo')->nullable();
            $table->string('imagen_url')->nullable();
            $table->string('contenido');
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    public function down(){
        Schema::drop('paginas');
    }
}
