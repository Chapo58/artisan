<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetalleCobroVentaTable extends Migration
{
    public function up()
    {
        Schema::create('detalles_cobro_ventas', function(Blueprint $table) {
            $table->increments('id');
            $table->timestamp('fecha');
            $table->integer('venta_id')->unsigned();
            $table->foreign('venta_id')->references('id')->on('ventas');
            $table->integer('entidad_id')->nullable()->unsigned();
            $table->integer('forma_cobro');
            $table->decimal('monto',10,2);
            $table->string('descripcion')->nullable();
            $table->integer('estado');
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    public function down()
    {
        Schema::drop('detalles_cobro_ventas');
    }
}
