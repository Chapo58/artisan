<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnImpuestoToArticulo extends Migration{
    public function up(){
        Schema::table('articulos', function (Blueprint $table) {
            $table->decimal('impuesto',10,2)->nullable()->after('porcentaje_iva');
        });
    }

    public function down(){
        Schema::table('articulos', function (Blueprint $table) {
            $table->dropColumn('impuesto');
        });
    }
}
