<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTawktoToEcommerceTable extends Migration{

    public function up(){
        Schema::table('ecommerce', function (Blueprint $table) {
            $table->text('tawkto_codigo')->nullable();
            $table->boolean('tawkto_habilitado')->default(true);
        });
    }

    public function down(){
        Schema::table('ecommerce', function (Blueprint $table) {
            $table->dropColumn('tawkto_codigo');
            $table->dropColumn('tawkto_habilitado');
        });
    }
}
