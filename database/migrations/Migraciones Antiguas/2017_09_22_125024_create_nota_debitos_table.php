<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNotaDebitosTable extends Migration{

    public function up(){
        Schema::create('notas_debito', function(Blueprint $table) {
            $table->increments('id');
            $table->timestamp('fecha');
            $table->string('numero', 10);
            $table->decimal('importe_neto',10,2);
            $table->decimal('importe_iva',10,2);
            $table->decimal('impuesto',10,2);
            $table->decimal('total',10,2);
            $table->decimal('total_cobro',10,2);
            $table->decimal('cotizacion',10,2);
            $table->integer('estado');
            $table->string('descripcion', 255)->nullable();
            $table->integer('empresa_id')->unsigned();
            $table->foreign('empresa_id')->references('id')->on('empresas');
            $table->integer('sucursal_id')->unsigned();
            $table->foreign('sucursal_id')->references('id')->on('sucursales');
            $table->integer('cliente_id')->unsigned();
            $table->foreign('cliente_id')->references('id')->on('clientes');
            $table->integer('moneda_id')->unsigned();
            $table->foreign('moneda_id')->references('id')->on('monedas');
            $table->integer('punto_venta_id')->unsigned();
            $table->foreign('punto_venta_id')->references('id')->on('puntos_de_ventas');
            $table->integer('tipo_comprobante_id');
            $table->integer('lista_precios_id')->unsigned();
            $table->foreign('lista_precios_id')->references('id')->on('lista_precios');
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    public function down(){
        Schema::drop('notas_debito');
    }
}
