<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFacturaAutomaticaToVenta extends Migration{

    public function up(){
        Schema::table('ventas', function (Blueprint $table) {
            $table->integer('factura_automatica_id')->unsigned()->nullable();
            $table->foreign('factura_automatica_id')->references('id')->on('facturas_ventas_automaticas');
        });
    }

    public function down(){
        Schema::table('ventas', function (Blueprint $table) {
            $table->dropForeign('ventas_factura_automatica_id_foreign');
            $table->dropColumn('factura_automatica_id');
        });
    }
}
