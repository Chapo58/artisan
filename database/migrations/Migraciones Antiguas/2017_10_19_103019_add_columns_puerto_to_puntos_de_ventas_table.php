<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsPuertoToPuntosDeVentasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('puntos_de_ventas', function (Blueprint $table) {
            $table->string('host_fiscal')->nullable();
            $table->integer('puerto_fiscal')->nullable();
            $table->integer('modelo_impresora_fiscal')->unsigned()->nullable();
            $table->integer('baudios_fiscal')->nullable();
            $table->decimal('anchura_papel_comandera',10,2)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('puntos_de_ventas', function (Blueprint $table) {
            $table->dropColumn('host_fiscal');
            $table->dropColumn('puerto_fiscal');
            $table->dropColumn('modelo_impresora_fiscal');
            $table->dropColumn('baudios_fiscal');
            $table->dropColumn('anchura_papel_comandera');
        });
    }
}
