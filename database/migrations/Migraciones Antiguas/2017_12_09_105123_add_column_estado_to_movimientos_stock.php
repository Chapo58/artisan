<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnEstadoToMovimientosStock extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        Schema::table('movimientos_stock', function (Blueprint $table) {
            $table->integer('sucursal_origen_id')->unsigned()->nullable();
            $table->foreign('sucursal_origen_id')->references('id')->on('sucursales');
            $table->integer('sucursal_destino_id')->unsigned()->nullable();
            $table->foreign('sucursal_destino_id')->references('id')->on('sucursales');
            $table->integer('estado')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        Schema::table('movimientos_stock', function (Blueprint $table) {
            $table->dropForeign('movimientos_stock_sucursal_origen_id_foreign');
            $table->dropColumn('sucursal_origen_id');
            $table->dropForeign('movimientos_stock_sucursal_destino_id_foreign');
            $table->dropColumn('sucursal_destino_id');
            $table->dropColumn('estado');
        });
    }
}
