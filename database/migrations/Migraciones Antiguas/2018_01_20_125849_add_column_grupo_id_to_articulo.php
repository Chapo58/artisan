<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnGrupoIdToArticulo extends Migration{

    public function up(){
        Schema::table('articulos', function (Blueprint $table) {
            $table->integer('grupo_articulos_id')->unsigned()->nullable();
            $table->foreign('grupo_articulos_id')->references('id')->on('grupos_articulos');
        });
    }

    public function down(){
        Schema::table('articulos', function (Blueprint $table) {
            $table->dropForeign('articulos_grupo_articulos_id_foreign');
            $table->dropColumn('grupo_articulos_id');
        });
    }
}
