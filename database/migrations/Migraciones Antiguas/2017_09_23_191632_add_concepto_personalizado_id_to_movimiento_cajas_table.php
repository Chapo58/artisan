<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddConceptoPersonalizadoIdToMovimientoCajasTable extends Migration{

    public function up(){
        Schema::table('movimiento_cajas', function (Blueprint $table) {
            $table->integer('concepto_personalizado_id')->unsigned()->nullable()->after('concepto_de_caja');
            $table->foreign('concepto_personalizado_id')->references('id')->on('concepto_cajas');
        });
    }

    public function down(){
        Schema::table('movimiento_cajas', function (Blueprint $table) {
            $table->dropForeign('concepto_cajas_concepto_personalizado_id_foreign');
            $table->dropColumn('concepto_personalizado_id');
        });
    }
}
