<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnEmpresaIdToMovBancario extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('movimientos_bancarios', function (Blueprint $table) {
            $table->integer('empresa_id')->unsigned();
            $table->foreign('empresa_id')->references('id')->on('empresas');
            $table->dropForeign('movimientos_bancarios_compra_id_foreign');
            $table->dropColumn('compra_id');
            $table->dropForeign('movimientos_bancarios_venta_id_foreign');
            $table->dropColumn('venta_id');
            $table->dropForeign('movimientos_bancarios_cheque_id_foreign');
            $table->dropColumn('cheque_id');
            $table->dropForeign('movimientos_bancarios_cupon_id_foreign');
            $table->dropColumn('cupon_id');
            $table->integer('entidad_id')->unsigned()->nullable();
            $table->string('entidad_clase')->nullable();
            $table->string('url')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('movimientos_bancarios', function (Blueprint $table) {
            $table->dropForeign('movimientos_bancarios_empresa_id_foreign');
            $table->dropColumn('empresa_id');
            $table->integer('compra_id')->unsigned()->nullable();
            $table->foreign('compra_id')->references('id')->on('empresas');
            $table->integer('venta_id')->unsigned()->nullable();
            $table->foreign('venta_id')->references('id')->on('empresas');
            $table->integer('cheque_id')->unsigned()->nullable();
            $table->foreign('cheque_id')->references('id')->on('empresas');
            $table->integer('cupon_id')->unsigned()->nullable();
            $table->foreign('cupon_id')->references('id')->on('empresas');
            $table->dropColumn('entidad_id');
            $table->dropColumn('entidad_clase');
            $table->dropColumn('url');
        });
    }
}
