<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetallesOrdenPedidosLqhTable extends Migration{

    public function up(){
        Schema::create('detalles_orden_pedidos_lqh', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('orden_pedido_id')->unsigned();
                $table->foreign('orden_pedido_id')->references('id')->on('orden_pedidos_lqh');
            $table->integer('tipo_producto_id')->unsigned();
                $table->foreign('tipo_producto_id')->references('id')->on('tipo_productos');
            $table->string('detalle')->nullable();
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    public function down(){
        Schema::dropIfExists('detalles_orden_pedidos_lqh');
    }
}
