<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetallePagoCompra extends Migration{

    public function up(){
        Schema::create('detalles_pago_compras', function(Blueprint $table) {
            $table->increments('id');
            $table->timestamp('fecha');
            $table->integer('compra_id')->unsigned();
            $table->foreign('compra_id')->references('id')->on('compras');
            $table->integer('entidad_id')->nullable()->unsigned();
            $table->integer('forma_pago');
            $table->decimal('monto',10,2);
            $table->decimal('interes',10,2)->nullable();
            $table->string('descripcion')->nullable();
            $table->integer('estado');
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    public function down(){
        Schema::drop('detalles_pago_compras');
    }
}
