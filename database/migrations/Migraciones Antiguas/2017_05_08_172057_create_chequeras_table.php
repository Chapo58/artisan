<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateChequerasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chequeras', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('cuenta_bancaria_id')->unsigned();
            $table->foreign('cuenta_bancaria_id')->references('id')->on('cuentas_bancarias');
            $table->string('desde');
            $table->string('hasta');
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('chequeras');
    }
}
