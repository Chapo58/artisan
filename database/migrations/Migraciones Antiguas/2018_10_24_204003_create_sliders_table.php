<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSlidersTable extends Migration{

    public function up(){
        Schema::create('sliders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('empresa_id')->unsigned();
            $table->foreign('empresa_id')->references('id')->on('empresas');
            $table->string('titulo');
            $table->string('texto')->nullable();
            $table->string('imagen_url');
            $table->string('link')->nullable();
            $table->integer('orden')->nullable();
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    public function down(){
        Schema::drop('sliders');
    }
}
