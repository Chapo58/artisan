<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMovimientoCajasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movimiento_cajas', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('empresa_id')->unsigned();
			$table->foreign('empresa_id')->references('id')->on('empresas');
            $table->integer('usuario_id')->unsigned();
			$table->foreign('usuario_id')->references('id')->on('users');
            $table->integer('caja_id')->unsigned();
			$table->foreign('caja_id')->references('id')->on('cajas');
            $table->integer('moneda_id')->unsigned();
			$table->foreign('moneda_id')->references('id')->on('monedas');
            $table->dateTime('fecha');
            $table->string('tipo')->nullable();
            $table->string('forma_de_pago');
            $table->decimal('entrada',10,2)->default(0);
            $table->decimal('salida',10,2)->default(0);
            $table->integer('entidad_id')->nullable();
            $table->string('entidad_clase')->nullable();
            $table->string('url')->nullable();
            $table->integer('concepto_de_caja')->unsigned()->nullable();
            $table->string('detalle')->nullable();
            $table->string('comentario')->nullable();
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('movimiento_cajas');
    }
}
