<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnImporteImpuestoInternoToVenta extends Migration{
    public function up(){
        Schema::table('ventas', function (Blueprint $table) {
            $table->decimal('importe_impuesto_interno',10,2)->nullable()->default(0)->after('impuesto');
        });
        Schema::table('notas_credito', function (Blueprint $table) {
            $table->decimal('importe_impuesto_interno',10,2)->nullable()->default(0)->after('impuesto');
        });
        Schema::table('notas_debito', function (Blueprint $table) {
            $table->decimal('importe_impuesto_interno',10,2)->nullable()->default(0)->after('impuesto');
        });
    }

    public function down(){
        Schema::table('ventas', function (Blueprint $table) {
            $table->dropColumn('importe_impuesto_interno');
        });
        Schema::table('notas_credito', function (Blueprint $table) {
            $table->dropColumn('importe_impuesto_interno');
        });
        Schema::table('notas_debito', function (Blueprint $table) {
            $table->dropColumn('importe_impuesto_interno');
        });
    }
}
