<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnModelo2ToArticulo extends Migration{
    public function up(){
        Schema::table('articulos', function (Blueprint $table) {
            $table->string('modelo_secundario', 100)->nullable()->after('modelo');
        });
    }

    public function down(){
        Schema::table('articulos', function (Blueprint $table) {
            $table->dropColumn('modelo_secundario');
        });
    }
}
