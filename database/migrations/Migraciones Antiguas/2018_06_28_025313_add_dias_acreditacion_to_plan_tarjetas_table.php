<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDiasAcreditacionToPlanTarjetasTable extends Migration{

    public function up(){
        Schema::table('plan_tarjetas', function (Blueprint $table) {
            $table->integer('dias_acreditacion')->nullable()->after('cuotas');
        });
    }

    public function down(){
        Schema::table('plan_tarjetas', function (Blueprint $table) {
            $table->dropColumn('dias_acreditacion');
        });
    }
}
