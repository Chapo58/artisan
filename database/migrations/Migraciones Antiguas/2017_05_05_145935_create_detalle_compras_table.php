<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDetalleComprasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalles_compras', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('compra_id')->unsigned();
            $table->foreign('compra_id')->references('id')->on('compras');
            $table->integer('articulo_id')->nullable()->unsigned();
            $table->foreign('articulo_id')->references('id')->on('articulos');
            $table->decimal('precio',10,2);
            $table->decimal('importe_iva',10,2)->nullable();
            $table->string('descripcion')->nullable();
            $table->decimal('cantidad',10,2);
			$table->decimal('porcentaje_iva',10,2)->default(0);
            $table->integer('estado');
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('detalles_compras');
    }
}
