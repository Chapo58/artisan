<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCuentasCorrientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cuentas_corrientes', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('cliente_id')->unsigned()->nullable();
            $table->foreign('cliente_id')->references('id')->on('clientes');
            $table->integer('entidad_id')->unsigned()->nullable();
            $table->string('url')->nullable();
            $table->string('detalle')->nullable();
            $table->integer('tipo_comprobante')->nullable();
            $table->integer('tipo_cuenta_corriente_id')->unsigned()->nullable();
            $table->foreign('tipo_cuenta_corriente_id')->references('id')->on('tipos_cuenta_corriente');
            $table->decimal('debe',10,2)->default(0);
            $table->decimal('haber',10,2)->default(0);
            $table->dateTime('proximo_vencimiento')->nullable();
            $table->integer('empresa_id')->unsigned();
            $table->foreign('empresa_id')->references('id')->on('empresas');
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cuentas_corrientes');
    }
}
