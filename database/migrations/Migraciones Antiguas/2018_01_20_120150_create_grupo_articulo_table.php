<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGrupoArticuloTable extends Migration{

    public function up(){
        Schema::create('grupos_articulos', function(Blueprint $table) {
            $table->increments('id');
            $table->string('nombre_grupo',100);
            $table->integer('empresa_id')->unsigned();
            $table->foreign('empresa_id')->references('id')->on('empresas');
            $table->integer('rubro_id')->nullable()->unsigned();
            $table->foreign('rubro_id')->references('id')->on('rubros');
            $table->integer('subrubro_id')->nullable()->unsigned();
            $table->foreign('subrubro_id')->references('id')->on('subrubros');
            $table->integer('marca_id')->nullable()->unsigned();
            $table->foreign('marca_id')->references('id')->on('marcas');
            $table->integer('unidad_id')->nullable()->unsigned();
            $table->foreign('unidad_id')->references('id')->on('unidades');
            $table->integer('moneda_id')->nullable()->unsigned();
            $table->foreign('moneda_id')->references('id')->on('monedas');
            $table->integer('codigo')->nullable();
            $table->string('codigo_barra',20)->nullable();
            $table->string('nombre',254)->nullable();
            $table->integer('porcentaje_iva')->nullable();
            $table->decimal('cantidad', 10, 2)->nullable();
            $table->string('imagen_url',254)->nullable();
            $table->decimal('ultimo_costo',10,2)->nullable();
            $table->decimal('impuesto',10,2)->nullable();
            $table->decimal('utilidad',10,2)->nullable();
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    public function down(){
        Schema::drop('grupos_articulos');
    }
}
