<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetallesPlanillasCortesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalles_planillas_cortes', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('planillas_corte_id')->unsigned();
            $table->foreign('planillas_corte_id')->references('id')->on('planillas_corte');
            $table->integer('rollo_id')->unsigned();
            $table->foreign('rollo_id')->references('id')->on('rollos');
            $table->string('color', 100);
            $table->string('tela', 100);
            $table->decimal('peso', 10, 2);
            $table->integer('tiradas');
            $table->decimal('restos', 10, 2)->nullable();
            $table->decimal('prendas', 10, 2);
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('detalles_planillas_cortes');
    }
}
