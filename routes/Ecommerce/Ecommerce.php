<?php

    Route::get('/ecommerce/frame/{id}', 'SetFrameController@setEmpresaId');

    Route::get('/ecommerce', 'IndexController@index')->name('index');
    Route::get('/ecommerce/categoria/{id}', 'IndexController@categoria');
    Route::get('/ecommerce/subcategoria/{id}', 'IndexController@subcategoria');
    Route::get('/ecommerce/marca/{id}', 'IndexController@marca');
    Route::get('/ecommerce/pagina/{id}', 'IndexController@verPagina');
    Route::get('/ecommerce/contacto', 'IndexController@contacto');
    Route::post('/ecommerce/contacto/mailContacto', 'IndexController@mailContacto');
    Route::get('/ecommerce/informacion/privacidad', 'IndexController@politicaDePrivacidad');
    Route::get('/ecommerce/informacion/terminos-condiciones', 'IndexController@terminosYCondiciones');
    Route::post('/ecommerce/buscar', 'IndexController@buscar');
    Route::post('/ecommerce/nuevaSuscripcion', 'IndexController@nuevaSuscripcion');


    Route::get('/ecommerce/carrito', 'CarritoController@index');
    Route::get('/ecommerce/checkout', 'CarritoController@checkout');
    Route::post('/ecommerce/realizarCompra', 'CarritoController@nuevaOrdenDeVenta');
    Route::post('/ecommerce/agregar-producto-carrito', 'CarritoController@agregar');
    Route::post('/ecommerce/eliminar-producto-carrito', 'CarritoController@eliminar');
    Route::post('/ecommerce/actualizar-carrito', 'CarritoController@actualizar');


    Route::get('/ecommerce/producto/{id}', 'ProductoController@verProducto');
    Route::post('/ecommerce/modal-producto', 'ProductoController@modalProducto');

    Route::get('/ecommerce/login', 'LoginController@login')->name('login');
    Route::get('/ecommerce/dashboard', 'LoginController@dashboard')->name('dashboard');
    Route::get('/ecommerce/logout', 'LoginController@logout')->name('logout');
    Route::post('/ecommerce/registro', 'LoginController@registro');
    Route::post('/ecommerce/actualizarDatosUsuario', 'LoginController@actualizarDatos');
    Route::post('/ecommerce/actualizarDireccion', 'LoginController@actualizarDireccion');

