<?php

/**
 * All route names are prefixed with 'admin.'.
 */
Route::get('dashboard', 'DashboardController@index')->name('dashboard');

/*
 * Empresas
 */
Route::group(['namespace' => 'Empresas', 'as' => 'empresas.'], function () {
    Route::get('empresas/empresa/{id}/actualizar-empresa', 'EmpresaController@actualizarEmpresa');
    Route::get('empresas/empresa/{id}/permisos-empresa', 'PermisoEmpresaController@index');
    Route::post('empresas/empresa/permisos-empresa/guardar', 'PermisoEmpresaController@guardar');
    Route::resource('empresas/empresa', 'EmpresaController');

    Route::resource('empresas/distribuidor', 'DistribuidorController');
});

Route::group(['namespace' => 'Planes', 'as' => 'planes.'], function () {
    Route::resource('planes/plan', 'PlanController');

    Route::resource('planes/pago', 'PagoController');

    Route::post('planes/efectivo/guardar', 'EfectivoController@guardar');
    Route::post('planes/deposito/guardar', 'DepositoController@guardar');

    Route::resource('planes/efectivo', 'EfectivoController');
    Route::resource('planes/deposito', 'DepositoController');
});

Route::group(['namespace' => 'Configuraciones', 'as' => 'configuraciones.'], function () {

    Route::post('configuraciones/tawkto/guardar', 'TawktoController@guardar');
    Route::resource('configuraciones/tawkto', 'TawktoController');

    Route::post('configuraciones/actualizar/actualizar', 'ActualizarController@actualizar');
    Route::resource('configuraciones/actualizar', 'ActualizarController');

    Route::resource('configuraciones/tips', 'TipsController');

});