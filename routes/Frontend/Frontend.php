<?php

/**
 * Frontend Controllers
 * All route names are prefixed with 'frontend.'.
 */
Route::get('/', 'LandingController@index')->name('index');
Route::get('macros', 'FrontendController@macros')->name('macros');
Route::get('welcome', 'FrontendController@welcome')->name('welcome');
Route::post('configuraciones/primerosPasos', 'FrontendController@guardarDatos');

/*
 * These frontend controllers require the user to be logged in
 * All route names are prefixed with 'frontend.'
 */
Route::group(['middleware' => 'auth'], function () {
    Route::group(['namespace' => 'User', 'as' => 'user.'], function () {
        /*
         * User Dashboard Specific
         */
        Route::get('dashboard', 'DashboardController@index')->name('dashboard');

        /*
         * User Account Specific
         */
        Route::get('account', 'AccountController@index')->name('account');

        /*
         * User Profile Specific
         */
        Route::patch('profile/update', 'ProfileController@update')->name('profile.update');

    });

    /*
     * Módulo Articulo
     */
    Route::group(['namespace' => 'Articulos', 'as' => 'articulos.'], function () {

     //   Route::get('articulos/articulo/uploadImages','ArticuloController@uploadImages');
        Route::post('articulo',['as' => 'UploadImages','uses'=>'ArticuloController@uploadImages']);
        Route::get('articulos/articulo/create_rapido', 'ArticuloController@createRapido');
        Route::post('articulos/articulo/guardarArticuloRapido', 'ArticuloController@storeRapido');
        Route::post('articulos/articulo/importar', 'ArticuloController@importarArticulos');
        Route::get('articulos/articulo/exportar', 'ArticuloController@exportarArticulos');
        Route::get('articulos/articulo/plantilla', 'ArticuloController@plantillaArticulos');
        Route::get('articulos/articulo/autocomplete', 'ArticuloController@autocomplete');
        Route::get('articulos/articulo/obtenerArticuloConPorcentajeIva', 'ArticuloController@obtenerArticuloConPorcentajeIva');
        Route::get('articulos/articulo/obtenerPrecioArticulo/{lista_id}', 'ArticuloController@obtenerPrecioArticulo');
        Route::get('articulos/articulo/obtenerPrecioArticuloNoStock/{lista_id}', 'ArticuloController@obtenerPrecioArticuloNoStock');
        Route::get('articulos/articulo/obtenerArticulo', 'ArticuloController@obtenerArticulo');
        Route::get('articulos/articulo/obtenerStockArticulo/{sucursal_id}', 'ArticuloController@obtenerStockArticulo');
        Route::get('articulo', ['as' => 'ArticulosData', 'uses' => 'ArticuloController@datos']);
        Route::resource('articulos/articulo', 'ArticuloController');


        Route::get('grupo-articulo', ['as' => 'GruposArticulosData', 'uses' => 'GrupoArticuloController@datos']);
        Route::resource('articulos/grupo-articulo', 'GrupoArticuloController');

        Route::resource('articulos/detalle-articulo', 'DetalleArticuloController');

        Route::post('articulos/rubro/guardarNuevoRubro', 'RubroController@guardarNuevoRubro');
        Route::get('articulos/rubro/autocomplete', 'RubroController@autocomplete');
        Route::get('rubro', ['as' => 'RubrosData', 'uses' => 'RubroController@datos']);
        Route::resource('articulos/rubro', 'RubroController');

        Route::post('articulos/subrubro/guardarNuevoSubrubro', 'SubrubroController@guardarNuevoSubrubro');
        Route::get('articulos/subrubro/autocomplete', 'SubrubroController@autocomplete');
        Route::get('subrubro/{id}', ['as' => 'SubRubrosData', 'uses' => 'SubrubroController@datos']);
        Route::get('articulos/subrubro/create/{id}', 'SubrubroController@create');
        Route::resource('articulos/subrubro', 'SubrubroController');

        Route::post('articulos/marca/guardarNuevaMarca', 'MarcaController@guardarNuevaMarca');
        Route::get('marca', ['as' => 'MarcasData', 'uses' => 'MarcaController@datos']);
        Route::resource('articulos/marca', 'MarcaController');

        Route::get('articulos/stock/solicitud-masiva', 'StockController@solicitudMasiva');
        Route::post('articulos/stock/guardarSolicitudMasiva', 'StockController@guardarSolicitudMasiva');
        Route::post('articulos/stock/transferir', 'StockController@transferir');
        Route::resource('articulos/stock', 'StockController');

        Route::get('articulos/movimiento-stock/sucursales', 'MovimientoStockController@sucursales');
        Route::get('articulos/movimiento-stock/{id}/create', 'MovimientoStockController@create');
        Route::get('articulos/movimiento-stock/aprobar/{id}', 'MovimientoStockController@aprobar');
        Route::get('articulos/movimiento-stock/rechazar/{id}', 'MovimientoStockController@rechazar');
        Route::get('movimiento-stock', ['as' => 'MovimientosStockData', 'uses' => 'MovimientoStockController@datos']);
        Route::resource('articulos/movimiento-stock', 'MovimientoStockController');

        Route::resource('articulos/tipo-movimiento-stock', 'TipoMovimientoStockController');

    });

    Route::group(['namespace' => 'Personas', 'as' => 'personas.'], function () {

        Route::get('personas/proveedor/autocomplete', 'ProveedorController@autocomplete');
        Route::get('personas/proveedor/obtenerProveedorParaCompra', 'ProveedorController@obtenerProveedorParaCompra');
        Route::post('personas/proveedor/guardarNuevoProveedor', 'ProveedorController@guardarNuevoProveedor');
        Route::resource('personas/proveedor', 'ProveedorController');
        Route::get('proveedor', ['as' => 'ProveedoresData', 'uses' => 'ProveedorController@datos']);
        Route::get('personas/proveedor/{id}/showArticulos', 'ProveedorController@showArticulos');

        Route::post('personas/cliente/importar', 'ClienteController@importarClientes');
        Route::get('personas/cliente/plantilla', 'ClienteController@plantillaClientes');
        Route::get('personas/cliente/exportar', 'ClienteController@exportarClientes');
        Route::get('personas/cliente/obtenerClienteParaVenta', 'ClienteController@obtenerClienteParaVenta');
        Route::post('personas/cliente/guardarNuevoCliente', 'ClienteController@guardarNuevoCliente');
        Route::get('cliente', ['as' => 'ClientesData', 'uses' => 'ClienteController@datos']);
        Route::resource('personas/cliente', 'ClienteController');
    });

    Route::group(['namespace' => 'Ventas', 'as' => 'ventas.'], function () {

        Route::get('ventas/venta/create_rapido', 'VentaController@createRapido');
        Route::post('ventas/venta/guardarVentaRapida', 'VentaController@storeRapido');
        Route::get('ventas/venta/obtenerVenta', 'VentaController@obtenerVenta');
        Route::post('ventas/venta/setEstadoFacturacion', 'VentaController@setEstadoFacturacion');
        Route::get('ventas/venta/imprimir/{id}', 'VentaController@imprimir');
        Route::get('factura-venta-automatica', ['as' => 'VentasAutomaticasData', 'uses' => 'VentaController@facturasAutomaticasDatos']);
        Route::get('ventas/venta/listado-ventas-automaticas', 'VentaController@listadoVentasAutomaticas');
        Route::post('ventas/venta/obtenerEstadoVenta', 'VentaController@obtenerEstadoVenta');
        Route::get('venta', ['as' => 'VentasData', 'uses' => 'VentaController@datos']);
        Route::resource('ventas/venta', 'VentaController');

        Route::get('ventas/lista-precios/imprimir', 'ListaPreciosController@imprimir');
        Route::post('ventas/lista-precios/generarPdf', 'ListaPreciosController@generarPdf');
        Route::resource('ventas/lista-precios', 'ListaPreciosController');

        Route::post('ventas/precio/create/{id}', 'PrecioController@create');
        Route::resource('ventas/precio', 'PrecioController');

        Route::post('ventas/cupon-tarjeta/acreditar', 'CuponTarjetaController@acreditar');
        Route::get('cupon-tarjeta', ['as' => 'CuponesTarjetasData', 'uses' => 'CuponTarjetaController@datos']);
        Route::resource('ventas/cupon-tarjeta', 'CuponTarjetaController');

        Route::get('ventas/cuenta-corriente/show-cliente/{cliente_id}', 'CuentaCorrienteController@showCuentaCorrienteCliente');
        Route::get('ventas/cuenta-corriente/show-proveedor/{proveedor_id}', 'CuentaCorrienteController@showCuentaCorrienteProveedor');
        Route::get('cuenta-corriente', ['as' => 'CuentasCorrientesData', 'uses' => 'CuentaCorrienteController@datos']);
        Route::resource('ventas/cuenta-corriente', 'CuentaCorrienteController');

        Route::get('ventas/recibo/imprimir/{id}', 'ReciboController@imprimir');
        Route::get('ventas/recibo/crearReciboCliente/{cliente_id}', 'ReciboController@crearReciboCliente');
        Route::get('recibo', ['as' => 'RecibosData', 'uses' => 'ReciboController@datos']);
        Route::resource('ventas/recibo', 'ReciboController');

        Route::post('ventas/nota-credito/setEstadoFacturacion', 'NotaCreditoController@setEstadoFacturacion');
        Route::get('ventas/nota-credito/imprimir/{id}', 'NotaCreditoController@imprimir');
        Route::get('nota-credito', ['as' => 'NotasCreditoData', 'uses' => 'NotaCreditoController@datos']);
        Route::resource('ventas/nota-credito', 'NotaCreditoController');

        Route::post('ventas/nota-debito/setEstadoFacturacion', 'NotaDebitoController@setEstadoFacturacion');
        Route::get('ventas/nota-debito/imprimir/{id}', 'NotaDebitoController@imprimir');
        Route::get('nota-debito', ['as' => 'NotasDebitoData', 'uses' => 'NotaDebitoController@datos']);
        Route::resource('ventas/nota-debito', 'NotaDebitoController');

        Route::get('ventas/presupuesto/imprimir/{id}', 'PresupuestoController@imprimir');
        Route::get('ventas/presupuesto/generar-venta/{id}', 'PresupuestoController@generarVenta');
        Route::get('presupuesto', ['as' => 'PresupuestosData', 'uses' => 'PresupuestoController@datos']);
        Route::resource('ventas/presupuesto', 'PresupuestoController');

        Route::get('ventas/facturacion-fiscal/facturarVenta/{id}', 'FacturacionFiscal@facturarVenta');
        Route::get('ventas/facturacion-fiscal/facturarNotaDeCredito/{id}', 'FacturacionFiscal@facturarNotaDeCredito');
        Route::get('ventas/facturacion-fiscal/facturarNotaDeDebito/{id}', 'FacturacionFiscal@facturarNotaDeDebito');

        Route::get('ventas/facturacion-electronica/facturarVenta/{id}', 'FacturacionElectronica@facturarVenta');
        Route::get('ventas/facturacion-electronica/facturarNotaDeCredito/{id}', 'FacturacionElectronica@facturarNotaDeCredito');
        Route::get('ventas/facturacion-electronica/facturarNotaDeDebito/{id}', 'FacturacionElectronica@facturarNotaDeDebito');

        Route::get('admin/factura-venta-automatica/generar-facturas', 'FacturaVentaAutomaticaController@generarFacturas');
        Route::resource('ventas/factura-venta-automatica', 'FacturaVentaAutomaticaController');

        Route::get('ventas/calcularTotales', 'CalculosVistas@calcularTotales');
        Route::post('ventas/calcularTotales', 'CalculosVistas@calcularTotales');
    });

    Route::group(['namespace' => 'Compras', 'as' => 'compras.'], function () {

        Route::get('compras/compra/obtenerDatos', 'CompraController@obtenerDatos');
        Route::get('compra', ['as' => 'ComprasData', 'uses' => 'CompraController@datos']);
        Route::resource('compras/compra', 'CompraController');

        Route::get('compras/orden-pago/crearOrdenPagoProveedor/{proveedor_id}', 'OrdenPagoController@crearOrdenPagoProveedor');
        Route::get('orden-pago', ['as' => 'OrdenesPagoData', 'uses' => 'OrdenPagoController@datos']);
        Route::resource('compras/orden-pago', 'OrdenPagoController');

        Route::get('orden-compra', ['as' => 'OrdenesComprasData', 'uses' => 'OrdenCompraController@datos']);
        Route::get('compras/orden-compra/generar-compra/{id}', 'OrdenCompraController@generarCompra');
        Route::resource('compras/orden-compra', 'OrdenCompraController');

        Route::post('compras/calcularTotales', 'CalculosVistas@calcularTotales');
    });

    Route::group(['namespace' => 'Configuraciones', 'as' => 'configuraciones.'], function () {

        Route::get('configuraciones/moneda/actualizarDolar/{id}', 'MonedaController@actualizarDolar');
        Route::post('configuraciones/moneda/obtenerCotizacion', 'MonedaController@obtenerCotizacion');
        Route::resource('configuraciones/moneda', 'MonedaController');

        Route::get('configuraciones/punto-de-venta/generarCSR', 'PuntoDeVentaController@generarCSR');
        Route::resource('configuraciones/punto-de-venta', 'PuntoDeVentaController');

        Route::post('configuraciones/tarjeta/planesDeTarjeta', 'TarjetaController@planesDeTarjeta');
        Route::resource('configuraciones/tarjeta', 'TarjetaController');

        Route::get('plan-tarjeta', ['as' => 'PlanTarjetaData', 'uses' => 'PlanTarjetaController@datos']);
        Route::post('configuraciones/plan-tarjeta/datosDePlanTarjeta', 'PlanTarjetaController@datosDePlanTarjeta');
        Route::resource('configuraciones/plan-tarjeta', 'PlanTarjetaController');

        Route::resource('configuraciones/tarjeta-propia', 'TarjetaPropiaController');

        Route::get('usuario', ['as' => 'UsuarioData', 'uses' => 'UsuarioController@datos']);
        Route::get('configuraciones/usuario/{id}/log', 'UsuarioController@showLog');
        Route::post('configuraciones/usuario/marcarTipLeido', 'UsuarioController@marcarTipLeido');
        Route::resource('configuraciones/usuario', 'UsuarioController');

        Route::resource('configuraciones/historial', 'HistorialController');

        Route::get('opcion', ['as' => 'OpcionData', 'uses' => 'OpcionController@datos']);
        Route::post('configuraciones/opcion/obtenerNumeracion', 'OpcionController@obtenerNumeracionPorTipoComprobante');
        Route::resource('configuraciones/opcion', 'OpcionController');

        Route::resource('configuraciones/actividad-afip', 'ActividadAfipController');

        Route::post('configuraciones/provincia/guardarNuevaProvincia', 'ProvinciaController@guardarNuevaProvincia');
        Route::post('configuraciones/localidad/guardarNuevaLocalidad', 'LocalidadController@guardarNuevaLocalidad');

        Route::resource('configuraciones/datos-empresa', 'DatosEmpresaController');

        Route::resource('admin/configuraciones/unidad', 'UnidadController');

        Route::resource('admin/configuraciones/localidad', 'LocalidadController');

        Route::resource('admin/configuraciones/provincia', 'ProvinciaController');

        Route::resource('admin/configuraciones/pais', 'PaisController');

        Route::resource('configuraciones/soporte', 'SoporteController');

        Route::post('configuraciones/tipo-cuenta-corriente/datosTipo', 'TipoCuentaCorrienteController@datosTipo');
        Route::resource('configuraciones/tipo-cuenta-corriente', 'TipoCuentaCorrienteController');

        Route::post('configuraciones/apariencia/actualizar', 'AparienciaController@actualizar');
        Route::resource('configuraciones/apariencia', 'AparienciaController');

        Route::post('configuraciones/pago/obtenerInformacion', 'PagoController@obtenerInformacion');
        Route::resource('configuraciones/pago', 'PagoController');
    });

    Route::group(['namespace' => 'Fondos', 'as' => 'fondos.'], function () {

        Route::resource('fondos/banco', 'BancoController');

        Route::resource('fondos/cuenta-bancaria', 'CuentaBancariaController');

        Route::resource('fondos/chequera', 'ChequeraController');

        Route::resource('fondos/gasto-bancario', 'GastoBancarioController');

        Route::get('cheque-propio', ['as' => 'ChequesPropiosData', 'uses' => 'ChequePropioController@datos']);
        Route::resource('fondos/cheque-propio', 'ChequePropioController');

        Route::get('fondos/movimiento-bancario/create/{tipo}', 'MovimientoBancarioController@create');
        Route::resource('fondos/movimiento-bancario', 'MovimientoBancarioController');

        Route::post('fondos/cheque/depositar', 'ChequeController@depositar');
        Route::get('fondos/cheque/cobrar/{id}', 'ChequeController@cobrar');
        Route::post('fondos/cheque/datos-cheque', 'ChequeController@datosCheque');
        Route::get('cheque', ['as' => 'ChequesData', 'uses' => 'ChequeController@datos']);
        Route::resource('fondos/cheque', 'ChequeController');
    });


    Route::group(['namespace' => 'Caja', 'as' => 'caja.'], function () {

        Route::resource('caja/cajas', 'CajaController');

        Route::post('caja/movimiento-caja/asignar', 'MovimientoCajaController@asignarCaja');
        Route::post('caja/movimiento-caja/abrir', 'MovimientoCajaController@abrirCaja');
        Route::post('caja/movimiento-caja/cerrar', 'MovimientoCajaController@cerrarCaja');
        Route::post('caja/movimiento-caja/filtrar', 'MovimientoCajaController@filtrar');
        Route::get('caja/movimiento-caja/listar/{id}', 'MovimientoCajaController@listar');
        Route::get('caja/movimiento-caja/create/{tipo}', 'MovimientoCajaController@create');
        Route::get('caja/movimiento-caja/imprimir/{id}', 'MovimientoCajaController@imprimir');
        Route::resource('caja/movimiento-caja', 'MovimientoCajaController');

        Route::get('concepto-caja', ['as' => 'ConceptosCajaData', 'uses' => 'ConceptoCajaController@datos']);
        Route::resource('caja/concepto-caja', 'ConceptoCajaController');

        Route::get('cierre-caja', ['as' => 'CierreCajaData', 'uses' => 'CierreCajaController@datos']);
        Route::resource('caja/cierre-caja', 'CierreCajaController');
    });

    Route::group(['namespace' => 'Produccion', 'as' => 'produccion.'], function () {

        Route::resource('produccion/tipo-tela', 'TipoTelaController');

        Route::resource('produccion/color', 'ColorController');

        Route::get('produccion/rollo/obtenerRollo', 'RolloController@obtenerRollo');
        Route::get('produccion/rollo/ingresoMasivoDeRollos', 'RolloController@ingresoMasivoDeRollos');
        Route::post('produccion/rollo/guardarIngresoMasivo', 'RolloController@guardarIngresoMasivo');
        Route::get('rollo', ['as' => 'RollosData', 'uses' => 'RolloController@datos']);
        Route::resource('produccion/rollo', 'RolloController');

        Route::get('produccion/planilla-corte/asignar/{id}', 'PlanillaCorteController@asignarTaller');
        Route::resource('produccion/planilla-corte', 'PlanillaCorteController');

        Route::resource('produccion/talleres', 'TalleresController');

        Route::resource('produccion/tipo-corte', 'TipoCorteController');

        Route::resource('produccion/tipo-producto', 'TipoProductoController');

        Route::resource('produccion/orden-pedido', 'OrdenPedidoController');

    });

    Route::group(['namespace' => 'Informes', 'as' => 'informes.'], function () {

        Route::post('informes/libro-iva-compra/imprimir', 'LibroIvaCompraController@imprimir');
        Route::resource('informes/libro-iva-compra', 'LibroIvaCompraController');

        Route::post('informes/libro-iva-venta/imprimir', 'LibroIvaVentaController@imprimir');
        Route::resource('informes/libro-iva-venta', 'LibroIvaVentaController');

        Route::post('informes/actividad/imprimir', 'ActividadController@imprimir');
        Route::resource('informes/actividad', 'ActividadController');

        Route::post('informes/regimen/comprobantesVentas', 'RegimenController@comprobantesVentas');
        Route::post('informes/regimen/alicuotasVentas', 'RegimenController@alicuotasVentas');
        Route::post('informes/regimen/comprobantesCompras', 'RegimenController@comprobantesCompras');
        Route::post('informes/regimen/alicuotasCompras', 'RegimenController@alicuotasCompras');
        Route::resource('informes/regimen', 'RegimenController');

        Route::resource('informes/conceptos-caja', 'ConceptoCajaController');

        Route::post('informes/vendedor/imprimir', 'VendedorController@imprimir');
        Route::resource('informes/vendedor', 'VendedorController');

    });

    Route::group(['namespace' => 'Calendario', 'as' => 'Calendario.'], function () {

        Route::resource('calendario/tareas', 'EventsController');

    });

    Route::group(['namespace' => 'Ecommerce', 'as' => 'ecommerce.'], function () {

        Route::resource('ecommerce/slider', 'SliderController');
        Route::resource('ecommerce/paginas', 'PaginasController');
        Route::resource('ecommerce/caracteristicas', 'CaracteristicasController');
        Route::get('ecommerce/parametros/downloadIndex', 'ParametrosController@downloadIndex');
        Route::post('ecommerce/parametros/guardar', 'ParametrosController@guardar');
        Route::resource('ecommerce/parametros', 'ParametrosController');
        Route::resource('ecommerce/suscripciones', 'SuscripcionesController');
        Route::resource('ecommerce/formas-pago', 'FormasPagoController');

        Route::get('ecommerce/orden-venta/imprimir/{id}', 'OrdenVentaController@imprimir');
        Route::get('ecommerce/orden-venta/generar-venta/{id}', 'OrdenVentaController@generarVenta');
        Route::get('orden-venta', ['as' => 'OrdenesVentasData', 'uses' => 'OrdenVentaController@datos']);
        Route::resource('ecommerce/orden-venta', 'OrdenVentaController');

    });

});
