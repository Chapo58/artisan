<?php

/**
 * Global Routes
 * Routes that are used between both frontend and backend.
 */

// Switch between the included languages
Route::get('lang/{lang}', 'LanguageController@swap');

/* ----------------------------------------------------------------------- */

/*
 * Frontend Routes
 * Namespaces indicate folder structure
 */
Route::group(['namespace' => 'Frontend', 'as' => 'frontend.'], function () {
    includeRouteFiles(__DIR__.'/Frontend/');
});

/*
 * Ecommerce Routes
 * Namespaces indicate folder structure
 */
Route::group(['namespace' => 'Ecommerce', 'as' => 'ecommerce.'], function () {
    includeRouteFiles(__DIR__.'/Ecommerce/');
});

/* ----------------------------------------------------------------------- */

/*
 * Backend Routes
 * Namespaces indicate folder structure
 */
Route::group(['namespace' => 'Backend', 'prefix' => 'admin', 'as' => 'admin.', 'middleware' => 'admin'], function () {
    /*
     * These routes need view-backend permission
     * (good if you want to allow more than one group in the backend,
     * then limit the backend features by different roles or permissions)
     *
     * Note: Administrator has all permissions so you do not have to specify the administrator role everywhere.
     */
    includeRouteFiles(__DIR__.'/Backend/');
});

Route::get('/', 'LandingController@index')->name('index');
Route::get('/landing/registro', 'LandingController@registro')->name('registro');
Route::get('/landing/contacto', 'LandingController@contacto')->name('contacto');
Route::get('/landing/servicios', 'LandingController@servicios')->name('servicios');
Route::get('/landing/ecommerce', 'LandingController@ecommerce')->name('ecommerce');
Route::get('/landing/app', 'LandingController@app')->name('app');
Route::get('/landing/privacidad', 'LandingController@privacidad')->name('privacidad');
Route::get('/landing/terminos', 'LandingController@terminos')->name('terminos');
Route::post('/landing/contacto/enviarMail', 'LandingController@enviarMail')->name('landing.contacto.enviarMail');
Route::post('/landing/suscribirse', 'LandingController@suscribirse')->name('landing.suscribirse');