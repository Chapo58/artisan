## Sistema de Gestion v1.0

[![Latest Stable Version](https://poser.pugx.org/rappasoft/laravel-5-boilerplate/v/stable)](https://packagist.org/packages/rappasoft/laravel-5-boilerplate) [![Latest Unstable Version](https://poser.pugx.org/rappasoft/laravel-5-boilerplate/v/unstable)](https://packagist.org/packages/rappasoft/laravel-5-boilerplate) [![StyleCI](https://styleci.io/repos/30171828/shield?style=plastic)](https://styleci.io/repos/30171828/shield?style=plastic)

### Official Documentation

[Click here for the official documentation](http://laravel-boilerplate.com)

##### Documentación Appzcoder

https://github.com/appzcoder/crud-generator

### Instalación
$composer install

$npm install

configurar el .env con la base de datos
crear db en phpmyadmin
nombre: sistema-gestion-web-v1.0
cotejamiento: utf8_general_ci

$php artisan key:generate

$php artisan migrate

$php artisan db:seed

$npm run dev

$php artisan serve

admin@admin.com pass:1234
executive@executive.com pass:1234
user@user.com pass:1234

### Uso Appzcorder
Ya está instalado en el ultimo commit.

 Generar vistas

$ php artisan crud:generate Rubro --fields="nombre#string; descripcion#text; deleted_at#timestamp#nullable;"
--model-namespace=Models\\Frontend\\Articulos
--view-path=frontend\\articulos
--controller-namespace=Frontend\\Articulos
--route-group=articulos"

Resultado: Me genera el modelo, controlador, migracion y las vistas en relacion a las generadas en view/crud-generator

$ php artisan crud:generate Subrubro --fields="nombre#string; descripcion#text; rubro_id#integer; deleted_at#timestamp#nullable" --model-namespace=Models\\Frontend\\Articulos --view-path=frontend\\articulos --controller-namespace=Frontend\\Articulos --route-group=articulos --relationships="rubro#belongsTo#Models\\Frontend\\Articulos\\Rubro"

En este caso puedo agregar relaciones, pero todavia tiene algunos errores al crear la migración. Sin la clave foranea funciona, creando el campo modelo_id donde modelo es la clase a la cual se relaciona, ya q por defento laravel guarda la relacion en ese campo.

####Funciona la validación!!

--validations="nombre#required|unique:rubros; text#required" donde rubros es el nombre de la tabla en la cual es único ese campo

Se agregó el softdelete!

$ php artisan crud:generate Prueba --fields="nombre#string; descripcion#text#nullable; deleted_at#timestamp#nullable" --model-namespace=Models\\Frontend\\Articulos --view-path=frontend\\articulos --controller-namespace=Frontend\\Articulos --route-group=articulos --validations="nombre#required;"

$ php artisan crud:generate GastoBancario
--fields="nombre#string; porcentaje#decimal#nullable; importe#decimal#nullable; empresa_id#integer#unsigned"
--model-namespace=Models\Frontend\Fondos
--view-path=Frontend\Fondos
--controller-namespace=Frontend\Fondos
--validations="nombre#required;"

php artisan make:migration add_cuenta_bancaria_id_to_cajas_table --table=cajas

#### Modelo Box
<div class="col-xs-12 col-sm-7">
  <div class="widget-box">
    <div class="widget-header">
      <h4 class="widget-title">Titulo</h4>
      <div class="widget-toolbar">
        <a href="#" data-action="collapse">
          <i class="ace-icon fa fa-chevron-up"></i>
        </a>
      </div>
    </div>
    <div class="widget-body">
      <div class="widget-main">

        Cuerpo del box

      </div>
    </div>
  </div>
</div>

<div class="form-group"></div>
<div class="clearfix form-actions">
  <div class="col-md-offset-3 col-md-9">
    {{ Form::button(isset($submitButtonText) ? '<i class="ace-icon fa fa-check bigger-110"></i> '.$submitButtonText : '<i class="ace-icon fa fa-check bigger-110"></i> '.trans('buttons.general.save'), array('type' => 'submit', 'class' => 'btn btn-info pull-right')) }}
  </div>
</div>

#### Instalacion DomPDF -> https://github.com/dompdf/dompdf
composer require dompdf/dompdf
composer require barryvdh/laravel-dompdf

composer update

#### Image.Intervention -> http://image.intervention.io/getting_started/introduction ####

composer require intervention/image

activar en php.ini la ext  "fileinfo"

#### Instalacion Laravel Excel -> https://github.com/Maatwebsite/Laravel-Excel

composer require "maatwebsite/excel:~2.1.0"

#### FACTURACION ELECTRONICA -> https://github.com/ivanalemunioz/afip-php

#### CONTROLADORA FISCAL -> http://bitingenieria.com.ar/doc/ifu/IFUniversal_TLB/IDriver.html

Emulador Hasar 1000: www.bitingenieria.com.ar/emuladorhasar.zip

#### Instalacion Charts -> https://erik.cat/projects/Charts/docs/5

composer require consoletvs/charts:5.*

#### Instalación ShoppingCart -> https://github.com/Crinsane/LaravelShoppingcart

composer require gloudemans/shoppingcart